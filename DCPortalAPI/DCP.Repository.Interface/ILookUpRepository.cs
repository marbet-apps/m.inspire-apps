﻿using DCP.Models;
using System.Collections.Generic;

namespace DCP.Repository.Interface
{
    public interface ILookUpRepository
    {
        /// <summary>
        /// Country List
        /// </summary>
        /// <returns></returns>
        TemplateLookUp GetCountryList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Radius List
        /// </summary>
        /// <returns></returns>
        TemplateLookUp GetRadiusList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Hotel Chain List
        /// </summary>
        /// <returns></returns>
        TemplateLookUpString GetChainList( string term, int page, int pageSize, int type);

        /// <summary>
        /// Hotel List
        /// </summary>
        /// <returns></returns>
        TemplateLookUpString GetHotelList(string term, int page, int pageSize, int type);

        /// <summary>
        /// Restaurant List
        /// </summary>
        /// <returns></returns>
        TemplateLookUpString GetRestaurantList(string term, int page, int pageSize, int type);

        /// <summary>
        /// Location List
        /// </summary>
        /// <returns></returns>
        TemplateLookUpString GetLocationList(string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Hotel Type List
        /// </summary>
        TemplateLookUp GetHotelTypeList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Default Hotel Type List
        /// </summary>
        TemplateLookUp GetDefaultHotelTypeList(string Lang,int[] Ids);


        /// <summary>
        /// Get Restaurant Type List
        /// </summary>
        TemplateLookUp GetRestaurantTypeList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Location Type List
        /// </summary>
        TemplateLookUp GetLocationTypeList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Kitchen List
        /// </summary>
        TemplateLookUp GetKitchenList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Kitchen List
        /// </summary>
        TemplateLookUp GetSuitableForList(string Lang, string term, int page, int pageSize, int type);
        /// <summary>
        /// Get Subscription List
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        TemplateLookUp GetSubscriptionTypeList(string Lang);
        /// <summary>
        /// Get Request list.
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        TemplateLookUpString GetRequestStatusList(string Lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        TemplateLookUpString GetRequestTypes(string lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        TemplateLookUpString GetRequestItemTypes(string lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        TemplateLookUpString GetRoomTypes(string lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        TemplateLookUpString GetRoomTechniques(string lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        TemplateLookUpString GetSeatingTypes(string lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        TemplateLookUpString GetDaysRunning(string lang);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        TemplateLookUpString GetTimeIntervals(int interval);

        /// <summary>
        /// Get City list.
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        TemplateLookUpString GetCityList();

        /// <summary>
        /// Get Abort Reason.
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        TemplateLookUpString GetAbortReason(string lang);

    }
}
