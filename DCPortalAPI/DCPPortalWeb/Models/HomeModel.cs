﻿using DCP.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DCPPortalWeb.Models
{
    public class HomeModel
    {
        public HomeModel()
        {
            _headersearch = new HeaderSearch();
        }

        public HeaderSearch _headersearch;
    }

    public class MyTripModel
    {
        public MyTripModel()
        {
            list_trip = new List<Lookup>();
        }

        public IList<Lookup> list_trip { get; set; }

        public int trip_id { get; set; }
    }

    public class LeftFilters
    {
        public LeftFilters()
        {
            type = new MultiSelectList(Enumerable.Empty<IList>());
            suitablefor = new MultiSelectList(Enumerable.Empty<IList>());
            kitchen = new MultiSelectList(Enumerable.Empty<IList>());
            chain = new MultiSelectList(Enumerable.Empty<IList>());
        }

        public MultiSelectList suitablefor { get; set; }
        public MultiSelectList type { get; set; }
        public MultiSelectList chain { get; set; }
        public MultiSelectList kitchen { get; set; }

        public int[] ids { get; set; }
        public int[] kitchen_id { get; set; }
        public int[] chain_id { get; set; }
        public string[] id { get; set; }
        public int[] suitablefor_id { get; set; }
        public string city { get; set; }
    }

    public class HeaderSearch
    {
        public HeaderSearch()
        {
            list_country = new List<Lookup>();
            list_radius = new List<Lookup>();
            suitableforL = new MultiSelectList(Enumerable.Empty<IList>());
            suitableforR = new MultiSelectList(Enumerable.Empty<IList>());
            kitchen = new MultiSelectList(Enumerable.Empty<IList>());
            Locationtype = new MultiSelectList(Enumerable.Empty<IList>());
            Restauranttype = new MultiSelectList(Enumerable.Empty<IList>());
            Hoteltype = new MultiSelectList(Enumerable.Empty<IList>());
        }

        public MultiSelectList kitchen { get; set; }
        public MultiSelectList suitableforL { get; set; }
        public MultiSelectList suitableforR { get; set; }
        public MultiSelectList Locationtype { get; set; }
        public MultiSelectList Restauranttype { get; set; }
        public MultiSelectList Hoteltype { get; set; }
        public IList<Lookup> list_country { get; set; }
        public IList<Lookup> list_radius { get; set; }

        public int user_id { get; set; }
        public int country_id { get; set; }
        public int radius_id { get; set; }
        public int[] suitableforL_id { get; set; }
        public int[] suitableforR_id { get; set; }
        public int[] kitchen_id { get; set; }
        public int[] Locationtype_id { get; set; }
        public int[] Restauranttype_id { get; set; }
        public int[] Hoteltype_id { get; set; }

        public int[] star { get; set; }
        public int min_total_rooms { get; set; }
        public int max_total_rooms { get; set; }
        public int min_total_conference { get; set; }
        public int max_total_conference { get; set; }

        public double min_size_of_largest_conference { get; set; }
        public double max_size_of_largest_conference { get; set; }

        public string destination { get; set; }
        public string city { get; set; }

    }

    public class FilterModel
    {
        public int country { get; set; }
        public string chains { get; set; }
        public string city { get; set; }
        public string Typeids { get; set; }
        public string name { get; set; }
        public string totalroom { get; set; }
        public string totalcroom { get; set; }
        public string sizecroom { get; set; }
        public string parkingspace { get; set; }
        public string chkpp { get; set; }
        public string chkclose { get; set; }
        public string chkblocked { get; set; }
        public string chkgreen { get; set; }
        public string chkreopen { get; set; }
        public string chkblank { get; set; }
        public string distance { get; set; }
        public string location { get; set; }
        public string hours { get; set; }
        public string minutes { get; set; }
        public string stars { get; set; }
        public string loc { get; set; }
        public int recorddisplayed { get; set; }
        public int totalrecord { get; set; }

        public bool IsLU { get; set; }
        public string LUType { get; set; }
    }

    public class RFilterModel
    {
        public int country { get; set; }
        public string city { get; set; }
        public string name { get; set; }
        public string kids { get; set; }
        public string Typeids { get; set; }
        public string Suitableids { get; set; }
        public string Rtotalroom { get; set; }
        public string Rsizecroom { get; set; }
        public string roomcapacity { get; set; }
        public string restaurantcapacity { get; set; }
        public string minimumturnover { get; set; }
        public string chkpp { get; set; }
        public string chkclose { get; set; }
        public string chkblocked { get; set; }
        public string chkgreen { get; set; }
        public string chkreopen { get; set; }
        public string chkoutdoor { get; set; }
        public string chkrestinhotel { get; set; }
        public string distance { get; set; }
        public string location { get; set; }
        public string loc { get; set; }
        public int recorddisplayed { get; set; }
        public int totalrecord { get; set; }

        public bool IsLU { get; set; }
        public string LUType { get; set; }
    }

    public class LFilterModel
    {
        public int country { get; set; }
        public string city { get; set; }
        public string name { get; set; }
        public string Suitableids { get; set; }
        public string Ltotalcroom { get; set; }
        public string Lsizecroom { get; set; }
        public string roomheight { get; set; }
        public string rentalcost { get; set; }
        public string multilocationtype { get; set; }
        public string chkpp { get; set; }
        public string chkclose { get; set; }
        public string chkblocked { get; set; }
        public string chkgreen { get; set; }
        public string chkreopen { get; set; }
        public string chkoutdoor { get; set; }
        public string chkCP { get; set; }
        public string chkTP { get; set; }
        public string chkCD { get; set; }
        public string distance { get; set; }
        public string location { get; set; }
        public string loc { get; set; }
        public int recorddisplayed { get; set; }
        public int totalrecord { get; set; }

        public bool IsLU { get; set; }
        public string LUType { get; set; }
    }

    public class ReviewModel
    {
        public int kb_type { get; set; }
        public int kb_id { get; set; }
        public int user_id { get; set; }
        public string user_name { get; set; }
        public int type { get; set; }
        public int? month { get; set; }
        public int? year { get; set; }
        public int? overall_rating { get; set; }
        public int? condition { get; set; }
        public bool? isrecommanded { get; set; }
        public string remarks { get; set; }
        public DateTime sdate { get; set; }
        public DateTime fdate { get; set; }
        public DateTime tdate { get; set; }
        public string date { get; set; }
    }

    public class DataModel
    {
        public int? stars { get; set; }
        public int id { get; set; }

        public string name { get; set; }
        public string chainname { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }

        public int? totalroom { get; set; }
        public int? totalcroom { get; set; }
        public double? sizecroom { get; set; }

        public bool chkpp { get; set; }
        public bool chkclose { get; set; }
        public bool chkblocked { get; set; }
        public bool chkgreen { get; set; }
        public bool chkreopen { get; set; }

        public int recorddisplayed { get; set; }
        public int totalrecord { get; set; }

    }

    public class PhotosEntity
    {
        public int photo_id { get; set; }
        public int tripinfo_id { get; set; }
        public int photo_type { get; set; }
        public int transaction_id { get; set; }
        public string photo_name { get; set; }
        public long photo_size { get; set; }

    }

    public class ImageUpload
    {
        public string fileurl { get; set; }
        public string fileName { get; set; }
        public string base64String { get; set; }
        public long filesize { get; set; }
    }

    /// <summary>
    /// Hotel Model
    /// </summary>
    public class HotelModel
    {
        public int id { get; set; }
        public Nullable<int> star { get; set; }
        public Nullable<int> total_rooms { get; set; }
        public Nullable<int> total_conference { get; set; }
        public Nullable<int> parking_spaces { get; set; }

        public string hotel_name { get; set; }
        public string hotel_chain { get; set; }
        public string street { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }
        public string miscellaneous_info { get; set; }
        public string airport_name { get; set; }
        public string airport_transfer_time { get; set; }
        public string hour { get; set; }
        public string minute { get; set; }

        public Nullable<double> size_of_largest_conference { get; set; }
        public Nullable<double> airport_distance { get; set; }

    }

    /// <summary>
    /// Restaurant Model
    /// </summary>
    public class RestaurantModel
    {

        public int id { get; set; }
        public string restaurant_name { get; set; }
        public string country { get; set; }
        public string street { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }
        public bool restaurant_in_hotel { get; set; }
        public string hotelname { get; set; }
        public Nullable<int> capacity { get; set; }
        public Nullable<int> total_conference { get; set; }
        public Nullable<double> size_of_largest_conference { get; set; }
        public Nullable<int> capacity_of_largest_room { get; set; }
        public bool? outdoor_possibility { get; set; }
        public string outdoor_details { get; set; }
        public float? minimum_turnover { get; set; }
        public string miscellaneous_info { get; set; }
        public string kitchen { get; set; }

    }

    /// <summary>
    /// Location Model
    /// </summary>
    public class LocationModel
    {
        public int id { get; set; }
        public string location_name { get; set; }

        public string street { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }

        public Nullable<int> total_conference { get; set; }
        public Nullable<double> size_of_largest_conference { get; set; }
        public Nullable<double> room_height { get; set; }
        public Nullable<double> rent_begining { get; set; }

        public bool? catering { get; set; }
        public bool? technik { get; set; }
        public bool? car_drive { get; set; }
        public bool? outdoor_possibility { get; set; }
        public string outdoor_details { get; set; }
        public string miscellaneous_info { get; set; }
        public string location_type { get; set; }

    }

    public class CountModel
    {
        public int Lrestaurantcount { get; set; }
        public int Llocationcount { get; set; }
        public int Lhotelcount { get; set; }
        public int Urestaurantcount { get; set; }
        public int Ulocationcount { get; set; }
        public int Uhotelcount { get; set; }
    }
}