﻿using System.Web.Optimization;

namespace DCPPortalWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Contents/LoginCss").Include(
                    "~/Content/Bootstrap/bootstrap.css",
                    "~/Component/notification/bootoast.css",
                    "~/Content/Common/common.css",
                    "~/Content/Login/login.css"));

            bundles.Add(new ScriptBundle("~/bundles/LoginJs").Include(
                        "~/Scripts/JQuery/jquery-3.3.1.js",
                        "~/Scripts/JQuery/jquery.cookie.js",
                        "~/Scripts/Bootstrap/Bootstrap.js",
                        "~/Component/notification/bootoast.js",
                        "~/Scripts/Common/common.js",
                        "~/Scripts/Common/managecode.js",
                        "~/Scripts/Common/ajaxrequest.js",
                        "~/Scripts/Login/index.js"));

            bundles.Add(new StyleBundle("~/Contents/MainCss").Include(
                        "~/Content/Home/Control.css",
                        "~/Content/Home/Headertag.css",
                        "~/Content/Home/Menuheader.css",
                        "~/Content/Home/Screen.css"));

            bundles.Add(new StyleBundle("~/Contents/HomeCss").Include(
                       "~/Content/Home/Home.css"));

            bundles.Add(new StyleBundle("~/Content/CommonCss").Include(
                       "~/Content/Bootstrap/bootstrap.css",
                       "~/Content/JQuery/jquery-ui.css",
                       "~/Component/select2/select2-bootstrap.css",
                       "~/Component/select2/select2.css",
                       "~/Component/notification/bootoast.css",
                       "~/Component/rangeslider/jquery.range.css",
                       "~/Content/Common/Common.css",
                       "~/Component/tosrus/css/jquery.tosrus.all.css"));

            bundles.Add(new ScriptBundle("~/bundles/MainJs").Include(
                      "~/Scripts/JQuery/jquery-{version}.js",
                      "~/Scripts/JQuery/jquery-ui.js",
                      "~/Scripts/JQuery/jquery.unobtrusive*",
                       "~/Scripts/JQuery/jquery.cookie.js",
                      "~/Scripts/Bootstrap/bootstrap.js",
                      "~/Component/rangeslider/jquery.range.js",
                      "~/Scripts/Home/panelslider.js",
                       "~/Component/notification/bootoast.js",
                      "~/Component/select2/select2.js",
                      "~/Scripts/Common/common.js",
                       "~/Scripts/Common/managecode.js",
                      "~/Scripts/Common/ajaxrequest.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/HomeJs").Include(
                     "~/Scripts/Home/common.js",
                     "~/Scripts/Home/Lookup.js",
                     "~/Scripts/Home/Menuheader.js",
                     "~/Scripts/Home/Search.js",
                     "~/Scripts/Home/Map.js"));

            #region CART

            bundles.Add(new ScriptBundle("~/bundles/CARTJs").Include(
                     "~/Scripts/Cart/MyCartView.js"));

            #endregion CART

            #region RFP

            bundles.Add(new StyleBundle("~/Contents/VerticalTabCss").Include(
                      "~/Content/Common/vertical-tab.css"));

            bundles.Add(new StyleBundle("~/Contents/RFPCss").Include(
                      "~/Content/RFP/*.css"));

            bundles.Add(new ScriptBundle("~/bundles/RFPJs").Include(
                     "~/Scripts/RFP/rfp.js"));

            bundles.Add(new ScriptBundle("~/bundles/MYRFPJs").Include(
                    "~/Scripts/RFP/MyRFP.js"));

            #endregion RFP

            bundles.Add(new ScriptBundle("~/bundles/InfoJs").Include(
                     "~/Scripts/Info/Index.js"));

            bundles.Add(new StyleBundle("~/Contents/InfoCss").Include(
                      "~/Content/Info/Info.css"));

            bundles.Add(new ScriptBundle("~/bundles/EditProfileJs").Include(
                    "~/Scripts/Info/EditProfile.js"));

            bundles.Add(new ScriptBundle("~/bundles/UserJs").Include(
                "~/Scripts/User/User.js"));

            bundles.Add(new StyleBundle("~/Contents/RfpDetailCss").Include(
                    "~/Content/RFP/rfpdetail.css"));

            bundles.Add(new ScriptBundle("~/bundles/ResetPasswordJs").Include(
                "~/Scripts/JQuery/jquery-{version}.js",
                 "~/Scripts/JQuery/jquery.cookie.js",
                  "~/Component/notification/bootoast.js",
                    "~/Scripts/Common/managecode.js",
                     "~/Scripts/Common/common.js",
               "~/Scripts/Login/ResetPassword.js"));


            bundles.Add(new StyleBundle("~/Contents/ResetPasswordCss").Include(
                "~/Content/Bootstrap/bootstrap.css",                
               "~/Component/notification/bootoast.css",
              "~/Content/Login/ResetPassword.css"));

            bundles.Add(new ScriptBundle("~/bundles/ThankyouJs").Include(
              "~/Scripts/JQuery/jquery-{version}.js",            
             "~/Scripts/Login/ThankYou.js"));

        }
    }
}
