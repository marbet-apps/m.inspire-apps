﻿using DCP.Business.Interface;
using DCP.Models;
using DCP.Utilities;
using DCPortalAPI.Settings;
using DCPortalAPI.Filter;
using System.Web.Http;
using DCP.Models.RFP;
using System;
using System.Linq;
using DCP.Mice.Models;
using DCP.Entities;
using Newtonsoft.Json;

namespace DCPortalAPI.Controllers
{
    [RoutePrefix("api/request")]
    [ExceptionHandler]
    public class RequestController : ApiController
    {
        private IRequestBL _IRequestBL;
        private IRequestDetail _IRequestDetail;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IRequestBl"></param>
        /// <param name="_IRequestDetail"></param>
        public RequestController(IRequestBL IRequestBl, IRequestDetail _IRequestDetail)
        {
            this._IRequestBL = IRequestBl;
            this._IRequestDetail = _IRequestDetail;
        }

        #region Cart

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cartModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("MarkResourceForTheRequest")]
        public IHttpActionResult MarkResourceForTheRequest(RequestCartModel cartModel)
        {
            WriteLog.LogInformation(LogMessages.MarkResourceForTheRequestStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);

                cartModel.UserId = userId;
                cartModel.CreatedBy = userId;
                cartModel.CreatedDate = DateTime.Now;

                if (cartModel.CartResources != null && cartModel.CartResources.Any())
                {
                    cartModel.CartResources.All(cr => {
                        cr.CreatedBy = userId;
                        cr.CreatedDate = DateTime.Now;
                        return true;
                    });
                }
                var Response = _IRequestBL.MarkResourceForTheRequest(cartModel);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.MarkResourceForTheRequestEnded);
            }
        }

        ///INSPIRE-102
        ///RFPId wise delete record from multiple RFP of user
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="resourceType"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("RemoveResourceFromTheSelection")]
        public IHttpActionResult RemoveResourceFromTheSelection(int resourceId, string resourceType, int rfpId=0)
        {
            WriteLog.LogInformation(LogMessages.RemoveResourceFromTheSelectionStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IRequestBL.RemoveResourceFromTheSelection(userId,resourceId,resourceType, rfpId);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RemoveResourceFromTheSelectionEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="resourceType"></param>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddResourceToRFP")]
        public IHttpActionResult AddResourceToRFP(string resourceId, string resourceType, int rfpId)
        {
            WriteLog.LogInformation(LogMessages.AddResourceToRFPStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = new ApiResult();
                if (resourceId != null && resourceId.Length > 0)
                {
                    var resourceIds = JsonConvert.DeserializeObject<string[]>(resourceId);
                    if (resourceIds != null && resourceIds.Length > 0)
                    {
                        Response = _IRequestBL.AddResourceToRFP(userId, resourceIds, resourceType, rfpId);
                    }
                }
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.AddResourceToRFPEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Cart")]
        public IHttpActionResult GetRequestCart()
        {
            WriteLog.LogInformation(LogMessages.GetRequestCartStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IRequestBL.GetRequestCart(userId);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRequestCartEnded);
            }
        }

        ///INSPIRE-102
        ///Call stored rfp method and return request for proposal modal
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("MakeRequestForProposal")]
        public IHttpActionResult MakeRequestForProposal(RequestForProposalModel rfpModel)
        {
            WriteLog.LogInformation(LogMessages.MakeRequestForProposalStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                rfpModel.UserId = userId;
                var Response = _IRequestBL.MakeRequestForProposal(rfpModel);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.MakeRequestForProposalEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("CartCount")]
        public IHttpActionResult GetRequestCartCount()
        {
            WriteLog.LogInformation(LogMessages.GetRequestCartCountStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IRequestBL.GetRequestCartCount(userId);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRequestCartCountEnded);
            }
        }

        #endregion Cart

        #region Lookup Methods

        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("ConferenceFoods")]
        public IHttpActionResult GetConferenceFoods()
        {
            WriteLog.LogInformation(LogMessages.GetConferenceFoodsStarted);
            try
            {
                var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                var Response = _IRequestBL.GetConferenceFoods(lang);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetConferenceFoodsEnded);
            }
        }

        #endregion Lookup Methods

        #region RFP Request Items

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpMiceRequestItemId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("RFPRequestItems")]
        public IHttpActionResult GetRFPRequestItems(int rfpMiceRequestItemId)
        {
            WriteLog.LogInformation(LogMessages.GetRFPRequestItemsStarted);
            try
            {
                var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                var Response = _IRequestBL.GetRFPRequestItems(rfpMiceRequestItemId, lang);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRFPRequestItemsEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestItemModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddEditRFPRequestItem")]
        public IHttpActionResult AddEditRFPRequestItem(RequestItemModel requestItemModel)
        {
            WriteLog.LogInformation(LogMessages.AddEditRFPRequestItemStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                requestItemModel.CreatedBy = userId;
                requestItemModel.CreatedDate = DateTime.Now;
                var Response = _IRequestBL.AddEditRFPRequestItem(requestItemModel);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.AddEditRFPRequestItemEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpMiceRequestItemId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteRFPRequestItem")]
        public IHttpActionResult DeleteRFPRequestItem(int rfpMiceRequestItemId)
        {
            WriteLog.LogInformation(LogMessages.DeleteRFPRequestItemStarted);
            try
            {
                var Response = _IRequestBL.DeleteRFPRequestItem(rfpMiceRequestItemId);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.DeleteRFPRequestItemEnded);
            }
        }

        #endregion RFP Request Items

        #region RFP

        /// <summary>
        /// Get Request For Proposal
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("RequestForProposal")]
        public IHttpActionResult GetRequestForProposal()
        {
            WriteLog.LogInformation(LogMessages.GetRequestForProposalStarted);
            try
            {
                var langCode = CommonMethod.GetLanguageHeader(Request.Headers);
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IRequestBL.GetRequestForProposal(userId, langCode);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRequestForProposalEnded);
            }
        }

        /// <summary>
        /// Get Request For Proposal
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRequestForProposalById")]
        public IHttpActionResult GetRequestForProposalById(int id, string strId)
        {
            WriteLog.LogInformation(LogMessages.GetRequestForProposalStarted);
            try
            {
                string value = Crypto.DecryptString(strId);
                if(string.IsNullOrEmpty(value))
                {
                    return BadRequest();
                }
                id = Convert.ToInt32(value);
                var langCode = CommonMethod.GetLanguageHeader(Request.Headers);
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IRequestBL.GetRequestForProposalById(id, langCode);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRequestForProposalEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SubmitRFP")]
        public IHttpActionResult SubmitRFP(RequestForProposalModel rfpModel)
        {
            WriteLog.LogInformation(LogMessages.SubmitRFPStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IRequestBL.SubmitRFP(rfpModel, rfpModel.IsSubmitted, userId);

               
               

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.SubmitRFPEnded);
            }
        }

        #endregion RFP

        #region RFP Detail
        /// <summary>
        /// Retrive list for RFP.
        /// </summary>
        /// <param name="term"></param>
        /// <param name="status"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRFPList")]
        [Authorize]
        public IHttpActionResult GetRfpList(int page, int pageSize, string status, string term = "")
        {
            WriteLog.LogInformation(LogMessages.RFPListStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);

                var result = _IRequestDetail.GetRfpList(page, pageSize, status, CommonMethod.GetHeaders(Request.Headers, "Lang"), term, userId);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RFPListEnded);
            }
        }

        [HttpPost]
        [Route("BookedOrder")]
        [Authorize]
        public IHttpActionResult BookedOrder(OrderBookingModel bookingModel)
        {
            WriteLog.LogInformation(LogMessages.RFPListStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);

                var result = _IRequestBL.BookedOrder(bookingModel, userId);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RFPListEnded);
            }
        }
        /// <summary>
        /// for aborting the request.
        /// </summary>
        /// <param name="abortModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AbortOrder")]
        [Authorize]
        public IHttpActionResult AbortOrder(OrderAbortModel abortModel)
        {
            WriteLog.LogInformation(LogMessages.RFPListStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);

                var result = _IRequestBL.AbortOrder(abortModel);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RFPListEnded);
            }
        }

        ///INSPIRE-102
        ///RfpId wise return resource item detail of rfp
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortBy"></param>
        /// <param name="type"></param>
        /// <param name="requestItemType"></param>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetResourceItemDetail")]
        [Authorize]
        public IHttpActionResult GetResourceItemDetail(string sortBy, string type, string requestItemType, int rfpId)
        {
            WriteLog.LogInformation(LogMessages.GetResourceItemDetailStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var langCode = CommonMethod.GetLanguageHeader(Request.Headers);
                var result = _IRequestBL.GetResourceItemDetail(userId, langCode, sortBy, type, requestItemType, rfpId);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetResourceItemDetailEnded);
            }
        }

        #endregion
    }
}