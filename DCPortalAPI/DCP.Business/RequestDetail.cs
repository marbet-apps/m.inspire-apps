﻿using System;
using System.Net;
using DCP.Business.Interface;
using DCP.Entities;
using DCP.Models.RFP;
using DCP.Repository.Interface;
using DCP.Utilities;

namespace DCP.Business
{
    public class RequestDetail : IRequestDetail
    {
        private IRequestDetailRepository _IRequestDetailRepository;

        public RequestDetail(IRequestDetailRepository _IRequestDetailRepository)
        {
            this._IRequestDetailRepository = _IRequestDetailRepository;
        }
        /// <summary>
        /// Retrieve RFP List.
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="term"></param>
        /// <param name="status"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public ApiResult GetRfpList(int page, int pageSize, string status, string lang,string term = "", int? userId =0)
        {
            try
            {
                var response = _IRequestDetailRepository.getRfpList(page, pageSize, status, lang, term, userId);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }

        }
        /// <summary>
        /// Get RFP Details.
        /// </summary>
        /// <param name="rfPID"></param>
        /// <returns></returns>
        public ApiResult GetRFPDetail(int rfPID, int? userId = 0, string type = "", string lang = "")
        {
            try
            {
                var response = _IRequestDetailRepository.getRFPDetail(rfPID,userId,type,lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch(WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch(Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }


        /// <summary>
        /// Get RFPMiceRequest with Proposal
        /// </summary>
        /// <param name="rfPID"></param>
        /// <returns></returns>
        public ApiResult  GetRFPMiceRequestWithProposal(int page, int pageSize, int? userId, string sortby, string search)
        {
            try
            {
                var response = _IRequestDetailRepository.GetRFPMiceRequestWithProposal(page,pageSize,userId,sortby,search);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

    }
}
