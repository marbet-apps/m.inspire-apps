﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class List_Airport
    {
        [Key]
        public int Id { get; set; }
        public string Airport_name { get; set; }
        public int? country_id { get; set; }
        [MaxLength(250)]
        public string state { get; set; }
        [MaxLength(250)]
        public string city { get; set; }
        [MaxLength(250)]
        public string zipcode { get; set; }
        public double? airport_lan { get; set; }
        public double? airport_lat { get; set; }

        public ICollection<Hotels> Hotels { get; set; }
        
    }
}
