﻿using System;

namespace DCP.Models.RFP
{
    public class RequestCartResourceModel
    {
        public int Id { get; set; }

        public int RequestCartId { get; set; }

        public int ResourceId { get; set; }

        public string ResourceType { get; set; }

        public bool IsAddedToRFP { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}
