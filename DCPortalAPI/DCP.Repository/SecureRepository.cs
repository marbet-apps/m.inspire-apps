﻿using DCP.Entities;
using DCP.Models;
using DCP.Repository.Interface;
using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DCP.Repository
{
    public class SecureRepository : ISecureRepository
    {
        private DCPDbContext _context = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public SecureRepository()
        {
            this._context = new DCPDbContext();
        }

        /// <summary>
        /// validating user's credentials.
        /// </summary>
        /// <returns></returns>
        public ResponseUserLoginModel CheckUserAuth(string Email, string Password)
        {
            ResponseUserLoginModel userLoginModel = new ResponseUserLoginModel();
            var dbCredentialEntity = _context.User_Credential.Where(x => x.Email == Email && x.Password == Password).FirstOrDefault();
            if (dbCredentialEntity != null)
            {
                var temp = _context.User_Masters.Where(Y => Y.User_Id == dbCredentialEntity.User_Master_Id).ToList().FirstOrDefault();
                var accessrights = _context.User_Accessess.Where(x => x.User_Id == temp.User_Id).FirstOrDefault();

                UpdateLastAccessDate(temp.User_Id);
                userLoginModel = new ResponseUserLoginModel()
                {
                    First_Name = temp.First_Name,
                    Last_Name = temp.Last_Name,
                    Email = dbCredentialEntity.Email,
                    IsActive = temp.IsActive,
                    IsDelete = temp.IsDeleted,
                    User_Type = temp.Subscription_Type_Id,
                    LoggedInUserID = temp.User_Id,
                    Last_AccessDate = temp.Last_AccessDate.Value.ToString("dd.MM.yyyy"),
                    IsHotel = accessrights.Hotel ?? false,
                    IsRestaurant = accessrights.Restaurant ?? false,
                    IsLocation = accessrights.Location ?? false,
                    IsManageUser = temp.isAdmin ?? false,
                    Photo=temp.photo  //Start User photo changes  15 april

                };
            }
            return userLoginModel;
        }
        /// <summary>
        /// validate administrator credentials.
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="Password"></param>
        /// <param name="UserType"></param>s
        /// <returns></returns>
        public ResponseUserLoginModel CheckAdminUserAuth(string Email, string Password, int UserType)
        {
            var dbEntity = _context.Admin_Master.Where(x => x.Email == Email && x.Password == Password &&
                                                         x.User_Type == UserType).ToList().FirstOrDefault();
            ResponseUserLoginModel userLoginModel = null;
            if (dbEntity != null && dbEntity.Email != null && !string.IsNullOrEmpty(dbEntity.Email) && !string.IsNullOrWhiteSpace(dbEntity.Email))
            {
               // userLoginModel = new ResponseUserLoginModel();
                userLoginModel = new ResponseUserLoginModel()
                {
                    First_Name = dbEntity.First_Name,
                    Last_Name = dbEntity.Last_Name,
                    Email = dbEntity.Email,
                    IsActive = dbEntity.IsActive,
                    User_Type = dbEntity.User_Type,
                    LoggedInUserID = dbEntity.User_Id,
                    Last_AccessDate = dbEntity.Modify_Date.Value.ToString("dd.MM.yyyy")
                };
                UpdateModifiedDate(dbEntity.User_Id);
            }
            return userLoginModel;
        }
        /// <summary>
        /// creating/register user.
        /// </summary>
        /// <param name="userRegisterModel"></param>
        /// <returns></returns>
        public RegisterationModel RegisterUser(UserRegisterModel userRegisterModel)
        {

           // var dbUserSubscription = _context.List_SubscriptionType.Where(Y => Y.Type_Name == userRegisterModel.Subscription_Type_Name).Select(x => x.Id).FirstOrDefault();
            RegisterationModel objRegisterModel = new RegisterationModel();
            var dbentityUser = _context.User_Credential.Where(z => z.Email == userRegisterModel.Email).FirstOrDefault();
            if (dbentityUser != null && string.IsNullOrEmpty(dbentityUser.Email) == false)
            {
                return objRegisterModel;
            }
            var dbUserCommunication = (new User_Communication
            {
                Country_Id = userRegisterModel.Country_Id,
                Place = userRegisterModel.Place,
                PostCode = userRegisterModel.PostCode,
                Street = userRegisterModel.Street,
                Telephone = userRegisterModel.Telephone,
                Tax_Indentifier_No = userRegisterModel.Tax_Indentifier_No,
                created_by = userRegisterModel.loggedInUserId,
                created_date = DateTime.Now
            });
            var dbUserCredentialEntity = (new User_Credential
            {
                Email = userRegisterModel.Email,
                //Password = Crypto.EncryptSHA512Managed(userRegisterModel.Password),
                created_by = userRegisterModel.loggedInUserId,
                created_date = DateTime.Now,
            });
            var dbUserEntity = (new User_Master
            {
                First_Name = userRegisterModel.First_Name,
                Last_Name = userRegisterModel.Last_Name,
                Company_Name = userRegisterModel.Company_Name,
                Department_Name = userRegisterModel.Department_Name,
                IsApproved = userRegisterModel.IsApproved,
                IsActive = false, //userRegisterModel.IsActive,
                IsDeleted = userRegisterModel.IsDeleted,
                Salutation = userRegisterModel.Salutation,
                Isself_Created = userRegisterModel.Isself_Created,
                created_by = userRegisterModel.loggedInUserId,
                created_date = DateTime.Now,
                Subscription_Type_Id = userRegisterModel.Subscription_Type_Name,
                isAdmin = userRegisterModel.isAdmin
            });

            User_Access dbUserAccessEntity = new User_Access();

            if (userRegisterModel.IsRegisterFromManageUser==true)
            {
                var user = _context.User_Accessess.Where(t => t.User_Id == userRegisterModel.loggedInUserId).FirstOrDefault();
                if(user!=null)
                {
                    dbUserAccessEntity = (new User_Access
                    {
                        Hotel = user.Hotel,
                        Location = user.Location,
                        Restaurant = user.Restaurant,
                        GoogleMap = user.GoogleMap,
                        UserMgmt = user.GoogleMap,
                        User_Id = user.User_Id,
                        RFPTemplate = user.RFPTemplate,
                        NearBy = user.NearBy,
                        Created_By = userRegisterModel.loggedInUserId,
                        Created_Date = DateTime.Now
                    });                   
                }
            }
            else
            {
                 dbUserAccessEntity = (new User_Access
                {
                    Hotel = userRegisterModel.Hotel,
                    Location = userRegisterModel.Location,
                    Restaurant = userRegisterModel.Restaurant,
                    GoogleMap = userRegisterModel.GoogleMap,
                    UserMgmt = userRegisterModel.GoogleMap,
                    User_Id = dbUserEntity.User_Id,
                    RFPTemplate = userRegisterModel.RFPTemplate,
                    NearBy = userRegisterModel.NearBy,
                    Created_By = userRegisterModel.loggedInUserId,
                    Created_Date = DateTime.Now
                });
            }

           
            _context.User_Communication.Add(dbUserCommunication);
            _context.User_Credential.Add(dbUserCredentialEntity);
            _context.User_Masters.Add(dbUserEntity);
            dbUserAccessEntity.User_Id = dbUserEntity.User_Id;
            _context.User_Accessess.Add(dbUserAccessEntity);
            int savedata = _context.SaveChanges();

            if (savedata > 0)
            {
                objRegisterModel.Id = dbUserEntity.User_Id;
                objRegisterModel.First_Name = userRegisterModel.First_Name;
                objRegisterModel.Last_Name = userRegisterModel.Last_Name;
                objRegisterModel.Created_date = DateTime.Now;
                objRegisterModel.Email = userRegisterModel.Email;
                objRegisterModel.IsActive = userRegisterModel.IsActive;
                objRegisterModel.Isself_Created = userRegisterModel.Isself_Created;
            }

            return objRegisterModel;
        }
        /// <summary>
        /// Generate user password.
        /// </summary>
        /// <param name="userPassword"></param>
        /// <returns></returns>
        public UserPasswordModel ForgotUserPassword(UserPasswordModel userPassword, out string name, string token)
        {          
            name = "";
            UserPasswordModel passwordModel = new UserPasswordModel();
            var dbEntity = _context.User_Credential.Where(x => x.Email == userPassword.Email).FirstOrDefault();
            if (dbEntity != null && string.IsNullOrEmpty(dbEntity.Email) != true)
            {
                //dbEntity.Password = Crypto.EncryptSHA512Managed(userPassword.Password);
                //_context.SaveChanges();
                passwordModel = new UserPasswordModel()
                {
                    Email = dbEntity.Email,
                    Id = dbEntity.User_Master_Id,                    
                  //  Password = dbEntity.Password,
                };
                var userMst = _context.User_Masters.Where(t => t.User_Id == dbEntity.User_Master_Id).FirstOrDefault();
                if (userMst != null)
                {
                    userMst.Token = token;
                    _context.SaveChanges();

                    name = dbEntity.user_Masters.First_Name + " " + dbEntity.user_Masters.Last_Name;
                }

            }
            return passwordModel;
        }
        /// <summary>
        /// Retrive all users/Retrive details of the specific user.
        /// </summary>
        /// <param name="activestatus"></param>
        /// <param name="email"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public ResponseUserModel getAllUser(string activestatus = "All", string email = "", int? UserId = 0, int page = 1, int pageSize = 50, string lang="")
        {
            List<UserRegisterModel> userRegisters = new List<UserRegisterModel>();
            ResponseUserModel responseUserModel = new ResponseUserModel();
            var dbEntity = (
                                from UM in _context.User_Masters.
                                Where(
                                      Q => Q.IsActive == (
                                                            activestatus.ToUpper() == "ACTIVE" ? true :
                                                            activestatus.ToUpper() == "INACTIVE" ? false : Q.IsActive
                                                          )
                                       && Q.User_Id == (
                                                            UserId == 0 ? Q.User_Id : UserId
                                                          )
                                       && Q.IsDeleted == false
                                      )
                                from UC in _context.User_Credential.Where
                                                                    (
                                                                        x => x.User_Master_Id == UM.User_Id
                                                                    ).ToList()
                                from UCC in _context.User_Communication.Where
                                                                    (
                                                                        y => y.User_Master_Id == UM.User_Id
                                                                    ).ToList()
                                from sb in _context.List_SubscriptionType.Where
                                                                    (
                                                                        z => z.Id == UM.Subscription_Type_Id
                                                                    ).ToList()
                                join UA in _context.User_Accessess on UM.User_Id equals UA.User_Id
                                select new UserRegisterModel()
                                {
                                    Company_Name = UM.Company_Name,
                                    Department_Name = UM.Department_Name,
                                    First_Name = UM.First_Name,
                                    Last_Name = UM.Last_Name,
                                    Country_Id = UCC.Country_Id,
                                    Email = UC.Email,
                                    IsActive = UM.IsActive,
                                    IsApproved = UM.IsApproved,
                                    IsDeleted = UM.IsDeleted,
                                    Isself_Created = UM.Isself_Created,
                                    Place = UCC.Place,
                                    PostCode = UCC.PostCode,
                                    Salutation = UM.Salutation,
                                    Street = UCC.Street,
                                    Subscription_Type_Name = sb.Id,
                                    Tax_Indentifier_No = UCC.Tax_Indentifier_No,
                                    Telephone = UCC.Telephone,
                                    Userid = UM.User_Id,
                                    GoogleMap = UA.GoogleMap,
                                    Hotel = UA.Hotel,
                                    Location = UA.Location,
                                    NearBy = UA.NearBy,
                                    Restaurant = UA.Restaurant,
                                    RFPTemplate = UA.RFPTemplate,
                                    UserMgmt = UA.UserMgmt,
                                    CountryName = (UserId != null && UserId > 0) ? (lang== "de-DE" ? _context.List_Country.Where(c => c.Id == UCC.Country_Id).FirstOrDefault().country_name_de : _context.List_Country.Where(c => c.Id == UCC.Country_Id).FirstOrDefault().country_name)  : "",
                                    isAdmin = UM.isAdmin,
                                    Photo =  UM.photo,
                                    
                                }
                                );
            //userRegisters = dbEntity;


            


            if(!string.IsNullOrEmpty(email) && !string.IsNullOrWhiteSpace(email))
            {
                dbEntity = dbEntity.Where(x => x.Email.Contains(email) || x.Company_Name.Contains(email) || x.Department_Name.Contains(email));
            }

           
            responseUserModel.users = dbEntity.OrderBy(x => x.Company_Name).Skip(Skipcount(page, pageSize)).Take(pageSize).ToList();
            if (UserId != null && UserId > 0)
            {
                UserRegisterModel user = responseUserModel.users.FirstOrDefault();
                responseUserModel.users.FirstOrDefault().strPhoto = user.Photo != null ? Convert.ToBase64String(user.Photo) : "";


            }
            responseUserModel.totalRecord = dbEntity.Count();

            return responseUserModel;
        }
        /// <summary>
        /// Remove the user.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public UserModel removeUser(int userID)
        {
            UserModel userModel = new UserModel();
            var dbEntity = _context.User_Masters.Where(x => x.User_Id == userID && x.IsDeleted == false).FirstOrDefault();
            if (dbEntity != null)
            {
                dbEntity.IsDeleted = true;
                dbEntity.modify_date = DateTime.Now;
                _context.SaveChanges();
                userModel = new UserModel()
                {
                    First_Name = dbEntity.First_Name,
                    Last_Name = dbEntity.Last_Name,
                    IsActive = dbEntity.IsActive,
                    Last_AccessDate = dbEntity.Last_AccessDate
                };
            }
            return userModel;
        }
        /// <summary>
        /// Change user password.
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public int ChangePassword(ChangePasswordModel userModel)
        {
            int isSuccess = 0;
            var TdbEntity = (from UC in _context.User_Credential.Where(y => y.Email == userModel.Email)
                             join UM in _context.User_Masters.Where(x => x.IsActive == true && x.IsDeleted == false)
                             on UC.User_Master_Id equals UM.User_Id
                             select new
                             {
                                 UC,
                                 UM
                             }).FirstOrDefault();

            if (TdbEntity != null && TdbEntity.UC != null && TdbEntity.UC.Password != null)
            {
                if (TdbEntity.UC.Password != Crypto.EncryptSHA512Managed(userModel.OldPassword))
                    return isSuccess = 2;
                TdbEntity.UC.Password = Crypto.EncryptSHA512Managed(userModel.NewPassword);
                TdbEntity.UC.modify_date = DateTime.Now;
                TdbEntity.UM.modify_date = DateTime.Now;
                _context.SaveChanges();
                return isSuccess = 1;
            }
            return isSuccess;
        }
        /// <summary>
        /// Update user details.
        /// </summary>
        /// <param name="updateUserModel"></param>
        /// <returns></returns>
        public ResponseModel UpdateUser(int userID, int loggedInUserID, UpdateUserModel updateUserModel)
        {
            //var dbUserSubscription = 0;
            //if (string.IsNullOrEmpty(updateUserModel.Subscription_Type_Name) == false && string.IsNullOrWhiteSpace(updateUserModel.Subscription_Type_Name) == false)
            //    dbUserSubscription = _context.List_SubscriptionType.Where(Y => Y.Type_Name == updateUserModel.Subscription_Type_Name).Select(x => x.Id).FirstOrDefault();
            bool isSuccess = false;
            int logedInUserId = 0;
            ResponseModel responseModel = new ResponseModel();
            var TdbEntity = (from UC in _context.User_Credential.Where(y => y.Email == updateUserModel.Email)
                             join UM in _context.User_Masters.Where(x =>  x.IsDeleted == false && x.User_Id == userID) //x.IsActive == true &&
                             on UC.User_Master_Id equals UM.User_Id
                             join UCC in _context.User_Communication
                             on UM.User_Id equals UCC.User_Master_Id
                             join UA in _context.User_Accessess on UM.User_Id equals UA.User_Id
                             select new
                             {
                                 UC,
                                 UM,
                                 UCC,
                                 UA
                             }).FirstOrDefault();
            if (TdbEntity != null && TdbEntity.UC != null && string.IsNullOrEmpty(TdbEntity.UC.Email) == false)
            {
                TdbEntity.UM.Company_Name = (string.IsNullOrEmpty(updateUserModel.Company_Name) == false ?
                                                updateUserModel.Company_Name :
                                                TdbEntity.UM.Company_Name);
                TdbEntity.UM.Department_Name = (string.IsNullOrEmpty(updateUserModel.Department_Name) == false ?
                                             updateUserModel.Department_Name :
                                             TdbEntity.UM.Department_Name);
                TdbEntity.UM.First_Name = (string.IsNullOrEmpty(updateUserModel.First_Name) == false ?
                                             updateUserModel.First_Name :
                                             TdbEntity.UM.First_Name);
                TdbEntity.UM.Last_Name = (string.IsNullOrEmpty(updateUserModel.Last_Name) == false ?
                                             updateUserModel.Last_Name :
                                             TdbEntity.UM.Last_Name);
                TdbEntity.UM.Salutation = (string.IsNullOrEmpty(updateUserModel.Salutation) == false ?
                                             updateUserModel.Salutation :
                                             TdbEntity.UM.Salutation);
                TdbEntity.UM.Subscription_Type_Id = Convert.ToInt32(updateUserModel.Subscription_Type_Name); //dbUserSubscription > 0 ? dbUserSubscription : TdbEntity.UM.Subscription_Type_Id;
                TdbEntity.UM.IsActive = (updateUserModel.IsActive == null ? TdbEntity.UM.IsActive : (bool)updateUserModel.IsActive);
                TdbEntity.UM.IsDeleted = (updateUserModel.IsDeleted == null ? TdbEntity.UM.IsDeleted : (bool)updateUserModel.IsDeleted);
                TdbEntity.UCC.Street = (string.IsNullOrEmpty(updateUserModel.Street) == false ?
                                       updateUserModel.Street : TdbEntity.UCC.Street);
                TdbEntity.UCC.Place = (string.IsNullOrEmpty(updateUserModel.Place) == false ?
                                       updateUserModel.Place : TdbEntity.UCC.Place);
                TdbEntity.UCC.PostCode = (string.IsNullOrEmpty(updateUserModel.PostCode) == false ?
                                         updateUserModel.PostCode : TdbEntity.UCC.PostCode);
                TdbEntity.UCC.Tax_Indentifier_No = (string.IsNullOrEmpty(updateUserModel.Tax_Indentifier_No) == false
                                                    ? updateUserModel.Tax_Indentifier_No : TdbEntity.UCC.Tax_Indentifier_No);
                TdbEntity.UCC.Telephone = (string.IsNullOrEmpty(updateUserModel.Telephone) == false ?
                                         updateUserModel.Telephone : TdbEntity.UCC.Telephone);
                TdbEntity.UCC.Country_Id = (updateUserModel.Country_Id == null ?
                                            TdbEntity.UCC.Country_Id : (int)updateUserModel.Country_Id);
                TdbEntity.UM.modified_by = loggedInUserID;
                TdbEntity.UM.modify_date = Convert.ToDateTime(DateTime.Now);

                TdbEntity.UC.modified_by = updateUserModel.LogedInUserId;
                TdbEntity.UC.modify_date = Convert.ToDateTime(DateTime.Now);

                TdbEntity.UCC.modified_by = loggedInUserID;
                TdbEntity.UCC.modify_date = Convert.ToDateTime(DateTime.Now);

                TdbEntity.UA.UserMgmt = updateUserModel.UserMgmt;
                TdbEntity.UA.RFPTemplate = updateUserModel.RFPTemplate;
                TdbEntity.UA.Restaurant = updateUserModel.Restaurant;
                TdbEntity.UA.Hotel = updateUserModel.Hotel;
                TdbEntity.UA.Location = updateUserModel.Location;
                TdbEntity.UA.GoogleMap = updateUserModel.GoogleMap;
                TdbEntity.UA.NearBy = updateUserModel.NearBy;
                //TdbEntity.UM.isAdmin = updateUserModel.isAdmin;
                _context.SaveChanges();
                isSuccess = true;
                responseModel.UserModel = updateUserModel;

                List<User_Master> userList =  _context.User_Masters.Where(t => t.created_by == userID).ToList();
                if(userList!=null && userList.Count()>0)
                {
                    foreach (var item in userList)
                    {
                       User_Access user_Access = _context.User_Accessess.Where(t => t.User_Id == item.User_Id).FirstOrDefault();
                       if(user_Access!=null)
                       {
                            user_Access.Hotel = updateUserModel.Hotel;
                            user_Access.Restaurant = updateUserModel.Restaurant;
                            user_Access.Location = updateUserModel.Location;
                            user_Access.GoogleMap = updateUserModel.GoogleMap;
                            user_Access.UserMgmt = updateUserModel.UserMgmt;
                            user_Access.RFPTemplate = updateUserModel.RFPTemplate;
                            _context.Entry(user_Access).State = System.Data.Entity.EntityState.Modified;
                            _context.SaveChanges();
                       }
                    }
                }
            }
            logedInUserId = (updateUserModel.LogedInUserId == null ? 0 : (int)updateUserModel.LogedInUserId);
            responseModel.isSuccess = isSuccess;
            responseModel.LogedInUserId = logedInUserId;
            return responseModel;
        }

        /// <summary>
        /// Update user profile record into db
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public UpdateProfileResponseModel UpdateUserProfile(UpdateProfileModel model)
        {
            UpdateProfileResponseModel responseModel = new UpdateProfileResponseModel();

            var dbUserEntity = (from um in _context.User_Masters
                                join uc in _context.User_Credential on um.User_Id equals uc.User_Master_Id
                                where um.User_Id == model.LogedInUserId && uc.Email == model.Email select new { um }).FirstOrDefault();
            if (dbUserEntity != null)
            {
                dbUserEntity.um.First_Name = model.First_Name;
                dbUserEntity.um.Last_Name = model.Last_Name;
                dbUserEntity.um.Salutation = model.Salutation;
                dbUserEntity.um.modified_by = model.LogedInUserId;
                dbUserEntity.um.modify_date = DateTime.Now;
                _context.SaveChanges();
            }

           
            var dbUserCommunication = (from um in _context.User_Communication
                     join uc in _context.User_Credential on um.User_Master_Id equals uc.User_Master_Id
                     where um.User_Master_Id == model.LogedInUserId && uc.Email == model.Email
                     select new { um }).FirstOrDefault();
            if (dbUserCommunication != null)
            {
                dbUserCommunication.um.Street = model.Street;
                dbUserCommunication.um.Place = model.Place;
                dbUserCommunication.um.PostCode = model.PostCode;
                dbUserCommunication.um.Tax_Indentifier_No = model.Tax_Indentifier_No;
                dbUserCommunication.um.Telephone = model.Telephone;
                dbUserCommunication.um.Country_Id = model.Country_Id;
                dbUserCommunication.um.modified_by = model.LogedInUserId;
                dbUserCommunication.um.modify_date = DateTime.Now;
                _context.SaveChanges();
            }

            responseModel.IsSuccess = true;
            responseModel.LogedInUserId = model.LogedInUserId;
            responseModel.UpdateProfileModel = model;
            return responseModel;

        }

        /// <summary>
        /// Update user's last access date
        /// </summary>
        /// <param name="userID"></param>
        private void UpdateLastAccessDate(int userID)
        {
            var dbUserEntity = _context.User_Masters.Where(x => x.User_Id == userID).FirstOrDefault();
            if (dbUserEntity != null && dbUserEntity.User_Id > 0)
            {
                dbUserEntity.Last_AccessDate = DateTime.Now;
                _context.SaveChanges();
            }
        }

        /// <summary>
        /// Update user's modified date
        /// </summary>
        /// <param name="userID"></param>
        private void UpdateModifiedDate(int userID)
        {
            var dbUserEntity = _context.Admin_Master.Where(x => x.User_Id == userID).FirstOrDefault();
            if (dbUserEntity != null && dbUserEntity.User_Id > 0)
            {
                dbUserEntity.Modify_Date = DateTime.Now;
                _context.SaveChanges();
            }
        }
        /// <summary>
        /// Skip count
        /// </summary>
        /// <returns></returns>
        private int Skipcount(int page, int pageSize)
        {
            int skipCount = page == 1 ? 0 : (pageSize * (page - 1));

            return skipCount;
        }

       public bool UploadImage(byte[] bytes, int userId)
       {
            try
            {
               User_Master user = _context.User_Masters.Where(t => t.User_Id == userId).FirstOrDefault();
               if(user!=null)
                {
                    user.photo = bytes;
                    _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {

                return false;
            }
       }

        public bool DeleteImage(int userId)
        {
            try
            {
                User_Master user = _context.User_Masters.Where(t => t.User_Id == userId).FirstOrDefault();
                if (user != null)
                {
                    user.photo = null;
                    _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public UserModel ResetPassword(ResetPasswordModel userModel, bool isValidateUserForActive)
        {
            //int isSuccess = 0;

            UserModel model = new UserModel();

            if (isValidateUserForActive == false)
            {
                var TdbEntity = (from UC in _context.User_Credential.Where(y => y.User_Master_Id == userModel.Id)
                                 join UM in _context.User_Masters.Where(x => x.IsActive == true && x.IsDeleted == false)
                                 on UC.User_Master_Id equals UM.User_Id
                                 select new
                                 {
                                     UC,
                                     UM
                                 }).FirstOrDefault();

                if (TdbEntity != null && TdbEntity.UC != null)
                {
                    TdbEntity.UC.Password = Crypto.EncryptSHA512Managed(userModel.NewPassword);
                    TdbEntity.UC.modify_date = DateTime.Now;
                    TdbEntity.UM.modify_date = DateTime.Now;
                    _context.SaveChanges();


                    User_Master user = _context.User_Masters.Where(t => t.User_Id == userModel.Id).FirstOrDefault();
                    user.Token = null;

                    _context.SaveChanges();

                    model.First_Name = TdbEntity.UM.First_Name;
                    model.Last_Name = TdbEntity.UM.Last_Name;
                    model.Email = TdbEntity.UC.Email;
                    return model;
                }
                
            }
            else
            {
                var TdbEntity = (from UC in _context.User_Credential.Where(y => y.User_Master_Id == userModel.Id)
                                 join UM in _context.User_Masters.Where(x => x.IsDeleted == false)
                                 on UC.User_Master_Id equals UM.User_Id
                                 select new
                                 {
                                     UC,
                                     UM
                                 }).FirstOrDefault();

                if (TdbEntity != null && TdbEntity.UM != null)
                {
                    TdbEntity.UM.IsActive = true;                  
                    TdbEntity.UM.modify_date = DateTime.Now;
                    TdbEntity.UM.Token = string.Empty;
                    _context.SaveChanges();                   
                }

                if (TdbEntity != null && TdbEntity.UC != null)
                {
                    TdbEntity.UC.Password = Crypto.EncryptSHA512Managed(userModel.NewPassword);
                    TdbEntity.UC.modify_date = DateTime.Now;
                    TdbEntity.UM.modify_date = DateTime.Now;
                    _context.SaveChanges();
                   // return isSuccess = 1;

                    model.First_Name = TdbEntity.UM.First_Name;
                    model.Last_Name = TdbEntity.UM.Last_Name;
                    model.Email = TdbEntity.UC.Email;
                    return model;
                }
            }

               

            return model;
        }

        public UserModel ValidateUser(ResetPasswordModel userModel, bool isValidateUserForActive)
        {
            UserModel model = new UserModel();
            

            if(isValidateUserForActive == false)
            {
                var TdbEntity = (from UM in _context.User_Masters.Where(x => x.User_Id == userModel.Id && x.IsActive==true && x.IsDeleted == false)
                                 select new
                                 {                                    
                                     UM
                                 }).FirstOrDefault();

                if (TdbEntity != null && TdbEntity.UM != null)
                {
                    model.First_Name = TdbEntity.UM.First_Name;
                    model.Last_Name = TdbEntity.UM.Last_Name;
                    model.Token = TdbEntity.UM.Token;
                }
            }
            else if (isValidateUserForActive == true)
            {
                var TdbEntity = (from UM in _context.User_Masters.Where(x => x.User_Id == userModel.Id && x.IsDeleted == false)
                                 select new
                                 {
                                     UM
                                 }).FirstOrDefault();

                if (TdbEntity != null && TdbEntity.UM != null)
                {
                    model.First_Name = TdbEntity.UM.First_Name;
                    model.Last_Name = TdbEntity.UM.Last_Name;
                    model.Token = TdbEntity.UM.Token;
                }
            }

          
            return model;
        }



    }
}
