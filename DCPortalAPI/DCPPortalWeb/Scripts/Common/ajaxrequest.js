﻿var ajaxRequest = {
    variables: {
        authorizationTokenKey: "AuthorizationToken",
        CurrentUser: "CurrentUser",
        CurrentUserPhoto: "CurrentUserPhoto"
    },
    httpPost: function (url, data, callbackSuccess, callbackFailure) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (response) {
                if (callbackSuccess && typeof (callbackSuccess) === "function")
                    callbackSuccess(response);
            },
            error: function (req, status, error) {
                if (callbackFailure && typeof (callbackFailure) === "function")
                    callbackFailure(req, status, error);
            }
        });
    },
    ///INSPIRE-102
    ///When user click on startrfp and directly add any service, 1st call rfp save action and then call service save action method. so rfp save action method call through this httpPostSync
    httpPostSync: function (url, data, callbackSuccess, callbackFailure) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: false,
            cache: false,
            success: function (response) {
                if (callbackSuccess && typeof (callbackSuccess) === "function")
                    callbackSuccess(response);
            },
            error: function (req, status, error) {
                if (callbackFailure && typeof (callbackFailure) === "function")
                    callbackFailure(req, status, error);
            }
        });
    },
    httpGetSyncWithData: function (url, data, callbackSuccess, callbackFailure) {
        $.ajax({
            type: "GET",
            url: url,
            data: data,
            contentType: "text/html",
            async: false,
            cache: false,
            success: function (response) {
                if (callbackSuccess && typeof (callbackSuccess) === "function")
                    callbackSuccess(response);
            },
            error: function (req, status, error) {
                if (callbackFailure && typeof (callbackFailure) === "function" && common.ajaxHandleUnauthorizeAccess(req))
                    callbackFailure(req, status, error);
            }
        });
    },
    getQuerystring: function (key) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] === key) {
                return pair[1];
            }
        }
    },
    setAuthorizationToken: function (token) {
        window.localStorage.setItem(ajaxRequest.variables.authorizationTokenKey, token);
    },
    getAuthorizationToken: function () {
        return window.localStorage.getItem(ajaxRequest.variables.authorizationTokenKey);
    },
    setCurrentUser: function (key) {
        window.localStorage.setItem(ajaxRequest.variables.CurrentUser, key);
    },

     //Start User photo changes  15 april
    setCurrentUserPhoto: function (key) {
        window.localStorage.setItem(ajaxRequest.variables.CurrentUserPhoto, key);
    },
    SetUserProfilePhotoIntoImage: function () {
        if (window.localStorage.getItem(ajaxRequest.variables.CurrentUserPhoto) != "null" && window.localStorage.getItem(ajaxRequest.variables.CurrentUserPhoto).length > 0) {
            $($c.controls.imgCurrentUser).attr("src", 'data:image/png;base64,' + window.localStorage.getItem(ajaxRequest.variables.CurrentUserPhoto));
            // $($c.controls.imgCurrentUser).attr("alt", user.First_Name + ' ' + user.Last_Name);
        }
    },
    getCurrentUser: function () {
        return window.localStorage.getItem(ajaxRequest.variables.CurrentUser);
    },

   
    removelocalstorage: function () {
        window.localStorage.removeItem(ajaxRequest.variables.authorizationTokenKey);
        window.localStorage.removeItem(ajaxRequest.variables.CurrentUser);
    },
    CheckRights: function (type) {

        var user = JSON.parse(ajaxRequest.getCurrentUser());

        if (type == "H") {
            return user.IsHotel;
        }

        if (type == "R") {
            return user.IsRestaurant;
        }

        if (type == "L") {
            return user.IsLocation;
        }
    }
};

/// Set the authorization token for all the request.
$.ajaxSetup({
    beforeSend: function (xhr) {
        
        var authorizationToken = ajaxRequest.getAuthorizationToken();
        var lang = $.cookie("Language");
        var user = JSON.parse(ajaxRequest.getCurrentUser());
        if (user != null && user != undefined &&  user.LoggedInUserID != undefined && user.LoggedInUserID != null)
            xhr.setRequestHeader("UserId", user.LoggedInUserID);

        if (lang === undefined || lang === null || lang === "") {
            lang = "en-US";
        }
        if (authorizationToken !== null && authorizationToken !== undefined && authorizationToken.length > 0) {
            // console.log('Bearer ' + authorizationToken.replace('"', '').replace('"', '') + '');
            xhr.setRequestHeader("Authorization", "Bearer " + authorizationToken.replace('"', '').replace('"', '') + "");
            xhr.setRequestHeader("Lang", lang);
            xhr.setRequestHeader("UserId", JSON.parse(ajaxRequest.getCurrentUser()).LoggedInUserID);
        }
    },
    error: function (jqXHR, textStatus, errorThrown) {
        
        $managecode.Decision(false, "", jqXHR.status, textStatus + ": " + errorThrown, "error", false);
    }
});