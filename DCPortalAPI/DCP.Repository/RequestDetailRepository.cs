﻿using DCP.Entities;
using DCP.Mice.Models;
using DCP.MiceAPI;
using DCP.Models.RFP;
using DCP.Repository.Interface;
using DCP.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DCP.Repository
{
    public class RequestDetailRepository : IRequestDetailRepository
    {
        private DCPDbContext _context = null;
        public RequestDetailRepository()
        {
            _context = new DCPDbContext();
        }
        /// <summary>
        /// retrive list of rfp
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="term"></param>
        /// <param name="status"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public ResponseRfpModel getRfpList(int page, int pageSize, string status, string lang, string term = "", int? userId = 0)
        {
            ResponseRfpModel responseRfp = new ResponseRfpModel();
            bool isStatus = false;

            if (!string.IsNullOrEmpty(status) && !string.IsNullOrWhiteSpace(status) && status.ToUpper() != "ALL")
                isStatus = true;
            var dbEntity = (from rf in _context.RequestForProposals
                            join UM in _context.User_Masters
                            on rf.user_id equals UM.User_Id
                            join rt in _context.RequestTypes on
                            rf.rfp_type equals rt.mice_request_type
                            join rs in _context.RequestStatuses on
                            rf.status equals rs.mice_status_type
                            where UM.User_Id == (userId > 0 ? userId : UM.User_Id) &&
                            rf.status == (isStatus ? status : rf.status) &&
                            rf.user_id == (userId > 0 ? userId : rf.user_id)
                            select new
                            {
                                rf,
                                UM,
                                rt,
                                rs
                            }).AsEnumerable();

            List<RFPModel> rfpModel;

            rfpModel = dbEntity.Select(x => new RFPModel()
            {
                Company = x.UM.Company_Name,
                Days_running = x.rf.days_running,
                RequestId = x.rf.Id,
                Request_date = x.rf.created_date.ToString("dd.MM.yyyy"),
                Event_name = x.rf.event_name,
                Rfp_status = (lang == "en-US" ? x.rs.request_status : x.rs.request_status_de),
                Rfp_Type = (lang == "en-US" ? x.rt.request_type : x.rt.request_type_de),
                Mice_status_type = x.rf.status,
                Name = x.UM.First_Name + " " + x.UM.Last_Name
            }).OrderBy(x => x.RequestId).Skip(Skipcount(page, pageSize)).Take(pageSize).ToList();

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
                rfpModel = rfpModel.Where(
                                x => x.Name.ToUpper().Contains(term.ToUpper())
                                || x.Company.ToUpper().Contains(term.ToUpper()) ||
                                x.Rfp_Type.ToUpper().Contains(term.ToUpper())
                            ).ToList();
            responseRfp.RFPModels = rfpModel;
            responseRfp.TotalCount = dbEntity.Count();

            return responseRfp;
        }
        /// <summary>
        /// Retrive RFP Details
        /// </summary>
        /// <param name="RfpId"></param>
        /// <returns></returns>
        public RequestForProposalModel getRFPDetail(int RfpId, int? userId = 0, string type = "", string lang = "")
        {
            int resource_type = 0;

            if (type == "H")
            {
                resource_type = 0;
            }
            else if (type == "R")
            {
                resource_type = 1;
            }
            else if (type == "L")
            {
                resource_type = 2;
            }

            RequestForProposalModel proposalModel = new RequestForProposalModel();
            var rfpDbEntity = _context.RequestForProposals.Where(x => x.Id == RfpId && x.user_id == (userId > 0 ? userId : x.user_id))
                             .FirstOrDefault();
            if (rfpDbEntity != null && rfpDbEntity.Id > 0)
            {
                var rfpmDbEntity = _context.RFPMiceRequests.DefaultIfEmpty().Where(x => x.rfp_id == rfpDbEntity.Id && x.resource_type == resource_type).ToList();
                if (rfpmDbEntity != null && rfpmDbEntity.Count > 0)
                {
                    foreach (var item in rfpmDbEntity)
                    {
                        if (item.mice_request_id.HasValue)
                        {

                            try
                            {
                                Dictionary<string, object> keyValues = new Dictionary<string, object>();
                                keyValues["RFPId"] = RfpId;
                                keyValues["MiceRequestId"] = item.mice_request_id;
                                keyValues["ResourceType"] = resource_type;
                                keyValues["UserId"] = userId;

                                DCPMiceRequests miceRequests = SetDCPMiceRequestModel(RfpId, item.mice_request_id, JsonConvert.SerializeObject(keyValues), item.resource_type, userId, (int)ManageEnum.requestType.rfpdetailRequest);
                                RequestRepository requestRepository = new RequestRepository();
                                requestRepository.AddDCPMiceRequest(miceRequests);
                            }
                            catch (Exception ex)
                            {
                                WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
                            }

                            var miceAPI = new MiceRequestAPI(ManageSetting.MiceAgencyName, ManageSetting.MiceAgencyPassword);
                            var apiResponse = miceAPI.GetRequestDetail(item.mice_request_id.Value);
                            item.status = apiResponse.Status;

                            try
                            {
                                DCPMiceRequests miceRequests = SetDCPMiceRequestModel(RfpId, item.mice_request_id, JsonConvert.SerializeObject(apiResponse), item.resource_type, userId, (int)ManageEnum.requestType.rfpdetailResponse);
                                RequestRepository requestRepository = new RequestRepository();
                                requestRepository.AddDCPMiceRequest(miceRequests);
                            }
                            catch (Exception ex)
                            {
                                WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
                            }

                            if (apiResponse != null && apiResponse.RequestId > 0)
                            {
                                var DBrfpMiceRequestdetail = _context.RFPMiceRequestDetails.Where(x => x.rfp_mice_request_id == item.Id)
                                    .FirstOrDefault();
                                if (DBrfpMiceRequestdetail != null && DBrfpMiceRequestdetail.rfp_mice_request_id > 0)
                                {
                                    DBrfpMiceRequestdetail.status = apiResponse.Status;
                                    DBrfpMiceRequestdetail.request_detail_object = JsonConvert.SerializeObject(apiResponse);
                                    DBrfpMiceRequestdetail.modified_by = userId.Value;
                                    DBrfpMiceRequestdetail.modified_date = DateTime.Now;
                                }
                                else
                                {
                                    var rFPMiceRequestDetail = (new RFPMiceRequestDetails()
                                    {
                                        status = apiResponse.Status,
                                        rfp_mice_request_id = item.Id,
                                        request_detail_object = JsonConvert.SerializeObject(apiResponse),
                                        created_by = userId.Value,
                                        created_date = DateTime.Now,
                                    });
                                    _context.RFPMiceRequestDetails.Add(rFPMiceRequestDetail);
                                }

                                //28-04-2020 - Change due to wrong entry of status in RFPResources table
                                //api response according update status into RFPResources records
                                var DBrfpMiceResource = _context.RFPResources.Where(x => x.rfp_mice_request_id == item.Id )
                                    .ToList();
                                if (DBrfpMiceResource != null)
                                {
                                    foreach (var rfpMiceResource in DBrfpMiceResource)
                                    {
                                        if (apiResponse.Offers.Count > 0)
                                        {
                                            var offer = apiResponse.Offers.Where(t => t.HotelId == rfpMiceResource.mice_resource_id).FirstOrDefault();

                                            string status = "new";
                                            if(offer!=null)
                                            {
                                                status = offer.Status;
                                                rfpMiceResource.status = status;
                                                rfpMiceResource.modified_by = userId.Value;
                                                rfpMiceResource.modified_date = DateTime.Now;
                                            }
                                            _context.SaveChanges();
                                           
                                        }
                                    }
                                    
                                }
                               
                            }
                        }
                    }
                }
                var rfsDBEntity = (from rms in _context.RFPMiceRequests.Where(x => x.rfp_id == RfpId && x.resource_type == resource_type)
                                   join rfs in _context.RFPResources.DefaultIfEmpty() on
                                   rms.Id equals rfs.rfp_mice_request_id
                                   select new
                                   {
                                       rms,
                                       rfs
                                   }
                                  );
                var rmiDBEntity = (from rms in _context.RFPMiceRequests.Where(x => x.rfp_id == RfpId && x.resource_type == resource_type)
                                   join rmi in _context.RFPMiceRequestItems.DefaultIfEmpty() on
                                   rms.Id equals rmi.rfp_mice_request_id
                                   where rms.Id == rmi.rfp_mice_request_id
                                   select new
                                   {
                                       rms,
                                       rmi
                                   }
                                  ).AsEnumerable();

                var rfdDBEntity = (from rms in _context.RFPMiceRequests.Where(x => x.rfp_id == RfpId && x.resource_type == resource_type)
                                   join rfd in _context.RFPMiceRequestDetails.DefaultIfEmpty() on
                                   rms.Id equals rfd.rfp_mice_request_id
                                   where rms.Id == rfd.rfp_mice_request_id
                                   select new
                                   {
                                       rms,
                                       rfd
                                   }).AsEnumerable();
                var rmoDBEntity = (from rms in _context.RFPMiceRequests.Where(x => x.rfp_id == RfpId && x.resource_type == resource_type)
                                   join rmo in _context.RFPMiceRequestOrders.DefaultIfEmpty() on
                                   rms.Id equals rmo.rfp_mice_request_id
                                   select new
                                   {
                                       rms,
                                       rmo
                                   }).AsEnumerable();
                List<RFPMiceRequestModel> rFPMiceRequests;
                rFPMiceRequests = rfpmDbEntity.Select(x => new RFPMiceRequestModel()
                {
                    CityId = x.city_id,
                    Comments = x.comments,
                    Comments2 = x.comments2,
                    Comments3 = x.comments3,
                    Comments4 = x.comments4,
                    CountryId = x.country_id,
                    CreatedBy = x.created_by,
                    CreatedDate = x.created_date,
                    Id = x.Id,
                    MiceRequestId = x.mice_request_id,
                    ModifiedBy = x.modified_by,
                    ModifiedDate = x.modified_date,
                    PlaceId = x.place_id,
                    RfpId = x.rfp_id,
                    ResourceType = x.resource_type,
                    Status = x.status,
                    RFPResources = rfsDBEntity.Select(Y => new RFPResourceModel()
                    {
                        Id = Y.rfs.Id,
                        CreatedBy = Y.rfs.created_by,
                        CreatedDate = Y.rfs.created_date,
                        MiceResourceId = Y.rfs.mice_resource_id,
                        ModifiedBy = Y.rfs.modified_by,
                        ModifiedDate = Y.rfs.modified_date,
                        ResourceId = Y.rfs.resource_id,
                        RFPMiceRequestId = Y.rfs.rfp_mice_request_id,
                        Status = Y.rfs.status
                    }).Where(a => a.RFPMiceRequestId == x.Id).ToList(),
                    RFPMiceRequestItems = rmiDBEntity.Select(Z => new RequestItemModel()
                    {
                        CreatedBy = Z.rmi.created_by,
                        CreatedDate = Z.rmi.created_date,
                        Id = Z.rmi.Id,
                        ModifiedBy = Z.rmi.modified_by,
                        ModifiedDate = Z.rmi.modified_date,
                        RequestItemType = Z.rmi.request_item_type,
                        //RFPId = x.,
                        RFPMiceRequestId = Z.rmi.rfp_mice_request_id,
                        RequestItems = JsonConvert.DeserializeObject<List<RequestItemObject>>(Z.rmi.request_item_object)
                    }).Where(a => a.RFPMiceRequestId == x.Id).ToList(),
                    RequestDetail = rfdDBEntity.Select(Q => new RFPMiceRequestDetailModel()
                    {
                        CreatedBy = Q.rfd.created_by,
                        CreatedDate = Q.rfd.created_date,
                        Id = Q.rfd.Id,
                        MiceRequestId = Q.rfd.rfp_mice_request_id,
                        ModifiedBy = Q.rfd.modified_by,
                        ModifiedDate = Q.rfd.modified_date,
                        RFPMiceRequestId = Q.rfd.rfp_mice_request_id,
                        Status = Q.rfd.status,
                        RequestOfferDetails = JsonConvert.DeserializeObject<OfferModel>(Q.rfd.request_detail_object)
                    }).Where(a => a.RFPMiceRequestId == x.Id).FirstOrDefault(),
                    OrderDetail = rmoDBEntity.Select(P => new RFPMiceRequestOrderModel()
                    {
                        CreatedBy = P.rmo.created_by,
                        CreatedDate = P.rmo.created_date,
                        Id = P.rmo.Id,
                        MiceOrderId = P.rmo.mice_order_id,
                        ModifiedBy = P.rmo.modified_by,
                        ModifiedDate = P.rmo.modified_date,
                        OrderDetail = JsonConvert.DeserializeObject<OrderDetailModel>(P.rmo.order_detail_object),
                        RFPMiceRequestId = P.rmo.rfp_mice_request_id,
                        Status = P.rmo.status
                    }
                ).Where(a => a.RFPMiceRequestId == x.Id).FirstOrDefault()
                }).ToList();


                if (rFPMiceRequests.Where(t => t.RequestDetail != null).Count() > 0 && rFPMiceRequests.Where(t => t.RequestDetail.RequestOfferDetails != null).Count() > 0 && rFPMiceRequests.Where(t => t.RequestDetail.RequestOfferDetails.Offers != null).Count() > 0)
                {
                    List<Offer> lstOffer = rFPMiceRequests.Select(q => q.RequestDetail.RequestOfferDetails.Offers).FirstOrDefault();

                    foreach (var item in lstOffer)
                    {
                        IEnumerable<Models.ResourceModel> resourceModels = null;
                        if (resource_type == 0)
                        {
                            var hotelInfo = _context.Hotels.Where(h => h.mice_id == item.HotelId.ToString()).FirstOrDefault();
                            if (hotelInfo != null)
                            {
                                SearchRepository searchRepository = new SearchRepository();
                                int[] hotelIdValue = new int[1];
                                hotelIdValue[0] = hotelInfo.dcp_hotelinfo_id;

                                resourceModels = searchRepository.Hotels(hotelIdValue, lang);


                            }
                        }
                        else if (resource_type == 1)
                        {
                            var restaurantsInfo = _context.Restaurants.Where(h => h.mice_id == item.HotelId.ToString()).FirstOrDefault();
                            if (restaurantsInfo != null)
                            {
                                SearchRepository searchRepository = new SearchRepository();
                                int[] hotelIdValue = new int[1];
                                hotelIdValue[0] = restaurantsInfo.dcp_restaurantinfo_id;

                                resourceModels = searchRepository.Restaurants(hotelIdValue, lang);
                                // item.Resource = resourceModels;
                            }
                        }
                        else if (resource_type == 2)
                        {
                            var locationInfo = _context.Locations.Where(h => h.mice_id == item.HotelId.ToString()).FirstOrDefault();
                            if (locationInfo != null)
                            {
                                SearchRepository searchRepository = new SearchRepository();
                                int[] hotelIdValue = new int[1];
                                hotelIdValue[0] = locationInfo.dcp_locationinfo_id;

                                resourceModels = searchRepository.Locations(hotelIdValue, lang);
                                //  item.Resource = resourceModels;
                            }
                        }
                        item.resource_type = resource_type;
                        item.Resource = resourceModels.Select(t => new DCP.Mice.Models.ResourceModel
                        {
                            id = t.id,
                            name = t.name,
                            country = t.country,
                            countryid = t.countryid,
                            city = t.city,
                            state = t.state,
                            street = t.street,
                            webaddress = t.webaddress,
                            info = t.info,
                            zipcode = t.zipcode,
                            lng = t.lng,
                            lat = t.lat,
                            time = t.time,
                            distance = t.distance,
                            KM = t.KM,
                            is360degree = t.is360degree,
                            type = t.type,
                            isincart = t.isincart,
                            stars = t.stars,
                            chainname = t.chainname,
                            airport = t.airport,
                            htotalroom = t.htotalroom,
                            htotalcroom = t.htotalcroom,
                            hsizecroom = t.hsizecroom,
                            hparking_spaces = t.hparking_spaces,
                            rcapacity = t.rcapacity,
                            rkitchenId = t.rkitchenId,
                            rtotal_conference = t.rtotal_conference,
                            rcapacity_of_largest_room = t.rcapacity_of_largest_room,
                            rsize_of_largest_conference = t.rsize_of_largest_conference,
                            rminimum_turnover = t.rminimum_turnover,
                            ltotal_conference = t.ltotal_conference,
                            lroom_height = t.lroom_height,
                            lsize_of_largest_conference = t.lsize_of_largest_conference,
                            lrent_begining = t.lrent_begining,
                            chkCP = t.chkCP,
                            chkTP = t.chkTP,
                            chkCD = t.chkCD,
                            hotelname = t.hotelname,
                            outdoor_details = t.outdoor_details,
                            chkOP = t.chkOP,
                            chkRIH = t.chkRIH,
                            lstkitchen = t.lstkitchen,
                            kitchen = t.kitchen,
                            lstsuitablefor = t.lstsuitablefor,
                            suitablefor = t.suitablefor,
                            lstrestauranttype = t.lstrestauranttype,
                            restauranttype = t.restauranttype,
                            lsthoteltype = t.lsthoteltype,
                            hoteltype = t.hoteltype,
                            lstlocationtype = t.lstlocationtype,
                            locationtype = t.locationtype
                        }).ToList();
                    }
                }







                //List<Offer> lstOffer = rFPMiceRequests.Select(q => q.RequestDetail.RequestOfferDetails.Offers).FirstOrDefault();
                //var hotelIds = (from of in lstOffer select new { of.HotelId }).ToList();
                //if (hotelIds.Count > 0)
                //{
                //    int[] hotelIdValue = new int[20];
                //    int i = 0;
                //    foreach (var item in hotelIds)
                //    {
                //        string miceId = item.HotelId.ToString();
                //        var hotelInfo = _context.Hotels.Where(h => h.mice_id == miceId).FirstOrDefault();
                //        hotelIdValue[i] = hotelInfo.dcp_hotelinfo_id;
                //        i++;
                //    }

                //    SearchRepository searchRepository = new SearchRepository();
                //    if (resource_type == 0)
                //    {
                //        IEnumerable<ResourceModel> resourceModels = searchRepository.Hotels(hotelIdValue, "en-Us");

                //        List<OfferModel> offerModels = rFPMiceRequests.Select(t => t.RequestDetail).ToList().Select(t => t.RequestOfferDetails).ToList();
                //        foreach (var item in offerModels)
                //        {
                //            foreach (var offer in item.Offers)
                //            {

                //            }
                //        }
                //    }
                //}

                var request = rFPMiceRequests.FirstOrDefault();
                if(request!=null && request.RequestDetail!=null && request.RequestDetail.RequestOfferDetails!=null)
                {
                    if(request.RequestDetail.RequestOfferDetails.AbortReason!=null)
                    {
                        var res = _context.AbortReasons.Where(t => t.mice_abort_reason == request.RequestDetail.RequestOfferDetails.AbortReason).FirstOrDefault();
                        request.RequestDetail.RequestOfferDetails.AbortReasonValue = (lang == "en-US" ? res.abort_reason : res.abort_reason_de);
                        
                    }
                }



                proposalModel = new RequestForProposalModel()
                {
                    CreatedBy = rfpDbEntity.created_by,
                    CreatedDate = rfpDbEntity.created_date,
                    Id = rfpDbEntity.Id,
                    DaysRunning = rfpDbEntity.days_running,
                    IsSubmitted = rfpDbEntity.is_submitted,
                    MiceUserId = rfpDbEntity.mice_user_id,
                    ModifiedBy = rfpDbEntity.modified_by,
                    ModifiedDate = rfpDbEntity.modified_date,
                    RFPType = rfpDbEntity.rfp_type,
                    Status = rfpDbEntity.status,
                    UserId = rfpDbEntity.user_id,
                    EventName = rfpDbEntity.event_name,
                    RFPMiceRequests = rFPMiceRequests
                };
            }
            return proposalModel;
        }
        /// <summary>
        /// Skip count
        /// </summary>
        /// <returns></returns>
        private int Skipcount(int page, int pageSize)
        {
            int skipCount = page == 1 ? 0 : (pageSize * (page - 1));

            return skipCount;
        }

        ///INSPIRE-102
        ///User can store multiple rfp so is_submitted flag is comment and user wise retrive all records  
        /// <summary>
        /// Retrive RFPMiceRequest With Proposal
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MainRFPMiceRequestWithProposalModel GetRFPMiceRequestWithProposal(int page, int pageSize, int? userId, string sortby, string search)
        {
            MainRFPMiceRequestWithProposalModel miceRequestWithProposalModel = new MainRFPMiceRequestWithProposalModel();
            List<RFPMiceRequestWithProposalModel> requestWithProposalModel = new List<RFPMiceRequestWithProposalModel>();
            var dbEntity = (from rp in _context.RequestForProposals
                            join us in _context.User_Masters on rp.user_id equals us.User_Id
                            where (userId == null ? 1 == 1 : (us.User_Id == userId || us.Parenet_User_Id == userId)) //&& rp.user_id == userId 
                           // && rp.is_submitted == true
                            select new
                            {
                                rp.Id,
                                rp.mice_user_id,
                                rp.days_running,
                                rp.created_date,
                                rp.event_name,
                                userName = us.First_Name + " " + us.Last_Name,
                                us.isAdmin,
                                us.Parenet_User_Id,
                                rp.is_submitted                               
                            }).AsEnumerable();

            if (!string.IsNullOrEmpty(search))
            {
                dbEntity = dbEntity.Where(t => t.event_name.ToLower().Contains(search.ToLower()));
            }



            if (!string.IsNullOrEmpty(sortby))
            {
                if (sortby == "CreatedDateAsc")
                {
                    dbEntity = dbEntity.OrderBy(t => t.created_date);
                }
                else if (sortby == "CreatedDateDesc")
                {
                    dbEntity = dbEntity.OrderByDescending(t => t.created_date);
                }
                else if (sortby == "EventNameAsc")
                {
                    dbEntity = dbEntity.OrderBy(t => t.event_name);
                }
                else if (sortby == "EventNameDesc")
                {
                    dbEntity = dbEntity.OrderByDescending(t => t.event_name);
                }
            }
            else
            {
                dbEntity = dbEntity.OrderByDescending(t => t.created_date);
            }



            var dbRequestEntity = (from rp in dbEntity
                                   join rr in _context.RFPMiceRequests on rp.Id equals rr.rfp_id
                                   select new
                                   {
                                       proposalId = rr.Id,
                                       mice_request_id = rr.mice_request_id,
                                       status = rr.status,
                                       rfp_id = rr.rfp_id,
                                       resourcetype = rr.resource_type
                                   }).ToList();

            var dbRFPMiceRequestDetail = (from rr in _context.RFPMiceRequests
                                          join rd in _context.RFPResources on rr.Id equals rd.rfp_mice_request_id
                                          select new
                                          {
                                              requstId = rr.Id,
                                              requstDetailId = rd.Id,
                                              rfpId = rr.rfp_id,
                                              status = rd.status,
                                              type = rr.resource_type
                                          }).ToList();

            List<string> lstStatus = new List<string>();
            lstStatus.Add("running");
            lstStatus.Add("new");

            miceRequestWithProposalModel.TotalRecords = dbEntity.Count();

            requestWithProposalModel = dbEntity.Select(t => new RFPMiceRequestWithProposalModel()
            {
                RfpId = t.Id,
                MiceUserId = t.mice_user_id,
                DaysRunning = t.days_running,
                CreatedDate = ManageSetting.FormatDateValue(t.created_date),
                Event_Name = t.event_name,
                IsSubmitted= t.is_submitted,

                Hotel_Mice_Request_id = dbRequestEntity.Where(r => r.rfp_id == t.Id && r.resourcetype == 0).Select(q => q.mice_request_id).FirstOrDefault(),
                Hotel_Rfp_Misc_Id = dbRequestEntity.Where(r => r.rfp_id == t.Id && r.resourcetype == 0).Select(q => q.proposalId).FirstOrDefault(),
                Hotel_Status = dbRequestEntity.Where(r => r.rfp_id == t.Id && r.resourcetype == 0).Select(q => q.status).FirstOrDefault(),

                Restaurant_Misc_request_Id = dbRequestEntity.Where(r => r.rfp_id == t.Id && r.resourcetype == 1).Select(q => q.mice_request_id).FirstOrDefault(),
                Restaurant_Rfp_Misc_Id = dbRequestEntity.Where(r => r.rfp_id == t.Id && r.resourcetype == 1).Select(q => q.proposalId).FirstOrDefault(),
                Restaurant_Status = dbRequestEntity.Where(r => r.rfp_id == t.Id && r.resourcetype == 1).Select(q => q.status).FirstOrDefault(),

                Location_Misc_request_Id = dbRequestEntity.Where(r => r.rfp_id == t.Id && r.resourcetype == 2).Select(q => q.mice_request_id).FirstOrDefault(),
                Location_Rfp_Misc_Id = dbRequestEntity.Where(r => r.rfp_id == t.Id && r.resourcetype == 2).Select(q => q.proposalId).FirstOrDefault(),
                Location_Status = dbRequestEntity.Where(r => r.rfp_id == t.Id && r.resourcetype == 2).Select(q => q.status).FirstOrDefault(),
                UserName = t.userName,
                TotalResource = dbRequestEntity.Where(r => r.rfp_id == t.Id).Count(),
                HotelStatusCount = dbRFPMiceRequestDetail.Where(r => r.rfpId == t.Id && r.type == 0 && !lstStatus.Contains(r.status)).Count(),
                RestaurantStatusCount = dbRFPMiceRequestDetail.Where(r => r.rfpId == t.Id && r.type == 1 && !lstStatus.Contains(r.status)).Count(),
                LocationStatusCount = dbRFPMiceRequestDetail.Where(r => r.rfpId == t.Id && r.type == 2 && !lstStatus.Contains(r.status)).Count(),
                TotalHotelResource = dbRFPMiceRequestDetail.Where(r => r.rfpId == t.Id && r.type == 0).Count(),
                TotalRestaurantResource = dbRFPMiceRequestDetail.Where(r => r.rfpId == t.Id && r.type == 1).Count(),
                TotalLocationResource = dbRFPMiceRequestDetail.Where(r => r.rfpId == t.Id && r.type == 2).Count(),
                RFPIdValue = Crypto.EnryptString(t.Id.ToString())
            }).Skip(Skipcount(page, pageSize)).Take(pageSize).ToList();

            miceRequestWithProposalModel.RFPMiceRequestWithProposalModels = requestWithProposalModel;

            return miceRequestWithProposalModel;

        }



        private DCPMiceRequests SetDCPMiceRequestModel(int rfpId, int? miceReqestId, string detail, int resource_type, int? userId, int request_type)
        {
            DCPMiceRequests miceRequests = new DCPMiceRequests();

            miceRequests.rfp_id = rfpId;
            miceRequests.mice_request_id = miceReqestId;
            miceRequests.request_detail_object = detail;
            miceRequests.resource_type = resource_type;
            miceRequests.request_type = request_type;
            miceRequests.created_by = userId==null?0: Convert.ToInt32(userId);
            miceRequests.created_date = DateTime.Now;
            return miceRequests;
        }

       
    }
}
