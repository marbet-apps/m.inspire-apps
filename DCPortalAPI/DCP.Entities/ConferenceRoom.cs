﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DCP.Entities
{
    public class ConferenceRoom
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public double? SquareMeter { get; set; }
        public int TransactionId { get; set; }
        public int TransactionType { get; set; }

        public ICollection<ConferenceRoomQuantities> ConferenceRoomQuantities { get; set; }
    }
}
