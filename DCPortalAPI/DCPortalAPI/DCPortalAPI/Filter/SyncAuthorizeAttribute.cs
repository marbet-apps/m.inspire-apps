﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Security.Cryptography;
using System.Text;

namespace DCPortalAPI.Filter
{
    public class SyncAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if(actionContext.Request.Headers.Authorization==null)
            {
               // actionContext.Response =  new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                HandleUnauthorizedRequest(actionContext);
            }
            else
            {
                if (IsAuthorize(actionContext))
                {
                    return;
                }
                HandleUnauthorizedRequest(actionContext);
              //  actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }

        private bool IsAuthorize(HttpActionContext actionContext)
        {
           string value = GetBasicAuthenticationString();
           if (actionContext.Request.Headers.Authorization.Parameter.ToString()==value)
           {
                return true;
           }
            return false;
        }
        private static string GetBasicAuthenticationString()
        {
            string input = string.Format("{0}{1}", Settings.Constants.UserNameForSyncService, Settings.Constants.PasswordForSyncService);
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes("abmw-8hn9-sqoy20");
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
    }
}