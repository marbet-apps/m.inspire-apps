﻿using System.Collections.Generic;

namespace DCP.Mice.Models.Common
{
    public class MiceMasterValue
    {
        public static List<string> GetAllRequestTypes()
        {
            return new List<string>
            {
                MiceConstants.RequestType.EVENTS,
                MiceConstants.RequestType.GROUPS,
                MiceConstants.RequestType.INDIVIDUAL,
                MiceConstants.RequestType.MEETINGS
            };
        }

        public static List<string> GetAllRequestItemTypes()
        {
            return new List<string>
            {
                MiceConstants.RequestItemType.ACCOMMODATION,
                MiceConstants.RequestItemType.CONFERENCE_ROOM,
                MiceConstants.RequestItemType.GROUP_ROOM
            };
        }

        public static List<string> GetAllRoomTypes()
        {
            return new List<string>
            {
                MiceConstants.RoomType.ROOM_DOUBLE_BED,
                MiceConstants.RoomType.ROOM_DOUBLE_BED_SINGLE_USE,
                MiceConstants.RoomType.ROOM_FOUR_BED,
                MiceConstants.RoomType.ROOM_JUNIOR_SUITE,
                MiceConstants.RoomType.ROOM_SINGLE_BED,
                MiceConstants.RoomType.ROOM_SUITE,
                MiceConstants.RoomType.ROOM_THREE_BED,
                MiceConstants.RoomType.ROOM_TWIN
            };
        }

        public static List<string> GetAllSeatingTypes()
        {
            return new List<string>
            {
                MiceConstants.SeatingType.BANQUET,
                MiceConstants.SeatingType.BLOCK,
                MiceConstants.SeatingType.CABARET,
                MiceConstants.SeatingType.CIRCLE,
                MiceConstants.SeatingType.NOSEATING,
                MiceConstants.SeatingType.PARLIAMENTARY,
                MiceConstants.SeatingType.ROWS,
                MiceConstants.SeatingType.TABLES,
                MiceConstants.SeatingType.USHAPE
            };
        }

        public static List<string> GetAllAbortReasons()
        {
            return new List<string>
            {
                MiceConstants.AbortReason.CHANGED_DATE,
                MiceConstants.AbortReason.CHANGED_DESTINATION,
                MiceConstants.AbortReason.CHANGED_HOTEL_SELECTION,
                MiceConstants.AbortReason.INCORRECT
            };
        }

        public static List<int> GetAllDurationChioces()
        {
            return new List<int>
            {
                MiceConstants.DurationChioce.FIVE,
                MiceConstants.DurationChioce.TEN,
                MiceConstants.DurationChioce.FIFTEEN,
                MiceConstants.DurationChioce.TWENTY
            };
        }
    }
}
