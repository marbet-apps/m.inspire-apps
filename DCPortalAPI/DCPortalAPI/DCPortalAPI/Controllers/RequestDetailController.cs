﻿using DCP.Business.Interface;
using DCP.Utilities;
using DCPortalAPI.Filter;
using DCPortalAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;


namespace DCPortalAPI.Controllers
{
   
    [RoutePrefix("api/RequestDetail")]
    [Authorize]
    [ExceptionHandler]
    public class RequestDetailController : ApiController
    {

        private IRequestBL _IRequestBL;
        private IRequestDetail _IRequestDetail;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IRequestBl"></param>
        /// <param name="_IRequestDetail"></param>
        public RequestDetailController(IRequestBL IRequestBl, IRequestDetail _IRequestDetail)
        {
            this._IRequestBL = IRequestBl;
            this._IRequestDetail = _IRequestDetail;
        }

        /// <summary>
        /// Retrive RFPMiceRequest with Proposal
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortby"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRFPMiceRequestWithProposal")]
        public IHttpActionResult GetRFPMiceRequestWithProposal(int page, int pageSize, string sortby=null, string search=null)
        {
            WriteLog.LogInformation(LogMessages.GetRFPMiceRequestWithProposalStarted);
            try
            {                
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var result = _IRequestDetail.GetRFPMiceRequestWithProposal(page, pageSize, userId,sortby, search);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRFPMiceRequestWithProposalEnded);
            }
        }

        /// <summary>
        /// Retrive details for RFP.
        /// </summary>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRFPDetail")]
        [Authorize]
        public IHttpActionResult GetRFPDetails(string rfpId, string type)
        {
            WriteLog.LogInformation(LogMessages.RFPListStarted);
            try
            { 
                string strId = Crypto.DecryptString(rfpId);
                int rfpIdValue = Convert.ToInt32(strId);
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                var result = _IRequestDetail.GetRFPDetail(rfpIdValue, userId, type,lang);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RFPListEnded);
            }
        }


    }
}