﻿using DCP.Business.Interface;
using DCP.Entities;
using DCP.Mice.Models;
using DCP.Models;
using DCP.Models.RFP;
using DCP.Repository.Interface;
using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.Net;

namespace DCP.Business
{
    public class RequestBL : IRequestBL
    {
        private IRequestRepository _IRequestRepository;

        public RequestBL(IRequestRepository _IRequestRepository)
        {
            this._IRequestRepository = _IRequestRepository;
        }

        #region Cart

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cartModel"></param>
        /// <returns></returns>
        public ApiResult MarkResourceForTheRequest(RequestCartModel cartModel)
        {
            try
            {
                var response = _IRequestRepository.MarkResourceForTheRequest(cartModel);

                if (response == 0) {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.Saved, response, ManageEnum.status.fail.ToString(), ResponceMessages.Error.Saved);
                }

                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        ///INSPIRE-102
        ///RFPId wise delete record from multiple RFP of user
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="resourceId"></param>
        /// <param name="resourceType"></param>
        /// <returns></returns>
        public ApiResult RemoveResourceFromTheSelection(int userId, int resourceId, string resourceType, int? rfpId)
        {
            try
            {
                var response = _IRequestRepository.RemoveResourceFromTheSelection(userId, resourceId, resourceType, rfpId);
                return ResultSet.SuccessResponse(ResponceCodes.Success.Deleted, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Deleted);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="resourceId"></param>
        /// <param name="resourceType"></param>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        public ApiResult AddResourceToRFP(int userId, string[] resourceIds, string resourceType, int rfpId)
        {
            try
            {
                foreach (var item in resourceIds)
                {
                    _IRequestRepository.AddResourceToRFP(userId, Convert.ToInt32(item), resourceType, rfpId);
                }
                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, true, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ApiResult GetRequestCart(int userId)
        {
            try
            {
                var response = _IRequestRepository.GetRequestCart(userId);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        ///INSPIRE-102
        ///Store RFP and return request proposal modal for multiple RFP save purpose
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ApiResult MakeRequestForProposal(RequestForProposalModel rfpModel)
        {
            try
            {
                var response = _IRequestRepository.MakeRequestForProposalFromStartRfp(rfpModel);
                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ApiResult GetRequestCartCount(int userId)
        {
            try
            {
                var response = _IRequestRepository.GetRequestCartCount(userId);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetRequestForProposalById(int id, string langCode)
        {
            try
            {
                var response = _IRequestRepository.GetRequestForProposalById(id,langCode);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        #endregion Cart

        #region Lookup Methods


        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public ApiResult GetConferenceFoods(string lang)
        {
            try
            {
                var response = _IRequestRepository.GetConferenceFoods(lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }


        #endregion Lookup Methods

        #region RFP Request Items

        public ApiResult GetRFPRequestItems(int rfpMiceRequestItemId, string lang)
        {
            try
            {
                var response = _IRequestRepository.GetRFPRequestItems(rfpMiceRequestItemId, lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult AddEditRFPRequestItem(RequestItemModel requestItemModel)
        {
            try
            {
                var response = _IRequestRepository.AddEditRFPRequestItem(requestItemModel);
                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult DeleteRFPRequestItem(int rfpMiceRequestItemId)
        {
            try
            {
                var response = _IRequestRepository.DeleteRFPRequestItem(rfpMiceRequestItemId);
                return ResultSet.SuccessResponse(ResponceCodes.Success.Deleted, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Deleted);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        #endregion RFP Request Items

        #region RFP

        public ApiResult GetRequestForProposal(int userId, string langCode)
        {
            try
            {
                var response = _IRequestRepository.GetRequestForStartRFP(userId, langCode);
                //var response = _IRequestRepository.GetRequestForProposal(userId, langCode);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        public ApiResult SubmitRFP(RequestForProposalModel rfpModel, bool isSubmit, int userId)
        {
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                if (isSubmit==true)
                {                   
                    result = _IRequestRepository.ValidateSubmitRFP(rfpModel);
                }

                if(result!=null && result.Count>0)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.InvalidData, result, ManageEnum.status.fail.ToString(), ResponceMessages.Error.Saved);
                }
                var response = _IRequestRepository.SubmitRFP(rfpModel, isSubmit, userId);

                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        #endregion RFP

        public ApiResult BookedOrder(OrderBookingModel bookingModel,int userId)
        {
            try
            {
                var response = _IRequestRepository.BookedOrder(bookingModel, userId);
                if(response.Status_code == "BadRequest")
                {
                    return ResultSet.SuccessResponse(ResponceCodes.MiceError.OrderFailed, response, ManageEnum.status.error.ToString(), ResponceMessages.MiceError.OrderFailed);
                }
                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
        public ApiResult AbortOrder(OrderAbortModel abortModel)
        {
            try
            {
                var response = _IRequestRepository.AbortOrder(abortModel);
                if (response.Status_code == "BadRequest")
                {
                    return ResultSet.SuccessResponse(ResponceCodes.MiceError.AbortFailed, response, ManageEnum.status.error.ToString(), ResponceMessages.MiceError.AbortFailed);
                }
                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        ///INSPIRE-102
        ///User wise save multiple RFP into table so rfpid wise get resource detail 
        public ApiResult GetResourceItemDetail(int userId, string langCode, string sortBy, string type, string requestItemType, int rfpId)
        {
            try
            {
                var response = _IRequestRepository.GetResourceItemDetail(userId, langCode, sortBy,type,requestItemType,rfpId);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

    }
}
