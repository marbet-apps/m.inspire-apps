﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class User_Master
    {
        [Key]
        public int User_Id { get; set; }
       
        [Required, MaxLength(200)]
        public string First_Name { get; set; }
        [Required, MaxLength(200)]
        public string Last_Name { get; set; }
        [Required(AllowEmptyStrings =true), MaxLength(200)]
        public string Company_Name { get; set; }
        [Required(AllowEmptyStrings = true), MaxLength(200)]
        public string Department_Name { get; set; }
        [Required,MaxLength(10), Column(TypeName = "varchar")]
        public string Salutation { get; set; }
        [Required,DefaultValue("true")]
        public bool IsActive { get; set; }
        [Required]
        public bool Isself_Created { get; set; }

        
        [ForeignKey("Subscription_Type")]
        public int Subscription_Type_Id { get; set; }

        public int? Parenet_User_Id { get; set; }
        [Required]
        public bool IsApproved { get; set; }

        [Required]
        public bool IsDeleted { get; set; }
        
        public DateTime? Last_AccessDate { get; set; }
        public int? created_by { get; set; }
        public DateTime? created_date { get; set; }
        public int? modified_by { get; set; }
        public DateTime? modify_date { get; set; }

        public bool? isAdmin { get; set; }

        [MaxLength]
        public byte[] photo { get; set; }

        public List_SubscriptionType Subscription_Type  { get; set; }

        public ICollection<User_Communication> User_Communications { get; set; }
        public ICollection<User_Credential> User_Credentials { get; set; }

        public ICollection<User_Access> User_Access { get; set; }
        public ICollection<RequestForProposal> RequestForProposals { get; set; }
        public ICollection<RequestCart> RequestCarts { get; set; }

        [MaxLength(200), Column(TypeName = "varchar")]
        public string Token { get; set; }
    }
}
