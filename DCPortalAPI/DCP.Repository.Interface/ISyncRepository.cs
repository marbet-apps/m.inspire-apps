﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DCP.Models;
namespace DCP.Repository.Interface
{
    public interface ISyncRepository
    {
        List<ResponseSyncModel> SyncHotels(MigrationHotelModel migrationModel);

        List<ResponseSyncModel> SyncLocations(MigrationLocationModel LocationMigrationModels);

        List<ResponseSyncModel> SyncRestaurants(MigrationResaurantModel ResaurantMigrationModels);

        bool MigrateAirport(List<AirportModel> sourceTable);

        bool MigrateCountry(List<CountryModel> sourceTable);

        bool MigrateHotelType(List<HotelTypeModel> sourceTable);

        bool MigrateRestaurantType(List<RestaurantTypeModel> sourceTable);

        bool MigrateLocationType(List<LocationTypeModel> sourceTable);

        bool MigrateKitchen(List<KitchenModel> sourceTable);

        bool MigrateSuitableFor(List<SuitableForModel> sourceTable);

        bool DeleteResource(List<KbStatusModel> statusModels);
    }
}
