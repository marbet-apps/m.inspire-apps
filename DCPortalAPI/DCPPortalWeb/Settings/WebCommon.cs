﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace DCPPortalWeb.Settings
{
    public class Constants
    {
        public static readonly string APIBaseURL = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["APIBaseURL"]) ? ConfigurationManager.AppSettings["APIBaseURL"] : "localhost";
        public static readonly string ImageBaseURL = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["ImageBaseURL"]) ? ConfigurationManager.AppSettings["ImageBaseURL"] : "localhost";
        public static readonly string GoogleMapKey = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["MapKey"]) ? (ConfigurationManager.AppSettings["MapKey"]) : "";
        public static readonly string NearByKM = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["NearByKM"]) ? (ConfigurationManager.AppSettings["NearByKM"]) : "5";

        public static readonly string WebsiteURL = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["WebsiteURL"]) ? ConfigurationManager.AppSettings["WebsiteURL"] : "localhost";
        

    }

}