﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace DCPPortalWeb.Controllers
{
    public class BaseController : Controller
    {
        #region Constructor

        static BaseController()
        {
        }

        #endregion

        #region Overridden Methods

        /// <summary>
        /// To change current ui culture based on session value that is assigned when language is selected from language dropdown.
        /// Also assigning value in viewbag which is used to populate selected language in language dropdown
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Get the cookie of language
            HttpCookie languageCookie = System.Web.HttpContext.Current.Request.Cookies["Language"];
            var myCulture = string.Empty;
            if (languageCookie != null)
            {
                myCulture = Convert.ToString(languageCookie.Value);
            }
            else
            {
                myCulture = Convert.ToString(filterContext.HttpContext.Session["MyCulture"]);
            }
            
            if (!string.IsNullOrEmpty(myCulture))
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(myCulture);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(myCulture);
            }
            else
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("de-DE");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            }
            ViewBag.Language = Convert.ToString(Thread.CurrentThread.CurrentUICulture);
        }

        /// <summary>
        /// Changes culture based on which flag is clicked
        /// </summary>
        /// <param name="lang">change culture to which language</param>
        /// <param name="returnUrl">Redirect Url</param>
        /// <returns></returns>
        public ActionResult ChangeCulture(string lang)
        {
            //Add cookie of language in response
            if (System.Web.HttpContext.Current.Request.Cookies["Language"] != null)
                Response.Cookies.Remove("Language");
            var langCookie = new HttpCookie("Language");
            langCookie["Language"] = lang;
            langCookie.Value = lang;
            langCookie.Expires = DateTime.Now.AddYears(10);
            Response.Cookies.Add(langCookie);

            System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(lang);
            System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;
            HttpContext.Session["MyCulture"] = cultureInfo;
            return Redirect(Request.UrlReferrer.ToString());
        }

        #endregion

    }
}