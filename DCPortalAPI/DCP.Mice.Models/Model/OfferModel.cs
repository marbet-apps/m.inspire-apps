﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DCP.Mice.Models
{
    public class OfferModel
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("id")]
        public int RequestId { get; set; }

        [JsonProperty("createTime")]
        public DateTime? CreateTime { get; set; }

        [JsonProperty("bookedTime")]
        public DateTime? BookedTime { get; set; }

        [JsonProperty("abortedTime")]
        public DateTime? AbortedTime { get; set; }

        [JsonProperty("abortReason")]
        public string AbortReason { get; set; }

        [JsonProperty("duration")]
        public int Duration { get; set; }

        [JsonProperty("budget")]
        public double Budget { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("arrivalTime")]
        public DateTime? ArrivalTime { get; set; }

        [JsonProperty("departureTime")]
        public DateTime? DepartureTime { get; set; }

        [JsonProperty("commentsBooked")]
        public string CommentsBooked { get; set; }

        [JsonProperty("offers")]
        public List<Offer> Offers { get; set; }

        [JsonProperty("bookedOffer")]
        public Offer BookedOffer { get; set; }

        public string AbortReasonValue { get; set; }

        public string DisplayAbortedTime
        {
            get
            {
                if(!string.IsNullOrEmpty(Convert.ToString(AbortedTime)))
                {
                    return Convert.ToDateTime(AbortedTime).ToString("dd.MM.yyyy");
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }

    public class Offer
    {
        [JsonProperty("id")]
        public int OfferId { get; set; }

        [JsonProperty("hotelid")]
        public int HotelId { get; set; }

        [JsonProperty("createTime")]
        public DateTime? CreateTime { get; set; }

        [JsonProperty("offeredTime")]
        public DateTime? OfferedTime { get; set; }

        [JsonProperty("rejectedTime")]
        public DateTime? RejectedTime { get; set; }

        [JsonProperty("rejectReason")]
        public string RejectReason { get; set; }

        [JsonProperty("volume")]
        public float Volume { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("comments")]
        public string Comment { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("roomPrices")]
        public List<RoomPrice> RoomPrices { get; set; }

        [JsonProperty("resourceType")]
        public int resource_type { get; set; }

        [JsonProperty("resource")]
        public IEnumerable<ResourceModel> Resource { get; set; }
    }

    public class RoomPrice
    {
        [JsonProperty("price")]
        public double Price { get; set; }

        [JsonProperty("comments")]
        public string Comment { get; set; }

        [JsonProperty("type")]
        public string RoomType { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("reserveFromTime")]
        public DateTime ReserveFromDate { get; set; }

        [JsonProperty("reserveToTime")]
        public DateTime ReserveToDate { get; set; }

        [JsonProperty("isFlatRate")]
        public bool IsFlatRate { get; set; }
    }

    /// <summary>
    /// Resource data
    /// </summary>
    public class ResourceModel
    {
        // Common Details
        public int id { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public int countryid { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string street { get; set; }
        public string webaddress { get; set; }
        public string info { get; set; }
        public string zipcode { get; set; }
        public double? lng { get; set; }
        public double? lat { get; set; }
        public string time { get; set; }
        public double? distance { get; set; }
        public double? KM { get; set; }
        public bool? is360degree { get; set; }
        public string type { get; set; }
        public bool? isincart { get; set; }
        //public int cartId { get; set; }
        // Hotel Details
        public int? stars { get; set; }
        public string chainname { get; set; }
        public string airport { get; set; }
        public int? htotalroom { get; set; }
        public int? htotalcroom { get; set; }
        public double? hsizecroom { get; set; }
        public Nullable<int> hparking_spaces { get; set; }

        // Restaurant Details
        public int? rcapacity { get; set; }
        public int? rkitchenId { get; set; }
        public int? rtotal_conference { get; set; }
        public int? rcapacity_of_largest_room { get; set; }
        public double? rsize_of_largest_conference { get; set; }
        public double? rminimum_turnover { get; set; }

        // Location Details
        public int? ltotal_conference { get; set; }
        public double? lroom_height { get; set; }
        public double? lsize_of_largest_conference { get; set; }
        public double? lrent_begining { get; set; }
        public bool? chkCP { get; set; }
        public bool? chkTP { get; set; }
        public bool? chkCD { get; set; }

        public string hotelname { get; set; }
        public string outdoor_details { get; set; }
        public bool? chkOP { get; set; }
        public bool? chkRIH { get; set; }

        public IQueryable<string> lstkitchen { get; set; }
        public string kitchen { get; set; }

        public IQueryable<string> lstsuitablefor { get; set; }
        public string suitablefor { get; set; }

        public IQueryable<string> lstrestauranttype { get; set; }
        public string restauranttype { get; set; }

        public IQueryable<string> lsthoteltype { get; set; }
        public string hoteltype { get; set; }

        public IQueryable<string> lstlocationtype { get; set; }
        public string locationtype { get; set; }

    }

}
