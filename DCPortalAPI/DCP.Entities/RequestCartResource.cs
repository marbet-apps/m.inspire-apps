﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class RequestCartResource
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Cart")]
        public int request_cart_id { get; set; }

        public int resource_id { get; set; }

        public int resource_type { get; set; }

        public bool is_added_to_rfp { get; set; }

        public int created_by { get; set; }

        public DateTime created_date { get; set; }

        public int? modified_by { get; set; }

        public DateTime? modified_date { get; set; }

        public RequestCart Cart { get; set; }
    }
}
