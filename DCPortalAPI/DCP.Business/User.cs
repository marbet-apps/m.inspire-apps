﻿using DCP.Business.Interface;
using DCP.Entities;
using DCP.Models;
using DCP.Repository.Interface;
using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Business
{
    public class User : IUser
    {
        private IUserRepository _IUserRepository;

        public User(IUserRepository _IUserRepository)
        {
            this._IUserRepository = _IUserRepository;
        }

        public ApiResult DeleteUser(int userId)
        {
            try
            {
                var Response = _IUserRepository.DeleteUser(userId);
                return ResultSet.SuccessResponse(ResponceCodes.Success.DeleteUser, Response, ManageEnum.status.success.ToString(), ResponceMessages.Success.DeleteUser);


            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetUserList(string search, string sortBy, int loggedInUserId)
        {
            try
            {
               var Response = _IUserRepository.GetUserList(search,sortBy,loggedInUserId);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, Response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
                
                
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetUserDetail(int userId, string lang = "")
        {
            try
            {
                var Response = _IUserRepository.GetUserDetail(userId);
                return ResultSet.SuccessResponse(ResponceCodes.Success.RetrieveUser, Response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);


            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult UpdateUser(UpdateUserModel model)
        {
            try
            {
                var Response = _IUserRepository.UpdateUser(model);
                return ResultSet.SuccessResponse(ResponceCodes.Success.UpdateUser, Response, ManageEnum.status.success.ToString(), ResponceMessages.Success.UpdateUser);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult CheckAllowNewUser(string totalUser,int insetedRow)
        {
            try
            {
                if(!string.IsNullOrEmpty(totalUser))
                {

                    if(insetedRow >= Convert.ToInt32(totalUser))
                    {
                        return ResultSet.SuccessResponse(ResponceCodes.Success.NotAllowNewUser, totalUser, ManageEnum.status.success.ToString(), ResponceMessages.Success.NotAllowNewUser);
                    }
                    else
                    {
                        return ResultSet.SuccessResponse(ResponceCodes.Success.AllowNewUser, totalUser, ManageEnum.status.success.ToString(), ResponceMessages.Success.AllowNewUser);
                    }                    
                }
                else
                {
                    return ResultSet.SuccessResponse("", totalUser, ManageEnum.status.success.ToString(), "");
                }
                
                
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
    }
}
