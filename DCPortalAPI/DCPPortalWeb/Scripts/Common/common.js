﻿var common = {
    //Generic function: add coma(,) or dot(.) according to culture
    addGroupSeparator: function (nStr, GroupSeparator, DecimalSeparator) {
        nStr += '';
        x = nStr.split('.');//first split with dot(.)
        x1 = x[0];
        x2 = x.length > 1 ? DecimalSeparator + x[1] : DecimalSeparator + '00';//insert decimal separator
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + GroupSeparator + '$2');//inser group separator
        }
        return x1 + x2;
    },
    //Generic function: converts to normal number for client side calculation
    removeGroupSeparator: function (nStr, GroupSeparator, DecimalSeparator) {
        GroupSeparator = '\\' + GroupSeparator;
        DecimalSeparator = '\\' + DecimalSeparator;
        nStr = nStr.replace(new RegExp(GroupSeparator, 'g'), '');//remove all group separator
        nStr = nStr.replace(new RegExp(DecimalSeparator, 'g'), '.');//replace decimal separator with dot(.)
        return nStr;
    },
    // Generic function: Add thousand separator for decimal value
    addThousandsSeparator: function (nStr, groupSeparator, decimalSeparator) {
        nStr += '';
        x = nStr.split(decimalSeparator);
        x1 = x[0];
        x2 = x.length > 1 ? decimalSeparator + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            //x1 = x1.replace(rgx, '$1' + ',' + '$2');
            x1 = x1.replace(rgx, '$1' + groupSeparator + '$2');
        }
        return x1 + x2;
    },
    displayMessage: function (msg, IsSucceed) {

        bootoast.toast({
            message: (msg != null && msg != "") ? msg : '&nbsp;',
            type: IsSucceed ? 'success' : 'danger',
           // icon: IsSucceed ? 'ok-sign' : 'warning-sign',
            timeout: 3,
            timeoutProgress: false,
            animationDuration: 500,
            dismissable: true
        });

        if (IsSucceed == true) {
            $(".bootoast .glyphicon").removeClass("glyphicon-ok-sign");
            $(".bootoast .glyphicon").html("<img src='/Images/success2.png' style='margin-top:-12px' />");    
        }
        else {
            $(".bootoast .glyphicon").removeClass("glyphicon-remove-sign");
            $(".bootoast .glyphicon").html("<img src='/Images/alert2.png' style='margin-top:-10px' />");  
        }
        
    },
    displayLoader: function () {
        $('body').append($('<div/>', {
            class: 'lmask'
        }));
    },
    hideLoader: function () {
        $(".lmask").remove();
    },

    /* Auto hide messages */
    closeMessage: function (msgId, divId) {
        $(msgId).removeClass();
        $(divId).removeClass();
        $(divId).toggle(false);
    },
    bindDatePicker: function (controlId) {
        $(controlId).datepicker({
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy',
            changeMonth: true,
            changeYear: true
           
        });

      
        $(controlId).datepicker('setDate', common.getDateFromPicker($(controlId).val()));

        //Disable paste for datetime textbox for detail tab
        $(controlId).bind("paste", function (e) {
            e.preventDefault();
        });

        //Disable key press event for datetime 
        $(controlId).keypress(function (event) { event.preventDefault(); });
    },
    bindDatePickerStartWithCurrentDate: function (controlId) {
        $(controlId).datepicker({
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy',
            changeMonth: true,
            changeYear: true,
            minDate: new Date()
        });

        $(controlId).datepicker('setDate', common.getDateFromPicker($(controlId).val()));

        //Disable paste for datetime textbox for detail tab
        $(controlId).bind("paste", function (e) {
            e.preventDefault();
        });

        //Disable key press event for datetime 
        $(controlId).keypress(function (event) { event.preventDefault(); });
    },
    getDateFromPicker: function (dateValueText) {
        if (typeof dateValueText !== "undefined") {
            var dateValueTextParts = dateValueText.split(" ");
            var dateValueTextDatePart = dateValueTextParts[0].split(".");

            var datePart = new Date(dateValueTextDatePart[2], (dateValueTextDatePart[1] - 1), dateValueTextDatePart[0]);
            let hoursDiff = datePart.getHours() - datePart.getTimezoneOffset() / 60;
            let minutesDiff = (datePart.getHours() - datePart.getTimezoneOffset()) % 60;
            datePart.setHours(hoursDiff);
            datePart.setMinutes(minutesDiff);
            return datePart;
        }
    },
    addtimewithdate: function (dateValueText, time) {
      
        if (typeof dateValueText !== "undefined") {

            var dateValueTextParts = dateValueText.split(" ");
            var dateValueTextDatePart = dateValueTextParts[0].split(".");

            var datePart = new Date(dateValueTextDatePart[2], (dateValueTextDatePart[1] - 1), dateValueTextDatePart[0]);

            var timeParts = time.split(":");
          
            if (timeParts != undefined && timeParts != null) {

                let hoursDiff = datePart.getHours() - datePart.getTimezoneOffset() / 60 + parseInt(timeParts[0]);
                let minutesDiff = (datePart.getHours() - datePart.getTimezoneOffset()) % 60 + parseInt(timeParts[1]);

                datePart.setHours(hoursDiff);
                datePart.setMinutes(minutesDiff);

            } else {

                let hoursDiff = datePart.getHours() - datePart.getTimezoneOffset() / 60;
                let minutesDiff = (datePart.getHours() - datePart.getTimezoneOffset()) % 60;

                datePart.setHours(hoursDiff);
                datePart.setMinutes(minutesDiff);
            }

            return datePart;
        }
    }
};
