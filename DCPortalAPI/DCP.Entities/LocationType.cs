﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class LocationType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [ForeignKey("Locations")]
        public int dcp_locationinfo_id { get; set; }
        [Required]
        [ForeignKey("LocationTypes")]
        public int dcp_locationtype_id { get; set; }

        public Locations Locations { get; set; }
        public List_LocationType LocationTypes { get; set; }
    }
}
