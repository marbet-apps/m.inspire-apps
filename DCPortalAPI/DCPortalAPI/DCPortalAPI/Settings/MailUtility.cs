﻿using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

namespace DCPortalAPI.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public class MailUtility
    {


        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isBodyHtml"></param>
        /// <param name="cc"></param>
        /// <param name="bcc"></param>        
        /// <returns></returns>
        public static bool SendMail(string email, string subject, string body, bool isBodyHtml, string cc = "", string bcc = "", bool islogoInheader = false)
        {

            try
            {
                WriteLog.LogInformation(LogMessages.SendMailStarted);

                WriteLog.LogInformation("Mail Parameter => FromMail : " + Constants.FromMail + ", ToMail : " + email + ", CC : " + cc + ", BCC : " + bcc);

                SmtpSection smtpSec = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

                var mailMessage = new System.Net.Mail.MailMessage();
                foreach (var address in email.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mailMessage.To.Add(address);
                }

                foreach (var address in cc.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mailMessage.CC.Add(address);
                }

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);

                if (islogoInheader == true)
                {
                    LinkedResource res = new LinkedResource(HttpContext.Current.Server.MapPath("\\Images\\logo_header.png"));
                    res.ContentId = "headerimg";                  
                    htmlView.LinkedResources.Add(res);
                    mailMessage.AlternateViews.Add(htmlView);
                }

                mailMessage.From = new System.Net.Mail.MailAddress(smtpSec.From != null ? smtpSec.From : string.Empty);
                mailMessage.Body = body;
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = isBodyHtml;

               

               

                var client = new System.Net.Mail.SmtpClient();

                client.Host = smtpSec.Network.Host;
                client.Port = smtpSec.Network.Port;

                client.Send(mailMessage);
                
                //System.Threading.Tasks.Task t = System.Threading.Tasks.Task.Run(async () =>
                //{
                //    using (var client = new System.Net.Mail.SmtpClient())
                //    {
                //        var message = new System.Net.Mail.MailMessage();

                //        message.From = new System.Net.Mail.MailAddress(smtpSec.From != null ? smtpSec.From : string.Empty);

                //        foreach (var address in email.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                //        {
                //            message.To.Add(address);
                //        }

                //        if (!string.IsNullOrEmpty(cc))
                //        {
                //            foreach (var cc1 in cc.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                //            {
                //                message.CC.Add(cc1);
                //            }

                //        }
                //AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);
                //if (islogoInheader==true)
                //{
                //    LinkedResource res = new LinkedResource(HttpContext.Current.Server.MapPath("\\Images\\logo_header.png"));
                //    res.ContentId = "headerimg";
                //    htmlView.LinkedResources.Add(res);
                //}

                //body = body.Replace("filename", res.ContentId);

                //        if (!string.IsNullOrEmpty(bcc))
                //        {
                //            foreach (var bcc1 in bcc.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                //            {
                //                message.Bcc.Add(bcc1);
                //            }

                //        }

                //        message.Subject = subject;

                //        message.IsBodyHtml = true;

                //        string html = body;

                //        System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(html, new System.Net.Mime.ContentType("text/html;charset=UTF-8"));

                //        message.AlternateViews.Add(htmlView);

                //        await client.SendMailAsync(message);
                //    }
                //});

                WriteLog.LogInformation(LogMessages.SendMailEnded);
				return true;
            }
            catch (Exception ex)
            {
                WriteLog.LogInformation("Failed to send email with this error:" + ex);
                return false;
            }

        }

        public static bool SendRegistrationMail(string lang, string First_Name, string Last_Name, string Email, string Password, string strId, string requestDateTime)
        {
            WriteLog.LogInformation(LogMessages.SendRegistrationMailStarted);
            bool flag = true;
            try
            {

                string emailTemplate = "";

                string msgBody = "";

                emailTemplate = lang.ToLower() == "en-US".ToLower() ? EmailTemplate.NewUserRegistration_En : EmailTemplate.NewUserRegistration_DE;
                emailTemplate = System.Web.HttpRuntime.AppDomainAppPath + emailTemplate;
                using (StreamReader reader = new StreamReader(emailTemplate))
                {
                    msgBody = reader.ReadToEnd();
                }

                msgBody = msgBody.Replace("$$Name$$", First_Name + " " + Last_Name);
                //msgBody = msgBody.Replace("$$UserName$$", Email);
                //msgBody = msgBody.Replace("$$Password$$", Password);

                string link = Constants.WebsiteLink + "/login/ResetPassword?in_d=" + strId + "&in_t=" + requestDateTime + "&in_a=" + Crypto.EnryptString("true");

                msgBody = msgBody.Replace("$$Url$$", link);

                msgBody = msgBody.Replace("$$Link$$", link);

                string subject = lang.ToLower() == "en-US".ToLower() ? EmailSubject.NewUserRegistrationSubject_EN : EmailSubject.NewUserRegistrationSubject_DE;
                flag = MailUtility.SendMail(Email, subject, msgBody, true, "", "", true);
                return flag;

            }
            catch (System.Exception ex)
            {
                flag = false;
                WriteLog.LogInformation("Error Generate :" + ex);
            }
            WriteLog.LogInformation(LogMessages.SendRegistrationMailEnded);

            return flag;
        }

        public static bool SendForgotPasswordMail(string lang, string Name, string Email, string Password, string strId, string requestDateTime)
        {
            WriteLog.LogInformation(LogMessages.SendRegistrationMailStarted);
            bool flag = true;
            try
            {

                string emailTemplate = "";

                string msgBody = "";

                emailTemplate = lang.ToLower() == "en-US".ToLower() ? EmailTemplate.ForgotPassword_En : EmailTemplate.ForgotPassword_DE;
                emailTemplate = System.Web.HttpRuntime.AppDomainAppPath + emailTemplate;
                using (StreamReader reader = new StreamReader(emailTemplate))
                {
                    msgBody = reader.ReadToEnd();
                }

                string link = Constants.WebsiteLink + "/login/ResetPassword?in_d=" + strId + "&in_t=" + requestDateTime;

                msgBody = msgBody.Replace("$$Name$$", Name);
                //msgBody = msgBody.Replace("$$UserName$$", Email);
                //msgBody = msgBody.Replace("$$Password$$", Password);
                msgBody = msgBody.Replace("$$Link$$", link);
                msgBody = msgBody.Replace("$$Url$$", link);


                string subject = lang.ToLower() == "en-US".ToLower() ? EmailSubject.ForgotPasswordSubject_En : EmailSubject.ForgotPasswordSubject_DE;



                flag = MailUtility.SendMail(Email, subject, msgBody, true, "", "", true);
                return flag;

            }
            catch (System.Exception ex)
            {
                flag = false;
                WriteLog.LogInformation("Error Generate :" + ex);
            }
            WriteLog.LogInformation(LogMessages.SendRegistrationMailEnded);

            return flag;
        }

        public static bool SendPasswordGeneratedMail(string lang, string Name, string Email)
        {
            WriteLog.LogInformation(LogMessages.SendRegistrationMailStarted);
            bool flag = true;
            try
            {

                string emailTemplate = "";

                string msgBody = "";

                emailTemplate = lang.ToLower() == "en-US".ToLower() ? EmailTemplate.PasswordGenerated_En : EmailTemplate.PasswordGenerated_DE;
                emailTemplate = System.Web.HttpRuntime.AppDomainAppPath + emailTemplate;
                using (StreamReader reader = new StreamReader(emailTemplate))
                {
                    msgBody = reader.ReadToEnd();
                }

                string link ="<a href='" + Constants.WebsiteLink + "/login'>"+ lang.ToLower() == "en-US".ToLower() ? "Login": "Anmeldung" + "</a>";

                msgBody = msgBody.Replace("$$Name$$", Name);               
                msgBody = msgBody.Replace("$$Login$$", link);
              


                string subject = lang.ToLower() == "en-US".ToLower() ? EmailSubject.PasswordGeneratedSubject_En : EmailSubject.PasswordGeneratedSubject_DE;



                flag = MailUtility.SendMail(Email, subject, msgBody, true, "", "", true);
                return flag;

            }
            catch (System.Exception ex)
            {
                flag = false;
                WriteLog.LogInformation("Error Generate :" + ex);
            }
            WriteLog.LogInformation(LogMessages.SendRegistrationMailEnded);

            return flag;
        }
    }
}