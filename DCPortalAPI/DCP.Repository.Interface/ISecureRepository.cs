﻿using DCP.Models;
using System.Collections.Generic;

namespace DCP.Repository.Interface
{
    public interface ISecureRepository
    {
        /// <summary>
        /// Check User Auth
        /// </summary>
        /// <returns></returns>
        ResponseUserLoginModel CheckUserAuth(string UserName,string Password);

        ResponseUserLoginModel CheckAdminUserAuth(string Email,string Password,int UserType);

        RegisterationModel RegisterUser(UserRegisterModel userRegisterModel);

        UserPasswordModel ForgotUserPassword(UserPasswordModel userPassword, out string name, string token);

        ResponseUserModel getAllUser(string activestatus ="", string email="",int? UserID =0, int page = 1, int pageSize = 50, string lang="");
        UserModel removeUser(int userID);

        int ChangePassword(ChangePasswordModel userModel);
        ResponseModel UpdateUser(int userID, int loggedInUserID, UpdateUserModel updateUserModel);

        UpdateProfileResponseModel UpdateUserProfile(UpdateProfileModel model);

        bool UploadImage(byte[] bytes, int userId);

        bool DeleteImage(int userId);

        UserModel ResetPassword(ResetPasswordModel userModel, bool isValidateUserForActive);

        UserModel ValidateUser(ResetPasswordModel userModel, bool isValidateUserForActive);
    }
}
