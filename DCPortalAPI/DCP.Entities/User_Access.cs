﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class User_Access
    {
        [Key]
        public int User_Access_Id { get; set; }

        [ForeignKey("User_Master")]
        public int User_Id { get; set; }

        public bool? Hotel { get; set; }

        public bool? Location { get; set; }

        public bool? Restaurant { get; set; }

        public bool? GoogleMap { get; set; }
        public bool? UserMgmt { get; set; }
        public bool? RFPTemplate { get; set; }

        public bool? NearBy { get; set; }

        public User_Master User_Master { get; set; }

        public int? Created_By { get; set; }
        public DateTime? Created_Date { get; set; }
        public int? Modified_By { get; set; }
        public DateTime? Modify_Date { get; set; }
        
    }
}
