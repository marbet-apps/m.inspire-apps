﻿using System.Collections.Generic;

namespace DCP.Repository.Interface
{
    public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// Get All the table data
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Get single row of selected id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(object id);

        /// <summary>
        /// Insert single objects of table data
        /// </summary>
        /// <param name="obj"></param>
        void Insert(T obj);

        /// <summary>
        /// Update single object of table data
        /// </summary>
        /// <param name="obj"></param>
        void Update(T obj);

        /// <summary>
        /// Delete single object by id
        /// </summary>
        /// <param name="id"></param>
        void Delete(object id);

        /// <summary>
        /// Save data
        /// </summary>
        void Save();
    }
}
