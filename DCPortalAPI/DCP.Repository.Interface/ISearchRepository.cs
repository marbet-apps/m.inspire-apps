﻿using DCP.Models;
using System.Collections.Generic;

namespace DCP.Repository.Interface
{
    public interface ISearchRepository
    {
        /// <summary>
        /// Resources List
        /// </summary>
        /// <returns></returns>
        BaseResourceModel GetResourcesList(FilterModel SearchModel, string lang, int? UserId);

        ///// <summary>
        ///// Hotel Data
        ///// </summary>
        ///// <returns></returns>
        //HotelModel GetHotelById(int Id, string lang);

        ///// <summary>
        ///// Restaurant Data
        ///// </summary>
        ///// <returns></returns>
        //RestaurantModel GetRestaurantById(int Id, string lang);

        ///// <summary>
        ///// Location Data
        ///// </summary>
        ///// <returns></returns>
        //LocationModel GetLocationById(int Id, string lang);

        /// <summary>
        /// Image Data
        /// </summary>
        /// <returns></returns>
        List<PhotosModel> GetImageData(int id, string type);

        /// <summary>
        /// Get Near By Restaurant By Id
        /// </summary>
        List<NearByModel> GetNearByRestaurantList(string lat, string lng, int? cid, string type, int distance);

        /// <summary>
        /// Get Near By Hotel By Id
        /// </summary>
        List<NearByModel> GetNearByHotelList(string lat, string lng, int? cid, string type, int distance);

        /// <summary>
        /// Get Near By Location By Id
        /// </summary>
        List<NearByModel> GetNearByLocationList(string lat, string lng, int? cid, string type, int distance);

        /// <summary>
        /// Get Hotel By Id
        /// </summary>
        HotelModel GetHotelInfo(int id, string Lang, int UserId);

        /// <summary>
        /// Get Restaurant By Id
        /// </summary>
        RestaurantModel GetRestaurantInfo(int id, string Lang, int UserId);

        /// <summary>
        /// Save search history
        /// </summary>
        void SaveSearchHistory(string searchCriteria, int userId);

        /// <summary>
        /// Get search criteria by userid
        /// </summary>
        string GetSearchCriteria(int userId);

        /// <summary>
        /// Get Location By Id
        /// </summary>
        LocationModel GetLocationInfo(int id, string Lang, int UserId);

        /// <summary>
        /// RFP Resources List
        /// </summary>
        /// <returns></returns>
        BaseResourceModel GetRFPResourceList(string Rids, string type, string lang);

        /// <summary>
        /// Cart Resources List
        /// </summary>
        /// <returns></returns>
        BaseResourceModel GetCartList(int UserId, string lang);

        /// <summary>
        /// Cart Resources Count
        /// </summary>
        /// <returns></returns>
        BaseResourceModel GetCartCount(int UserId, string lang);
    }
}
