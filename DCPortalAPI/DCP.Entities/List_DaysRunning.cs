﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class List_DaysRunning
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(50), Column(TypeName = "nvarchar")]
        public string days_running { get; set; }

        [Required, MaxLength(50), Column(TypeName = "nvarchar")]
        public string days_running_de { get; set; }

        [Required, MaxLength(50), Column(TypeName = "varchar")]
        public string mice_days_running { get; set; }
    }
}
