﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace DCP.Mice.Models
{
    public class RequestModel
    {
        /// <summary>
        /// A list of hotelids which will be requested
        /// </summary>
        [JsonProperty("hotelids")]
        public List<int> HotelIds { get; set; }

        /// <summary>
        /// since 2.2.0 Use "events" for non standard requests
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Reference to an existing user in MICE access, mandatory if no Customer object is shipped.
        /// </summary>
        [JsonProperty("userid")]
        public int UserId { get; set; }

        /// <summary>
        /// Customer object for creating a new customer and user in MICE access, mandatory if no userid value is shipped.
        /// </summary>
        [JsonProperty("customer")]
        public CustomerModel Customer { get; set; }

        /// <summary>
        /// A list of RequestItem specifiying the concret request.
        /// </summary>
        [JsonProperty("requestItems")]
        public List<RequestItem> RequestItems { get; set; }

        /// <summary>
        /// An additional comment shipped to hotels. since 2.2.0 used for Anmerkungen zur Technik in case of type "event"
        /// </summary>
        [JsonProperty("comments")]
        public string Comments { get; set; }

        /// <summary>
        /// since 2.2.0 used for Anmerkungen zur Verpflegung in case of type "event"
        /// </summary>
        [JsonProperty("comments2")]
        public string Comments2 { get; set; }

        /// <summary>
        /// since 2.2.0 used for sonstige Anmerkungen in case of type "event"
        /// </summary>
        [JsonProperty("comments3")]
        public string Comments3 { get; set; }

        /// <summary>
        /// since 2.2.0 used for Allgemeine Angaben in case of type "event"
        /// </summary>
        [JsonProperty("comments4")]
        public string Comments4 { get; set; }

        /// <summary>
        /// MICE access cityid.
        /// </summary>
        [JsonProperty("cityid")]
        [JsonIgnore]
        public int CityId { get; set; }

        /// <summary>
        /// MICE access placeid.
        /// </summary>
        [JsonProperty("placeid")]
        [JsonIgnore]
        public int PlaceId { get; set; }

        /// <summary>
        /// MICE access countryid.
        /// </summary>
        [JsonProperty("countryid")]
        [JsonIgnore]
        public int CountryId { get; set; }

        /// <summary>
        /// since 2.2.3. A Briefing File, base64 encoded PDF File
        /// </summary>
        [JsonProperty("attachment")]
        [JsonIgnore]
        public string Attachment { get; set; }

        /// <summary>
        /// since 2.2.6. A number between 1 and 30 days. 
        /// Default we are setting as follows:
        /// Regarding to start of event:
        /// ============================
        ///     1 > 5 days	1 day
        ///     6 - 10 days	4 days
        ///     11 - 30 days	10 days
        ///     31 - 60 days	14 days
        ///     61 - 90 days	20 days
        ///     90+	30 days
        /// </summary>
        [JsonProperty("daysRunning")]
        public int DaysRunning { get; set; }
    }

    public class ResponseRequestConfirmationModel
    {
        /// <summary>
        /// Uniq id of the created request.
        /// </summary>
        [JsonProperty("requestid")]
        public int RequestId { get; set; }

        /// <summary>
        /// Userid of new user if a new customer is requested in MICE access.
        /// </summary>
        [JsonProperty("userid")]
        public int? UserId { get; set; }

        /// <summary>
        /// Username of new user if a new customer is requested in MICE access.
        /// </summary>
        [JsonProperty("username")]
        public string UserName { get; set; }

        /// <summary>
        /// Password created for new user if a new customer is requested in.
        /// </summary>
        [JsonProperty("password")]
        public string Password { get; set; }
    }

    public class RequestItem
    {
        /// <summary>
        /// The specification of request item: Accommodation, ConferenceRoom or GroupRoom
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Quantity of request item, i.e. number of Rooms in Accommodation or participants in ConferenceRoom and GroupRoom.
        /// </summary>
        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonIgnore]
        public DateTime ReserveFromDate { get; set; }

        /// <summary>
        /// Starting date and time of a request item, i.e. arrival date in Accommodation or event day with approx. start and end in ConferenceRoom and GroupRoom.
        /// </summary>
        [JsonProperty("reserve_from_time")]
        public string ReserverFromDateISO8601
        {
            get
            {
                return ReserveFromDate.ToString("yyyy-MM-dd'T'HH:mm:ss.fffK", CultureInfo.InvariantCulture);
            }
        }

        [JsonIgnore]
        public DateTime ReserveToDate { get; set; }

        /// <summary>
        /// Starting date and time of a request item, i.e. arrival date in Accommodation or event day with approx. start and end in ConferenceRoom and GroupRoom.
        /// </summary>
        [JsonProperty("reserve_to_time")]
        public string ReserveToDateISO8601
        {
            get
            {
                return ReserveToDate.ToString("yyyy-MM-dd'T'HH:mm:ss.fffK", CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Desired budget of that request item.
        /// </summary>
        [JsonProperty("budget")]
        public double Budget { get; set; }

        /// <summary>
        /// An additional comment shipped to hotels.
        /// </summary>
        [JsonProperty("comments")]
        public string Comments { get; set; }

        /// <summary>
        /// Desired roomtype. choice['room_single_bed'|'room_double_bed'|'room_double_bed_single_use'|'room_three_bed'|'room_four_bed'|'room_twin'|'room_junior_suite'|'room_suite']
        /// </summary>
        [JsonProperty("roomtype")]
        public string RoomType { get; set; }

        /// <summary>
        /// The disired type of seating. choice['rows'|'circle'|'tables'|'u'|'parliamentary'|'block'|'no_seating'|'banquet'|'cabaret']
        /// </summary>
        [JsonProperty("seating")]
        public string SeatingType { get; set; }

        /// <summary>
        /// An object of type RoomTechnique, see RoomTechnique
        /// </summary>
        [JsonProperty("technique")]
        public RoomTechnique Technique { get; set; }

        /// <summary>
        /// An object of type ConferenceFood, see ConferenceFood
        /// </summary>
        [JsonProperty("food")]
        public ConferenceFood Food { get; set; }

        /// <summary>
        /// The disired type of Room size at least in squaremeters
        /// </summary>
        [JsonProperty("size")]
        public double RoomSize { get; set; }
    }

    public class RoomTechnique
    {
        [JsonProperty("pens_and_notepads")]
        public bool PensAndNotepads { get; set; }

        [JsonProperty("canvas")]
        public int? Canvas { get; set; }

        [JsonProperty("overhead_projector")]
        public int? OverHeadProjector { get; set; }

        [JsonProperty("flipchart")]
        public int? FlipChart { get; set; }

        [JsonProperty("wall")]
        public int? Wall { get; set; }

        [JsonProperty("presentation_case")]
        public int? PresentationCase { get; set; }

        [JsonProperty("beamer")]
        public int? Beamer { get; set; }

        [JsonProperty("isdn")]
        public int? ISDN { get; set; }

        [JsonProperty("internet")]
        public int? Internet { get; set; }

        [JsonProperty("microphone")]
        public int? Microphone { get; set; }

        [JsonProperty("wireless_microphone")]
        public int? WirelessMicrophone { get; set; }

        [JsonProperty("sound_system")]
        public int? SoundSystem { get; set; }

        [JsonProperty("rostrum")]
        public int? Rostrum { get; set; }
    }

    public class ConferenceFood
    {
        [JsonProperty("morning_two_drinks")]
        public bool MorningTwoDrinks { get; set; }

        [JsonProperty("morning_drinks_flat")]
        public bool MorningDrinksFlat { get; set; }

        [JsonProperty("morning_coffee_and_tea")]
        public bool MorningCoffeeAndTea { get; set; }

        [JsonProperty("morning_welcome_coffee_and_tea")]
        public bool MorningWelcomeCoffeeAndTea { get; set; }

        [JsonProperty("morning_coffee_and_tea_break")]
        public bool MorningCoffeeAndTeaBreak { get; set; }

        [JsonProperty("midday_lunch_buffet")]
        public bool MiddayLunchBuffet { get; set; }

        [JsonProperty("midday_lunch_one_dish")]
        public bool MiddayLunchOneDish { get; set; }

        [JsonProperty("midday_lunch_two_course_meal")]
        public bool MiddayLunchTwoCourseMeal { get; set; }

        [JsonProperty("midday_lunch_three_course_meal")]
        public bool MiddayLunchThreeCourseMeal { get; set; }

        [JsonProperty("midday_lunch_snack")]
        public bool MiddayLunchSnack { get; set; }

        [JsonProperty("midday_one_drink")]
        public bool MiddayOneDrink { get; set; }

        [JsonProperty("midday_drinks_flat")]
        public bool MiddayDrinksFlat { get; set; }

        [JsonProperty("midday_coffee_and_tea_break")]
        public bool MiddayCoffeeAndTeaBreak { get; set; }

        [JsonProperty("second_coffebreak")]
        public bool SecondCoffeeBreak { get; set; }

        [JsonProperty("evening_dinner_buffet")]
        public bool EveningDinnerBuffet { get; set; }

        [JsonProperty("evening_dinner_one_dish")]
        public bool EveningDinnerOneDish { get; set; }

        [JsonProperty("evening_dinner_two_course_meal")]
        public bool EveningDinnerTwoCourseMeal { get; set; }

        [JsonProperty("evening_dinner_three_course_meal")]
        public bool EveningDinnerThreeCourseMeal { get; set; }

        [JsonProperty("evening_dinner_snack")]
        public bool EveningDinnerSnack { get; set; }

        [JsonProperty("gala_dinner")]
        public bool GalaDinner { get; set; }

        [JsonProperty("evening_one_drink")]
        public bool EveningOneDrink { get; set; }

        [JsonProperty("evening_drinks_flat")]
        public bool EveningDrinkFlat { get; set; }
    }
}
