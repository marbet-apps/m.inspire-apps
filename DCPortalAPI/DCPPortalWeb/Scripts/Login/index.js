﻿    var $c = {
    txtemail: "#txtemail",
    txtpassword: '#txtpassword',
    btnlogin: '#btnlogin',
    hdnAPIBaseURL: '#hdnAPIBaseURL',
    btnSend: "#btnSend",
    txtEmailForForgotPwd: "#txtEmailForForgotPwd"
};

var $u = {
    loginURL: "Secure/Login",
    forgotPassword: "Secure/ForgotPassword"
   
};

var $Login = {

    validateform: function () {

        var flag = true;
        var email = $($c.txtemail).val();
        var password = $($c.txtpassword).val();

        if (email == null || email == undefined || email == "") {
            $($c.txtemail).addClass("Error");
            flag = false;
        } else {
            $($c.txtemail).removeClass("Error");
        }

        if (password == undefined || password == null || password == "") {
            $($c.txtpassword).addClass("Error");
            flag = false;
        } else {
            $($c.txtpassword).removeClass("Error");
        }

        return flag;
    },
    Authorize: function () {
        
        common.displayLoader();

        var login = {
            Email: $($c.txtemail).val(),
            Password: $($c.txtpassword).val()
        };

        $.ajax({
            type: "POST",
            url: $($c.hdnAPIBaseURL).val() + $u.loginURL,
            data: JSON.stringify(login),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {
                

                if (data != undefined && data != null && data.code == "705") {

                    var token = response.getResponseHeader('authorization');
                    var datas = data.data;

                    ajaxRequest.setAuthorizationToken(JSON.stringify(token));
                    ajaxRequest.setCurrentUser(JSON.stringify(datas));
                   // Start User photo changes  15 april
                    ajaxRequest.setCurrentUserPhoto(datas.Photo);
                    

                    location.href = "/home";

                } else {
                    $managecode.Decision(false, "", data.code, data.message, "Login", false);
                }

            },
            failure: function (data) {
                
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

    },

    forgotPassword: function () {

        common.displayLoader();

        var login = {
            Email: $($c.txtEmailForForgotPwd).val(),
            Password: ''
        };

        $.ajax({
            type: "PUT",
            url: $($c.hdnAPIBaseURL).val() + $u.forgotPassword,
            data: JSON.stringify(login),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {
                
                if (data != undefined && data != null && data.code == "707") {

                    $managecode.Decision(false, "", data.code, data.message, "Login", true);

                    $('.register-info-box').fadeIn();
                    $('.login-info-box').fadeOut();

                    $('.white-panel').removeClass('right-log');

                    $('.login-show').addClass('show-log-panel');
                    $('.register-show').removeClass('show-log-panel');
                    $($c.txtEmailForForgotPwd).val("");

                } else {
                    $managecode.Decision(false, "", data.code, data.message, "Login", false);
                    return false;
                }

            },
            failure: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    }

};

$(document).ready(function () {

    $('.login-info-box').fadeOut();
    $('.login-show').addClass('show-log-panel');

    window.localStorage.removeItem(ajaxRequest.variables.authorizationTokenKey);
    window.localStorage.removeItem(ajaxRequest.variables.CurrentUser);

    $($c.btnlogin).click(function () {

        if ($Login.validateform()) {
            $Login.Authorize();
        }

    });

    
    $($c.btnSend).click(function () {
        var flag = true;
        var email = $($c.txtEmailForForgotPwd).val();
       
        if (email == null || email == undefined || email == "") {
            $($c.txtEmailForForgotPwd).addClass("Error");
            flag = false;
        } else {
            $($c.txtEmailForForgotPwd).removeClass("Error");
        }

        if (flag == true) {
            $Login.forgotPassword();
        }
    });

    $(document).on("keypress", function (e) {
        if (e.which == 13) {
            $($c.btnlogin).click();
        }
    });

});

$('.login-reg-panel input[type="radio"]').on('click', function () {
    if ($('#log-login-show').is(':checked')) {
        $('.register-info-box').fadeOut();
        $('.login-info-box').fadeIn();

        $('.white-panel').addClass('right-log');
        $('.register-show').addClass('show-log-panel');
        $('.login-show').removeClass('show-log-panel');

    }
    else if ($('#log-reg-show').is(':checked')) {
        $('.register-info-box').fadeIn();
        $('.login-info-box').fadeOut();

        $('.white-panel').removeClass('right-log');

        $('.login-show').addClass('show-log-panel');
        $('.register-show').removeClass('show-log-panel');
    }
});

$.ajaxSetup({
    beforeSend: function (xhr) {

       
        var lang = $.cookie("Language");
       
        if (lang === undefined || lang === null || lang === "") {
            lang = "en-US";
        }
        xhr.setRequestHeader("Lang", lang);
   
    },
    error: function (jqXHR, textStatus, errorThrown) {

        $managecode.Decision(false, "", jqXHR.status, textStatus + ": " + errorThrown, "error", false);
    }
});