﻿using Swashbuckle.Swagger;
using System.Collections.Generic;
using System.Web.Http.Description;

namespace DCPortalAPI.Filter
{
    /// <summary>
    /// API Request Header Parameter
    /// </summary>
    public class HeaderParameterFilter : IOperationFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="schemaRegistry"></param>
        /// <param name="apiDescription"></param>
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters == null)
                operation.parameters = new List<Parameter>();

            operation.parameters.Add(new Parameter
            {
                name = "Authorization",
                @in = "header",
                type = "apiKey",
                description = "Authentication Token.",
                required = false
            });

            operation.parameters.Add(new Parameter
            {
                name = "userId",
                @in = "header",
                type = "string",
                description = "userId.",
                required = false
            });

            operation.parameters.Add(new Parameter
            {
                name = "localization",
                @in = "header",
                type = "string",
                description = "localization.",
                required = false
            });
        }
    }
}