﻿
var $info = {
    loadDetailImages: function (id, t) {

        var jasonData = { id: id, type: t };
        var imageControl = "#imageGallery";
        $.ajax({
            url: $bind.URL($u.path.loadimgdata),
            data: jasonData,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (result) {

                if (result.code != 704 && result.code != "704") {
                    $managecode.Decision(false, "", result.code, result.message, "Photos", false);
                }

                data = result.data;

                $(imageControl).empty();

                if (data != null && data.length > 0) {
                    data.forEach(function (item, indx) {
                        var img = "<a href=" + $bind.ImageURL(item.photo_name) + " class='img-fluid'> <img src='" + $bind.ImageURL(item.photo_name) + "' data-imgname='" + item.photo_name + "' class='img-strip'/></a>";
                        $(imageControl).append(img);
                        //console.log(id);
                    });

                    try {

                        $(imageControl + ' a').tosrus({
                            buttons: true,
                            pagination: {
                                add: true,
                                type: 'thumbnails'
                            }
                        });

                        var tos = $(imageControl).tosrus({
                            slides: {
                                visible: 1
                            }
                        });
                    } catch (e) { console.log(e); }
                    

                    $(imageControl).removeClass("displaynone");

                }

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

    },
    GetInfo: function (id, type) {
        var jasonData = { id: id };

        var URL = $u.path.GetHotelInfo;
        var IURL = $u.path.IGetHotelInfo;

        if (type == "R") {

            URL = $u.path.GetRestaurantInfo;
            IURL = $u.path.IGetRestaurantInfo;

        } else if (type == "L") {

            URL = $u.path.GetLocationInfo;
            IURL = $u.path.IGetLocationInfo;

        }

        $.ajax({
            url: $bind.URL(URL),
            data: jasonData,
            type: "GET",
            async: true,
            cache: false,
            success: function (result) {
                var titlePrefix = "m.inspire ";
                if (result.code != 711 && result.code != "711") {
                    $managecode.Decision(false, "", result.code, result.message, "NearBy", false);
                }
                if (result.data) {
                    var data = result.data;
                    $info.loadDetailImages(id, type);
                    if (type == "H") {
                        $info.bindHotelDetail(data);

                        var title = $($c.controls.infoHotelName).text()                      
                        if ($($c.controls.infoHotelChain).text() != null && $($c.controls.infoHotelChain).text().trim() != "") {
                            title += $($c.controls.infoHotelChain).text();
                        }
                        title = titlePrefix + title;
                        $(document).attr("title", title);
                    } else if (type == "L") {
                        $info.bindLocationDetail(data);

                        var title = $($c.controls.infoLocName).text(); 
                        title = titlePrefix + title;
                        $(document).attr("title", title);

                    } else if (type == "R") {
                        $info.bindRestaurantDetail(data);
                       
                        var title = $($c.controls.infoRestName).text(); 
                        title = titlePrefix + title;
                        $(document).attr("title", title);
                    }
                    $info.bindModalButtons(data, type);
                }

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

    },
    bindHotelDetail: function (data) {

        var address = data.city;

        if (data.zipcode != undefined && data.zipcode != '' && data.zipcode != null) {
            address = address + ',' + data.zipcode;
        }

        if (data.country != undefined && data.country != '' && data.country != null) {
            address = address + ',' + data.country;
        }

        $($c.controls.infoHotelName).text(data.hotel_name);
        
        if (data.hotel_chain != null && data.hotel_chain.toString().trim() != "") {
            $($c.controls.infoHotelChain).text("" + data.hotel_chain + "");
        }
        var rating = "";
        if (data.star != "") {
            for (var i = 0; i < data.star; i++) {
                rating = rating + "<span class='glyphicon glyphicon-star'></span>";
            }
            $($c.controls.infoHotelRating).html(rating);
        }
        //if (data.street == null || data.street.toString().trim() == "") {
        //    $($c.controls.divinfoHotelAddress).addClass("d-none");
        //} else {
        //    $($c.controls.infoHotelAddr).text(data.street);
        //}

        $($c.controls.infoHotelAddr).text(address);

        if (data.webaddress == null || data.webaddress.toString().trim() == "") {
            $($c.controls.divinfoHotelsite).addClass("d-none");
        } else {
            $($c.controls.infoHotelWebsite + " a").attr("href", data.webaddress);
        }
        if ((data.airport_transfer_time == null || data.airport_transfer_time.toString().trim() == "0.0") && 
            (data.airport_distance == null || data.airport_distance.toString().trim() == "0")) {
            $($c.controls.divinfoHotelAirport).addClass("d-none");
        } else {
            $($c.controls.infoHotelAirportName).text(data.airport_name);
            $($c.controls.infoHotelAirportHours).text(data.hour);
            $($c.controls.infoHotelAirportMin).text(data.minute);
            $($c.controls.infoHotelAirportDistance).text(parseFloat(data.airport_distance).toFixed(1));
        }
        
        if (data.hotel_type != null && data.hotel_type != "") {
            var typeList = data.hotel_type.split(',');
            var ulList = "<div class='CONTENTLink'>" + $($c.controls.hdninfohoteltype).val() +"</div><hr><ul class='p0'>";
            typeList.forEach(function (item, index) {
                ulList += "<li class='badge'>" + item + "</li>";
            });
            ulList += "</ul>";
            $($c.controls.infoHotelType).html(ulList);
        } else {
            $('.uiList').addClass('d-none');
        }

        $($c.controls.infoHotelNoOfRoom).text(data.total_rooms);
        $($c.controls.infoHotelNoOfConfRoom).text(data.total_conference);
        $($c.controls.infoHotelSizeOfConfRoom).text(data.size_of_largest_conference.toFixed(2));
        $($c.controls.infoHotelParkingSpace).text(data.parking_spaces);
        $($c.controls.infoHotelMiscInfo).text(data.miscellaneous_info);
        if (data.miscellaneous_info !=null && data.miscellaneous_info.trim().length > 0) {
            $($ctrl.divAdditionalInfo).show();
            $($ctrl.hrAdditionalInfo).show();
        }
        else {
            $($ctrl.divAdditionalInfo).hide();
            $($ctrl.hrAdditionalInfo).hide();
        }
        
        if (data.green != null || data.green) {
            $($c.controls.infoHotelGreen).removeClass("d-none");
            $($c.controls.infoHotelGreen + ' img').attr('data-original-title', $($c.controls.sGreenTitle).val());
        }
        if (data.blacked != null || data.blacked) {
            $($c.controls.infoHotelBlacked).removeClass("d-none");
        }
        if (data.closed != null || data.closed) {
            $($c.controls.infoHotelClosed).removeClass("d-none");
        }
        
        if (data.reopening != null || data.reopening) {
            $($c.controls.infoHotelReOpen).removeClass("d-none");
            $($c.controls.infoHotelReOpen + ' img').attr('data-original-title', $($c.controls.sReopenTitle).val());
        }
    },  
    bindLocationDetail: function (data) {
        $($c.controls.infoLocName).text(data.location_name);

        if (data.street == null || data.street.toString().trim() == "") {
            $($c.controls.divinfoLocAddress).addClass("d-none");
        } else {
            $($c.controls.infoLocAddr).text(data.street);
        }
        if (data.webaddress == null || data.webaddress.toString().trim() == "") {
            $($c.controls.divinfoLocsite).addClass("d-none");
        } else {
            $($c.controls.infoLocWebsite + " a").attr("href", data.webaddress);
        }

        
        if (data.location_type != null && data.location_type != "") {
            var typeList = data.location_type.split(',');
            var ulList = "<div class='CONTENTLink'>" + $($c.controls.hdninfolocationtype).val() +"</div><hr><ul class='p0'>";
            typeList.forEach(function (item, index) {
                ulList += "<li class='badge'>"+ item +"</li>";
            });
            ulList += "</ul>";
            $($c.controls.infoLocType).html(ulList);
        }

        if (data.suitableforwhat != null && data.suitableforwhat != "") {
            var typeList = data.suitableforwhat.split(',');
            var ulList = "<div class='CONTENTLink'>" + $($c.controls.hdninfosuitable).val() +"</div><hr><ul class='p0'>";
            typeList.forEach(function (item, index) {
                ulList += "<li class='badge'>" + item + "</li>";
            });
            ulList += "</ul>";
            $($c.controls.infoLocSuitable).html(ulList);
        }

        

        $($c.controls.infoLocRoomHeight).text(data.room_height.toFixed(2));
        $($c.controls.infoLocNoOfConfRoom).text(data.total_conference);
        $($c.controls.infoLocSizeOfConfRoom).text(data.size_of_largest_conference.toFixed(2));
        $($c.controls.infoLocRentBegining).text(data.rent_begining.toFixed(2));

        $($c.controls.infoLocMiscInfo).text(data.miscellaneous_info);
        
        if (data.miscellaneous_info !=null && data.miscellaneous_info.trim().length > 0) {
            $($ctrl.divLocAdditionalInfo).show();
            $($ctrl.hrLocAdditionalInfo).show();
        }
        else {
            $($ctrl.divLocAdditionalInfo).hide();
            $($ctrl.hrLocAdditionalInfo).hide();
        }
        
        if (data.outdoor_possibility != null && data.outdoor_possibility == true && data.outdoor_details != null && data.outdoor_details.trim() != '') {        
            $($c.controls.infoLocOutdoorDetail).text(data.outdoor_details);
        } else {
            $($c.controls.divinfoLocOutdoorPossibility).addClass("d-none");
        }
        
        if (data.green != null || data.green) {
            $($c.controls.infoLocGreen).removeClass("d-none");
            $($c.controls.infoLocGreen + ' img').attr('data-original-title', $($c.controls.sGreenTitle).val());
        }
        if (data.blacked != null || data.blacked) {
            $($c.controls.infoLocBlacked).removeClass("d-none");
        }
        if (data.closed != null || data.closed) {
            $($c.controls.infoLocClosed).removeClass("d-none");
        }
        if (data.catering != null && data.catering==true) {
            $($c.controls.infoLocCatering).removeClass("d-none");
            $($c.controls.infoLocCatering + ' img').attr('data-original-title', $($c.controls.sCateringPartnerTitle).val());
        }
        if (data.technik != null && data.technik==true) {
            $($c.controls.infoLocTechnik).removeClass("d-none");
            $($c.controls.infoLocTechnik + ' img').attr('data-original-title', $($c.controls.sTechnicalPartnerTitle).val());
        }
        if (data.car_drive != null && data.car_drive==true) {
            $($c.controls.infoLocCarDrive).removeClass("d-none");
            $($c.controls.infoLocCarDrive + ' img').attr('data-original-title', $($c.controls.sAccessibleForCarsTitle).val());
        }
        if (data.reopening != null || data.reopening) {
            $($c.controls.infoLocReOpen).removeClass("d-none");
            $($c.controls.infoLocReOpen + ' img').attr('data-original-title', $($c.controls.sReopenTitle).val());
        }
    },
    bindRestaurantDetail: function (data) {
        $($c.controls.infoRestName).text(data.restaurant_name);
        if (data.street == null || data.street.toString().trim() == "") {
            $($c.controls.divinfoRestAddress).addClass("d-none");
        } else {
            $($c.controls.infoRestAddr).text(data.street);
        }
        if (data.webaddress == null || data.webaddress.toString().trim() == "") {
            $($c.controls.divinfoRestsite).addClass("d-none");
        } else {
            $($c.controls.infoRestWebsite + " a").attr("href", data.webaddress);
        }

        
        if (data.restaurant_type != null && data.restaurant_type != "") {
            var typeList = data.restaurant_type.split(',');
            var ulList = "<div class='CONTENTLink'>" + $($c.controls.hdninforestauranttype).val() +"</div><hr><ul class='p0'>";
            typeList.forEach(function (item, index) {
                ulList += "<li class='badge'>" + item + "</li>";
            });
            ulList += "</ul>";
            $($c.controls.infoRestType).html(ulList);
        }

        if (data.suitableforwhat != null && data.suitableforwhat != "") {
            var typeList = data.suitableforwhat.split(',');
            var ulList = "<div class='CONTENTLink'>" + $($c.controls.hdninfosuitable).val() +"</div><hr><ul class='p0'>";
            typeList.forEach(function (item, index) {
                ulList += "<li class='badge'>" + item + "</li>";
            });
            ulList += "</ul>";
            $($c.controls.infoRestSuitable).html(ulList);
        }

        if (data.kitchen != null && data.kitchen != "") {
            var typeList = data.kitchen.split(',');
            var ulList = "<div class='CONTENTLink'>" + $($c.controls.hdninfokitchen).val() +"</div><hr><ul class='p0'>";
            typeList.forEach(function (item, index) {
                ulList += "<li class='badge'>" + item + "</li>";
            });
            ulList += "</ul>";
            $($c.controls.infoRestKitchen).html(ulList);
        }

        $($c.controls.infoRestCapacity).text(data.capacity);
        $($c.controls.infoRestNoOfConfRoom).text(data.total_conference);
        $($c.controls.infoRestSizeOfConfRoom).text(data.size_of_largest_conference.toFixed(2));
        $($c.controls.infoRestLargeRoomCapacity).text(data.capacity_of_largest_room);
        $($c.controls.infoRestMinTurnover).text(data.minimum_turnover.toFixed(2));
        
        $($c.controls.infoRestMiscInfo).text(data.miscellaneous_info);
        
        if (data.miscellaneous_info!=null && data.miscellaneous_info.trim().length > 0) {
            $($ctrl.divRestAdditionalInfo).show();
            $($ctrl.hrRestAdditionalInfo).show();
        }
        else {
            $($ctrl.divRestAdditionalInfo).hide();
            $($ctrl.hrRestAdditionalInfo).hide();
        }

        if (data.outdoor_possibility != null && data.outdoor_possibility == true && data.outdoor_details != null && data.outdoor_details.trim()!='') {
            $($c.controls.infoRestOutdoorDetail).text(data.outdoor_details);
        } else {
            $($c.controls.divinfoRestOutdoorPossibility).addClass("d-none");
        }

        if (data.green != null || data.green) {
            $($c.controls.infoRestGreen).removeClass("d-none");
            $($c.controls.infoRestGreen + ' img').attr('data-original-title', $($c.controls.sGreenTitle).val());
        }
        if (data.blacked != null || data.blacked) {
            $($c.controls.infoRestBlacked).removeClass("d-none");
        }
        if (data.closed != null || data.closed) {
            $($c.controls.infoRestClosed).removeClass("d-none");
        }
        if (data.reopening != null || data.reopening) {
            $($c.controls.infoRestReOpen).removeClass("d-none");
            $($c.controls.infoRestReOpen + ' img').attr('data-original-title', $($c.controls.sReopenTitle).val());
        }

    },
    bindModalButtons: function (data, type) {
        var templates = "";

        var name = "";
        if (type == "H") {

            templates += "<div class='pointer show-tooltip plr2 modal-buttons col-md-2 col-sm-4 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByRestaurant).val() + "' onclick='$search.openrestaurant(event, this);' data-name='" + data.hotel_name + "' data-id='" + data.id + "'  data-lng='" + data.lan + "' data-lat='" + data.lat + "' id='diar'> <div class='amenities-icon'><img src='\\Images\\nearby_restaurant.png' /></div></div>";
            templates += "<div class='pointer show-tooltip plr2 modal-buttons col-md-2 col-sm-4 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByLocation).val() + "' onclick='$search.openlocation(event, this);' data-name='" + data.hotel_name + "' data-id='" + data.id + "'  data-lng='" + data.lan + "' data-lat='" + data.lat + "' id='dial'> <div class='amenities-icon'><img src='\\Images\\nearby_location.png' /></div></div>";
  
            name = data.hotel_name;

        } else if (type == "R") {

            templates += "<div class='pointer show-tooltip plr2 modal-buttons col-md-2 col-sm-4 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByHotel).val() + "' onclick='$search.openhotel(event, this);' data-name='" + data.restaurant_name + "' data-id='" + data.id + "'  data-lng='" + data.lan + "' data-lat='" + data.lat + "' id='diar'> <div class='amenities-icon'><img  src='\\Images\\nearby_hotel.png' /></div></div>";
            templates += "<div class='pointer show-tooltip plr2 modal-buttons col-md-2 col-sm-4 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByLocation).val() + "' onclick='$search.openlocation(event, this);' data-name='" + data.restaurant_name + "' data-id='" + data.id + "'  data-lng='" + data.lan + "' data-lat='" + data.lat + "' id='dial'> <div class='amenities-icon'><img  src='\\Images\\nearby_location.png' /></div></div>";

            name = data.restaurant_name;

        } else if (type == "L") {

            templates += "<div class='pointer show-tooltip plr2 modal-buttons col-md-2 col-sm-4 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByHotel).val() + "' onclick='$search.openhotel(event, this);' data-name='" + data.location_name + "' data-id='" + data.id + "'  data-lng='" + data.lan + "' data-lat='" + data.lat + "' id='dial'> <div class='amenities-icon'><img  src='\\Images\\nearby_hotel.png' /></div></div>";
            templates += "<div class='pointer show-tooltip plr2 modal-buttons col-md-2 col-sm-4 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByRestaurant).val() + "' onclick='$search.openrestaurant(event, this);' data-name='" + data.location_name + "' data-id='" + data.id + "'  data-lng='" + data.lan + "' data-lat='" + data.lat + "' id='diar'> <div class='amenities-icon'><img   src='\\Images\\nearby_restaurant.png' /></div></div>";

            name = data.location_name;
        }

        templates += "<div class='pointer show-tooltip plr2 modal-buttons col-md-2 col-sm-4 col-xs-3' title='" + $($c.controls.sViewOnMap).val() + "' data-name='" + name + "'  data-lng='" + data.lan + "' data-lat='" + data.lat + "' data-type='" + type + "' onclick='$search.openmap(event, this);' id='diam'> <div class='amenities-icon'><img  src='\\Images\\mapview.png' /></div></div>";

        if (data.is360degreeview != undefined && data.is360degreeview != '' && data.is360degreeview != null && data.is360degreeview == true) {

            var url = 'https://www.google.com/maps/@?api=1&map_action=pano&viewpoint=' + data.lat + ',' + data.lan + '&heading=-20&pitch=38&fov=80';

            templates += "<div class='notify-bubble ' ></div><a class='pointer show-tooltipb plr2 360Degree  modal-buttons col-md-2 col-sm-4 col-xs-3' onclick='$bind.ClickStopPropagation(event);' target='_blank' href='" + url + "' title='" + $($c.controls.s360degreeview).val() + "'  > <div class='amenities-icon'><img  src='\\Images\\streetview.png' /></div></a>&nbsp;";
        }

        $($c.controls.infoModalBtn).html(templates);
        
        templates = "";
        if (data.isincart == true) {
            templates += "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "'  data-id='" + data.id + "'  data-type='" + type + "' id='btnRequestRFP' onClick='$search.RemoveResourceCart(event, this,\"infoDetail\");'><i class='glyphicon glyphicon-bookmark bookmarkfont'></i></a>";
            //templates += "<a class='btn  btn-primary request-rfp show-tooltip'  data-id='" + data.id + "'  data-type='" + type + "' id='btnRequestRFP' onClick='$search.RemoveResourceCart(this,\"infoDetail\");'><i class='glyphicon glyphicon-bookmark'></i>&nbsp;" + $($c.controls.hdnremove).val() + "</a>";
        }
        else {
            templates += "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sAddBookmark).val() + "'  data-id='" + data.id + "'  data-type='" + type + "' id='btnRequestRFP' onClick='$search.AddResourceCart(event, this, \"infoDetail\");'><i class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></i></a>";
           // templates += "<a class='btn btn-primary request-rfp show-tooltip'  data-id='" + data.id + "'  data-type='" + type + "' id='btnRequestRFP' onClick='$search.AddResourceCart(this, \"infoDetail\");'><i class='glyphicon glyphicon-bookmark'></i></a>";
        }
        $('#addtocart').html(templates);
        $bind.bindTooltip();
    }
};

var $ctrl = {

    txtoldpass: "#txtoldpass",
    txtnewpass: "#txtnewpass",
    txtconfirmpass: "#txtconfirmpass",
    hdnAPIBaseURL: "#hdnAPIBaseURL",
    usercontainer: '#user-container',
    btnupdate: "#btnupdate",
    hdnOldPassMsg: "#hdnOldPassMsg",
    hdnConfirmPassMsg: "#hdnConfirmPassMsg",
    btnClose: "#btnClose",
    divAdditionalInfo: "#divAdditionalInfo",
    hrAdditionalInfo:"#hrAdditionalInfo",
    divLocAdditionalInfo: "#divLocAdditionalInfo",
    hrLocAdditionalInfo: "#hrLocAdditionalInfo",
    divRestAdditionalInfo: "#divRestAdditionalInfo",
    hrRestAdditionalInfo:"#hrRestAdditionalInfo"
};

var $url = {
    changePasswordURL: "AdminSecure/ChangePassword"
};

$(document).ready(function () {
    $($ctrl.btnupdate).click(function () {
        if ($changepassword.validateform()) {
            $changepassword.submitchangepassword();
        }
    });
});


var $changepassword = {
    validateform: function () {

        var flag = true;
        var oldpass = $($ctrl.txtoldpass).val();
        var newpass = $($ctrl.txtnewpass).val();
        var confirmpass = $($ctrl.txtconfirmpass).val();

        if (oldpass == null || oldpass == undefined || oldpass == "") {
            $($ctrl.txtoldpass).addClass("Error");
            flag = false;
        } else {
            $($ctrl.txtoldpass).removeClass("Error");
        }

        if (newpass == undefined || newpass == null || newpass == "") {
            $($ctrl.txtnewpass).addClass("Error");
            flag = false;
        } else {
            $($ctrl.txtnewpass).removeClass("Error");
        }

        if (confirmpass == undefined || confirmpass == null || confirmpass == "") {
            $($ctrl.txtconfirmpass).addClass("Error");
            flag = false;
        } else {
            $($ctrl.txtconfirmpass).removeClass("Error");
        }

        if (confirmpass != undefined || confirmpass != null || confirmpass != "" || newpass == undefined || newpass == null || newpass == "") {
            if ($($ctrl.txtnewpass).val() != $($ctrl.txtconfirmpass).val()) {
                $($ctrl.txtnewpass).addClass("Error");
                $($ctrl.txtconfirmpass).addClass("Error");
                $managecode.Decision(false, "", "", $($ctrl.hdnConfirmPassMsg).val(), "Login", false);
                flag = false;
            }
        }
        return flag;
    },
    submitchangepassword: function () {
        
        common.displayLoader();

        var login = {
            Email: JSON.parse(localStorage.CurrentUser).Email,
            OldPassword: $($ctrl.txtoldpass).val(),
            NewPassword: $($ctrl.txtnewpass).val()
        };
        
        $.ajax({
            type: "POST",
            url: $($ctrl.hdnAPIBaseURL).val() + $url.changePasswordURL,
            data: JSON.stringify(login),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {

                if (data != undefined && data != null && data.code == "703") {

                    //var token = response.getResponseHeader('authorization');
                    //var datas = data.data;

                    //ajaxRequest.setAuthorizationToken(JSON.stringify(token));
                    //ajaxRequest.setCurrentUser(JSON.stringify(datas));

                    window.localStorage.removeItem(ajaxRequest.variables.authorizationTokenKey);
                    window.localStorage.removeItem(ajaxRequest.variables.CurrentUser);

                    $($ctrl.usercontainer).modal("hide");
                    $managecode.Decision(false, "", data.code, data.message, "Login", true);
                    setTimeout(function () { location.href = "/Login"; }, 5000);
                    //location.href = "/Login";

                } else {
                    
                    if (data != undefined && data != null && data.code == "610") {
                       
                        $managecode.Decision(false, "", "", $($ctrl.hdnOldPassMsg).val(), "Login", false);
                    }
                    else {
                        
                        $managecode.Decision(false, "", data.code, data.message, "Login", false);
                    }

                }

            },
            failure: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    }
}