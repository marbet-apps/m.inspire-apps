﻿using DCP.Entities;
using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace DCPortalAPI.Filter
{
    /// <summary>
    /// Validate Model Attribute
    /// </summary>
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Get Responces
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                if (actionContext.ModelState.IsValid == false)
                {
                    string err = string.Empty;
                    var expetion = string.Empty;
                    var dd = actionContext.ModelState.Values;

                    foreach (var d in dd)
                    {
                        if (d.Errors.Count > 0)
                        {
                            foreach (var e1 in d.Errors)
                            {
                                if (!string.IsNullOrEmpty(e1.ErrorMessage))
                                {
                                    err += e1.ErrorMessage;
                                }

                                if (e1.Exception != null)
                                {
                                    expetion += e1.Exception.Message;
                                }
                            }
                        }
                    }

                   // var ErrorCode = string.IsNullOrEmpty(expetion) ? ErrorCodes.MissingArgument : ErrorCodes.FaultError;
                    var data = string.IsNullOrEmpty(expetion) ? err : expetion;

                    //if (responsemodel.Error.Contains(ErrorMessage.MessageSeprator))
                    //{
                    //    var errs = Utility.GenralUtility.ExceptionResponse(ErrorMessage.MessageSeprator, responsemodel.Error);
                    //    responsemodel.Error = errs[0];
                    //    responsemodel.ErrorCode = errs[1];
                    //}

                    var result = ResultSet.SuccessResponse(ResponceCodes.Error.ModelValidateFail, "", ManageEnum.status.error.ToString(), data);
                    
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, result);

                    WriteLog.LogWarning("Validation : " + data);
                }
            }
            catch (Exception ex)
            {
                WriteLog.LogException(ex.Message, ex);
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.InternalServerError, ResultSet.ErrorResponse(ResponceCodes.Error.ModelValidateFail, ex));
            }


        }
    }

    /// <summary>
    /// Check model for null attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class CheckModelForNullAttribute : ActionFilterAttribute
    {

        private readonly Func<Dictionary<string, object>, bool> _validate;

        /// <summary>
        /// Check Model For Null Attribute
        /// </summary>
        public CheckModelForNullAttribute() : this(arguments => arguments.ContainsValue(null))
        {

        }

        /// <summary>
        /// Check Model For Null Attribute
        /// </summary>
        /// <param name="checkCondition"></param>
        public CheckModelForNullAttribute(Func<Dictionary<string, object>, bool> checkCondition)
        {
            _validate = checkCondition;
        }

        /// <summary>
        /// On Action Executing
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (_validate(actionContext.ActionArguments))
            {
                var result = ResultSet.SuccessResponse(ResponceCodes.Error.ModelValidateFail, "", ManageEnum.status.error.ToString(), ResponceMessages.Error.InsufficientParam);
                WriteLog.LogWarning("Validation : " + ResponceCodes.Error.ModelValidateFail);
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }
        }

    }



}