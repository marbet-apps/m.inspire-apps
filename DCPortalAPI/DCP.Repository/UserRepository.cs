﻿using DCP.Entities;
using DCP.Models;
using DCP.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Repository
{
    public class UserRepository : IUserRepository
    {
        private DCPDbContext _context = null;


        public UserRepository()
        {
            this._context = new DCPDbContext();
        }
        public List<UserRegisterModel> GetUserList(string search, string sortBy, int loggedInUserId)
        {

            var dbUsers1 = (from um in _context.User_Masters select new { um });

            var dbUsers = (from um in _context.User_Masters
                     join uc in _context.User_Credential on um.User_Id equals uc.User_Master_Id
                           where um.created_by==loggedInUserId && (um.isAdmin==null || um.isAdmin==false) && um.IsDeleted==false// && um.IsActive == true
                           select new { User_Id= um.User_Id,First_Name = um.First_Name, Last_Name= um.Last_Name, Email = uc.Email, IsActive= um.IsActive, Salutation= um.Salutation, Company_Name = um.Company_Name }).ToList();

            if (!string.IsNullOrEmpty(search))
            {
                dbUsers = dbUsers.Where(u => u.First_Name.ToLower().Contains(search) || u.Last_Name.ToLower().Contains(search)).ToList();
            }

            if(!string.IsNullOrEmpty(sortBy))
            {
                if(sortBy.ToLower()=="First_NameAsc".ToLower())
                {
                    dbUsers = dbUsers.OrderBy(t => t.First_Name).ToList();
                }
                else if (sortBy.ToLower() == "First_Namedesc".ToLower())
                {
                    dbUsers = dbUsers.OrderByDescending(t => t.First_Name).ToList();
                }
                else if (sortBy.ToLower() == "Last_NameAsc".ToLower())
                {
                    dbUsers = dbUsers.OrderBy(t => t.Last_Name).ToList();
                }
                else if (sortBy.ToLower() == "Last_Namedesc".ToLower())
                {
                    dbUsers = dbUsers.OrderByDescending(t => t.Last_Name).ToList();
                }
                else if (sortBy.ToLower() == "Emailasc".ToLower())
                {
                    dbUsers = dbUsers.OrderBy(t => t.Email).ToList();
                }
                else if (sortBy.ToLower() == "Emaildesc".ToLower())
                {
                    dbUsers = dbUsers.OrderByDescending(t => t.Email).ToList();
                }
                else if (sortBy.ToLower() == "Company_Nameasc".ToLower())
                {
                    dbUsers = dbUsers.OrderBy(t => t.Company_Name).ToList();
                }
                else if (sortBy.ToLower() == "Company_Namedesc".ToLower())
                {
                    dbUsers = dbUsers.OrderByDescending(t => t.Company_Name).ToList();
                }
                else if (sortBy.ToLower() == "Salutationasc".ToLower())
                {
                    dbUsers = dbUsers.OrderBy(t => t.Salutation).ToList();
                }
                else if (sortBy.ToLower() == "Salutationdesc".ToLower())
                {
                    dbUsers = dbUsers.OrderByDescending(t => t.Salutation).ToList();
                }
                
            }
            else
            {
                dbUsers = dbUsers.OrderBy(t => t.First_Name).ToList();
            }

            List<UserRegisterModel> userList = dbUsers.Select(m => new UserRegisterModel()
            {
                Email = m.Email,
                First_Name = m.First_Name,
                Last_Name = m.Last_Name,
                Userid = m.User_Id,
                Salutation = m.Salutation,
                IsActive = m.IsActive,
                Company_Name = m.Company_Name
                
            }).ToList();

            return userList;
        }

        public bool DeleteUser(int userId)
        {
            bool isDelete = false;

            var dbUser = _context.User_Masters.Where(t => t.User_Id == userId).FirstOrDefault();
            if (dbUser != null)
            {
                dbUser.IsActive = false;
                _context.SaveChanges();
                isDelete = true;
            }
            return isDelete;
        }

        public UserRegisterModel GetUserDetail(int userId, string lang = "")
        {
            UserRegisterModel userModel = new UserRegisterModel();
            var dbUser = (from um in _context.User_Masters
                          join uc in _context.User_Communication on um.User_Id equals uc.User_Master_Id
                          join ur in _context.User_Credential on um.User_Id equals ur.User_Master_Id
                          where um.User_Id == userId select new { um,uc,ur}).FirstOrDefault();
            if(dbUser!=null)
            {
                userModel.Salutation = dbUser.um.Salutation;
                userModel.First_Name = dbUser.um.First_Name;
                userModel.Last_Name = dbUser.um.Last_Name;
                userModel.Company_Name = dbUser.um.Company_Name;
                userModel.Department_Name = dbUser.um.Department_Name;
                userModel.Street = dbUser.uc.Street;
                userModel.PostCode = dbUser.uc.PostCode;
                userModel.Place = dbUser.uc.Place;
                userModel.Country_Id = dbUser.uc.Country_Id;
                userModel.Email = dbUser.ur.Email;
                userModel.Telephone = dbUser.uc.Telephone;
                userModel.Tax_Indentifier_No = dbUser.uc.Tax_Indentifier_No;
                userModel.IsActive = dbUser.um.IsActive;
                userModel.CountryName = (userId > 0) ? (lang == "de-DE" ? _context.List_Country.Where(c => c.Id == dbUser.uc.Country_Id).FirstOrDefault().country_name_de : _context.List_Country.Where(c => c.Id == dbUser.uc.Country_Id).FirstOrDefault().country_name) : "";
                userModel.Userid = dbUser.um.User_Id;
            }
            return userModel;
        }

        public ResponseModel UpdateUser(UpdateUserModel model)
        {
            ResponseModel responseModel = new ResponseModel();

            var dbUserEntity = (from um in _context.User_Masters
                                join uc in _context.User_Credential on um.User_Id equals uc.User_Master_Id
                                where um.User_Id == model.LogedInUserId && uc.Email == model.Email
                                select new { um }).FirstOrDefault();
            if (dbUserEntity != null)
            {
                dbUserEntity.um.First_Name = model.First_Name;
                dbUserEntity.um.Last_Name = model.Last_Name;
                dbUserEntity.um.Company_Name = model.Company_Name;
                dbUserEntity.um.Department_Name = model.Department_Name;
                dbUserEntity.um.IsActive = model.IsActive==null?false: Convert.ToBoolean(model.IsActive);
                dbUserEntity.um.Salutation = model.Salutation;
                dbUserEntity.um.modified_by = model.LogedInUserId;
                dbUserEntity.um.modify_date = DateTime.Now;
                _context.SaveChanges();
            }


            var dbUserCommunication = (from um in _context.User_Communication
                                       join uc in _context.User_Credential on um.User_Master_Id equals uc.User_Master_Id
                                       where um.User_Master_Id == model.LogedInUserId && uc.Email == model.Email
                                       select new { um }).FirstOrDefault();
            if (dbUserCommunication != null)
            {
                dbUserCommunication.um.Street = model.Street;
                dbUserCommunication.um.Place = model.Place;
                dbUserCommunication.um.PostCode = model.PostCode;
                dbUserCommunication.um.Tax_Indentifier_No = model.Tax_Indentifier_No;
                dbUserCommunication.um.Telephone = model.Telephone;
                dbUserCommunication.um.Country_Id = Convert.ToInt32(model.Country_Id);
                dbUserCommunication.um.modified_by = model.LogedInUserId;
                dbUserCommunication.um.modify_date = DateTime.Now;
                _context.SaveChanges();
            }



            responseModel.isSuccess = true;
            responseModel.LogedInUserId = Convert.ToInt32(model.LogedInUserId);
            responseModel.UserModel = model;
            return responseModel;

        }
    }
}
