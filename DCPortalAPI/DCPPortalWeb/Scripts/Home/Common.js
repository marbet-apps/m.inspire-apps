﻿var $c = {

    controls: {
        btnsearch: '.search',
        ddl: '.ddl',
        addformelements: '.add-form-elements',
        iaddformelements: '.iadd-form-elements',
        form: '.form',
        advancefilter: '.af',
        nonadvancefilter: '.naf',
        hrladvancefilter: '.hrladvancefilter',
        typeclass: '.typeclass',
        Ccontainer: '.Ccontainer',
        PartialLeftFilter: '.PartialLeftFilter',
        PartialMiddleContent: '.PartialMiddleContent',
        starbtn: '.fl-group__btn',
        starbtnlg: '.fl-btnlg',
        Screatetrip: '#Screatetrip',
        sortby: '.sortby',
        rstotalroom: '#rstotalroom',
        rstotalconfroom: '#rstotalconfroom',
        rsconfsize: '#rsconfsize',
        rslftotalroom: '#rslftotalroom',
        rslftotalconfroom: '#rslftotalconfroom',
        rslfparkingspace: '#rslfparkingspace',
        rslfconfsize: '#rslfconfsize',
        Rrstotalroom: '#Rrstotalroom',
        Rrsroomsize: '#Rrsroomsize',
        Rrsroomcapacity: '#Rrsroomcapacity',
        Rrsrestaurantcapacity: '#Rrsrestaurantcapacity',
        Rrslftotalroom: '#Rrslftotalroom',
        Rrslfroomsize: '#Rrslfroomsize',
        Rrslfroomcapacity: '#Rrslfroomcapacity',
        Rrslfrestaurantcapacity: '#Rrslfrestaurantcapacity',
        Rrslfminimumturnover: '#Rrslfminimumturnover',
        Lrstotalconfroom: '#Lrstotalconfroom',
        Lrsconfsize: '#Lrsconfsize',
        Lrsroomheight: '#Lrsroomheight',
        Lrsrentalcost: '#Lrsrentalcost',
        Lrslftotalconfroom: '#Lrslftotalconfroom',
        Lrslfconfsize: '#Lrslfconfsize',
        Lrslfroomheight: '#Lrslfroomheight',
        Lrslfrentalcost: '#Lrslfrentalcost',
        multilocationtype: '#multilocationtype',
        multilocationtypei: '#multilocationtypei',
        multirestaurantkitchen: '#multirestaurantkitchen',
        multirestaurantkitcheni: '#multirestaurantkitcheni',
        multirestauranttype: '#multirestauranttype',
        multirestauranttypei: '#multirestauranttypei',
        multirestaurantsuitablefor: '#multirestaurantsuitablefor',
        multirestaurantsuitablefori: '#multirestaurantsuitablefori',
        multihoteltype: '#multihoteltype',
        multihoteltypei: '#multihoteltypei',
        multilocationsuitablefor: '#multilocationsuitablefor',
        multilocationsuitablefori: '#multilocationsuitablefori',
        ihotel: '#ihotel',
        irestaurant: '#irestaurant',
        liSearchCountry: '#liSearchCountry',
        liSearchDestination: '#liSearchDestination',
        ilocation: '#ilocation',
        headertag: '#headertag',
        headersection: '#headersection',
        largemenu: '.largemenu',
        bhoechie: '#bhoechie',
        hidelabel: '.hidelabel',
        panelwhiteback: '.panelwhiteback',
        DivGeneralInfo: '#DivGeneralInfo',
        leftfilter: '#leftfilter',
        middlecontent: '#middlecontent',
        txtcity: '#txtcity',
        txtrestaurantcity: '#txtrestaurantcity',
        txtlocationcity: '#txtlocationcity',
        txthotelname: '#txthotelname',
        txtrestaurantname: '#txtrestaurantname',
        txtlocationname: '#txtlocationname',
        ddlcountry: '#ddlcountry',
        ddlrestaurantcountry: '#ddlrestaurantcountry',
        ddllocationcountry: '#ddllocationcountry',
        ddlmytrip: '#ddlmytrip',
        s2idddlcountry: '#s2id_ddlcountry .select2-choice',
        s2idddlradius: '#s2id_ddlradius .select2-choice',
        s2idddlrestaurantcountry: '#s2id_ddlrestaurantcountry .select2-choice',
        s2idddllocationcountry: '#s2id_ddllocationcountry .select2-choice',
        btnresetadvance: '#btnresetadvance',
        txtdistance: '#txtdistance',
        txtRlocation: '#txtRlocation',
        txtHlocation: '#txtHlocation',
        txtLlocation: '#txtLlocation',
        txthours: '#txthours',
        txtminutes: '#txtminutes',
        chkoutdoor: 'input[name=choice_radioop]:checked',
        chkCP: 'input[name=choice_radiocp]:checked',
        chkTP: 'input[name=choice_radiotp]:checked',
        chkCD: 'input[name=choice_radiocd]:checked',
        chkrestinhotel: 'input[name=choice_radiorih]:checked',
        lblMsg: '#lblMsg',
        divMsg: '#divMsg',
        drawcontents: '#drawcontents',
        sNoofRoom: '#sNoofRoom',
        sRentalCost: '#sRentalCost',
        sNoofConference: '#sNoofConference',
        sSizeofLargestroom: '#sSizeofLargestroom',
        sCapacityOfRoom: '#sCapacityOfRoom',
        sCapacityOfRestaurant: '#sCapacityOfRestaurant',
        sPP: '#sPP',
        sGreen: '#sGreen',
        sClose: '#sClose',
        sBlacked: '#sBlacked',
        sReopen: '#sReopen',
        lbldisplayed: '#lbldisplayed',
        lblmapdisplayed: '#lblmapdisplayed',
        lbltotal: '#lbltotal',
        modeltitle: '#modeltitle',
        infotitle: '#infotitle',
        mapmsg: '#mapmsg',
        afurtherfilter: '#afurtherfilter',
        listview: '#listview',
        sNearByHotel: '#sNearByHotel',
        sNearByRestaurant: '#sNearByRestaurant',
        sNearByLocation: '#sNearByLocation',
        NearByKM: '#NearByKM',
        sMore: '#sMore',
        map: '#map',
        mapview: '#mapview',
        mapwrapper: '#mapwrapper',
        drawmap: '#drawmap',
        divshowtotal: '#divshowtotal',
        divshowstartrfp: '#divshowstartrfp',
        divshowsortby: '#divshowsortby',
        cansmall: '.cansmall',
        addtocart: '.addtocart',
        smalladdtocart: '.smalladdtocart',
        Mh75: '.Mh75',
        imagediv: '.imagediv',
        mloader: '#mloader',
        cartCountloader: '#cartCountloader',
        bookmarkCartCount: '#bookmarkCartCount',
        nearbylist: '#nearbylist',
        sorting: '#sorting',
        sorted: '#sorted',
        btntoggle: '#btntoggle',
        sNoRecords: '#sNoRecords',
        lblnorecords: '#lblnorecords',
        sViewOnMap: '#sViewOnMap',
        sLimaRating: '#sLimaRating',
        s360degreeview: '#s360degreeview',
        btnchk: '.btnchk',
        hotelname: '.hotelname',
        btnhotel: '#btnhotel',
        hotel: '#hotel',
        restaurant: '#restaurant',
        location: '#location',
        bihotel: '#bihotel',
        birestaurant: '#birestaurant',
        bilocation: '#bilocation',
        btnadvancehotel: '#btnadvancehotel',
        btnadvancerestaurant: '#btnadvancerestaurant',
        btnadvancelocation: '#btnadvancelocation',
        btnleftfiltersearch: '#btnleftfiltersearch',
        aNoofRoom: '#aNoofRoom',
        mytripinfo: '#mytripinfo',
        popovercontent: '#popover-content',
        mytripdiv: '#mytripdiv',
        MyTripList: '#MyTripList',
        LimaR: '.LimaR',
        aNoofConference: '#aNoofConference',
        aSizeofLargestroom: '#aSizeofLargestroom',
        aParkingSpaces: '#aParkingSpaces',
        aNoofRoomo: '#aNoofRoomo',
        aNoofConferenceo: '#aNoofConferenceo',
        aSizeofLargestroomo: '#aSizeofLargestroomo',
        sMIn: '#sMIn',
        sMax: '#sMax',
        sSet: '#sSet',
        backtotop: '#back-to-top',
        copyurl: '#copyurl',
        savefilter: '#savefilter',
        sharefilter: '#sharefilter',
        slide: '.sidepanel .slide',
        slidebutton: '.sidepanel .button',
        sAdminUpdateMail: '#sAdminUpdateMail',
        sUpdateMailSubject: '#sUpdateMailSubject',
        txtinsidehotelname: '#txtinsidehotelname',
        txtinsiderestname: '#txtinsiderestname',
        txtinsidelocname: '#txtinsidelocname',
        RaNoofRoomo: '#RaNoofRoomo',
        RaNoofRoom: '#RaNoofRoom',
        RaSizeofLargestroomo: '#RaSizeofLargestroomo',
        RaSizeofLargestroom: '#RaSizeofLargestroom',
        RaCapacityofroomo: '#RaCapacityofroomo',
        RaCapacityofroom: '#RaCapacityofroom',
        RaCapacityofrestauranto: '#RaCapacityofrestauranto',
        RaCapacityofrestaurant: '#RaCapacityofrestaurant',
        RaMinimumturnover: '#RaMinimumturnover',
        LaNoofConferenceo: '#LaNoofConferenceo',
        LaNoofConference: '#LaNoofConference',
        LaSizeofLargestroomo: '#LaSizeofLargestroomo',
        LaSizeofLargestroom: '#LaSizeofLargestroom',
        Laroomheighto: '#Laroomheighto',
        Laroomheight: '#Laroomheight',
        Larentalcosto: '#Larentalcosto',
        Larentalcost: '#Larentalcost',
        btnlocation: '#btnlocation',
        btnrestaurant: '#btnrestaurant',
        infomodal: '#infomodal',
        infoplace: '#infoplace',
        userbody: '#userbody',
        usercontainer: '#user-container',
        Usertitle: '#Usertitle',
        reviewplace: '#reviewplace',
        reviewtitle: '#reviewtitle',
        reviewmodal: '#reviewmodal',
        mytripmodal: '#mytripmodal',
        btnfilter: '#btnfilter',
        ddltemplate: '#ddltemplate',
        txttemplatenamecopy: '#txttemplatenamecopy',
        s2idddltemplate: '#s2id_ddltemplate .select2-choice',
        ddlusers: '#ddlusers',
        s2idddlusers: '#s2id_ddlusers .select2-choices',
        hdnselectcountry: '#hdnselectcountry',
        sSelectUsers: '#sSelectUsers',
        templateC: '.templateC',
        applytemplate: '#applytemplate',
        addtemplate: '#addtemplate',
        sharetemplate: '#sharetemplate',
        divMsgws: '#divMsgws',
        lblMsgws: '#lblMsgws',
        txttemplatename: '#txttemplatename',
        txttripname0: '#txttripname0',
        txttripname1: '#txttripname1',
        btnsavemytrip: '#btnsavemytrip',
        txtmiscellaneousinfo0: '#txtmiscellaneousinfo0',
        txtmiscellaneousinfo1: '#txtmiscellaneousinfo1',
        btnaddtemplate: '#btnaddtemplate',
        btncopyemplate: '#btncopyemplate',
        btndeletetemplate: '#btndeletetemplate',
        btnsavetemplate: '#btnsavetemplate',
        btnshareemailtemplate: '#btnshareemailtemplate',
        divloader: '#divloader',
        txtinfo: '#txtinfo',
        sWebBaseUrl: '#sWebBaseUrl',
        btncopyurl: '#btncopyurl',
        spancopydone: '#spancopydone',
        imgcopydone: '#imgcopydone',
        btnapplyemplate: '#btnapplyemplate',
        sDeleteConfirm: '#sDeleteConfirm',
        sDeleteConfirmTitle: '#sDeleteConfirmTitle',
        sDeleteConfirmOk: '#sDeleteConfirmOk',
        sDeleteConfirmCancel: '#sDeleteConfirmCancel',
        sAddBookmark: '#sAddBookmark',
        sRemoveBookmark: '#sRemoveBookmark',
        sGreenTitle: '#sGreenTitle',
        sReopenTitle: '#sReopenTitle',
        sCateringPartnerTitle: '#sCateringPartnerTitle',
        sTechnicalPartnerTitle: '#sTechnicalPartnerTitle',
        sAccessibleForCarsTitle: '#sAccessibleForCarsTitle',

        yes_radioPP: '#yes_radioPP',
        maybe_radioPP: '#maybe_radioPP',
        no_radioPP: '#no_radioPP',
        yes_radioGreen: '#yes_radioGreen',
        maybe_radioGreen: '#maybe_radioGreen',
        no_radioGreen: '#no_radioGreen',
        yes_radioReopen: '#yes_radioReopen',
        maybe_radioReopen: '#maybe_radioReopen',
        no_radioReopen: '#no_radioReopen',
        yes_radioClose: '#yes_radioClose',
        maybe_radioClose: '#maybe_radioClose',
        no_radioClose: '#no_radioClose',
        yes_radioBlack: '#yes_radioBlack',
        maybe_radioBlack: '#maybe_radioBlack',
        no_radioBlack: '#no_radioBlack',
        yes_radioOP: '#yes_radioOP',
        maybe_radioOP: '#maybe_radioOP',
        no_radioOP: '#no_radioOP',
        yes_radioRIH: '#yes_radioRIH',
        maybe_radioRIH: '#maybe_radioRIH',
        no_radioRIH: '#no_radioRIH',
        yes_radioCP: '#yes_radioCP',
        maybe_radioCP: '#maybe_radioCP',
        no_radioCP: '#no_radioCP',
        yes_radioTP: '#yes_radioTP',
        maybe_radioTP: '#maybe_radioTP',
        no_radioTP: '#no_radioTP',
        yes_radioCD: '#yes_radioCD',
        maybe_radioCD: '#maybe_radioCD',
        no_radioCD: '#no_radioCD',
        multihotelchaini: '#multihotelchaini',
        tabcontent: '.tab-content',
        drawbutton: '#drawbutton',
        Cards: '.Cards',
        card5: '.card-5',
        contentdiv: '.contentdiv',
        refreshmap: '#refreshmap',
        applylastfilterH: '#applylastfilterH',
        applylastfilterR: '#applylastfilterR',
        applylastfilterL: '#applylastfilterL',
        btnCreatetrip: '#btnCreatetrip',
        wrappertrip: '.wrapper-trip',
        Edittrip: '.Edittrip',
        hdnTripId: '#hdnTripId',
        hdnhotel: '#hdnhotel',
        hdnrestaurant: '#hdnrestaurant',
        hdnlocation: '#hdnlocation',
        hdnLanguage: '#hdnLanguage',
        hdnUser: '#hdnUser',
        sReviews: '#sReviews',
        aLHotel: '#aLHotel',
        aUHotel: '#aUHotel',
        aLRestaurant: '#aLRestaurant',
        aURestaurant: '#aURestaurant',
        aLLocation: '#aLLocation',
        aULocation: '#aULocation',
        infoboxnumber: '.info-box-number',
        advancehotel: '#advancehotel',
        advancerestaurant: '#advancerestaurant',
        advancelocation: '#advancelocation',
        SEnterTrip: '#SEnterTrip',
        SMiscellaneousInfo: '#SMiscellaneousInfo',
        SSave: '#SSave',
        SList: '#SList',
        SEdit: '#SEdit',
        SDelete: '#SDelete',
        SCopy: '#SCopy',
        Tripcard: '.Trip-card',
        ddltrips: '#ddltrips',
        tripplace: '#tripplace',
        SAdd: '#SAdd',
        hdnAPIBaseURL: '#hdnAPIBaseURL',
        hdnImageBaseURL: '#hdnImageBaseURL',
        ddlradius: '#ddlradius',
        hdnselecthotelchain: '#hdnselecthotelchain',
        hdnselectradius: '#hdnselectradius',
        hdnselectlocation: '#hdnselectlocation',
        hdnselectsuitablefor: '#hdnselectsuitablefor',
        hdnselectkitchen: '#hdnselectkitchen',
        hdnselectrestaurant: '#hdnselectrestaurant',
        hdnselecthotel: '#hdnselecthotel',
        txtdestinationname: '#txtdestinationname',
        chkhotel: '#chkhotel',
        RHotel: '.RHotel',
        RRestaurant: '.RRestaurant',
        RLocation: '.RLocation',
        aprofile: '#aprofile',
        achangepass: '#achangepass',
        chkrestaurant: '#chkrestaurant',
        chklocation: '#chklocation',
        cityclass: '#cityclass',
        chkirestaurant: '#chkirestaurant',
        chkilocation: '#chkilocation',
        chkihotel: '#chkihotel',
        locationpanel: '#locationpanel',
        restaurantpanel: '#restaurantpanel',
        hotelpanel: '#hotelpanel',
        hotelfilter: '#hotelfilter',
        restaurantfilter: '#restaurantfilter',
        locationfilter: '#locationfilter',
        spanLAD: '#spanLAD',
        spanuser: '#spanuser',
        hdnadd: '#hdnadd',
        hdnremove: '#hdnremove',
        aihotel: '#aihotel',
        airestaurant: '#airestaurant',
        ailocation: '#ailocation',
        ddlocountry: '#ddlocountry',
        txtocity: '#txtocity',
        headermapview: '#map-header-cta',
        closewindow: '#closewindow',
        infoHotelName: '#infoHotelName',
        infoHotelNameTitle: '#infoHotelNameTitle',
        infoHotelChain: '#infoHotelChain',
        infoHotelRating: '#infoHotelRating',
        infoHotelAddr: '#infoHotelAddr',
        infoHotelWebsite: '#infoHotelWebsite',
        infoHotelAirportName: '#infoHotelAirportName',
        infoHotelAirportTime: '#infoHotelAirportTime',
        infoHotelAirportDistance: '#infoHotelAirportDistance',
        btnRequestRFP: '#btnRequestRFP',
        infoHotelType: '#infoHotelType',
        infoHotelNoOfRoom: '#infoHotelNoOfRoom',
        infoHotelNoOfConfRoom: '#infoHotelNoOfConfRoom',
        infoHotelSizeOfConfRoom: '#infoHotelSizeOfConfRoom',
        infoHotelParkingSpace: '#infoHotelParkingSpace',
        infoHotelMiscInfo: '#infoHotelMiscInfo',
        infoHotelFeature: '#infoHotelFeature',
        infoHotelPP: '#infoHotelPP',
        infoHotelGreen: '#infoHotelGreen',
        infoHotelNewOpen: '#infoHotelNewOpen',
        infoHotelBlacked: '#infoHotelBlacked',
        infoHotelClosed: '#infoHotelClosed',
        infoHotelReOpen: '#infoHotelReOpen',
        infoLocReOpen: "#infoLocReOpen",
        infoRestReOpen: "#infoRestReOpen",
        divinfoHotelAddress: '#divinfoHotelAddress',
        divinfoHotelsite: '#divinfoHotelsite',
        divinfoHotelAirport: '#divinfoHotelAirport',
        infoHotelAirportHours: '#infoHotelAirportHours',
        infoHotelAirportMin: '#infoHotelAirportMin',
        myrfpdiv: '#myrfpdiv',
        myrfpdraftdiv: '#myrfpdraftdiv',
        infoLocName: '#infoLocName',
        divinfoLocAddress: '#divinfoLocAddress',
        infoLocAddr: '#infoLocAddr',
        divinfoLocsite: '#divinfoLocsite',
        infoLocWebsite: '#infoLocWebsite',
        infoLocType: '#infoLocType',
        infoLocRoomHeight: '#infoLocRoomHeight',
        infoLocNoOfConfRoom: '#infoLocNoOfConfRoom',
        infoLocSizeOfConfRoom: '#infoLocSizeOfConfRoom',
        infoLocRentBegining: '#infoLocRentBegining',
        infoLocMiscInfo: '#infoLocMiscInfo',
        divinfoLocOutdoorPossibility: '#divinfoLocOutdoorPossibility',
        infoLocOutdoorDetail: '#infoLocOutdoorDetail',
        infoLocCatering: '#infoLocCatering',
        infoLocTechnik: '#infoLocTechnik',
        infoLocCarDrive: '#infoLocCarDrive',
        infoLocGreen: '#infoLocGreen',
        infoLocBlacked: '#infoLocBlacked',
        infoLocClosed: '#infoLocClosed',
        infoLocSuitable: '#infoLocSuitable',
        licart: '#licart',
        tabs: '.tabs',
        infoRestName: '#infoRestName',
        divinfoRestAddress: '#divinfoRestAddress',
        infoRestAddr: '#infoRestAddr',
        divinfoRestsite: '#divinfoRestsite',
        infoRestWebsite: '#infoRestWebsite',
        infoRestType: '#infoRestType',
        infoRestCapacity: '#infoRestCapacity',
        infoRestNoOfConfRoom: '#infoRestNoOfConfRoom',
        infoRestSizeOfConfRoom: '#infoRestSizeOfConfRoom',
        infoRestLargeRoomCapacity: '#infoRestLargeRoomCapacity',
        infoRestMinTurnover: '#infoRestMinTurnover',
        infoRestMiscInfo: '#infoRestMiscInfo',
        divinfoRestOutdoorPossibility: '#divinfoRestOutdoorPossibility',
        infoRestOutdoorDetail: '#infoRestOutdoorDetail',
        infoRestGreen: '#infoRestGreen',
        infoRestBlacked: '#infoRestBlacked',
        infoRestClosed: '#infoRestClosed',
        infoRestSuitable: '#infoRestSuitable',
        infoRestKitchen: '#infoRestKitchen',
        limyrfp: '#limyrfp',
        btnViewRestaurant: '#btnViewRestaurant',
        btnViewLocation: '#btnViewLocation',
        btnViewHotel: '#btnViewHotel',
        infoModalBtn: '#infoModalBtn',
        txtsearchevent: '#txtsearchevent',
        hdninfohoteltype: '#hdninfohoteltype',
        hdninfolocationtype: '#hdninfolocationtype',
        hdninforestauranttype: '#hdninforestauranttype',
        hdninfokitchen: '#hdninfokitchen',
        hdninfosuitable: '#hdninfosuitable',
        btnViewMap: '#btnViewMap',
        hdnview: '#hdnview',
        hdnrunningdays: '#hdnrunningdays',
        hdncreatedon: '#hdncreatedon',
        hdncreatedBy: '#hdncreatedBy',
        hdnstatus: '#hdnstatus',
        hdnNoUpdate: '#hdnNoUpdate',
        hdnChangePass: '#hdnChangePass',
        hdnEditProfile: '#hdnEditProfile',
        amanageuser: '#amanageuser',
        Accotab: '#Accotab',
        Conftab: '#Conftab',
        Grptab: '#Grptab',
        hdnCheckInDate: '#hdnCheckInDate',
        hdnCheckOutDate: '#hdnCheckOutDate',
        hdnAccomodation: '#hdnAccomodation',
        hdnGroupRoom: '#hdnGroupRoom',
        hdnConferenceRoom:'#hdnConferenceRoom',
        hdnDateTime: '#hdnDateTime',
        hdnParticipants: '#hdnParticipants',
        hdnRequest:'#hdnRequest',
        hdnPensandNotepads: '#hdnPensandNotepads',
        hdnScreen: '#hdnScreen',
        hdnLCDProjector: '#hdnLCDProjector',
        hdnFlipchart: '#hdnFlipchart',
        hdnPinboard: '#hdnPinboard',
        hdnMeetingKit: '#hdnMeetingKit',
        hdnWhiteboard: '#hdnWhiteboard',
        hdnISDN: '#hdnISDN',
        hdnInternet: '#hdnInternet',
        hdnMicrophone: '#hdnMicrophone',
        hdnWirelessMicrophone: '#hdnWirelessMicrophone',
        hdnSoundSystem: '#hdnSoundSystem',
        hdnLectern: '#hdnLectern',
        hdnmorning_two_drinks: '#hdnmorning_two_drinks',
        hdnmorning_drinks_flat: '#hdnmorning_drinks_flat',
        hdnmorning_coffee_and_tea: '#hdnmorning_coffee_and_tea',
        hdnmorning_welcome_coffee_and_tea: '#hdnmorning_welcome_coffee_and_tea',
        hdnmorning_coffee_and_tea_break: '#hdnmorning_coffee_and_tea_break',
        hdnmidday_coffee_and_tea_break: '#hdnmidday_coffee_and_tea_break',
        hdnsecond_coffebreak: '#hdnsecond_coffebreak',
        hdnevening_dinner_buffet: '#hdnevening_dinner_buffet',
        hdnevening_dinner_one_dish: '#hdnevening_dinner_one_dish',
        hdnevening_dinner_two_course_meal: '#hdnevening_dinner_two_course_meal',
        hdnevening_dinner_three_course_meal: '#hdnevening_dinner_three_course_meal',
        hdnevening_dinner_snack: '#hdnevening_dinner_snack',
        hdngala_dinner: '#hdngala_dinner',
        hdnevening_one_drink: '#hdnevening_one_drink',
        hdnevening_drinks_flat: '#hdnevening_drinks_flat',
        hdnyes: '#hdnYes',
        hdnno: '#hdnNo',
        hdnEquipment: '#hdnEquipment',
        hdnFood: '#hdnFood',
        hdnmidday_drinks_flat: '#hdnmidday_drinks_flat',
        hdnmidday_lunch_buffet: '#hdnmidday_lunch_buffet',
        hdnmidday_lunch_one_dish: '#hdnmidday_lunch_one_dish',
        hdnmidday_lunch_three_course_meal: '#hdnmidday_lunch_three_course_meal',
        hdnmidday_lunch_two_course_meal: '#hdnmidday_lunch_two_course_meal',
        hdnmidday_one_drink: '#hdnmidday_one_drink',
        hdnmidday_lunch_snack: '#hdnmidday_lunch_snack',
        MandateRadius: '#hdnMandateRadius',
        MandateDestination: '#hdnMandateDestination',
        hdnBook: '#hdnBook',
        hdnhotelLabel: "#hdnHotelLabel",
        hdnRestaurantLabel: "#hdnRestaurantLabel",
        hdnLocationLabel: "#hdnLocationLabel",
        liResourceName: "#liResourceName",
        liNoofRoom: "#liNoofRoom",
        liNoofConference: "#liNoofConference",
        liSizeofLargestroom: "#liSizeofLargestroom",
        liDistance: "#liDistance",
        liHotelStar: "#liHotelStar",
        liResNoofConference: "#liResNoofConference",
        liResCapacityOflargestRoom: "#liResCapacityOflargestRoom",
        liCapacityOfRestaurant: "#liCapacityOfRestaurant",
        lilocNoofConference: "#lilocNoofConference",
        lilocSizeofLargestroom: "#lilocSizeofLargestroom",
        liRentalCost: "#liRentalCost",
        hdnSortBy: "#hdnSortBy",
        hdnResourceName: "#hdnResourceName",
        hdnDestinationRadius: "#hdnDestinationRadius",
        hdnCountryCity: "#hdnCountryCity",
        hdnDestinationValidation: "#hdnDestinationValidation",
        imgCurrentUser: "#imgCurrentUser",
        rfpinfotitle: "#rfpinfotitle",
        Accotab_tab: "#Accotab-tab",
        Conftab_tab: "#Conftab-tab",
        Grptab_tab: "#Grptab-tab",
        divider1:"#divider1",
        searchCountryContent: '#searchCountryContent',
        searchDestinationContent: '#searchDestinationContent'

    }
};

var $v = {

    variables: {
        stars: [],
        totalroom: "",
        Rtotalroom: "",
        totalcroom: "",
        parkingspace: "",
        Ltotalcroom: "",
        sizecroom: "",
        Rsizecroom: "",
        Lsizecroom: "",
        roomcapacity: "",
        roomheight: "",
        rentalcost: "",
        restaurantcapacity: "",
        minimumturnover: "",
        multikitchen: "",
        multisuitableforR: "",
        multisuitableforL: "",
        multichain: "",
        multilocationtype: "",
        multirestauranttype: "",
        multihoteltype: "",
        multicountry: "",
        city: "",
        ddlradius: null,
        recordsDisplayed: 0,
        totalRecords: 0,
        sidemap: null,
        infowindow: null,
        markers: [],
        nearbymarkers: [],
        bounds: null,
        center: [],
        i: 0,
        j: 0,
        Isopen: 0,
        scrollTrigger: 850,
        Isfurtherfilter: false,
        IsLUfilter: false,
        LUType: '',
        FFdistance: 0,
        FFname: "",
        FFlat: 0,
        FFlng: 0,
        NeedSearch: true,
        mapCentre: null,
        templat: null,
        templng: null,
        tempzoom: null,
        IsHotel: null,
        IsRestaurant: null,
        IsLocation: null,
        historyCriteria: null
    }
}

var $u = {

    path: {
        loaddata: 'Search/GetResourcesList',
        loadSearchHistoryCriteria: 'Search/GetSearchCriteriaFromHistory',
        GetSetSelectedLookupItems: 'Search/GetSelectedLookUpList',
        loadimgdata: 'Search/GetImageData',
        GetNearByRestaurantList: 'Search/GetNearByRestaurantList',
        GetNearByHotelList: 'Search/GetNearByHotelList',
        GetNearByLocationList: 'Search/GetNearByLocationList',
        GetHotelInfo: 'Search/GetHotelInfo',
        GetRestaurantInfo: 'Search/GetRestaurantInfo',
        GetLocationInfo: 'Search/GetLocationInfo',
        GetSearchInfo: 'LookUp/GetSearchInfo',
        GetCountryList: 'LookUp/CountryList',
        GetChainList: 'LookUp/ChainList',
        GetHotelTypeList: 'LookUp/HotelTypeList',
        GetRestaurantTypeList: 'LookUp/RestaurantTypeList',
        GetLocationTypeList: 'LookUp/LocationTypeList',
        GetKitchenList: 'LookUp/KitchenList',
        GetSuitableForList: 'LookUp/SuitableForList',
        GetRadiusList: 'LookUp/RadiusList',
        GetCartList: 'Search/GetCartList',
        GetCartCount: 'Search/GetCartCount',
        RemoveResourceFromTheSelection: 'Request/RemoveResourceFromTheSelection',
        AddResourceToRFP: 'Request/AddResourceToRFP',
        AddToCart: 'request/MarkResourceForTheRequest',
        GetMyRFPList: 'RequestDetail/GetRFPMiceRequestWithProposal',
        GetChangePassword: '/Info/changepassword',
        GetEditProfile: '/Info/editprofile',
        GetMyRFPResourceList: '/RequestDetail/GetRFPDetail',
        BookOrder: '/Request/BookedOrder',
        AbortOrder: '/Request/AbortOrder',
        FillAbortReason: 'LookUp/AbortReason',
        GetResourcesDetails: 'Search/GetRFPResourceList'
    }
}

var $bind = {

    jrange: function (id, from, to, step, scale, format, showLabels, isRange, width, type, isevent) {

        $(id).jRange({
            from: from,
            to: to,
            step: step,
            scale: scale,
            format: format,
            showLabels: showLabels,
            isRange: isRange,
            width: width,
            ondragend: function (myvalue) {
                var min = myvalue.split(",")[0];
                var max = myvalue.split(",")[1];
                this.domNode.find(".lowValue").val(min);
                this.domNode.find(".highValue").val(max);
                if (isevent) {
                    $search.applysearch(type);
                }
            },
            onbarclicked: function (myvalue) {

                if (isevent) {
                    $search.applysearch(type);
                }
            }
        });

        $(id).next("div").find("div").find(".lowValue").val($(id).next("div").find("div").find("div.pointer-label.low").text());
        $(id).next("div").find("div").find(".highValue").val($(id).next("div").find("div").find("div.pointer-label.high").text());

        $(id).next("div").find("div").find(".lowValue").attr("id", "txt" + id.replace("#", "") + "low");
        $(id).next("div").find("div").find(".highValue").attr("id", "txt" + id.replace("#", "") + "high");

        $(id).next("div").find("div").find(".lowValue").change(function () {
            $bind.SetSliderValue(this, isevent);
        });

        $(id).next("div").find("div").find(".highValue").change(function () {
            $bind.SetSliderValue(this, isevent);
        });

    },
    StarE: function (e, f) {

        if ($(e).hasClass('fl-group__btn--stars-active')) {
            $(e).removeClass('fl-group__btn--stars-active').addClass('fl-group__btn--stars');
        } else {
            $(e).addClass('fl-group__btn--stars-active').removeClass('fl-group__btn--stars');
        }

        if (f) {
            $search.applysearch("H");
        }

    },
    PopOver: function (e) {

        //LIMA


        // Hotels

        $($c.controls.aNoofRoom).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.aNoofConference).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.aSizeofLargestroom).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.aParkingSpaces).popover({
            content: "",
            placement: 'top',
            html: true
        });

        // Restaurants

        $($c.controls.RaNoofRoom).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.RaSizeofLargestroom).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.RaCapacityofroom).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.RaCapacityofrestaurant).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.RaMinimumturnover).popover({
            content: "",
            placement: 'top',
            html: true
        });

        // Locations

        $($c.controls.LaNoofConference).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.LaSizeofLargestroom).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.Laroomheight).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.Larentalcost).popover({
            content: "",
            placement: 'top',
            html: true
        });

    },
    PopOvero: function (e) {

        // Hotels

        $($c.controls.aNoofRoomo).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.aNoofConferenceo).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.aSizeofLargestroomo).popover({
            content: "",
            placement: 'top',
            html: true
        });

        // Restaurants

        $($c.controls.RaNoofRoomo).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.RaSizeofLargestroomo).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.RaCapacityofroomo).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.RaCapacityofrestauranto).popover({
            content: "",
            placement: 'top',
            html: true
        });

        // Locations

        $($c.controls.LaNoofConferenceo).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.LaSizeofLargestroomo).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.Laroomheighto).popover({
            content: "",
            placement: 'top',
            html: true
        });

        $($c.controls.Larentalcosto).popover({
            content: "",
            placement: 'top',
            html: true
        });

    },
    openmodel: function () {
        $('#modal-container').removeAttr('class').addClass("one");
        $('body').addClass('modal-active');
    },
    closemodel: function () {
        $('#modal-container').addClass('out');
        $('body').removeClass('modal-active');
    },
    showdiv: function () {

        setTimeout(function () {
            $($c.controls.middlecontent).removeClass("col-lg-9 col-md-8 col-sm-12 col-xs-12").addClass("col-lg-4 col-md-4 col-sm-6 col-xs-0");

            $($c.controls.Cards).removeClass("col-lg-4 col-md-6 col-sm-6 col-xs-12").addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12 pointer");

            $($c.controls.card5).addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12 p0");

            $($c.controls.divshowtotal).removeClass().addClass("col-lg-6 col-md-12 col-sm-12 col-xs-12 ac mtpb15");

            $($c.controls.divshowsortby).removeClass().addClass("col-lg-6 col-md-12 col-sm-12 col-xs-12 ac mtpb15");

            $($c.controls.divshowstartrfp).removeClass().addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12 ac ptpb10");

            $($c.controls.addtocart).addClass("smalladdtocart");

            $('.PartialMiddleContent ' + $c.controls.Mh75).removeClass("card-info-height");

            $($c.controls.imagediv).removeClass("col-lg-12 col-md-12 col-sm-12 col-xs-12 largeimage").addClass("col-lg-5 col-md-5 col-sm-5 col-xs-5 mt15");

            $('.PartialMiddleContent '+ $c.controls.contentdiv).removeClass("col-lg-12 col-md-12 col-sm-12 col-xs-12").addClass("col-lg-7 col-md-7 col-sm-7 col-xs-7");

        }, 200);

        setTimeout(function () {

            $($c.controls.drawmap).removeClass("displaynone");

            $('.PartialMiddleContent '+ $c.controls.cansmall).addClass("displaynone");

        }, 200);

        //$($c.controls.drawcontents).removeClass().addClass("row mo col-lg-6 col-md-6 col-sm-12 col-xs-12");
        //$($c.controls.drawbutton).removeClass().addClass("row mo col-lg-6 col-md-6 col-sm-12 col-xs-12");

        //$('.paneldiv').removeClass("col-lg-6").addClass("col-lg-12");

        //$($c.controls.hotelname).removeClass("ellipsis").addClass("shortellipsis");

        //$($c.controls.copyurl).hide();
        //$($c.controls.savefilter).hide();
        //$($c.controls.sharefilter).hide();


    },
    hidediv: function () {

        $($c.controls.middlecontent).removeClass("col-lg-4 col-md-4 col-sm-6 col-xs-0").addClass("col-lg-9 col-md-8 col-sm-12 col-xs-12");

        $($c.controls.Cards).removeClass("col-lg-12 col-md-12 col-sm-12 col-xs-12 pointer").addClass("col-lg-4 col-md-6 col-sm-6 col-xs-12");

        $($c.controls.card5).removeClass("col-lg-12 col-md-12 col-sm-12 col-xs-12 p0");

        $($c.controls.divshowtotal).removeClass().addClass("col-lg-3 col-md-3 col-sm-3 col-xs-12 mtpb15");

        $($c.controls.divshowsortby).removeClass().addClass("col-lg-6 col-md-6 col-sm-6 col-xs-6 mtpb15 ac");

        $($c.controls.divshowstartrfp).removeClass().addClass("col-lg-3 col-md-3 col-sm-3 col-xs-12 ptpb10");

        //$($c.controls.cansmall).removeClass("displaynone");
        $('.PartialMiddleContent ' + $c.controls.cansmall).removeClass("displaynone");
        $($c.controls.addtocart).removeClass("smalladdtocart");

        $($c.controls.Mh75).addClass("card-info-height");

        $($c.controls.imagediv).removeClass("col-lg-5 col-md-5 col-sm-5 col-xs-5 mt15").addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12 largeimage");

        $($c.controls.contentdiv).removeClass("col-lg-7 col-md-7 col-sm-7 col-xs-7").addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12");

        setTimeout(function () {
            $($c.controls.drawmap).addClass("displaynone");
        }, 200);

        //$($c.controls.drawmap).removeClass("col-lg-6 col-md-6 col-sm-12 col-xs-12");

        //$($c.controls.drawcontents).removeClass().addClass("row mo");
        //$($c.controls.drawbutton).removeClass().addClass("row mo");

        //$('.paneldiv').addClass("col-lg-6").removeClass("col-lg-12");

        //$($c.controls.hotelname).removeClass("shortellipsis").addClass("largeellipsis");

        //$($c.controls.copyurl).show();
        //$($c.controls.savefilter).show();
        //$($c.controls.sharefilter).show();
    },
    top100: function () {
        $('html, body').animate({
            scrollTop: 100
        }, 0);
    },
    handleEnter: function (e) {

        var keycode = (e.keyCode ? e.keyCode : e.which);

        if (keycode == '13') {

            if ($("#btnsearch").val() != undefined) {
                $("#btnsearch").click();
            } else {
                $($c.controls.btnadvancehotel).click();
            }
        }
    },
    SetSliderValue: function (e, flag) {
        var vals = '';
        var id = e.id.replace("txt", "").replace("high", "").replace("low", "");
        if (e.className.indexOf("lowValue") > 0) {
            vals = e.value;

            var clsName = e.parentNode.className;
            vals += "," + $("." + clsName).find("#txt" + id + "high").val();
        }
        else if (e.className.indexOf("highValue") > 0) {
            var clsName = e.parentNode.className;
            vals = $("." + clsName).find("#txt" + id + "low").val();

            vals += "," + e.value;


        }


        if (vals != '') {
            $("#" + id).jRange('setValue', vals);
        }

        if (flag) {
            $search.applysearch('');
        }
    },
    SetSlider: function (e, flag) {

        var vals = $("#min" + $(e).attr('data-type')).val() + "," + $("#max" + $(e).attr('data-type')).val();

        $("#" + $(e).attr('for')).jRange('setValue', vals);

        if ($(e).attr('data-type') == 'nr') {
            $v.variables.totalroom = vals;
        }
        else if ($(e).attr('data-type') == 'ncr') {
            $v.variables.totalcroom = vals;
        }
        else if ($(e).attr('data-type') == 'scr') {
            $v.variables.sizecroom = vals;
        }
        else if ($(e).attr('data-type') == 'rnr') {
            $v.variables.Rtotalroom = vals;
        }
        else if ($(e).attr('data-type') == 'sr') {
            $v.variables.Rsizecroom = vals;
        }
        else if ($(e).attr('data-type') == 'cr') {
            $v.variables.roomcapacity = vals;
        }
        else if ($(e).attr('data-type') == 'cre') {
            $v.variables.restaurantcapacity = vals;
        }
        else if ($(e).attr('data-type') == 'lncr') {
            $v.variables.Ltotalcroom = vals;
        }
        else if ($(e).attr('data-type') == 'lscr') {
            $v.variables.Lsizecroom = vals;
        }
        else if ($(e).attr('data-type') == 'rh') {
            $v.variables.roomheight = vals;
        }
        else if ($(e).attr('data-type') == 'rc') {
            $v.variables.rentalcost = vals;
        }
        else if ($(e).attr('data-type') == 'ps') {
            $v.variables.parkingspace = vals;
        }

        $('[data-original-title]').popover('hide');

        if (flag) {
            $search.applysearch($(e).attr('data-call'));
        }

    },
    GetSlider: function (e, flag) {

        var minval = $("#" + $(e).attr('for')).val().split(",")[0];
        var maxval = $("#" + $(e).attr('for')).val().split(",")[1];
        var myPopover = $("#" + $(e).attr('id')).data('bs.popover');

        var bindToMe = "<div>";
        bindToMe += "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-4 p5'><label for='min' class='t10'>" + $($c.controls.sMIn).val() + "</label><input class='form-control p0' min='0' value=" + minval + " type='number' id='min" + $(e).attr('data-type') + "'></div>";
        bindToMe += "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-4 p5'><label for='min' class='t10'>" + $($c.controls.sMax).val() + "</label><input class='form-control p0' min='0' value=" + maxval + " type='number' id='max" + $(e).attr('data-type') + "'></div>";
        bindToMe += "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-4 pt10 ac'><label for='min' class='popstype'><button type='button' class='close' for='" + $(e).attr('id') + "' onclick='$bind.closehover(this," + flag + ");'>×</button></label><a role='button' onclick='$bind.SetSlider(this," + flag + ");' class='alink' data-call='" + $(e).attr('data-call') + "' for='" + $(e).attr('for') + "' data-type='" + $(e).attr('data-type') + "'>" + $($c.controls.sSet).val() + "</a></div>";
        bindToMe += "</div>";

        myPopover.options.html = true;
        myPopover.options.content = bindToMe;



        //$("#" + $(e).attr('id')).popover('show');
    },
    closehover: function (e, flag) {

        $("#" + $(e).attr('for')).popover('hide');
        // $("#" + $(e).attr('for')).click();        
    },
    backToTop: function () {

        var scrollTop = $(window).scrollTop();

        if (scrollTop > $v.variables.scrollTrigger) {
            $($c.controls.backtotop).addClass('show');
        } else {
            $($c.controls.backtotop).removeClass('show');
        }

    },
    hotelJrange: function (w) {

        if (w) {

            $bind.jrange($c.controls.rstotalroom, 0, 6000, 1, [0, 1000, 2000, 3000, 4000, 5000, 6000], '%s', true, true, 250, "H", false);

            $bind.jrange($c.controls.rstotalconfroom, 0, 700, 1, [0, 100, 200, 300, 400, 500, 600, 700], '%s', true, true, 250, "H", false);

            $bind.jrange($c.controls.rsconfsize, 0, 15000, 1, [0, 3000, 6000, 9000, 12000, 15000], '%s', true, true, 250, "H", false);

        } else {

            $bind.jrange($c.controls.rslftotalroom, 0, 6000, 1, [0, 1000, 2000, 3000, 4000, 5000, 6000], '%s', true, true, 250, "H", true);

            $bind.jrange($c.controls.rslftotalconfroom, 0, 700, 1, [0, 100, 200, 300, 400, 500, 600, 700], '%s', true, true, 250, "H", true);

            $bind.jrange($c.controls.rslfconfsize, 0, 15000, 1, [0, 3000, 6000, 9000, 12000, 15000], '%s', true, true, 250, "H", true);

            $bind.jrange($c.controls.rslfparkingspace, 0, 1000, 1, [0, 200, 400, 600, 800, 1000], '%s', true, true, 250, "H", true);



        }
    },
    restaurantJrange: function (w) {

        if (w) {

            $bind.jrange($c.controls.Rrstotalroom, 0, 40, 1, [0, 10, 20, 30, 40], '%s', true, true, 250, "R", false);

            $bind.jrange($c.controls.Rrsroomsize, 0, 3000, 1, [0, 500, 1000, 1500, 2000, 2500, 3000], '%s', true, true, 250, "R", false);

            $bind.jrange($c.controls.Rrsroomcapacity, 0, 15000, 1, [0, 3000, 6000, 9000, 12000, 15000], '%s', true, true, 250, "R", false);

            $bind.jrange($c.controls.Rrsrestaurantcapacity, 0, 3000, 1, [0, 500, 1000, 1500, 2000, 2500, 3000], '%s', true, true, 250, "R", false);

        } else {

            $bind.jrange($c.controls.Rrslftotalroom, 0, 40, 1, [0, 10, 20, 30, 40], '%s', true, true, 250, "R", true);

            $bind.jrange($c.controls.Rrslfroomsize, 0, 3000, 1, [0, 500, 1000, 1500, 2000, 2500, 3000], '%s', true, true, 250, "R", true);

            $bind.jrange($c.controls.Rrslfroomcapacity, 0, 15000, 1, [0, 3000, 6000, 9000, 12000, 15000], '%s', true, true, 250, "R", true);

            $bind.jrange($c.controls.Rrslfrestaurantcapacity, 0, 3000, 1, [0, 500, 1000, 1500, 2000, 2500, 3000], '%s', true, true, 250, "R", true);

            $bind.jrange($c.controls.Rrslfminimumturnover, 0, 15000, 1, [0, 3000, 6000, 9000, 12000, 15000], '%s', true, true, 250, "R", true);

        }
    },
    locationJrange: function (w) {

        if (w) {

            $bind.jrange($c.controls.Lrstotalconfroom, 0, 220, 1, [0, 44, 88, 132, 176, 220], '%s', true, true, 250, "L", false);

            $bind.jrange($c.controls.Lrsconfsize, 0, 86000, 1, [0, 17200, 34400, 51600, 68800, 86000], '%s', true, true, 250, "L", false);

            $bind.jrange($c.controls.Lrsroomheight, 0, 50, 1, [0, 10, 20, 30, 40, 50], '%s', true, true, 250, "L", false);

            $bind.jrange($c.controls.Lrsrentalcost, 0, 200000, 1, [0, 40000, 80000, 120000, 160000, 200000], '%s', true, true, 250, "L", false);

        } else {

            $bind.jrange($c.controls.Lrslftotalconfroom, 0, 220, 1, [0, 44, 88, 132, 176, 220], '%s', true, true, 250, "L", true);

            $bind.jrange($c.controls.Lrslfconfsize, 0, 86000, 1, [0, 17200, 34400, 51600, 68800, 86000], '%s', true, true, 250, "L", true);

            $bind.jrange($c.controls.Lrslfroomheight, 0, 50, 1, [0, 10, 20, 30, 40, 50], '%s', true, true, 250, "L", true);

            $bind.jrange($c.controls.Lrslfrentalcost, 0, 200000, 1, [0, 40000, 80000, 120000, 160000, 200000], '%s', true, true, 250, "L", true);

        }
    },
    ClearPopupwindow: function () {
        $($c.controls.map).empty();
        $($c.controls.nearbylist).empty();
    },
    FilterLoader: function () {
        //$($c.controls.tabcontent).css('opacity', 0.5).fadeIn();
        common.displayLoader();
    },
    EFilterLoader: function () {
        //$($c.controls.tabcontent).css('opacity', 1.0).fadeIn();
        common.hideLoader();
    },
    unbindTooltip: function () {
        $('.show-tooltip').tooltip("hide");
        $('.show-tooltipb').tooltip("hide");
    },
    bindTooltip: function () {
        setTimeout(function() {
            $('.show-tooltip').tooltip({ placement: 'top' });
            $('.show-tooltipb').tooltip({ placement: 'bottom' });
        }, 100);
    },
    StartLoader: function (t) {

        if (t == "M") {
            $($c.controls.map).html("<div class='maploader' id='maploader' >");
        } if (t == "F") {
            $($c.controls.infoplace).html("<div class='maploader' id='maploader' >");
        } if (t == "U") {
            $($c.controls.userbody).html("<div class='maploader' id='maploader' >");
        } if (t == "MT5") {
            $($c.controls.popovercontent).css('opacity', 0.5);
        } if (t == "MT") {
            $($c.controls.popovercontent).css('opacity', 1.0);
        } if (t == "MTL") {
            $($c.controls.MyTripList).css('opacity', 0.5);
        } else {
            $($c.controls.divloader).show();
            $($c.controls.divloader).html("<div class='maploader' id='maploader' >");
        }
    },
    EndLoader: function (t) {

        $($c.controls.divloader).hide();
        $($c.controls.divloader).html("");

    },
    FormatObject: function (value) {
        return { id: value.Id, text: value.Name };
    },
    CommonHideShow: function (t) {

        // $($c.controls.drawmap).removeClass("displaynone");

        $($c.controls.typeclass).removeClass('col-lg-8 col-md-8 col-sm-6 col-xs-12').addClass('col-lg-7 col-md-7 col-sm-7 col-xs-12');

        $($c.controls.liSearchCountry).removeClass('col-lg-6 col-md-6 col-xs-6 col-sm-6');
        $($c.controls.liSearchDestination).removeClass('col-lg-6 col-md-6 col-xs-6 col-sm-6');

        $($c.controls.largemenu).removeClass().addClass('smallmenu largemenu');
        $($c.controls.bhoechie).removeClass('bhoechie-tab-container');

        $($c.controls.hidelabel).addClass('displaynone');
        $($c.controls.panelwhiteback).removeClass('mt20');
        $($c.controls.DivGeneralInfo).addClass('p0');

        $($c.controls.headersection).addClass('sectionclass');
        $($c.controls.headertag).hide();
        $($c.controls.advancefilter).hide();
        $($c.controls.nonadvancefilter).show();
        $($c.controls.btntoggle).removeClass("displaynone");
        $($c.controls.applylastfilter).addClass("hide");

        setTimeout(function () {
            $($c.controls.headertag).remove();
            $($c.controls.advancefilter).remove();
        }, 2000);

        $($c.controls.Ccontainer).removeClass("displaynone");


    },
    Events: function (t) {

        $($c.controls.chkihotel).click(function () {
            if ($bind.ShowHideLeftPanel()) {
                $v.variables.IsHotel = $($c.controls.chkihotel).is(":checked");
                $bind.commontxtevent();
            } else {
                $($c.controls.chkihotel).prop('checked', true);
            }
        });

        $($c.controls.chkirestaurant).click(function (e) {
            if ($bind.ShowHideLeftPanel()) {
                $v.variables.IsRestaurant = $($c.controls.chkirestaurant).is(":checked");
                $bind.commontxtevent();
            } else {
                $($c.controls.chkirestaurant).prop('checked', true);
            }
        });

        //$($c.controls.headermapview).click(function (e) {
        //    $($c.controls.closewindow).show(500);
        //    $($c.controls.closewindow).click();

        //    $bind.showdiv();
        //    $v.variables.Isopen = 1;
        //});

        $($c.controls.chkilocation).click(function (e) {
            if ($bind.ShowHideLeftPanel()) {
                $v.variables.IsLocation = $($c.controls.chkilocation).is(":checked");
                $bind.commontxtevent();
            } else {
                $($c.controls.chkilocation).prop('checked', true);
            }
        });

        $($c.controls.multihotelchaini).change(function () {
            $bind.commontxtevent();
        });

        $($c.controls.multihoteltypei).change(function () {
            $bind.commontxtevent();
        });

        //$($c.controls.ddlocountry).change(function () {
        //    $bind.commontxtevent();
        //});

        $($c.controls.multirestauranttypei).change(function () {
            $bind.commontxtevent();
        });

        $($c.controls.multirestaurantkitcheni).change(function () {
            $bind.commontxtevent();
        });

        $($c.controls.multirestaurantsuitablefori).change(function () {
            $bind.commontxtevent();
        });

        $($c.controls.multilocationtypei).change(function () {
            $bind.commontxtevent();
        });

        $($c.controls.multilocationsuitablefori).change(function () {
            $bind.commontxtevent();
        });

        $($c.controls.multihotelchaini, $c.controls.multihoteltypei).change(function () {
            $bind.commontxtevent();
        });

        //$($c.controls.txtocity).click(function () {
        //    $bind.commontxtevent();
        //});

    },
    CommonStartFlow: function () {

        setTimeout(function () {
            $Map.initializeSideM();
            $search.loadleftfilter();
        }, 100);

        setTimeout(function () {
            $bind.top100();
        }, 150);

    },
    commontxtevent: function () {
        $($c.controls.sorted).html($($c.controls.hdnSortBy).val() + ": " + $($c.controls.hdnResourceName).val() + " &nbsp; <span data-sort='resourceasc' class='sortby glyphicon glyphicon-arrow-up pointer bluefont'></span> <span class='caret'></span>");

        $($c.controls.liResourceName + " [data-sort='resourcedesc']").removeClass("bluefont");
        $($c.controls.liResourceName + " [data-sort='resourceasc']").addClass("bluefont");
        $search.applysearch();

    },
    Bindneardistance: function (id, lng, lat, name, type, distance, from) {

        $($c.controls.afurtherfilter).removeAttr("href");
        $($c.controls.afurtherfilter).attr("href", ("/Discover/index?type=" + from + "&for=FurtherF&lng=" + lng + "&lat=" + lat + "&name=" + encodeURIComponent(name) + "&distance=" + distance));

        var nearby = '', namer = '';

        if (from == "H") {
            namer = $($c.controls.sNearByHotel).val();
            nearby = '<li class="list-group-item active linearheader"><span class="glyphicon glyphicon-list"></span>&nbsp;' + namer + '&nbsp;';
            nearby += '<select class="ddldistance fr" onchange="$search.GetNearByHotelList(' + id + ',' + '\'' + lng + '\'' + ',' + '\'' + lat + '\'' + ',' + '\'' + name + '\'' + ', ' + '\'' + type + '\'' + ', ' + distance + ');">';
        } else if (from == "R") {
            namer = $($c.controls.sNearByRestaurant).val();
            nearby = '<li class="list-group-item active linearheader"><span class="glyphicon glyphicon-list"></span>&nbsp;' + namer + '&nbsp;';
            nearby += '<select class="ddldistance fr" onchange="$search.GetNearByRestaurantList(' + id + ',' + '\'' + lng + '\'' + ',' + '\'' + lat + '\'' + ',' + '\'' + name + '\'' + ', ' + '\'' + type + '\'' + ', ' + distance + '); ">';
        } else if (from == "L") {
            namer = $($c.controls.sNearByLocation).val();
            nearby = '<li class="list-group-item active linearheader"><span class="glyphicon glyphicon-list"></span>&nbsp;' + namer + '&nbsp;';
            nearby += '<select class="ddldistance fr" onchange="$search.GetNearByLocationList(' + id + ',' + '\'' + lng + '\'' + ',' + '\'' + lat + '\'' + ',' + '\'' + name + '\'' + ', ' + '\'' + type + '\'' + ', ' + distance + ');">';
        }

        var kms = $($c.controls.NearByKM).val().split(",");

        nearby += '<option  value="' + distance + '">' + distance + ' Km</option>';


        $(kms).each(function (i) {
            if (distance == kms[i]) { return; }
            nearby += '<option  value="' + kms[i] + '">' + kms[i] + ' Km</option>';
        });

        nearby += '</select>';
        nearby += '</li>';

        return nearby;
    },
    OpenDetailOnClick: function (e) {
        if ($v.variables.Isopen == 0) {
            window.open($(e).attr('data-url').toString(), '_blank');
        }
    },
    ShowMapIcon: function (e, isDelete) {

        if ($v.variables.Isopen == 0 && !isDelete) {
            window.open($(e).attr('data-url').toString(), '_blank');
        }

        var markers = $v.variables.markers;
        // var type = $(e).attr('data-type').toString();

        var i = parseInt($(e).attr('data-card').toString());
        if (markers[i] != undefined && markers[i] != null && markers[i] != "") {
            google.maps.event.trigger(markers[i], 'click');
        }

        //for (var i = 0; i < markers.length; i++) {

        //    if (i.toString() == $(e).attr('data-card').toString()) {

        //        if (markers[i] != undefined && markers[i] != null && markers[i] != "") {

        //            if (type == "H") {
        //                markers[i].setIcon('\\Images\\ShowHMap.png');
        //            } else if (type == "R") {
        //                markers[i].setIcon('\\Images\\ShowRMap.png');
        //            } else if (type == "L") {
        //                markers[i].setIcon('\\Images\\ShowLMap.png');
        //            }

        //            google.maps.event.trigger(markers[i], 'click');
        //            markers[i].setAnimation(google.maps.Animation.DROP);

        //            //setTimeout(function () {
        //            //   markers[i].setAnimation(null);
        //            //}, 700);

        //            //var divRightValue = ($("#drawmap").css("right")).replace("px", "");
        //            //if (divRightValue > -10 && divRightValue <= 0) {
        //            //    $('#drawcontents .zoomclass' + i + " .card").effect("highlight", { color: "rgb(255, 211, 8)" }, 3000);
        //            //}

        //            //$('.zoomclass' + i + " .card").effect("highlight", { color: "rgb(255, 211, 8)" }, 3000);

        //        }

        //    }
        //    else {

        //        if (markers[i] != undefined && markers[i] != null && markers[i] != "") {

        //            var itype = markers[i].getLabel();

        //            if (itype.text == "H") {
        //                markers[i].setIcon('\\Images\\hotelm.png');
        //            } else if (itype.text == "R") {
        //                markers[i].setIcon('\\Images\\restaurantm.png');
        //            } else if (itype.text == "L") {
        //                markers[i].setIcon('\\Images\\locationm.png');
        //            }

        //            markers[i].setAnimation(null);
        //        }

        //    }

        //}

    },
    ShowNearByMapIcon: function (e) {

        $(".list-group-item").removeClass("liback");
        $(e).addClass("liback");

        var markers = $v.variables.nearbymarkers;
        var type = $(e).attr('data-type').toString();

        for (var i = 0; i < markers.length; i++) {
            // console.log(markers[i]);
            //if (i.toString() == $(e).attr('data-card').toString()) {
            if (markers[i].id.toString() == $(e).attr('data-id').toString()) {

                if (markers[i] != undefined && markers[i] != null && markers[i] != "") {

                    if (type == "H") {
                        markers[i].setIcon('\\Images\\ShowHMap.png');
                    } else if (type == "R") {
                        markers[i].setIcon('\\Images\\ShowRMap.png');
                    } else if (type == "L") {
                        markers[i].setIcon('\\Images\\ShowLMap.png');
                    }

                    google.maps.event.trigger(markers[i], 'click');
                    markers[i].setAnimation(google.maps.Animation.DROP);
                    //markers[i].setAnimation(google.maps.Animation.BOUNCE);

                  //  $('.nearzoomclass' + i + "").effect("highlight", { color: "rgb(255, 211, 8)" }, 3000);

                }

            }
            else {

                if (markers[i] != undefined && markers[i] != null && markers[i] != "") {

                    if (type == "H") {
                        markers[i].setIcon('\\Images\\hotelm.png');
                    } else if (type == "R") {
                        markers[i].setIcon('\\Images\\restaurantm.png');
                    } else if (type == "L") {
                        markers[i].setIcon('\\Images\\locationm.png');
                    }

                    markers[i].setAnimation(null);
                    //  marker[i].infowindow.close();
                }

            }

        }

    },
    URL: function (e) {
        return $($c.controls.hdnAPIBaseURL).val() + e;
    },
    ImageURL: function (e) {
        return $($c.controls.hdnImageBaseURL).val() + e;
    },
    ShowHideLeftPanel: function () {

        var ishotel = $($c.controls.chkihotel).is(":checked");
        var isrestaurant = $($c.controls.chkirestaurant).is(":checked");
        var islocation = $($c.controls.chkilocation).is(":checked");

        var flag = false;

        if (!ishotel && !isrestaurant && !islocation) {
            return flag;
        }

        if (ishotel) {
            $($c.controls.hotelfilter).removeClass("disablelayout");
        } else {
            $($c.controls.hotelfilter).addClass("disablelayout");
        }

        if (isrestaurant) {
            $($c.controls.restaurantfilter).removeClass("disablelayout");
        } else {
            $($c.controls.restaurantfilter).addClass("disablelayout");
        }

        if (islocation) {
            $($c.controls.locationfilter).removeClass("disablelayout");
        } else {
            $($c.controls.locationfilter).addClass("disablelayout");
        }

        return true;

    },
    ShowHideBottomPanel: function () {

        var ishotel = $($c.controls.chkhotel).is(":checked");
        var isrestaurant = $($c.controls.chkrestaurant).is(":checked");
        var islocation = $($c.controls.chklocation).is(":checked");

        var flag = false;

        if (!ishotel && !isrestaurant && !islocation) {
            return flag;
        }

        if (ishotel) {

            $($c.controls.hotel).removeClass("disablelayout");
            if (!flag) {
                flag = true;
                //   $($c.controls.aihotel).click();
            }
        } else {
            $($c.controls.hotel).addClass("disablelayout");
            $search.resetcontrols('O', 'H');
        }

        if (isrestaurant) {
            $($c.controls.restaurant).removeClass("disablelayout");
            if (!flag) {
                flag = true;
                //   $($c.controls.airestaurant).click();
            }
        } else {
            $($c.controls.restaurant).addClass("disablelayout");
            $search.resetcontrols('O', 'R');
        }

        if (islocation) {
            $($c.controls.location).removeClass("disablelayout");
            if (!flag) {
                flag = true;
                //$($c.controls.ailocation).click();
            }
        } else {
            $($c.controls.location).addClass("disablelayout");
            $search.resetcontrols('O', 'L');
        }

        return flag;

    },
    ManageTab: function (id) {
        $($c.controls.tabs).removeClass("active");
        $(id).addClass("active");
    },
    ClickStopPropagation: function (event) {
        if (event != null) {
            event.stopPropagation();
        }
    },
    addResourceToRFP: function (resourceIds, resourceType, modelId) {
        var rfpId = $($rfp.variables.RfpId).val();
        var urlValue;
        if (resourceIds == null || resourceIds.length === 0) {
            return;
        }
        if (window.location.href.toString().toLocaleLowerCase().indexOf("startrfp") > 0 && $($rfp.variables.RfpId) != null) {
            urlValue = $bind.URL($u.path.AddResourceToRFP) + "?resourceId=" + resourceIds + "&resourceType=" + resourceType + "&rfpId=" + rfpId;
        }
        common.displayLoader();
        $.ajax({
            type: "POST",
            url: urlValue,
            data: {},
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                $(modelId).modal("hide");
                var activeLink = '';
                setTimeout(function () {
                    common.hideLoader();
                }, 1000);
                if (response != undefined && response != null && response.code == "701") {
                    if (location.href.toLocaleLowerCase().indexOf("startrfp") > 0) {
                        if ($($rfp.controls.aRFPHotel)[0].className.indexOf("active") > 0) {
                            activeLink = "Hotels";
                        }
                        else if ($($rfp.controls.aRFPRestaurant)[0].className.indexOf("active") > 0) {
                            activeLink = "Restaurants"
                        }
                        else if ($($rfp.controls.aRFPLocation)[0].className.indexOf("active") > 0) {
                            activeLink = "Location"
                        }
                        window.localStorage.setItem("activeLink", activeLink);
                    }
                    location.reload();
                }
            }
        });



    },
    deletecartresource: function (event, obj) {
        event.preventDefault();
        event.stopPropagation();
        BootstrapDialog.confirm($($c.controls.sDeleteConfirm).val(), function (result) {

            if (result) {

                var id = $(obj).attr("data-id");
                var type = $(obj).attr("data-type");

                $("#" + id).css('opacity', 0.5);

                var urlValue = $bind.URL($u.path.RemoveResourceFromTheSelection) + "?resourceId=" + id + "&resourceType=" + type;
                ////INSPIRE-102
                ////If Resource is linked with rfp, then pass rfpid with remove resouce action method 
                if (window.location.href.toString().toLocaleLowerCase().indexOf("startrfp") > 0 && $($rfp.variables.RfpId) != null) {
                    urlValue = $bind.URL($u.path.RemoveResourceFromTheSelection) + "?resourceId=" + id + "&resourceType=" + type + "&rfpId=" + $($rfp.variables.RfpId).val();
                }

                $.ajax({
                    url: urlValue,
                    type: "DELETE",
                    dataType: "json",
                    contentType: 'application/json; charset=utf-8',
                    async: true,
                    cache: false,
                    success: function (response) {
                        var activeLink = '';
                        if (response != undefined && response != null && response.code == "702") {
                            if (location.href.toLocaleLowerCase().indexOf("startrfp") > 0) {
                                if ($($rfp.controls.aRFPHotel)[0].className.indexOf("active") > 0) {
                                    activeLink = "Hotels";
                                }
                                else if ($($rfp.controls.aRFPRestaurant)[0].className.indexOf("active") > 0) {
                                    activeLink = "Restaurants"
                                }
                                else if ($($rfp.controls.aRFPLocation)[0].className.indexOf("active") > 0) {
                                    activeLink = "Location"
                                }
                                window.localStorage.setItem("activeLink", activeLink);
                            }


                            location.reload();


                        } else {
                            $managecode.Decision(false, "", response.code, response.message, "Delete", false);
                        }

                    },
                    error: function (req, status, error) {
                        $managecode.Decision(true, data, "", "", "error", false);
                        $bind.EFilterLoader();
                        $($c.controls.mloader).hide();
                    }
                });
            }
        }, $($c.controls.sDeleteConfirmTitle).val(), $($c.controls.sDeleteConfirmOk).val(), $($c.controls.sDeleteConfirmCancel).val());
    },
    showhidemap: function () {
        var $mapIcon = $($c.controls.headermapview + ' #map-icon');
        var $listIcon = $($c.controls.headermapview + ' #list-icon');
        var $mapAndListIconTitle = '';
        if ($v.variables.Isopen == 1) {
            $bind.hidediv();
            $v.variables.Isopen = 0;
            $mapIcon.show();
            $listIcon.hide();
            $mapAndListIconTitle = $mapIcon.data('title');
            $($c.controls.headermapview).removeClass('list-view-toggle');
        } else {
            $bind.showdiv();
            $v.variables.Isopen = 1;
            $mapIcon.hide();
            $listIcon.show();
            $mapAndListIconTitle = $listIcon.data('title');
            $($c.controls.headermapview).addClass('list-view-toggle');
        }
        $($c.controls.headermapview).attr('data-original-title', $mapAndListIconTitle);
        $($c.controls.headermapview).tooltip("hide");
        setTimeout(function () {
            $($c.controls.headermapview).tooltip("show");
        }, 300);
    },
    bindInitialMapIcon: function () {
        var $mapIcon = $($c.controls.headermapview + ' #map-icon');
        var $listIcon = $($c.controls.headermapview + ' #list-icon');
        var $mapAndListIconTitle = '';
        if ($v.variables.Isopen == 1) {
            $mapIcon.hide();
            $listIcon.show();
            $mapAndListIconTitle = $listIcon.data('title');
            $($c.controls.headermapview).addClass('list-view-toggle');
        } else {
            $mapIcon.show();
            $listIcon.hide();
            $mapAndListIconTitle = $mapIcon.data('title');
            $($c.controls.headermapview).removeClass('list-view-toggle');
        }
        $($c.controls.headermapview).attr('data-original-title', $mapAndListIconTitle);
    },
    RedirectStartRFP: function () {
        window.localStorage.removeItem("activeLink");
        // location.href = "/RFP/StartRFP";
        window.open("/RFP/StartRFP", "_blank");
    },
    GetCartCount: function () {

        $($c.controls.cartCountloader).show();

        $.ajax({
            url: $bind.URL($u.path.GetCartCount),
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (response) {
                $($c.controls.cartCountloader).hide();
                if (response != undefined && response != null && response.code == "704" && response.data.totalrecord != 0) {
                    $($c.controls.bookmarkCartCount).text(response.data.totalrecord);
                    $($c.controls.bookmarkCartCount).show();
                } else {
                    $($c.controls.bookmarkCartCount).hide();
                    $($c.controls.bookmarkCartCount).text('');
                }

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
                $($c.controls.cartCountloader).hide();
            }
        });

    },
    setSearchFromHistory: function () {
        $v.variables.historyCriteria = null;
        $.ajax({
            url: $bind.URL($u.path.loadSearchHistoryCriteria),
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (result) {

                if (result.code != 704 && result.code != "704") {
                    $managecode.Decision(false, "", result.code, result.message, "Search History", false);
                }

                data = result.data;
                if (data != null) {
                    $v.variables.historyCriteria = data;

                    //$search.gatherprimaryinfo();
                    $bind.CommonHideShow();
                    $bind.CommonStartFlow();
                    $LookUp.BindDropdown($c.controls.ddlradius, $($c.controls.hdnselectradius).val(), $u.path.GetRadiusList, 0, 50, false, true);

                }

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    },
}

var $validation = {

    mandateprimary: function (type) {

        var flag = true;

        //var country = $($c.controls.ddlcountry).val();
        //var redcountry = $c.controls.s2idddlcountry;

        //if (country == undefined || country == "" || country == null) {
        //    $(redcountry).css('cssText', 'border-bottom-color:red !important');
        //    flag = false;
        //} else {
        //    $(redcountry).removeAttr('style');
        //}

        var name = $($c.controls.txtdestinationname).val();
        var redname = $c.controls.txtdestinationname;
        var radius = $($c.controls.ddlradius).val();
        var redradius = $c.controls.s2idddlradius;

        if (name != undefined && name != "" && name != null) {

            if (radius == undefined || radius == "" || radius == null) {
                $(redradius).css('cssText', 'border-color:red !important');
                flag = false;
                common.displayMessage($($c.controls.MandateRadius).val(), false);
            } else {
                $(redradius).removeAttr('style');
            }

        } else {
            $(redradius).removeAttr('style');
        }

        if (radius != undefined && radius != "" && radius != null) {

            if (name == undefined || name == "" || name == null) {
                $(redname).css('cssText', 'border-color:red !important');
                flag = false;
                common.displayMessage($($c.controls.MandateDestination).val(), false);
            } else {

                if (name != $($c.controls.txtdestinationname).attr("data-name")) {
                    $(redname).css('cssText', 'border-color:red !important');
                    flag = false;
                    common.displayMessage($($c.controls.hdnDestinationValidation).val(), false);
                }
                else {
                    $(redname).removeAttr('style');
                }
            }

        } else {
            $(redname).removeAttr('style');
        }

        return flag;
    },
    ValidateRights: function () {

        if (!ajaxRequest.CheckRights("H")) {
            $($c.controls.RHotel).hide();
            $($c.controls.aihotel).hide();
            $($c.controls.hotelpanel).hide();
            $v.variables.IsHotel = false;
            $($c.controls.chkhotel).prop('checked', false);
            $($c.controls.chkihotel).prop('checked', false);
        } else {
            $($c.controls.RHotel).show();
            $($c.controls.aihotel).show();
            $($c.controls.hotelpanel).show(500);
        }

        if (!ajaxRequest.CheckRights("R")) {
            $($c.controls.RRestaurant).hide();
            $($c.controls.airestaurant).hide();
            $($c.controls.restaurantpanel).hide();
            $v.variables.IsRestaurant = false;
            $($c.controls.chkrestaurant).prop('checked', false);
            $($c.controls.chkirestaurant).prop('checked', false);
        } else {
            $($c.controls.RRestaurant).show();
            $($c.controls.airestaurant).show();
            $($c.controls.restaurantpanel).show(500);
        }

        if (!ajaxRequest.CheckRights("L")) {
            $($c.controls.ailocation).hide();
            $($c.controls.RLocation).hide();
            $v.variables.IsLocation = false;
            $($c.controls.locationpanel).hide();
            $($c.controls.chklocation).prop('checked', false);
            $($c.controls.chkilocation).prop('checked', false);
        } else {
            $($c.controls.ailocation).show();
            $($c.controls.RLocation).show();
            $($c.controls.locationpanel).show(200);
        }

        $bind.ShowHideBottomPanel();
    }
}

$(document).ready(function () {

    if (window.location.href.toLowerCase().indexOf("resetpassword")<=0 &&(ajaxRequest.getAuthorizationToken() == undefined || ajaxRequest.getAuthorizationToken() == null)) {
        location.href = "/login";
    }

    $bind.GetCartCount();

    setTimeout(function () { $validation.ValidateRights(); }, 1);

    $($c.controls.form).find('input, textarea').on('keyup blur focus', function (e) {

        var $this = $(this),
            $parent = $this.parent();

        if (e.type == 'keyup') {
            if ($this.val() == '') {
                $parent.addClass('js-hide-label');
            } else {
                $parent.removeClass('js-hide-label');
            }
        }
        else if (e.type == 'blur') {
            if ($this.val() == '') {
                $parent.addClass('js-hide-label');
            }
            else {
                $parent.removeClass('js-hide-label').addClass('js-unhighlight-label');
            }
        }
        else if (e.type == 'focus') {
            if ($this.val() !== '') {
                $parent.removeClass('js-unhighlight-label');
            }
        }
    });

    $($c.controls.ddl).change(function () {

        var $this = $(this),
            $parent = $this.parent();

        if ($this.val() == '') {
            $parent.addClass('js-hide-label');
        } else {
            $parent.removeClass('js-hide-label');
        }

    });

    $($c.controls.chkhotel).click(function () {
        if (!$bind.ShowHideBottomPanel()) {
            $($c.controls.chkhotel).prop('checked', true);
        }
    });

    $($c.controls.chkrestaurant).click(function (e) {
        if (!$bind.ShowHideBottomPanel()) {
            $($c.controls.chkrestaurant).prop('checked', true);
        }
    });

    $($c.controls.chklocation).click(function (e) {
        if (!$bind.ShowHideBottomPanel()) {
            $($c.controls.chklocation).prop('checked', true);
        }
    });

    //$($c.controls.aprofile).click(function (e) {

    //    $bind.StartLoader("U");
    //    $($c.controls.Usertitle).text($($c.controls.hdnEditProfile).val());
    //    $($c.controls.userbody).load($(this).data("url"));
    //    $($c.controls.usercontainer).modal("show");

    //});

    $($c.controls.achangepass).click(function (e) {

        $bind.StartLoader("U");
        $($c.controls.Usertitle).text($($c.controls.hdnChangePass).val());
        $($c.controls.userbody).load($(this).data("url"));
        $($c.controls.usercontainer).modal("show");

    });

    $bind.hotelJrange(true);

    $bind.restaurantJrange(true);

    $bind.locationJrange(true);

    //$($c.controls.drawmap).tabSlideOut({
    //    tabLocation: 'right',
    //    offsetReverse: true,
    //    clickScreenToClose: false,
    //    handleOffsetReverse: true,
    //    offset: '40px',
    //    onOpen: function () {
    //        $bind.showdiv();
    //        $v.variables.Isopen = 1;
    //    },
    //    onClose: function () {
    //        $bind.hidediv();
    //        $v.variables.Isopen = 0;
    //    }
    //});



    $($c.controls.slide).css('marginRight', '-475px');

    $($c.controls.slidebutton).on("click", function () {

        $($c.controls.slide).animate({ marginRight: "-475px" });
        var slide = "#slide" + $(this)[0].id;

        if ($(slide).css("marginRight") == "-475px") {
            $(slide).animate({ marginRight: "2px" });
            $($c.controls.slidebutton).hide();
            $("#" + $(this)[0].id).show();
        } else if ($(slide).css("marginRight") == "2px") {
            $(slide).animate({ marginRight: "-475px" });
            $($c.controls.slidebutton).show();
        }

    });

    $($c.controls.btntoggle).click(function () {

        $($c.controls.btntoggle).toggleClass("showbtntoggle");
        $($c.controls.leftfilter).toggleClass("showtoggledmenu");

        //$($c.controls.btntoggle).animate({ "left": "320px", "top": "5px" }, 500);
        //$($c.controls.leftfilter).animate({ "left": "5px", "top": "5px" }, 500);
    });

    $bind.PopOvero();

    $($c.controls.ddlcountry).on('change', function (e) {
        $($c.controls.txtcity).focus();
    });

    $($c.controls.backtotop).on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 92
        }, 700);
    });

    $('html').on('click', '[data-dismiss="popover"]', function (e) {
        $(this).closest(".popover").remove();
    });

    setTimeout(function () {

        //  $Map.autocompleteDestination();

        var user = JSON.parse(ajaxRequest.getCurrentUser());
        $($c.controls.spanuser).text(user.First_Name + ' ' + user.Last_Name);

        //Start User photo changes  15 april
        //if (window.localStorage.getItem(ajaxRequest.variables.CurrentUserPhoto)!="null" && window.localStorage.getItem(ajaxRequest.variables.CurrentUserPhoto).length > 0) {
        //    $($c.controls.imgCurrentUser).attr("src", 'data:image/png;base64,' + window.localStorage.getItem(ajaxRequest.variables.CurrentUserPhoto));
        //   // $($c.controls.imgCurrentUser).attr("alt", user.First_Name + ' ' + user.Last_Name);
        //}
        ajaxRequest.SetUserProfilePhotoIntoImage();
        $($c.controls.spanLAD).text(user.Last_AccessDate);
        if (user.IsManageUser) {
            $($c.controls.amanageuser).show();
            $($c.controls.divider1).show();
        }
        else {
            $($c.controls.amanageuser).hide();
            $($c.controls.divider1).hide();
        }
           
    }, 100);

    var header = $("#guide-template");

    $(window).scroll(function () {

        //var middlecontantLength = $("#middlecontent").html().length;

        //if (middlecontantLength > 10) {

        //    var scroll = $(window).scrollTop();
        //    if (scroll > 200 && parseInt(parseInt($(window).scrollTop().toFixed())) != parseInt($(document).height() - $(window).height())) {
        //        header.addClass("fixed");
        //        $('.wizard-inner').hide();
        //        $($c.controls.headersection).addClass("pt31 borderbottom");
        //        $(".nav-pills").addClass("bgtop");
        //    } else {

        //        if (parseInt(parseInt($(window).scrollTop().toFixed())) != parseInt($(document).height() - $(window).height())) {
        //            header.removeClass("fixed");
        //            $('.wizard-inner').show();
        //            $($c.controls.headersection).removeClass("pt31 borderbottom");
        //            $(".nav-pills").removeClass("bgtop");
        //        }

        //    }

        //}

        $bind.backToTop();

    });

});
