﻿using DCPPortalWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DCPPortalWeb.Controllers
{
    public class InfoController : BaseController
    {
        /// <summary>
        /// Get Hotel Info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetHotelInfo(int id)
        {
            ViewBag.id = id;
            return View();
        }

        /// <summary>
        /// Get Restaurant Info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetRestaurantInfo(int id)
        {
            ViewBag.id = id;
            return View();
        }

        /// <summary>
        /// Get Location Info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetLocationInfo(int id)
        {
            ViewBag.id = id;
            return View();
        }

        /// <summary>
        /// Change Password
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult changepassword()
        {
            return PartialView("_ChangePassword");
        }

        /// <summary>
        /// Edit Profile
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult editprofile()
        {
            return PartialView("_EditProfile");
        }

        [HttpGet]
        public ActionResult Profile()
        {
            return View();
        }

    }
}
