﻿using System;
using System.Collections.Generic;
using DCP.Models;
using DCP.Models.RFP;
namespace DCP.Repository.Interface
{
    public interface IRequestDetailRepository
    {
        #region RFP list
        ResponseRfpModel getRfpList(int page, int pageSize, string status, string lang,string term = "", int? userId=0);
        RequestForProposalModel getRFPDetail(int RfpId, int? userId = 0, string type = "", string lang = "");

        MainRFPMiceRequestWithProposalModel GetRFPMiceRequestWithProposal(int page, int pageSize, int? userId, string sortby, string search);
        #endregion
    }
}
