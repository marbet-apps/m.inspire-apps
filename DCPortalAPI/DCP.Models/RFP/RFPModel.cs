﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace DCP.Models.RFP
{
    public class RFPModel
    {
        [JsonProperty("request_id")]
        public int RequestId { get; set; }

        [JsonProperty("request_date")]
        public string Request_date { get; set; }
        [JsonProperty("event_name")]
        public string Event_name { get; set; }
        [JsonProperty("company")]
        public string Company { get; set; }

        [JsonProperty("request_duration")]
        public int? Days_running { get; set; }

        [JsonProperty("type_of_event")]
        public string Rfp_Type { get; set; }

        [JsonProperty("status")]
        public string Rfp_status { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonIgnore]
        [JsonProperty("mice_status_type")]
        public string Mice_status_type { get; set; }
    }

    
    public class RFPDetailModel : RFPModel
    {
        [JsonProperty("company_name")]
        public string Company_Name { get; set; }
        [JsonProperty("department_name")]
        public string Department_Name { get; set; }
        [JsonProperty("rfpmice_requests")]
        public List<RFPMiceRequestModel> RFPMiceRequests { get; set; }
    }
    public class ResponseRfpModel
    {
        [JsonProperty("totalcount")]
        public int TotalCount { get; set; }
        [JsonProperty("rfp")]
        public List<RFPModel> RFPModels { get; set; }
    }

    public class ResponseRfpDetail
    {

    }
}
