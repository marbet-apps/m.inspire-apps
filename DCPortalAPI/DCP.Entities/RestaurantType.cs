﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class RestaurantType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [ForeignKey("Restaurants")]
        public int dcp_restaurantinfo_id { get; set; }
        [Required]
        [ForeignKey("RestaurantTypes")]
        public int dcp_restauranttype_id { get; set; }

        public Restaurants Restaurants { get; set; }
        public List_RestaurantType RestaurantTypes { get; set; }
    }
}
