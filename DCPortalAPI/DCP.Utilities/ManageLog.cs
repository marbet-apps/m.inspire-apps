﻿using System;
using System.Runtime.CompilerServices;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace DCP.Utilities
{
    public class LogManager
    {
        public static log4net.ILog GetLogger([CallerFilePath]string filename = "")
        {
            return log4net.LogManager.GetLogger(filename);
        }
    }

    public class WriteLog
    {
        private static readonly log4net.ILog log = LogManager.GetLogger();

        public static void LogDeveloper(string message)
        {
            log.Debug(message);
        }

        public static void LogInformation(string message)
        {
            log.Info(message);
        }

        public static void LogWarning(string message)
        {
            log.Warn(message);

        }

        public static void LogException(string message, Exception ex)
        {
            log.Error(message, ex);
        }
    }
}
