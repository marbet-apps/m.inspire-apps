﻿using DCP.Business.Interface;
using DCP.Entities;
using DCP.Models;
using DCP.Repository.Interface;
using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.Net;

namespace DCP.Business
{
    public class Secure : IBSecure
    {
        private ISecureRepository _ISecureRepository;

        public Secure(ISecureRepository _ISecureRepository)
        {
            this._ISecureRepository = _ISecureRepository;
        }

        /// <summary>
        /// validate user's login credentials.
        /// </summary>
        public ApiResult CheckUserAuth(string username, string password, out bool flag)
        {
            flag = false;

            try
            {
                ResponseUserLoginModel UserModel = _ISecureRepository.CheckUserAuth(username, password);
               

                if (UserModel != null && string.IsNullOrEmpty(UserModel.Email) != true)
                {
                    if (UserModel.IsActive && !UserModel.IsDelete)
                    {
                        flag = true;

                        return ResultSet.SuccessResponse(ResponceCodes.Success.GetToken, UserModel, ManageEnum.status.success.ToString(), ResponceMessages.Success.GetToken);
                    }
                    else if (UserModel.IsDelete)
                    {
                        flag = false;
                        return ResultSet.SuccessResponse(ResponceCodes.Error.DeletedUser, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.DeletedUser);
                    }
                    else
                    {
                        return ResultSet.SuccessResponse(ResponceCodes.Error.InactiveUser, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.InactiveUser);
                    }
                }
                else
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.InvalidCredential, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.InvalidCredential);
                }

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }

        }

        /// <summary>
        /// Validate administrator login credentials.
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="password"></param>
        /// <param name="usertype"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public ApiResult CheckAdminUserAuth(string Email, string password, int usertype, out bool flag)
        {
            flag = false;

            try
            {
                ResponseUserLoginModel UserModel = _ISecureRepository.CheckAdminUserAuth(Email, password, usertype);


                if (UserModel != null)
                {
                    if (UserModel.IsActive)
                    {
                        flag = true;
                        return ResultSet.SuccessResponse(ResponceCodes.Success.GetToken, UserModel, ManageEnum.status.success.ToString(), ResponceMessages.Success.GetToken);
                    }
                    else
                    {
                        return ResultSet.SuccessResponse(ResponceCodes.Error.InactiveUser, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.InactiveUser);
                    }
                }
                else
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.InvalidCredential, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.InvalidCredential);
                }

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }

        }

        /// <summary>
        /// for creating/register user.
        /// </summary>
        /// <param name="userRegisterModel"></param>
        /// <returns></returns>
        public ApiResult RegisterUser(UserRegisterModel userRegisterModel, out bool flag)
        {
            flag = false;
            try
            {
               RegisterationModel registerationModel =  _ISecureRepository.RegisterUser(userRegisterModel);
                if(registerationModel != null && string.IsNullOrEmpty(registerationModel.Email) != true)
                {
                    flag = true;
                    return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, registerationModel, ManageEnum.status.success.ToString(), ResponceMessages.Success.UserRegistered);

                }
                return ResultSet.SuccessResponse(ResponceCodes.Error.AlreadyExists, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.AlreadyExists);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
        /// <summary>
        /// for generate password.
        /// </summary>
        /// <param name="userPasswordModel"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public ApiResult ForgotUserPassword(UserPasswordModel userPasswordModel, out bool flag, out string name, string token)
        {
            flag = false;
            name = "";
            try
            {
                UserPasswordModel userPassword  = _ISecureRepository.ForgotUserPassword(userPasswordModel,out name, token);
                if (userPassword != null && string.IsNullOrEmpty(userPassword.Email) != true)
                {
                    flag = true;
                    return ResultSet.SuccessResponse(ResponceCodes.Success.PasswordCreated, userPassword, ManageEnum.status.success.ToString(), ResponceMessages.Success.PasswordCreated);

                }
                return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.UserNotFound);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
        /// <summary>
        /// Retrive all user.
        /// </summary>
        /// <param name="activestatus"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public ApiResult GetAllUser(string activestatus, string email, int page = 1, int pageSize = 50)
        {
            try
            {
                ResponseUserModel registerationModel = _ISecureRepository.getAllUser(activestatus,email,page:page,pageSize:pageSize);
                if (registerationModel != null && registerationModel.totalRecord > 0)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, registerationModel, ManageEnum.status.success.ToString(), ResponceMessages.Success.RetrieveAllUser);
                }
                return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, registerationModel, ManageEnum.status.fail.ToString(), ResponceMessages.Error.UsersNotFound);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Retrive user detail bassed on the userid.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public ApiResult GetUserDetail(int userID, string lang)
        {
            try
            {
                ResponseUserModel registerationModel = _ISecureRepository.getAllUser(UserID:userID, lang:lang);
                if (registerationModel != null && registerationModel.totalRecord > 0)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, registerationModel, ManageEnum.status.success.ToString(), ResponceMessages.Success.RetrieveUser);
                }
                return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.UsersNotFound);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// for remove user
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public ApiResult RemoveUser(int userID)
        {
            try
            {
                UserModel userModel = _ISecureRepository.removeUser(userID:userID);
                if (userModel != null && userModel.First_Name != null)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Success.Deleted, userModel, ManageEnum.status.success.ToString(), ResponceMessages.Success.DeleteUser);
                }
                return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.UsersNotFound);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
        /// <summary>
        /// for change password.
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public ApiResult ChangePassword(ChangePasswordModel userModel)
        {
            try
            {
                int Response = _ISecureRepository.ChangePassword(userModel: userModel);
                if(Response == 1)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Success.ChangePassword, userModel, ManageEnum.status.success.ToString(), ResponceMessages.Success.ChangePassword);
                }
                else if(Response == 2)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.PasswordNotMatch, userModel, ManageEnum.status.error.ToString(), ResponceMessages.Error.PasswordNotMatch);
                }
                else
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.UsersNotFound);
                }
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
        /// <summary>
        /// for updating the user.
        /// </summary>
        /// <param name="updateUserModel"></param>
        /// <returns>returns the usermodel</returns>
        public ApiResult UpdateUser(int userID, int loggedInUserID, UpdateUserModel updateUserModel)
        {
            try
            {
                ResponseModel Response = _ISecureRepository.UpdateUser(userID,loggedInUserID,updateUserModel);
                if (Response.isSuccess)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Success.Updated, Response, ManageEnum.status.success.ToString(), ResponceMessages.Success.UpdateUser);
                }
                else
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, Response, ManageEnum.status.fail.ToString(), ResponceMessages.Error.UsersNotFound);
                }
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult UpdateUseProfile(UpdateProfileModel model)
        {
            try
            {
                UpdateProfileResponseModel Response = _ISecureRepository.UpdateUserProfile(model);
                if (Response.IsSuccess)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Success.UpdateProfile, Response, ManageEnum.status.success.ToString(), ResponceMessages.Success.UpdateProfile);
                }
                else
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, Response, ManageEnum.status.fail.ToString(), ResponceMessages.Error.UsersNotFound);
                }
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult UploadImage(byte[] bytes, int userId)
        {
            try
            {
                bool result = _ISecureRepository.UploadImage(bytes, userId);
                if (result)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Success.UpdateProfile, "", ManageEnum.status.success.ToString(), ResponceMessages.Success.UpdateProfile);
                }
                else
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.UsersNotFound);
                }
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult DeleteImage(int userId)
        {
            try
            {
                bool result = _ISecureRepository.DeleteImage(userId);
                if (result)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Success.DeleteProfilePhoto, "", ManageEnum.status.success.ToString(), ResponceMessages.Success.DeleteProfilePhoto);
                }
                else
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.Deleted, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.Deleted);
                }
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult ResetPassword(ResetPasswordModel model)
        {
            try
            {
               

                bool RequestSendForActivateUser = false;
                if (!string.IsNullOrEmpty(model.RequestSendForActivateUser))
                {
                    string forActivateUser = Crypto.DecryptString(model.RequestSendForActivateUser);
                    if (!string.IsNullOrEmpty(forActivateUser))
                    {
                        Boolean.TryParse(forActivateUser, out RequestSendForActivateUser);
                    }
                }

                string idValue = Crypto.Decrypt(model.IdValue);
                int id = 0;
                int.TryParse(idValue, out id);

                model.Id = id;
                UserModel Response = _ISecureRepository.ResetPassword(model, RequestSendForActivateUser);
                if (Response !=null)
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Success.ResetPassword, Response, ManageEnum.status.success.ToString(), ResponceMessages.Success.ResetPassword);
                }               
                else
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.UsersNotFound);
                }
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult ValidateUser(ResetPasswordModel model)
        {
            try
            {
                string idValue = Crypto.Decrypt(model.IdValue);
                if(!string.IsNullOrEmpty(idValue))
                {
                    model.Id = Convert.ToInt32(idValue);

                    bool RequestSendForActivateUser = false;
                    if (!string.IsNullOrEmpty(model.RequestSendForActivateUser))
                    {
                        string forActivateUser = Crypto.DecryptString(model.RequestSendForActivateUser);
                        if (!string.IsNullOrEmpty(forActivateUser))
                        {
                            Boolean.TryParse(forActivateUser, out RequestSendForActivateUser);
                        }
                    }

                    UserModel Response = _ISecureRepository.ValidateUser(model, RequestSendForActivateUser);
                    if (Response !=null)
                    {
                        if (string.IsNullOrEmpty(Response.Token) && Convert.ToString(Response.Token)!=model.RequestedDate)
                        {
                            return ResultSet.SuccessResponse(ResponceCodes.Error.RequestExpire, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.RequestExpire);
                        }
                        else if (!string.IsNullOrEmpty(Response.Token) && Convert.ToString(Response.Token) != model.RequestedDate)
                        {
                            return ResultSet.SuccessResponse(ResponceCodes.Error.InvalidUrl, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.InvalidUrl);
                        }

                        //string requestDate = Crypto.DecryptString(model.RequestedDate);
                        //if (!string.IsNullOrEmpty(requestDate))
                        //{


                        //    DateTime date = new DateTime();
                        //    DateTime.TryParseExact(requestDate, "ddMMyyyyHHmm", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out date);

                        //    TimeSpan time = DateTime.Now.Subtract(date);

                        //    if (time.TotalHours > 24)
                        //    {
                        //        return ResultSet.SuccessResponse(ResponceCodes.Error.RequestExpire, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.RequestExpire);
                        //    }
                        //}

                        return ResultSet.SuccessResponse(ResponceCodes.Success.UserExist, Response, ManageEnum.status.success.ToString(), ResponceMessages.Success.UserExist);
                    }
                    else
                    {
                        return ResultSet.SuccessResponse(ResponceCodes.Error.UserNotFound, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.UsersNotFound);
                    }

                }
                else
                {
                    return ResultSet.SuccessResponse(ResponceCodes.Error.InvalidUrl, "", ManageEnum.status.fail.ToString(), ResponceMessages.Error.InvalidUrl);
                }
                
               
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

    }
}
