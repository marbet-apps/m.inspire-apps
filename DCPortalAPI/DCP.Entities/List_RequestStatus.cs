﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DCP.Entities
{
    public class List_RequestStatus
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(50), Column(TypeName = "nvarchar")]
        public string request_status { get; set; }

        [Required, MaxLength(50), Column(TypeName = "nvarchar")]
        public string request_status_de { get; set; }

        [Required, MaxLength(50), Column(TypeName = "varchar")]
        public string mice_status_type { get; set; }
    }
}
