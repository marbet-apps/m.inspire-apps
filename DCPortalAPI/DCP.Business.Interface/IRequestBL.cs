﻿using DCP.Entities;
using DCP.Mice.Models;
using DCP.Models.RFP;

namespace DCP.Business.Interface
{
    public interface IRequestBL
    {
        #region Cart

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cartModel"></param>
        /// <returns></returns>
        ApiResult MarkResourceForTheRequest(RequestCartModel cartModel);

        ///INSPIRE-102
        ///RFPId wise delete record from multiple RFP of user
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="resourceId"></param>
        /// <param name="resourceType"></param>
        /// <returns></returns>
        ApiResult RemoveResourceFromTheSelection(int userId, int resourceId, string resourceType, int? rfpId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="resourceId"></param>
        /// <param name="RType"></param>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        ApiResult AddResourceToRFP(int userId, string[] resourceIds, string RType, int rfpId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ApiResult GetRequestCart(int userId);

        ///INSPIRE-102
        ///Store RFP and return request proposal modal for multiple RFP save purpose
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ApiResult MakeRequestForProposal(RequestForProposalModel rfpModel);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ApiResult GetRequestCartCount(int userId);

        #endregion Cart

        #region Lookup Methods

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        ApiResult GetConferenceFoods(string lang);

        #endregion Lookup Methods

        #region RFP Request Items

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpMiceRequestItemId"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        ApiResult GetRFPRequestItems(int rfpMiceRequestItemId, string lang);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestItemModel"></param>
        /// <returns></returns>
        ApiResult AddEditRFPRequestItem(RequestItemModel requestItemModel);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpMiceRequestItemId"></param>
        /// <param name="requestItemObjectId"></param>
        /// <returns></returns>
        ApiResult DeleteRFPRequestItem(int rfpMiceRequestItemId);

        #endregion RFP Request Items

        #region RFP

        ApiResult GetRequestForProposal(int userId, string langCode);

        ApiResult SubmitRFP(RequestForProposalModel rfpModel, bool isSubmit, int userId);

        ApiResult GetRequestForProposalById(int id, string langCode);

        #endregion RFP

        ApiResult BookedOrder(OrderBookingModel bookingModel,int userId);
        ApiResult AbortOrder(OrderAbortModel bookingModel);

        ///INSPIRE-102
        ///User wise save multiple RFP into table so rfpid wise get resource detail 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="langCode"></param>
        /// <param name="sortBy"></param>
        /// <param name="type"></param>
        /// <param name="requestItemType"></param>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        ApiResult GetResourceItemDetail(int userId, string langCode, string sortBy, string type, string requestItemType, int rfpId);

    }
}
