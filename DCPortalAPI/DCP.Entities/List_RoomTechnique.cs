﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class List_RoomTechnique
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string room_technique_name { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string room_technique_name_de { get; set; }

        [Required, MaxLength(100), Column(TypeName = "varchar")]
        public string mice_room_technique { get; set; }

        public int? sequence { get; set; }
    }
}
