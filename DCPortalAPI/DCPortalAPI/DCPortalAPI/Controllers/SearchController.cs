﻿using DCP.Business.Interface;
using DCP.Entities;
using DCP.Models;
using DCP.Utilities;
using DCPortalAPI.Filter;
using DCPortalAPI.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace DCPortalAPI.Controllers
{
    [RoutePrefix("api/Search")]
    [ValidateModel]
    [Authorize]
    [ExceptionHandler]
    public class SearchController : ApiController
    {
        private IBSearch _IBSearch;
        private IBLookUp _IBLookUp;

        public SearchController(IBSearch IBSearch, IBLookUp IBLookUp)
        {
            this._IBSearch = IBSearch;
            this._IBLookUp = IBLookUp;
        }

        /// <summary>
        /// Get All Resources
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetResourcesList")]
        public IHttpActionResult GetResourcesList(FilterModel SearchModel)
        {
            WriteLog.LogInformation(LogMessages.RetrieveResourcesStart);

            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);

                if (SearchModel != null && !SearchModel.isFromHistory)
                {
                    var jsSerializer = new JavaScriptSerializer();
                    //Save search history
                    _IBSearch.SaveSearchHistory(jsSerializer.Serialize(SearchModel), userId);
                }
                //var Response = _IBSearch.GetResourcesList(SearchModel,CommonMethod.GetHeaders(this.Request.Headers, "Lang"));
                var Response = _IBSearch.GetResourcesList(SearchModel,CommonMethod.GetHeaders(this.Request.Headers, "Lang"), userId);

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RetrieveResourcesEnd);

            }
        }

        /// <summary>
        /// get Selected lookup list
        /// </summary>
        /// <returns></returns>
        [Route("GetSelectedLookUpList")]
        public IHttpActionResult GetSelectedLookUpList(string url, string ids)
        {
            var Response = new DCP.Entities.ApiResult();
            string[] idArray;
            if(ids != "" && ids.Split(',').Length > 0)
            {
                idArray = ids.Split(',');
            } else
            {
                return Ok(Response);
            }
            
            var client = GetHttpClient();
            var appendUrl = url + "?page=1&type=0&pageSize=50";
            client.BaseAddress = new Uri(Settings.Constants.APIBaseURL+ "api/");
            var requestResponse = client.GetAsync(appendUrl).Result;
            if (requestResponse.IsSuccessStatusCode)
            {
                var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                
                Response = JsonConvert.DeserializeObject<ApiResult>(jsonResponse.Result);

                var selectedResult = new List<dynamic>();
                if (Response != null && Response.Data != null)
                {
                    var resultData = JsonConvert.DeserializeObject<TemplateLookUp>(Response.Data.ToString());
                    foreach(var item in resultData.lookup)
                    {
                        if (idArray.Contains(item.Id.ToString()))
                        {
                            selectedResult.Add(new { id= item.Id, text = item.Name });
                        }
                    }
                }
                Response.Data = selectedResult;
            }
            return Ok(Response);
        }
        private ApiResult GetSelectedLookupListWithText(ApiResult result)
        {
            if (result != null && result.Data != null)
            {
                var lang = CommonMethod.GetHeaders(this.Request.Headers, "Lang");
                var filterModel = JsonConvert.DeserializeObject<FilterModel>(result.Data.ToString());
                if(filterModel.HFilterModel.typeids != "")
                {
                    var list = _IBLookUp.GetHotelTypeList(lang, "", 1, 50, 0);
                    filterModel.HFilterModel.typeids = getFormattedLookup(filterModel.HFilterModel.typeids, list);
                }
                if (filterModel.RFilterModel.kids != "")
                {
                    var list = _IBLookUp.GetKitchenList(lang, "", 1, 50, 0);
                    filterModel.RFilterModel.kids = getFormattedLookup(filterModel.RFilterModel.kids, list);
                }
                if (filterModel.RFilterModel.typeids != "")
                {
                    var list = _IBLookUp.GetRestaurantTypeList(lang, "", 1, 50, 0);
                    filterModel.RFilterModel.typeids = getFormattedLookup(filterModel.RFilterModel.typeids, list);
                }
                if (filterModel.RFilterModel.suitableids != "")
                {
                    var list = _IBLookUp.GetSuitableForList(lang, "", 1, 50, 1);
                    filterModel.RFilterModel.suitableids = getFormattedLookup(filterModel.RFilterModel.suitableids, list);
                }

                if (filterModel.LFilterModel.typeids != "")
                {
                    var list = _IBLookUp.GetLocationTypeList(lang, "", 1, 50, 0);
                    filterModel.LFilterModel.typeids = getFormattedLookup(filterModel.LFilterModel.typeids, list);
                }
                if (filterModel.LFilterModel.suitableids != "")
                {
                    var list = _IBLookUp.GetSuitableForList(lang, "", 1, 50, 2);
                    filterModel.LFilterModel.suitableids = getFormattedLookup(filterModel.LFilterModel.suitableids, list);
                }

                if (filterModel.country != "")
                {
                    var list = _IBLookUp.GetCountryList(lang, "", 1, 50, 0);
                    filterModel.country = getFormattedLookup(filterModel.country, list);
                }
                if (filterModel.distance != null)
                {
                    var list = _IBLookUp.GetRadiusList(lang, "", 1, 50, 0);
                    filterModel.selectedDistanceList = getFormattedDistance(filterModel.distance, list);
                }
                if (filterModel.HFilterModel.hotelchains != null)
                {
                    var list = _IBLookUp.GetChainList("", 1, 50, 0);
                    filterModel.HFilterModel.hotelchains = getFormattedLookupForHotelChain(filterModel.HFilterModel.hotelchains, list);
                }

                result.Data = filterModel;
            }
            return result;
        }
        private string getFormattedLookupForHotelChain(string ids, ApiResult resultData)
        {
            var selectedResult = new List<SelectedLookupString>();
            if (ids != "" && ids.Split(',').Length > 0)
            {
                string[] idArray = ids.Split(',');
                if (resultData != null && resultData.Data != null)
                {
                    var data = (TemplateLookUpString)resultData.Data;
                    foreach (var item in data.lookup)
                    {
                        if (idArray.Contains(item.Id.ToString()))
                        {
                            selectedResult.Add(new SelectedLookupString() { id = item.Id, text = item.Name });
                        }
                    }
                }
            }
            return JsonConvert.SerializeObject(selectedResult);
        }
        private string getFormattedLookup(string ids, ApiResult resultData)
        {
            var selectedResult = new List<SelectedLookup>();
            if (ids != "" && ids.Split(',').Length > 0)
            {
                string[] idArray = ids.Split(',');
                if (resultData != null && resultData.Data != null)
                {
                    var data = (TemplateLookUp)resultData.Data;
                    foreach (var item in data.lookup)
                    {
                        if (idArray.Contains(item.Id.ToString()))
                        {
                            selectedResult.Add(new SelectedLookup() { id = item.Id, text = item.Name });
                        }
                    }
                }
            }
            return JsonConvert.SerializeObject(selectedResult);
        }
        private string getFormattedDistance(double? id, ApiResult resultData)
        {
            var selectedResult = new List<SelectedLookup>();
            if (id != null && id.HasValue)
            {
                if (resultData != null && resultData.Data != null)
                {
                    var data = (TemplateLookUp)resultData.Data;
                    foreach (var item in data.lookup)
                    {
                        if (id.Value.ToString() == item.Id.ToString())
                        {
                            selectedResult.Add(new SelectedLookup() { id = item.Id, text = item.Name });
                        }
                    }
                }
            }
            return JsonConvert.SerializeObject(selectedResult);
        }
        private HttpClient GetHttpClient()
        {
            var auth = CommonMethod.GetHeaders(this.Request.Headers, "Authorization");
            var lang = CommonMethod.GetHeaders(this.Request.Headers, "Lang");
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer",
                       auth.Split(' ')[1]);
            client.DefaultRequestHeaders.Add("Lang", lang);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        /// <summary>
        /// Get All Resources by history
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSearchCriteriaFromHistory")]
        public IHttpActionResult GetSearchCriteriaFromHistory()
        {
            WriteLog.LogInformation(LogMessages.RetrieveResourcesFromHistoryStart);

            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IBSearch.GetSearchCriteria(userId);

                if(Response != null && Response.Data != null)
                {
                    Response = GetSelectedLookupListWithText(Response);
                }

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RetrieveResourcesFromHistoryEnd);

            }
        }

        ///// <summary>
        ///// Get Hotel By Id
        ///// </summary>
        ///// <param name="Id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[Route("GetHotelById")]
        //public IHttpActionResult GetHotelById(int Id)
        //{
        //    WriteLog.LogInformation(LogMessages.RetrieveHotelStart);

        //    try
        //    {
        //        var Response = _IBSearch.GetHotelById(Id, CommonMethod.GetHeaders(this.Request.Headers, "Lang"));

        //        return Ok(Response);
        //    }
        //    finally
        //    {
        //        WriteLog.LogInformation(LogMessages.RetrieveHotelEnd);

        //    }
        //}

        ///// <summary>
        ///// Get Restaurant By Id
        ///// </summary>
        ///// <param name="Id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[Route("GetRestaurantById")]
        //public IHttpActionResult GetRestaurantById(int Id)
        //{
        //    WriteLog.LogInformation(LogMessages.RetrieveRestaurantStart);

        //    try
        //    {
        //        var Response = _IBSearch.GetRestaurantById(Id, CommonMethod.GetHeaders(this.Request.Headers, "Lang"));
        //        return Ok(Response);
        //    }
        //    finally
        //    {
        //        WriteLog.LogInformation(LogMessages.RetrieveRestaurantEnd);

        //    }
        //}

        ///// <summary>
        ///// Get Location By Id
        ///// </summary>
        ///// <param name="Id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[Route("GetLocationById")]
        //public IHttpActionResult GetLocationById(int Id)
        //{
        //    WriteLog.LogInformation(LogMessages.RetrieveLocationStart);

        //    try
        //    {
        //        var Response = _IBSearch.GetLocationById(Id, CommonMethod.GetHeaders(this.Request.Headers, "Lang"));
        //        return Ok(Response);
        //    }
        //    finally
        //    {
        //        WriteLog.LogInformation(LogMessages.RetrieveLocationEnd);
        //    }
        //}

        /// <summary>
        /// Get Image List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetImageData")]
        public IHttpActionResult GetImageData(int id, string type)
        {
            WriteLog.LogInformation(LogMessages.ImageRetrieveStarted);

            try
            {
                var Response = _IBSearch.GetImageData(id, type);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.ImageRetrieveEnded);
            }
        }

        /// <summary>
        /// Get Near By Restaurant List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetNearByRestaurantList")]
        public IHttpActionResult GetNearByRestaurantList(string lat, string lng, int? cid, string type, int distance)
        {
            WriteLog.LogInformation(LogMessages.GetNearByRestaurantListStarted);

            try
            {
                var Response = _IBSearch.GetNearByRestaurantList(lat, lng, cid, type, distance);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetNearByRestaurantListEnded);
            }
        }

        /// <summary>
        /// Get Near By Hotel List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetNearByHotelList")]
        public IHttpActionResult GetNearByHotelList(string lat, string lng, int? cid, string type, int distance)
        {
            WriteLog.LogInformation(LogMessages.GetNearByHotelListStarted);

            try
            {
                var Response = _IBSearch.GetNearByHotelList(lat, lng, cid, type, distance);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetNearByHotelListEnded);
            }
        }

        /// <summary>
        /// Get Near By Location List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetNearByLocationList")]
        public IHttpActionResult GetNearByLocationList(string lat, string lng, int? cid, string type, int distance)
        {
            WriteLog.LogInformation(LogMessages.GetNearByLocationListStarted);

            try
            {
                var Response = _IBSearch.GetNearByLocationList(lat, lng, cid, type, distance);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetNearByLocationListEnded);
            }
        }

        /// <summary>
        /// Get Location By Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetLocationInfo")]
        public IHttpActionResult GetLocationInfo(int Id)
        {
            WriteLog.LogInformation(LogMessages.GetLocationInfoStarted);

            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IBSearch.GetLocationInfo(Id, CommonMethod.GetHeaders(this.Request.Headers, "Lang"), userId);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetLocationInfoEnded);
            }
        }

        /// <summary>
        /// Get Restaurant By Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRestaurantInfo")]
        public IHttpActionResult GetRestaurantInfo(int Id)
        {
            WriteLog.LogInformation(LogMessages.GetRestaurantInfoStarted);

            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IBSearch.GetRestaurantInfo(Id, CommonMethod.GetHeaders(this.Request.Headers, "Lang"), userId);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRestaurantInfoEnded);
            }
        }


        /// <summary>
        /// Get Hotel By Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetHotelInfo")]
        public IHttpActionResult GetHotelInfo(int Id)
        {
            WriteLog.LogInformation(LogMessages.GetHotelInfoStarted);

            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var Response = _IBSearch.GetHotelInfo(Id, CommonMethod.GetHeaders(this.Request.Headers, "Lang"), userId);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetHoteInfoEnded);
            }
        }

        /// <summary>
        /// Get RFP Resources List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRFPResourceList")]
        public IHttpActionResult GetRFPResourceList(string Rids, string type)
        {
            WriteLog.LogInformation(LogMessages.RetrieveRFPResourceStart);

            try
            {

                var Response = _IBSearch.GetRFPResourceList(Rids, type, CommonMethod.GetHeaders(this.Request.Headers, "Lang"));

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RetrieveRFPResourceEnd);

            }
        }

        /// <summary>
        /// Get RFP Resources List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCartList")]
        public IHttpActionResult GetCartList()
        {
            WriteLog.LogInformation(LogMessages.RetrieveCARTResourceStart);

            try
            {
                var Response = _IBSearch.GetCartList(Convert.ToInt32(CommonMethod.GetHeaders(this.Request.Headers, "UserId")), CommonMethod.GetHeaders(this.Request.Headers, "Lang"));

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RetrieveCARTResourceEnd);

            }
        }

        /// <summary>
        /// Get RFP Resources Count
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCartCount")]
        public IHttpActionResult GetCartCount()
        {
            WriteLog.LogInformation(LogMessages.RetrieveCARTResourceStart);

            try
            {
                var Response = _IBSearch.GetCartCount(Convert.ToInt32(CommonMethod.GetHeaders(this.Request.Headers, "UserId")), CommonMethod.GetHeaders(this.Request.Headers, "Lang"));

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RetrieveCARTResourceEnd);

            }
        }

    }
}