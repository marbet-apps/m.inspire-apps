﻿namespace DCP.Utilities
{
    public static class ResponceCodes
    {
        /// <summary>
        /// Responce Success Codes
        /// </summary>
        public static class Success
        {
            public const string Saved = "701";
            public const string Deleted = "702";
            public const string Updated = "703";
            public const string GetList = "704";
            public const string GetToken = "705";
            public const string UserRegistered = "706";
            public const string PasswordCreated = "707";
            public const string RetrieveAllUser = "708";
            public const string RetrieveUser = "709";
            public const string DeleteUser = "710";
            public const string GetResource = "711";
            public const string ChangePassword = "712";
            public const string UpdateUser = "713";
            public const string NotAllowNewUser = "714";
            public const string AllowNewUser = "715";
            public const string SendMailFail = "716";
            public const string FogotPasswordMailFail = "717";
            public const string UpdateProfile = "718";
            public const string MigrationSuccessed = "720";
            public const string DeleteProfilePhoto = "721";
            public const string UserExist = "722";
            public const string ResetPassword = "723";
        }

        /// <summary>
        /// Responce Error Codes
        /// </summary>
        public static class Error
        {
            public const string InvalidCredential = "601";
            public const string Saved = "602";
            public const string Deleted = "603";
            public const string Updated = "604";
            public const string MissingToken = "605";
            public const string ModelValidateFail = "606";
            public const string MissingParameter = "607";
            public const string InactiveUser = "608";
            public const string UserNotFound = "609";
            public const string PasswordNotMatch = "610";
            public const string DeletedUser = "611";
            public const string AlreadyExists = "612";
            public const string InvalidData = "613";
            public const string InvalidUrl = "614";
            public const string RequestExpire = "615";
        }

        /// <summary>
        /// Responce Common Codes
        /// </summary>
        public static class Common
        {
            public const string UnAuthorized = "401";
            public const string Forbidden = "403";
            public const string MissingArgument = "410";
            public const string FaultError = "411";
        }

        public static class MiceError
        {
            public const string OrderFailed = "801";
            public const string AbortFailed = "802";
        }
    }

    public static class ResponceMessages
    {

        /// <summary>
        /// Responce Success Messages
        /// </summary>
        public static class Success
        {
            public const string Saved = "Data Saved successfully!";
            public const string Deleted = "Data Deleted successfully!";
            public const string Updated = "Data Updated successfully!";
            public const string Retrieved = "Data Retrieved successfully!";
            public const string GetToken = "Token Retrieved successfully!";
            public const string UserRegistered = "User Registered successfully!";
            public const string PasswordCreated = "Please check your inbox, we have sent you reset password url.";
            public const string RetrieveAllUser = "All user retrieved successfully!";
            public const string RetrieveUser = "User detail retieved successfully!";
            public const string DeleteUser = "User deleted successfully!";

            public const string ChangePassword = "Password changed successfully!";
            public const string UpdateUser = "User updated successfully!";
            public const string NotAllowNewUser = "You can not add new user!";
            public const string AllowNewUser = "You can add new user!";
            public const string SendMailFail = "User Registered successfully, But send mail failed. Please contact to cystem administrator";
            public const string FogotPasswordMailFail = "Fogot Password mail failed. Please contact to cystem administrator";
            public const string UpdateProfile = "Your profile has been updated successfully.";
            public const string MigrationSuccess = "Data Migration done successfully.";
            public const string DeleteProfilePhoto = "Profile photo deleted successfully!";
            public const string ResetPassword = "Your password has been changed successfully!";
            public const string UserExist = "User profile has been verified successfully.";
        }

        /// <summary>
        /// Responce Error Messages
        /// </summary>
        public static class Error
        {
            public const string InvalidCredential = "Invalid credential!";
            public const string Saved = "Unable to save data!";
            public const string Deleted = "Unable to delete data!";
            public const string Updated = "Unable to update data!";
            public const string UnAuthorized = "Un-authorized access!";
            public const string UnAuthorizedToken = "Invalid token!";
            public const string InternalServerToken = "Invalid token format!";
            public const string MissingToken = "Token is missing !";
            public const string InsufficientParam = "Insufficient parameters!";
            public const string InactiveUser = "In-active User!";
            public const string UserNotFound = "Requested User/Email is not found!";
            public const string UsersNotFound = "User not found";
            public const string PasswordNotMatch = "Password not matched!";
            public const string DeletedUser = "Requested User has been deleted!";
            public const string AlreadyExists = "Email-Id already exists!";
            public const string InvalidUrl = "Invalid Url";
            public const string RequestExpire = "Password has already reset for current request";
        }

        /// <summary>
        /// Responce Common Messages
        /// </summary>
        public static class Common
        {
            public const string UnAuthorized = "Un-authorized access!";
            public const string Forbidden = "Forbidden!";
            public const string MissingArgument = "Argument is missing!";
            public const string FaultError = "Fault Error!";
            public const string InternalServerError = "Internal server error!";
        }
        public static class MiceError
        {
            public const string OrderFailed = "Error,Not able to place order.";
            public const string AbortFailed = "Error,Not able to abort.";

        }
    }

    public static class LogMessages
    {
        public const string LoginStarted = "Login Started!";
        public const string LoginEnded = "Login Ended!";

        public const string RegisterStarted = "Register Started";
        public const string RegisterEnded = "Register Ended";

        public const string GeneratePasswordStarted = "Generate password started";
        public const string GeneratePasswordEnded = "Generate password ended";

        public const string RetriveAllUserStarted = "Retrieve all user started";
        public const string RetriveAllUserEnded = "Retrieve all user ended";

        public const string RetriveUserDetail = "Retrieve user detail started!";
        public const string RetriveUserDetailEnded = "Retrieve user detail ended";

        public const string DeleteUser = "User delete started";
        public const string DeleteUserEnded = "User delete Ended";

        public const string CountryListStarted = "Country List Started!";
        public const string CountryListEnded = "Country List Ended!";

        public const string RadiusListStarted = "Radius List Started!";
        public const string RadiusListEnded = "Radius List Ended!";

        public const string HotelChainListStarted = "Hotel Chain List Started!";
        public const string HotelChainListEnded = "Hotel Chain List Ended!";

        public const string RetrieveHotelStart = "Retrieve Hotel Started!";
        public const string RetrieveHotelEnd = "Retrieve Hotel Ended!";

        public const string RetrieveRestaurantStart = "Retrieve Restaurant Started!";
        public const string RetrieveRestaurantEnd = "Retrieve Restaurant Ended!";

        public const string RetrieveLocationStart = "Retrieve Location Started!";
        public const string RetrieveLocationEnd = "Retrieve Location Ended!";

        public const string RetrieveResourcesStart = "Retrieve Resources Started!";
        public const string RetrieveResourcesEnd = "Retrieve Resources Ended!";

        public const string ChangePasswordStarted = "Change password started";
        public const string ChangePasswordEnd = "Change password ended";
        public const string HotelFilterStart = "Hotel Filter Started!";
        public const string HotelFilterEnd = "Hotel Filter Ended!";

        public const string UpdateUserStarted = "Update user started";
        public const string UpdateUserEnd = "Update user ended";

        public const string RestaurantFilterStart = "Restaurant Filter Started!";
        public const string RestaurantFilterEnd = "Restaurant Filter Ended!";

        public const string LocationFilterStart = "Location Filter Started!";
        public const string LocationFilterEnd = "Location Filter Ended!";

        public const string HotelTypeListStarted = "Hotel Type List Started!";
        public const string HotelTypeListEnded = "Hotel Type List Ended!";

        public const string DefaultHotelTypeListStarted = "Default Hotel Type List Started!";
        public const string DefaultHotelTypeListEnded = "Default Hotel Type List Ended!";

        public const string RestaurantTypeListStarted = "Restaurant Type List Started!";
        public const string RestaurantTypeListEnded = "Restaurant Type List Ended!";

        public const string LocationTypeListStarted = "Location Type List Started!";
        public const string LocationTypeListEnded = "Location Type List Ended!";

        public const string KitchenListStarted = "Kitchen List Started!";
        public const string KitchenListEnded = "Kitchen List Ended!";

        public const string SuitableForListStarted = "Suitable For List Started!";
        public const string SuitableForListEnded = "Suitable For List Ended!";

        public const string SubscriptionTypeStarted = "Subscription Type List Started!";
        public const string SubscriptionTypeEnded = "Subscription Type List Ended!";

        public const string ImageRetrieveStarted = "Image retrieve started!";
        public const string ImageRetrieveEnded = "Image retrieve Ended!";

        public const string RFPListStarted = "RFP List Started.";

        public const string MarkResourceForTheRequestStarted = "MarkResourceForTheRequest Started.";
        public const string MarkResourceForTheRequestEnded = "MarkResourceForTheRequest Ended.";

        public const string RemoveResourceFromTheSelectionStarted = "RemoveResourceFromTheSelection Started.";
        public const string RemoveResourceFromTheSelectionEnded = "RemoveResourceFromTheSelection Ended.";

        public const string GetRequestCartStarted = "GetRequestCart Started.";
        public const string GetRequestCartEnded = "GetRequestCart Ended.";

        public const string MakeRequestForProposalStarted = "MakeRequestForProposal Started.";
        public const string MakeRequestForProposalEnded = "MakeRequestForProposal Ended.";

        public const string GetRequestTypesStarted = "GetRequestTypes Started.";
        public const string GetRequestTypesEnded = "GetRequestTypes Ended.";

        public const string GetRequestItemTypesStarted = "GetRequestItemTypes Started.";
        public const string GetRequestItemTypesEnded = "GetRequestItemTypes Ended.";

        public const string GetRoomTypesStarted = "GetRoomTypes Started.";
        public const string GetRoomTypesEnded = "GetRoomTypes Ended.";

        public const string GetRoomTechniquesStarted = "GetRoomTechniques Started.";
        public const string GetRoomTechniquesEnded = "GetRoomTechniques Ended.";

        public const string GetSeatingTypesStarted = "GetSeatingTypes Started.";
        public const string GetSeatingTypesEnded = "GetSeatingTypes Ended.";

        public const string GetConferenceFoodsStarted = "GetConferenceFoods Started.";
        public const string GetConferenceFoodsEnded = "GetConferenceFoods Ended.";

        public const string GetRFPRequestItemsStarted = "GetRFPRequestItems Started.";
        public const string GetRFPRequestItemsEnded = "GetRFPRequestItems Ended.";

        public const string AddEditRFPRequestItemStarted = "AddEditRFPRequestItem Started.";
        public const string AddEditRFPRequestItemEnded = "AddEditRFPRequestItem Ended.";

        public const string DeleteRFPRequestItemStarted = "DeleteRFPRequestItem Started.";
        public const string DeleteRFPRequestItemEnded = "DeleteRFPRequestItem Ended.";

        public const string RFPListEnded = "RFP List Ended!";

        public const string GetRequestStatusListStarted = "Request Status List Started";
        public const string GetRequestStatusListEnded = "Request Status List Ended";

        public const string GetNearByRestaurantListStarted = "Near By Restaurant List Started";
        public const string GetNearByRestaurantListEnded = "Near By Restaurant List Ended";

        public const string GetNearByHotelListStarted = "Near By Hotel List Started";
        public const string GetNearByHotelListEnded = "Near By Hotel List Ended";

        public const string GetNearByLocationListStarted = "Near By Location List Started";
        public const string GetNearByLocationListEnded = "Near By Location List Ended";

        public const string GetLocationInfoStarted = "Location Info Started";
        public const string GetLocationInfoEnded = "Location Info Ended";

        public const string GetRestaurantInfoStarted = "Restaurant Info Started";
        public const string GetRestaurantInfoEnded = "Restaurant Info Ended";

        public const string GetHotelInfoStarted = "Hotel Info Started";
        public const string GetHoteInfoEnded = "Hotel Info Ended";

        public const string RFPDetailStarted = "RFP Detail Started.";
        public const string RFPDetailEnded = "RFP Detail Ended.";

        public const string GetRequestForProposalStarted = "GetRequestForProposal Started.";
        public const string GetRequestForProposalEnded = "GetRequestForProposal Ended.";

        public const string RetrieveRFPResourceStart = "Retrieve RFP Resources Started!";
        public const string RetrieveRFPResourceEnd = "Retrieve RFP Resources Ended!";

        public const string GetRequestCartCountStarted = "GetRequestCartCount Started.";
        public const string GetRequestCartCountEnded = "GetRequestCartCount Ended.";

        public const string SubmitRFPStarted = "SubmitRFP Started.";
        public const string SubmitRFPEnded = "SubmitRFP Ended.";

        public const string RetrieveCARTResourceStart = "Retrieve CART Resources Started!";
        public const string RetrieveCARTResourceEnd = "Retrieve CART Resources Ended!";

        public const string GetDaysRunningStarted = "GetDaysRunning Started.";
        public const string GetDaysRunningEnded = "GetDaysRunning Ended.";

        public const string GetTimeIntervalsStarted = "GetTimeIntervals Started.";
        public const string GetTimeIntervalsEnded = "GetTimeIntervals Ended.";

        public const string GetRFPMiceRequestWithProposalStarted = "GetRFPMiceRequestWithProposal Started!";
        public const string GetRFPMiceRequestWithProposalEnded = "GetRFPMiceRequestWithProposal Ended!";

        public const string GetCityListStarted = "GetCityList started!";
        public const string GetCityListEnded = "GetCityList Ended!";

        public const string GetResourceItemDetailStarted = "GetResourceItemDetail Started";
        public const string GetResourceItemDetailEnded = "GetResourceItemDetail Ended";

        public const string UpdateUserProfileStarted = "User Profile Edit Started";
        public const string UpdateUserProfileEnded = "User Profile Edit Ended";


        public const string GetAbortReasonStarted = "Get Abort Reason Started.";
        public const string GetAbortReasonEnded = "Get Abort Reason Ended.";

        public const string RegisterNewUserStarted = "Register New User Started";
        public const string RegisterNewUserEnded = "Register New User Ended";

        public const string CheckAllowNewUserStarted = "Check Allow New User Started";
        public const string CheckAllowNewUserEnded = "Check Allow New User Ended";

        public const string SendMailStarted = "Send Mail Started";
        public const string SendMailEnded = "Send Mail Ended";

        public const string SendRegistrationMailStarted = "Send Registration Mail Started";
        public const string SendRegistrationMailEnded = "Send Registration Mail Ended";

        public const string SyncHotelStarted = "Sync Hotel Started";
        public const string SyncHotelEnded = "Sync Hotel Ended";

        public const string MigrateAirportStarted = "Migrate Airport Started";
        public const string MigrateAirportEnded = "Migrate Airport Ended";

        public const string ErrorInAddDCPMiceRequest = "Error in add DCPMiceRequest entry";

        public const string MigrateCountryStarted = "Migrate Country Started";
        public const string MigrateCountryEnded = "Migrate Country Ended";

        public const string MigrateHotelTypeStarted = "Migrate Hotel Type Started";
        public const string MigrateHotelTypeEnded = "Migrate Hotel Type Ended";

        public const string MigrateRestaurantTypeStarted = "Migrate Restaurant Type Started";
        public const string MigrateRestaurantTypeEnded = "Migrate Restaurant Type Ended";

        public const string MigrateLocationTypeStarted = "Migrate Location Type Started";
        public const string MigrateLocationTypeEnded = "Migrate Location Type Ended";

        public const string MigrateKitchenStarted = "Migrate Kitchen Started";
        public const string MigrateKitchenEnded = "Migrate Kitchen Ended";

        public const string MigrateSuitableforStarted = "Migrate Suitable for Started";
        public const string MigrateSuitableforEnded = "Migrate Suitable for Ended";

        public const string DeleteResourceStarted = "DeleteResourceStatred";
        public const string DeleteResourceEnded = "DeleteResourceEnded";

        public const string DeleteImageStarted = "DeleteImageStarted";
        public const string DeleteImageEnded = "DeleteImageEnded";

        public const string ResetPasswordStarted = "ResetPasswordStarted";
        public const string ResetPasswordEnded = "ResetPasswordEnded";

        public const string AddResourceToRFPStarted = "AddResourceToRFPStarted Started.";
        public const string AddResourceToRFPEnded = "AddResourceToRFPEnded Ended.";

        public const string RetrieveResourcesFromHistoryStart = "Retrieve Resources From History Started!";
        public const string RetrieveResourcesFromHistoryEnd = "Retrieve Resources From History Ended!";
    }

    public static class ValidationMessages
    {
        public const string UserNameRequired = "UserName is required! ";
        public const string PasswordRequired = "Password is required! ";
        public const string UserTypeRequired = "UserType is required! ";
        public const string UserTypeInvalid = "UserType is Invalid! ";

        public const string UserFirstNameRequired = "FirstName is required! ";
        public const string UserLastNameRequired = "LastName is required! ";
        public const string CompanyNameRequired = "The company name is required! ";
        public const string DepartmentNameRequired = "Department name is required! ";
        public const string ActiveStatusRequired = "Active status is required! ";
        public const string SelfCreatedStatusRequired = "Self-created status is required! ";
        public const string SalutaionRequired = "Salutation is required!";
        public const string SubscriptionTypeRequired = "Subscription type is required! ";
        public const string ApprovedStatusRequired = "Approved status is required! ";
        public const string DeleteStatusRequired = "Delete status is required! ";
        public const string ContryCodeRequired = "The country id is required! ";

        public const string EmailRequired = "Email is required! ";
        public const string EmailInvalid = "Invalid Email Address! ";
        public const string ActiveStatusInvalid = "Invalid active status value, it should be either true/false! ";
        public const string SelfCreatedStatusInvalid = "Invalid self-created status value, it should be either true/false! ";
        public const string ApproveStatusInvalid = "Invalid approve status value, it should be either true/false! ";
        public const string DeleteStatusInvalid = "Invalid delete status value, it should be either true/false! ";

        public const string OldPasswordRequired = "Old Password is required! ";
        public const string NewPasswordRequired = "New Password is required! ";

        public const string LoggedInUserid = "Logged in UserId is required! ";

    }

    public class EmailTemplate
    {
        public const string NewUserRegistration_En = "Email\\UserRegistration-EN.html";
        public const string NewUserRegistration_DE = "Email\\UserRegistration-DE.html";
        public const string ForgotPassword_En = "Email\\ForgotPassword-EN.html";
        public const string ForgotPassword_DE = "Email\\ForgotPassword-DE.html";
        public const string PasswordGenerated_En = "Email\\PasswordGenerated-EN.html";
        public const string PasswordGenerated_DE = "Email\\PasswordGenerated-DE.html";
    }

    public class EmailSubject
    {
        public const string NewUserRegistrationSubject_EN = "Registration Successfully, Account Activate Url";
        public const string NewUserRegistrationSubject_DE = "Registration Successfully, Account Activate Url";
        public const string ForgotPasswordSubject_En = "Password Reset Url";
        public const string ForgotPasswordSubject_DE = "Password Reset Url";
        public const string PasswordGeneratedSubject_En = "Password Changed Successfully";
        public const string PasswordGeneratedSubject_DE = "Das Passwort wurde erfolgreich geändert";
    }
}
