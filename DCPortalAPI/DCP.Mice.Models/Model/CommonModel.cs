﻿using Newtonsoft.Json;

namespace DCP.Mice.Models
{
    public class Country
    {
        [JsonProperty("names")]
        public MultilingualCountryName Names { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("location")]
        public GeoLocation Location { get; set; }
    }

    public class MultilingualCountryName
    {
        [JsonProperty("de_DE")]
        public string GermanName { get; set; }

        [JsonProperty("en_US")]
        public string EnglishName { get; set; }
    }

    public class GeoLocation
    {
        [JsonProperty("lat")]
        public double Latitude { get; set; }

        [JsonProperty("lon")]
        public double Longtitude { get; set; }
    }
}
