﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class Hotels
    {
        [Key]
        public int dcp_hotelinfo_id { get; set; }
        [Required, Column(TypeName = "varchar")]
        public string mice_id { get; set; }
        public string hotel_name { get; set; }
        public string hotel_chain { get; set; }
        [ForeignKey("Country")]
        public int country_id { get; set; }
        [Required]
        public int kb_id { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }
        
        public int? star { get; set; }
        [ForeignKey("Airport")]
        public int? airport_id { get; set; }
        [Column(TypeName = "varchar")]
        public string airport_transfer_time { get; set; }
        public double? airport_distance { get; set; }
        public int? total_rooms { get; set; }

        public int? total_conference { get; set; }
        public double? size_of_largest_conference { get; set; }
        public int? parking_spaces { get; set; }
        public string miscellaneous_info { get; set; }

        public bool? PP { get; set; }
        public bool? green { get; set; }
        public bool? reopening { get; set; }
        public bool? blacked { get; set; }
        public bool? closed { get; set; }

        public double? hotel_lan { get; set; }
        public double? hotel_lat { get; set; }
        public bool? is360degreeview { get; set; }
        public int created_by { get; set; }
        public DateTime created_date { get; set; }
        public int modify_by { get; set; }
        public DateTime modify_date { get; set; }

        public List_Country Country { get; set; }
        public List_Airport Airport { get; set; }

        public ICollection<HotelType> HotelType { get; set; }
    }
}
