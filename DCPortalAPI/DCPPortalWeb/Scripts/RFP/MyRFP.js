﻿var $MRFP = {

    GetRFPListData: function (flag) {

        $($c.controls.mloader).show();

        var jasonData = { page: $v.variables.i, pageSize: 50, sortby: ($($c.controls.sortby + ".bluefont").attr("data-sort") == undefined ? "CreatedDateDesc" : $($c.controls.sortby + ".bluefont").attr("data-sort")), search: $($c.controls.txtsearchevent).val() };

        $.ajax({
            url: $bind.URL($u.path.GetMyRFPList),
            type: "GET",
            data: jasonData,
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (response) {

                if (response != undefined && response != null && response.code == "704") {

                    $v.variables.i = $v.variables.i + 1;

                    if (flag) {
                        $($c.controls.myrfpdiv).html("");
                        $($c.controls.myrfpdraftdiv).html("");
                    }

                    response = response.data;

                    var rcount = 0;
                    var dcount = 0;

                    $.each(response.rfpmicerequestwithproposalmodel, function (i) {

                        var data = response.rfpmicerequestwithproposalmodel[i];

                        if (data.issubmitted == false) {

                            var htmlDcontents = $MRFP.MyRFPList(response.rfpmicerequestwithproposalmodel[i]);

                            $($c.controls.myrfpdraftdiv).append(htmlDcontents);

                            dcount++;

                        } else {

                            var htmlcontents = $MRFP.MyRFPList(response.rfpmicerequestwithproposalmodel[i]);

                            $($c.controls.myrfpdiv).append(htmlcontents);

                            rcount++;
                        }

                    });

                    $v.variables.recordsDisplayed = $v.variables.recordsDisplayed + response.rfpmicerequestwithproposalmodel.length;
                    $v.variables.totalRecords = response.totalrecords;

                    if ($v.variables.recordsDisplayed < $v.variables.totalRecords && $v.variables.totalRecords != 0 && $v.variables.recordsDisplayed != 0) {
                        $($c.controls.drawbutton).show();
                    } else {
                        $($c.controls.drawbutton).hide();
                    }

                    if (rcount == 0) {
                        $($c.controls.myrfpdiv).html("<div class='col-lg-offset-2 col-sm-offset-2 col-xs-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8  ac mt20p'><img src='\\Images\\img_nodata.png'/>&nbsp;<br/><span class='f20'> <b>" + $($c.controls.sNoRecords).val() + "</b> </span></div>");
                    }

                    if (dcount == 0) {
                        $($c.controls.myrfpdraftdiv).html("<div class='col-lg-offset-2 col-sm-offset-2 col-xs-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8  ac mt20p'><img src='\\Images\\img_nodata.png'/>&nbsp;<br/><span class='f20'> <b>" + $($c.controls.sNoRecords).val() + "</b> </span></div>");
                    }

                    if ($v.variables.totalRecords == 0) {
                        $($c.controls.myrfpdiv).html("<div class='col-lg-offset-2 col-sm-offset-2 col-xs-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8  ac mt20p'><img src='\\Images\\img_nodata.png'/>&nbsp;<br/><span class='f20'> <b>" + $($c.controls.sNoRecords).val() + "</b> </span></div>");
                        $($c.controls.myrfpdraftdiv).html("<div class='col-lg-offset-2 col-xs-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8  ac mt20p'><img src='\\Images\\img_nodata.png'/>&nbsp;<br/><span class='f20'> <b>" + $($c.controls.sNoRecords).val() + "</b> </span></div>");
                    } else {
                        $($c.controls.lblnorecords).remove();
                    }

                } else {
                    $managecode.Decision(false, "", response.code, response.message, "MyRFPList", false);
                }

                $($c.controls.mloader).hide();

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
                $bind.EFilterLoader();
                $($c.controls.mloader).hide();
            }
        });

    },
    variable: {
        isUpdate: false,
        ddlAbortReason: '#ddlAbortReason'
    },
    localization: {
        hdnAbortReason: "#hdnAbortReason",
        hdnAbort: "#hdnAbort",
        hdnBook: "#hdnBook",
        hdnAbortedDate: "#hdnAbortedDate",
        hdnSelectReason:"#hdnSelectReason"
    },
    OpenRFPAck: function (e) {
       
        //if ($(e).attr("data-status") == "new") {
        //    $managecode.Decision(false, "", "", $($c.controls.hdnNoUpdate).val(), "NoUpdate", true);
        //    return;
        //}
        //var isUpdate = false;
        //common.displayLoader();
        $($c.controls.infotitle).text(" " + $(e).attr("data-event") + " : " + $(e).attr("data-name") + "");
        // $($c.controls.infomodal).modal('toggle');
        $($c.controls.infoplace).html('');
        $($c.controls.infoplace).show();
        //Start rfp detail page change on 17 april 
        // $MRFP.GetMyRFPResourceList(e);
        $MRFP.GetRfPDetail(e);
        
        //if ($MRFP.variable.isUpdate == true) {

        //}
        //else {
        //    $managecode.Decision(false, "", "", $($c.controls.hdnNoUpdate).val(), "NoUpdate", true);
        //}
    },
    MyRFPList: function (data) {
        //Start rfp detail page change on 17 april 
        ///INSPIRE-102
        ///If Rfp is not submitted, Rfp is added edit icon after event name of RFP.
        var templates = "<div class='col-md-12 col-lg-12 col-sm-12 col-xs-12' data-rfpid=" + data.rfp_id + ">";
        templates += "<div class='panel rfpbox'>";
        templates += "<div class='panel-heading panelgrey'>";
        templates += "<h3 class='panel-title'>";
        templates += "<span class='glyphicon glyphicon-bookmark'></span>&nbsp;<a class='labelheader' href='/rfp/startrfp/" + data.rfpidvalue + "' target='_blank'> " + data.event_name + "</a>";
        templates += "</h3>";
        templates += "<div class='pull-right'>";
        templates += "<span class='vat'>";
        templates += "" + $($c.controls.hdnrunningdays).val() + " : <span class='labelbox'>" + data.days_running + "</span>";
        templates += "</span> <a class='labelheader' href='/rfp/startrfp/" + data.rfpidvalue + "' target='_blank'>" + (data.issubmitted == false ? "<span class='glyphicon glyphicon-edit redirectlink'></span>" : "<span class='glyphicon glyphicon-check redirectlink'></span>") + "</a>";
        templates += "</div>";
        templates += "</div>";
        templates += "<div class='panel-body'>";
        templates += "<div class='row'>";
        templates += "<div class='col-xs-12 col-md-12'>";
        templates += "<div class='col-xs-12 mt10 col-md-4 col-sm-4 ac " + ((data.hotel_rfp_misc_id == null || data.hotel_rfp_misc_id == 0 || data.issubmitted == false || data.hotel_Status == 'fail') ? 'pointernone' : '') + "'>";
        templates += "<a href='javascript:void(0);' class='btn Rounded-Rectangle notification' data-rfpid=" + data.rfpidvalue + " data-type='H' data-id='" + data.hotel_rfp_misc_id + "' data-status=" + data.hotel_Status + " data-event='" + data.event_name + "' data-name='" + $($c.controls.hdnhotel).val() + "' onclick='$MRFP.OpenRFPAck(this);'>";

        if ((data.hotel_rfp_misc_id == null || data.hotel_rfp_misc_id == 0 || data.issubmitted == false || data.hotel_Status == 'fail')) {
            templates += "<img src='/Images/Hotel_disable.png' />&nbsp;&nbsp;<span class='labelbox'>" + $($c.controls.hdnhotel).val() + "</span>";
        } else {
            templates += "<span class='badge'>" + data.hotelstatuscount + "</span>";
            templates += "<img src='/Images/hotel_request.png' />&nbsp;&nbsp;<span class='labelbox'>" + $($c.controls.hdnhotel).val() + "</span>";
        }

        templates += "<hr />";

        templates += "<span class='badgeN'>" + data.totalhotelresource + "</span>";
        templates += "&nbsp;&nbsp;<span class='ac labelbox " + (data.hotel_Status == 'fail' ? 'failStatusColor' : '') + "'>";
        templates += "<i class='glyphicon glyphicon-stats'></i>&nbsp;" + ((data.hotel_rfp_misc_id == null || data.hotel_rfp_misc_id == 0) ? "N/A" : $MRFP.ToPascalCase(data.hotel_Status)) + "";
        templates += "</span>";
        templates += "</a>";
        templates += "</div>";
        templates += "<div class='col-xs-12 mt10 col-md-4 col-sm-4 ac " + ((data.restaurant_rfp_misc_id == null || data.restaurant_rfp_misc_id == 0 || data.issubmitted == false || data.restaurant_Status=='fail') ? 'pointernone' : '') + "'>";
        templates += "<a href='javascript:void(0);' class='btn Rounded-Rectangle notification' data-rfpid=" + data.rfpidvalue + " data-type='R' data-id='" + data.restaurant_rfp_misc_id + "' data-status=" + data.hotel_Status + " data-event='" + data.event_name + "' data-name='" + $($c.controls.hdnrestaurant).val() + "' onclick='$MRFP.OpenRFPAck(this);'>";

        if ((data.restaurant_rfp_misc_id == null || data.restaurant_rfp_misc_id == 0 || data.issubmitted == false || data.restaurant_Status == 'fail')) {

            templates += "<img src='/Images/Restaurant_disable.png' />&nbsp;&nbsp;<span class='labelbox'>" + $($c.controls.hdnrestaurant).val() + "</span>";
        } else {
            templates += "<span class='badge'>" + data.restaurantstatuscount + "</span>";
            templates += "<img src='/Images/restaurant_request.png' />&nbsp;&nbsp;<span class='labelbox'>" + $($c.controls.hdnrestaurant).val() + "</span>";
        }

        templates += "<hr />";
        templates += "<span class='badgeN'>" + data.totalrestaurantresource + "</span>";
        templates += "&nbsp;&nbsp;<span class='ac labelbox " + (data.restaurant_Status == 'fail' ? 'failStatusColor':'') +"'>";
        templates += "<i class='glyphicon glyphicon-stats'></i>&nbsp;" + ((data.restaurant_rfp_misc_id == null || data.restaurant_rfp_misc_id == 0) ? "N/A" : $MRFP.ToPascalCase(data.restaurant_Status)) + "";
        templates += "</span>";
        templates += "</a>";
        templates += "</div>";
        templates += "<div class='col-xs-12 mt10 col-md-4 col-sm-4 ac " + ((data.location_rfp_misc_id == null || data.location_rfp_misc_id == 0 || data.issubmitted == false || data.location_Status == 'fail') ? 'pointernone' : '') + "'>";
        templates += "<a href='javascript:void(0);' class='btn Rounded-Rectangle notification' data-rfpid=" + data.rfpidvalue + " data-type='L' data-id='" + data.location_rfp_misc_id + "' data-status=" + data.hotel_Status + " data-event='" + data.event_name + "' data-name='" + $($c.controls.hdnlocation).val() + "' onclick='$MRFP.OpenRFPAck(this);'>";

        if ((data.location_rfp_misc_id == null || data.location_rfp_misc_id == 0 || data.issubmitted == false || data.location_Status == 'fail')) {
            templates += "<img src='/Images/Location_disable.png' />&nbsp;&nbsp;<span class='labelbox'>" + $($c.controls.hdnlocation).val() + "</span>";
        } else {
            templates += "<span class='badge'> " + data.locationstatuscount + "</span>";
            templates += "<img src='/Images/location_request.png' /> &nbsp;&nbsp;<span class='labelbox'>" + $($c.controls.hdnlocation).val() + "</span>";
        }

        templates += "<hr />";
        templates += "<span class='badgeN'>" + data.totallocationresource + "</span>";
        templates += "&nbsp;&nbsp;<span class='ac labelbox " + (data.location_Status == 'fail' ? 'failStatusColor' : '') + "'>";
        templates += "<i class='glyphicon glyphicon-stats'></i>&nbsp;" + ((data.location_rfp_misc_id == null || data.location_rfp_misc_id == 0) ? "N/A" : $MRFP.ToPascalCase(data.location_Status)) + "";
        templates += "</span>";
        templates += "</a>";
        templates += "</div>";
        templates += "</div>";
        templates += "</div>";
        templates += "</div>";
        templates += "<div class='panel-footer'>";
        templates += "<div class='row'>";
        templates += "<div class='fl col-lg-6 col-md-6 col-sm-6'>";
        templates += "" + $($c.controls.hdncreatedon).val() + " : <span class='labelbox'>" + data.created_date + "</span>";
        templates += "</div>";
        templates += "<div class='fr col-lg-6 col-md-6 col-sm-6 ar'>";
        templates += "" + $($c.controls.hdncreatedBy).val() + " : <span class='labelbox'>" + data.username + "</span>";
        templates += "</div>";
        templates += "</div>";
        templates += "</div>";
        templates += "</div>";
        templates += "</div>";


        return templates;
    },
    MyRFPResourceList: function (data) {

        templates = "";

        // data = data.micerequests;

        templates += "<div class='row col-lg-12 col-md-12 col-sm-12 col-xs-12 ac paneldiv removablediv prplmrml0 zoomclass1 Cards padl0'>";
        templates += "<div class='panel-body oborder card card-5 box padl0 hf205 box arrow-left'>";
        templates += "    <div class='row'>";
        templates += "        <div class='col-xs-12 col-md-12'>";

        if (data.status == "expired") {

            templates += "<div class='col-sm-12'>";
            templates += "        <span class='fontbold'>" + $($c.controls.hdnstatus).val() + "</span>";
            templates += "        <hr />";
            templates += "        <span>" + data.status + "</span>";
            templates += "</div>";

        }
        else if (data.status == "offered" || data.status == "booked") {

            var offers = data.roomPrices;
            var k = 0;

            $.each(offers, function (j) {

                k = j + 1;

                if (k % 2 != 0) {
                    templates += "<div class='col-sm-6 borderright'>";
                } else {
                    templates += "<div class='col-sm-6'>";
                }

                templates += "    <div class='col-sm-2 col-md-2 p0'>";
                templates += "        <span class='fontbold'>" + offers[j].price + "$</span>";
                templates += "        <hr />";
                templates += "        <span>" + offers[j].quantity + " Qty</span>";
                templates += "    </div>";
                templates += "    <div class='col-sm-10 col-md-10'>";
                templates += "        <span class='fontbold'>" + offers[j].type + "</span>";
                templates += "        <hr />";
                templates += "        <span class='block-ellipsis'>" + (offers[j].comments == null ? '' : offers[j].comments) + "</span >";
                templates += "    </div>";
                templates += "</div>";

                if (k % 2 == 0) {
                    templates += "<div class='clear10'></div>";
                    templates += "<hr />";
                }
            });

        }

        templates += "        </div>";
        templates += "    </div>";
        templates += "</div>";

        templates += "    </div>";


        return templates;
    },
    GetMyRFPResourceList: function (rfpId, type) {
        //Start rfp detail page change on 17 april 
        //$bind.ClearPopupwindow();
        $bind.StartLoader("F");

        //var htmlcontents = $MRFP.MyRFPResourceList("");
        //$($c.controls.map).html(htmlcontents);

        var jasonData = { rfpId: rfpId, type: type };
        //var jasonData = { rfpId: $(e).attr("data-rfpid"), type: 'H' }; // $(e).attr("data-type")


        $.ajax({
            url: $bind.URL($u.path.GetMyRFPResourceList),
            data: jasonData,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (response) {


                var htmlcontents = "";

                if (response != undefined && response != null && response.code == "704" && response.data.micerequests != null) {
                    var typeName = '';
                    if (type == "H") {
                        typeName = $($c.controls.hdnhotel).val();
                    }
                    else if (type == "R") {
                        typeName = $($c.controls.hdnrestaurant).val();
                        $($c.controls.Accotab).hide();
                        $($c.controls.Accotab_tab).hide();
                        $($c.controls.Accotab_tab).removeClass("active");

                        $($c.controls.Conftab_tab).click();

                    }
                    else if (type == "L") {
                        typeName = $($c.controls.hdnlocation).val();
                        $($c.controls.Accotab).hide();
                        $($c.controls.Accotab_tab).hide();
                        $($c.controls.Accotab_tab).removeClass("active");

                        $($c.controls.Conftab_tab).click();
                    }

                    
                    $($c.controls.rfpinfotitle).html(response.data.event_name + " : " + "<span class='StatusLabel'>" + $MRFP.ToPascalCase(response.data.micerequests[0].status) + "</span>");
                    
                    var requestStatus = '';
                    if (response.data.micerequests[0].request_detail != null && response.data.micerequests[0].request_detail.request_offer_details != null) {
                        requestStatus = response.data.micerequests[0].request_detail.request_offer_details.status;

                    }
                    
                    var isBooked = false;
                    if (response.data.micerequests.length > 0 && response.data.micerequests[0].request_detail != null && response.data.micerequests[0].request_detail != undefined) {
                        var offerRecord = response.data.micerequests[0].request_detail.request_offer_details.offers;

                        $.each(offerRecord, function (i) {
                            var da = offerRecord[i];
                            if (da.status == "booked") {
                                isBooked = true;
                            }
                        });
                    }
                   
                    //29-04-2020 - Change due to set wrong status, so abort portion is not showing on screen.
                    if (requestStatus == "running" && isBooked == false) {

                        htmlcontents += "   <div class='row prl10'>";
                        htmlcontents += "       <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                        htmlcontents += "           <div class='panel panel-default'>";
                        htmlcontents += "               <div class='panel-heading panelgrey'>";
                        htmlcontents += "                   <h3 class='panel-title'>";
                        htmlcontents += "                       <span class='glyphicon glyphicon-floppy-remove'></span> &nbsp;" + $($MRFP.localization.hdnAbort).val() + '&nbsp;';
                        htmlcontents += "                   </h3>";
                        htmlcontents += "               </div>";
                        htmlcontents += "               <div class='panel-body'>";
                        // htmlcontents += "               <div class='pull-right col-md-4 mt2'><div class='row'>";
                        htmlcontents += "                   <div class='row'>";
                        htmlcontents += "                       <div class='col-lg-4 col-md-4 col-sm-6 col-xs-6'>";
                        htmlcontents += "                           <div id='ddlAbortReason' class='add-form-elements' ></div> &nbsp;&nbsp; ";
                        htmlcontents += "                       </div>";
                        htmlcontents += "                       <div class='col-lg-8 col-md-8 col-sm-6 col-xs-6 text-left'>";
                        htmlcontents += "                           <button type='button' onClick='$MRFP.AbortOrder(" + response.data.micerequests[0].request_detail.request_offer_details.id + ");' class='btn draw-borderB show-tooltip' id='btnabort' ><span class='glyphicon glyphicon-remove'> </span>&nbsp;&nbsp;" + $($MRFP.localization.hdnAbort).val() + "</button>";
                        htmlcontents += "                       </div>";
                        htmlcontents += "                   </div>";
                        htmlcontents += "                </div>";
                        htmlcontents += "           </div>";
                        htmlcontents += "       </div>";
                        htmlcontents += "   </div>";

                    }
                    else if (requestStatus == "aborted") {

                        htmlcontents += "   <div class='row prl10'>";
                        htmlcontents += "       <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>";
                        htmlcontents += "           <div class='panel panel-default'>";
                        htmlcontents += "               <div class='panel-heading panelgrey'>";
                        htmlcontents += "                   <h3 class='panel-title'>";
                        htmlcontents += "                       <span class='glyphicon glyphicon-floppy-remove'></span> &nbsp;" + $($MRFP.localization.hdnAbort).val() + '&nbsp;';
                        htmlcontents += "                   </h3>";
                        htmlcontents += "               </div>";
                        htmlcontents += "               <div class='panel-body'>";
                        // htmlcontents += "               <div class='pull-right col-md-4 mt2'><div class='row'>";
                        htmlcontents += "                   <div class='row mt20'>";
                        htmlcontents += "                       <div class='form-group col-lg-4 col-md-4 col-sm-6 col-xs-6'>";
                        htmlcontents += "                          <label class='labelheader'>" + $($MRFP.localization.hdnAbortedDate).val() + "</label>";
                        htmlcontents += "                            <span class='labelbox'>" + response.data.micerequests[0].request_detail.request_offer_details.DisplayAbortedTime + "</span>";
                        htmlcontents += "                       </div>";
                        htmlcontents += "                       <div class='col-lg-8 col-md-8 col-sm-6 col-xs-6 text-left'>";
                        htmlcontents += "                          <label class='labelheader'>" + $($MRFP.localization.hdnAbortReason).val() + "</label>";
                        htmlcontents += "                            <span class='labelbox'>" + response.data.micerequests[0].request_detail.request_offer_details.AbortReasonValue + "</span>";
                        htmlcontents += "                       </div>";
                        htmlcontents += "                   </div>";                       
 
                        htmlcontents += "                </div>";
                        htmlcontents += "           </div>";
                        htmlcontents += "       </div>";
                        htmlcontents += "   </div>";
                    }

                    $MRFP.ManageContingents(response);

                    if (response.data.micerequests.length > 0 && response.data.micerequests[0].request_detail != null && response.data.micerequests[0].request_detail != undefined) {

                        console.log(response.data);
                        
                        var offers = response.data.micerequests[0].request_detail.request_offer_details.offers;
                        var micerequestid = response.data.micerequests[0].request_detail.request_offer_details.id;
                        //var requestId = response.data.micerequests[0].request_detail.micerequestid;

                        //var requestStatus = response.data.micerequests[0].request_detail.request_offer_details.status;
                        //var count = 0;
                        
                       
                        
                        htmlcontents += "<div class='row prl10'>";

                        $.each(offers, function (i) {

                            var da = offers[i];

                            htmlcontents += "<div class='col-xl-4 col-md-4 col-sm-6 col-xs-12'>";

                            htmlcontents += "<div class='panel panel-default'>";

                            htmlcontents += $MRFP.ManageHeaders(da, micerequestid, isBooked, requestStatus);

                            // htmlcontents += "   <div class='clear10'></div>";

                            htmlcontents += "   <div class='panel-body'>";
                            htmlcontents += $search.drawpanel(da.resource[0], da.resource[0].type, $v.variables.i, "X", false, true, false);
                            htmlcontents += "<input type='hidden' class='resourceStatus' id='hdnResourceStatus_" + da.id +" value='" + da.status + "' />";
                            htmlcontents += "   </div>";

                            htmlcontents += $MRFP.ManageFooters(da);

                            htmlcontents += "</div>";

                            htmlcontents += "</div>";

                            setTimeout(function () { $search.loadimages(da.resource[0].id, da.resource[0].type); }, 100);

                        });

                        htmlcontents += "</div>";

                        $($c.controls.infoplace).html(htmlcontents);

                    }
                    else {
                        $($c.controls.infoplace).html(htmlcontents);
                    }
                    $MRFP.variable.isUpdate = true;

                } else {
                    $MRFP.variable.isUpdate = false;
                }

                $LookUp.BindDropdown($MRFP.variable.ddlAbortReason, $($MRFP.localization.hdnSelectReason).val(), $u.path.FillAbortReason, 0, 50, false, true);

                //var resourceStatus = $(".resourceStatus");

                //$.each(resourceStatus, function (obj, i) {


                //    if ($(this).val().trim().toLowerCase() == "Booked") {
                //        $(".bookButton").hide();
                //    }
                //});

            },
            error: function (data, status, error) {
                //common.hideLoader();
                $managecode.Decision(true, data, "", "", "error", false);
                $MRFP.variable.isUpdate = false;
            }
        });

    },
    BookOrder: function (micerequestid, offerId, comment) {

        $($c.controls.mloader).show();

        var BookOrderReq = {
            requestId: micerequestid,
            micerequestid: micerequestid,
            offerId: offerId,
            comment: comment,
            userId: JSON.parse(localStorage.CurrentUser).LoggedInUserID
        };
        $.ajax({
            url: $bind.URL($u.path.BookOrder),
            data: JSON.stringify(BookOrderReq),
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (response) {

                if (response != null && response != undefined && response.code == "701") {
                    $managecode.Decision(false, "", response.code, response.message, "", true);
                    setTimeout(function () { location.reload(); }, 2000);
                } else {
                    $managecode.Decision(false, "", response.code, response.message, "", false);
                }

                $($c.controls.mloader).hide();
            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
                $($c.controls.mloader).hide();

            }
        });
    },
    AbortOrder: function (requestId) {

        if ($($MRFP.variable.ddlAbortReason).val() == "") {
            $("#s2id_ddlAbortReason a").css('cssText', 'border-color:red !important');
          
            return;
        }


        $($c.controls.mloader).show();

        var BookOrderReq = {
            requestId: requestId,
            abortReason: $($MRFP.variable.ddlAbortReason).val(),
            userId: JSON.parse(localStorage.CurrentUser).LoggedInUserID
        }
        $.ajax({
            url: $bind.URL($u.path.AbortOrder),
            data: JSON.stringify(BookOrderReq),
            type: "POST",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (response) {
                if (response != null && response != undefined && response.code == "701") {
                    $managecode.Decision(false, "", response.code, response.message, "", false);
                    location.reload();
                }
                else {
                    $managecode.Decision(true, response, response.code, response.message, "error", false);
                }

                $($c.controls.mloader).hide();
            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
                $($c.controls.mloader).hide();
            }
        });
    },
    closemodel: function (modelId) {
        $(modelId).modal("hide");
        $($MRFP.variable.ddlAbortReason).select2("val", "");
    },
    OpenModel: function (modelId) {
        $('#ddlAbortReason').modal("show");
        $('#ddlAbortReason').appendTo("body");
    },
    DrawAccomodation: function (R) {
        
        var htmlview = '';

        htmlview += '<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12 mt10">';
        htmlview += '<div class="panel panel-default" >';

        htmlview += '        <div class="panel-heading">';
        htmlview += '           <h3 class="panel-title">';
        htmlview += '               <span class="badge panelgrey fr">' + R.quantity + '</span>&nbsp<span class="glyphicon glyphicon-bookmark"></span>&nbsp;<span class="labelheader">' + R.display_roomtype + "</span>";
        htmlview += '           </h3 >';
        htmlview += '        </div>';

        htmlview += '<div class="panel-body mt10">';
        htmlview += '       <div class="clear10"></div>';
        htmlview += '       <div class="row">';
        htmlview += '           <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">';
        htmlview += '               <label class="labelheader">';
        htmlview += '                   ' + $($c.controls.hdnCheckInDate).val();
        htmlview += '               </label>';
        htmlview += '               <span class="glyphicon glyphicon-calendar yellow"></span> &nbsp;<span class="labelbox">' + R.display_reserve_fromdate + "</span>";
        htmlview += '           </div>';
        htmlview += '           <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">';
        htmlview += '               <label class="labelheader">';
        htmlview += '                   ' + $($c.controls.hdnCheckOutDate).val();
        htmlview += '               </label>';
        htmlview += '               <span class="glyphicon glyphicon-calendar yellow"></span> &nbsp;<span class="labelbox">' + R.display_reserve_todate + "</span>";
        htmlview += '           </div>';
        htmlview += '       </div>';
        htmlview += '</div>';

        htmlview += '</div >';
        htmlview += '</div >';

        return htmlview;
    },
    DrawConfRoomInfo: function (R) {
        //Start rfp detail page change on 17 april 
        var htmlview = '';

        htmlview += '<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10">';
        htmlview += '   <div class="panel panel-default" >';

        htmlview += '<div class="panel-heading">';
        htmlview += '   <h3 class="panel-title">';
        htmlview += '       <span class="glyphicon glyphicon-bookmark"></span>&nbsp;<span class="labelheader">' + $($c.controls.hdnRequest).val() + "</span>";
        htmlview += '   </h3>';
        htmlview += '</div>';

        htmlview += '<div class="panel-body">';    

        htmlview += '<div class="clear10"></div>';

        htmlview += '   <div class="row">';
        htmlview += '       <div class="form-group col-lg-4 col-md-4 col-sm-5 col-xs-12 mt10">';
        htmlview += '           <label class="labelheader">';
        htmlview += '               ' + $($c.controls.hdnDateTime).val();
        htmlview += '           </label>';
        htmlview += '           <span class="glyphicon glyphicon-calendar yellow"></span> &nbsp; <span class="labelbox">' + R.display_date_time + "</span>";
        htmlview += '       </div>';
        htmlview += '       <div class="form-group col-lg-4 col-md-4 col-sm-5 col-xs-12 mt10">';
        htmlview += '           <label class="labelheader">';
        htmlview += '               ' + $($c.controls.hdnConferenceRoom).val();
        htmlview += '           </label>';
        htmlview += '           <span class="glyphicon glyphicon-bed yellow"></span> &nbsp; <span class="labelbox">at least ' + R.roomsize + 'm2 and ' + R.display_seating_type + "</span>";
        htmlview += '       </div>';
        htmlview += '       <div class="form-group col-lg-4 col-md-4 col-sm-2 col-xs-12 mt10">';
        htmlview += '           <label class="labelheader">';
        htmlview += '               ' + $($c.controls.hdnParticipants).val();
        htmlview += '           </label>';
        htmlview += '           <span class="glyphicon glyphicon-user yellow"></span> &nbsp; <span class="labelbox">' + R.quantity + "</span>";
        htmlview += '       </div>';
        htmlview += '       <div class="clear10"></div>';

        htmlview += '       <div class="form-group col-lg-4 col-md-4 col-sm-5 col-xs-12 mt15">';
        htmlview += '           <label class="labelheader">';
        htmlview += '               ' + $($c.controls.hdnEquipment).val();
        htmlview += '           </label>';

        htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
        htmlview += '   <span class="badge fr minw50">' + (R.technique != null && R.technique["pens_and_notepads"] == true ? $($c.controls.hdnyes).val() : $($c.controls.hdnno).val()) + '</span> ' + $($c.controls.hdnPensandNotepads).val();
        htmlview += '</div>';

        // htmlview += '<div class="clear10"></div>';

        if (R.technique != null && R.technique["beamer"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["beamer"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnWhiteboard).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["canvas"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["canvas"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnScreen).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["flipchart"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["flipchart"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnFlipchart).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["internet"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["internet"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnInternet).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["isdn"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["isdn"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnISDN).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["microphone"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["microphone"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnMicrophone).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["overhead_projector"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["overhead_projector"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnLCDProjector).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["presentation_case"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["presentation_case"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnMeetingKit).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["rostrum"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["rostrum"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnLectern).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["sound_system"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["sound_system"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnSoundSystem).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["wall"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["wall"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnPinboard).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.technique != null && R.technique["wireless_microphone"] != "0") {
            htmlview += '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 pl0 mt10">';
            htmlview += '   <span class="badge fr minw50">' + (R.technique != null ? R.technique["wireless_microphone"] : "0") + '</span> <span class="labelbox">' + $($c.controls.hdnWirelessMicrophone).val() + "</span>";
            htmlview += '</div>';
        }

        htmlview += '       </div>';
        //htmlview += '       <div class="clear10"></div>';
        htmlview += '       <div class="form-group col-lg-8 col-md-8 col-sm-7 col-xs-12 mt15">';
        htmlview += '           <label class="labelheader">';
        htmlview += '               ' + $($c.controls.hdnFood).val();
        htmlview += '           </label>';

        if (R.food["evening_dinner_buffet"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnevening_dinner_buffet).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["evening_dinner_one_dish"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnevening_dinner_one_dish).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["evening_dinner_snack"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnevening_dinner_snack).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["evening_dinner_three_course_meal"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnevening_dinner_three_course_meal).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["evening_dinner_two_course_meal"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnevening_dinner_two_course_meal).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["evening_drinks_flat"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnevening_drinks_flat).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["evening_one_drink"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnevening_one_drink).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["gala_dinner"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdngala_dinner).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["midday_coffee_and_tea_break"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmidday_coffee_and_tea_break).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["midday_drinks_flat"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmidday_drinks_flat).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["midday_lunch_buffet"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmidday_lunch_buffet).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["midday_lunch_one_dish"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmidday_lunch_one_dish).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["midday_lunch_snack"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmidday_lunch_snack).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["midday_lunch_three_course_meal"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmidday_lunch_three_course_meal).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["midday_lunch_two_course_meal"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmidday_lunch_two_course_meal).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["midday_one_drink"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmidday_one_drink).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["morning_coffee_and_tea"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmorning_coffee_and_tea).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["morning_coffee_and_tea_break"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmorning_coffee_and_tea_break).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["morning_drinks_flat"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmorning_drinks_flat).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["morning_two_drinks"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmorning_two_drinks).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["morning_welcome_coffee_and_tea"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnmorning_welcome_coffee_and_tea).val() + "</span>";
            htmlview += '</div>';
        }

        if (R.food["second_coffebreak"]) {
            htmlview += '<div class="col-lg-6 col-sm-12 col-md-6 col-xs-12 p0 mt10 displaycontents">';
            htmlview += '<span class="glyphicon glyphicon-check yellow"></span> &nbsp;<span class="labelbox">' + $($c.controls.hdnsecond_coffebreak).val() + "</span>";
            htmlview += '</div>';
        }

        htmlview += '       </div>';
        htmlview += '   </div>';
        htmlview += '</div>';

        htmlview += '</div >';
        htmlview += '</div >';

        return htmlview;
    },
    DrawGroupRoom: function (R) {

        var htmlview = '';

        htmlview += '<div class="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12 mt10">';
        htmlview += '   <div class="panel panel-default" >';

        htmlview += '       <div class="panel-heading">';
        htmlview += '           <h3 class="panel-title">';
        htmlview += '                <span class="glyphicon glyphicon-bookmark"></span>&nbsp;<span class="labelheader">' + $($c.controls.hdnRequest).val() + "</span>";
        htmlview += '           </h3>';
        htmlview += '       </div>';

        htmlview += '<div class="panel-body">';
        htmlview += '<div class="clear10"></div>';

        htmlview += '<div class="row">';
        htmlview += '   <div class="form-group col-lg-7 col-md-7 col-sm-12 col-xs-12 mt10">';
        htmlview += '       <label class="labelheader">';
        htmlview += '           ' + $($c.controls.hdnDateTime).val();
        htmlview += '       </label>';
        htmlview += '       <span class="glyphicon glyphicon-calendar yellow"></span> &nbsp;<span class="labelbox">' + R.display_date_time + "</span>";
        htmlview += '   </div>';
        htmlview += '   <div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12 mt10">';
        htmlview += '       <label class="labelheader">';
        htmlview += '           ' + $($c.controls.hdnParticipants).val();
        htmlview += '       </label>';
        htmlview += '       <span class="glyphicon glyphicon-user yellow"></span> &nbsp;<span class="labelbox">' + R.quantity + "</span>";
        htmlview += '   </div>';
        htmlview += '   <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10">';
        htmlview += '       <label class="labelheader">';
        htmlview += '           ' + $($c.controls.hdnConferenceRoom).val();
        htmlview += '       </label>';
        htmlview += '       <span class="glyphicon glyphicon-bed yellow"></span> &nbsp; at least ' + R.roomsize + 'm2 and ' + R.display_seating_type;
        htmlview += '    </div>';
        htmlview += '</div>';
        htmlview += '</div>';
        htmlview += '</div>';
        htmlview += '</div>';

        return htmlview;
    },
    ManageContingents: function (response) {

        if (response.data.micerequests[0] != null && response.data.micerequests[0].rfpmicerequestitems != null) {

            $($c.controls.Accotab).html('');
            $($c.controls.Conftab).html('');
            $($c.controls.Grptab).html('');

            $.each(response.data.micerequests[0].rfpmicerequestitems, function (i) {

                var items = response.data.micerequests[0].rfpmicerequestitems[i];

                if (items.request_itemtype == "Accommodation") {
                    $($c.controls.Accotab).append($MRFP.DrawAccomodation(items.requestitems[0]));
                } else if (items.request_itemtype == "ConferenceRoom") {
                    $($c.controls.Conftab).append($MRFP.DrawConfRoomInfo(items.requestitems[0]));
                } else if (items.request_itemtype == "GroupRoom") {
                    $($c.controls.Grptab).append($MRFP.DrawGroupRoom(items.requestitems[0]));
                }

            });

        }

    },
    ManageHeaders: function (response, micerequestid, isBooked, miceRequestStatus) {
        
        if (isBooked == null || isBooked == '' || isBooked == undefined) {
            isBooked == false;
        }

        
        
        var htmlcontents = "";

        if (response.status == "running" || response.status == "rejected" || response.status == "elapsed") {

            htmlcontents += "   <div class='panel-heading panelgrey'>";
            htmlcontents += "       <h3 class='panel-title'>";
            htmlcontents += "          <span class='glyphicon glyphicon-stats'></span> &nbsp;<span class='CONTENTLink'>" + response.status + "</span>";
            htmlcontents += "       </h3>";
            htmlcontents += "   </div>";

        } else if (response.status == "offered") {

            htmlcontents += "   <div class='panel-heading panelgrey'>";
            htmlcontents += "       <h3 class='panel-title'>";
            htmlcontents += "          <span class='glyphicon glyphicon-stats'></span> &nbsp;<span class='CONTENTLink'>" + response.status + '&nbsp;</span>';
            if (isBooked == false && miceRequestStatus !="aborted"  ) {
                htmlcontents += "       <div class='pull-right'><div><button type='button' onClick='$MRFP.BookOrder(" + micerequestid + "," + response.id + ",);' class='btn draw-borderB fr mt-8 search show-tooltip bookButton' id='btnbook' ><span class='glyphicon glyphicon-send'> </span>&nbsp;&nbsp;" + $($c.controls.hdnBook).val() + "</button></div></div>";
            }

            
            htmlcontents += "       </h3>";
            htmlcontents += "   </div>";

        } else if (response.status == "booked") {

            htmlcontents += "   <div class='panel-heading panelred'>";
            htmlcontents += "       <h3 class='panel-title'>";
            htmlcontents += "          <span class='glyphicon glyphicon-stats'></span> &nbsp;<span class='CONTENTLink'>" + response.status + '</span>';
            htmlcontents += "       </h3>";
            htmlcontents += "   </div>";

        }
       
        return htmlcontents;

    },
    ManageFooters: function (response) {

        //console.log(response);

        var htmlcontents = "";

        if (response.status == "rejected" || response.status == "elapsed") {

            htmlcontents += "<div class='panel-footer fixfooter'>";
            htmlcontents += "   <div class='row'>";


            if (response.rejectReason != null && response.rejectReason != undefined) {
                htmlcontents += "   <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0'>&nbsp;";
                htmlcontents += "       <div class='panel panel-default'>";
                htmlcontents += "           <div class='panel-heading'>";
                htmlcontents += "               <h3 class='panel-title'>";
                htmlcontents += "                   <span class='glyphicon glyphicon-remove'></span>&nbsp;" + response.rejectReason + "&nbsp; (" + response.rejectedTime + ")";
                htmlcontents += "               </h3>";
                htmlcontents += "           </div>";
                htmlcontents += "       </div>";
                htmlcontents += "   </div>";
            } else {
                htmlcontents += "   <span class='labelheader'><b> &nbsp; " + $($c.controls.hdnNoUpdate).val() + " </b> &nbsp; </span>";
            }


            htmlcontents += "   </div>";
            htmlcontents += "</div>";


        } else if (response.status == "running") {

            htmlcontents += "<div class='panel-footer fixfooter'>";
            htmlcontents += "   <div class='row'>";
            htmlcontents += "       <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 ac'>";
            htmlcontents += "           <span class='labelheader'> <b> &nbsp; " + $($c.controls.hdnNoUpdate).val() + " </b> &nbsp; </span>";
            htmlcontents += "       </div>";
            htmlcontents += "   </div>";
            htmlcontents += "</div>";

        } else if (response.status == "offered" || response.status == "booked") {

            htmlcontents += "<div class='panel-footer fixfooter'>";
            htmlcontents += "   <div class='row'>";

            $.each(response.roomPrices, function (i) {

                var data = response.roomPrices[i];

                htmlcontents += "   <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 pb2 pt2'>";
                htmlcontents += "       <div class='panel panel-default'>";
                htmlcontents += "           <div class='panel-heading'>";
                htmlcontents += "               <h3 class='panel-title'>";
                htmlcontents += "                   <span class='badge panelgrey'>" + data.quantity + "</span>&nbsp;<span class='labelheader'>" + $MRFP.NamingContext(data.type) + "</span>";
                htmlcontents += "               <div class='pull-right '><div class='xlarge'><b class='badge badgeY'>" + data.price + "$</b></div></div>";
                htmlcontents += "               </h3>";
                htmlcontents += "           </div>";
                htmlcontents += "           <div class='panel-body'>";
                htmlcontents += "               <div class='clear10'></div>";
                htmlcontents += "               <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 ac'>";
                htmlcontents += "                 <span class='CONTENT11'> from <b> &nbsp; " + data.reserveFromTime + " </b> &nbsp; until <b> &nbsp; " + data.reserveToTime + " </b></span>";
                htmlcontents += "               </div>";
                htmlcontents += "           </div>";
                htmlcontents += "       </div>";
                htmlcontents += "       </div>";

            });

            htmlcontents += "   </div>";
            htmlcontents += "</div>";

        }

        return htmlcontents;

    },
    GetRfPDetail: function (e) {
        //Start rfp detail page change on 17 april 
        window.open('/RFP/rfpdetail?rfpId=' + $(e).attr("data-rfpid") + "&type=" + $(e).attr("data-type"), "_blank");
    },
    NamingContext: function (d) {


        if (d == "room_conference") {
            return $($c.controls.hdnConferenceRoom).val();
        } else if (d == "room_group") {
            return $($c.controls.hdnGroupRoom).val();
        } else {
            return $($c.controls.hdnAccomodation).val();
        }

    },
    ToPascalCase: function (word) {
        if (word != null && word != '') {
            return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase()
        }
        else {
            return "";
        }
       
    }

};


$(document).ready(function () {

    $('body').css('backgroundImage', 'url(../../Images/img_banner_myrfp.png)');

    setTimeout(function () {
        $bind.ManageTab($c.controls.limyrfp);
        $MRFP.GetRFPListData(true);
    }, 500);

    $($c.controls.txtsearchevent).keyup(function () {
        $v.variables.recordsDisplayed = 0;
        $v.variables.totalRecords = 0;
        $v.variables.i = 0;
        $MRFP.GetRFPListData(true);
    });

    $('#material-tabs').each(function () {

        var $active, $content, $links = $(this).find('a');

        $active = $($links[0]);
        $active.addClass('active');

        $content = $($active[0].hash);

        $links.not($active).each(function () {
            $(this.hash).hide();
        });

        $(this).on('click', 'a', function (e) {


            $active.removeClass('active');
            $content.hide();

            $active = $(this);
            $content = $(this.hash);

            $active.addClass('active');
            $content.show();

            e.preventDefault();
        });
    });

});

