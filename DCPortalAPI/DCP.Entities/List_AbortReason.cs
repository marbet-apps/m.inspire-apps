﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class List_AbortReason
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string abort_reason { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string abort_reason_de { get; set; }

        [Required, MaxLength(100), Column(TypeName = "varchar")]
        public string mice_abort_reason { get; set; }
    }
}
