﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class List_LocationType
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(500)]
        [Required]
        public string location_name { get; set; }
        [MaxLength(500)]
        [Required]
        public string location_name_de { get; set; }

        public ICollection<LocationType> LocationType { get; set; }
    }
}
