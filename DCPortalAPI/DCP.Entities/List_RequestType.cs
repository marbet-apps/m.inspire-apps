﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class List_RequestType
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(50), Column(TypeName = "nvarchar")]
        public string request_type { get; set; }

        [Required, MaxLength(50), Column(TypeName = "nvarchar")]
        public string request_type_de { get; set; }

        [Required, MaxLength(50), Column(TypeName = "varchar")]
        public string mice_request_type { get; set; }
    }
}
