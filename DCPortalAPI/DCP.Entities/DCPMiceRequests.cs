﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class DCPMiceRequests
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("RFP")]
        public int rfp_id { get; set; }

        public int? mice_request_id { get; set; }

        public int resource_type { get; set; }

        [Required, Column(TypeName = "ntext")]
        public string request_detail_object { get; set; }

        public int request_type { get; set; }

        public int created_by { get; set; }

        public DateTime created_date { get; set; }

        public int? modified_by { get; set; }

        public DateTime? modified_date { get; set; }

        public RequestForProposal RFP { get; set; }
    }
}
