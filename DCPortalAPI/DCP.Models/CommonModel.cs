﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Models
{
    public class Lookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class Lookup<T>
    {
        public T Value { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class LookupString
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }

    public class TemplateLookUpString
    {
        public List<LookupString> lookup { get; set; }
        public int totalRecord { get; set; }
    }

    public class TemplateLookUp
    {
        public List<Lookup> lookup { get; set; }
        public int totalRecord { get; set; }
    }

}
