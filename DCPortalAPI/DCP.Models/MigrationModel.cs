using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace DCP.Models
{
    public class MigrationHotelModel
    {
        public List<HotelMigrationModel> Hotels;
        public List<kb_Hotel_Type> HotelType;
        public List<Photos> Photos;
        
    }
    public class MigrationLocationModel
    {
        public List<LocationMigerationModel> Locations;
        public List<LocationType> LocationType;
        public List<KbSuitable> KbSuitable;
        public List<Photos> Photos;
    }
    public class MigrationResaurantModel
    {
        public List<RestaurantMigerationModel> Restaurants;
        public List<kb_restaurant_type> RestaurantTType;
        public List<kb_kitchen> Kitchens;
        public List<KbSuitable> KbSuitable;
        public List<Photos> Photos;
    }

    public class HotelMigrationModel
    {
        [JsonProperty(PropertyName = "kb_hotelinfo_id")]
        public int kb_id { get; set; }
        public string mice_id { get; set; }
        public string hotel_name { get; set; }
        public string hotel_chain { get; set; }
        public int? country_id { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }
        public int star { get; set; }
        public int? airport_id { get; set; }
        public string airport_transfer_time { get; set; }
        public double? airport_distance { get; set; }
        public int? total_rooms { get; set; }
        public int? total_conference { get; set; }
        public double? size_of_largest_conference { get; set; }
        public int? parking_spaces { get; set; }
        public string miscellaneous_info { get; set; }
        public bool? PP { get; set; }
        public bool? green { get; set; }
        public bool? reopening { get; set; }
        public bool? blacked { get; set; }
        public bool? Closed { get; set; }
        public double? hotel_lan { get; set; }
        public double? hotel_lat { get; set; }
        public bool? is360degreeview { get; set; }
        public int created_by { get; set; }
        public DateTime created_date { get; set; }
        public int modify_by { get; set; }
        public DateTime modify_date { get; set; }

        public int smt_hoteltypelist_id { get; set; }

        public string photo_name { get; set; }
        public int photo_size { get; set; }
        public int kb_photo_type{get;set;}
    }


    public class LocationMigerationModel
    {
        [JsonProperty(PropertyName = "kb_locationinfo_id")]
        public int kb_id { get; set; }
        public string mice_id { get; set; }
        public string location_name { get; set; }
        public int country_id { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }
        public int? total_conference { get; set; }
        public double? size_of_largest_conference { get; set; }
        public double? room_height { get; set; }
        public double? rent_begining { get; set; }
        public bool? catering { get; set; }
        public bool? technik { get; set; }
        public bool? car_drive { get; set; }
        public bool? outdoor_possibility { get; set; }
        public string outdoor_details { get; set; }
        public string miscellaneous_info { get; set; }
        public bool? PP { get; set; }
        public bool? green { get; set; }
        public bool? reopening { get; set; }
        public bool? blacked { get; set; }
        public bool? closed { get; set; }
        public double? location_lan { get; set; }
        public double? location_lat { get; set; }
        public bool? is360degreeview { get; set; }
        //public int created_by { get; set; }
        //public DateTime created_date { get; set; }
        //public int modify_by { get; set; }
        //public DateTime modify_date { get; set; }
        //public int smt_list_locationtype_id { get; set; }
        //public int smt_suitablefor_id { get; set; }
        //public int kb_tripinfo_type { get; set; }
        //public string photo_name { get; set; }
        //public int photo_size { get; set; }
        //public int kb_photo_type { get; set; }


    }

    public class LocationType
    {
        public int smt_list_locationtype_id { get; set; }
        //[JsonProperty(PropertyName = "kb_locationinfo_id")]
        public int kb_locationinfo_id { get; set; }
    }
    public class KbSuitable
    {
        public int smt_suitablefor_id { get; set; }
        public int kb_tripinfo_type { get; set; }
        public int kb_transaction_id { get; set; }

    }
    public class Photos
    {
        public string photo_name { get; set; }
        public long photo_size { get; set; }
        public int kb_photo_type { get; set; }
        public int kb_transaction_id { get; set; }

    }

    public class RestaurantMigerationModel
    {
        [JsonProperty(PropertyName = "kb_restaurantinfo_id")]
        public int kb_id { get; set; }
        public string mice_id { get; set; }
        public string restaurant_name { get; set; }
        public int? country_id { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }
        public bool restaurant_in_hotel { get; set; }
        public string hotelname { get; set; }
        public int? capacity { get; set; }
        public int? total_conference { get; set; }
        public double? size_of_largest_conference { get; set; }
        public int? capacity_of_largest_room { get; set; }
        public bool? outdoor_possibility { get; set; }
        public string outdoor_details { get; set; }
        public string miscellaneous_info { get; set; }
        public double? minimum_turnover { get; set; }
        public bool? PP { get; set; }
        public bool? green { get; set; }
        public bool? reopening { get; set; }
        public bool? blacked { get; set; }
        public bool? closed { get; set; }
        public double? restaurant_lan { get; set; }
        public double? restaurant_lat { get; set; }
        public bool? is360degreeview { get; set; }
    }

    public class kb_restaurant_type
    {
        public int smt_restauranttypelist_id { get; set; }
        public int kb_restaurantinfo_id { get; set; }
    }
    public class kb_kitchen
    {
        public int smt_kitchenlist_id { get; set; }
        public int kb_restaurantinfo_id { get; set; }
    }
    public class ResponseSyncModel
    {
        [JsonProperty(propertyName: "resourceid")]
        public int ResourceId { get; set; }

        [JsonProperty(propertyName: "issyncsuccess")]
        public bool IsSyncSuccess { get; set; }

        [JsonProperty(propertyName: "ResourceType")]
        public string ResourceType { get; set; }
    }

    public class AirportModel
    {
        public int airport_id { get; set; }

        public string airport_name { get; set; }

        public int? country_id { get; set; }

        public string state { get; set; }

        public string city { get; set; }

        public string zipcode { get; set; }

        public double? airport_lan { get; set; }

        public double? airport_lat { get; set; }

    }

    public class CountryModel
    {
        public int id { get; set; }

        public string country_name { get; set; }

        public string country_name_de { get; set; }

        public string country_code { get; set; }
    }

    public class HotelTypeModel
    {
        public int id { get; set; }

        public string hoteltype_name { get; set; }

        public string hoteltype_name_de { get; set; }
    }

    public class RestaurantTypeModel
    {
        public int id { get; set; }

        public string restaurant_name { get; set; }

        public string restaurant_name_de { get; set; }
    }

    public class LocationTypeModel
    {
        public int id { get; set; }

        public string location_name { get; set; }

        public string location_name_de { get; set; }
    }

    public class KitchenModel
    {
        public int id { get; set; }

        public string kitchen_name { get; set; }

        public string kitchen_name_de { get; set; }
    }

    public class SuitableForModel
    {
        public int id { get; set; }

        public string suitablefor_name { get; set; }

        public string suitablefor_name_de { get; set; }

        public int suitablefor_type { get; set; }
    }

    public class KbStatusModel
    {
        public int id { get; set; }

        public int kb_id { get; set; }

        public int kb_type { get; set; }

        public string dcp_status { get; set; }

        public string update_from { get; set; }
    }

    public class kb_Hotel_Type
    {
        public int kb_hoteltype_id { get; set; }

        public int kb_hotelinfo_id { get; set; }

        public int smt_hoteltypelist_id { get; set; }
    }

}
