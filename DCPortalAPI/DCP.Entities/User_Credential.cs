﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
namespace DCP.Entities
{
    public class User_Credential
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("user_Masters")]
        public int User_Master_Id { get; set; }

        [Required, Column(TypeName = "varchar"), Index(IsUnique =true)]
        public string Email { get; set; }

        [Column(TypeName = "varchar"),MaxLength(4000)]
        public string Password { get; set; }

        public int? Mice_Userid { get; set; }

        [Column(TypeName = "varchar"), MaxLength(200)]
        public string Mice_Username { get; set; }

        [Column(TypeName = "varchar"), MaxLength(200)]
        public string Mice_Password { get; set; }

        public int? created_by { get; set; }
        public DateTime? created_date { get; set; }
        public int? modified_by { get; set; }
        public DateTime? modify_date { get; set; }
        public User_Master user_Masters { get; set; }
    }
}
