﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DCP.Models;
namespace DCP.Repository.Interface
{
    public interface IUserRepository
    {
        List<UserRegisterModel> GetUserList(string search, string sortBy, int loggedInUserId);

        bool DeleteUser(int userId);

        UserRegisterModel GetUserDetail(int userId, string lang = "");

        ResponseModel UpdateUser(UpdateUserModel model);
    }
}
