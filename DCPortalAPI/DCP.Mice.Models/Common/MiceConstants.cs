﻿namespace DCP.Mice.Models.Common
{
    public static class MiceConstants
    {
        public class APIConstants
        {
            public const string APIURL = "https://jsonapi.mice-access.com/";
        }

        public class RequestType
        {
            public const string GROUPS = "groups";
            public const string MEETINGS = "meetings";
            public const string EVENTS = "events";
            public const string INDIVIDUAL = "individual";
        }

        public class RequestItemType
        {
            public const string ACCOMMODATION = "Accommodation";
            public const string CONFERENCE_ROOM = "ConferenceRoom";
            public const string GROUP_ROOM = "GroupRoom";
        }

        public class RoomType
        {
            public const string ROOM_SINGLE_BED = "room_single_bed";
            public const string ROOM_DOUBLE_BED = "room_double_bed";    
            public const string ROOM_DOUBLE_BED_SINGLE_USE = "room_double_bed_single_use";
            public const string ROOM_THREE_BED = "room_three_bed";
            public const string ROOM_FOUR_BED = "room_four_bed";
            public const string ROOM_TWIN = "room_twin";
            public const string ROOM_JUNIOR_SUITE = "room_junior_suite";
            public const string ROOM_SUITE = "room_suite";
        }

        public class SeatingType
        {
            public const string ROWS = "rows";
            public const string CIRCLE = "circle";
            public const string TABLES = "tables";
            public const string USHAPE = "u";
            public const string PARLIAMENTARY = "parliamentary";
            public const string BLOCK = "block";
            public const string NOSEATING = "no_seating";
            public const string BANQUET = "banquet";
            public const string CABARET = "cabaret";
        }

        public class AbortReason
        {
            public const string CHANGED_DATE = "changed_date";
            public const string CHANGED_DESTINATION = "changed_destination";
            public const string CHANGED_HOTEL_SELECTION = "changed_hotel_selection";
            public const string INCORRECT = "incorrect";
        }

        public class DurationChioce
        {
            public const int FIVE = 5;
            public const int TEN = 10;
            public const int FIFTEEN = 15;
            public const int TWENTY = 20;
        }
    }
}
