﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace DCP.Models.RFP
{
    public class RFPMiceRequestModel
    {
        public RFPMiceRequestModel()
        {
            requestItemObjects = new List<RequestItemObject>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("rfpid")]
        public int RfpId { get; set; }
        [JsonProperty("micerequestid")]
        public int? MiceRequestId { get; set; }
        [JsonProperty("resource_type")]
        public int ResourceType { get; set; }
        [JsonProperty("comments")]
        public string Comments { get; set; }
        [JsonProperty("comments2")]
        public string Comments2 { get; set; }
        [JsonProperty("comments3")]
        public string Comments3 { get; set; }
        [JsonProperty("comments4")]
        public string Comments4 { get; set; }
        [JsonProperty("city_id")]
        public int? CityId { get; set; }
        [JsonProperty("place_id")]
        public int? PlaceId { get; set; }
        [JsonProperty("country_id")]
        public int? CountryId { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("created_by")]
        public int CreatedBy { get; set; }
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("modified_by")]
        public int? ModifiedBy { get; set; }
        [JsonProperty("modified_date")]
        public DateTime? ModifiedDate { get; set; }
        [JsonProperty("rfpresources")]
        public List<RFPResourceModel> RFPResources { get; set; }
        [JsonProperty("rfpmicerequestitems")]
        public List<RequestItemModel> RFPMiceRequestItems { get; set; }
        [JsonProperty("request_detail")]
        public RFPMiceRequestDetailModel RequestDetail { get; set; }
        [JsonProperty("order_detail")]
        public RFPMiceRequestOrderModel OrderDetail { get; set; }

        public List<RequestItemObject> requestItemObjects { get; set; }
    }


  
}
