﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class RequestCart
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("RequestedUser")]
        public int user_id { get; set; }

        public int created_by { get; set; }

        public DateTime created_date { get; set; }

        public int? modified_by { get; set; }

        public DateTime? modified_date { get; set; }

        public User_Master RequestedUser { get; set; }

        public ICollection<RequestCartResource> CartResources { get; set; }
    }
}
