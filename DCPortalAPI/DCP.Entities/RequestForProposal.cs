﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class RequestForProposal
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(100), Column(TypeName = "varchar")]
        public string rfp_type { get; set; }

        public string event_name { get; set; }

        [ForeignKey("RFPUser")]
        public int user_id { get; set; }

        public int? mice_user_id { get; set; }

        public int? days_running { get; set; }

        public bool is_submitted { get; set; }

        [Required, MaxLength(50), Column(TypeName = "varchar")]
        public string status { get; set; }

        public int created_by { get; set; }

        public DateTime created_date { get; set; }

        public int? modified_by { get; set; }

        public DateTime? modified_date { get; set; }

        public User_Master RFPUser { get; set; }

        public ICollection<RFPMiceRequest> RFPMiceRequests { get; set; }

        public DateTime? eventstartdate { get; set; }

        public DateTime? eventenddate { get; set; }
    }
}
