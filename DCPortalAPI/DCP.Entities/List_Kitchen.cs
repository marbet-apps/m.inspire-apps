﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class List_Kitchen
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(500)]
        [Required]
        public string kitchen_name { get; set; }
        [MaxLength(500)]
        [Required]
        public string kitchen_name_de { get; set; }

        public ICollection<KitchenType> KitchenType { get; set; }
    }
}
