﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Models
{

    /// <summary>
    /// Base Filter criteria
    /// </summary>
    public class BaseFilterModel
    {
        public int displayRecords { get; set; }
        public int pageSize { get; set; }
        public string sort { get; set; }
        public int userId { get; set; }
        public bool ishotel { get; set; }
        public bool isrestaurant { get; set; }
        public bool islocation { get; set; }
    }

    /// <summary>
    /// Filter Model
    /// </summary>
    public class FilterModel : BaseFilterModel
    {
        public HFilterModel HFilterModel { get; set; }
        public RFilterModel RFilterModel { get; set; }
        public LFilterModel LFilterModel { get; set; }
        
        public string country { get; set; }
        public string city { get; set; }
        public double? lat { get; set; }
        public double? lon { get; set; }
        public double? distance { get; set; }
        public string selectedDistanceList { get; set; }
        public string name { get; set; }
        public bool isFromHistory { get; set; }
    }

    /// <summary>
    /// Hotel Model
    /// </summary>
    public class HFilterModel
    {
        public string hotelname { get; set; }
        public string hotelchains { get; set; }
        public string htotalroom { get; set; }
        public string htotalcroom { get; set; }
        public string hsizecroom { get; set; }
        public string hparkingspace { get; set; }
        public string typeids { get; set; }
        public string stars { get; set; }
        public string airporthours { get; set; }
        public string airportminutes { get; set; }
    }

    /// <summary>
    /// Restaurant Model
    /// </summary>
    public class RFilterModel
    {
        public string restaurantname { get; set; }
        public string kids { get; set; }
        public string typeids { get; set; }
        public string suitableids { get; set; }
        public string rtotalroom { get; set; }
        public string rsizecroom { get; set; }
        public string rroomcapacity { get; set; }
        public string rrestaurantcapacity { get; set; }
        public string rminimumturnover { get; set; }
        public string chkoutdoor { get; set; }
        public string chkrestinhotel { get; set; }
    }

    /// <summary>
    /// Location Model
    /// </summary>
    public class LFilterModel
    {
        public string locationname { get; set; }
        public string typeids { get; set; }
        public string suitableids { get; set; }
        public string ltotalcroom { get; set; }
        public string lsizecroom { get; set; }
        public string lroomheight { get; set; }
        public string lrentalcost { get; set; }
        public string chkCP { get; set; }
        public string chkTP { get; set; }
        public string chkCD { get; set; }
        public string chkoutdoor { get; set; }
        
    }


    /// <summary>
    /// Base Resource Model
    /// </summary>
    public class BaseResourceModel
    {
        public BaseResourceModel()
        {
            ResourceModel = new List<ResourceModel>();
        }

        public List<ResourceModel> ResourceModel { get; set; }

        public int totalrecord { get; set; }
    }

    /// <summary>
    /// Resource data
    /// </summary>
    public class ResourceModel
    {
        // Common Details
        public int id { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public int countryid { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string street { get; set; }
        public string webaddress { get; set; }
        public string info { get; set; }
        public string zipcode { get; set; }
        public double? lng { get; set; }
        public double? lat { get; set; }
        public string time { get; set; }
        public double? distance { get; set; }

        public double? diffDistance { get; set; }
        public double? KM { get; set; }
        public bool? is360degree { get; set; }
        public string type { get; set; }
        public bool? isincart { get; set; }
        //public int cartId { get; set; }
        // Hotel Details
        public int? stars { get; set; }
        public string chainname { get; set; }
        public string airport { get; set; }
        public int? htotalroom { get; set; }
        public int? htotalcroom { get; set; }
        public double? hsizecroom { get; set; }
        public Nullable<int> hparking_spaces { get; set; }

        // Restaurant Details
        public int? rcapacity { get; set; }
        public int? rkitchenId { get; set; }
        public int? rtotal_conference { get; set; }
        public int? rcapacity_of_largest_room { get; set; }
        public double? rsize_of_largest_conference { get; set; }
        public double? rminimum_turnover { get; set; }

        // Location Details
        public int? ltotal_conference { get; set; }
        public double? lroom_height { get; set; }
        public double? lsize_of_largest_conference { get; set; }
        public double? lrent_begining { get; set; }
        public bool? chkCP { get; set; }
        public bool? chkTP { get; set; }
        public bool? chkCD { get; set; }

        public string hotelname { get; set; }
        public string outdoor_details { get; set; }
        public bool? chkOP { get; set; }
        public bool? chkRIH { get; set; }
       
        public IQueryable<string> lstkitchen { get; set; }
        public string kitchen { get; set; }

        public IQueryable<string> lstsuitablefor { get; set; }
        public string suitablefor { get; set; }

        public IQueryable<string> lstrestauranttype { get; set; }
        public string restauranttype { get; set; }

        public IQueryable<string> lsthoteltype { get; set; }
        public string hoteltype { get; set; }

        public IQueryable<string> lstlocationtype { get; set; }
        public string locationtype { get; set; }

    }

    /// <summary>
    /// Hotel Model
    /// </summary>
    public class HotelModel
    {
        public int id { get; set; }
        public Nullable<int> star { get; set; }
        public Nullable<int> total_rooms { get; set; }
        public Nullable<int> total_conference { get; set; }
        public Nullable<int> parking_spaces { get; set; }

        public string hotel_name { get; set; }
        public string hotel_chain { get; set; }
        public string street { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }
        public string miscellaneous_info { get; set; }
        public string airport_name { get; set; }
        public string airport_transfer_time { get; set; }
        public string hour { get; set; }
        public string minute { get; set; }

        public Nullable<double> size_of_largest_conference { get; set; }
        public Nullable<double> airport_distance { get; set; }

        public IQueryable<string> lsthoteltype { get; set; }
        public bool? green { get; set; }
        public bool? blacked { get; set; }
        public bool? closed { get; set; }
        public double? lan { get; set; }
        public double? lat { get; set; }
        public bool? is360degreeview { get; set; }
        public string hotel_type { get; set; }
        public bool isincart { get; set; }
        public bool? reopening { get; set; }

    }

    /// <summary>
    /// Restaurant Model
    /// </summary>
    public class RestaurantModel
    {

        public int id { get; set; }
        public string restaurant_name { get; set; }
        public string country { get; set; }
        public string street { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }
        public bool restaurant_in_hotel { get; set; }
        public string hotelname { get; set; }
        public IQueryable<string> lstkitchen { get; set; }
        public Nullable<int> capacity { get; set; }
        public Nullable<int> total_conference { get; set; }
        public Nullable<double> size_of_largest_conference { get; set; }
        public Nullable<int> capacity_of_largest_room { get; set; }
        public bool? outdoor_possibility { get; set; }
        public string outdoor_details { get; set; }
        public double? minimum_turnover { get; set; }
        public string miscellaneous_info { get; set; }
        public string kitchen { get; set; }
        public IQueryable<string> lstrestauranttype { get; set; }
        public bool? green { get; set; }
        public bool? blacked { get; set; }
        public bool? closed { get; set; }
        public double? lan { get; set; }
        public double? lat { get; set; }
        public bool? is360degreeview { get; set; }
        public IQueryable<string> lstsuitableforwhat { get; set; }

        public string restaurant_type { get; set; }

        public string suitableforwhat { get; set; }
        public bool isincart { get; set; }
        public bool? reopening { get; set; }
    }

    /// <summary>
    /// Location Model
    /// </summary>
    public class LocationModel
    {
        public int id { get; set; }
        public string location_name { get; set; }

        public string street { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }

        public Nullable<int> total_conference { get; set; }
        public Nullable<double> size_of_largest_conference { get; set; }
        public Nullable<double> room_height { get; set; }
        public Nullable<double> rent_begining { get; set; }

        public bool? catering { get; set; }
        public bool? technik { get; set; }
        public bool? car_drive { get; set; }
        public bool? outdoor_possibility { get; set; }
        public string outdoor_details { get; set; }
        public string miscellaneous_info { get; set; }
        public string location_type { get; set; }

        public IQueryable<string> lstlocationtype { get; set; }

        public bool? green { get; set; }
        public bool? blacked { get; set; }
        public bool? closed { get; set; }
        public double? lan { get; set; }
        public double? lat { get; set; }
        public IQueryable<string> lstsuitableforwhat { get; set; }
        public string suitableforwhat { get; set; }
        public bool isincart { get; set; }
        public bool? reopening { get; set; }
    }

    public class PhotosModel
    {
        public int photo_id { get; set; }
        public int photo_type { get; set; }
        public int transaction_id { get; set; }
        public string photo_name { get; set; }
        public long photo_size { get; set; }

    }

    public class NearByModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public double? lng { get; set; }
        public double? lat { get; set; }
        public double? KM { get; set; }
    }

    public class SelectedLookup
    {
        public int id { get; set; }
        public string text { get; set; }
    }
    public class SelectedLookupString
    {
        public string id { get; set; }
        public string text { get; set; }
    }

}
