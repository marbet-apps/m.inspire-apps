﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Reflection;

namespace DCP.Entities
{
    public class DCPDbContext : DbContext
    {
        public DCPDbContext() : base("name=DCPConnectionString")
        {
            //Database.SetInitializer<DCPDbContext>(new CreateDatabaseIfNotExists<DCPDbContext>());

            //Disable initializer
            Database.SetInitializer<DCPDbContext>(null);
        }

        public DbSet<User_Master> User_Masters { get; set; }
        public DbSet<Admin_Master> Admin_Master { get; set; }
        public DbSet<User_Communication> User_Communication { get; set; }
        public DbSet<User_Credential> User_Credential { get; set; }
        public DbSet<List_Country> List_Country { get; set; }
        public DbSet<List_Airport> List_Airport { get; set; }
        public DbSet<List_HotelType> List_HotelType { get; set; }
        public DbSet<List_Kitchen> List_Kitchen { get; set; }
        public DbSet<List_LocationType> List_LocationType { get; set; }
        public DbSet<List_RestaurantType> List_RestaurantType { get; set; }
        public DbSet<List_SubscriptionType> List_SubscriptionType { get; set; }
        public DbSet<List_SuitableFor> List_SuitableFor { get; set; }
        public DbSet<Locations> Locations { get; set; }
        public DbSet<Photos> Photos { get; set; }
        public DbSet<Restaurants> Restaurants { get; set; }
        public DbSet<Hotels> Hotels { get; set; }
        public DbSet<HotelType> HotelType { get; set; }
        public DbSet<RestaurantType> RestaurantType { get; set; }
        public DbSet<LocationType> LocationType { get; set; }
        public DbSet<KitchenType> KitchenType { get; set; }
        public DbSet<SuitableForType> SuitableForType { get; set; }
        public DbSet<ConferenceRoom> ConferenceRoom { get; set; }
        public DbSet<ConferenceRoomQuantities> ConferenceRoomQuantities { get; set; }
        public DbSet<User_Access> User_Accessess { get; set; }
        public DbSet<List_AbortReason> AbortReasons { get; set; }
        public DbSet<List_RequestItem> RequestItemTypes { get; set; }
        public DbSet<List_RoomTechnique> RoomTechniques { get; set; }
        public DbSet<List_RoomType> RoomTypes { get; set; }
        public DbSet<List_SeatingType> SeatingTypes { get; set; }
        public DbSet<List_ConferenceFood> ConferenceFoods { get; set; }
        public DbSet<List_RequestType> RequestTypes { get; set; }
        public DbSet<RequestForProposal> RequestForProposals { get; set; }
        public DbSet<RFPResource> RFPResources { get; set; }
        public DbSet<List_Radius> List_Radius { get; set; }
        public DbSet<List_ConferenceFoodGroup> List_ConferenceFoodGroups { get; set; }
        public DbSet<RequestCart> RequestCarts { get; set; }
        public DbSet<RFPMiceRequest> RFPMiceRequests { get; set; }
        public DbSet<RFPMiceRequestDetails> RFPMiceRequestDetails { get; set; }
        public DbSet<RFPMiceRequestItems> RFPMiceRequestItems { get; set; }
        public DbSet<RFPMiceRequestOrders> RFPMiceRequestOrders { get; set; }
        public DbSet<RequestCartResource> RequestCartResources { get; set; }
        public DbSet<List_RequestStatus> RequestStatuses { get; set; }
        public DbSet<List_DaysRunning> List_DaysRunnings { get; set; }
        public DbSet<DCPMiceRequests> DCPMiceRequests { get; set; }
        public DbSet<SearchHistory> SearchHistory { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
           .Where(type => !String.IsNullOrEmpty(type.Namespace))
           .Where(type => type.BaseType != null && type.BaseType.IsGenericType
                && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Entity<Admin_Master>()
             .HasOptional(a => a.Created_by1)
             .WithMany()
             .HasForeignKey(a => a.Created_By);

            modelBuilder.Entity<Admin_Master>()
                .HasOptional(a => a.Modify_By1)
                .WithMany()
                .HasForeignKey(a => a.Modify_By);

    //        modelBuilder.Entity<Hotels>()
    //.HasOptional(a => a.country_id)
    //.WithMany()
    //.HasForeignKey(a => a.Modify_By);
            base.OnModelCreating(modelBuilder);
        }
    }
}
