﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class Admin_Master
    {
        [Key]
        public int User_Id { get; set; }

        
        [MaxLength(100)]
        public string First_Name { get; set; }
        [MaxLength(100)]
        public string Last_Name { get; set; }

        [Required, Column(TypeName = "varchar"), MaxLength(100), Index(IsUnique = true)]
        public string Email { get; set; }

        [Column(TypeName = "varchar"), MaxLength(4000)]
        public string Password { get; set; }

        public bool IsActive { get; set; }

        public int User_Type { get; set; }
        
        public int? Created_By { get; set; }

        public Admin_Master Created_by1 { get; set; }

        public DateTime? Created_Date { get; set; }

        public int? Modify_By { get; set; }

        public Admin_Master Modify_By1 { get; set; }

        public DateTime? Modify_Date { get; set; }
    }
}
