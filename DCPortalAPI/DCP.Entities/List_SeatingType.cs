﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class List_SeatingType
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string seating_type { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string seating_type_de { get; set; }

        [Required, MaxLength(100), Column(TypeName = "varchar")]
        public string mice_seating_type { get; set; }
    }
}
