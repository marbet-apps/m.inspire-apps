﻿using DCP.Business.Interface;
using DCP.Models;
using DCP.Utilities;
using DCPortalAPI.Settings;
using DCPortalAPI.Filter;
using System.Web.Http;
using System.Net.Mail;
using System.Net.Mime;
using System;

namespace DCPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/Secure")]
    [AllowAnonymous]
    [ValidateModel]
    [ExceptionHandler]
    public class SecureController : ApiController
    {
        private IBSecure _IBSecure;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="IBSecure"></param>
        public SecureController(IBSecure IBSecure)
        {
            this._IBSecure = IBSecure;
        }

        /// <summary>
        /// If user is valid, This action method redirect user to home screen 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Login(CredentialRequest login)
        {
            WriteLog.LogInformation(LogMessages.LoginStarted);

            try
            {
                var Response = _IBSecure.CheckUserAuth(login.Email, Crypto.EncryptSHA512Managed(login.Password), out bool flag);

                if (flag)
                {
                    var userModel = (ResponseUserLoginModel)Response.Data;
                    return Ok(Response).AddHeader("authorization", TokenValidationHandler.CreateToken(login.Email, userModel.User_Type));
                }

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.LoginEnded);

            }
        }

        /// <summary>
        /// generate new password of the user.
        /// </summary>
        /// <param name="userPassword"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("ForgotPassword")]
        public IHttpActionResult ForgotPassword(UserPasswordModel userPassword)
        {
            WriteLog.LogInformation(LogMessages.GeneratePasswordStarted);

            try
            {
                string requestDateTime = Crypto.EnryptString(System.DateTime.Now.ToString("ddMMyyyyHHmm"));

                // userPassword.Password = Crypto.GeneratePassword(5);
                var result = _IBSecure.ForgotUserPassword(userPassword, out bool flag, out string name, requestDateTime);

                if(result.Code=="707")
                {
                    var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                    var model = (UserPasswordModel)result.Data;
                    model.strId = Crypto.encrypt(model.Id.ToString());

                    //string path = "/Images/logo_header.png";
                    //LinkedResource Img = new LinkedResource(path, MediaTypeNames.Image.Jpeg);
                    //Img.ContentId = "MyImage";

                   
                    bool isSuccess = MailUtility.SendForgotPasswordMail(lang, name, userPassword.Email, userPassword.Password, model.strId, requestDateTime);

                    if (isSuccess == false)
                    {
                        result.Code = ResponceCodes.Success.FogotPasswordMailFail;
                        result.Message = ResponceMessages.Success.FogotPasswordMailFail;
                    }
                }
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GeneratePasswordEnded);

            }
        }

        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="resetPassword"></param>
        /// <returns></returns>
        [Route("ResetPassword")]
        public IHttpActionResult ResetPassword(ResetPasswordModel resetPassword)
        {
            WriteLog.LogInformation(LogMessages.ResetPasswordStarted);
            try
            {
                var result = _IBSecure.ResetPassword(resetPassword);
                var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                if (result.Code=="723")
                {
                    UserModel model = (UserModel)result.Data;
                    MailUtility.SendPasswordGeneratedMail(lang, model.First_Name + " " + model.Last_Name, model.Email);
                }
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.ResetPasswordEnded);
            }

        }

        /// <summary>
        ///  Validate User
        /// </summary>
        /// <param name="resetPassword"></param>
        /// <returns></returns>
        [Route("ValidateUser")]
        public IHttpActionResult ValidateUser(ResetPasswordModel resetPassword)
        {
            WriteLog.LogInformation(LogMessages.ResetPasswordStarted);
            try
            {
                var result = _IBSecure.ValidateUser(resetPassword);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.ResetPasswordEnded);
            }

        }

    }
}