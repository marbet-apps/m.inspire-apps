﻿using Newtonsoft.Json;

namespace DCP.Mice.Models
{
    public class CustomerModel
    {
        /// <summary>
        /// Set commercial use to true, if that request belongs to a company or false if this is for private reasons.
        /// </summary>
        [JsonProperty("is_commercial")]
        public bool IsCommercial { get; set; }

        /// <summary>
        /// Company name.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Gender of person submiting this request. choice['male'|'female']
        /// </summary>
        [JsonProperty("salutation")]
        public string Salutation { get; set; }

        /// <summary>
        /// Firstname of person submiting this request.
        /// </summary>
        [JsonProperty("firstname")]
        public string FirstName { get; set; }

        /// <summary>
        /// Lastname of person submiting this request.
        /// </summary>
        [JsonProperty("lastname")]
        public string LastName { get; set; }

        /// <summary>
        /// Street (incl. Number).
        /// </summary>
        [JsonProperty("address_street")]
        public string AddressStreet { get; set; }

        /// <summary>
        /// Zipcode.
        /// </summary>
        [JsonProperty("address_zipcode")]
        public string AddressZipCode { get; set; }

        /// <summary>
        /// Location.
        /// </summary>
        [JsonProperty("address_location")]
        public string AddressLocation { get; set; }

        /// <summary>
        /// Countrycode to that address. For a complete list, see Countries.
        /// </summary>
        [JsonProperty("address_countrycode")]
        public string AddressCountryCode { get; set; }

        /// <summary>
        /// e-mail-Address used for creating account in MICE access.
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// Telephone.
        /// </summary>
        [JsonProperty("address_telephone")]
        public string AddressTelephone { get; set; }
    }

    public class CustomerDetailModel : CustomerModel
    {
        [JsonProperty("country")]
        public Country Country { get; set; }
    }
}
