﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Utilities
{
    public class ManageConstants
    {
        #region RFP

        public class RFPStatus
        {
            public const string NEW = "new";
            public const string SUBMITTED = "submitted";
        }

        #endregion RFP

        #region Profile

        public static  class ProfileSetting
        {
            public const int maxWidth = 250;
            public const int maxHeight = 250;
        }
        #endregion
    }
}
