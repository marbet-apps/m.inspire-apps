﻿$(document).ready(function () {
    if (ajaxRequest.getAuthorizationToken() == undefined || ajaxRequest.getAuthorizationToken() == null) {
        location.href = "/login";
    }
    $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
    
    $rfp.validaterights();
    
    var id = $rfp.GetURLParameter();
    if ((id == '') || (id!=null && id.toLocaleLowerCase()=="startrfp")) {
       // $rfp.startRFP();
        ///INSPIRE-102
        ///When user click on startrfp, fill detail from request cart realted tables.
        var url = $bind.URL($rfp.apiPath.GetRequestForProposalDetail);
        ajaxRequest.httpGetSyncWithData(url, null, $rfp.getRequestForProposalDetailNewOnSuccess, $rfp.getRequestForProposalDetailOnError);
    }
    else {
        $rfp.getRequestForProposalById(0,id);
    }


    var $sortable = $('.sortable');

    $sortable.on('click', function () {

        var $this = $(this);
        var asc = $this.hasClass('asc');
        var desc = $this.hasClass('desc');
        $sortable.removeClass('asc').removeClass('desc');
        if (desc || (!asc && !desc)) {
            $this.addClass('asc');
        } else {
            $this.addClass('desc');
        }
        var tableid = $('#' + $(this).closest('th')[0].id).closest('table')[0].id
        if (tableid === "tblHotelAccomodation") {
            $rfp.getResourceItemDetail($(this).closest('th')[0].id + ($(this).hasClass('asc') == true ? 'asc' : 'desc'), 'H', 'Accommodation')
        }
        if (tableid === "tblHotelConferenceRoom") {
            $rfp.getResourceItemDetail($(this).closest('th')[0].id + ($(this).hasClass('asc') == true ? 'asc' : 'desc'), 'H', 'ConferenceRoom')
        }
        if (tableid === "tblHotelGroupRoom") {
            $rfp.getResourceItemDetail($(this).closest('th')[0].id + ($(this).hasClass('asc') == true ? 'asc' : 'desc'), 'H', 'GroupRoom')
        }
        if (tableid === "tblRestaurantConferenceRoom") {
            $rfp.getResourceItemDetail($(this).closest('th')[0].id + ($(this).hasClass('asc') == true ? 'asc' : 'desc'), 'R', 'ConferenceRoom')
        }
        if (tableid === "tblRestaurantGroupRoom") {
            $rfp.getResourceItemDetail($(this).closest('th')[0].id + ($(this).hasClass('asc') == true ? 'asc' : 'desc'), 'R', 'GroupRoom')
        }
        if (tableid === "tblLocationConferenceRoom") {
            $rfp.getResourceItemDetail($(this).closest('th')[0].id + ($(this).hasClass('asc') == true ? 'asc' : 'desc'), 'L', 'ConferenceRoom')
        }
        if (tableid === "tblLocationGroupRoom") {
            $rfp.getResourceItemDetail($(this).closest('th')[0].id + ($(this).hasClass('asc') == true ? 'asc' : 'desc'), 'L', 'GroupRoom')
        }
    });

    $($rfp.controls.txtGroupRoomStartTimeInterval).timepicker({
        minuteStep: 1,
        template: 'dropdown',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: 'current',
        showInputs: false

    });

    $($rfp.controls.txtGroupRoomEndTimeInterval).timepicker({
        minuteStep: 1,
        template: 'dropdown',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: 'current',
        showInputs: false
    });

    $($rfp.controls.txtStartTimeInterval).timepicker({
        minuteStep: 1,
        template: 'dropdown',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: 'current',
        showInputs: false

    });

    $($rfp.controls.txtEndTimeInterval).timepicker({
        minuteStep: 1,
        template: 'dropdown',
        appendWidgetTo: 'body',
        showSeconds: false,
        showMeridian: false,
        defaultTime: 'current',
        showInputs: false

    });

    $($rfp.controls.txtGroupRoomStartTimeInterval).on("focus", function () {
        $(this).timepicker('showWidget');
        //$(".bootstrap-timepicker-widget").removeClass('dropdown-menu');
       // $(".bootstrap-timepicker-widget").css("{'backgroud':'#cccccc'}");
        
    });

    $($rfp.controls.txtGroupRoomEndTimeInterval).on("focus", function () {
        $(this).timepicker('showWidget');
    });

    $($rfp.controls.txtStartTimeInterval).on("focus", function () {
        $(this).timepicker('showWidget');
    });

    $($rfp.controls.txtEndTimeInterval).on("focus", function () {
        $(this).timepicker('showWidget');
    });

   

});

var $rfp = {
    constant: {
        NoSeatingImageName: "NoSeat",
        ParlamentryImageName: "Parlamentry",
        UShapeImageName: "UShape",
        RowsImageName: "Rows",
        CircleImageName: "Circle",
        TablesImageName: "Tables",
        BlockImageName: "Block",
        BanquetImageName: "Banquet",
        CabaretImageName:"Cabaret"
    },

    localization: {
        SelectEventType: "#hdnSelectEventType",
        SelectDuration: "#hdnSelectDuration",
        AddRoomLabel: "#hdnAddRoomLabel",
        SelectRoomType: "#hdnSelectRoomType",
        RFPSavedSuccessMessage: "#hdnRFPSavedSuccessMessage",
        RFPSubmitSuccessMessage: "#hdnRFPSubmitSuccessMessage",
        SelectSeatingType: "#hdnSelectSeatingType",
        SelectStartTime: "#hdnSelectStartTime",
        SelectEndTime: "#hdnSelectEndTime",
        hdnConferenceRoomButtonLabel: "#hdnConferenceRoomButtonLabel",
        hdnRoomButtonLabel: "#hdnRoomButtonLabel",
        btnAbortGroupRoom: "#btnAbortGroupRoom",
        hdnRFPSubmitUnSuccessMessage: "#hdnRFPSubmitUnSuccessMessage",
        hdnSelectHotel: '#hdnSelectHotel',
        hdnSelectRestaurant: '#hdnSelectRestaurant',
        hdnSelectLocation: '#hdnSelectLocation',
        hdnAddHotel: '#hdnAddHotel',
        hdnAddRestaurant: '#hdnAddRestaurant',
        hdnAddLocation: '#hdnAddLocation'
    },
    controls: {
        ddleventtype: "#ddleventtype",
        ddlduration: "#ddlduration",
        divHotelSection: "#divHotelSection",
        divRestaurantSection: "#divRestaurantSection",
        divLocationSection: "#divLocationSection",
        addroommodelcontainer: "#addroommodelcontainer",
        ddlRoomType: "#ddlRoomType",
        ddlResource: '#ddlResource',
        txtCheckedInDate: "#txtCheckedInDate",
        txtCheckedOutDate: "#txtCheckedOutDate",
        txtNoOfRooms: "#txtNoOfRooms",
        txtBudget: "#txtBudget",
        hdnFromType: "#hdnFromType",
        hdnModelType: "#hdnModelType",
        txtHotelComments: "#txtHotelComments",
        txtRestaurantComments: "#txtRestaurantComments",
        txtLocationComments: "#txtLocationComments",
        tbHotelAccomodation: "#tbHotelAccomodation",
        tbRestaurantAccomodation: "#tbRestaurantAccomodation",
        tbLocationAccomodation: "#tbLocationAccomodation",
        btnSubmitRFP: "#btnSubmitRFP",
        btnSaveRFP: "#btnSaveRFP",
        txtEventName: "#txtEventName",
        tbHotelConferenceRoom: "#tbHotelConferenceRoom",
        tbHotelGroupRoom: "#tbHotelGroupRoom",
        tbRestaurantConferenceRoom: "#tbRestaurantConferenceRoom",
        tbRestaurantGroupRoom: "#tbRestaurantGroupRoom",
        tbLocationConferenceRoom: "#tbLocationConferenceRoom",
        tbLocationGroupRoom: "#tbLocationGroupRoom",
        txtEventDate: "#txtEventDate",
        ddlStartTimeInterval: "#ddlStartTimeInterval",
        ddlEndTimeInterval: "#ddlEndTimeInterval",
        txtParticipants: "#txtParticipants",
        ddlSeatingType: "#ddlSeatingType",
        txtRoomSize: "#txtRoomSize",
        txtConferenceRoomBudget: "#txtConferenceRoomBudget",
        txtGroupRoomEventDate: "#txtGroupRoomEventDate",
        ddlGroupRoomStartTimeInterval: "#ddlGroupRoomStartTimeInterval",
        ddlGroupRoomEndTimeInterval: "#ddlGroupRoomEndTimeInterval",
        txtGroupRoomParticipants: "#txtGroupRoomParticipants",
        ddlGroupRoomSeatingType: "#ddlGroupRoomSeatingType",
        txtGroupRoomRoomSize: "#txtGroupRoomRoomSize",
        txtGroupRoomRoomBudget: "#txtGroupRoomRoomBudget",
        rfpconferenceroommodeltitle: "#rfpconferenceroommodeltitle",
        hdnMiceRequestId: "#hdnMiceRequestId",
        divRoomEquipment: '#divRoomEquipment',
        hdnMiceRequestId: "#hdnMiceRequestId",
        btnUpdateRoom: "#btnUpdateRoom",
        divRoomEquipment: '#divRoomEquipment',
        divConferenceFoods: '#divConferenceFoods',
        btnAddRoom: '#btnAddRoom',
        btnAddGroupRoom: '#btnAddGroupRoom',
        btnUpdateGroupRoom: '#btnUpdateGroupRoom',
        btnUpdateConferenceRoom: '#btnUpdateConferenceRoom',
        btnAddConferenceRoom: '#btnAddConferenceRoom',
        txtComments: '#txtComments',
        aRFPHotel: '#aRFPHotel',
        aRFPRestaurant: '#aRFPRestaurant',
        aRFPLocation: '#aRFPLocation',
        divRFPRestaurantSection: '#divRFPRestaurantSection',
        divRFPHotelSection: '#divRFPHotelSection',
        divRFPLocationSection: '#divRFPLocationSection',
        hdnIsViewMode: "#hdnIsViewMode",
        btnAbortRoom: "#btnAbortRoom",
        btnAbortConferenceRoom: "#btnAbortConferenceRoom",
        btnAbortGroupRoom: "#btnAbortGroupRoom",
        divRFPInfo: "#divRFPInfo",
        hdnEventNameValidation: "#hdnEventNameValidation",
        hdnDurationValidation: "#hdnDurationValidation",
        DivGeneralInfo: '#DivGeneralInfo',
        s2id_ddlRoomType: "#s2id_ddlRoomType",
        s2id_ddlSeatingType: "#s2id_ddlSeatingType",
        s2id_ddlGroupRoomSeatingType: "#s2id_ddlGroupRoomSeatingType",
        s2id_ddlGroupRoomStartTimeInterval: "#s2id_ddlGroupRoomStartTimeInterval",
        s2id_ddlGroupRoomEndTimeInterval: "#s2id_ddlGroupRoomEndTimeInterval",
        hdnHotelValidationMsg: "#hdnHotelValidationMsg",
        hdnRestaurantValidationMsg: "#hdnRestaurantValidationMsg",
        hdnLocationValidationMsg: "#hdnLocationValidationMsg",
        txtGroupRoomStartTimeInterval:"#txtGroupRoomStartTimeInterval",
        txtGroupRoomEndTimeInterval: "#txtGroupRoomEndTimeInterval",
        txtStartTimeInterval: "#txtStartTimeInterval",
        txtEndTimeInterval: "#txtEndTimeInterval",
        hdnCheckinDateValidationMsg: "#hdnCheckinDateValidationMsg",
        hdnCheckOutDateValidationMsg:"#hdnCheckOutDateValidationMsg",
        hdnEventDayValidationMsg: "#hdnEventDayValidationMsg",
        hdnGroupEventDayValidationMsg: "#hdnGroupEventDayValidationMsg",
        txtEventStartDate: "#txtEventStartDate",
        txtEventEndDate: "#txtEventEndDate",
        hdnDuration: "#hdnDuration",
        hdnGroupRoomParticipantMessage: "#hdnGroupRoomParticipantMessage",
        hdnConfParticipantMessage: "#hdnConfParticipantMessage",
        hdnSelectedResourceType: '#hdnSelectedResourceType'
    },
    variables: {
        RfpId: "#hdnRfpId",
        HotelRequestId: "#hdnHotelRequestId",
        RestaurantRequestId: "#hdnRestaurantRequestId",
        LocationRequestId: "#hdnLocationRequestId",
        AccomodationType: "Accommodation",
        ConferenceRoomType: "ConferenceRoom",
        GroupRoomType: "GroupRoom",
        IsRFPSubmitted: false,
        euroIcon: "<span class='glyphicon glyphicon-euro euroSign-space'></span>"
    },
    apiPath: {
        GetEventTypeList: 'LookUp/RequestTypes',
        GetResourceList: 'LookUp/GetResourceList',
        GetDaysRunningList: 'LookUp/DaysRunnings',
        StartRFP: 'Request/MakeRequestForProposal',
        GetRequestForProposalDetail: 'Request/RequestForProposal',
        GetResourcesDetails: 'Search/GetRFPResourceList',
        GetRoomTypes: 'Lookup/RoomTypes',
        AddRequestItem: 'Request/AddEditRFPRequestItem',
        GetRequestItem: 'Request/RFPRequestItems',
        DeleteRFPRequestItem: 'Request/DeleteRFPRequestItem',
        SubmitRFP: 'Request/SubmitRFP',
        GetSeatingTypes: 'Lookup/SeatingTypes',
        GetTimeIntervals: 'Lookup/TimeIntervals',
        GetConferenceFoods: 'Request/ConferenceFoods',
        GetRoomTechniques: 'LookUp/RoomTechniques',
        RemoveRequestItem: 'Request/DeleteRFPRequestItem',
        GetResourceItemDetail: 'Request/GetResourceItemDetail',
        GetRequestForProposalById: "Request/GetRequestForProposalById"
    },
    validaterights: function () {
        var isVisible = false;
        if (ajaxRequest.CheckRights("H")) {
            $($rfp.controls.aRFPHotel).show();
            if (isVisible == false)
                $($rfp.controls.aRFPHotel).click();
            isVisible = true;
        }
        if (ajaxRequest.CheckRights("R")) {
            $($rfp.controls.aRFPRestaurant).show();
            if (isVisible == false)
                $($rfp.controls.aRFPRestaurant).click();
            isVisible = true;

        }
        if (ajaxRequest.CheckRights("L")) {

            $($rfp.controls.aRFPLocation).show();
            if (isVisible == false)
                $($rfp.controls.aRFPLocation).click();
            isVisible = true;
        }
    },
    startRFP: function () {
        var url = $bind.URL($rfp.apiPath.StartRFP);
        common.displayLoader();
        ajaxRequest.httpPost(url, null, $rfp.startRFPOnSuccess, $rfp.startRFPOnError);
    },
    startRFPOnSuccess: function (data) {
        
        var rfpId = data.data;
        if (rfpId !== undefined && rfpId !== null && rfpId > 0) {
           var url = $bind.URL($rfp.apiPath.GetRequestForProposalDetail);
            ajaxRequest.httpGetSyncWithData(url, null, $rfp.getRequestForProposalDetailOnSuccess, $rfp.getRequestForProposalDetailOnError);
            common.hideLoader();
        } else {
            $($rfp.controls.divRFPInfo).hide();
            $($rfp.controls.btnSaveRFP).hide();
            $($rfp.controls.btnSubmitRFP).hide();
            $($rfp.controls.DivGeneralInfo).html("");
            $($rfp.controls.DivGeneralInfo).html($($c.controls.sNoRecords).val());
            common.hideLoader();
        }
    },
    startRFPOnError: function (jqXHR, textStatus, errorThrown) {
        $managecode.Decision(false, "", jqXHR.status, textStatus + ": " + errorThrown, "error", false);
        common.hideLoader();
    },
    getRequestForProposalDetailOnSuccess: function (data) {
        
        
        var rfpDetail = data.data;
        if (rfpDetail.event_name !== undefined && rfpDetail.event_name !== null) {
            $($rfp.controls.txtEventName).val(rfpDetail.event_name);
        }
        $($rfp.controls.hdnIsViewMode).val(rfpDetail.isviewmode);
        $($rfp.variables.RfpId).val(rfpDetail.id);
       

       

        if (rfpDetail.micerequests !== null && rfpDetail.micerequests !== undefined) {
            $.each(rfpDetail.micerequests, function (index, value) {
                var resType = '';
                if (value.resource_type === 0) {
                    $($rfp.variables.HotelRequestId).val(value.id);
                    $rfp.getResourceDetail("H", value.rfpresources);
                    resType = "H";
                    // Set Comments
                    if (value.comments !== undefined && value.comments !== null) {
                        $($rfp.controls.txtHotelComments).val(value.comments);
                    }
                } else if (value.resource_type === 1) {
                    $($rfp.variables.RestaurantRequestId).val(value.id);
                    $rfp.getResourceDetail("R", value.rfpresources);
                    resType = "R";
                    // Set Comments
                    if (value.comments !== undefined && value.comments !== null) {
                        $($rfp.controls.txtRestaurantComments).val(value.comments);
                    }
                } else if (value.resource_type === 2) {
                    $($rfp.variables.LocationRequestId).val(value.id);
                    $rfp.getResourceDetail("L", value.rfpresources);
                    resType = "L";
                    // Set Comments
                    if (value.comments !== undefined && value.comments !== null) {
                        $($rfp.controls.txtLocationComments).val(value.comments);
                    }
                }
                $rfp.fillRFPMiceRequestItems(resType, value.rfpmicerequestitems);
            });
        }


        var isHotelExist = false;
        var isRestExist = false;
        var isLocationExist = false;
        $.each(rfpDetail.micerequests, function (index, value) {
            if (value.resource_type === 0 && value.rfpresources != null) {
                isHotelExist = true;
            }
            else if (value.resource_type === 1 && value.rfpresources != null) {
                isRestExist = true;
            }
            else if (value.resource_type === 2 && value.rfpresources != null) {
                isLocationExist = true;
            }
        });

        if (isHotelExist == false) {
            $($rfp.controls.aRFPHotel).hide();
        }
        if (isRestExist == false) {
            $($rfp.controls.aRFPRestaurant).hide();
        }
        if (isLocationExist == false) {
            $($rfp.controls.aRFPLocation).hide();
        }

        if (isHotelExist == true) {
            $($rfp.controls.aRFPHotel).addClass("active");
            $($rfp.controls.divRFPHotelSection).addClass("active");
        }
        else if (isRestExist == true) {
            $($rfp.controls.aRFPRestaurant).addClass("active");
            $($rfp.controls.divRFPHotelSection).removeClass("active");
            $($rfp.controls.divRFPRestaurantSection).addClass("active");
        }
        else if (isLocationExist == true) {
            $($rfp.controls.aRFPLocation).addClass("active");
            $($rfp.controls.divRFPHotelSection).removeClass("active");
            $($rfp.controls.divRFPLocationSection).addClass("active");
        }

        if (window.localStorage.getItem("activeLink") !== null && window.localStorage.getItem("activeLink") !== undefined) {
            var activeLink = window.localStorage.getItem("activeLink");
            if (isHotelExist == true && activeLink == "Hotels") {
                $($rfp.controls.aRFPHotel).addClass("active");
                $($rfp.controls.divRFPHotelSection).addClass("active");
            }
            else if (isRestExist == true && activeLink == "Restaurants") {
                $($rfp.controls.aRFPRestaurant).addClass("active");
                $($rfp.controls.aRFPHotel).removeClass("active");
                $($rfp.controls.divRFPHotelSection).removeClass("active");
                $($rfp.controls.divRFPRestaurantSection).addClass("active");
            }
            else if (isLocationExist == true && activeLink == "Location") {
                $($rfp.controls.aRFPLocation).addClass("active");
                $($rfp.controls.aRFPHotel).removeClass("active");
                $($rfp.controls.divRFPHotelSection).removeClass("active");
                $($rfp.controls.divRFPLocationSection).addClass("active");
            }
        }

        if (isHotelExist == false && isRestExist == false && isLocationExist == false) {
            $($rfp.controls.divRFPInfo).hide();
            $($rfp.controls.btnSaveRFP).hide();
            $($rfp.controls.btnSubmitRFP).hide();
            $($rfp.controls.DivGeneralInfo).html("");
            $($rfp.controls.DivGeneralInfo).html($($c.controls.sNoRecords).val());
            // $($rfp.controls.DivGeneralInfo).hide();
        }

        $LookUp.BindDropdown($rfp.controls.ddleventtype, $($rfp.localization.SelectEventType).val(), $rfp.apiPath.GetEventTypeList, 0, 50, false, true);
       // $LookUp.BindDropdown($rfp.controls.ddlduration, $($rfp.localization.SelectDuration).val(), $rfp.apiPath.GetDaysRunningList, 0, 50, false, true);
        $LookUp.BindDropdown($rfp.controls.ddlRoomType, $($rfp.localization.SelectRoomType).val(), $rfp.apiPath.GetRoomTypes, 0, 50, false, true);
        $LookUp.BindDropdownWithImage($rfp.controls.ddlSeatingType, $($rfp.localization.SelectSeatingType).val(), $rfp.apiPath.GetSeatingTypes, 0, 50, false, true);
        $LookUp.BindDropdownWithImage($rfp.controls.ddlGroupRoomSeatingType, $($rfp.localization.SelectSeatingType).val(), $rfp.apiPath.GetSeatingTypes, 0, 50, false, true);
       // $LookUp.BindDropdown($rfp.controls.ddlStartTimeInterval, $($rfp.localization.SelectStartTime).val(), $rfp.apiPath.GetTimeIntervals + "?intervals=30", 0, 50, false, true);
       // $LookUp.BindDropdown($rfp.controls.ddlEndTimeInterval, $($rfp.localization.SelectEndTime).val(), $rfp.apiPath.GetTimeIntervals + "?intervals=30", 0, 50, false, true);
       // $LookUp.BindDropdown($rfp.controls.ddlGroupRoomStartTimeInterval, $($rfp.localization.SelectStartTime).val(), $rfp.apiPath.GetTimeIntervals + "?intervals=30", 0, 50, false, true);
       // $LookUp.BindDropdown($rfp.controls.ddlGroupRoomEndTimeInterval, $($rfp.localization.SelectEndTime).val(), $rfp.apiPath.GetTimeIntervals + "?intervals=30", 0, 50, false, true);

        //if (rfpDetail.days_running != null) {
        //    $($rfp.controls.ddlduration).select2("data", {
        //    "id": rfpDetail.days_running,
        //    "text": rfpDetail.dayrunningname
        //});
        //}

       
        $($rfp.controls.hdnDuration).val(rfpDetail.days_running);

        common.bindDatePickerStartWithCurrentDate($rfp.controls.txtCheckedInDate);
        common.bindDatePickerStartWithCurrentDate($rfp.controls.txtCheckedOutDate);
        common.bindDatePickerStartWithCurrentDate($rfp.controls.txtEventDate);
        common.bindDatePickerStartWithCurrentDate($rfp.controls.txtGroupRoomEventDate);
       // common.bindDatePickerStartWithCurrentDate($rfp.controls.txtEventStartDate);
       // common.bindDatePickerStartWithCurrentDate($rfp.controls.txtEventEndDate);


        $($rfp.controls.txtEventStartDate).datepicker({
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy',
            changeMonth: true,
            changeYear: true,
            minDate: new Date(),
          //  maxDate: $($rfp.controls.txtEventEndDate).val() != '' ? $($rfp.controls.txtEventEndDate).val():-1
            onSelect: function () {
                var minDate = $($rfp.controls.txtEventStartDate).datepicker('getDate');
                $($rfp.controls.txtEventEndDate).datepicker('change', { minDate: minDate });

                var maxDate = $($rfp.controls.txtEventEndDate).datepicker('getDate');
                
                if (maxDate != null) {
                    var diff = maxDate - minDate;
                    var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
                    $rfp.setDuration(diffDays);
                }
            }
        });
        

        $($rfp.controls.txtEventEndDate).datepicker({
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy',
            changeMonth: true,
            changeYear: true,
            minDate: $($rfp.controls.txtEventStartDate).val() != '' ? $($rfp.controls.txtEventStartDate).val() : new Date(),
            onSelect: function () {
                var maxDate = $($rfp.controls.txtEventEndDate).datepicker('getDate');
                $($rfp.controls.txtEventStartDate).datepicker("change", { maxDate: maxDate });

                var minDate = $($rfp.controls.txtEventStartDate).datepicker('getDate');
                if (minDate != null) {
                    var diff = maxDate - minDate;
                    var diffDays = Math.ceil(diff / (1000 * 3600 * 24));                    
                    $rfp.setDuration(diffDays);
                }
            }
        });
       

        
        if (rfpDetail.strEventStartDate != null) {             
            $($rfp.controls.txtEventStartDate).datepicker("setDate", common.getDateFromPicker(rfpDetail.strEventStartDate));
        }
        else {
            $($rfp.controls.txtEventStartDate).datepicker('setDate', common.getDateFromPicker($($rfp.controls.txtEventStartDate).val()));
        }

        if (rfpDetail.strEventEndDate != null) {
            $($rfp.controls.txtEventEndDate).datepicker("setDate", common.getDateFromPicker(rfpDetail.strEventEndDate));
        }
        else {
            $($rfp.controls.txtEventEndDate).datepicker('setDate', common.getDateFromPicker($($rfp.controls.txtEventEndDate).val()));
        }

        common.hideLoader();
        ///INSPIRE-102
        //If RFP is submitted, which is display as view mode
        if (rfpDetail.is_submitted == true) {
            $($rfp.controls.btnSaveRFP).hide();
            $($rfp.controls.btnSubmitRFP).hide();
            $("#btnHotelAddRoom").hide();
            $("#btnHotelAddConferenceRoom").hide();
            $("#btnHotelAddGroupRoom").hide();
            $("#btnRestaurantAddConferenceRoom").hide();
            $("#btnRestaurantAddGroupRoom").hide();
            $("#btnLocationAddConferenceRoom").hide();
            $("#btnLocationAddGroupRoom").hide();
            $('.add-more-to-cart').hide();
            $(".lnkEdit").hide();
            $(".lnkDelete").hide();
            $('.deleteresource').hide();
        }
        else {
            $(".lnkView").hide();
        }

        if ($($rfp.variables.RfpId).val() != '' && $($rfp.variables.RfpId).val() == 0) {
            $(".add-more-to-cart").hide();
        }
    },
    ///INSPIRE-102
    getRequestForProposalDetailNewOnSuccess: function (data) {


        var rfpDetail = data.data;
        if (rfpDetail.event_name !== undefined && rfpDetail.event_name !== null) {
            $($rfp.controls.txtEventName).val(rfpDetail.event_name);
        }
        $($rfp.controls.hdnIsViewMode).val(rfpDetail.isviewmode);
        $($rfp.variables.RfpId).val(rfpDetail.id);


            

        if (rfpDetail.micerequests !== null && rfpDetail.micerequests !== undefined) {
            $.each(rfpDetail.micerequests, function (index, value) {
                var resType = '';
                if (value.resource_type === 0) {
                    $($rfp.variables.HotelRequestId).val(value.id);
                    $rfp.getResourceDetail("H", value.rfpresources);
                    resType = "H";
                    // Set Comments
                    if (value.comments !== undefined && value.comments !== null) {
                        $($rfp.controls.txtHotelComments).val(value.comments);
                    }
                } else if (value.resource_type === 1) {
                    $($rfp.variables.RestaurantRequestId).val(value.id);
                    $rfp.getResourceDetail("R", value.rfpresources);
                    resType = "R";
                    // Set Comments
                    if (value.comments !== undefined && value.comments !== null) {
                        $($rfp.controls.txtRestaurantComments).val(value.comments);
                    }
                } else if (value.resource_type === 2) {
                    $($rfp.variables.LocationRequestId).val(value.id);
                    $rfp.getResourceDetail("L", value.rfpresources);
                    resType = "L";
                    // Set Comments
                    if (value.comments !== undefined && value.comments !== null) {
                        $($rfp.controls.txtLocationComments).val(value.comments);
                    }
                }
               // $rfp.fillRFPMiceRequestItems(resType, value.rfpmicerequestitems);
            });
        }


        var isHotelExist = false;
        var isRestExist = false;
        var isLocationExist = false;
        $.each(rfpDetail.micerequests, function (index, value) {
            if (value.resource_type === 0 && value.rfpresources != null) {
                isHotelExist = true;
            }
            else if (value.resource_type === 1 && value.rfpresources != null) {
                isRestExist = true;
            }
            else if (value.resource_type === 2 && value.rfpresources != null) {
                isLocationExist = true;
            }
        });

        if (isHotelExist == false) {
            $($rfp.controls.aRFPHotel).hide();
        }
        if (isRestExist == false) {
            $($rfp.controls.aRFPRestaurant).hide();
        }
        if (isLocationExist == false) {
            $($rfp.controls.aRFPLocation).hide();
        }

        if (isHotelExist == true) {
            $($rfp.controls.aRFPHotel).addClass("active");
            $($rfp.controls.divRFPHotelSection).addClass("active");
        }
        else if (isRestExist == true) {
            $($rfp.controls.aRFPRestaurant).addClass("active");
            $($rfp.controls.divRFPHotelSection).removeClass("active");
            $($rfp.controls.divRFPRestaurantSection).addClass("active");
        }
        else if (isLocationExist == true) {
            $($rfp.controls.aRFPLocation).addClass("active");
            $($rfp.controls.divRFPHotelSection).removeClass("active");
            $($rfp.controls.divRFPLocationSection).addClass("active");
        }

        if (window.localStorage.getItem("activeLink") !== null && window.localStorage.getItem("activeLink") !== undefined) {
            var activeLink = window.localStorage.getItem("activeLink");
            if (isHotelExist == true && activeLink == "Hotels") {
                $($rfp.controls.aRFPHotel).addClass("active");
                $($rfp.controls.divRFPHotelSection).addClass("active");
            }
            else if (isRestExist == true && activeLink == "Restaurants") {
                $($rfp.controls.aRFPRestaurant).addClass("active");
                $($rfp.controls.aRFPHotel).removeClass("active");
                $($rfp.controls.divRFPHotelSection).removeClass("active");
                $($rfp.controls.divRFPRestaurantSection).addClass("active");
            }
            else if (isLocationExist == true && activeLink == "Location") {
                $($rfp.controls.aRFPLocation).addClass("active");
                $($rfp.controls.aRFPHotel).removeClass("active");
                $($rfp.controls.divRFPHotelSection).removeClass("active");
                $($rfp.controls.divRFPLocationSection).addClass("active");
            }
        }

        if (isHotelExist == false && isRestExist == false && isLocationExist == false) {
            $($rfp.controls.divRFPInfo).hide();
            $($rfp.controls.btnSaveRFP).hide();
            $($rfp.controls.btnSubmitRFP).hide();
            $($rfp.controls.DivGeneralInfo).html("");
            $($rfp.controls.DivGeneralInfo).html($($c.controls.sNoRecords).val());
            // $($rfp.controls.DivGeneralInfo).hide();
        }

        $LookUp.BindDropdown($rfp.controls.ddleventtype, $($rfp.localization.SelectEventType).val(), $rfp.apiPath.GetEventTypeList, 0, 50, false, true);
        // $LookUp.BindDropdown($rfp.controls.ddlduration, $($rfp.localization.SelectDuration).val(), $rfp.apiPath.GetDaysRunningList, 0, 50, false, true);
        $LookUp.BindDropdown($rfp.controls.ddlRoomType, $($rfp.localization.SelectRoomType).val(), $rfp.apiPath.GetRoomTypes, 0, 50, false, true);
        $LookUp.BindDropdownWithImage($rfp.controls.ddlSeatingType, $($rfp.localization.SelectSeatingType).val(), $rfp.apiPath.GetSeatingTypes, 0, 50, false, true);
        $LookUp.BindDropdownWithImage($rfp.controls.ddlGroupRoomSeatingType, $($rfp.localization.SelectSeatingType).val(), $rfp.apiPath.GetSeatingTypes, 0, 50, false, true);
        // $LookUp.BindDropdown($rfp.controls.ddlStartTimeInterval, $($rfp.localization.SelectStartTime).val(), $rfp.apiPath.GetTimeIntervals + "?intervals=30", 0, 50, false, true);
        // $LookUp.BindDropdown($rfp.controls.ddlEndTimeInterval, $($rfp.localization.SelectEndTime).val(), $rfp.apiPath.GetTimeIntervals + "?intervals=30", 0, 50, false, true);
        // $LookUp.BindDropdown($rfp.controls.ddlGroupRoomStartTimeInterval, $($rfp.localization.SelectStartTime).val(), $rfp.apiPath.GetTimeIntervals + "?intervals=30", 0, 50, false, true);
        // $LookUp.BindDropdown($rfp.controls.ddlGroupRoomEndTimeInterval, $($rfp.localization.SelectEndTime).val(), $rfp.apiPath.GetTimeIntervals + "?intervals=30", 0, 50, false, true);

        //if (rfpDetail.days_running != null) {
        //    $($rfp.controls.ddlduration).select2("data", {
        //    "id": rfpDetail.days_running,
        //    "text": rfpDetail.dayrunningname
        //});
        //}


        $($rfp.controls.hdnDuration).val(rfpDetail.days_running);

        common.bindDatePickerStartWithCurrentDate($rfp.controls.txtCheckedInDate);
        common.bindDatePickerStartWithCurrentDate($rfp.controls.txtCheckedOutDate);
        common.bindDatePickerStartWithCurrentDate($rfp.controls.txtEventDate);
        common.bindDatePickerStartWithCurrentDate($rfp.controls.txtGroupRoomEventDate);
        // common.bindDatePickerStartWithCurrentDate($rfp.controls.txtEventStartDate);
        // common.bindDatePickerStartWithCurrentDate($rfp.controls.txtEventEndDate);


        $($rfp.controls.txtEventStartDate).datepicker({
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy',
            changeMonth: true,
            changeYear: true,
            minDate: new Date(),
            //  maxDate: $($rfp.controls.txtEventEndDate).val() != '' ? $($rfp.controls.txtEventEndDate).val():-1
            onSelect: function () {
                var minDate = $($rfp.controls.txtEventStartDate).datepicker('getDate');
                $($rfp.controls.txtEventEndDate).datepicker('change', { minDate: minDate });

                var maxDate = $($rfp.controls.txtEventEndDate).datepicker('getDate');

                if (maxDate != null) {
                    var diff = maxDate - minDate;
                    var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
                    $rfp.setDuration(diffDays);
                }
            }
        });


        $($rfp.controls.txtEventEndDate).datepicker({
            showButtonPanel: true,
            dateFormat: 'dd.mm.yy',
            changeMonth: true,
            changeYear: true,
            minDate: $($rfp.controls.txtEventStartDate).val() != '' ? $($rfp.controls.txtEventStartDate).val() : new Date(),
            onSelect: function () {
                var maxDate = $($rfp.controls.txtEventEndDate).datepicker('getDate');
                $($rfp.controls.txtEventStartDate).datepicker("change", { maxDate: maxDate });

                var minDate = $($rfp.controls.txtEventStartDate).datepicker('getDate');
                if (minDate != null) {
                    var diff = maxDate - minDate;
                    var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
                    $rfp.setDuration(diffDays);
                }
            }
        });



        if (rfpDetail.strEventStartDate != null) {
            $($rfp.controls.txtEventStartDate).datepicker("setDate", common.getDateFromPicker(rfpDetail.strEventStartDate));
        }
        else {
            $($rfp.controls.txtEventStartDate).datepicker('setDate', common.getDateFromPicker($($rfp.controls.txtEventStartDate).val()));
        }

        if (rfpDetail.strEventEndDate != null) {
            $($rfp.controls.txtEventEndDate).datepicker("setDate", common.getDateFromPicker(rfpDetail.strEventEndDate));
        }
        else {
            $($rfp.controls.txtEventEndDate).datepicker('setDate', common.getDateFromPicker($($rfp.controls.txtEventEndDate).val()));
        }

        common.hideLoader();
        ///INSPIRE-102
        //If RFP is submitted, which is display as view mode
        if (rfpDetail.is_submitted == true) {
            $($rfp.controls.btnSaveRFP).hide();
            $($rfp.controls.btnSubmitRFP).hide();
            $("#btnHotelAddRoom").hide();
            $("#btnHotelAddConferenceRoom").hide();
            $("#btnHotelAddGroupRoom").hide();
            $("#btnRestaurantAddConferenceRoom").hide();
            $("#btnRestaurantAddGroupRoom").hide();
            $("#btnLocationAddConferenceRoom").hide();
            $("#btnLocationAddGroupRoom").hide();
            $(".lnkEdit").hide();
            $(".lnkDelete").hide();
            $(".add-more-to-cart").hide();
            $('.deleteresource').hide();
        }
        else {
            $(".lnkView").hide();
        }

        if ($($rfp.variables.RfpId).val() != '' && $($rfp.variables.RfpId).val() == 0) {
            $(".add-more-to-cart").hide();
        }
    },
    setDuration: function (days) {
        var durationId = '';
        if (days >= 0 && days <= 5) {
            durationId = 1;
        }
        else if (days >= 6 && days <= 10) {
            durationId = 4;
        }
        else if (days >= 11 && days <= 30) {
            durationId = 10;
        }
        else if (days >= 31 && days <= 60) {
            durationId = 14;
        }
        else if (days >= 61 && days <= 90) {
            durationId = 20;
        }
        else if (days >= 91) {
            durationId = 30;
        }
        if (durationId != '') {
            $($rfp.controls.hdnDuration).val(durationId);
        }
    },
    getRequestForProposalDetailOnError: function (data) {
        common.hideLoader();
    },
    getResourceDetail: function (resourceType, resources) {
        var resourceIds = "";
        if (resources !== null) {
            $.each(resources, function (index, value) {
                if (resourceIds === "") {
                    resourceIds = value.resourceid;
                } else {
                    resourceIds = resourceIds + "," + value.resourceid;
                }
            });
        }
        if (resourceIds !== "") {
            var url = $bind.URL($rfp.apiPath.GetResourcesDetails);
            var requestData = {
                Rids: resourceIds,
                type: resourceType
            };
            ajaxRequest.httpGetSyncWithData(url, requestData, $rfp.getResourceDetailOnSuccess, $rfp.getResourceDetailOnError);
        }
    },
    getResourceDetailOnSuccess: function (data) {
        var resources = data.data.ResourceModel;
        var resourcesIds = [];
        if (resources !== undefined && resources !== null) {
            $.each(resources, function (index, value) {
               
                resourcesIds.push(value.id);
                var resourceContent = $search.drawpanel(value, value.type, index, "T", false, true, true);
                if (value.type === "H") {
                    $($rfp.controls.divHotelSection).append(resourceContent);
                } else if (value.type === "R") {
                    $($rfp.controls.divRestaurantSection).append(resourceContent);
                } else if (value.type === "L") {
                    $($rfp.controls.divLocationSection).append(resourceContent);
                }
                var id = $rfp.GetURLParameter();    
                ///INSPIRE-102
                //If RFP is display view mode, delete icon hide from resource
                if (isNaN(id) == false && $($rfp.controls.hdnIsViewMode).val()=="true") {
                    $(".deleteResource").hide();
                }
                setTimeout(function () { $search.loadimages(value.id, value.type); }, 200);
            });
        }
        $('#hdnSavedResourceIds').val(JSON.stringify(resourcesIds));
    },
    getResourceDetailOnError: function (data) {
        common.hideLoader();
    },
    addToCart: function (modelId, type) {
        var modalTitle = '';
        var controlTitle = '';
        if (type === 'H') {
            modalTitle = $($rfp.localization.hdnAddHotel).val(); 
            controlTitle = $($rfp.localization.hdnSelectHotel).val();
            $LookUp.BindDropdown($rfp.controls.ddlResource, $($rfp.localization.hdnSelectHotel).val(), $rfp.apiPath.GetResourceList, 0, 50, false, true);
        } else if (type === 'R') {
            modalTitle = $($rfp.localization.hdnAddRestaurant).val(); 
            controlTitle = $($rfp.localization.hdnSelectRestaurant).val();
            $LookUp.BindDropdown($rfp.controls.ddlResource, $($rfp.localization.hdnSelectRestaurant).val(), $rfp.apiPath.GetResourceList, 1, 50, false, true);
        } else if (type === 'L') {
            modalTitle = $($rfp.localization.hdnAddLocation).val(); 
            controlTitle = $($rfp.localization.hdnSelectLocation).val();
            $LookUp.BindDropdown($rfp.controls.ddlResource, $($rfp.localization.hdnSelectLocation).val(), $rfp.apiPath.GetResourceList, 2, 50, false, true);
        }
        $($rfp.controls.hdnSelectedResourceType).val(type);
        $(modelId + ' .rfpModalTitle').text(modalTitle);
        $(modelId + ' .labelheader span').text(controlTitle);
        //$('.rfpSearchResources').html('');
        $('.rfpAddResources').html('');
        $('#hdnNewAddedResourceIds').val('');
        $(modelId).modal("show");
        $('#validationmsg').hide();

        $($rfp.controls.ddlResource).off('change').on('change', function () {
            $('#validationmsg').hide();
            common.displayLoader();
            if ($($rfp.controls.ddlResource).val() != null && $($rfp.controls.ddlResource).val() != '') {
                var resourceid = $($rfp.controls.ddlResource).val();

                var resourceIds = $('#hdnNewAddedResourceIds').val();
                var resourceArray = [];
                if (resourceIds != null && resourceIds != '') {
                    resourceArray = JSON.parse(resourceIds);
                }
                var savedresourceArray = [];
                $('#divRFPInfo .card-5').each(function() {
                    savedresourceArray.push($(this).attr('data-id'));
                });

                //var savedresourceIds = $('#hdnSavedResourceIds').val();
                //var savedresourceArray = [];
                //if (savedresourceIds != null && savedresourceIds != '') {
                //    savedresourceArray = JSON.parse(savedresourceIds);
                //}
                
                if (resourceArray.indexOf(resourceid) < 0 && savedresourceArray.indexOf(resourceid) < 0) {
                //if (resourceArray.indexOf(parseInt(resourceid)) < 0) {
                    var responseHtml = $search.GetRInfo($($rfp.controls.ddlResource).val(), type, null);
                    var title = $('#hdnRemove').val();
                    //responseHtml = "<div class='pos-relative' id='" + 'rfp_resource_' + resourceid + "'><a href='javascript:void(0);' title='" + title +"' class='rfpAdd' onclick='$rfp.AddResourceToRFPCart(event ," + resourceid + ")'><i class='glyphicon glyphicon-plus-sign deleteResource'></i></a>" + responseHtml + "</div>";
                    responseHtml = "<div class='pos-relative col-md-6' id='" + 'rfp_resource_' + resourceid + "'><a href='javascript:void(0);' title='" + title + "' class='rfpRemove' onclick='$rfp.RemoveResourceToRFPCart(event ," + resourceid + ")'><i class='glyphicon glyphicon-trash deleteResource'></i></a>" + responseHtml + "</div>";

                    //$('.rfpSearchResources').html(responseHtml);
                    //setTimeout(function () { $search.loadimages(resourceid, type); }, 100);

                    $('.rfpAddResources').append(responseHtml);
                    setTimeout(function () { $search.loadimages(resourceid, $($rfp.controls.hdnSelectedResourceType).val()); }, 100);
                    $rfp.addRemoveResourceIdFromBucket(resourceid, true);
                } else {
                    $('#validationmsg').show();
                }
            } else {
                //$('.rfpSearchResources').html('');
            }
            setTimeout(function () {
                common.hideLoader();
            }, 1000);
        });

    },
    //AddResourceToRFPCart: function (event, addedResourceId) {
    //    event.preventDefault();
    //    event.stopPropagation();
    //    if (addedResourceId != null && addedResourceId != '') {
    //        var resourceid = addedResourceId;
    //        $('.rfpSearchResources').html('');

    //        var resourceIds = $('#hdnNewAddedResourceIds').val();
    //        var resourceArray = [];
    //        if (resourceIds != null && resourceIds != '') {
    //            resourceArray = JSON.parse(resourceIds);
    //        }

    //        if (resourceArray.indexOf(resourceid.toString()) < 0) {
    //            var responseHtml = $search.GetRInfo(resourceid, $($rfp.controls.hdnSelectedResourceType).val(), null);
    //            var title = $('#hdnRemove').val();
    //            responseHtml = "<div class='pos-relative' id='" + 'rfp_resource_' + resourceid + "'><a href='javascript:void(0);' title='" + title +"' class='rfpRemove' onclick='$rfp.RemoveResourceToRFPCart(event ," + resourceid +")'><i class='glyphicon glyphicon-trash deleteResource'></i></a>" + responseHtml + "</div>";
    //            $('.rfpAddResources').append(responseHtml);
    //            setTimeout(function () { $search.loadimages(resourceid, $($rfp.controls.hdnSelectedResourceType).val()); }, 100);
    //            $rfp.addRemoveResourceIdFromBucket(resourceid, true);
    //        }


    //        $($rfp.controls.ddlResource).select2('val', '');
    //    }
    //},
    RemoveResourceToRFPCart: function (event, resourceId) {
        event.preventDefault();
        event.stopPropagation();
        $rfp.addRemoveResourceIdFromBucket(resourceId, false);
        var myObj = $('#rfp_resource_' + resourceId);
        myObj.remove();
    },
    addRemoveResourceIdFromBucket: function (resourceId, isAdd) {
        var resourceIds = $('#hdnNewAddedResourceIds').val();
        var resourceArray = [];
        if (resourceIds != null && resourceIds != '') {
            resourceArray = JSON.parse(resourceIds);
        }

        if (isAdd) {
            resourceArray.push(resourceId);
        } else {
            if (resourceArray.indexOf(resourceId.toString()) > -1) {
                resourceArray.splice(resourceArray.indexOf(resourceId), 1);
            }
        }
        $('#hdnNewAddedResourceIds').val(JSON.stringify(resourceArray));
    },
    saveRFPCart: function (modelId) {
        var resourceType = $($rfp.controls.hdnSelectedResourceType).val();
        var resourceIds = $('#hdnNewAddedResourceIds').val();
        var resourceArray = [];
        if (resourceIds != null && resourceIds != '') {
            resourceArray = JSON.parse(resourceIds);
            if (resourceArray.length > 0) {
                $bind.addResourceToRFP(resourceIds, resourceType, modelId);
            }
        } else {
            
        }
    },
    openmodel: function (modelId, fromType, isEdit, tableid) {
        $($rfp.controls.hdnFromType).val(fromType);
        $($rfp.controls.hdnModelType).val(modelId);
        $('#hdnTableId').val(tableid);
        $('.for-hotel').css('display', 'none');

        if (modelId === "#addconferenceroommodelcontainer") {
            $($rfp.controls.rfpconferenceroommodeltitle).val($($rfp.localization.hdnConferenceRoomButtonLabel).val());
            $rfp.getConferenceFoods();
            if (fromType !== "R") {
                $('.for-hotel').css('display', '');                
                $rfp.getRoomTechniques();
            }
        } else {
            $($rfp.controls.rfpconferenceroommodeltitle).val($($rfp.localization.hdnRoomButtonLabel).val());
        }
        if (isEdit) {
            if (modelId == "#addroommodelcontainer") {
                $($rfp.controls.btnUpdateRoom).css("display", "block");
                $($rfp.controls.btnAddRoom).css("display", "none");

            }
            if (modelId == "#addconferenceroommodelcontainer") {
                $($rfp.controls.btnUpdateConferenceRoom).css("display", "block");
                $($rfp.controls.btnAddConferenceRoom).css("display", "none");
            }
            if (modelId == "#addgrouproommodelcontainer") {
                $($rfp.controls.btnUpdateGroupRoom).css("display", "block");
                $($rfp.controls.btnAddGroupRoom).css("display", "none");
            }
        }
        else {
            ///INSPIRE-102
            ///If service is enter, before enter event name then validate RFP
            if ($($rfp.variables.RfpId).val() != '' && $($rfp.variables.RfpId).val() == 0) {
                if ($rfp.validateRFP() == false) {
                    return false;
                }               
            }

            if (modelId == "#addroommodelcontainer") {
                $($rfp.controls.btnUpdateRoom).css("display", "none");
                $($rfp.controls.btnAddRoom).css("display", "block");
                $($rfp.controls.txtCheckedInDate).datepicker("setDate", common.getDateFromPicker($($rfp.controls.txtEventStartDate).val()));
                $($rfp.controls.txtCheckedOutDate).datepicker("setDate", common.getDateFromPicker($($rfp.controls.txtEventEndDate).val()));
            }
            if (modelId == "#addconferenceroommodelcontainer") {
                $($rfp.controls.btnUpdateConferenceRoom).css("display", "none");
                $($rfp.controls.btnAddConferenceRoom).css("display", "block");

                $($rfp.controls.txtEventDate).datepicker("setDate", common.getDateFromPicker($($rfp.controls.txtEventStartDate).val()));
            }
            if (modelId == "#addgrouproommodelcontainer") {
                $($rfp.controls.btnUpdateGroupRoom).css("display", "none");
                $($rfp.controls.btnAddGroupRoom).css("display", "block");

                $($rfp.controls.txtGroupRoomEventDate).datepicker("setDate", common.getDateFromPicker($($rfp.controls.txtEventStartDate).val()));
            }
        }
        $(modelId).modal("show");
        $($rfp.controls.txtCheckedInDate).datepicker({ minDate: new Date() });
    },
    closemodel: function (modelId) {
        $(modelId).modal("hide");
        $(modelId).find("input,textarea,select,select2").val('').end();
       // $($rfp.controls.ddlStartTimeInterval).select2("val", "");
       // $($rfp.controls.ddlEndTimeInterval).select2("val", "");
        $($rfp.controls.ddlSeatingType).select2("val", "");
       // $($rfp.controls.ddlStartTimeInterval).select2("val", "");
       // $($rfp.controls.ddlEndTimeInterval).select2("val", "");
        $($rfp.controls.ddlSeatingType).select2("val", "");
        // $(".select2-container").select2("data", { "ID": "", "text": "" });
       // $(".hasDatepicker").datepicker("setDate", new Date());
        
        
        $($rfp.controls.s2id_ddlRoomType).select2("val", "");
        $($rfp.controls.s2id_ddlSeatingType).select2("val", "");
        $($rfp.controls.s2id_ddlGroupRoomSeatingType).select2("val", "");

        var dt = new Date();
        var time = dt.getHours() + ":" + dt.getMinutes();

        //$($rfp.controls.txtGroupRoomStartTimeInterval).val(time);
        //$($rfp.controls.txtGroupRoomEndTimeInterval).val(time);
        $($rfp.controls.txtStartTimeInterval).timepicker('setTime', time);
        $($rfp.controls.txtEndTimeInterval).timepicker('setTime', time);
        $($rfp.controls.txtGroupRoomStartTimeInterval).timepicker('setTime', time);
        $($rfp.controls.txtGroupRoomEndTimeInterval).timepicker('setTime', time);
      //  $($rfp.controls.s2id_ddlGroupRoomStartTimeInterval).select2("val", "");
      //  $($rfp.controls.s2id_ddlGroupRoomEndTimeInterval).select2("data", "");

    },
    
    getRoomTechniques: function () {
        var url = $bind.URL($rfp.apiPath.GetRoomTechniques);
        ajaxRequest.httpGetSyncWithData(url, {}, $rfp.bindRoomTechniques, $rfp.getRequestItemOnError);
    },
    getConferenceFoods: function () {
        var url = $bind.URL($rfp.apiPath.GetConferenceFoods);
        ajaxRequest.httpGetSyncWithData(url, {}, $rfp.bindConferenceFoods, $rfp.getRequestItemOnError);
    },
    bindRoomTechniques: function (data) {
        var resData = data.data.lookup;
        var penAndNotepad = resData[0];
        var htmlContent = "<div class='row mt-15'>";
        htmlContent = htmlContent + "<div class='col-md-3'>";
        htmlContent = htmlContent + "<label class='checkbox-inline lblcheckbox-pad-note chkcontainer bluefont'>&nbsp;" + penAndNotepad.Name;
        htmlContent = htmlContent + "<input type='checkbox' class='checkbox-pad-note' id='" + penAndNotepad.Id + "'><span class='checkmark'></span></label>";
        htmlContent = htmlContent + "</div></div><div class='clear10'></div>";
        resData.splice(0, 1);
        htmlContent = htmlContent + "<div class='row'>";
        $.each(resData, function (index, value) {
            htmlContent = htmlContent + "<div class='form-group col-md-4'>";
            htmlContent = htmlContent + "<div class='inner-addon left-addon'>";
            htmlContent = htmlContent + "<label class='labelheader'>" + value.Name + "</label>";
            htmlContent = htmlContent + "<input class=' form-control' value='0' min='0' type='number' id='" + value.Id + "' />";
            htmlContent = htmlContent + "</div>";
            htmlContent = htmlContent + "</div>";
        });
        htmlContent = htmlContent + "</div>";
        $($rfp.controls.divRoomEquipment).html(htmlContent);
    },
    bindConferenceFoods: function (data) {
        var resData = data.data;
        var htmlContent = "";
        $.each(resData, function (index_i, value_i) {
            if (value_i.ConferenceFoods && value_i.ConferenceFoods.length > 0) {
                htmlContent = htmlContent + "<div class='row mt-15'><div class='col-md-4 confGroup'>";
                htmlContent = htmlContent + "<label class='bluefont' id='" + value_i.GroupId + "'>" + value_i.GroupName + "</label>";
                htmlContent = htmlContent + "</div>";
                htmlContent = htmlContent + "<div class='col-md-8'>";
                $.each(value_i.ConferenceFoods, function (index_j, value_j) {
                    htmlContent = htmlContent + "<div class='col-md-12'>";
                    htmlContent = htmlContent + "<label class='checkbox-inline chkcontainer'><input type='checkbox' id='" + value_j.Id + "'>" + value_j.Name + "<span class='checkmark'></span></label>";
                    htmlContent = htmlContent + "</div>";

                });
                htmlContent = htmlContent + "</div></div><div class='row'><div class='col-md-12'><hr></div></div><div class='clear10'></div>";
            }


            //if (value_i.ConferenceFoods && value_i.ConferenceFoods.length > 0) {
            //    //htmlContent = htmlContent + "<div class='row mt-15' style='padding-bottom:25px'><div class='col-md-12 confGroup'>";
            //    //htmlContent = htmlContent + "<label id='" + value_i.GroupId + "'>" + value_i.GroupName + "</label>";
            //    //htmlContent = htmlContent + "</div></div>";
               
            //    $.each(value_i.ConferenceFoods, function (index_j, value_j) {

            //        if (index_j % 2 == 0) {
            //            htmlContent = htmlContent + "<div class='row mt-5' style='margin-top:10px'>";
            //        }
                   

            //        htmlContent = htmlContent + "<div class='col-md-6'>";
            //        htmlContent = htmlContent + "<label class='checkbox-inline chkcontainer'><input type='checkbox' id='" + value_j.Id + "'>" + value_j.Name + "<span class='checkmark'></span></label>";
            //        htmlContent = htmlContent + "</div>";
            //        if (index_j % 2 != 0) {
            //            htmlContent = htmlContent + "</div>";
            //        }

            //    });
            //    htmlContent = htmlContent + "</div></div><div class='row'><div class='col-md-12'><hr></div></div><div class='clear10'></div>";
            //}

        });
        $($rfp.controls.divConferenceFoods).html(htmlContent);
    },
    getItemInformation: function (resourceType, RequestId, modelId) {

        if (modelId === 0) {
            if (resourceType === "H") {
                $rfp.openmodel("#addroommodelcontainer", resourceType, true, "#tblHotelAccomodation");

            }
            //if (resourceType === "R") {
            //    $rfp.openmodel("#addroommodelcontainer", resourceType, true, "");
            //}
            //if (resourceType === "L") {
            //    $rfp.openmodel("#addroommodelcontainer", resourceType, true, "");
            //}
        }
        else if (modelId === 1) {

            //$rfp.openmodel("#addconferenceroommodelcontainer", resourceType, true);
            if (resourceType === "H") {
                $rfp.openmodel("#addconferenceroommodelcontainer", resourceType, true, "#tblHotelConferenceRoom");
            }
            if (resourceType === "R") {
                $rfp.openmodel("#addconferenceroommodelcontainer", resourceType, true, "#tblRestaurantConferenceRoom");
            }
            if (resourceType === "L") {
                $rfp.openmodel("#addconferenceroommodelcontainer", resourceType, true, "#tblLocationConferenceRoom");
            }
        }
        else if (modelId === 2) {
            if (resourceType === "H") {
                $rfp.openmodel("#addgrouproommodelcontainer", resourceType, true, "#tblHotelGroupRoom");
            }
            if (resourceType === "R") {
                $rfp.openmodel("#addgrouproommodelcontainer", resourceType, true, "#tblRestaurantGroupRoom");
            }
            if (resourceType === "L") {
                $rfp.openmodel('#addgrouproommodelcontainer', resourceType, true, '#tblLocationGroupRoom');
            }
            //$rfp.openmodel("#addgrouproommodelcontainer", resourceType,true);
        }

        $($rfp.controls.hdnMiceRequestId).val(RequestId);
        //$(modelId).modal('show');
        common.displayLoader();
        if (RequestId > 0) {
            var url = $bind.URL($rfp.apiPath.GetRequestItem);
            var reqdata = { rfpMiceRequestItemId: RequestId };
            //console.log(reqdata);
            ajaxRequest.httpGetSyncWithData(url, reqdata, $rfp.RetriveItemInformation, $rfp.getRequestItemOnError);
        } else {
            common.hideLoader();
        }
    },
    getResourceItemDetail: function (sortBy, resourceType, requestItemType) {
        ///INSPIRE-102
        ///Get resource list by rfpid wise
        $.ajax({
            type: 'GET',
            url: $bind.URL($rfp.apiPath.GetResourceItemDetail) + '?sortBy=' + sortBy + '&type=' + resourceType + '&requestItemType=' + requestItemType + '&rfpId='+ $($rfp.variables.RfpId).val(),
            success: function (response) {
                if (response != undefined && response != null && response.code == "704") {

                    if (requestItemType === "Accommodation") {
                        $rfp.fillAccomodationRequestItems(resourceType, response.data, 0, true);
                    } else if (requestItemType === "ConferenceRoom") {
                        $rfp.fillConferrenceRoomRequestItems(resourceType, response.data, 0, true);
                    } else if (requestItemType === "GroupRoom") {
                        $rfp.fillGroupRoomRequestItems(resourceType, response.data, 0, true);
                    }
                }
            }
        });
    },
    deleteMiceRequestItem: function (RequestId, e) {
        BootstrapDialog.confirm($($c.controls.sDeleteConfirm).val(), function (result) {
            if (result) {
                if (RequestId > 0) {
                    var url = $bind.URL($rfp.apiPath.DeleteRFPRequestItem);
                    var reqdata = { rfpMiceRequestItemId: RequestId };
                    $.ajax({
                        type: "DELETE",
                        url: $bind.URL($rfp.apiPath.DeleteRFPRequestItem) + '?rfpMiceRequestItemId=' + RequestId + '',
                        success: function (response) {
                            if (response != undefined && response != null && response.code == "702") {
                                $managecode.Decision(false, "", response.code, response.message, "Filter", true);
                                $(e).parent('td').parent('tr').remove();
                            } else {
                                $managecode.Decision(false, "", response.code, response.message, "Filter", false);
                            }
                        },
                        error: function (data, status, error) {
                            $managecode.Decision(true, data, "", "", "error", false);
                        }
                    });
                } else {
                    common.hideLoader();
                }
            }
        });
    },
    RetriveItemInformation: function (data) {
        var fromType = $($rfp.controls.hdnFromType).val();
        var modalId = $($rfp.controls.hdnModelType).val();

        if (data.data.requestitems.length > 0) {
            if (modalId === "#addroommodelcontainer") {
                $("#txtNoOfRooms").val(data.data.requestitems["0"].quantity);
                $("#txtBudget").val(data.data.requestitems["0"].display_budget);
                $("#txtCheckedInDate").val(data.data.requestitems["0"].display_reserve_fromdate);
                $("#txtCheckedOutDate").val(data.data.requestitems["0"].display_reserve_todate);
                $($rfp.controls.ddlRoomType).select2('data', {
                    "id": data.data.requestitems["0"].room_type,
                    "text": data.data.requestitems["0"].display_roomtype
                });
            }
            if (modalId === "#addconferenceroommodelcontainer") {
                $($rfp.controls.txtEventDate).val(data.data.requestitems["0"].display_reserve_fromdate);
                //$($rfp.controls.ddlStartTimeInterval).val(data.data.requestitems["0"].start_time);
                //$($rfp.controls.ddlStartTimeInterval).select2('data', {
                //    "id": data.data.requestitems["0"].start_time,
                //    "text": data.data.requestitems["0"].start_time
                //});
                ////$($rfp.controls.ddlEndTimeInterval).val(data.data.requestitems["0"].end_time);
                //$($rfp.controls.ddlEndTimeInterval).select2('data', {
                //    "id": data.data.requestitems["0"].end_time,
                //    "text": data.data.requestitems["0"].end_time
                //});
                $($rfp.controls.txtStartTimeInterval).timepicker('setTime', data.data.requestitems["0"].start_time);
                $($rfp.controls.txtEndTimeInterval).timepicker('setTime', data.data.requestitems["0"].end_time);
                $($rfp.controls.txtParticipants).val(data.data.requestitems["0"].quantity);
                $($rfp.controls.ddlSeatingType).select2('data', {
                    "id": data.data.requestitems["0"].seating_type,
                    "text": data.data.requestitems["0"].display_seating_type
                });
                $($rfp.controls.txtRoomSize).val(data.data.requestitems["0"].roomsize);
                $($rfp.controls.txtConferenceRoomBudget).val(data.data.requestitems["0"].budget);
                $($rfp.controls.txtComments).val(data.data.requestitems["0"].comments);



                $rfp.setTechnique(data.data.requestitems["0"].technique);
                $rfp.setFood(data.data.requestitems["0"].food);
            }
            if (modalId === "#addgrouproommodelcontainer") {
                $($rfp.controls.txtGroupRoomEventDate).val(data.data.requestitems["0"].display_reserve_fromdate);
                //$($rfp.controls.ddlGroupRoomStartTimeInterval).select2('data', {
                //    "id": data.data.requestitems["0"].start_time,
                //    "text": data.data.requestitems["0"].start_time
                //});
                //$($rfp.controls.ddlGroupRoomEndTimeInterval).select2('data', {
                //    "id": data.data.requestitems["0"].end_time,
                //    "text": data.data.requestitems["0"].end_time
                //});
    

                $($rfp.controls.txtGroupRoomStartTimeInterval).timepicker('setTime', data.data.requestitems["0"].start_time);
                $($rfp.controls.txtGroupRoomEndTimeInterval).timepicker('setTime', data.data.requestitems["0"].end_time);

                $($rfp.controls.ddlGroupRoomSeatingType).select2('data', {
                    "id": data.data.requestitems["0"].seating_type,
                    "text": data.data.requestitems["0"].display_seating_type
                })
                $($rfp.controls.txtGroupRoomParticipants).val(data.data.requestitems["0"].quantity);
                $($rfp.controls.txtGroupRoomRoomSize).val(data.data.requestitems["0"].roomsize);
                $($rfp.controls.txtGroupRoomRoomBudget).val(data.data.requestitems["0"].budget);
            }
        }

        if ($($rfp.controls.hdnIsViewMode).val() == "true") {
            $($rfp.controls.btnUpdateRoom).hide();
            $($rfp.controls.btnUpdateConferenceRoom).hide();
            $($rfp.controls.btnUpdateGroupRoom).hide();
            $($rfp.controls.btnAbortRoom).hide();
            $($rfp.controls.btnAbortConferenceRoom).hide();
            $($rfp.controls.btnAbortGroupRoom).hide();
        }


        //console.log(data);
        common.hideLoader();


    },
    setTechnique: function (data) {
        if (data) {
            if (data['pens_and_notepads']) {
                $('#pens_and_notepads').prop("checked", true);
            }
            $.each($('#divRoomEquipment input[type=number]'), function () {
                $(this).val(data[$(this).attr('id')]);
            });
        }
    },
    setFood: function (data) {
        if (data) {
            $.each($('#divConferenceFoods input[type=checkbox]'), function () {
                if (data[$(this).attr('id')]) {
                    $(this).prop("checked", true);
                }
            });
        }
    },
    RetriveItemInformationError: function (data) {
        common.hideLoader();
    },
    saveRoomInformation: function () {

        ///INSPIRE-102
        ///Validate rfp entry and then save service record
        if ($($rfp.variables.RfpId).val() != '' && $($rfp.variables.RfpId).val() == 0) {
            if ($rfp.saveStartRFP() == false) {
                return false;
            }
        }

        var fromType = $($rfp.controls.hdnFromType).val();
        var rfpMiceRequestId = 0;
        if (fromType === "H") {
            rfpMiceRequestId = $($rfp.variables.HotelRequestId).val();
        } else if (fromType === "L") {
            rfpMiceRequestId = $($rfp.variables.LocationRequestId).val();
        } else if (fromType === "R") {
            rfpMiceRequestId = $($rfp.variables.RestaurantRequestId).val();
        }
        var MiceRequestId = $($rfp.controls.hdnMiceRequestId).val() != null && $($rfp.controls.hdnMiceRequestId).val() > 0 ? $($rfp.controls.hdnMiceRequestId).val() : 0;
        var requestItem = {
            id: MiceRequestId,
            rfp_id: $($rfp.variables.RfpId).val(),
            rfp_micerequestid: rfpMiceRequestId,
            request_itemtype: $rfp.variables.AccomodationType,
            requestitems: []
        };
        console.log($($rfp.controls.txtCheckedOutDate).val());
        //var fromdate = new Date(JSON.parse(JSON.stringify(new Date(common.getDateFromPicker($($rfp.controls.txtCheckedInDate).val())).toString())))
        //var todate = new Date(JSON.parse(JSON.stringify(new Date(common.getDateFromPicker($($rfp.controls.txtCheckedOutDate).val())).toString())))


        if ($($rfp.controls.txtCheckedInDate).val() == undefined || $($rfp.controls.txtCheckedInDate).val() == "" || $($rfp.controls.txtCheckedInDate).val() == null) {
            $($rfp.controls.txtCheckedInDate).css('cssText', 'border-color:red !important');           
            $managecode.Decision(false, "", "", $($rfp.controls.hdnCheckinDateValidationMsg).val(), "Error", false);
            return false;
        }

        if ($($rfp.controls.txtCheckedOutDate).val() == undefined || $($rfp.controls.txtCheckedOutDate).val() == "" || $($rfp.controls.txtCheckedOutDate).val() == null) {
            $($rfp.controls.txtCheckedOutDate).css('cssText', 'border-color:red !important');
            $managecode.Decision(false, "", "", $($rfp.controls.hdnCheckOutDateValidationMsg).val(), "Error", false);
            return false;
        }


        var requestItemObject = {
            type: $rfp.variables.AccomodationType,
            quantity: $($rfp.controls.txtNoOfRooms).val(),
            reserve_fromdate: common.getDateFromPicker($($rfp.controls.txtCheckedInDate).val()),
            reserve_todate: common.getDateFromPicker($($rfp.controls.txtCheckedOutDate).val()),
            budget: $($rfp.controls.txtBudget).val(),
            comments: "",
            room_type: $($rfp.controls.ddlRoomType).val()
        };
        //requestItem.RequestItems.push(requestItemObject);
        requestItem.requestitems.push(requestItemObject);
        var url = $bind.URL($rfp.apiPath.AddRequestItem);
        common.displayLoader();
        console.log(JSON.stringify(requestItem));
        ajaxRequest.httpPost(url, JSON.stringify(requestItem), $rfp.saveRoomInformationOnSuccess, $rfp.saveRoomInformationOnError);
    },
    saveRoomInformationOnSuccess: function (data) {
        var rfpMiceRequestItemId = data.data;
        
        $rfp.fillRequestDetails();
        var modelId = $($rfp.controls.hdnModelType).val();
        $(modelId).modal('hide');
        $rfp.closemodel($(modelId))
        //if (rfpMiceRequestItemId > 0) {
        //    var url = $bind.URL($rfp.apiPath.GetRequestItem);
        //    var reqdata = { rfpMiceRequestItemId: rfpMiceRequestItemId };
        //    ajaxRequest.httpGetSyncWithData(url, reqdata, $rfp.getRequestItemOnSuccess, $rfp.getRequestItemOnError);
        //} else {
        //    common.hideLoader();
        //}
        if (data.code == "701") {
            $managecode.Decision(false, "", data.code, data.message, "Filter", true);
        }
        else {
            $managecode.Decision(false, "", data.code, data.message, "Filter", false);
        }


    },
    saveRoomInformationOnError: function (data) {
        $managecode.Decision(true, data, "", "", "error", false);
        common.hideLoader();
    },
    saveConferenceRoomInformation: function () {

        ///INSPIRE-102
        ///Validate rfp entry and then save service record
        if ($($rfp.variables.RfpId).val() != '' && $($rfp.variables.RfpId).val() == 0) {
            if ($rfp.saveStartRFP() == false) {
                return false;
            }
        }


        var fromType = $($rfp.controls.hdnFromType).val();
        var rfpMiceRequestId = 0;
        var MiceRequestId = $($rfp.controls.hdnMiceRequestId).val() != null && $($rfp.controls.hdnMiceRequestId).val() > 0 ? $($rfp.controls.hdnMiceRequestId).val() : 0;
        if (fromType === "H") {
            rfpMiceRequestId = $($rfp.variables.HotelRequestId).val();
        } else if (fromType === "L") {
            rfpMiceRequestId = $($rfp.variables.LocationRequestId).val();
        } else if (fromType === "R") {
            rfpMiceRequestId = $($rfp.variables.RestaurantRequestId).val();
        }

        if ($($rfp.controls.txtEventDate).val() == undefined || $($rfp.controls.txtEventDate).val() == "" || $($rfp.controls.txtEventDate).val() == null) {
            $($rfp.controls.txtEventDate).css('cssText', 'border-color:red !important');
            $managecode.Decision(false, "", "", $($rfp.controls.hdnEventDayValidationMsg).val(), "Error", false);
            return false;
        }

        if ($($rfp.controls.txtParticipants).val() == undefined || $($rfp.controls.txtParticipants).val() == "" || $($rfp.controls.txtParticipants).val() == null) {
            $($rfp.controls.txtParticipants).css('cssText', 'border-color:red !important');
            $managecode.Decision(false, "", "", $($rfp.controls.hdnConfParticipantMessage).val(), "Error", false);
            return false;
        }

        var requestItem = {
            id: MiceRequestId,
            rfp_id: $($rfp.variables.RfpId).val(),
            rfp_micerequestid: rfpMiceRequestId,
            request_itemtype: $rfp.variables.ConferenceRoomType,
            requestitems: []
        };

        var requestItemObject = {
            type: $rfp.variables.ConferenceRoomType,
            quantity: $($rfp.controls.txtParticipants).val(),
            reserve_fromdate: common.addtimewithdate($($rfp.controls.txtEventDate).val(), $($rfp.controls.txtStartTimeInterval).val()),
            reserve_todate: common.addtimewithdate($($rfp.controls.txtEventDate).val(), $($rfp.controls.txtEndTimeInterval).val()),
            budget: $($rfp.controls.txtConferenceRoomBudget).val(),
            roomsize: $($rfp.controls.txtRoomSize).val(),
            comments: $($rfp.controls.txtComments).val(),
            //start_time: $($rfp.controls.ddlStartTimeInterval).val(),
            //end_time: $($rfp.controls.ddlEndTimeInterval).val(),
            start_time: $($rfp.controls.txtStartTimeInterval).val(),
            end_time: $($rfp.controls.txtEndTimeInterval).val(),
            seating_type: $($rfp.controls.ddlSeatingType).val()
        };

        //console.log(requestItemObject);

        if (fromType === "H" || fromType === "L") {
            requestItemObject.technique = $rfp.getTechnique();
            requestItemObject.food = $rfp.getFood();
        }
        if (fromType === "R") {
            requestItemObject.food = $rfp.getFood();
        }
        
        requestItem.requestitems.push(requestItemObject);
        var url = $bind.URL($rfp.apiPath.AddRequestItem);
        common.displayLoader();
        ajaxRequest.httpPost(url, JSON.stringify(requestItem), $rfp.saveConferenceRoomInformationOnSuccess, $rfp.saveConferenceRoomInformationOnError);
    },
    getTechnique: function () {
        var reqData = {};
        reqData['pens_and_notepads'] = $('#pens_and_notepads').prop("checked");
        $.each($('#divRoomEquipment input[type=number]'), function () {
            reqData[$(this).attr('id')] = $(this).val();
        });
        return reqData;
    },
    getFood: function () {
        var reqData = {};
        $.each($('#divConferenceFoods input[type=checkbox]'), function () {
            reqData[$(this).attr('id')] = $(this).prop("checked");
        });
        return reqData;
    },
    saveConferenceRoomInformationOnSuccess: function (data) {
        var rfpMiceRequestItemId = data.data;

        $rfp.fillRequestDetails();
        var modelId = $($rfp.controls.hdnModelType).val();
        $(modelId).modal('hide');
        $rfp.closemodel($(modelId))
        //if (rfpMiceRequestItemId > 0) {
        //    var url = $bind.URL($rfp.apiPath.GetRequestItem);
        //    var reqdata = { rfpMiceRequestItemId: rfpMiceRequestItemId };
        //    ajaxRequest.httpGetSyncWithData(url, reqdata, $rfp.getRequestItemOnSuccess, $rfp.getRequestItemOnError);
        //} else {
        //    common.hideLoader();
        //}
        if (data.code == "701") {
            $managecode.Decision(false, "", data.code, data.message, "Filter", true);
        }
        else {
            $managecode.Decision(false, "", data.code, data.message, "Filter", false);
        }
        $rfp.closemodel('#addconferenceroommodelcontainer');
    },
    saveConferenceRoomInformationOnError: function (data) {
        common.hideLoader();
        $managecode.Decision(true, data, "", "", "error", false);
    },
    saveGroupRoomInformation: function () {
        ///INSPIRE-102
        ///Validate rfp entry and then save service record
        if ($($rfp.variables.RfpId).val() != '' && $($rfp.variables.RfpId).val() == 0) {
            if ($rfp.saveStartRFP() == false) {
                return false;
            }
        }

        var fromType = $($rfp.controls.hdnFromType).val();
        var rfpMiceRequestId = 0;
        if (fromType === "H") {
            rfpMiceRequestId = $($rfp.variables.HotelRequestId).val();
        } else if (fromType === "L") {
            rfpMiceRequestId = $($rfp.variables.LocationRequestId).val();
        } else if (fromType === "R") {
            rfpMiceRequestId = $($rfp.variables.RestaurantRequestId).val();
        }
        var MiceRequestId = $($rfp.controls.hdnMiceRequestId).val() != null && $($rfp.controls.hdnMiceRequestId).val() > 0 ? $($rfp.controls.hdnMiceRequestId).val() : 0;

        if ($($rfp.controls.txtGroupRoomEventDate).val() == undefined || $($rfp.controls.txtGroupRoomEventDate).val() == "" || $($rfp.controls.txtGroupRoomEventDate).val() == null) {
            $($rfp.controls.txtGroupRoomEventDate).css('cssText', 'border-color:red !important');
            $managecode.Decision(false, "", "", $($rfp.controls.hdnGroupEventDayValidationMsg).val(), "Error", false);
            return false;
        }

        if ($($rfp.controls.txtGroupRoomParticipants).val() == undefined || $($rfp.controls.txtGroupRoomParticipants).val() == "" || $($rfp.controls.txtGroupRoomParticipants).val() == null) {
            $($rfp.controls.txtGroupRoomParticipants).css('cssText', 'border-color:red !important');
            $managecode.Decision(false, "", "", $($rfp.controls.hdnGroupRoomParticipantMessage).val(), "Error", false);
            return false;
        }

        var requestItem = {
            id: MiceRequestId,
            rfp_id: $($rfp.variables.RfpId).val(),
            rfp_micerequestid: rfpMiceRequestId,
            request_itemtype: $rfp.variables.GroupRoomType,
            requestitems: []
        };

        var requestItemObject = {
            type: $rfp.variables.GroupRoomType,
            quantity: $($rfp.controls.txtGroupRoomParticipants).val(),
            reserve_fromdate: common.addtimewithdate($($rfp.controls.txtGroupRoomEventDate).val(), $($rfp.controls.txtGroupRoomStartTimeInterval).val()),
            reserve_todate: common.addtimewithdate($($rfp.controls.txtGroupRoomEventDate).val(), $($rfp.controls.txtGroupRoomEndTimeInterval).val()),
            budget: $($rfp.controls.txtGroupRoomRoomBudget).val(),
            roomsize: $($rfp.controls.txtGroupRoomRoomSize).val(),
            comments: "",
            //start_time: $($rfp.controls.ddlGroupRoomStartTimeInterval).val(),
            //end_time: $($rfp.controls.ddlGroupRoomEndTimeInterval).val(),
            start_time: $($rfp.controls.txtGroupRoomStartTimeInterval).val(),
            end_time: $($rfp.controls.txtGroupRoomEndTimeInterval).val(),
            seating_type: $($rfp.controls.ddlGroupRoomSeatingType).val()
        };


        //console.log(requestItemObject);

        requestItem.requestitems.push(requestItemObject);
        var url = $bind.URL($rfp.apiPath.AddRequestItem);
        common.displayLoader();
        //console.log(JSON.stringify(requestItem));
        ajaxRequest.httpPost(url, JSON.stringify(requestItem), $rfp.saveGroupRoomInformationOnSuccess, $rfp.saveGroupRoomInformationOnError);
    },
    saveGroupRoomInformationOnSuccess: function (data) {
        var rfpMiceRequestItemId = data.data;
        $rfp.fillRequestDetails();
        var modelId = $($rfp.controls.hdnModelType).val();
        $(modelId).modal('hide');
        $rfp.closemodel($(modelId))
        //if (rfpMiceRequestItemId > 0) {
        //    var url = $bind.URL($rfp.apiPath.GetRequestItem);
        //    var reqdata = { rfpMiceRequestItemId: rfpMiceRequestItemId };
        //    ajaxRequest.httpGetSyncWithData(url, reqdata, $rfp.getRequestItemOnSuccess, $rfp.getRequestItemOnError);
        //} else {
        //    common.hideLoader();
        //}
        if (data.code == "701") {
            $managecode.Decision(false, "", data.code, data.message, "Filter", true);
        }
        else {
            $managecode.Decision(false, "", data.code, data.message, "Filter", false);
        }
    },
    saveGroupRoomInformationOnError: function (data) {
        common.hideLoader();
        $managecode.Decision(true, data, "", "", "error", false);
    },
    getRequestItemOnSuccess: function (data) {
        var fromType = $($rfp.controls.hdnFromType).val();
        var modelId = $($rfp.controls.hdnModelType).val();
        var resourceType = 0;
        if (fromType === "H") {
            resourceType = 0;
        }
        if (fromType === "R") {
            resourceType = 1;
        }
        if (fromType === "L") {
            resourceType = 2;
        }
        var requestItems = [];
        requestItems.push(data.data);
        $rfp.fillRFPMiceRequestItems(fromType, requestItems);
        $(modelId).modal('hide');
        $rfp.closemodel($(modelId));
        common.hideLoader();
    },
    getRequestItemOnError: function (data) {
        common.hideLoader();
    },
    validateRFP: function () {
        var isValidate = true;
        var eventName = $($rfp.controls.txtEventName).val();
       // var eventduration = $($rfp.controls.ddlduration).val();
        var eventStartDate = $($rfp.controls.txtEventStartDate).val();
        var eventEndDate = $($rfp.controls.txtEventEndDate).val();


        if (eventName == undefined || eventName == "" || eventName == null) {
            $($rfp.controls.txtEventName).css('cssText', 'border-color:red !important');
            isValidate = false;
            $managecode.Decision(false, "", "", $($rfp.controls.hdnEventNameValidation).val(), "Error", false);
            return isValidate;
        }
        else {
            $($rfp.controls.txtEventName).removeAttr('style');
            isValidate = true;
        }

        if (eventStartDate == undefined || eventStartDate == "" || eventStartDate == null) {
            $($rfp.controls.txtEventStartDate).css('cssText', 'border-color:red !important');
            isValidate = false;
           // $managecode.Decision(false, "", "", $($rfp.controls.hdnEventNameValidation).val(), "Error", false);
            return isValidate;
        }
        else {
            $($rfp.controls.txtEventStartDate).removeAttr('style');
            isValidate = true;
        }

        if (eventEndDate == undefined || eventEndDate == "" || eventEndDate == null) {
            $($rfp.controls.txtEventEndDate).css('cssText', 'border-color:red !important');
            isValidate = false;
            // $managecode.Decision(false, "", "", $($rfp.controls.hdnEventNameValidation).val(), "Error", false);
            return isValidate;
        }
        else {
            $($rfp.controls.txtEventEndDate).removeAttr('style');
            isValidate = true;
        }




        //if (eventduration == undefined || eventduration == "" || eventduration == null) {
        //    $('.select2-choice').css('cssText', 'border-bottom-color:red !important');
        //    isValidate = false;
        //    $managecode.Decision(false, "", "", $($rfp.controls.hdnDurationValidation).val(), "Error", false);
        //    return isValidate;
        //}
        //else {
        //    $('.select2-choice').removeAttr('style');
        //    isValidate = true;
        //}
        return isValidate;
    },
    saveRFP: function (isSubmit) {
        $rfp.variables.IsRFPSubmitted = isSubmit;
        var rfpModel = {
            id: $($rfp.variables.RfpId).val(),
            //rfp_type: $($rfp.controls.ddleventtype).val(),
            rfp_type: 'meetings',
           // days_running: $($rfp.controls.ddlduration).val(),
            days_running: $($rfp.controls.hdnDuration).val(),
            is_submitted: isSubmit,
            event_name: $($rfp.controls.txtEventName).val(),
            eventstartdate: common.getDateFromPicker($($rfp.controls.txtEventStartDate).val()),
            eventenddate: common.getDateFromPicker($($rfp.controls.txtEventEndDate).val()),
            micerequests: []
        };
        if ($rfp.validateRFP()) {
            if ($($rfp.variables.HotelRequestId).val() !== undefined && $($rfp.variables.HotelRequestId).val() !== null && $($rfp.variables.HotelRequestId).val() > 0) {
                var hotelMiceRequest = {
                    rfpid: $($rfp.variables.RfpId).val(),
                    id: $($rfp.variables.HotelRequestId).val(),
                    comments: $($rfp.controls.txtHotelComments).val()
                };
                rfpModel.micerequests.push(hotelMiceRequest);
            }

            if ($($rfp.variables.LocationRequestId).val() !== undefined && $($rfp.variables.LocationRequestId).val() !== null && $($rfp.variables.LocationRequestId).val() > 0) {
                var locationMiceRequest = {
                    rfpid: $($rfp.variables.RfpId).val(),
                    id: $($rfp.variables.LocationRequestId).val(),
                    comments: $($rfp.controls.txtLocationComments).val()
                };
                rfpModel.micerequests.push(locationMiceRequest);
            }

            if ($($rfp.variables.RestaurantRequestId).val() !== undefined && $($rfp.variables.RestaurantRequestId).val() !== null && $($rfp.variables.RestaurantRequestId).val() > 0) {
                var restaurantsMiceRequest = {
                    rfpid: $($rfp.variables.RfpId).val(),
                    id: $($rfp.variables.RestaurantRequestId).val(),
                    comments: $($rfp.controls.txtRestaurantComments).val()
                };
                rfpModel.micerequests.push(restaurantsMiceRequest);
            }

            var url = $bind.URL($rfp.apiPath.SubmitRFP);
            common.displayLoader();
            ajaxRequest.httpPost(url, JSON.stringify(rfpModel), $rfp.saveRFPOnSuccess, $rfp.saveRFPOnError);
        }

    },
    fillRequestDetails: function () {
        var tableid = $('#hdnTableId').val();
        var sortColumn = '';
        var isasc = false;
        var modelId = $($rfp.controls.hdnModelType).val();
        if ($(tableid).find('.sortable.desc').length > 0) {
            sortColumn = $(tableid).find('.sortable.desc')[0].id;
        }
        if ($(tableid).find('.sortable.asc').length > 0) {
            sortColumn = $(tableid).find('.sortable.asc')[0].id
            isasc = true;
        }
        if (tableid === "#tblHotelAccomodation") {
            $rfp.getResourceItemDetail(sortColumn + (isasc == true ? 'asc' : 'desc'), 'H', 'Accommodation')
        }
        if (tableid === "#tblHotelConferenceRoom") {
            $rfp.getResourceItemDetail(sortColumn + (isasc == true ? 'asc' : 'desc'), 'H', 'ConferenceRoom')
        }
        if (tableid === "#tblHotelGroupRoom") {
            $rfp.getResourceItemDetail(sortColumn + (isasc == true ? 'asc' : 'desc'), 'H', 'GroupRoom')
        }
        if (tableid === "#tblRestaurantConferenceRoom") {
            $rfp.getResourceItemDetail(sortColumn + (isasc == true ? 'asc' : 'desc'), 'R', 'ConferenceRoom')
        }
        if (tableid === "#tblRestaurantGroupRoom") {
            $rfp.getResourceItemDetail(sortColumn + (isasc == true ? 'asc' : 'desc'), 'R', 'GroupRoom')
        }
        if (tableid === "#tblLocationConferenceRoom") {
            $rfp.getResourceItemDetail(sortColumn + (isasc == true ? 'asc' : 'desc'), 'L', 'ConferenceRoom')
        }
        if (tableid === "#tblLocationGroupRoom") {
            $rfp.getResourceItemDetail(sortColumn + (isasc == true ? 'asc' : 'desc'), 'L', 'GroupRoom')
        }
        //$(modelId).modal('hide');
        //$rfp.closemodel($(modelId))
    },
    saveRFPOnSuccess: function (data) {
        
        common.hideLoader();
        
        if (data.code == "613") {
            var result = data.data;
            var validateMsg = "";
            $.each(data.data, function (index, value) {
                if (index == "0") {
                    common.displayMessage($($rfp.controls.hdnHotelValidationMsg).val(), false);
                }
                else if (index == "1") {
                    common.displayMessage($($rfp.controls.hdnRestaurantValidationMsg).val(), false);
                }
                else if (index == "2") {
                    common.displayMessage($($rfp.controls.hdnLocationValidationMsg).val(), false);
                }
            });

        }
        else {
            ///INSPIRE-102
            ///Fill request proposal details into control
            var rfpDetail = data.data;
            if (rfpDetail != null && rfpDetail.id > 0) {

                var id = $rfp.GetURLParameter();
                if ((id == '') || (id != null && id.toLocaleLowerCase() == "startrfp")) {
                    var url = window.location.href;
                    window.location.href = url + '/' + rfpDetail.strid;
                    return;
                }
               
                if (rfpDetail.event_name !== undefined && rfpDetail.event_name !== null) {
                    $($rfp.controls.txtEventName).val(rfpDetail.event_name);
                }
                $($rfp.controls.hdnIsViewMode).val(rfpDetail.isviewmode);
                $($rfp.variables.RfpId).val(rfpDetail.id);

                if (rfpDetail.micerequests !== null && rfpDetail.micerequests !== undefined) {
                    $.each(rfpDetail.micerequests, function (index, value) {
                        
                        var resType = '';
                        if (value.resource_type === 0) {
                            $($rfp.variables.HotelRequestId).val(value.id);
                            $rfp.getResourceDetail("H", value.rfpresources);
                            resType = "H";
                            // Set Comments
                            if (value.comments !== undefined && value.comments !== null) {
                                $($rfp.controls.txtHotelComments).val(value.comments);
                            }
                        } else if (value.resource_type === 1) {
                            $($rfp.variables.RestaurantRequestId).val(value.id);
                            $rfp.getResourceDetail("R", value.rfpresources);
                            resType = "R";
                            // Set Comments
                            if (value.comments !== undefined && value.comments !== null) {
                                $($rfp.controls.txtRestaurantComments).val(value.comments);
                            }
                        } else if (value.resource_type === 2) {
                            $($rfp.variables.LocationRequestId).val(value.id);
                            $rfp.getResourceDetail("L", value.rfpresources);
                            resType = "L";
                            // Set Comments
                            if (value.comments !== undefined && value.comments !== null) {
                                $($rfp.controls.txtLocationComments).val(value.comments);
                            }
                        }

                    });
                }

                

                if ($rfp.variables.IsRFPSubmitted) {
                     //29-04-2020 - Change status wise display message.
                    var msg = '';
                    $.each(rfpDetail.micerequests, function (index, value) {
                        if (value.resource_type === 0) {
                            if (value.status == "fail") {
                                msg += $($c.controls.hdnhotel).val() + " " + $($rfp.localization.hdnRFPSubmitUnSuccessMessage).val() + "<br/>";
                            }
                            else {
                                msg += $($c.controls.hdnhotel).val() + " " + $($rfp.localization.RFPSubmitSuccessMessage).val() + "<br/>";
                            }

                        }
                        if (value.resource_type === 1) {
                            if (value.status == "fail") {
                                msg += $($c.controls.hdnrestaurant).val() + " " + $($rfp.localization.hdnRFPSubmitUnSuccessMessage).val() + "<br/>";
                            }
                            else {
                                msg += $($c.controls.hdnrestaurant).val() + " " + $($rfp.localization.RFPSubmitSuccessMessage).val() + "<br/>";
                            }
                        }
                        if (value.resource_type === 2) {
                            if (value.status == "fail") {
                                msg += $($c.controls.hdnlocation).val() + " " + $($rfp.localization.hdnRFPSubmitUnSuccessMessage).val() + "<br/>";
                            }
                            else {
                                msg += $($c.controls.hdnlocation).val() + " " + $($rfp.localization.RFPSubmitSuccessMessage).val() + "<br/>";
                            }
                        }
                    });

                   // common.displayMessage($($rfp.localization.RFPSubmitSuccessMessage).val(), true);
                    common.displayMessage(msg, true);
                    $($rfp.controls.btnSubmitRFP).hide();
                    $($rfp.controls.btnSaveRFP).hide();
                    setTimeout(function () { location.href = "/home"; }, 10000);
                } else {
                    common.displayMessage($($rfp.localization.RFPSavedSuccessMessage).val(), true);
                    //window.location.href = "/home";
                }
            }
        }
    },
    saveRFPOnError: function (data) {
        common.hideLoader();
    },
    fillRFPMiceRequestItems: function (resourceType, rfpMiceRequestItems) {
        $.each(rfpMiceRequestItems, function (index, value) {
            if (value.request_itemtype === "Accommodation") {
                $rfp.fillAccomodationRequestItems(resourceType, value.requestitems, value.id);
            } else if (value.request_itemtype === "ConferenceRoom") {
                $rfp.fillConferrenceRoomRequestItems(resourceType, value.requestitems, value.id);
            } else if (value.request_itemtype === "GroupRoom") {
                $rfp.fillGroupRoomRequestItems(resourceType, value.requestitems, value.id);
            }
        });
    },
    fillAccomodationRequestItems: function (resourceType, requestItems, micerequestid, isSortBy) {
        var htmlContent = "";
        $.each(requestItems, function (index, value) {
            htmlContent = htmlContent + "<tr>";
            //if (micerequestid > 0)
            htmlContent = htmlContent + "<td id='tdAmicerequestid" + (micerequestid > 0 ? micerequestid : value.id) + "' style='display:none;'>" + (micerequestid > 0 ? micerequestid : value.id) + "</td>";
            htmlContent = htmlContent + "<td>" + value.display_reserve_fromdate + "</td>";
            htmlContent = htmlContent + "<td>" + value.display_reserve_todate + "</td>";
            htmlContent = htmlContent + "<td>" + (value.display_roomtype == null ? '' : value.display_roomtype)  + "</td>";
            htmlContent = htmlContent + "<td>" + value.quantity + "</td>";
            htmlContent = htmlContent + "<td>" + value.display_budget + $rfp.variables.euroIcon + "</td>";
            htmlContent = htmlContent + "<td> <a href='javascript:void(0)' class='lnkView' onClick='$rfp.getItemInformation(" + "\"" + resourceType + "\"" + "," + (micerequestid > 0 ? micerequestid : value.id) + ",0);'><i class='glyphicon glyphicon-list-alt'></i></a> <a href='javascript:void(0)' class='lnkEdit' onClick='$rfp.getItemInformation(" + "\"" + resourceType + "\"" + "," + (micerequestid > 0 ? micerequestid : value.id) + ",0);'><i class='glyphicon glyphicon-edit'></i></a> <a href='javascript:void()' class='lnkDelete' onClick='$rfp.deleteMiceRequestItem(" + (micerequestid > 0 ? micerequestid : value.id) + ",this);'><i class='glyphicon glyphicon-trash'></i></a> </td>";
            htmlContent = htmlContent + "</tr>";
        });
        if (resourceType === "H" || resourceType === 0) {
            if (isSortBy == true) {
                $($rfp.controls.tbHotelAccomodation).empty();
            }
            $($rfp.controls.tbHotelAccomodation).append(htmlContent);
            if ($($rfp.controls.hdnIsViewMode).val() == "false") {
                $(".lnkView").hide();
            }
        } else if (resourceType === "R" || resourceType === 1) {
            if (isSortBy == true) {
                $($rfp.controls.tbRestaurantAccomodation).empty();
            }
            $($rfp.controls.tbRestaurantAccomodation).append(htmlContent);
            if ($($rfp.controls.hdnIsViewMode).val() == "false") {
                $(".lnkView").hide();
            }
        } else if (resourceType === "L" || resourceType === 2) {
            if (isSortBy == true) {
                $($rfp.controls.tbLocationAccomodation).empty();
            }
            $($rfp.controls.tbLocationAccomodation).append(htmlContent);
            if ($($rfp.controls.hdnIsViewMode).val() == "false") {
                $(".lnkView").hide();
            }
        }
    },
    fillConferrenceRoomRequestItems: function (resourceType, requestItems, micerequestid, isSortBy) {
        var htmlContent = "";
        $.each(requestItems, function (index, value) {
            htmlContent = htmlContent + "<tr>";
            //if (micerequestid > 0)
            htmlContent = htmlContent + "<td id='tdCmicerequestid" + (micerequestid > 0 ? micerequestid : value.id) + "' style='display:none;'>" + (micerequestid > 0 ? micerequestid : value.id) + "</td>";
            htmlContent = htmlContent + "<td>" + value.display_date_time + "</td>";
            htmlContent = htmlContent + "<td>" + (value.display_seating_type == null ? '' : value.display_seating_type) + "</td>";
            htmlContent = htmlContent + "<td>" + value.quantity + "</td>";
            htmlContent = htmlContent + "<td>" + value.roomsize + "</td>";
            htmlContent = htmlContent + "<td>" + value.display_budget + $rfp.variables.euroIcon + "</td>";
            htmlContent = htmlContent + "<td><a href='javascript:void(0)' class='lnkView' onClick='$rfp.getItemInformation(" + "\"" + resourceType + "\"" + "," + (micerequestid > 0 ? micerequestid : value.id) + ",1);'><i class='glyphicon glyphicon-list-alt'></i></a> <a href='javascript:void(0)' class='lnkEdit' onClick='$rfp.getItemInformation(" + "\"" + resourceType + "\"" + "," + (micerequestid > 0 ? micerequestid : value.id) + ",1);'><i class='glyphicon glyphicon-edit'></i></a> <a href='javascript:void(0)' class='lnkDelete' onClick='$rfp.deleteMiceRequestItem(" + (micerequestid > 0 ? micerequestid : value.id) + ",this);'><i class='glyphicon glyphicon-trash'></i></a> </td>";
            htmlContent = htmlContent + "</tr>";
        });
        if (resourceType === "H") {
            if (isSortBy == true) {
                $($rfp.controls.tbHotelConferenceRoom).empty();
            }
            $($rfp.controls.tbHotelConferenceRoom).append(htmlContent);
            if ($($rfp.controls.hdnIsViewMode).val() == "false") {
                $(".lnkView").hide();
            }
        } else if (resourceType === "R" || resourceType === 1) {
            if (isSortBy == true) {
                $($rfp.controls.tbRestaurantConferenceRoom).empty();
            }
            $($rfp.controls.tbRestaurantConferenceRoom).append(htmlContent);
            if ($($rfp.controls.hdnIsViewMode).val() == "false") {
                $(".lnkView").hide();
            }
        } else if (resourceType === "L" || resourceType === 2) {
            if (isSortBy == true) {
                $($rfp.controls.tbLocationConferenceRoom).empty();
            }
            $($rfp.controls.tbLocationConferenceRoom).append(htmlContent);
            if ($($rfp.controls.hdnIsViewMode).val() == "false") {
                $(".lnkView").hide();
            }
        }
    },
    fillGroupRoomRequestItems: function (resourceType, requestItems, micerequestid, isSortBy) {
        var htmlContent = "";
        $.each(requestItems, function (index, value) {
            htmlContent = htmlContent + "<tr>";
            //if (micerequestid > 0)
            htmlContent = htmlContent + "<td id='tdGmicerequestid" + (micerequestid > 0 ? micerequestid : value.id) + "' style='display:none;'>" + (micerequestid > 0 ? micerequestid : value.id) + "</td>";
            htmlContent = htmlContent + "<td>" + value.display_date_time + "</td>";
            htmlContent = htmlContent + "<td>" + value.quantity + "</td>";
            htmlContent = htmlContent + "<td>" + (value.display_seating_type == null ? '' : value.display_seating_type)  + "</td>";
            htmlContent = htmlContent + "<td>" + value.roomsize + "</td>";
            htmlContent = htmlContent + "<td>" + value.display_budget + $rfp.variables.euroIcon + "</td>";
            htmlContent = htmlContent + "<td> <a href='javascript:void(0)' class='lnkView' onClick='$rfp.getItemInformation(" + "\"" + resourceType + "\"" + "," + (micerequestid > 0 ? micerequestid : value.id) + ",2);'><i class='glyphicon glyphicon-list-alt'></i></a> <a href='javascript:void(0)' class='lnkEdit' onClick='$rfp.getItemInformation(" + "\"" + resourceType + "\"" + "," + (micerequestid > 0 ? micerequestid : value.id) + ",2);'><i class='glyphicon glyphicon-edit'></i></a> <a href='javascript:void(0)' class='lnkDelete' onClick='$rfp.deleteMiceRequestItem(" + (micerequestid > 0 ? micerequestid : value.id) + ",this);'><i class='glyphicon glyphicon-trash'></i></a> </td>";
            htmlContent = htmlContent + "</tr>";
        });
        if (resourceType === "H") {
            if (isSortBy == true) {
                $($rfp.controls.tbHotelGroupRoom).empty();
            }
            $($rfp.controls.tbHotelGroupRoom).append(htmlContent);
            if ($($rfp.controls.hdnIsViewMode).val() == "false") {
                $(".lnkView").hide();
            }
        } else if (resourceType === "R" || resourceType === 1) {
            if (isSortBy == true) {
                $($rfp.controls.tbRestaurantGroupRoom).empty();
            }
            $($rfp.controls.tbRestaurantGroupRoom).append(htmlContent);
            if ($($rfp.controls.hdnIsViewMode).val() == "false") {
                $(".lnkView").hide();
            }
        } else if (resourceType === "L" || resourceType === 2) {
            if (isSortBy == true) {
                $($rfp.controls.tbLocationGroupRoom).empty();
            }
            $($rfp.controls.tbLocationGroupRoom).append(htmlContent);
            if ($($rfp.controls.hdnIsViewMode).val() == "false") {
                $(".lnkView").hide();
            }
        }
    },

    getRequestForProposalById: function (id, strId) {
        var data = { id: id, strId: strId };
        if (data !== undefined && data !== null) {
            var url = $bind.URL($rfp.apiPath.GetRequestForProposalById);
            ajaxRequest.httpGetSyncWithData(url, data, $rfp.getRequestForProposalDetailOnSuccess, $rfp.getRequestForProposalDetailOnError);
        } else {
            common.hideLoader();
        }
    },
    GetURLParameter: function () {
        var sPageURL = window.location.href;
        var indexOfLastSlash = sPageURL.lastIndexOf("/");

        if (indexOfLastSlash > 0 && sPageURL.length - 1 != indexOfLastSlash)
            return sPageURL.substring(indexOfLastSlash + 1);
        else
            return 0;
    },

    formatSeatingType: function (option) {
        if (!option.id) { return option.text; }
        var imagepath = "";
        
        if (option.id == "u") {
            imagepath = "/images/" + $rfp.constant.UShapeImageName + ".png";
        }
        else if (option.id == "parliamentary") {
            imagepath = "/images/" + $rfp.constant.ParlamentryImageName + ".png";
        }
        else if (option.id == "no_seating") {
            imagepath = "/images/" + $rfp.constant.NoSeatingImageName + ".png";
        }
        else if (option.id == "banquet") {
            imagepath = "/images/" + $rfp.constant.BanquetImageName + ".png";
        }
        else if (option.id == "cabaret") {
            imagepath = "/images/" + $rfp.constant.CabaretImageName + ".png";
        }
        else if (option.id == "tables") {
            imagepath = "/images/" + $rfp.constant.TablesImageName + ".png";
        }
        else if (option.id == "block") {
            imagepath = "/images/" + $rfp.constant.BlockImageName + ".png";
        }
        else if (option.id == "rows") {
            imagepath = "/images/" + $rfp.constant.RowsImageName + ".png";
        }
        else if (option.id == "circle") {
            imagepath = "/images/" + $rfp.constant.CircleImageName + ".png";
        }
       

        var option = $(           
            '<span ><img sytle="display: inline-block;" src="' + imagepath + '" /> ' + option.text + '</span>'
        );
        return option;
    },
    ///INSPIRE-102
    ///When user click on startrfp, enter directly service detail and click on add button, then system store rfp detail through this function.
    saveStartRFP: function () {
        
        if ($($rfp.variables.RfpId).val() != '' && $($rfp.variables.RfpId).val()==0) {
            if ($rfp.validateRFP()) {
                var url = $bind.URL($rfp.apiPath.StartRFP);

                var minDate = $($rfp.controls.txtEventStartDate).datepicker('getDate');
                

                var maxDate = $($rfp.controls.txtEventEndDate).datepicker('getDate');

                if (maxDate != null && minDate!=null) {
                    var diff = maxDate - minDate;
                    var diffDays = Math.ceil(diff / (1000 * 3600 * 24));
                    $rfp.setDuration(diffDays);
                }

                var rfpModel = {
                    id: $($rfp.variables.RfpId).val(),
                    //rfp_type: $($rfp.controls.ddleventtype).val(),
                    rfp_type: 'meetings',
                    // days_running: $($rfp.controls.ddlduration).val(),
                    days_running: $($rfp.controls.hdnDuration).val(),
                    is_submitted: false,
                    event_name: $($rfp.controls.txtEventName).val(),
                    eventstartdate: common.getDateFromPicker($($rfp.controls.txtEventStartDate).val()),
                    eventenddate: common.getDateFromPicker($($rfp.controls.txtEventEndDate).val()),
                    micerequests: []
                };

                //common.displayLoade
                ajaxRequest.httpPostSync(url, JSON.stringify(rfpModel), $rfp.saveStartRFPOnSuccess, $rfp.saveStartRFPOnError);
                return true;
            }
            else {
                return false;
            }
        }
        return true;
    },
    saveStartRFPOnSuccess: function (data) {
        
        //var rfpId = data.data;
        //$($rfp.variables.RfpId).val(rfpId);
        
        var rfpDetail = data.data;      
        if (rfpDetail.event_name !== undefined && rfpDetail.event_name !== null) {
            $($rfp.controls.txtEventName).val(rfpDetail.event_name);
        }
        $($rfp.controls.hdnIsViewMode).val(rfpDetail.isviewmode);
        $($rfp.variables.RfpId).val(rfpDetail.id);
                     
        if (rfpDetail.micerequests !== null && rfpDetail.micerequests !== undefined) {
            $.each(rfpDetail.micerequests, function (index, value) {
                var resType = '';
                if (value.resource_type === 0) {
                    $($rfp.variables.HotelRequestId).val(value.id);
                    $rfp.getResourceDetail("H", value.rfpresources);
                    resType = "H";
                    // Set Comments
                    if (value.comments !== undefined && value.comments !== null) {
                        $($rfp.controls.txtHotelComments).val(value.comments);
                    }
                } else if (value.resource_type === 1) {
                    $($rfp.variables.RestaurantRequestId).val(value.id);
                    $rfp.getResourceDetail("R", value.rfpresources);
                    resType = "R";
                    // Set Comments
                    if (value.comments !== undefined && value.comments !== null) {
                        $($rfp.controls.txtRestaurantComments).val(value.comments);
                    }
                } else if (value.resource_type === 2) {
                    $($rfp.variables.LocationRequestId).val(value.id);
                    $rfp.getResourceDetail("L", value.rfpresources);
                    resType = "L";
                    // Set Comments
                    if (value.comments !== undefined && value.comments !== null) {
                        $($rfp.controls.txtLocationComments).val(value.comments);
                    }
                }
               
            });
        }


        return true;
    },
    saveStartRFPOnError: function (jqXHR, textStatus, errorThrown) { 
        $managecode.Decision(false, "", jqXHR.status, textStatus + ": " + errorThrown, "error", false);
        return false;
       // common.hideLoader();
    },
};


