﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class ConferenceRoomQuantities
    {
        [Key]
        public int Id { get; set; }
        public int BlockType { get; set; }
        public int UShapeType { get; set; }
        public string ParliamentaryType { get; set; }
        public string BanquetHallType { get; set; }
        public string TheaterType { get; set; }
        public string CabaretType { get; set; }
        [ForeignKey("ConferenceRoom")]
        public int ConferenceId { get; set; }

        public ConferenceRoom ConferenceRoom { get; set; }
    }
}
