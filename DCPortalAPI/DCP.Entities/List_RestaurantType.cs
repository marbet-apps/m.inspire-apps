﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class List_RestaurantType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string restaurant_name { get; set; }
        [Required]
        public string restaurant_name_de { get; set; }

        public ICollection<RestaurantType> RestaurantType { get; set; }
    }
}
