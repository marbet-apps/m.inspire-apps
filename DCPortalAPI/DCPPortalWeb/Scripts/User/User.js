﻿var $userCtrl = {
    controls: {
        txtFilterUser: "#txtFilterUser",
        hdnActive: "#hdnActive",
        hdnInActive: "#hdnInActive",
        tbUserInfo: "#tbUserInfo",
        btnSearch: "#btnSearch",
        addusermodelcontainer: "#addusermodelcontainer",
        ddlGender: "#ddlGender",
        txtFirstName: "#txtFirstName",
        txtLastName: "#txLastName",         
        txtCompany: "#txtCompany",
        txtDepartment: "#txtDepartment",
        txtStreet: "#txtStreet",
        txtPostCode: "#txtPostCode",
        txtPlace: "#txtPlace",
        ddlLand: "#ddlLand",
        txtEmail: "#txtEmail",
        txtTelephone: "#txtTelephone",
        txtTaxIdentifactionNo: "#txtTaxIdentifactionNo",
        chkIsActive: "#chkIsActive",
        hdnUserId: "#hdnUserId",
        btnAddUser: "#btnAddUser",
        s2idddlLand: '#s2id_ddlLand .select2-choice',
        lblUserModeltitle: "#lblUserModeltitle",       
        btnCloseModal: "#btnCloseModal",
        btnUpdate: "#btnUpdate",
        btnSubmit: "#btnSubmit",
        s2idddlGender: '#s2id_ddlGender .select2-choice'
        
    },
    localization: {
        hdnLandDesc: "#hdnLandDesc",
        hdnEditUserLabel: "#hdnEditUserLabel",
        hdnAddUserLabel: "#hdnAddUserLabel"
    },
    apiPath: {
        GetCountryList: "lookup/CountryList",
        GetUserList: "/user/GetUserList",
        DeleteUser: "/user/DeleteUser",
        GetUserDetail: "/user/GetUserDetail",
        UpdateUserDetailUrl: "/user/UpdateUserDetail",
        RegisterNewUser: "/User/RegisterNewUser",
        CheckAllowNewUser:"/User/CheckAllowNewUser"
    },    
    getUserList: function (sortby) {        
        $.ajax({
            type: "GET",
            url: $($c.controls.hdnAPIBaseURL).val() + this.apiPath.GetUserList,
            data: { search: $($userCtrl.controls.txtFilterUser).val(), sortBy: sortby },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {
                
                if (data != undefined && data != null && data.code=="704" && data.data != undefined && data.data != null) {
                    var userList = data.data;
                    var userRow = '';
                    if (userList.length > 0) {
                        $($userCtrl.controls.tbUserInfo).empty();
                        var status = '';
                        for (var i = 0; i < userList.length; i++) {   
                            status = userList[i].IsActive === true ? $($userCtrl.controls.hdnActive).val() : $($userCtrl.controls.hdnInActive).val();
                            userRow = "<tr>";
                            userRow += "<td>" + userList[i].First_Name + "</td>";
                            userRow += "<td>" + userList[i].Last_Name + "</td>";
                            userRow += "<td>" + userList[i].Salutation + "</td>";
                            userRow += "<td>" + userList[i].Email + "</td>";
                            userRow += "<td>" + userList[i].Company_Name + "</td>";
                            userRow += "<td>" + status  + "</td>";
                            userRow += "<td> <a href='javascript:void(0)' onClick='$userCtrl.getUserDetail(" + "\"" + userList[i].Userid + "\"" + ");'><i class='glyphicon glyphicon-edit'></i></a> <a href='javascript:void(0)' onClick='$userCtrl.deleteUserDetail(" + "\"" + userList[i].Userid + "\"" + ");'><i class='glyphicon glyphicon-trash'></i></a></td>";
                            $($userCtrl.controls.tbUserInfo).append(userRow);
                        }
                    }
                } else {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                }

            },
            failure: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    },
    deleteUserDetail: function (userId) {
        BootstrapDialog.confirm($($c.controls.sDeleteConfirm).val(), function (result) {
        if (result) {
            if (userId > 0) {
                
                $.ajax({
                    type: "POST",
                    url: $bind.URL($userCtrl.apiPath.DeleteUser) + "?userId=" + userId,                   
                    dataType : "json",
                    success: function (response) {
                        if (response != undefined && response != null && response.code == "710") {
                            $managecode.Decision(false, "", response.code, response.message, "Filter", true);
                           // $(e).parent('td').parent('tr').remove();
                            $userCtrl.getUserList("");
                        } else {
                            $managecode.Decision(false, "", response.code, response.message, "Filter", false);
                        }
                    },
                    error: function (data, status, error) {
                        $managecode.Decision(true, data, "", "", "error", false);
                    }
                });
            } else {
                common.hideLoader();
            }
        }
     });
    },
    getUserDetail: function (userId) {


        $.ajax({
            type: "GET",
            url: $bind.URL($userCtrl.apiPath.GetUserDetail) + "?userId=" + userId,
            dataType: "json",
            success: function (data) {
                
                $($userCtrl.controls.addusermodelcontainer).modal("show");
                $($userCtrl.controls.ddlGender).select2();
                if (data != undefined && data != null && data.data != undefined && data.data != null) {
                    var user = data.data;
                    $($userCtrl.controls.txtEmail).val(user.Email);
                    $($userCtrl.controls.ddlGender).select2("data", {
                        "id": user.Salutation,
                        "text": user.Salutation
                    });
                    $($userCtrl.controls.txtFirstName).val(user.First_Name);
                    $($userCtrl.controls.txtLastName).val(user.Last_Name);
                    $($userCtrl.controls.txtCompany).val(user.Company_Name);
                    $($userCtrl.controls.txtDepartment).val(user.Department_Name);
                    $($userCtrl.controls.txtStreet).val(user.Street);
                    $($userCtrl.controls.txtPostCode).val(user.PostCode);
                    $($userCtrl.controls.txtPlace).val(user.Place);
                    $($userCtrl.controls.ddlLand).select2("data", {
                        "id": user.Country_Id,
                        "text": user.CountryName
                    });

                    $($userCtrl.controls.txtTelephone).val(user.Telephone);
                    $($userCtrl.controls.txtTaxIdentifactionNo).val(user.Tax_Indentifier_No);

                    $($userCtrl.controls.chkIsActive).attr("checked", user.IsActive);
                    $($userCtrl.controls.hdnUserId).val(user.Userid);
                    $($userCtrl.controls.txtEmail).attr("readonly", "readonly");
                    $($userCtrl.controls.btnSubmit).hide();
                    $($userCtrl.controls.btnUpdate).show();
                   
                } else {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                }


                
            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    },

    UpdateUserDetail: function () {
        
        var model = {
            LogedInUserId: $($userCtrl.controls.hdnUserId).val(),
            Salutation: $($userCtrl.controls.ddlGender).val(),
            First_Name: $($userCtrl.controls.txtFirstName).val(),
            Last_Name: $($userCtrl.controls.txtLastName).val(),
            Company_Name: $($userCtrl.controls.txtCompany).val(),
            Department_Name: $($userCtrl.controls.txtDepartment).val(),
            Street: $($userCtrl.controls.txtStreet).val(),
            PostCode: $($userCtrl.controls.txtPostCode).val(),
            Place: $($userCtrl.controls.txtPlace).val(),
            Telephone: $($userCtrl.controls.txtTelephone).val(),
            Tax_Indentifier_No: $($userCtrl.controls.txtTaxIdentifactionNo).val(),
            Country_Id: $($userCtrl.controls.ddlLand).val(),
            Email: $($userCtrl.controls.txtEmail).val(),
            IsActive: $($userCtrl.controls.chkIsActive).is(":checked")
        };

        $.ajax({
            type: "PUT",
            url: $($c.controls.hdnAPIBaseURL).val() + this.apiPath.UpdateUserDetailUrl,
            data: JSON.stringify(model),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {

                if (data != undefined && data != null && data.data != undefined && data.data != null && data.data.isSuccess == true) {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                    $($userCtrl.controls.addusermodelcontainer).modal("hide");
                    $userCtrl.getUserList("");

                } else {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                }

            },
            failure: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    },
    SubmitUserDetail: function () {
        
        var model = {
            loggedInUserId: JSON.parse(localStorage.CurrentUser).LoggedInUserID,
            Salutation: $($userCtrl.controls.ddlGender).val(),
            First_Name: $($userCtrl.controls.txtFirstName).val(),
            Last_Name: $($userCtrl.controls.txtLastName).val(),
            Company_Name: $($userCtrl.controls.txtCompany).val(),
            Department_Name: $($userCtrl.controls.txtDepartment).val(),
            Street: $($userCtrl.controls.txtStreet).val(),
            PostCode: $($userCtrl.controls.txtPostCode).val(),
            Place: $($userCtrl.controls.txtPlace).val(),
            Telephone: $($userCtrl.controls.txtTelephone).val(),
            Tax_Indentifier_No: $($userCtrl.controls.txtTaxIdentifactionNo).val(),
            Country_Id: $($userCtrl.controls.ddlLand).val(),
            Email: $($userCtrl.controls.txtEmail).val(),
            IsActive: $($userCtrl.controls.chkIsActive).is(":checked"),
            isAdmin: false,
            Subscription_Type_Name: 3,
            IsRegisterFromManageUser:true
        };

        $.ajax({
            type: "POST",
            url: $($c.controls.hdnAPIBaseURL).val() + this.apiPath.RegisterNewUser,
            data: JSON.stringify(model),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {
                
                if (data != undefined && data != null && data.data != undefined && data.data != null && data.code == "701") {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                    $($userCtrl.controls.addusermodelcontainer).modal("hide");
                    $userCtrl.getUserList("");

                } else {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                }

            },
            failure: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

    },

    ValidateUserDetail: function () {
        var flag = true;
        var firstName = $($userCtrl.controls.txtFirstName).val();
        var lastName = $($userCtrl.controls.txtLastName).val();
        var land = $($userCtrl.controls.ddlLand).val();
        var redLand = $($userCtrl.controls.s2idddlLand);
        var email = $($userCtrl.controls.txtEmail).val();
        var gender = $($userCtrl.controls.ddlGender).val();
        var redGender = $($userCtrl.controls.s2idddlGender);


        if (firstName == null || firstName == undefined || firstName == "") {
            $($userCtrl.controls.txtFirstName).addClass("Error");
            flag = false;
        } else {
            $($userCtrl.controls.txtFirstName).removeClass("Error");
        }

        if (lastName == null || lastName == undefined || lastName == "") {
            $($userCtrl.controls.txtLastName).addClass("Error");
            flag = false;
        } else {
            $($userCtrl.controls.txtLastName).removeClass("Error");
        }

        if (land == null || land == undefined || land == "") {
            $($userCtrl.controls.s2idddlLand).css('cssText', 'border:1px solid red !important');
            flag = false;
        } else {

            $($userCtrl.controls.s2idddlLand).removeAttr("style");
        }

        if (gender == null || gender == undefined || gender == "") {
            $($userCtrl.controls.s2idddlGender).css('cssText', 'border: 1px solid red !important');
            flag = false;
        } else {

            $($userCtrl.controls.s2idddlGender).removeAttr("style");
        }



        if (email == null || email == undefined || email == "") {
            $($userCtrl.controls.txtEmail).addClass("Error");
            flag = false;
        } else {
            $($userCtrl.controls.txtEmail).removeClass("Error");
        }

        return flag;
    },
    checkAllowNewUser: function () {
        var flag = false;

        $.ajax({
            type: "GET",
            async:false,
            url: $bind.URL($userCtrl.apiPath.CheckAllowNewUser) + "?insertedRow=" + $("#tbUserInfo tr").length,            
            dataType: "json",
            success: function (data) {                
                if (data != undefined && data != null && data.data != undefined && data.data != null && data.code == "714") {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                    flag = false;
                } else if (data != undefined && data != null && data.data != undefined && data.data != null && data.code == "715") {
                    flag = true;
                    return flag;
                }
            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
                
            }
        });

        return flag;
    },
    openModalForAddUser: function () {    
            $($userCtrl.controls.addusermodelcontainer).modal("show");
            $($userCtrl.controls.ddlGender).select2();
            $($userCtrl.controls.hdnUserId).val("");
            $userCtrl.resetControl();

            $.ajax({
                type: "GET",
                url: $bind.URL($userCtrl.apiPath.GetUserDetail) + "?userId=" + JSON.parse(localStorage.CurrentUser).LoggedInUserID,
                dataType: "json",
                success: function (data) {

                    $($userCtrl.controls.addusermodelcontainer).modal("show");
                    $($userCtrl.controls.ddlGender).select2();
                    if (data != undefined && data != null && data.data != undefined && data.data != null) {
                        var user = data.data;
                   
                   
                        $($userCtrl.controls.txtCompany).val(user.Company_Name);
                        $($userCtrl.controls.txtDepartment).val(user.Department_Name);
                        $($userCtrl.controls.txtStreet).val(user.Street);
                        $($userCtrl.controls.txtPostCode).val(user.PostCode);
                        $($userCtrl.controls.txtPlace).val(user.Place);
                        $($userCtrl.controls.ddlLand).select2("data", {
                            "id": user.Country_Id,
                            "text": user.CountryName
                        });

                        $($userCtrl.controls.txtTelephone).val(user.Telephone);
                        $($userCtrl.controls.txtTaxIdentifactionNo).val(user.Tax_Indentifier_No);

                        $($userCtrl.controls.chkIsActive).attr("checked", user.IsActive);
                    
                   

                    } else {
                        $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                    }



                },
                error: function (data, status, error) {
                    $managecode.Decision(true, data, "", "", "error", false);
                }
                });
        

    },
    closeModal: function () {
        $($userCtrl.controls.addusermodelcontainer).modal("hide");       
        $($userCtrl.controls.hdnUserId).val("");
    },
    resetControl: function () {
        $($userCtrl.controls.txtEmail).val('');
        $($userCtrl.controls.ddlGender).select2("data", {
            "id": "",
            "text": ""
        });
        $($userCtrl.controls.ddlGender).select2({
            placeholder: "change your placeholder" 
        });
       
        $($userCtrl.controls.txtFirstName).val("");
        $($userCtrl.controls.txtLastName).val("");
        $($userCtrl.controls.txtCompany).val("");
        $($userCtrl.controls.txtDepartment).val("");
        $($userCtrl.controls.txtStreet).val("");
        $($userCtrl.controls.txtPostCode).val("");
        $($userCtrl.controls.txtPlace).val("");
        $($userCtrl.controls.ddlLand).select2("val", "");
        //$($userCtrl.controls.ddlLand).select2("data", {
        //    "id": "",
        //    "text": ""
        //});

        $($userCtrl.controls.txtTelephone).val("");
        $($userCtrl.controls.txtTaxIdentifactionNo).val("");

        $($userCtrl.controls.chkIsActive).attr("checked", false);
        $($userCtrl.controls.hdnUserId).val("");
        $($userCtrl.controls.txtEmail).removeAttr("readonly");
    }


};

$(document).ready(function () {
    $userCtrl.getUserList("");
    $LookUp.BindDropdown($userCtrl.controls.ddlLand, $($userCtrl.localization.hdnLandDesc).val(), $userCtrl.apiPath.GetCountryList, 0, 50, false, true);
    $($userCtrl.controls.btnSearch).click(function () {
        $userCtrl.getUserList("");
    });

    $($userCtrl.controls.btnUpdate).click(function () {
        if ($userCtrl.ValidateUserDetail() == true) {
            $userCtrl.UpdateUserDetail();
        }           
    });

    $($userCtrl.controls.btnSubmit).click(function () {
        if ($userCtrl.ValidateUserDetail() == true) {
            $userCtrl.SubmitUserDetail();
        }
    });

    $($userCtrl.controls.btnAddUser).click(function () {
        
        if ($userCtrl.checkAllowNewUser() == true) {
            $userCtrl.openModalForAddUser();
            $($userCtrl.controls.btnSubmit).show();
            $($userCtrl.controls.btnUpdate).hide();
        }

       
    });

    var $sortable = $('.sortable');
    $sortable.on('click', function () {

        var $this = $(this);
        var asc = $this.hasClass('asc');
        var desc = $this.hasClass('desc');
        $sortable.removeClass('asc').removeClass('desc');
        if (desc || (!asc && !desc)) {
            $this.addClass('asc');
        } else {
            $this.addClass('desc');
        }
        var tableid = $('#' + $(this).closest('th')[0].id).closest('table')[0].id;
       
            $userCtrl.getUserList($(this).closest('th')[0].id + ($(this).hasClass('asc') == true ? 'asc' : 'desc'))
        
        
    });

});
