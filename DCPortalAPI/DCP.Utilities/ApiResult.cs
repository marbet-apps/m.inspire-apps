﻿using DCP.Utilities;
using Newtonsoft.Json;
using System;

namespace DCP.Entities
{
    public class ResultSet
    {
        /// <summary>
        /// Success Responce
        /// </summary>
        /// <param name="code"></param>
        /// <param name="data"></param>
        /// <param name="status"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ApiResult SuccessResponse(string code, object data, string status, string message)
        {
            ApiResult ApiResult = new ApiResult();

            try
            {
                ApiResult.Code = code;
                ApiResult.Data = data;
                ApiResult.Status = status;
                ApiResult.Message = message;

                return ApiResult;

            }
            finally
            {
                ApiResult = null;
                WriteLog.LogInformation(" " + status + " || " + code + " || " + message);
            }

        }

        /// <summary>
        /// Error Responce
        /// </summary>
        /// <param name="code"></param>
        /// <param name="data"></param>
        /// <param name="status"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ApiResult ErrorResponse(string code, Exception ex)
        {
            ApiResult ApiResult = new ApiResult();

            try
            {
                ApiResult.Code = code;
                ApiResult.Data = "";
                ApiResult.Status = ManageEnum.status.error.ToString();
                ApiResult.Message = ex.Message;

                return ApiResult;
            }
            finally
            {
                ApiResult = null;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                WriteLog.LogException(ex.Message, ex);
            }

        }
    }

    public class ApiResult
    {
        /// <summary>
        /// Represents Http Status code like 200,401
        /// </summary>
        [JsonProperty(Order = 1, PropertyName = "code")]
        public string Code { get; set; }

        /// <summary>
        /// Data output
        /// </summary>
        [JsonProperty(Order = 5, PropertyName = "data", NullValueHandling = NullValueHandling.Ignore)]
        public object Data { get; set; }

        /// <summary>
        /// Success
        /// </summary>
        [JsonProperty(Order = 2, PropertyName = "status")]
        public string Status { get; set; }

        /// <summary>
        /// Message will be filled incase of Error/Fail
        /// </summary>
        [JsonProperty(Order = 3, PropertyName = "message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }
    }

}
