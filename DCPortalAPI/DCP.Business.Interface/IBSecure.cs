﻿using DCP.Entities;
using DCP.Models;
namespace DCP.Business.Interface
{
    public interface IBSecure
    {
        /// <summary>
        /// Check User Auth
        /// </summary>
        ApiResult CheckUserAuth(string username, string password,out bool flag);

        ApiResult CheckAdminUserAuth(string username, string password, int usertype, out bool flag);

        ApiResult RegisterUser(UserRegisterModel userRegisterModel, out bool flag);

        ApiResult ForgotUserPassword(UserPasswordModel userPasswordModel, out bool flag, out string name, string token);

        ApiResult GetAllUser(string activestatus,string email, int page = 1, int pageSize = 50);

        ApiResult GetUserDetail(int userID, string lang);

        ApiResult RemoveUser(int userID);

        ApiResult ChangePassword(ChangePasswordModel userModel);

        ApiResult UpdateUser(int userID, int loggedInUserID, UpdateUserModel updateUserModel);

        ApiResult UpdateUseProfile(UpdateProfileModel model);

        ApiResult UploadImage(byte[] bytes, int userId);

        ApiResult DeleteImage(int userId);

        ApiResult ResetPassword(ResetPasswordModel userModel);

        ApiResult ValidateUser(ResetPasswordModel userModel);
    }

}
