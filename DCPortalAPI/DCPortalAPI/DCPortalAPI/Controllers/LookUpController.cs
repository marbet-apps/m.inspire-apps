﻿using DCP.Business.Interface;
using DCP.Models;
using DCP.Utilities;
using DCPortalAPI.Settings;
using DCPortalAPI.Filter;
using System.Web.Http;
using DCP.Entities;

namespace DCPortalAPI.Controllers
{
    [RoutePrefix("api/LookUp")]
    [Authorize]
    [ValidateModel]
    [ExceptionHandler]
    public class LookUpController : ApiController
    {
        private IBLookUp _IBLookUp;

        public LookUpController(IBLookUp IBLookUp)
        {
            this._IBLookUp = IBLookUp;
        }

        /// <summary>
        /// Get Country List
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("CountryList")]       
        public IHttpActionResult GetCountryList(int page, int pageSize, int type, string term = "")
        {           

            WriteLog.LogInformation(LogMessages.CountryListStarted);

            try
            {               

                var Response = _IBLookUp.GetCountryList(CommonMethod.GetHeaders(this.Request.Headers, "Lang"), term, page, pageSize, type);

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.CountryListEnded);
            }
        }

        /// <summary>
        /// Get Radius List
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("RadiusList")]
        public IHttpActionResult GetRadiusList(int page, int pageSize, int type, string term = "")
        {
            WriteLog.LogInformation(LogMessages.RadiusListStarted);

            try
            {                

                var Response = _IBLookUp.GetRadiusList(CommonMethod.GetHeaders(this.Request.Headers, "Lang"), term, page, pageSize, type);

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RadiusListEnded);
            }
        }

        /// <summary>
        /// Get Chain List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("ChainList")]
        public IHttpActionResult GetChainList(int page, int pageSize, int type, string term = "")
        {
            WriteLog.LogInformation(LogMessages.HotelChainListStarted);

            try
            {
                var Response = _IBLookUp.GetChainList(term, page, pageSize, type);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.HotelChainListEnded);

            }
        }

        /// <summary>
        /// Get Chain List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetResourceList")]
        public IHttpActionResult GetResourceList(int page, int pageSize, int type, string term = "")
        {
            WriteLog.LogInformation(LogMessages.HotelChainListStarted);

            try
            {
                ApiResult Response = new ApiResult();
                if (type == 0)
                {
                    Response = _IBLookUp.GetHotelList(term, page, pageSize, type);
                }
                 else if (type == 1)
                {
                    Response = _IBLookUp.GetRestaurantList(term, page, pageSize, type);
                }
                else if (type == 2)
                {
                    Response = _IBLookUp.GetLocationList(term, page, pageSize, type);
                }
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.HotelChainListEnded);

            }
        }

        /// <summary>
        /// Get Hotel Type List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("HotelTypeList")]
        public IHttpActionResult GetHotelTypeList(int page, int pageSize, int type, string term = "")
        {
            WriteLog.LogInformation(LogMessages.HotelTypeListStarted);

            try
            {
                var Response = _IBLookUp.GetHotelTypeList(CommonMethod.GetHeaders(this.Request.Headers, "Lang"), term, page, pageSize, type);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.HotelTypeListEnded);

            }
        }

        /// <summary>
        /// Get Restaurant Type List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("RestaurantTypeList")]
        public IHttpActionResult GetRestaurantTypeList(int page, int pageSize, int type, string term = "")
        {
            WriteLog.LogInformation(LogMessages.RestaurantTypeListStarted);

            try
            {
                var Response = _IBLookUp.GetRestaurantTypeList(CommonMethod.GetHeaders(this.Request.Headers, "Lang"), term, page, pageSize, type);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RestaurantTypeListEnded);

            }
        }

        /// <summary>
        /// Get Location Type List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("LocationTypeList")]
        public IHttpActionResult GetLocationTypeList(int page, int pageSize, int type, string term = "")
        {
            WriteLog.LogInformation(LogMessages.LocationTypeListStarted);

            try
            {
                var Response = _IBLookUp.GetLocationTypeList(CommonMethod.GetHeaders(this.Request.Headers, "Lang"), term, page, pageSize, type);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.LocationTypeListEnded);

            }
        }

        /// <summary>
        /// Get Kitchen Type List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("KitchenList")]
        public IHttpActionResult GetKitchenList(int page, int pageSize, int type, string term = "")
        {
            WriteLog.LogInformation(LogMessages.KitchenListStarted);

            try
            {
                var Response = _IBLookUp.GetKitchenList(CommonMethod.GetHeaders(this.Request.Headers, "Lang"), term, page, pageSize, type);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.KitchenListEnded);

            }
        }

        /// <summary>
        /// Get Suitable For Type List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("SuitableForList")]
        public IHttpActionResult GetSuitableForList(int page, int pageSize, int type, string term = "")
        {
            WriteLog.LogInformation(LogMessages.SuitableForListStarted);

            try
            {
                var Response = _IBLookUp.GetSuitableForList(CommonMethod.GetHeaders(this.Request.Headers, "Lang"), term, page, pageSize, type);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.SuitableForListEnded);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("SubscriptionType")]
        [Authorize]
        public IHttpActionResult GetSubscriptionList()
        {
            WriteLog.LogInformation(LogMessages.SubscriptionTypeStarted);

            try
            {
                var Response = _IBLookUp.GetSubscriptionTypeList(CommonMethod.GetHeaders(this.Request.Headers, "Lang"));
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.SubscriptionTypeEnded);
            }
        }
        /// <summary>
        /// Retrive Request status.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("RequestStatusList")]
        public IHttpActionResult GetRequestStatusList()
        {
            WriteLog.LogInformation(LogMessages.GetRequestStatusListStarted);
            try
            {
                var Response = _IBLookUp.GetRequestStatusList(CommonMethod.GetHeaders(this.Request.Headers, "Lang"));
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRequestStatusListEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("RequestTypes")]
        public IHttpActionResult GetRequestTypes()
        {
            WriteLog.LogInformation(LogMessages.GetRequestTypesStarted);
            try
            {
                var Response = _IBLookUp.GetRequestTypes(CommonMethod.GetLanguageHeader(this.Request.Headers));
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRequestTypesEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("RequestItemTypes")]
        public IHttpActionResult GetRequestItemTypes()
        {
            WriteLog.LogInformation(LogMessages.GetRequestItemTypesStarted);
            try
            {
                var Response = _IBLookUp.GetRequestItemTypes(CommonMethod.GetLanguageHeader(this.Request.Headers));
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRequestItemTypesEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("RoomTypes")]
        public IHttpActionResult GetRoomTypes()
        {
            WriteLog.LogInformation(LogMessages.GetRoomTypesStarted);
            try
            {
                var Response = _IBLookUp.GetRoomTypes(CommonMethod.GetLanguageHeader(this.Request.Headers));
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRoomTypesEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("RoomTechniques")]
        public IHttpActionResult GetRoomTechniques()
        {
            WriteLog.LogInformation(LogMessages.GetRoomTechniquesStarted);
            try
            {
                var Response = _IBLookUp.GetRoomTechniques(CommonMethod.GetLanguageHeader(this.Request.Headers));
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRoomTechniquesEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("SeatingTypes")]
        public IHttpActionResult GetSeatingTypes()
        {
            WriteLog.LogInformation(LogMessages.GetSeatingTypesStarted);
            try
            {
                var Response = _IBLookUp.GetSeatingTypes(CommonMethod.GetLanguageHeader(this.Request.Headers));
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetSeatingTypesEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("DaysRunnings")]
        public IHttpActionResult GetDaysRunning()
        {
            WriteLog.LogInformation(LogMessages.GetDaysRunningStarted);
            try
            {
                var Response = _IBLookUp.GetDaysRunning(CommonMethod.GetLanguageHeader(this.Request.Headers));
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetDaysRunningEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("TimeIntervals")]
        public IHttpActionResult GetTimeIntervals(int intervals)
        {
            WriteLog.LogInformation(LogMessages.GetTimeIntervalsStarted);
            try
            {
                var Response = _IBLookUp.GetTimeIntervals(intervals);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetTimeIntervalsEnded);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("CityList")]
        public IHttpActionResult GetCityList()
        {
            WriteLog.LogInformation(LogMessages.GetCityListStarted);
            try
            {
                var Response = _IBLookUp.GetCityList();
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetCityListEnded);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("AbortReason")]
        public IHttpActionResult GetAbortReason()
        {
            WriteLog.LogInformation(LogMessages.GetAbortReasonStarted);
            try
            {
                var Response = _IBLookUp.GetAbortReason(CommonMethod.GetLanguageHeader(this.Request.Headers));
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetAbortReasonEnded);
            }
        }
    }
}