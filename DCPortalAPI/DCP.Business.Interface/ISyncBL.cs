﻿using DCP.Entities;
using DCP.Models;
using System.Collections.Generic;

namespace DCP.Business.Interface
{
    public interface ISyncBL
    {
        ApiResult SyncHotels(MigrationHotelModel migrationModel);
        ApiResult SyncLocations(MigrationLocationModel migrationModel);
        ApiResult SyncRestaurants(MigrationResaurantModel migrationModel);

        ApiResult MigrateAirport(List<AirportModel> sourceTable);

        ApiResult MigrateCountry(List<CountryModel> sourceTable);

        ApiResult MigrateHotelType(List<HotelTypeModel> sourceTable);

        ApiResult MigrateRestaurantType(List<RestaurantTypeModel> sourceTable);

        ApiResult MigrateLocationType(List<LocationTypeModel> sourceTable);

        ApiResult MigrateKitchen(List<KitchenModel> sourceTable);

        ApiResult MigrateSuitablefor(List<SuitableForModel> sourceTable);

        ApiResult DeleteResource(List<KbStatusModel> statusModels);

    }
}
