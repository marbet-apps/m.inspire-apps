﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class Restaurants
    {
        [Key]
        public int dcp_restaurantinfo_id { get; set; }
        [Required, Column(TypeName = "varchar")]
        public string mice_id { get; set; }
        public string restaurant_name { get; set; }
        [ForeignKey("Country")]
        public int country_id { get; set; }
        [Required]
        public int kb_id { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string zipcode { get; set; }
        public string webaddress { get; set; }

        public bool restaurant_in_hotel { get; set; }
        public string hotelname { get; set; }

        public int? capacity { get; set; }
        public int? total_conference { get; set; }
        public double? size_of_largest_conference { get; set; }
        public int? capacity_of_largest_room { get; set; }
        public bool? outdoor_possibility { get; set; }

        public string outdoor_details { get; set; }
        public double? minimum_turnover { get; set; }
        public string miscellaneous_info { get; set; }

        public bool? PP { get; set; }
        public bool? green { get; set; }
        public bool? reopening { get; set; }
        public bool? blacked { get; set; }
        public bool? closed { get; set; }

        public double? restaurant_lan { get; set; }
        public double? restaurant_lat { get; set; }
        public bool? is360degreeview { get; set; }
        public int created_by { get; set; }
        public DateTime created_date { get; set; }
        public int modify_by { get; set; }
        public DateTime modify_date { get; set; }

        public List_Country Country { get; set; }

        public ICollection<RestaurantType> RestaurantType { get; set; }
        public ICollection<KitchenType> KitchenType { get; set; }
    }
}
