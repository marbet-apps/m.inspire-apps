﻿using System.Collections.Generic;

namespace DCP.Models.RFP
{
    public class ConferenceFoodGroupModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public List<LookupString> ConferenceFoods { get; set; }
    }
}
