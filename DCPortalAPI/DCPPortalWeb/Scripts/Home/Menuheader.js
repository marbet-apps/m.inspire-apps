﻿
var $mh = {

    Inactiveimg: function (aid) {

        if (aid == "Hotels") {
            $($c.controls.ihotel).attr('src', '/Images/HotelB.png');
            $($c.controls.irestaurant).attr('src', '/Images/Restaurant.png');
            $($c.controls.ilocation).attr('src', '/Images/Location.png');
        }

        if (aid == "Restaurants") {
            $($c.controls.ihotel).attr('src', '/Images/Hotel.png');
            $($c.controls.irestaurant).attr('src', '/Images/RestaurantB.png');
            $($c.controls.ilocation).attr('src', '/Images/Location.png');
        }

        if (aid == "Locations") {
            $($c.controls.ihotel).attr('src', '/Images/Hotel.png');
            $($c.controls.irestaurant).attr('src', '/Images/Restaurant.png');
            $($c.controls.ilocation).attr('src', '/Images/LocationB.png');
        }
    }
}

$(document).ready(function () {

    //$('.nav-tabs > li a[title]').tooltip();

    $('.show-tooltip').tooltip({ placement: 'bottom' });
    $('.show-tooltipl').tooltip({ placement: 'left' });
    $('.show-tooltipt').tooltip({ placement: 'top' });

    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        $mh.Inactiveimg($target.attr('aria-controls'));

    });

    $('.accordion-header').toggleClass('inactive-header');

    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({});

    $('.accordion-header').first().toggleClass('active-header').toggleClass('inactive-header');
    $('.accordion-content').first().slideDown().toggleClass('open-content');

    $('.accordion-header').click(function () {

        if ($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }

        else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }

    });

    return false;
});