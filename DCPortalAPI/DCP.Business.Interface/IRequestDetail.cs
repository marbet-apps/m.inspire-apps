﻿using System;
using DCP.Entities;
using DCP.Models.RFP;
namespace DCP.Business.Interface
{
    public interface IRequestDetail
    {
        /// <summary>
        /// Retrieve RFP List
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="term"></param>
        /// <param name="status"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        ApiResult GetRfpList(int page, int pageSize, string status, string lang,string term, int? userId=0);
        /// <summary>
        /// Get RFP Details.
        /// </summary>
        /// <param name="rfPID"></param>
        /// <returns></returns>
        ApiResult GetRFPDetail(int rfPID, int? userId = 0, string type = "", string lang = "");

        /// <summary>
        /// Get RFPMiceRequest with Proposal
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        ApiResult GetRFPMiceRequestWithProposal(int page, int pageSize, int? userId, string sortby, string search);
    }
}
