﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using DCP.Mice.Models;
using DCP.Mice.Models.Common;
using Newtonsoft.Json;
using DCP.Mice.Models.Common.Json;
using System.Net;

namespace DCP.MiceAPI
{
    public class MiceRequestAPI
    {
        #region variables & properties

        private byte[] _authenticationHeaders;

        #endregion variables & properties

        #region Constructor

        public MiceRequestAPI(string agencyName, string password)
        {
            if (!string.IsNullOrWhiteSpace(agencyName) && !string.IsNullOrWhiteSpace(password))
            {
                _authenticationHeaders = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", agencyName, password));
            }
            else
            {
                Logger.Error("Agency Name Or Password is missing");
                throw new Exception("Agency Name Or Password is missing");
            }
        }

        #endregion Constructor

        #region Request APIs

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ResponseRequestConfirmationModel CreateNewRequest(RequestModel requestModel)
        {
            try
            {
                var responseRequestConfirmationModel = new ResponseRequestConfirmationModel();
                var client = GetHttpClient();
                var jsonResolver = new PropertyIgnoreSerializerContractResolver();
                if (requestModel.UserId > 0)
                {
                    jsonResolver.IgnoreProperty(typeof(RequestModel), "customer");
                }
                else
                {
                    jsonResolver.IgnoreProperty(typeof(RequestModel), "userid");
                }

                var serializerSettings = new JsonSerializerSettings();
                serializerSettings.ContractResolver = jsonResolver;

                var requestParameter = JsonConvert.SerializeObject(requestModel, serializerSettings);

                Logger.Info(string.Format("Mice API - CreateNewRequest Parameter:{0} - {1}", MiceConstants.APIConstants.APIURL + "request", requestParameter));

                var requestResponse = client.PutAsync(MiceConstants.APIConstants.APIURL + "request", GetHttpContent(requestParameter)).Result;

                Logger.Info(string.Format("Mice API - CreateNewRequest Request Response:{0}", requestResponse));

                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - CreateNewRequest Json Response:{0}", jsonResponse.Result));

                    responseRequestConfirmationModel = JsonConvert.DeserializeObject<ResponseRequestConfirmationModel>(jsonResponse.Result);
                }
                return responseRequestConfirmationModel;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - CreateNewRequest Exception:", ex);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public OfferModel GetRequestDetail(int requestId)
        {
            try
            {
                var offerModel = new OfferModel();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(_authenticationHeaders));
                Logger.Info(string.Format("Mice API - GetRequestDetail Parameter:{0}{1}", MiceConstants.APIConstants.APIURL + "requests/", requestId));
                var requestResponse = client.GetAsync(MiceConstants.APIConstants.APIURL + "requests/" + requestId).Result;
                Logger.Info(string.Format("Mice API - GetRequestDetail Request Response:{0}", requestResponse));
                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - GetRequestDetail Json Response:{0}", jsonResponse.Result));
                    offerModel = JsonConvert.DeserializeObject<OfferModel>(jsonResponse.Result);
                }
                return offerModel;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - GetRequestDetail Exception:", ex);
            }
            return null;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<int> GetAllRequests()
        {
            try
            {
                var requestIds = new List<int>();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));
                Logger.Info(string.Format("Mice API - GetAllRequests Parameter:{0}", MiceConstants.APIConstants.APIURL + "requests"));
                var requestResponse = client.GetAsync(MiceConstants.APIConstants.APIURL + "requests").Result;
                Logger.Info(string.Format("Mice API - GetAllRequests Request Response:{0}", requestResponse));
                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - GetAllRequests Json Response:{0}", jsonResponse.Result));
                    requestIds = JsonConvert.DeserializeObject<List<int>>(jsonResponse.Result);
                }
                return requestIds;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - GetAllRequests Exception:", ex);
            }
            return null;
        }

        #endregion Request APIs

        #region Order APIs

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingModel"></param>
        /// <returns></returns>
        public OrderResponseModel BookedOrder(OrderBookingModel bookingModel)
        {
            try
            {
                var orderResponseModel = new OrderResponseModel();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));

                var requestParameter = JsonConvert.SerializeObject(bookingModel);
                Logger.Info(string.Format("Mice API - BookedOrder Parameter:{0} - {1}", MiceConstants.APIConstants.APIURL + "order", requestParameter));
                var requestResponse = client.PutAsync(MiceConstants.APIConstants.APIURL + "order", GetHttpContent(requestParameter)).Result;
                Logger.Info(string.Format("Mice API - BookedOrder Request Response:{0}", requestResponse));
                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - BookedOrder Json Response:{0}", jsonResponse));
                    orderResponseModel.Id = jsonResponse.Id;
                    orderResponseModel.Status_code = "OK";
                    orderResponseModel.Status = jsonResponse.Status.ToString();
                    // orderResponseModel = JsonConvert.DeserializeObject<OrderResponseModel>(jsonResponse);
                }
                return orderResponseModel;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - BookedOrder Exception:", ex);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="abortModel"></param>
        /// <returns></returns>
        public OrderResponseModel AbortOrder(OrderAbortModel abortModel)
        {
            try
            {
                var orderResponseModel = new OrderResponseModel();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));

                var requestParameter = JsonConvert.SerializeObject(abortModel);
                Logger.Info(string.Format("Mice API - AbortOrder Parameter:{0} - {1}", MiceConstants.APIConstants.APIURL + "order", requestParameter));
                var requestResponse = client.PutAsync(MiceConstants.APIConstants.APIURL + "order", GetHttpContent(requestParameter)).Result;
                Logger.Info(string.Format("Mice API - AbortOrder Request Response:{0}", requestResponse));
                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - AbortOrder Json Response:{0}", jsonResponse.Result));
                    orderResponseModel = JsonConvert.DeserializeObject<OrderResponseModel>(jsonResponse.Result);
                }
                return orderResponseModel;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - AbortOrder Exception:", ex);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="extendModel"></param>
        /// <returns></returns>
        public OrderResponseModel ExtendOrder(OrderExtendModel extendModel)
        {
            try
            {
                var orderResponseModel = new OrderResponseModel();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));

                var requestParameter = JsonConvert.SerializeObject(extendModel);
                Logger.Info(string.Format("Mice API - ExtendOrder Parameter:{0} - {1}", MiceConstants.APIConstants.APIURL + "order", requestParameter));
                var requestResponse = client.PutAsync(MiceConstants.APIConstants.APIURL + "order", GetHttpContent(requestParameter)).Result;
                Logger.Info(string.Format("Mice API - ExtendOrder Request Response:{0}", requestResponse));
                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - ExtendOrder Json Response:{0}", jsonResponse.Result));
                    orderResponseModel = JsonConvert.DeserializeObject<OrderResponseModel>(jsonResponse.Result);
                }
                return orderResponseModel;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - ExtendOrder Exception:", ex);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public OrderDetailModel GetOrderDetail(int orderId)
        {
            try
            {
                var orderDetailModel = new OrderDetailModel();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));
                Logger.Info(string.Format("Mice API - GetOrderDetail Parameter:{0}{1}", MiceConstants.APIConstants.APIURL + "orders/", orderId));
                var requestResponse = client.GetAsync(MiceConstants.APIConstants.APIURL + "orders/" + orderId).Result;
                Logger.Info(string.Format("Mice API - GetOrderDetail Request Response:{0}", requestResponse));
                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - GetOrderDetail Json Response:{0}", jsonResponse.Result));
                    orderDetailModel = JsonConvert.DeserializeObject<OrderDetailModel>(jsonResponse.Result);
                }
                return orderDetailModel;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - GetOrderDetail Exception:", ex);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<int> GetAllOrders()
        {
            try
            {
                var orderIds = new List<int>();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));
                Logger.Info(string.Format("Mice API - GetAllOrders Parameter:{0}", MiceConstants.APIConstants.APIURL + "orders"));
                var requestResponse = client.GetAsync(MiceConstants.APIConstants.APIURL + "orders").Result;
                Logger.Info(string.Format("Mice API - GetAllOrders Request Response:{0}", requestResponse));
                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - GetAllOrders Json Response:{0}", jsonResponse.Result));
                    orderIds = JsonConvert.DeserializeObject<List<int>>(jsonResponse.Result);
                }
                return orderIds;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - GetAllOrders Exception:", ex);
            }
            return null;
        }


        #endregion Order APIs

        #region Hotel APIs

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        public HotelDetailModel GetHotelDetail(int hotelId)
        {
            try
            {
                var hotelDetailModel = new HotelDetailModel();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));
                Logger.Info(string.Format("Mice API - GetHotelDetail Parameter:{0}{1}", MiceConstants.APIConstants.APIURL + "hotels/", hotelId));
                var requestResponse = client.GetAsync(MiceConstants.APIConstants.APIURL + "hotels/" + hotelId).Result;
                Logger.Info(string.Format("Mice API - GetHotelDetail Request Response:{0}", requestResponse));

                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - GetHotelDetail Json Response:{0}", jsonResponse.Result));
                    hotelDetailModel = JsonConvert.DeserializeObject<HotelDetailModel>(jsonResponse.Result);
                }
                return hotelDetailModel;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - GetHotelDetail Exception:", ex);
            }
            return null;
        }

        public List<int> GetAllHotels()
        {
            try
            {
                var hotelIds = new List<int>();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));
                Logger.Info(string.Format("Mice API - GetAllHotels Parameter:{0}", MiceConstants.APIConstants.APIURL + "hotels"));
                var requestResponse = client.GetAsync(MiceConstants.APIConstants.APIURL + "hotels").Result;
                Logger.Info(string.Format("Mice API - GetAllHotels Request Response:{0}", requestResponse));
                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - GetAllHotels Json Response:{0}", jsonResponse.Result));
                    hotelIds = JsonConvert.DeserializeObject<List<int>>(jsonResponse.Result);
                }
                return hotelIds;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - GetAllHotels Exception:", ex);
            }
            return null;
        }

        #endregion Hotel APIs

        #region Customer APIs

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public CustomerDetailModel GetCustomerDetail(int customerId)
        {
            try
            {
                var customerDetailModel = new CustomerDetailModel();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));
                Logger.Info(string.Format("Mice API - GetCustomerDetail Parameter:{0}{1}", MiceConstants.APIConstants.APIURL + "customers/", customerId));
                var requestResponse = client.GetAsync(MiceConstants.APIConstants.APIURL + "customers/" + customerId).Result;
                Logger.Info(string.Format("Mice API - GetCustomerDetail Request Response:{0}", requestResponse));
                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - GetCustomerDetail Json Response:{0}", jsonResponse.Result));
                    customerDetailModel = JsonConvert.DeserializeObject<CustomerDetailModel>(jsonResponse.Result);
                }
                return customerDetailModel;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - GetCustomerDetail Exception:", ex);
            }
            return null;

        }

        public List<int> GetAllCustomers()
        {
            try
            {
                var customerIds = new List<int>();
                var client = GetHttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));
                Logger.Info(string.Format("Mice API - GetAllCustomers Parameter:{0}", MiceConstants.APIConstants.APIURL + "customers"));
                var requestResponse = client.GetAsync(MiceConstants.APIConstants.APIURL + "customers").Result;
                Logger.Info(string.Format("Mice API - GetAllCustomers Request Response:{0}", requestResponse));

                if (requestResponse.IsSuccessStatusCode)
                {
                    var jsonResponse = requestResponse.Content.ReadAsStringAsync();
                    Logger.Info(string.Format("Mice API - GetAllCustomers Json Response:{0}", jsonResponse.Result));

                    customerIds = JsonConvert.DeserializeObject<List<int>>(jsonResponse.Result);
                }
                return customerIds;
            }
            catch (Exception ex)
            {
                Logger.Error("Mice API - GetAllCustomers Exception:", ex);
            }
            return null;
        }

        #endregion Customer APIs

        #region Methods

        private HttpContent GetHttpContent(string json)
        {
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private HttpClient GetHttpClient()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                       Convert.ToBase64String(_authenticationHeaders));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        #endregion Methods

    }
}
