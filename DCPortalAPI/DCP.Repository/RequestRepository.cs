﻿using DCP.Entities;
using DCP.Mice.Models;
using DCP.MiceAPI;
using DCP.Models;
using DCP.Models.RFP;
using DCP.Repository.Interface;
using DCP.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using static DCP.Utilities.ManageConstants;

namespace DCP.Repository
{
    public class RequestRepository : IRequestRepository
    {
        private DCPDbContext _context = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public RequestRepository()
        {
            this._context = new DCPDbContext();
        }

        #region Cart

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cartModel"></param>
        /// <returns></returns>
        public int MarkResourceForTheRequest(RequestCartModel cartModel)
        {
            var cartId = 0;

            if (cartModel != null)
            {
                if (cartModel.UserId > 0)
                {
                    var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == cartModel.UserId);

                    if (cart != null)
                    {
                        cartId = cart.Id;
                        RequestCartResource cartResource = new RequestCartResource();
                        cartModel.CartResources.All(cr =>
                        {
                            int ResourceType = (cr.ResourceType == "H" ? 0 : cr.ResourceType == "R" ? 1 : 2);

                            cartResource = _context.RequestCartResources.FirstOrDefault(rcr => rcr.request_cart_id == cartId && rcr.resource_type == ResourceType && rcr.resource_id == cr.ResourceId);

                            if (cartResource == null)
                            {
                                cartResource = new RequestCartResource
                                {
                                    Id = cr.Id,
                                    request_cart_id = cartId,
                                    resource_id = cr.ResourceId,
                                    resource_type = (cr.ResourceType == "H" ? 0 : cr.ResourceType == "R" ? 1 : 2),
                                    is_added_to_rfp = cr.IsAddedToRFP,
                                    created_by = cr.CreatedBy,
                                    created_date = cr.CreatedDate,
                                    modified_by = cr.ModifiedBy,
                                    modified_date = cr.ModifiedDate
                                };
                                _context.RequestCartResources.Add(cartResource);
                                _context.SaveChanges();
                            }
                            return true;
                        });
                    }
                    else
                    {
                        cart = new RequestCart
                        {
                            Id = cartModel.Id,
                            user_id = cartModel.UserId,
                            created_by = cartModel.CreatedBy,
                            created_date = cartModel.CreatedDate,
                            modified_by = cartModel.ModifiedBy,
                            modified_date = cartModel.ModifiedDate
                        };
                        _context.RequestCarts.Add(cart);
                        _context.SaveChanges();
                        cartId = cart.Id;

                        RequestCartResource cartResource = new RequestCartResource();
                        cartModel.CartResources.All(cr =>
                        {
                            cartResource = new RequestCartResource
                            {
                                Id = cr.Id,
                                request_cart_id = cartId,
                                resource_id = cr.ResourceId,
                                resource_type = (cr.ResourceType == "H" ? 0 : cr.ResourceType == "R" ? 1 : 2),
                                is_added_to_rfp = cr.IsAddedToRFP,
                                created_by = cr.CreatedBy,
                                created_date = cr.CreatedDate,
                                modified_by = cr.ModifiedBy,
                                modified_date = cr.ModifiedDate
                            };
                            _context.RequestCartResources.Add(cartResource);
                            return true;
                        });
                        _context.SaveChanges();
                    }
                }
            }
            return cartId;
        }

        ///INSPIRE-102
        ///Multiple RFP store into table so, RFPId wise delete record from table
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="resourceId"></param>
        /// <param name="resourceType"></param>
        /// <returns></returns>
        //public bool RemoveResourceFromTheSelection(int userId, int resourceId, string RType, int? rfpId)
        //{
        //    var isRemoved = false;

        //    int resourceType = (RType == "H" ? 0 : RType == "R" ? 1 : 2);

        //    var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == userId);
        //    if (cart != null)
        //    {
        //        var cartResource = _context.RequestCartResources.FirstOrDefault(rcr => rcr.request_cart_id == cart.Id && rcr.resource_type == resourceType && rcr.resource_id == resourceId);
        //        if (cartResource != null)
        //        {
        //            if (cartResource.is_added_to_rfp)
        //            {
        //                var rfpResource = (from resource in _context.RFPResources
        //                                   join miceRequest in _context.RFPMiceRequests on resource.rfp_mice_request_id equals miceRequest.Id
        //                                   join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
        //                                   where rfp.user_id == userId && rfp.is_submitted == false && miceRequest.resource_type == resourceType
        //                                   && resource.resource_id == resourceId
        //                                   select resource).FirstOrDefault();
        //                if (rfpResource != null)
        //                {
        //                    _context.RFPResources.Remove(rfpResource);
        //                    _context.SaveChanges();
        //                }

        //                // Remove RFPMiceRequest if not any resources exists.
        //                var rfpMiceRequest = (from miceRequest in _context.RFPMiceRequests
        //                                      join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
        //                                      where rfp.user_id == userId && rfp.is_submitted == false && miceRequest.resource_type == resourceType
        //                                      select miceRequest).FirstOrDefault();

        //                var rfpMResource = (from resource in _context.RFPResources
        //                                    join miceRequest in _context.RFPMiceRequests on resource.rfp_mice_request_id equals miceRequest.Id
        //                                    join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
        //                                    where rfp.user_id == userId && rfp.is_submitted == false && miceRequest.resource_type == resourceType
        //                                    //&& resource.resource_id == resourceId
        //                                    select resource).ToList().Count();

        //                if (rfpMiceRequest != null)
        //                {
        //                    List<RFPMiceRequestItems> rfpMiceRequestItem = _context.RFPMiceRequestItems.Where(t => t.rfp_mice_request_id == rfpMiceRequest.Id).ToList();

        //                    if (rfpMiceRequestItem.Count > 0 && rfpMResource == 0)
        //                    {
        //                        _context.RFPMiceRequestItems.RemoveRange(rfpMiceRequestItem);
        //                        _context.SaveChanges();
        //                    }

        //                    rfpMiceRequestItem = _context.RFPMiceRequestItems.Where(t => t.rfp_mice_request_id == rfpMiceRequest.Id).ToList();

        //                    if (rfpMiceRequest != null && rfpMResource == 0 && rfpMiceRequestItem.Count() == 0)
        //                    {
        //                        _context.RFPMiceRequests.Remove(rfpMiceRequest);
        //                        _context.SaveChanges();
        //                    }

        //                }

        //                // Remove RequestForProposals if not any Mice Request
        //                var rfpMiceRequests = (from miceRequest in _context.RFPMiceRequests
        //                                       join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
        //                                       where rfp.user_id == userId && rfp.is_submitted == false //&& miceRequest.resource_type == resourceType
        //                                       select miceRequest).ToList().Count();

        //                var requestForProposal = (from rfp in _context.RequestForProposals
        //                                          where rfp.user_id == userId && rfp.is_submitted == false
        //                                          select rfp).FirstOrDefault();
        //                if (requestForProposal != null && rfpMiceRequests == 0)
        //                {
        //                    _context.RequestForProposals.Remove(requestForProposal);
        //                    _context.SaveChanges();
        //                }
        //            }
        //            _context.RequestCartResources.Remove(cartResource);
        //            _context.SaveChanges();
        //            return true;
        //        }
        //    }
        //    return isRemoved;
        //}
        public bool AddResourceToRFP(int userId, int resourceId, string RType, int rfpId)
        {
            var isAdded = false;
            int resourceType = (RType == "H" ? 0 : RType == "R" ? 1 : 2);
            RequestForProposal proposal = _context.RequestForProposals.Where(t => t.Id == rfpId).FirstOrDefault();
            if (proposal != null)
            {
                var isRfpResourceExist = (from resource in _context.RFPResources
                                   join miceRequest in _context.RFPMiceRequests on resource.rfp_mice_request_id equals miceRequest.Id
                                   join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
                                   where rfp.Id == rfpId && miceRequest.resource_type == resourceType
                                   && resource.resource_id == resourceId
                                   select resource).FirstOrDefault();

                if (isRfpResourceExist == null)
                {
                    var addRfpResource = (from resource in _context.RFPResources
                                       join miceRequest in _context.RFPMiceRequests on resource.rfp_mice_request_id equals miceRequest.Id
                                       join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
                                       where rfp.Id == rfpId && miceRequest.resource_type == resourceType
                                       select resource).FirstOrDefault();

                    var newRFPResource = new RFPResource();   
                    newRFPResource = new RFPResource
                    {
                        resource_id = resourceId,
                        mice_resource_id = addRfpResource.mice_resource_id,
                        status = RFPStatus.NEW,
                        rfp_mice_request_id = addRfpResource.rfp_mice_request_id,
                        created_by = userId,
                        created_date = DateTime.Now
                    };
                    _context.RFPResources.Add(newRFPResource);
                    _context.SaveChanges();

                    isAdded = true;
                }
            }
            //else
            //{
            //    var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == userId);
            //    if (cart != null)
            //    {

            //        var cartResource = _context.RequestCartResources.FirstOrDefault(rcr => rcr.request_cart_id == cart.Id && rcr.resource_type == resourceType && rcr.resource_id == resourceId);

            //        if (cartResource == null)
            //        {
            //            RequestCartResource newResource = new RequestCartResource();
            //            newResource = new RequestCartResource
            //            {
            //                request_cart_id = cart.Id,
            //                resource_id = resourceId,
            //                resource_type = resourceType,
            //                is_added_to_rfp = false,
            //                created_by = userId,
            //                created_date = DateTime.Now
            //            };
            //            _context.RequestCartResources.Add(newResource);
            //            _context.SaveChanges();
            //            isAdded = true;
            //        }
            //    }
            //    else
            //    {
            //        cart = new RequestCart
            //        {
            //            user_id = userId,
            //            created_by = userId,
            //            created_date = DateTime.Now
            //        };
            //        _context.RequestCarts.Add(cart);
            //        _context.SaveChanges();
            //        int cartId = cart.Id;

            //        RequestCartResource newResource = new RequestCartResource();
            //        newResource = new RequestCartResource
            //        {
            //            request_cart_id = cartId,
            //            resource_id = resourceId,
            //            resource_type = resourceType,
            //            is_added_to_rfp = false,
            //            created_by = userId,
            //            created_date = DateTime.Now
            //        };
            //        _context.RequestCartResources.Add(newResource);
            //        _context.SaveChanges();
            //        isAdded = true;
            //    }
            //}
            return isAdded;
        }
        public bool RemoveResourceFromTheSelection(int userId, int resourceId, string RType, int? rfpId)
        {
            var isRemoved = false;

            int resourceType = (RType == "H" ? 0 : RType == "R" ? 1 : 2);

                       
            RequestForProposal proposal = _context.RequestForProposals.Where(t => t.Id == rfpId).FirstOrDefault();

            if(proposal!=null)
            {
                var rfpResource = (from resource in _context.RFPResources
                                   join miceRequest in _context.RFPMiceRequests on resource.rfp_mice_request_id equals miceRequest.Id
                                   join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
                                   where rfp.Id == rfpId && miceRequest.resource_type == resourceType
                                   && resource.resource_id == resourceId
                                   select resource).FirstOrDefault();
                if (rfpResource != null)
                {
                    _context.RFPResources.Remove(rfpResource);
                    _context.SaveChanges();
                }

                // Remove RFPMiceRequest if not any resources exists.
                var rfpMiceRequest = (from miceRequest in _context.RFPMiceRequests
                                      join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
                                      where rfp.Id == rfpId && miceRequest.resource_type == resourceType
                                      select miceRequest).FirstOrDefault();

                var rfpMResource = (from resource in _context.RFPResources
                                    join miceRequest in _context.RFPMiceRequests on resource.rfp_mice_request_id equals miceRequest.Id
                                    join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
                                    where rfp.Id == rfpId && miceRequest.resource_type == resourceType
                                    //&& resource.resource_id == resourceId
                                    select resource).ToList().Count();

                if (rfpMiceRequest != null)
                {
                    List<RFPMiceRequestItems> rfpMiceRequestItem = _context.RFPMiceRequestItems.Where(t => t.rfp_mice_request_id == rfpMiceRequest.Id).ToList();

                    if (rfpMiceRequestItem.Count > 0 && rfpMResource == 0)
                    {
                        _context.RFPMiceRequestItems.RemoveRange(rfpMiceRequestItem);
                        _context.SaveChanges();
                    }

                    rfpMiceRequestItem = _context.RFPMiceRequestItems.Where(t => t.rfp_mice_request_id == rfpMiceRequest.Id).ToList();

                    if (rfpMiceRequest != null && rfpMResource == 0 && rfpMiceRequestItem.Count() == 0)
                    {
                        _context.RFPMiceRequests.Remove(rfpMiceRequest);
                        _context.SaveChanges();
                    }

                }

                // Remove RequestForProposals if not any Mice Request
                var rfpMiceRequests = (from miceRequest in _context.RFPMiceRequests
                                       join rfp in _context.RequestForProposals on miceRequest.rfp_id equals rfp.Id
                                       where rfp.Id == rfpId //&& rfp.is_submitted == false //&& miceRequest.resource_type == resourceType
                                       select miceRequest).ToList().Count();

                var requestForProposal = (from rfp in _context.RequestForProposals
                                          where rfp.Id == rfpId
                                          select rfp).FirstOrDefault();
                if (requestForProposal != null && rfpMiceRequests == 0)
                {
                    _context.RequestForProposals.Remove(requestForProposal);
                    _context.SaveChanges();
                }
                return true;
            }
            else
            {
                var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == userId);
                if (cart != null)
                {
                    var cartResource = _context.RequestCartResources.FirstOrDefault(rcr => rcr.request_cart_id == cart.Id && rcr.resource_type == resourceType && rcr.resource_id == resourceId);

                    if (cartResource != null)
                    {
                        _context.RequestCartResources.Remove(cartResource);
                        _context.SaveChanges();
                        //return true;
                    }

                    if(_context.RequestCartResources.Where(t=>t.request_cart_id==cart.Id).Count()==0)
                    {
                        _context.RequestCarts.Remove(cart);
                        _context.SaveChanges();
                    }
                    return true;
                }
               
            }

            return isRemoved;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public RequestCartModel GetRequestCart(int userId)
        {
            var cartModel = new RequestCartModel();

            var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == userId);
            if (cart != null)
            {
                var cartResources = _context.RequestCartResources.Where(rcr => rcr.request_cart_id == cart.Id).ToList();
                cartModel = new RequestCartModel
                {
                    Id = cart.Id,
                    UserId = cart.user_id,
                    CreatedBy = cart.created_by,
                    CreatedDate = cart.created_date,
                    ModifiedBy = cart.modified_by,
                    ModifiedDate = cart.modified_date
                };
                cartModel.CartResources = new List<RequestCartResourceModel>();
                var cartResourceModel = new RequestCartResourceModel();
                cartResources.All(cr =>
                {
                    cartResourceModel = new RequestCartResourceModel
                    {
                        CreatedBy = cr.created_by,
                        CreatedDate = cr.created_date,
                        Id = cr.Id,
                        IsAddedToRFP = cr.is_added_to_rfp,
                        ModifiedBy = cr.modified_by,
                        ModifiedDate = cr.modified_date,
                        RequestCartId = cr.request_cart_id,
                        ResourceId = cr.resource_id,
                        ResourceType = cr.resource_type == 0 ? "H" : cr.resource_type == 1 ? "R" : "L"
                    };
                    cartModel.CartResources.Add(cartResourceModel);
                    return true;
                });
            }
            return cartModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int MakeRequestForProposal(int userId)
        {
            var rfpId = 0;

            var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == userId);
            if (cart != null)
            {
                var cartResources = _context.RequestCartResources.Where(rcr => rcr.request_cart_id == cart.Id && rcr.is_added_to_rfp == false).ToList();
                var resourceTypes = cartResources.Select(cr => cr.resource_type).Distinct().ToList();

                var rfp = _context.RequestForProposals.FirstOrDefault(rf => rf.user_id == userId && rf.is_submitted == false);
                if (rfp != null)
                {
                    rfpId = rfp.Id;
                    rfp.modified_by = userId;
                    rfp.modified_date = DateTime.Now;
                    _context.SaveChanges();
                    // Insert/Update Mice Request for each resource type
                    if (resourceTypes.Any())
                    {
                        var miceRequest = new RFPMiceRequest();
                        foreach (var resourceType in resourceTypes)
                        {
                            miceRequest = _context.RFPMiceRequests.FirstOrDefault(rmr => rmr.rfp_id == rfpId && rmr.resource_type == resourceType);
                            if (miceRequest == null)
                            {
                                miceRequest = new RFPMiceRequest
                                {
                                    resource_type = resourceType,
                                    rfp_id = rfpId,
                                    status = RFPStatus.NEW,
                                    created_by = userId,
                                    created_date = DateTime.Now
                                };
                                _context.RFPMiceRequests.Add(miceRequest);
                                _context.SaveChanges();
                            }
                            else
                            {
                                miceRequest.modified_by = userId;
                                miceRequest.modified_date = DateTime.Now;
                            }

                            // Insert mice resource
                            var selectedResources = cartResources.Where(cr => cr.resource_type == resourceType && cr.is_added_to_rfp == false).ToList();
                            var selectedResourceIds = selectedResources.Select(sr => sr.resource_id).ToList();
                            // Add Hotel Resource Type
                            if (resourceType == (int)ManageEnum.resourcetype.hotel)
                            {
                                var selectedHotels = _context.Hotels.Where(h => selectedResourceIds.Contains(h.dcp_hotelinfo_id)).ToList();
                                var miceResource = new RFPResource();
                                foreach (var selectedResource in selectedResources)
                                {
                                    var selectedHotel = selectedHotels.Where(sh => sh.dcp_hotelinfo_id == selectedResource.resource_id).FirstOrDefault();
                                    miceResource = new RFPResource
                                    {
                                        resource_id = selectedResource.resource_id,
                                        mice_resource_id = Convert.ToInt32(selectedHotel.mice_id),
                                        status = RFPStatus.NEW,
                                        rfp_mice_request_id = miceRequest.Id,
                                        created_by = userId,
                                        created_date = DateTime.Now
                                    };
                                    _context.RFPResources.Add(miceResource);
                                }
                                _context.SaveChanges();
                            }
                            // Add Location Resource Type
                            if (resourceType == (int)ManageEnum.resourcetype.location)
                            {
                                var selectedLocations = _context.Locations.Where(h => selectedResourceIds.Contains(h.dcp_locationinfo_id)).ToList();
                                var miceResource = new RFPResource();
                                foreach (var selectedResource in selectedResources)
                                {
                                    var selectedLocation = selectedLocations.Where(sh => sh.dcp_locationinfo_id == selectedResource.resource_id).FirstOrDefault();
                                    miceResource = new RFPResource
                                    {
                                        resource_id = selectedResource.resource_id,
                                        mice_resource_id = Convert.ToInt32(selectedLocation.mice_id),
                                        status = RFPStatus.NEW,
                                        rfp_mice_request_id = miceRequest.Id,
                                        created_by = userId,
                                        created_date = DateTime.Now
                                    };
                                    _context.RFPResources.Add(miceResource);
                                }
                                _context.SaveChanges();
                            }
                            // Add Restaurants Resource Type
                            if (resourceType == (int)ManageEnum.resourcetype.restaurant)
                            {
                                var selectedRestaurants = _context.Restaurants.Where(h => selectedResourceIds.Contains(h.dcp_restaurantinfo_id)).ToList();
                                var miceResource = new RFPResource();
                                foreach (var selectedResource in selectedResources)
                                {
                                    var selectedRestaurant = selectedRestaurants.Where(sh => sh.dcp_restaurantinfo_id == selectedResource.resource_id).FirstOrDefault();
                                    miceResource = new RFPResource
                                    {
                                        resource_id = selectedResource.resource_id,
                                        mice_resource_id = Convert.ToInt32(selectedRestaurant.mice_id),
                                        status = RFPStatus.NEW,
                                        rfp_mice_request_id = miceRequest.Id,
                                        created_by = userId,
                                        created_date = DateTime.Now
                                    };
                                    _context.RFPResources.Add(miceResource);
                                }
                                _context.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    int? miceUserId = null;
                    var userCredential = _context.User_Credential.FirstOrDefault(uc => uc.User_Master_Id == userId);
                    if (userCredential != null)
                    {
                        miceUserId = userCredential.Mice_Userid;
                    }
                    rfp = new RequestForProposal
                    {
                        created_by = userId,
                        created_date = DateTime.Now,
                        user_id = userId,
                        mice_user_id = miceUserId,
                        is_submitted = false,
                        status = RFPStatus.NEW
                    };
                    _context.RequestForProposals.Add(rfp);
                    _context.SaveChanges();

                    rfpId = rfp.Id;

                    // Insert Mice Request for each resource type
                    if (resourceTypes.Any())
                    {
                        var miceRequest = new RFPMiceRequest();
                        foreach (var resourceType in resourceTypes)
                        {
                            miceRequest = new RFPMiceRequest
                            {
                                resource_type = resourceType,
                                rfp_id = rfpId,
                                status = RFPStatus.NEW,
                                created_by = userId,
                                created_date = DateTime.Now
                            };
                            _context.RFPMiceRequests.Add(miceRequest);
                            _context.SaveChanges();

                            // Insert mice resource
                            var selectedResources = cartResources.Where(cr => cr.resource_type == resourceType).ToList();
                            var selectedResourceIds = selectedResources.Select(sr => sr.resource_id).ToList();
                            // Add Hotel Resource Type
                            if (resourceType == (int)ManageEnum.resourcetype.hotel)
                            {
                                var selectedHotels = _context.Hotels.Where(h => selectedResourceIds.Contains(h.dcp_hotelinfo_id)).ToList();
                                var miceResource = new RFPResource();
                                foreach (var selectedResource in selectedResources)
                                {
                                    var selectedHotel = selectedHotels.Where(sh => sh.dcp_hotelinfo_id == selectedResource.resource_id).FirstOrDefault();
                                    miceResource = new RFPResource
                                    {
                                        resource_id = selectedResource.resource_id,
                                        mice_resource_id = Convert.ToInt32(selectedHotel.mice_id),
                                        status = RFPStatus.NEW,
                                        rfp_mice_request_id = miceRequest.Id,
                                        created_by = userId,
                                        created_date = DateTime.Now
                                    };
                                    _context.RFPResources.Add(miceResource);
                                }
                                _context.SaveChanges();
                            }
                            // Add Location Resource Type
                            if (resourceType == (int)ManageEnum.resourcetype.location)
                            {
                                var selectedLocations = _context.Locations.Where(h => selectedResourceIds.Contains(h.dcp_locationinfo_id)).ToList();
                                var miceResource = new RFPResource();
                                foreach (var selectedResource in selectedResources)
                                {
                                    var selectedLocation = selectedLocations.Where(sh => sh.dcp_locationinfo_id == selectedResource.resource_id).FirstOrDefault();
                                    miceResource = new RFPResource
                                    {
                                        resource_id = selectedResource.resource_id,
                                        mice_resource_id = Convert.ToInt32(selectedLocation.mice_id),
                                        status = RFPStatus.NEW,
                                        rfp_mice_request_id = miceRequest.Id,
                                        created_by = userId,
                                        created_date = DateTime.Now
                                    };
                                    _context.RFPResources.Add(miceResource);
                                }
                                _context.SaveChanges();
                            }
                            // Add Restaurants Resource Type
                            if (resourceType == (int)ManageEnum.resourcetype.restaurant)
                            {
                                var selectedRestaurants = _context.Restaurants.Where(h => selectedResourceIds.Contains(h.dcp_restaurantinfo_id)).ToList();
                                var miceResource = new RFPResource();
                                foreach (var selectedResource in selectedResources)
                                {
                                    var selectedRestaurant = selectedRestaurants.Where(sh => sh.dcp_restaurantinfo_id == selectedResource.resource_id).FirstOrDefault();
                                    miceResource = new RFPResource
                                    {
                                        resource_id = selectedResource.resource_id,
                                        mice_resource_id = Convert.ToInt32(selectedRestaurant.mice_id),
                                        status = RFPStatus.NEW,
                                        rfp_mice_request_id = miceRequest.Id,
                                        created_by = userId,
                                        created_date = DateTime.Now
                                    };
                                    _context.RFPResources.Add(miceResource);
                                }
                                _context.SaveChanges();
                            }
                        }
                    }
                }

                // Set added to rfp flag true for the cart resources
                foreach (var cartResource in cartResources)
                {
                    cartResource.is_added_to_rfp = true;
                    cartResource.modified_by = userId;
                    cartResource.modified_date = DateTime.Now;
                }
                _context.SaveChanges();
            }
            return rfpId;
        }

        ///INSPIRE-102
        ///This method is new.
        ///This method is stored rfp records into request for proposal related tables and remove records from carts
        public RequestForProposalModel MakeRequestForProposalFromStartRfp(RequestForProposalModel rfpModel)
        {
            var rfpId = 0;

            var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == rfpModel.UserId);
            if (cart != null)
            {
                var cartResources = _context.RequestCartResources.Where(rcr => rcr.request_cart_id == cart.Id && rcr.is_added_to_rfp == false).ToList();
                var resourceTypes = cartResources.Select(cr => cr.resource_type).Distinct().ToList();
                int? miceUserId = null;
                var userCredential = _context.User_Credential.FirstOrDefault(uc => uc.User_Master_Id == rfpModel.UserId);
                if (userCredential != null)
                {
                    miceUserId = userCredential.Mice_Userid;
                }
                RequestForProposal rfp = new RequestForProposal
                {
                    created_by = rfpModel.UserId,
                    created_date = DateTime.Now,
                    user_id = rfpModel.UserId,
                    mice_user_id = miceUserId,
                    is_submitted = false,
                    event_name = rfpModel.EventName,
                    eventstartdate =rfpModel.EventStartDate,
                    eventenddate=rfpModel.EventEndDate,
                    status = RFPStatus.NEW,
                    days_running = rfpModel.DaysRunning,
                    rfp_type=rfpModel.RFPType
                };
                _context.RequestForProposals.Add(rfp);
                _context.SaveChanges();

                rfpId = rfp.Id;
                rfpModel.Id = rfpId;
                // Insert Mice Request for each resource type
                if (resourceTypes.Any())
                {
                    var miceRequest = new RFPMiceRequest();
                    foreach (var resourceType in resourceTypes)
                    {
                        miceRequest = new RFPMiceRequest
                        {
                            resource_type = resourceType,
                            rfp_id = rfpId,
                            status = RFPStatus.NEW,
                            created_by = rfpModel.UserId,
                            created_date = DateTime.Now
                        };
                        _context.RFPMiceRequests.Add(miceRequest);
                        _context.SaveChanges();

                        // Insert mice resource
                        var selectedResources = cartResources.Where(cr => cr.resource_type == resourceType).ToList();
                        var selectedResourceIds = selectedResources.Select(sr => sr.resource_id).ToList();
                        // Add Hotel Resource Type
                        if (resourceType == (int)ManageEnum.resourcetype.hotel)
                        {
                            var selectedHotels = _context.Hotels.Where(h => selectedResourceIds.Contains(h.dcp_hotelinfo_id)).ToList();
                            var miceResource = new RFPResource();
                            foreach (var selectedResource in selectedResources)
                            {
                                var selectedHotel = selectedHotels.Where(sh => sh.dcp_hotelinfo_id == selectedResource.resource_id).FirstOrDefault();
                                miceResource = new RFPResource
                                {
                                    resource_id = selectedResource.resource_id,
                                    mice_resource_id = Convert.ToInt32(selectedHotel.mice_id),
                                    status = RFPStatus.NEW,
                                    rfp_mice_request_id = miceRequest.Id,
                                    created_by = rfpModel.UserId,
                                    created_date = DateTime.Now
                                };
                                _context.RFPResources.Add(miceResource);
                            }
                            _context.SaveChanges();
                        }
                        // Add Location Resource Type
                        if (resourceType == (int)ManageEnum.resourcetype.location)
                        {
                            var selectedLocations = _context.Locations.Where(h => selectedResourceIds.Contains(h.dcp_locationinfo_id)).ToList();
                            var miceResource = new RFPResource();
                            foreach (var selectedResource in selectedResources)
                            {
                                var selectedLocation = selectedLocations.Where(sh => sh.dcp_locationinfo_id == selectedResource.resource_id).FirstOrDefault();
                                miceResource = new RFPResource
                                {
                                    resource_id = selectedResource.resource_id,
                                    mice_resource_id = Convert.ToInt32(selectedLocation.mice_id),
                                    status = RFPStatus.NEW,
                                    rfp_mice_request_id = miceRequest.Id,
                                    created_by = rfpModel.UserId,
                                    created_date = DateTime.Now
                                };
                                _context.RFPResources.Add(miceResource);
                            }
                            _context.SaveChanges();
                        }
                        // Add Restaurants Resource Type
                        if (resourceType == (int)ManageEnum.resourcetype.restaurant)
                        {
                            var selectedRestaurants = _context.Restaurants.Where(h => selectedResourceIds.Contains(h.dcp_restaurantinfo_id)).ToList();
                            var miceResource = new RFPResource();
                            foreach (var selectedResource in selectedResources)
                            {
                                var selectedRestaurant = selectedRestaurants.Where(sh => sh.dcp_restaurantinfo_id == selectedResource.resource_id).FirstOrDefault();
                                miceResource = new RFPResource
                                {
                                    resource_id = selectedResource.resource_id,
                                    mice_resource_id = Convert.ToInt32(selectedRestaurant.mice_id),
                                    status = RFPStatus.NEW,
                                    rfp_mice_request_id = miceRequest.Id,
                                    created_by = rfpModel.UserId,
                                    created_date = DateTime.Now
                                };
                                _context.RFPResources.Add(miceResource);
                            }
                            _context.SaveChanges();
                        }
                    }
                }

                // Set added to rfp flag true for the cart resources
                foreach (var cartResource in cartResources)
                {
                    cartResource.is_added_to_rfp = true;
                    cartResource.modified_by = rfpModel.UserId;
                    cartResource.modified_date = DateTime.Now;
                }
                _context.SaveChanges();


                List<RFPMiceRequest> RFPMiceRequestList =  _context.RFPMiceRequests.Where(t => t.rfp_id == rfpId).ToList();

                List<RFPMiceRequestModel> miceRequestModels = new List<RFPMiceRequestModel>();
                if(RFPMiceRequestList.Count>0)
                {
                    miceRequestModels = RFPMiceRequestList.Select(t => new RFPMiceRequestModel()
                    {
                        Id = t.Id,
                        RfpId = t.rfp_id,
                        MiceRequestId = t.mice_request_id,
                        ResourceType = t.resource_type,
                        Comments = t.comments,
                        Comments2 = t.comments2,
                        Comments3 = t.comments3,
                        Comments4 = t.comments4,
                        CityId = t.city_id,
                        PlaceId = t.place_id,
                        CountryId = t.country_id,
                        Status = t.status,
                        CreatedBy = t.created_by,
                        CreatedDate = t.created_date


                    }).ToList();
                    rfpModel.RFPMiceRequests = miceRequestModels;
                }
            }

            

            if (cart != null)
            {
                var resourcedata = _context.RequestCartResources.Where(x => x.request_cart_id == cart.Id).ToList();

                if (resourcedata != null)
                {
                    _context.RequestCartResources.RemoveRange(resourcedata);
                }
                _context.RequestCarts.Remove(cart);
            }

            _context.SaveChanges();
           
            return rfpModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int GetRequestCartCount(int userId)
        {
            var count = 0;
            var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == userId);
            if (cart != null)
            {
                count = _context.RequestCartResources.Count(rcr => rcr.request_cart_id == cart.Id);
            }
            return count;
        }

        #endregion Cart

        #region Lookup Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public List<ConferenceFoodGroupModel> GetConferenceFoods(string lang)
        {
            var lookupTypes = new List<ConferenceFoodGroupModel>();
            lookupTypes = (from cfg in _context.List_ConferenceFoodGroups
                           orderby cfg.sequence
                           select new ConferenceFoodGroupModel
                           {
                               GroupName = lang == "de-DE" ? cfg.conference_food_group_name_de : cfg.conference_food_group_name,
                               GroupId = cfg.Id
                           }).ToList();
            foreach (var lookupType in lookupTypes)
            {
                lookupType.ConferenceFoods = (from cf in _context.ConferenceFoods
                                              orderby cf.sequence
                                              where cf.conference_food_group_id == lookupType.GroupId
                                              select new LookupString
                                              {
                                                  Id = cf.mice_conference_food_name,
                                                  Name = lang == "de-DE" ? cf.conference_food_name_de : cf.conference_food_name
                                              }).ToList();
            }
            return lookupTypes;
        }


        #endregion Lookup Methods

        #region RFP Request Items

        public RequestItemModel GetRFPRequestItems(int rfpMiceRequestItemId, string langCode)
        {
            var requestItemModel = new RequestItemModel();

            var rfpMiceRequestItems = _context.RFPMiceRequestItems.FirstOrDefault(rmr => rmr.Id == rfpMiceRequestItemId);

            if (rfpMiceRequestItems != null)
            {
                requestItemModel.CreatedBy = rfpMiceRequestItems.created_by;
                requestItemModel.CreatedDate = rfpMiceRequestItems.created_date;
                requestItemModel.Id = rfpMiceRequestItems.Id;
                requestItemModel.ModifiedBy = rfpMiceRequestItems.modified_by;
                requestItemModel.ModifiedDate = rfpMiceRequestItems.modified_date;
                requestItemModel.RequestItems = JsonConvert.DeserializeObject<List<RequestItemObject>>(rfpMiceRequestItems.request_item_object);
                requestItemModel.RequestItemType = rfpMiceRequestItems.request_item_type;
                requestItemModel.RFPMiceRequestId = rfpMiceRequestItems.rfp_mice_request_id;
                requestItemModel.RFPId = rfpMiceRequestItems.RFPMiceRequest != null ? rfpMiceRequestItems.RFPMiceRequest.rfp_id : 0;
                if (requestItemModel.RequestItems.Any())
                {
                    requestItemModel.RequestItems.All(ri =>
                    {
                        ri.LangCode = langCode;
                        return true;
                    });
                }
            }
            return requestItemModel;
        }

        public int AddEditRFPRequestItem(RequestItemModel requestItemModel)
        {
            var rfpRequestItemId = 0;
            var requestItem = requestItemModel.RequestItems.FirstOrDefault();
            if (requestItem != null)
            {
                var roomType = _context.RoomTypes.FirstOrDefault(rt => rt.mice_room_type == requestItem.RoomType);
                var seatingType = _context.SeatingTypes.FirstOrDefault(st => st.mice_seating_type == requestItem.SeatingType);
                if (roomType != null)
                {
                    requestItem.RoomTypeInEnglish = roomType.room_type;
                    requestItem.RoomTypeInGerman = roomType.room_type_de;
                }
                if (seatingType != null)
                {
                    requestItem.SeatingTypeInEnglish = seatingType.seating_type;
                    requestItem.SeatingTypeInGerman = seatingType.seating_type_de;
                }
                if (requestItemModel.Id > 0)
                {
                    var rfpRequestItem = _context.RFPMiceRequestItems.FirstOrDefault(rmr => rmr.Id == requestItemModel.Id);
                    if (rfpRequestItem != null && !string.IsNullOrWhiteSpace(rfpRequestItem.request_item_object))
                    {
                        rfpRequestItemId = rfpRequestItem.Id;
                        var rfpRequestItemObjects = JsonConvert.DeserializeObject<List<RequestItemObject>>(rfpRequestItem.request_item_object);
                        //var rio = rfpRequestItemObjects.FirstOrDefault(ri => ri.Id == requestItem.Id);
                        //if (rio != null)
                        //{
                        //    rfpRequestItemObjects.Remove(rio);
                        //}
                        //rfpRequestItemObjects.Add(requestItem);
                        var requestItemObjects = new List<RequestItemObject>();
                        requestItemObjects.Add(requestItem);
                        rfpRequestItem.request_item_type = requestItemModel.RequestItemType;
                        rfpRequestItem.modified_by = requestItemModel.ModifiedBy;
                        rfpRequestItem.modified_date = requestItemModel.ModifiedDate;
                        rfpRequestItem.request_item_object = JsonConvert.SerializeObject(requestItemObjects);
                        _context.SaveChanges();
                    }
                    else if (rfpRequestItem != null)
                    {
                        requestItem.Id = 1;
                        var requestItemObjects = new List<RequestItemObject>();
                        requestItemObjects.Add(requestItem);
                        rfpRequestItem.request_item_object = JsonConvert.SerializeObject(requestItemObjects);
                        _context.SaveChanges();
                    }
                }
                else
                {
                    
                    requestItem.Id = 1;
                    var requestItemObjects = new List<RequestItemObject>();
                    requestItemObjects.Add(requestItem);
                    var rfpMiceRequestItem = new RFPMiceRequestItems
                    {
                        rfp_mice_request_id = requestItemModel.RFPMiceRequestId,
                        request_item_type = requestItemModel.RequestItemType,
                        request_item_object = JsonConvert.SerializeObject(requestItemObjects),
                        created_by = requestItemModel.CreatedBy,
                        created_date = requestItemModel.CreatedDate,
                        modified_by = requestItemModel.ModifiedBy,
                        modified_date = requestItemModel.ModifiedDate
                    };
                    _context.RFPMiceRequestItems.Add(rfpMiceRequestItem);
                    _context.SaveChanges();
                    rfpRequestItemId = rfpMiceRequestItem.Id;
                }
            }
            return rfpRequestItemId;
        }

        public bool DeleteRFPRequestItem(int rfpMiceRequestItemId)
        {
            var isDeleted = false;
            var rfpMiceRequestItem = _context.RFPMiceRequestItems.FirstOrDefault(rmr => rmr.Id == rfpMiceRequestItemId);
            if (rfpMiceRequestItem != null)
            {
                //var rfpRequestItemObjects = JsonConvert.DeserializeObject<List<RequestItemObject>>(rfpMiceRequestItem.request_item_object);
                //var selectedRequestItemObj = rfpRequestItemObjects.FirstOrDefault(ri => ri.Id == requestItemObjectId);
                //if (selectedRequestItemObj != null)
                //{
                //    rfpRequestItemObjects.Remove(selectedRequestItemObj);
                //}
                //var refreshRequestItemObject = new List<RequestItemObject>();
                //rfpRequestItemObjects = rfpRequestItemObjects.OrderBy(ri => ri.Id).ToList();
                //for (var rioId = 0; rioId < rfpRequestItemObjects.Count; rioId++)
                //{
                //    var rio = rfpRequestItemObjects[rioId];
                //    rio.Id = rioId + 1;
                //    refreshRequestItemObject.Add(rio);
                //}
                _context.RFPMiceRequestItems.Remove(rfpMiceRequestItem);
                _context.SaveChanges();
                return true;
            }
            return isDeleted;
        }

        #endregion RFP Request Items

        #region RFP

        public RequestForProposalModel GetRequestForProposal(int userId, string langCode)
        {
            var requestForProposal = new RequestForProposalModel();
            bool isGerman = langCode == "de-DE";

            var rfp = _context.RequestForProposals.FirstOrDefault(r => r.user_id == userId && r.is_submitted == false);

            if (rfp != null)
            {
                requestForProposal = new RequestForProposalModel
                {
                    CreatedBy = rfp.created_by,
                    CreatedDate = rfp.created_date,
                    DaysRunning = rfp.days_running,
                    Id = rfp.Id,
                    IsSubmitted = rfp.is_submitted,
                    MiceUserId = rfp.mice_user_id,
                    ModifiedBy = rfp.modified_by,
                    ModifiedDate = rfp.modified_date,
                    RFPType = rfp.rfp_type,
                    Status = rfp.status,
                    UserId = rfp.user_id,
                    EventName = rfp.event_name,
                    EventStartDate=rfp.eventstartdate,
                    EventEndDate=rfp.eventenddate,
                    strEventStartDate = rfp.eventstartdate != null?Convert.ToDateTime(rfp.eventstartdate).ToString("dd.MM.yyyy"):"",
                    strEventEndDate = rfp.eventenddate != null ? Convert.ToDateTime(rfp.eventenddate).ToString("dd.MM.yyyy") : "",
                };

                string miceDaysRunning = Convert.ToString(requestForProposal.DaysRunning);

                var lstDaysRunning = _context.List_DaysRunnings.Where(l => l.mice_days_running == miceDaysRunning).FirstOrDefault();
                if (lstDaysRunning != null)
                {
                    requestForProposal.DayRunningName = isGerman == true ? lstDaysRunning.days_running_de : lstDaysRunning.days_running;
                }

                var rfpMiceRequests = _context.RFPMiceRequests.Where(mr => mr.rfp_id == requestForProposal.Id).ToList();

                var rfpMiceRequestIds = rfpMiceRequests.Select(mr => mr.Id).ToList();

                var rfpResourceEntities = _context.RFPResources.Where(rr => rfpMiceRequestIds.Contains(rr.rfp_mice_request_id)).ToList();

                var rfpMiceRequestItemEntities = _context.RFPMiceRequestItems.Where(ri => rfpMiceRequestIds.Contains(ri.rfp_mice_request_id)).ToList();

                if (rfpMiceRequests.Any())
                {
                    requestForProposal.RFPMiceRequests = new List<RFPMiceRequestModel>();
                    foreach (var mre in rfpMiceRequests)
                    {
                        var miceRequest = new RFPMiceRequestModel
                        {
                            CityId = mre.city_id,
                            Comments = mre.comments,
                            Comments2 = mre.comments2,
                            Comments3 = mre.comments3,
                            Comments4 = mre.comments4,
                            CountryId = mre.country_id,
                            CreatedBy = mre.created_by,
                            CreatedDate = mre.created_date,
                            Id = mre.Id,
                            MiceRequestId = mre.mice_request_id,
                            ModifiedBy = mre.modified_by,
                            ModifiedDate = mre.modified_date,
                            PlaceId = mre.place_id,
                            ResourceType = mre.resource_type,
                            RfpId = mre.rfp_id,
                            Status = mre.status
                        };
                        var rfpResources = rfpResourceEntities.Where(rr => rr.rfp_mice_request_id == mre.Id).ToList();
                        if (rfpResources.Any())
                        {
                            miceRequest.RFPResources = rfpResources.Select(r => new RFPResourceModel
                            {
                                CreatedBy = r.created_by,
                                CreatedDate = r.created_date,
                                Id = r.Id,
                                MiceResourceId = r.mice_resource_id,
                                ModifiedBy = r.modified_by,
                                ModifiedDate = r.modified_date,
                                ResourceId = r.resource_id,
                                RFPMiceRequestId = r.rfp_mice_request_id,
                                Status = r.status
                            }).ToList();
                        }
                        var rfpMiceRequestItems = rfpMiceRequestItemEntities.Where(ri => ri.rfp_mice_request_id == mre.Id).ToList();
                        if (rfpMiceRequestItems.Any())
                        {
                            miceRequest.RFPMiceRequestItems = rfpMiceRequestItems.Select(rm => new RequestItemModel
                            {
                                CreatedBy = rm.created_by,
                                CreatedDate = rm.created_date,
                                Id = rm.Id,
                                ModifiedBy = rm.modified_by,
                                ModifiedDate = rm.modified_date,
                                RequestItems = JsonConvert.DeserializeObject<List<RequestItemObject>>(rm.request_item_object),
                                RequestItemType = rm.request_item_type,
                                RFPId = rfp.Id,
                                RFPMiceRequestId = rm.rfp_mice_request_id
                            }).ToList();

                            if (miceRequest.RFPMiceRequestItems.Select(x => x.RequestItems).ToList().Any())
                            {
                                miceRequest.RFPMiceRequestItems.Select(x => x.RequestItems).ToList().All(ri =>
                                {
                                    ri.All(ri1 =>
                                    {
                                        ri1.LangCode = langCode;
                                        return true;
                                    });
                                    return true;
                                });
                            }
                        }
                        requestForProposal.RFPMiceRequests.Add(miceRequest);
                    }
                }

            }

            return requestForProposal;
        }

        ///INSPIRE-102
        ///This method is new.
        ///This method return cart records from tables when user click on startrfp table
        public RequestForProposalModel GetRequestForStartRFP(int userId, string langCode)
        {
            var requestForProposal = new RequestForProposalModel();
            var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == userId);
            if (cart != null)
            {
                var cartResources = _context.RequestCartResources.Where(rcr => rcr.request_cart_id == cart.Id).ToList();
                var resourceTypes = cartResources.Select(cr => cr.resource_type).Distinct().ToList();

                requestForProposal.IsViewMode = false;
                requestForProposal.IsSubmitted = false;

                List<RFPMiceRequestModel> miceRequestModels = new List<RFPMiceRequestModel>();
                foreach (var item in resourceTypes)
                {
                    RFPMiceRequestModel requestModel = new RFPMiceRequestModel();
                    requestModel.ResourceType = Convert.ToInt32(item.ToString());

                    List<RFPResourceModel> resourceModels = cartResources.Where(t => t.resource_type == Convert.ToInt32(item.ToString())).ToList().Select(p => new RFPResourceModel()
                    {
                        ResourceId = p.resource_id
                    }).ToList();

                    requestModel.RFPResources = resourceModels;
                    miceRequestModels.Add(requestModel);
                }
                                             
                requestForProposal.RFPMiceRequests = miceRequestModels;                
            }

            return requestForProposal;
        }

        public RequestForProposalModel GetRequestForProposalById(int id, string langCode)
        {
            var requestForProposal = new RequestForProposalModel();
            bool isGerman = langCode == "de-DE";

            var rfp = _context.RequestForProposals.FirstOrDefault(r => r.Id == id); // && r.is_submitted == true);

            if (rfp != null)
            {
                requestForProposal = new RequestForProposalModel
                {
                    CreatedBy = rfp.created_by,
                    CreatedDate = rfp.created_date,
                    DaysRunning = rfp.days_running,
                    Id = rfp.Id,
                    IsSubmitted = rfp.is_submitted,
                    MiceUserId = rfp.mice_user_id,
                    ModifiedBy = rfp.modified_by,
                    ModifiedDate = rfp.modified_date,
                    RFPType = rfp.rfp_type,
                    Status = rfp.status,
                    UserId = rfp.user_id,
                    EventName = rfp.event_name,
                    EventStartDate = rfp.eventstartdate,
                    EventEndDate = rfp.eventenddate,
                    strEventStartDate = rfp.eventstartdate != null ? Convert.ToDateTime(rfp.eventstartdate).ToString("dd.MM.yyyy") : "",
                    strEventEndDate = rfp.eventenddate != null ? Convert.ToDateTime(rfp.eventenddate).ToString("dd.MM.yyyy") : "",
                };

                string miceDaysRunning = Convert.ToString(requestForProposal.DaysRunning);

                var lstDaysRunning = _context.List_DaysRunnings.Where(l => l.mice_days_running == miceDaysRunning).FirstOrDefault();
                if (lstDaysRunning != null)
                {
                    requestForProposal.DayRunningName = isGerman == true ? lstDaysRunning.days_running_de : lstDaysRunning.days_running;
                }

                if(rfp.is_submitted==true)
                {
                    requestForProposal.IsViewMode = true;
                }
                else
                {
                    requestForProposal.IsViewMode = false;
                }
                

                var rfpMiceRequests = _context.RFPMiceRequests.Where(mr => mr.rfp_id == requestForProposal.Id).ToList();

                var rfpMiceRequestIds = rfpMiceRequests.Select(mr => mr.Id).ToList();

                var rfpResourceEntities = _context.RFPResources.Where(rr => rfpMiceRequestIds.Contains(rr.rfp_mice_request_id)).ToList();

                var rfpMiceRequestItemEntities = _context.RFPMiceRequestItems.Where(ri => rfpMiceRequestIds.Contains(ri.rfp_mice_request_id)).ToList();

                if (rfpMiceRequests.Any())
                {
                    requestForProposal.RFPMiceRequests = new List<RFPMiceRequestModel>();
                    foreach (var mre in rfpMiceRequests)
                    {
                        var miceRequest = new RFPMiceRequestModel
                        {
                            CityId = mre.city_id,
                            Comments = mre.comments,
                            Comments2 = mre.comments2,
                            Comments3 = mre.comments3,
                            Comments4 = mre.comments4,
                            CountryId = mre.country_id,
                            CreatedBy = mre.created_by,
                            CreatedDate = mre.created_date,
                            Id = mre.Id,
                            MiceRequestId = mre.mice_request_id,
                            ModifiedBy = mre.modified_by,
                            ModifiedDate = mre.modified_date,
                            PlaceId = mre.place_id,
                            ResourceType = mre.resource_type,
                            RfpId = mre.rfp_id,
                            Status = mre.status
                        };
                        var rfpResources = rfpResourceEntities.Where(rr => rr.rfp_mice_request_id == mre.Id).ToList();
                        if (rfpResources.Any())
                        {
                            miceRequest.RFPResources = rfpResources.Select(r => new RFPResourceModel
                            {
                                CreatedBy = r.created_by,
                                CreatedDate = r.created_date,
                                Id = r.Id,
                                MiceResourceId = r.mice_resource_id,
                                ModifiedBy = r.modified_by,
                                ModifiedDate = r.modified_date,
                                ResourceId = r.resource_id,
                                RFPMiceRequestId = r.rfp_mice_request_id,
                                Status = r.status
                            }).ToList();
                        }
                        var rfpMiceRequestItems = rfpMiceRequestItemEntities.Where(ri => ri.rfp_mice_request_id == mre.Id).ToList();
                        if (rfpMiceRequestItems.Any())
                        {
                            miceRequest.RFPMiceRequestItems = rfpMiceRequestItems.Select(rm => new RequestItemModel
                            {
                                CreatedBy = rm.created_by,
                                CreatedDate = rm.created_date,
                                Id = rm.Id,
                                ModifiedBy = rm.modified_by,
                                ModifiedDate = rm.modified_date,
                                RequestItems = JsonConvert.DeserializeObject<List<RequestItemObject>>(rm.request_item_object),
                                RequestItemType = rm.request_item_type,
                                RFPId = rfp.Id,
                                RFPMiceRequestId = rm.rfp_mice_request_id
                            }).ToList();

                            if (miceRequest.RFPMiceRequestItems.Select(x => x.RequestItems).ToList().Any())
                            {
                                miceRequest.RFPMiceRequestItems.Select(x => x.RequestItems).ToList().All(ri =>
                                {
                                    ri.All(ri1 =>
                                    {
                                        ri1.LangCode = langCode;
                                        return true;
                                    });
                                    return true;
                                });
                            }
                        }
                        requestForProposal.RFPMiceRequests.Add(miceRequest);
                    }
                }

            }

            return requestForProposal;
        }

        ///INSPIRE-102
        ///This method is update rfp records, submit rfp records and call MakeRequestForProposalFromStartRfp method for store new rfp into request for proposal related tables
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        public RequestForProposalModel SubmitRFP(RequestForProposalModel rfpModel, bool isSubmit, int userId)
        {
            var requestForProposal = new RequestForProposalModel();

            var rfp = _context.RequestForProposals.FirstOrDefault(r => r.Id == rfpModel.Id);

            if (rfp != null)
            {
                // Saved the data
                rfp.is_submitted = isSubmit;
                rfp.rfp_type = rfpModel.RFPType;
                rfp.days_running = rfpModel.DaysRunning;
                rfp.event_name = rfpModel.EventName;
                rfp.modified_by = rfpModel.ModifiedBy;
                rfp.modified_date = rfpModel.ModifiedDate;
                rfp.eventstartdate = rfpModel.EventStartDate;
                rfp.eventenddate = rfpModel.EventEndDate;
                _context.SaveChanges();

                foreach (var rfpMiceRequest in rfpModel.RFPMiceRequests)
                {
                    var rfpmr = _context.RFPMiceRequests.FirstOrDefault(r => r.Id == rfpMiceRequest.Id);
                    rfpmr.comments = rfpMiceRequest.Comments;
                    rfpmr.comments2 = rfpMiceRequest.Comments2;
                    rfpmr.comments3 = rfpMiceRequest.Comments3;
                    rfpmr.comments4 = rfpMiceRequest.Comments4;
                    rfpmr.city_id = rfpMiceRequest.CityId;
                    rfpmr.country_id = rfpMiceRequest.CountryId;
                    rfpmr.place_id = rfpMiceRequest.PlaceId;
                    rfpmr.modified_by = rfpMiceRequest.ModifiedBy;
                    rfpmr.modified_date = rfpMiceRequest.ModifiedDate;
                    _context.SaveChanges();
                }

                requestForProposal = new RequestForProposalModel
                {
                    CreatedBy = rfp.created_by,
                    CreatedDate = rfp.created_date,
                    DaysRunning = rfp.days_running,
                    Id = rfp.Id,
                    IsSubmitted = rfp.is_submitted,
                    MiceUserId = rfp.mice_user_id,
                    ModifiedBy = rfp.modified_by,
                    ModifiedDate = rfp.modified_date,
                    RFPType = rfp.rfp_type,
                    Status = rfp.status,
                    UserId = rfp.user_id
                };

                var rfpMiceRequests = _context.RFPMiceRequests.Where(mr => mr.rfp_id == requestForProposal.Id).ToList();

                var rfpMiceRequestIds = rfpMiceRequests.Select(mr => mr.Id).ToList();

                var rfpResourceEntities = _context.RFPResources.Where(rr => rfpMiceRequestIds.Contains(rr.rfp_mice_request_id)).ToList();

                var rfpMiceRequestItemEntities = _context.RFPMiceRequestItems.Where(ri => rfpMiceRequestIds.Contains(ri.rfp_mice_request_id)).ToList();

                if (rfpMiceRequests.Any())
                {
                    requestForProposal.RFPMiceRequests = new List<RFPMiceRequestModel>();
                    foreach (var mre in rfpMiceRequests)
                    {
                        var miceRequest = new RFPMiceRequestModel
                        {
                            CityId = mre.city_id,
                            Comments = mre.comments,
                            Comments2 = mre.comments2,
                            Comments3 = mre.comments3,
                            Comments4 = mre.comments4,
                            CountryId = mre.country_id,
                            CreatedBy = mre.created_by,
                            CreatedDate = mre.created_date,
                            Id = mre.Id,
                            MiceRequestId = mre.mice_request_id,
                            ModifiedBy = mre.modified_by,
                            ModifiedDate = mre.modified_date,
                            PlaceId = mre.place_id,
                            ResourceType = mre.resource_type,
                            RfpId = mre.rfp_id,
                            Status = mre.status
                        };
                        var rfpResources = rfpResourceEntities.Where(rr => rr.rfp_mice_request_id == mre.Id).ToList();
                        if (rfpResources.Any())
                        {
                            miceRequest.RFPResources = rfpResources.Select(r => new RFPResourceModel
                            {
                                CreatedBy = r.created_by,
                                CreatedDate = r.created_date,
                                Id = r.Id,
                                MiceResourceId = r.mice_resource_id,
                                ModifiedBy = r.modified_by,
                                ModifiedDate = r.modified_date,
                                ResourceId = r.resource_id,
                                RFPMiceRequestId = r.rfp_mice_request_id,
                                Status = r.status
                            }).ToList();
                        }
                        var rfpMiceRequestItems = rfpMiceRequestItemEntities.Where(ri => ri.rfp_mice_request_id == mre.Id).ToList();
                        if (rfpMiceRequestItems.Any())
                        {
                            miceRequest.RFPMiceRequestItems = rfpMiceRequestItems.Select(rm => new RequestItemModel
                            {
                                CreatedBy = rm.created_by,
                                CreatedDate = rm.created_date,
                                Id = rm.Id,
                                ModifiedBy = rm.modified_by,
                                ModifiedDate = rm.modified_date,
                                RequestItems = JsonConvert.DeserializeObject<List<RequestItemObject>>(rm.request_item_object),
                                RequestItemType = rm.request_item_type,
                                RFPId = rfp.Id,
                                RFPMiceRequestId = rm.rfp_mice_request_id
                            }).ToList();
                        }
                        requestForProposal.RFPMiceRequests.Add(miceRequest);
                    }
                }
            }
            else
            {
                rfpModel.UserId = userId;
                rfpModel = MakeRequestForProposalFromStartRfp(rfpModel);
            }

            // Mice Flow
            if (requestForProposal.RFPMiceRequests != null && requestForProposal.RFPMiceRequests.Any())
            {
                CustomerModel customer = new CustomerModel();
                //if (!requestForProposal.MiceUserId.HasValue || requestForProposal.MiceUserId == 0)
                //{
                var customerEntity = (from um in _context.User_Masters
                                      join uc in _context.User_Credential on um.User_Id equals uc.User_Master_Id into ucre
                                      from ucredential in ucre.DefaultIfEmpty()
                                      join ucc in _context.User_Communication on um.User_Id equals ucc.User_Master_Id into ucomm
                                      from ucommunication in ucomm.DefaultIfEmpty()
                                      where um.User_Id == userId
                                      select new
                                      {
                                          um,
                                          ucredential,
                                          ucommunication
                                      }).FirstOrDefault();
                var countryCode = "";
                if (customerEntity.ucommunication != null && customerEntity.ucommunication.Country_Id > 0)
                {
                    var country = _context.List_Country.FirstOrDefault(c => c.Id == customerEntity.ucommunication.Country_Id);
                    if (country != null)
                    {
                        countryCode = country.country_code;
                    }
                }
                customer = new CustomerModel
                {
                    AddressCountryCode = countryCode,
                    AddressLocation = customerEntity.ucommunication != null ? customerEntity.ucommunication.Place : "",
                    AddressStreet = customerEntity.ucommunication != null ? customerEntity.ucommunication.Street : "",
                    AddressTelephone = customerEntity.ucommunication != null ? customerEntity.ucommunication.Telephone : "",
                    AddressZipCode = customerEntity.ucommunication != null ? customerEntity.ucommunication.PostCode : "",
                    Email = customerEntity.ucredential != null ? customerEntity.ucredential.Email : "",
                    FirstName = customerEntity.um.First_Name,
                    IsCommercial = !string.IsNullOrWhiteSpace(customerEntity.um.Company_Name),
                    LastName = customerEntity.um.Last_Name,
                    Name = customerEntity.um.Company_Name,
                    Salutation = customerEntity.um.Salutation
                };
                //}

                if (isSubmit)
                {
                    // Mice Request For Hotels
                    var hotelMiceRequestModel = requestForProposal.RFPMiceRequests.FirstOrDefault(mr => mr.ResourceType == (int)ManageEnum.resourcetype.hotel);
                    if (hotelMiceRequestModel != null)
                    {
                        var miceRequestHotelAPIResponse = CreateMiceRequest(requestForProposal, hotelMiceRequestModel, customer);
                        if (miceRequestHotelAPIResponse != null)
                        {
                            if (!requestForProposal.MiceUserId.HasValue || requestForProposal.MiceUserId == 0)
                            {
                                requestForProposal.MiceUserId = miceRequestHotelAPIResponse.UserId;
                                var userCredential = _context.User_Credential.FirstOrDefault(uc => uc.User_Master_Id == requestForProposal.UserId);
                                if (userCredential != null)
                                {
                                    userCredential.Mice_Userid = miceRequestHotelAPIResponse.UserId;
                                    userCredential.Mice_Username = miceRequestHotelAPIResponse.UserName;
                                    userCredential.Mice_Password = miceRequestHotelAPIResponse.Password;
                                }
                                rfp.mice_user_id = miceRequestHotelAPIResponse.UserId;
                            }
                            var hotelMiceReq = _context.RFPMiceRequests.FirstOrDefault(mr => mr.Id == hotelMiceRequestModel.Id);
                            if (hotelMiceReq != null)
                            {
                                hotelMiceReq.mice_request_id = miceRequestHotelAPIResponse.RequestId;
                                
                            }

                            if(miceRequestHotelAPIResponse.RequestId==0)
                            {
                                hotelMiceReq.status = ManageEnum.status.fail.ToString();
                            }
                            _context.SaveChanges();

                            try
                            {
                                DCPMiceRequests miceRequests = SetDCPMiceRequestModel(hotelMiceRequestModel.RfpId, hotelMiceReq.mice_request_id, JsonConvert.SerializeObject(hotelMiceRequestModel), hotelMiceReq.resource_type, (int)ManageEnum.requestType.rfp, userId);
                                int dcpMiceRequestId = AddDCPMiceRequest(miceRequests);
                            }
                            catch (Exception ex)
                            {
                                WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
                            }

                        }
                        else
                        {
                            var hotelMiceReq = _context.RFPMiceRequests.FirstOrDefault(mr => mr.Id == hotelMiceRequestModel.Id);
                            if (hotelMiceReq != null)
                            {
                                hotelMiceReq.mice_request_id = 0;
                                hotelMiceReq.status = ManageEnum.status.fail.ToString();
                            }
                            _context.SaveChanges();
                        }
                    }

                    // Mice Request For Restaurants
                    var restaurantMiceRequestModel = requestForProposal.RFPMiceRequests.FirstOrDefault(mr => mr.ResourceType == (int)ManageEnum.resourcetype.restaurant);
                    if (restaurantMiceRequestModel != null)
                    {
                        var miceRequestRestaurantAPIResponse = CreateMiceRequest(requestForProposal, restaurantMiceRequestModel, customer);
                        if (miceRequestRestaurantAPIResponse != null)
                        {
                            if (!requestForProposal.MiceUserId.HasValue || requestForProposal.MiceUserId == 0)
                            {
                                requestForProposal.MiceUserId = miceRequestRestaurantAPIResponse.UserId;
                                var userCredential = _context.User_Credential.FirstOrDefault(uc => uc.User_Master_Id == requestForProposal.UserId);
                                if (userCredential != null)
                                {
                                    userCredential.Mice_Userid = miceRequestRestaurantAPIResponse.UserId;
                                    userCredential.Mice_Username = miceRequestRestaurantAPIResponse.UserName;
                                    userCredential.Mice_Password = miceRequestRestaurantAPIResponse.Password;
                                }
                                rfp.mice_user_id = miceRequestRestaurantAPIResponse.UserId;
                            }
                            var restaurentMiceReq = _context.RFPMiceRequests.FirstOrDefault(mr => mr.Id == restaurantMiceRequestModel.Id);
                            if (restaurentMiceReq != null)
                            {
                                restaurentMiceReq.mice_request_id = miceRequestRestaurantAPIResponse.RequestId;
                            }

                            if(miceRequestRestaurantAPIResponse.RequestId==0)
                            {
                                restaurentMiceReq.status = ManageEnum.status.fail.ToString();
                            }
                            _context.SaveChanges();

                            try
                            {
                                DCPMiceRequests miceRequests = SetDCPMiceRequestModel(restaurantMiceRequestModel.RfpId, restaurentMiceReq.mice_request_id, JsonConvert.SerializeObject(restaurantMiceRequestModel), restaurantMiceRequestModel.ResourceType, (int)ManageEnum.requestType.rfp, userId);
                                int dcpMiceRequestId = AddDCPMiceRequest(miceRequests);
                            }
                            catch (Exception ex)
                            {
                                WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
                            }
                        }
                        else
                        {
                            var restaurentMiceReq = _context.RFPMiceRequests.FirstOrDefault(mr => mr.Id == restaurantMiceRequestModel.Id);
                            if (restaurentMiceReq != null)
                            {
                                restaurentMiceReq.mice_request_id = 0;
                                restaurentMiceReq.status = ManageEnum.status.fail.ToString();
                            }
                            _context.SaveChanges();
                        }
                    }

                    // Mice Request For Location
                    var locationtMiceRequestModel = requestForProposal.RFPMiceRequests.FirstOrDefault(mr => mr.ResourceType == (int)ManageEnum.resourcetype.location);
                    if (locationtMiceRequestModel != null)
                    {
                        var miceRequestLocationAPIResponse = CreateMiceRequest(requestForProposal, locationtMiceRequestModel, customer);
                        if (miceRequestLocationAPIResponse != null)
                        {
                            if (!requestForProposal.MiceUserId.HasValue || requestForProposal.MiceUserId == 0)
                            {
                                requestForProposal.MiceUserId = miceRequestLocationAPIResponse.UserId;
                                var userCredential = _context.User_Credential.FirstOrDefault(uc => uc.User_Master_Id == requestForProposal.UserId);
                                if (userCredential != null)
                                {
                                    userCredential.Mice_Userid = miceRequestLocationAPIResponse.UserId;
                                    userCredential.Mice_Username = miceRequestLocationAPIResponse.UserName;
                                    userCredential.Mice_Password = miceRequestLocationAPIResponse.Password;
                                }
                                rfp.mice_user_id = miceRequestLocationAPIResponse.UserId;
                            }
                            var locationMiceReq = _context.RFPMiceRequests.FirstOrDefault(mr => mr.Id == locationtMiceRequestModel.Id);
                            if (locationMiceReq != null)
                            {
                                locationMiceReq.mice_request_id = miceRequestLocationAPIResponse.RequestId;
                            }

                            if (miceRequestLocationAPIResponse.RequestId==0)
                            {
                                locationMiceReq.status = ManageEnum.status.fail.ToString();
                            }
                            _context.SaveChanges();

                            try
                            {
                                DCPMiceRequests miceRequests = SetDCPMiceRequestModel(locationtMiceRequestModel.RfpId, locationMiceReq.mice_request_id, JsonConvert.SerializeObject(locationtMiceRequestModel), locationtMiceRequestModel.ResourceType, (int)ManageEnum.requestType.rfp, userId);
                                int dcpMiceRequestId = AddDCPMiceRequest(miceRequests);
                            }
                            catch (Exception ex)
                            {
                                WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
                            }
                        }
                        else
                        {
                            var locationMiceReq = _context.RFPMiceRequests.FirstOrDefault(mr => mr.Id == locationtMiceRequestModel.Id);
                            if (locationMiceReq != null)
                            {
                                locationMiceReq.mice_request_id = 0;
                                locationMiceReq.status = ManageEnum.status.fail.ToString();
                            }
                            _context.SaveChanges();
                        }
                    }

                    //DCPMiceRequests requests = new DCPMiceRequests();
                    // requests.mice_request_id = rfp.RFPMiceRequests.Where(t=>t.rfp_id==)
                }

                var cart = _context.RequestCarts.FirstOrDefault(rc => rc.user_id == userId);

                if (rfpModel.Id==0 && cart != null)
                {
                    var resourcedata = _context.RequestCartResources.Where(x => x.request_cart_id == cart.Id).ToList();

                    if (resourcedata != null)
                    {
                        _context.RequestCartResources.RemoveRange(resourcedata);
                    }
                    _context.RequestCarts.Remove(cart);
                }
                _context.SaveChanges();
            }

           

            rfpModel.strId = Crypto.EnryptString(rfpModel.Id.ToString());

            //29-04-2020 - Change - set value for status wise display message.
            if (isSubmit)
            {
                var rfpMiceRequest = _context.RFPMiceRequests.Where(mr => mr.rfp_id == requestForProposal.Id).ToList();

                if(rfpMiceRequest!=null)
                {
                    foreach (var item in rfpMiceRequest)
                    {
                       var miceRequest = rfpModel.RFPMiceRequests.Where(t => t.Id == item.Id).FirstOrDefault();
                        if(miceRequest!=null)
                        {
                            miceRequest.ResourceType = item.resource_type;
                            miceRequest.MiceRequestId = item.mice_request_id;
                            miceRequest.Status = item.status;
                        }
                    }
                }
            }

            return rfpModel;
        }

        public Dictionary<string,object> ValidateSubmitRFP(RequestForProposalModel rfpModel)
        {
            var requestForProposal = _context.RequestForProposals.FirstOrDefault(r => r.Id == rfpModel.Id);
            var rfpMiceRequests = _context.RFPMiceRequests.Where(mr => mr.rfp_id == requestForProposal.Id).ToList();
            Dictionary<string, object> result = new Dictionary<string, object>();
            foreach (var item in rfpMiceRequests)
            {
                int count = _context.RFPMiceRequestItems.Where(ri => ri.rfp_mice_request_id == item.Id).Count();
                if(count==0)
                {
                    result.Add(item.resource_type.ToString(), count);
                }
                
            }

            return result;
        }

        private ResponseRequestConfirmationModel CreateMiceRequest(RequestForProposalModel requestForProposal, RFPMiceRequestModel miceRequestModel, CustomerModel customer)
        {
            if (miceRequestModel != null)
            {
                var miceRequestForHotel = new RequestModel();
                var resourceIds = miceRequestModel.RFPResources.Select(r => r.MiceResourceId).ToList();

                miceRequestForHotel.HotelIds = resourceIds;
                miceRequestForHotel.Type = requestForProposal.RFPType;
                miceRequestForHotel.UserId = requestForProposal.MiceUserId.HasValue ? requestForProposal.MiceUserId.Value : 0;
                //if (miceRequestForHotel.UserId == 0)
                //{
                //}
                miceRequestForHotel.Customer = customer;

                miceRequestForHotel.Comments = miceRequestModel.Comments;
                miceRequestForHotel.Comments2 = miceRequestModel.Comments2;
                miceRequestForHotel.Comments3 = miceRequestModel.Comments3;
                miceRequestForHotel.Comments4 = miceRequestModel.Comments4;
                miceRequestForHotel.CityId = miceRequestModel.CityId.HasValue ? miceRequestModel.CityId.Value : 0;
                miceRequestForHotel.PlaceId = miceRequestModel.PlaceId.HasValue ? miceRequestModel.PlaceId.Value : 0;
                miceRequestForHotel.CountryId = miceRequestModel.CountryId.HasValue ? miceRequestModel.CountryId.Value : 0;
                miceRequestForHotel.Attachment = "";
                miceRequestForHotel.DaysRunning = requestForProposal.DaysRunning.HasValue ? requestForProposal.DaysRunning.Value : 0;

                if (miceRequestModel.RFPMiceRequestItems != null && miceRequestModel.RFPMiceRequestItems.Any())
                {
                    miceRequestForHotel.RequestItems = new List<RequestItem>();
                    foreach (var miceReqItem in miceRequestModel.RFPMiceRequestItems)
                    {
                        var reqItems = miceReqItem.RequestItems.Select(ri => new RequestItem
                        {
                            Budget = ri.Budget,
                            Comments = ri.Comments,
                            Food = ri.Food != null ? new Mice.Models.ConferenceFood
                            {
                                EveningDinnerBuffet = ri.Food.EveningDinnerBuffet,
                                EveningDinnerOneDish = ri.Food.EveningDinnerOneDish,
                                EveningDinnerSnack = ri.Food.EveningDinnerSnack,
                                EveningDinnerThreeCourseMeal = ri.Food.EveningDinnerThreeCourseMeal,
                                EveningDinnerTwoCourseMeal = ri.Food.EveningDinnerTwoCourseMeal,
                                EveningDrinkFlat = ri.Food.EveningDrinkFlat,
                                EveningOneDrink = ri.Food.EveningOneDrink,
                                GalaDinner = ri.Food.GalaDinner,
                                MiddayCoffeeAndTeaBreak = ri.Food.MiddayCoffeeAndTeaBreak,
                                MiddayDrinksFlat = ri.Food.MiddayDrinksFlat,
                                MiddayLunchBuffet = ri.Food.MiddayLunchBuffet,
                                MiddayLunchOneDish = ri.Food.MiddayLunchOneDish,
                                MiddayLunchSnack = ri.Food.MiddayLunchSnack,
                                MiddayLunchThreeCourseMeal = ri.Food.MiddayLunchThreeCourseMeal,
                                MiddayLunchTwoCourseMeal = ri.Food.MiddayLunchTwoCourseMeal,
                                MiddayOneDrink = ri.Food.MiddayOneDrink,
                                MorningCoffeeAndTea = ri.Food.MorningCoffeeAndTea,
                                MorningCoffeeAndTeaBreak = ri.Food.MorningCoffeeAndTeaBreak,
                                MorningDrinksFlat = ri.Food.MorningDrinksFlat,
                                MorningTwoDrinks = ri.Food.MorningTwoDrinks,
                                MorningWelcomeCoffeeAndTea = ri.Food.MorningWelcomeCoffeeAndTea,
                                SecondCoffeeBreak = ri.Food.SecondCoffeeBreak
                            } : null,
                            Quantity = ri.Quantity,
                            ReserveFromDate = ri.ReserveFromDate,
                            ReserveToDate = ri.ReserveToDate,
                            RoomSize = ri.RoomSize,
                            RoomType = ri.RoomType,
                            SeatingType = ri.SeatingType,
                            Type = ri.Type,
                            Technique = ri.Technique != null ? new Mice.Models.RoomTechnique
                            {
                                Beamer = ri.Technique.Beamer,
                                Canvas = ri.Technique.Canvas,
                                FlipChart = ri.Technique.FlipChart,
                                Internet = ri.Technique.Internet,
                                ISDN = ri.Technique.ISDN,
                                Microphone = ri.Technique.Microphone,
                                OverHeadProjector = ri.Technique.OverHeadProjector,
                                PensAndNotepads = ri.Technique.PensAndNotepads,
                                PresentationCase = ri.Technique.PresentationCase,
                                Rostrum = ri.Technique.Rostrum,
                                SoundSystem = ri.Technique.SoundSystem,
                                Wall = ri.Technique.Wall,
                                WirelessMicrophone = ri.Technique.WirelessMicrophone
                            } : null,
                        }).ToList();

                        miceRequestForHotel.RequestItems.AddRange(reqItems);
                    }
                }

                try
                {
                    //28-04-2020 - Change due to wrong entry of resource_type in DCPMiceRequests table
                    var resource_type = miceRequestModel.ResourceType; // requestForProposal.RFPMiceRequests.FirstOrDefault().ResourceType;
                    DCPMiceRequests miceRequests = SetDCPMiceRequestModel(requestForProposal.Id, null, JsonConvert.SerializeObject(miceRequestForHotel), resource_type, (int)ManageEnum.requestType.rfp, miceRequestForHotel.UserId);
                    int dcpMiceRequestId = AddDCPMiceRequest(miceRequests);
                }
                catch (Exception ex)
                {
                    WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
                }


                var miceAPI = new MiceRequestAPI(ManageSetting.MiceAgencyName, ManageSetting.MiceAgencyPassword);
                var apiResponse = miceAPI.CreateNewRequest(miceRequestForHotel);

                return apiResponse;
            }
            return null;
        }

        ///INSPIRE-102
        ///This method is return resource item list for selected rfp
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="langCode"></param>
        /// <param name="sortBy"></param>
        /// <param name="type"></param>
        /// <param name="requestItemType"></param>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        public List<RequestItemObject> GetResourceItemDetail(int userId, string langCode, string sortBy, string type, string requestItemType, int rfpId)
        {
            int resource_type = 0;

            if (type == "H")
            {
                resource_type = 0;
            }
            else if (type == "R")
            {
                resource_type = 1;
            }
            else if (type == "L")
            {
                resource_type = 2;
            }



            List<RFPMiceRequestModel> requestModel = new List<RFPMiceRequestModel>();
            bool isGerman = langCode == "de-DE";



            var rfpMiceRequests = (from rp in _context.RequestForProposals
                                   join rq in _context.RFPMiceRequests on rp.Id equals rq.rfp_id
                                   where rp.Id == rfpId && rp.user_id == userId && rq.resource_type == resource_type //rp.is_submitted == false &&
                                   select rq).FirstOrDefault();

            var rfpMiceRequestItemEntities = _context.RFPMiceRequestItems.Where(ri => ri.rfp_mice_request_id == rfpMiceRequests.Id && ri.request_item_type != null && ri.request_item_type == requestItemType).ToList();

            List<RequestItemObject> requestItemObjects = new List<RequestItemObject>();

            foreach (var item in rfpMiceRequestItemEntities)
            {
                RequestItemObject itemObject = JsonConvert.DeserializeObject<List<RequestItemObject>>(item.request_item_object)[0];
                itemObject.Id = item.Id;
                requestItemObjects.Add(itemObject);
            }

            if (requestItemObjects != null)
            {
                if (sortBy.ToLower() == "roomtypeasc")
                {
                    requestItemObjects = requestItemObjects.OrderBy(t => t.DisplayRoomType).ToList();
                }
                else if (sortBy.ToLower() == "roomtypedesc")
                {
                    requestItemObjects = requestItemObjects.OrderByDescending(t => t.DisplayRoomType).ToList();
                }
                else if (sortBy.ToLower() == "quantityasc" || sortBy.ToLower() == "cquantityasc" || sortBy.ToLower() == "gquantityasc" || sortBy.ToLower() == "rcquantityasc" || sortBy.ToLower() == "rgquantityasc" || sortBy.ToLower() == "lgquantityasc" || sortBy.ToLower() == "lcquantityasc")
                {
                    requestItemObjects = requestItemObjects.OrderBy(t => t.Quantity).ToList();
                }
                else if (sortBy.ToLower() == "quantitydesc" || sortBy.ToLower() == "cquantitydesc" || sortBy.ToLower() == "gquantitydesc" || sortBy.ToLower() == "rcquantitydesc" || sortBy.ToLower() == "rgquantitydesc" || sortBy.ToLower() == "lgquantitydesc" || sortBy.ToLower() == "lcquantitydesc")
                {
                    requestItemObjects = requestItemObjects.OrderByDescending(t => t.Quantity).ToList();
                }
                else if (sortBy.ToLower() == "DisplayReserveFromDateasc".ToLower())
                {
                    requestItemObjects = requestItemObjects.OrderBy(t => t.ReserveFromDate).ToList();
                }
                else if (sortBy.ToLower() == "DisplayReserveFromDatedesc".ToLower())
                {
                    requestItemObjects = requestItemObjects.OrderByDescending(t => t.ReserveFromDate).ToList();
                }
                else if (sortBy.ToLower() == "DisplayReserveToDateasc".ToLower())
                {
                    requestItemObjects = requestItemObjects.OrderBy(t => t.ReserveToDate).ToList();
                }
                else if (sortBy.ToLower() == "DisplayReserveToDatedesc".ToLower())
                {
                    requestItemObjects = requestItemObjects.OrderByDescending(t => t.ReserveToDate).ToList();
                }
                else if (sortBy.ToLower() == "budgetasc" || sortBy.ToLower() == "cbudgetasc" || sortBy.ToLower() == "gbudgetasc" || sortBy.ToLower() == "rcbudgetasc" || sortBy.ToLower() == "rgbudgetasc" || sortBy.ToLower() == "lgbudgetasc" || sortBy.ToLower() == "lcbudgetasc")
                {
                    requestItemObjects = requestItemObjects.OrderBy(t => t.Budget).ToList();
                }
                else if (sortBy.ToLower() == "budgetdesc" || sortBy.ToLower() == "cbudgetdesc" || sortBy.ToLower() == "gbudgetdesc" || sortBy.ToLower() == "rcbudgetdesc" || sortBy.ToLower() == "rgbudgetdesc" || sortBy.ToLower() == "lgbudgetdesc" || sortBy.ToLower() == "lcbudgetdesc")
                {
                    requestItemObjects = requestItemObjects.OrderByDescending(t => t.Budget).ToList();
                }
                else if (sortBy.ToLower() == "roomsizeasc" || sortBy.ToLower() == "groomsizeasc" || sortBy.ToLower() == "rcroomsizeasc" || sortBy.ToLower() == "rgroomsizeasc" || sortBy.ToLower() == "lgroomsizeasc" || sortBy.ToLower() == "lcroomsizeasc")
                {
                    requestItemObjects = requestItemObjects.OrderBy(t => t.RoomSize).ToList();
                }
                else if (sortBy.ToLower() == "roomsizedesc" || sortBy.ToLower() == "groomsizedesc" || sortBy.ToLower() == "rcroomsizedesc" || sortBy.ToLower() == "rgroomsizedesc" || sortBy.ToLower() == "lgroomsizedesc" || sortBy.ToLower() == "lcroomsizedesc")
                {
                    requestItemObjects = requestItemObjects.OrderByDescending(t => t.RoomSize).ToList();
                }
                else if (sortBy.ToLower() == "displaydatetimeasc".ToLower() || sortBy.ToLower() == "gdisplaydatetimeasc".ToLower() || sortBy.ToLower() == "rcdisplaydatetimeasc".ToLower() || sortBy.ToLower() == "rgdisplaydatetimeasc".ToLower() || sortBy.ToLower() == "lgdisplaydatetimeasc".ToLower() || sortBy.ToLower() == "lcdisplaydatetimeasc".ToLower())
                {
                    requestItemObjects = requestItemObjects.OrderBy(t => t.ReserveFromDate).ToList();
                }
                else if (sortBy.ToLower() == "displaydatetimedesc".ToLower() || sortBy.ToLower() == "gdisplaydatetimedesc".ToLower() || sortBy.ToLower() == "rcdisplaydatetimedesc".ToLower() || sortBy.ToLower() == "rgdisplaydatetimedesc".ToLower() || sortBy.ToLower() == "lgdisplaydatetimedesc".ToLower() || sortBy.ToLower() == "lcdisplaydatetimedesc".ToLower())
                {
                    requestItemObjects = requestItemObjects.OrderByDescending(t => t.ReserveFromDate).ToList();
                }
                else if (sortBy.ToLower() == "seatingtypeasc".ToLower() || sortBy.ToLower() == "gseatingtypeasc".ToLower() || sortBy.ToLower() == "rcseatingtypeasc".ToLower() || sortBy.ToLower() == "rgseatingtypeasc".ToLower() || sortBy.ToLower() == "lgseatingtypeasc".ToLower() || sortBy.ToLower() == "lcseatingtypeasc".ToLower())
                {
                    requestItemObjects = requestItemObjects.OrderBy(t => t.SeatingType).ToList();
                }
                else if (sortBy.ToLower() == "seatingtypedesc".ToLower() || sortBy.ToLower() == "gseatingtypedesc".ToLower() || sortBy.ToLower() == "rcseatingtypedesc".ToLower() || sortBy.ToLower() == "rgseatingtypedesc".ToLower() || sortBy.ToLower() == "lgseatingtypedesc".ToLower() || sortBy.ToLower() == "lcseatingtypedesc".ToLower())
                {
                    requestItemObjects = requestItemObjects.OrderByDescending(t => t.SeatingType).ToList();
                }

            }
            return requestItemObjects;
        }

        #endregion RFP

        #region MiceAPI Call

        public OrderResponseModel BookedOrder(OrderBookingModel bookingModel,int userId)
        {
            RFPMiceRequest request = new RFPMiceRequest();
            request = _context.RFPMiceRequests.Where(t => t.mice_request_id == bookingModel.Micerequestid).FirstOrDefault();

            try
            {
                DCPMiceRequests miceRequests = SetDCPMiceRequestModel(request.rfp_id, bookingModel.Micerequestid, JsonConvert.SerializeObject(bookingModel), request.resource_type, (int)ManageEnum.requestType.orderRequest, bookingModel.UserId);
                RequestRepository requestRepository = new RequestRepository();
                requestRepository.AddDCPMiceRequest(miceRequests);
            }
            catch (Exception ex)
            {
                WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
            }

            var miceAPI = new MiceRequestAPI(ManageSetting.MiceAgencyName, ManageSetting.MiceAgencyPassword);
            var apiResponse = miceAPI.BookedOrder(bookingModel);
            var RFPMiceRequestOrders = _context.RFPMiceRequestOrders.Where(x => x.mice_order_id == bookingModel.Micerequestid).FirstOrDefault();
            bookingModel.OrderDetailModel = miceAPI.GetOrderDetail(bookingModel.Micerequestid);
           
            try
            {
                if (RFPMiceRequestOrders == null)
                {
                    //29-04-2020 - Change due to error in insert record into table
                    int id = 0;
                    RFPMiceRequest miceRequest = _context.RFPMiceRequests.Where(t => t.mice_request_id == bookingModel.Micerequestid).FirstOrDefault();
                    if (miceRequest != null)
                    {
                        id = miceRequest.Id;
                    }

                    RFPMiceRequestOrders = new RFPMiceRequestOrders
                    {
                        
                        //mice_order_id = apiResponse.Id,
                        //rfp_mice_request_id = bookingModel.Micerequestid,
                        mice_order_id = bookingModel.Micerequestid, 
                        rfp_mice_request_id = id,
                        status = apiResponse.Status,
                        created_date = DateTime.Now,
                        created_by = userId,
                        order_detail_object = JsonConvert.SerializeObject(bookingModel.OrderDetailModel)
                    };
                    _context.RFPMiceRequestOrders.Add(RFPMiceRequestOrders);
                    _context.SaveChanges();
                }

                DCPMiceRequests miceRequests = SetDCPMiceRequestModel(request.rfp_id, bookingModel.Micerequestid, JsonConvert.SerializeObject(apiResponse), request.resource_type, (int)ManageEnum.requestType.orderResponse, bookingModel.UserId);
                RequestRepository requestRepository = new RequestRepository();
                requestRepository.AddDCPMiceRequest(miceRequests);
            }
            catch (Exception ex)
            {
                WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
            }

            return apiResponse;
        }

        public OrderResponseModel AbortOrder(OrderAbortModel abortModel)
        {
            RFPMiceRequestModel requestModel = new RFPMiceRequestModel();

            RFPMiceRequest request = _context.RFPMiceRequests.Where(t => t.mice_request_id == abortModel.RequestId).FirstOrDefault();
            if (request != null)
            {
                Dictionary<string, object> keyValues = new Dictionary<string, object>();
                requestModel.RfpId = request.rfp_id;
                requestModel.MiceRequestId = request.mice_request_id;
                requestModel.ResourceType = request.resource_type;
                requestModel.CreatedBy = request.created_by;

            }

            try
            {
                DCPMiceRequests miceRequests = SetDCPMiceRequestModel(requestModel.RfpId, requestModel.MiceRequestId, JsonConvert.SerializeObject(abortModel), requestModel.ResourceType, (int)ManageEnum.requestType.abortRequest, abortModel.UserId);
                RequestRepository requestRepository = new RequestRepository();
                requestRepository.AddDCPMiceRequest(miceRequests);
            }
            catch (Exception ex)
            {
                WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
            }

            var miceAPI = new MiceRequestAPI(ManageSetting.MiceAgencyName, ManageSetting.MiceAgencyPassword);
            var apiResponse = miceAPI.AbortOrder(abortModel);

            try
            {
                DCPMiceRequests miceRequests = SetDCPMiceRequestModel(requestModel.RfpId, requestModel.MiceRequestId, JsonConvert.SerializeObject(apiResponse), requestModel.ResourceType, (int)ManageEnum.requestType.abortResponse, abortModel.UserId);
                RequestRepository requestRepository = new RequestRepository();
                requestRepository.AddDCPMiceRequest(miceRequests);
            }
            catch (Exception ex)
            {
                WriteLog.LogInformation(LogMessages.ErrorInAddDCPMiceRequest + " : " + ex);
            }

            return apiResponse;

        }

        #endregion

        public int AddDCPMiceRequest(DCPMiceRequests requests)
        {
            DCPMiceRequests miceRequests = new DCPMiceRequests();
            miceRequests.rfp_id = requests.rfp_id;
            miceRequests.mice_request_id = requests.mice_request_id;
            miceRequests.request_detail_object = requests.request_detail_object;
            miceRequests.resource_type = requests.resource_type;
            miceRequests.request_type = requests.request_type;
            miceRequests.created_by = requests.created_by;
            miceRequests.created_date = DateTime.Now;
            _context.DCPMiceRequests.Add(miceRequests); ;
            _context.SaveChanges();

            return miceRequests.Id;
        }

        private DCPMiceRequests SetDCPMiceRequestModel(int rfpId, int? miceRequestId, string detail, int resourceType, int requestType, int userId)
        {
            DCPMiceRequests miceRequests = new DCPMiceRequests();

            miceRequests.rfp_id = rfpId;
            miceRequests.mice_request_id = miceRequestId;
            miceRequests.request_detail_object = detail;
            miceRequests.resource_type = resourceType;
            miceRequests.request_type = requestType;
            miceRequests.created_by = userId;
            miceRequests.created_date = DateTime.Now;
            return miceRequests;
        }

    }
}
