﻿using System;

using System.Collections.Generic;

using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace DCPortalAPI.Filter
{
    /// <summary>
    /// Exception Handler
    /// </summary>
    public class ExceptionHandler : ExceptionFilterAttribute  //ActionFilterAttribute, IExceptionFilter
    {
        /// <summary>
        /// Exception Handler
        /// </summary>
        /// <param name="context"></param>
        public override void OnException(HttpActionExecutedContext context)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(context.Exception);
        }

        //public void OnException(ExceptionContext filterContext)
        //{

        //    if (filterContext.HttpContext.Request.IsAjaxRequest() && filterContext.Exception != null)
        //    {
        //        filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        //        filterContext.Result = new JsonResult
        //        {
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet,
        //            Data = new
        //            {
        //                filterContext.Exception.Message,
        //                filterContext.Exception.StackTrace
        //            }
        //        };
        //        filterContext.ExceptionHandled = true;
        //    }
        //    else
        //    {
        //        Exception e = filterContext.Exception;
        //        filterContext.ExceptionHandled = true;
        //        filterContext.Result = new ViewResult()
        //        {
        //            ViewName = "Error"
        //        };
        //    }


        //}




    }
    
}