﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace DCPortalAPI.Settings
{
    /// <summary>
    /// 
    /// </summary>
    public class Constants
    {
        public static readonly string APIBaseURL = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["APIBaseURL"]) ? ConfigurationManager.AppSettings["APIBaseURL"] : "localhost";
        public static readonly string TokenKey = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["TokenKey"]) ? ConfigurationManager.AppSettings["TokenKey"] : "29090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
        public static readonly string TokenExpInMin = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["TokenExpInMin"]) ? Convert.ToString(ConfigurationManager.AppSettings["TokenExpInMin"]) : "60";
        public static readonly string richTextFontCSS = "b { font-size: x-small; } p { font-size: x-small; } div { font-size: x-small; } span { font-size: x-small;}";
        public static readonly string TotalUser = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["TotalUser"]) ? (ConfigurationManager.AppSettings["TotalUser"]) : "";

        public static readonly string Host = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["Host"]) ? ConfigurationManager.AppSettings["Host"] : "";
        public static readonly string Port = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["Port"]) ? ConfigurationManager.AppSettings["Port"] : "";
        public static readonly string FromMail = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["FromMail"]) ? ConfigurationManager.AppSettings["FromMail"] : "";
        public static readonly string UserNameForSendMail = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserNameForSendMail"]) ? ConfigurationManager.AppSettings["UserNameForSendMail"] : "";
        public static readonly string PasswordForSendMail = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["PasswordForSendMail"]) ? ConfigurationManager.AppSettings["PasswordForSendMail"] : "";

        public static readonly string WebsiteLink = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["WebsiteLink"]) ? ConfigurationManager.AppSettings["WebsiteLink"] : "";

        public static readonly string UserNameForSyncService = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["UserNameForSyncService"]) ? ConfigurationManager.AppSettings["UserNameForSyncService"] : "";

        public static readonly string PasswordForSyncService = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["PasswordForSyncService"]) ? ConfigurationManager.AppSettings["PasswordForSyncService"] : "";
    }

   

}