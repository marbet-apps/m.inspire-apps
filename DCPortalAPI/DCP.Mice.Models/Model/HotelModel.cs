﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DCP.Mice.Models
{
    public class HotelDetailModel
    {
        [JsonProperty("id")]
        public int HotelId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("addressStreet")]
        public string AddressStreet { get; set; }

        [JsonProperty("addressAdditional")]
        public string AddressAdditional { get; set; }

        [JsonProperty("addressZipcode")]
        public string AddressZipCode { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("location")]
        public GeoLocation Location { get; set; }

        [JsonProperty("category")]
        public int Category { get; set; }

        [JsonProperty("countRooms")]
        public int CountRooms { get; set; }

        [JsonProperty("countConferencerooms")]
        public int CountConferenceRooms { get; set; }

        [JsonProperty("isPremium")]
        public bool IsPremium { get; set; }

        [JsonProperty("isLocation")]
        public bool IsLocation { get; set; }

        [JsonProperty("conferencerooms")]
        public List<ConferenceRoom> ConferenceRooms { get; set; }

        [JsonProperty("images")]
        public List<ImageData> Images { get; set; }

        [JsonProperty("images_thumb")]
        public List<ImageData> ImagesThumb { get; set; }

        [JsonProperty("images_orig")]
        public List<ImageData> ImagesOriginal { get; set; }

        [JsonProperty("country")]
        public Country Country { get; set; }

        [JsonProperty("texts")]
        public HotelText Texts { get; set; }
    }

    public class ConferenceRoom
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("squaremeter")]
        public double SquareMeter { get; set; }

        [JsonProperty("quantities")]
        public ConferenceRoomQuantities Quantities { get; set; }
    }

    public class ConferenceRoomQuantities
    {
        [JsonProperty("block")]
        public int BlockType { get; set; }

        [JsonProperty("u")]
        public int UShapeType { get; set; }

        [JsonProperty("parliamentary")]
        public string ParliamentaryType { get; set; }

        [JsonProperty("banquet")]
        public string BanquetHallType { get; set; }

        [JsonProperty("theater")]
        public string TheaterType { get; set; }

        [JsonProperty("cabaret")]
        public string CabaretType { get; set; }
    }

    public class ImageData
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }
    }

    

    public class HotelText
    {
        [JsonProperty("de_DE")]
        public HotelDiscription DescriptionInGerman { get; set; }

        [JsonProperty("en_US")]
        public HotelDiscription DescriptionInEnglish { get; set; }
    }

    public class HotelDiscription
    {
        [JsonProperty("desc")]
        public string Desc { get; set; }

        [JsonProperty("desc_conf")]
        public string DescConf { get; set; }
    }

    
}
