﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class List_RequestItem
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(50), Column(TypeName = "nvarchar")]
        public string request_item_type { get; set; }

        [Required, MaxLength(50), Column(TypeName = "nvarchar")]
        public string request_item_type_de { get; set; }

        [Required, MaxLength(50), Column(TypeName = "varchar")]
        public string mice_request_item_type { get; set; }

    }
}
