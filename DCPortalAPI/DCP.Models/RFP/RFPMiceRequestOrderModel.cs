﻿using DCP.Mice.Models;
using Newtonsoft.Json;
using System;

namespace DCP.Models.RFP
{
    public class RFPMiceRequestOrderModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("rfp_micerequestid")]
        public int RFPMiceRequestId { get; set; }
        [JsonProperty("mice_order_id")]
        public int MiceOrderId { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("orderdetail")]
        public OrderDetailModel OrderDetail { get; set; }
        [JsonProperty("created_by")]
        public int CreatedBy { get; set; }
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("modified_by")]
        public int? ModifiedBy { get; set; }
        [JsonProperty("modified_date")]
        public DateTime? ModifiedDate { get; set; }
    }
}
