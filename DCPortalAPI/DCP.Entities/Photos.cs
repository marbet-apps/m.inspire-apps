﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class Photos
    {
        [Key]
        public int Id { get; set; }
        public int photo_type { get; set; }
        public int transaction_id { get; set; }
        [MaxLength(4000)]
        [Required]
        public string photo_name { get; set; }
        public long photo_size { get; set; }
    }
}
