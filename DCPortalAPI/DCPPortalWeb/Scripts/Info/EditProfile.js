﻿var $prCtrl = {
    controls: {
        ddlGender: "#ddlGender",
        ddlLand: "#ddlLand",
        txtEmail: "#txtEmail",
        txtFirstName: "#txtFirstName",
        txtLastName: "#txtLastName",
        txtStreet: "#txtStreet",
        txtPlace: "#txtPlace",
        txtPinCode: "#txtPinCode",
        txtTelephone: "#txtTelephone",
        txtTaxIdentificationNo: "#txtTaxIdentificationNo",
        btnupdate: "#btnupdate",
        s2idddlLand: '#s2id_ddlLand .select2-choice',
        lnkFileUpload: "#lnkFileUpload",
        btnFileUpload: "#btnFileUpload",
        hdnImgValidation: "#hdnImgValidation",
        imgPhoto: "#imgPhoto",
        hdnImageSizeValidation: "#hdnImageSizeValidation",
        lblUserName:"#lblUserName",

        txtoldpass: "#txtoldpass",
        txtnewpass: "#txtnewpass",
        txtconfirmpass: "#txtconfirmpass",
        hdnAPIBaseURL: "#hdnAPIBaseURL",
        usercontainer: '#user-container',
        btnUpdateChangePwd: "#btnUpdateChangePwd",
        hdnOldPassMsg: "#hdnOldPassMsg",
        hdnConfirmPassMsg: "#hdnConfirmPassMsg",
        btnCloseProfile: ".btnCloseProfile",
        hdnUploadPhotoSuccessMsg: "#hdnUploadPhotoSuccessMsg",
        btnDeletePhoto: "#btnDeletePhoto",
        hdnDeletePhotoConfirmationMsg: "#hdnDeletePhotoConfirmationMsg",
        hdnPasswordValidationMsg: "#hdnPasswordValidationMsg",
    },

    localization: {
        SelectGender: "#hdnSelectGender",
        LandDesc:"#hdnLandDesc"
    },
    apiPath: {
        GetCountryList: "lookup/CountryList",
        GetUserDetailUrl: "AdminSecure/UserDetail",
        EditProfileUrl: "AdminSecure/UpdateUserProfile",
        UploadImageUrl: "adminsecure/uploadimage",
        changePasswordURL: "AdminSecure/ChangePassword",
        DeleteImageURL: "AdminSecure/DeleteImage",
        UpdateUserProfilePhoto:"AdminSecure/UpdateUserProfilePhoto"
    },
    GetUserDetail: function () {
        
        var login = {
            userID: JSON.parse(localStorage.CurrentUser).LoggedInUserID
        };

        $.ajax({
            type: "GET",
            url: $($c.controls.hdnAPIBaseURL).val() + this.apiPath.GetUserDetailUrl,
            data: { userID: JSON.parse(localStorage.CurrentUser).LoggedInUserID},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {
                
                if (data != undefined && data != null && data.data != undefined && data.data != null && data.data.users != null && data.data.users.length>0) {
                    var user = data.data.users[0];
                    $($prCtrl.controls.txtEmail).val(user.Email);
                    $($prCtrl.controls.ddlGender).val(user.Salutation).trigger("change");
                  
                    //$($prCtrl.controls.ddlGender).select2("data", {
                    //    "id": user.Salutation,
                    //    "text": user.Salutation
                    //});
                    $($prCtrl.controls.txtFirstName).val(user.First_Name);
                    $($prCtrl.controls.txtLastName).val(user.Last_Name);
                    $($prCtrl.controls.txtStreet).val(user.Street);
                    $($prCtrl.controls.txtPlace).val(user.Place);
                    $($prCtrl.controls.txtTelephone).val(user.Telephone);
                    $($prCtrl.controls.txtTaxIdentificationNo).val(user.Tax_Indentifier_No);
                    $($prCtrl.controls.lblUserName).text(user.First_Name+' '+user.Last_Name);
                    
                    $($prCtrl.controls.txtPinCode).val(user.PostCode);
                    if (user.strPhoto != null && user.strPhoto != "") {
                        $($prCtrl.controls.imgPhoto).attr("src", "data:image/jpg;base64," + user.strPhoto);
                        $($prCtrl.controls.btnDeletePhoto).show();
                    }
                    else {
                        $($prCtrl.controls.btnDeletePhoto).hide();
                    }
                    
                    $($prCtrl.controls.ddlLand).select2("data", {
                        "id": user.Country_Id,
                        "text": user.CountryName
                        });

                } else {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                }

               

            },
            failure: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    },

    ValidateEditProfile: function () {
        var flag = true;
        var firstName = $($prCtrl.controls.txtFirstName).val();
        var lastName = $($prCtrl.controls.txtLastName).val();
        var land = $($prCtrl.controls.ddlLand).val();
        var redLand = $($prCtrl.controls.s2idddlLand);

        if (firstName == null || firstName == undefined || firstName == "") {
            $($prCtrl.controls.txtFirstName).addClass("Error");
            flag = false;
        } else {
            $($prCtrl.controls.txtFirstName).removeClass("Error");
        }

        if (lastName == null || lastName == undefined || lastName == "") {
            $($prCtrl.controls.txtLastName).addClass("Error");
            flag = false;
        } else {
            $($prCtrl.controls.txtLastName).removeClass("Error");
        }
        
        if (land == null || land == undefined || land == "") {

            $($prCtrl.controls.s2idddlLand).css('cssText', 'border-color:red !important');
            flag = false;

        } else {
           
            $($prCtrl.controls.s2idddlLand).removeAttr("style");
        }

        return flag;
    },

    UpdateEditProfile: function () {
        
        var model = {
            LogedInUserId: JSON.parse(localStorage.CurrentUser).LoggedInUserID,
            Salutation: $($prCtrl.controls.ddlGender).val(),
            First_Name: $($prCtrl.controls.txtFirstName).val(),
            Last_Name: $($prCtrl.controls.txtLastName).val(),
            Street: $($prCtrl.controls.txtStreet).val(),
            PostCode: $($prCtrl.controls.txtPinCode).val(),
            Place: $($prCtrl.controls.txtPlace).val(),
            Telephone: $($prCtrl.controls.txtTelephone).val(),
            Tax_Indentifier_No: $($prCtrl.controls.txtTaxIdentificationNo).val(),
            Country_Id: $($prCtrl.controls.ddlLand).val(),
            Email: $($prCtrl.controls.txtEmail).val(),
        };

        $.ajax({
            type: "PUT",
            url: $($c.controls.hdnAPIBaseURL).val() + this.apiPath.EditProfileUrl,
            data: JSON.stringify(model),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {
                
                if (data != undefined && data != null && data.code == "718") {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", true);
                   // $($c.controls.usercontainer).modal("hide");

                } else {
                    $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                }

            },
            failure: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    },
    
    DeleteImage: function () {

        var strValue = $($prCtrl.controls.imgPhoto).attr("src");
        if (strValue.indexOf("no-images.png") > 0) {
            return false;
        }

       

        
        
        var model = {
            LogedInUserId: JSON.parse(localStorage.CurrentUser).LoggedInUserID
            
        };

        BootstrapDialog.confirm($($prCtrl.controls.hdnDeletePhotoConfirmationMsg).val(), function (result) {

            if (result) {
                common.displayLoader();
                $.ajax({
                    type: "POST",
                    url: $($c.controls.hdnAPIBaseURL).val() + "AdminSecure/DeleteImage",
                    data: JSON.stringify(model),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, textStatus, response) {

                        if (data != undefined && data != null && data.code == "721") {

                            $($prCtrl.controls.imgPhoto).attr("src", "/Images/no-images.png");
                            $managecode.Decision(false, "", data.code, data.message, "EditProfile", true);


                        } else {
                            $managecode.Decision(false, "", data.code, data.message, "EditProfile", false);
                        }
                        $($prCtrl.controls.btnDeletePhoto).hide();
                        $prCtrl.SaveUserProfilePhoto(null);
                        $($c.controls.imgCurrentUser).attr("src", "/Images/avatar.png");
                        

                    },
                    failure: function (data) {
                        $managecode.Decision(true, data, "", "", "error", false);
                    },
                    error: function (data) {
                        $managecode.Decision(true, data, "", "", "error", false);
                    }
                });
            }
        });

       
       
    },

    SaveUserProfilePhoto: function (key) {
        ajaxRequest.setCurrentUserPhoto(key);
    },
    ShowUserProfilePhoto: function () {
        ajaxRequest.SetUserProfilePhotoIntoImage();
    }
};

var $changepassword = {
    validateChangePwd: function () {

        var flag = true;
        var oldpass = $($prCtrl.controls.txtoldpass).val();
        var newpass = $($prCtrl.controls.txtnewpass).val();
        var confirmpass = $($prCtrl.controls.txtconfirmpass).val();

        if (oldpass == null || oldpass == undefined || oldpass == "") {
            $($prCtrl.controls.txtoldpass).addClass("Error");
            flag = false;
        } else {
            $($prCtrl.controls.txtoldpass).removeClass("Error");
        }

        if (newpass == undefined || newpass == null || newpass == "") {
            $($prCtrl.controls.txtnewpass).addClass("Error");
            flag = false;
        } else {
            $($prCtrl.controls.txtnewpass).removeClass("Error");
        }

        if (confirmpass == undefined || confirmpass == null || confirmpass == "") {
            $($prCtrl.controls.txtconfirmpass).addClass("Error");
            flag = false;
        } else {
            $($prCtrl.controls.txtconfirmpass).removeClass("Error");
        }

        if (newpass != null && newpass != "" && $changepassword.checkPasswordStrength() == false) {
            flag = false;
        }

        if (confirmpass != undefined || confirmpass != null || confirmpass != "" || newpass == undefined || newpass == null || newpass == "") {
            if ($($prCtrl.controls.txtnewpass).val() != $($prCtrl.controls.txtconfirmpass).val()) {
                $($prCtrl.controlstxtnewpass).addClass("Error");
                $($prCtrl.controls.txtconfirmpass).addClass("Error");
                $managecode.Decision(false, "", "", $($prCtrl.controls.hdnConfirmPassMsg).val(), "Login", false);
                flag = false;
            }
        }
        return flag;
    },
    submitchangepassword: function () {

        common.displayLoader();

        var login = {
            Email: JSON.parse(localStorage.CurrentUser).Email,
            OldPassword: $($prCtrl.controls.txtoldpass).val(),
            NewPassword: $($prCtrl.controls.txtnewpass).val(),
            First_Name: JSON.parse(localStorage.CurrentUser).First_Name,
            Last_Name: JSON.parse(localStorage.CurrentUser).Last_Name
        };

        $.ajax({
            type: "POST",
            url: $($c.controls.hdnAPIBaseURL).val() + $prCtrl.apiPath.changePasswordURL,
            data: JSON.stringify(login),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {

                if (data != undefined && data != null && data.code == "712") {

                    //var token = response.getResponseHeader('authorization');
                    //var datas = data.data;

                    //ajaxRequest.setAuthorizationToken(JSON.stringify(token));
                    //ajaxRequest.setCurrentUser(JSON.stringify(datas));

                    window.localStorage.removeItem(ajaxRequest.variables.authorizationTokenKey);
                    window.localStorage.removeItem(ajaxRequest.variables.CurrentUser);

                    $managecode.Decision(false, "", data.code, data.message, "login", true);
                    //$managecode.Decision(false, "", "", $($prCtrl.controls.hdnOldPassMsg).val(), "Login", false);
                    setTimeout(function () { location.href = "/Login"; }, 5000);
                    //location.href = "/Login";

                } else {

                    if (data != undefined && data != null && data.code == "610") {

                        $managecode.Decision(false, "", "", $($prCtrl.controls.hdnOldPassMsg).val(), "Login", false);
                    }
                    else {

                        $managecode.Decision(false, "", data.code, data.message, "Login", false);
                    }

                }

            },
            failure: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    },
    checkPasswordStrength: function () {

        var flag = false;
        var newpass = $($prCtrl.controls.txtnewpass).val();

        var number = /([0-9])/;
        var alphabets = /([a-z])/;
        var upperAlphabets = /([A-Z])/;
        var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;

        if (newpass.length < 6) {
            $($prCtrl.controls.txtnewpass).addClass("Error");
            $managecode.Decision(false, "", "", $($prCtrl.controls.hdnPasswordValidationMsg).val(), "login", false);
            flag = false;
        }
        else {
            if ((newpass.match(number) != null && newpass.match(number).length > 0) && (newpass.match(alphabets) != null && newpass.match(alphabets).length > 0)
                && (newpass.match(special_characters) != null && newpass.match(special_characters).length > 0) && (newpass.match(upperAlphabets) != null && newpass.match(upperAlphabets).length > 0)) {
                $($prCtrl.controls.txtnewpass).removeClass("Error");
                flag = true;
            }
            else {

                $($prCtrl.controls.txtnewpass).addClass("Error");
                $managecode.Decision(false, "", "", $($prCtrl.controls.hdnPasswordValidationMsg).val(), "login", false);
                flag = false;


            }
        }

        return flag;
    }
};

$(document).ready(function () {   

    $('body').css('backgroundImage', 'url(../../Images/IMG_banner_Profile.png)');

    $($prCtrl.controls.lnkFileUpload).on('change', function () {
        var file = this.files[0];
        if (file == undefined) {
            return false;
        }

        
        var fileType = file["type"];
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if ($.inArray(fileType, validImageTypes) < 0) {
            $managecode.Decision(true, "", "", $($prCtrl.controls.hdnImgValidation).val(), "error", false);
            return false;
        }
        
        var data = new FormData();
        
        var files = $($prCtrl.controls.lnkFileUpload).get(0).files;

        
        var maxFileSize = 512000; //allow 500kb size //10485760; //allow 10mb 10*1024*1024
        if (parseInt(files[0].size) > parseInt(maxFileSize)) 
        {
            $managecode.Decision(true, "", "", $($prCtrl.controls.hdnImageSizeValidation).val(), "error", false);
                return false;
        }


        
        // Add the uploaded image content to the form data collection
        if (files.length > 0) {
            data.append("UploadedImage", files[0]);
            data.append("UserId", JSON.parse(localStorage.CurrentUser).LoggedInUserID);
        }

        var ajaxRequest = $.ajax({
            type: "POST",
            url: $($c.controls.hdnAPIBaseURL).val() + $prCtrl.apiPath.UploadImageUrl,
            contentType: false,
            processData: false,
            data: data,
            success: function (data) {
                imgs = data;
               
                $($prCtrl.controls.imgPhoto).attr("src", "data:image/jpg;base64,"
                    + imgs.base64imgage);
                
                if (imgs != null && imgs.base64imgage!=null && imgs.base64imgage.length > 0) {
                    $managecode.Decision(false, "", "", $($prCtrl.controls.hdnUploadPhotoSuccessMsg).val(), "Login", true);
                }
                
                $($prCtrl.controls.btnDeletePhoto).show();
               // $prCtrl.SetUserProfilePhotoIntoImage(imgs.base64imgage);
                //$prCtrl.ShowUserProfilePhoto();
                
                // ajaxRequest.SetUserProfilePhoto();
                window.localStorage.setItem("CurrentUserPhoto", imgs.base64imgage);
              
                if (window.localStorage.getItem("CurrentUserPhoto") != "null" && window.localStorage.getItem("CurrentUserPhoto").length > 0) {
                    $($c.controls.imgCurrentUser).attr("src", 'data:image/png;base64,' + window.localStorage.getItem("CurrentUserPhoto"));
                   
                }
                
               
            }
        });

        ajaxRequest.done(function (xhr, textStatus) {
            // Do other operation
        });

    });

    $LookUp.BindDropdown($prCtrl.controls.ddlLand, $($prCtrl.localization.LandDesc).val(), $prCtrl.apiPath.GetCountryList, 0, 50, false, true);
    $prCtrl.GetUserDetail();
    $($prCtrl.controls.ddlGender).select2();

    $($prCtrl.controls.btnUpdateChangePwd).click(function () {
        if ($changepassword.validateChangePwd()) {
            $changepassword.submitchangepassword();
        }
        else {
            return false;
        }

    });

    $($prCtrl.controls.btnupdate).click(function () {
        if ($prCtrl.ValidateEditProfile()) {
            $prCtrl.UpdateEditProfile();
        }
    });

    $($prCtrl.controls.btnDeletePhoto).click(function () {
        $prCtrl.DeleteImage();
      
    });

    $($prCtrl.controls.btnCloseProfile).click(function () {
       location.href = "/home";
    });
});


