﻿using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DCP.Business.Interface;
using DCPortalAPI.Settings;
using DCP.Models;
using System.IO;
using DCPortalAPI.Filter;

namespace DCPortalAPI.Controllers
{
    [RoutePrefix("api/user")]
    [Authorize]
    [ExceptionHandler]
    public class UserController : ApiController
    {
        private IUser _IUser;
        private IBSecure _IBSecure;
        public UserController(IUser IUser,IBSecure IBSecure)
        {
            this._IUser = IUser;
            this._IBSecure = IBSecure;
        }


        [HttpGet]
        [Route("GetUserList")]
        public IHttpActionResult GetUserList(string search, string sortBy)
        {
            WriteLog.LogInformation(LogMessages.RetrieveResourcesStart);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var response = _IUser.GetUserList(search, sortBy, userId);
                return Ok(response);
            }
            finally
            {

                WriteLog.LogInformation(LogMessages.RetrieveResourcesStart);
            }

        }

        [HttpPost]
        [Route("DeleteUser")]
        public IHttpActionResult DeleteUser(int userId)
        {
            WriteLog.LogInformation(LogMessages.DeleteUser);
            try
            {                
                var response = _IUser.DeleteUser(userId);
                return Ok(response);
            }
            finally
            {

                WriteLog.LogInformation(LogMessages.DeleteUserEnded);
            }

        }

        [HttpGet]
        [Route("GetUserDetail")]
        [Authorize]
        public IHttpActionResult GetUserDetail(int userId)
        {
            WriteLog.LogInformation(LogMessages.RetriveAllUserStarted);
            try
            {
                var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                var result = _IUser.GetUserDetail(userId, lang);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RetriveAllUserEnded);
            }
        }

        [HttpPut]
        [Route("UpdateUserDetail")]
        [Authorize]
        public IHttpActionResult UpdateUserDetail(DCP.Models.UpdateUserModel model)
        {
            WriteLog.LogInformation(LogMessages.UpdateUserStarted);
            try
            {
                var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                var result = _IUser.UpdateUser(model);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.UpdateUserEnd);
            }
        }

        [HttpPost]
        [Route("RegisterNewUser")]
        [Authorize]
        public IHttpActionResult RegisterNewUser(DCP.Models.UserRegisterModel model)
        {
            WriteLog.LogInformation(LogMessages.RegisterNewUserStarted);
            try
            {

               // model.Password = Crypto.GeneratePassword(5);
               

                var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                bool flag = false;
                var result = _IBSecure.RegisterUser(model,out flag);

                if(result.Code=="701")
                {
                    var resultModel = (RegisterationModel)result.Data;
                    string idValue = Crypto.encrypt(resultModel.Id.ToString());


                    string requestDateTime = Crypto.EnryptString(System.DateTime.Now.ToString("ddMMyyyyHHmm"));

                    bool isSuccess = MailUtility.SendRegistrationMail(lang, model.First_Name, model.Last_Name, model.Email, model.Password, idValue, requestDateTime);

                    if (isSuccess == false)
                    {
                        result.Code = ResponceCodes.Success.SendMailFail;
                        result.Message = ResponceMessages.Success.SendMailFail;
                    }
                }
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RegisterNewUserEnded);
            }
        }


        [HttpGet]
        [Route("CheckAllowNewUser")]
        [Authorize]
        public IHttpActionResult CheckAllowNewUser(int insertedRow)
        {
            WriteLog.LogInformation(LogMessages.CheckAllowNewUserStarted);
            try
            {              
                var result = _IUser.CheckAllowNewUser(Settings.Constants.TotalUser, insertedRow);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.CheckAllowNewUserEnded);
            }
        }

       
    }
}
