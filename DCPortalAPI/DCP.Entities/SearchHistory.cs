﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class SearchHistory
    {
        [Key]
        public int Id { get; set; }
        public string search_criteria { get; set; }
        public int created_by { get; set; }
        public DateTime created_date { get; set; }
    }
}
