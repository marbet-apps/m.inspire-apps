﻿using DCP.Business.Interface;
using DCP.Entities;
using DCP.Models;
using DCP.Repository.Interface;
using DCP.Utilities;
using System;
using System.Net;

namespace DCP.Business
{
    public class Search : IBSearch
    {
        private ISearchRepository _ISearchRepository;

        public Search(ISearchRepository _ISearchRepository)
        {
            this._ISearchRepository = _ISearchRepository;
        }

        /// <summary>
        /// Get All Resource List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetResourcesList(FilterModel SearchModel, string lang, int? UserId)
        {
            try
            {
                var ResourcesList = _ISearchRepository.GetResourcesList(SearchModel, lang, UserId);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, ResourcesList, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        ///// <summary>
        ///// Get Hotel By Id
        ///// </summary>
        ///// <returns></returns>
        //public ApiResult GetHotelById(int id, string Lang)
        //{
        //    try
        //    {
        //        var Hotel = _ISearchRepository.GetHotelById(id, Lang);
        //        return ResultSet.SuccessResponse(ResponceCodes.Success.GetResource, Hotel, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
        //    }
        //    catch (WebException ex)
        //    {
        //        return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
        //    }
        //}

        ///// <summary>
        ///// Get Restaurant By Id
        ///// </summary>
        ///// <returns></returns>
        //public ApiResult GetRestaurantById(int id, string Lang)
        //{
        //    try
        //    {
        //        var Restaurant = _ISearchRepository.GetRestaurantById(id, Lang);

        //        return ResultSet.SuccessResponse(ResponceCodes.Success.GetResource, Restaurant, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

        //    }
        //    catch (WebException ex)
        //    {
        //        return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
        //    }
        //}

        ///// <summary>
        ///// Get Location By Id
        ///// </summary>
        ///// <returns></returns>
        //public ApiResult GetLocationById(int id, string Lang)
        //{
        //    try
        //    {
        //        var Location = _ISearchRepository.GetLocationById(id, Lang);

        //        return ResultSet.SuccessResponse(ResponceCodes.Success.GetResource, Location, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

        //    }
        //    catch (WebException ex)
        //    {
        //        return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
        //    }
        //}

        /// <summary>
        /// Get Images By Id
        /// </summary>
        /// <returns></returns>
        public ApiResult GetImageData(int id, string type)
        {
            try
            {
                var Location = _ISearchRepository.GetImageData(id, type);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, Location, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Near By Restaurant 
        /// </summary>
        /// <returns></returns>
        public ApiResult GetNearByRestaurantList(string lat, string lng, int? cid, string type, int distance)
        {
            try
            {
                var data = _ISearchRepository.GetNearByRestaurantList(lat, lng, cid, type, distance);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, data, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Near By Location 
        /// </summary>
        /// <returns></returns>
        public ApiResult GetNearByLocationList(string lat, string lng, int? cid, string type, int distance)
        {
            try
            {
                var data = _ISearchRepository.GetNearByLocationList(lat, lng, cid, type, distance);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, data, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Near By Hotel 
        /// </summary>
        /// <returns></returns>
        public ApiResult GetNearByHotelList(string lat, string lng, int? cid, string type, int distance)
        {
            try
            {
                var data = _ISearchRepository.GetNearByHotelList(lat, lng, cid, type, distance);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, data, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Hotel Info 
        /// </summary>
        /// <returns></returns>
        public ApiResult GetHotelInfo(int id, string Lang, int UserId)
        {
            try
            {
                var data = _ISearchRepository.GetHotelInfo(id, Lang,UserId);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetResource, data, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Restaurant Info 
        /// </summary>
        /// <returns></returns>
        public ApiResult GetRestaurantInfo(int id, string Lang, int UserId)
        {
            try
            {
                var data = _ISearchRepository.GetRestaurantInfo(id, Lang, UserId);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetResource, data, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Location Info 
        /// </summary>
        /// <returns></returns>
        public ApiResult GetLocationInfo(int id, string Lang, int UserId)
        {
            try
            {
                var data = _ISearchRepository.GetLocationInfo(id, Lang, UserId);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetResource, data, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get RFP Resource List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetRFPResourceList(string Rids, string type, string lang)
        {
            try
            {
                var ResourcesList = _ISearchRepository.GetRFPResourceList(Rids, type,lang);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, ResourcesList, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Save search history
        /// </summary>
        /// <returns></returns>
        public ApiResult SaveSearchHistory(string searchCriteria, int userId)
        {
            try
            {
                _ISearchRepository.SaveSearchHistory(searchCriteria, userId);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, null, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Cart Resource List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetCartList(int UserId, string lang)
        {
            try
            {
                var ResourcesList = _ISearchRepository.GetCartList(UserId,lang);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, ResourcesList, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Resource List from History
        /// </summary>
        /// <returns></returns>
        public ApiResult GetSearchCriteria(int UserId)
        {
            try
            {
                var ResourcesList = _ISearchRepository.GetSearchCriteria(UserId);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, ResourcesList, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Cart Resource Count
        /// </summary>
        /// <returns></returns>
        public ApiResult GetCartCount(int UserId, string lang)
        {
            try
            {
                var ResourcesList = _ISearchRepository.GetCartCount(UserId, lang);

                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, ResourcesList, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
    }
}
