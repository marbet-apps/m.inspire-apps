﻿using System;
using System.Collections.Generic;

namespace DCP.Models.RFP
{
    public class RequestCartModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public List<RequestCartResourceModel> CartResources { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}
