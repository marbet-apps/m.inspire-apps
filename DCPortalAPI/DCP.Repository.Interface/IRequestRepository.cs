﻿using DCP.Models;
using DCP.Models.RFP;
using System.Collections.Generic;
using DCP.Mice.Models;
namespace DCP.Repository.Interface
{
    public interface IRequestRepository
    {
        #region Cart

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cartModel"></param>
        /// <returns></returns>
        int MarkResourceForTheRequest(RequestCartModel cartModel);

        ///INSPIRE-102
        ///Multiple RFP store into table so, RFPId wise delete record from table
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="resourceId"></param>
        /// <param name="resourceType"></param>
        /// <returns></returns>
        bool RemoveResourceFromTheSelection(int userId, int resourceId, string resourceType, int? rfpId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        RequestCartModel GetRequestCart(int userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int MakeRequestForProposal(int userId);

        ///INSPIRE-102
        ///This method is new.
        ///This method is stored rfp records into request for proposal related tables and remove records from carts
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpModel"></param>
        /// <returns></returns>
        RequestForProposalModel MakeRequestForProposalFromStartRfp(RequestForProposalModel rfpModel);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int GetRequestCartCount(int userId);

        #endregion Cart

        #region Lookup Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        List<ConferenceFoodGroupModel> GetConferenceFoods(string lang);

        #endregion Lookup Methods

        #region RFP Request Items

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpMiceRequestItemId"></param>
        /// <param name="langCode"></param>
        /// <returns></returns>
        RequestItemModel GetRFPRequestItems(int rfpMiceRequestItemId, string langCode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requestItemModel"></param>
        /// <returns></returns>
        int AddEditRFPRequestItem(RequestItemModel requestItemModel);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpMiceRequestItemId"></param>
        /// <param name="requestItemObjectId"></param>
        /// <returns></returns>
        bool DeleteRFPRequestItem(int rfpMiceRequestItemId);



        #endregion RFP Request Items

        #region RFP

        RequestForProposalModel GetRequestForProposal(int userId, string langCode);

        ///INSPIRE-102
        ///This method is return request proposal modal after update record of request proposal which is used for multiple RFP stored purpose
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rfpModel"></param>
        /// <param name="isSubmit"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        RequestForProposalModel SubmitRFP(RequestForProposalModel rfpModel, bool isSubmit, int userId);

        Dictionary<string, object> ValidateSubmitRFP(RequestForProposalModel rfpModel);

        ///INSPIRE-102
        ///This method is return resource item list for selected rfp
        List<RequestItemObject> GetResourceItemDetail(int userId, string langCode, string sortBy, string type, string requestItemType, int rfpId);

        RequestForProposalModel GetRequestForProposalById(int id, string langCode);

        #endregion RFP

        OrderResponseModel BookedOrder(OrderBookingModel bookingModel,int userId);
        OrderResponseModel AbortOrder(OrderAbortModel bookingModel);

        ///INSPIRE-102
        ///This method is new.
        ///This method return cart records from tables when user click on startrfp table
        RequestForProposalModel GetRequestForStartRFP(int userId, string langCode);

        bool AddResourceToRFP(int userId, int resourceId, string RType, int rfpId);
    }
}
