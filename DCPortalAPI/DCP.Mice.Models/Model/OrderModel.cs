﻿using Newtonsoft.Json;
using System;

namespace DCP.Mice.Models
{

    public class OrderBookingModel
    {
        [JsonProperty("requestId")]
        public int RequestId { get; set; }
        [JsonProperty("micerequestid")]
        public int Micerequestid { get; set; }
        [JsonProperty("offerId")]
        public int OfferId { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }
        [JsonProperty("order_detail_model")]
        public OrderDetailModel OrderDetailModel { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }
    }

    public class OrderAbortModel
    {
        [JsonProperty("requestId")]
        public int RequestId { get; set; }

        [JsonProperty("abortReason")]
        public string AbortReason { get; set; }

        [JsonProperty("userId")]
        public int UserId { get; set; }

        //[JsonProperty("comment")]
        //public string Comment { get; set; }
    }

    public class OrderExtendModel
    {
        [JsonProperty("requestId")]
        public int RequestId { get; set; }

        [JsonProperty("offerId")]
        public int OfferId { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }
    }

    public class OrderDetailModel
    {
        [JsonProperty("id")]
        public int OrderId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("bookedTime")]
        public DateTime? BookedTime { get; set; }

        [JsonProperty("abortedTime")]
        public DateTime? AbortedTime { get; set; }

        [JsonProperty("hotelid")]
        public int HotelId { get; set; }

        [JsonProperty("customerid")]
        public int CustomerId { get; set; }

        [JsonProperty("customer_name")]
        public string CustomerName { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("volume")]
        public float Volume { get; set; }

        [JsonProperty("arrivalTime")]
        public DateTime? ArrivalTime { get; set; }

        [JsonProperty("departureTime")]
        public DateTime? DepartureTime { get; set; }
    }

    public class OrderResponseModel
    {
        [JsonProperty("result")]
        public string Status { get; set; }
        [JsonProperty("status_code")]
        public string Status_code { get; set; }
        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonProperty("order_status")]
        public string Order_Status { get; set; }

        [JsonProperty("rfp_mice_request_id")]
        public int Rfp_Mice_Request_Id { get; set; }
    }
}
