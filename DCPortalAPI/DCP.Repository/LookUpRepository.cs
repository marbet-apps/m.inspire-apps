﻿using DCP.Entities;
using DCP.Models;
using DCP.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DCP.Repository
{
    public class LookUpRepository : ILookUpRepository
    {
        private DCPDbContext _context = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public LookUpRepository()
        {
            this._context = new DCPDbContext();
        }

        /// <summary>
        /// Get Country List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUp GetCountryList(string Lang, string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUp();

            var qry = from c in _context.List_Country
                      orderby Lang == "en-US" ? (c.country_name) : c.country_name_de ascending
                      select new Lookup()
                      {
                          Id = c.Id,
                          Name = Lang == "en-us" ? c.country_name : c.country_name_de,
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Count();
            result.lookup = qry.OrderBy(x => x.Name).Skip(Skipcount(page,pageSize)).Take(pageSize).ToList();

            return result;
        }

        /// <summary>
        /// Get Radius List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUp GetRadiusList(string Lang, string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUp();

            var qry = from c in _context.List_Radius
                      orderby Lang == "en-US" ? (c.Name) : c.Name_de ascending
                      select new Lookup()
                      {
                          Id = c.KM,
                          Name = Lang == "en-us" ? c.Name : c.Name_de,
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Count();
            result.lookup = qry.OrderBy(x => x.Id).Skip(Skipcount(page, pageSize)).Take(pageSize).ToList();

            return result;
        }
        /// <summary>
        /// Get Chain List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUpString GetChainList(string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUpString();

            var qry = from h in _context.Hotels
                      where h.hotel_chain != null && h.hotel_chain != ""
                      select new LookupString()
                      {
                          Id = h.hotel_chain.ToLower(),
                          Name = h.hotel_chain.ToLower()
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Distinct().Count();
            result.lookup = qry.Distinct().OrderBy(x => x.Name).Skip(Skipcount(page,pageSize)).Take(pageSize).ToList();

            return result;
        }
        /// <summary>
        /// Get Hotel List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUpString GetHotelList(string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUpString();

            var qry = from h in _context.Hotels
                      where h.hotel_name != null && h.hotel_name != ""
                      select new LookupString()
                      {
                          Id = h.dcp_hotelinfo_id.ToString(),
                          Name = h.hotel_name.ToLower()
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Distinct().Count();
            result.lookup = qry.Distinct().OrderBy(x => x.Name).Skip(Skipcount(page, pageSize)).Take(pageSize).ToList();

            return result;
        }
        /// <summary>
        /// Get Restaurant List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUpString GetRestaurantList(string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUpString();

            var qry = from h in _context.Restaurants
                      where h.restaurant_name != null && h.restaurant_name != ""
                      select new LookupString()
                      {
                          Id = h.dcp_restaurantinfo_id.ToString(),
                          Name = h.restaurant_name.ToLower()
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Distinct().Count();
            result.lookup = qry.Distinct().OrderBy(x => x.Name).Skip(Skipcount(page, pageSize)).Take(pageSize).ToList();

            return result;
        }
        /// <summary>
        /// Get Location List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUpString GetLocationList(string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUpString();

            var qry = from h in _context.Locations
                      where h.location_name != null && h.location_name != ""
                      select new LookupString()
                      {
                          Id = h.dcp_locationinfo_id.ToString(),
                          Name = h.location_name.ToLower()
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Distinct().Count();
            result.lookup = qry.Distinct().OrderBy(x => x.Name).Skip(Skipcount(page, pageSize)).Take(pageSize).ToList();

            return result;
        }

        /// <summary>
        /// Get Hotel Type List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUp GetHotelTypeList(string Lang, string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUp();

            var qry = from h in _context.List_HotelType
                      select new Lookup()
                      {
                          Id = h.Id,
                          Name = Lang == "en-US"? h.hoteltype_name:h.hoteltype_name_de
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Count();
            result.lookup = qry.OrderBy(x => x.Name).Skip(Skipcount(page,pageSize)).Take(pageSize).ToList();

            return result;
        }

        /// <summary>
        /// Get Default Hotel Type List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUp GetDefaultHotelTypeList(string Lang, int[] Ids)
        {
            var result = new TemplateLookUp();

            var qry = from h in _context.List_HotelType
                      select new Lookup()
                      {
                          Id = h.Id,
                          Name = Lang == "en-US" ? h.hoteltype_name : h.hoteltype_name_de
                      };

            if (Ids.Count() > 0)
            {
                qry = qry.Where(x => Ids.Contains(x.Id));
            }

            result.lookup = qry.OrderBy(x => x.Name).ToList();

            return result;
        }

        /// <summary>
        /// Get Restautant Type List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUp GetRestaurantTypeList(string Lang, string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUp();

            var qry = from h in _context.List_RestaurantType
                      select new Lookup()
                      {
                          Id = h.Id,
                          Name = Lang == "en-US" ? h.restaurant_name : h.restaurant_name_de
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Count();
            result.lookup = qry.OrderBy(x => x.Name).Skip(Skipcount(page,pageSize)).Take(pageSize).ToList();

            return result;
        }

        /// <summary>
        /// Get Location Type List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUp GetLocationTypeList(string Lang, string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUp();

            var qry = from h in _context.List_LocationType
                      select new Lookup()
                      {
                          Id = h.Id,
                          Name = Lang == "en-US" ? h.location_name : h.location_name_de
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Count();
            result.lookup = qry.OrderBy(x => x.Name).Skip(Skipcount(page,pageSize)).Take(pageSize).ToList();

            return result;
        }

        /// <summary>
        /// Get Kitchen List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUp GetKitchenList(string Lang, string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUp();

            var qry = from h in _context.List_Kitchen
                      select new Lookup()
                      {
                          Id = h.Id,
                          Name = Lang == "en-US" ? h.kitchen_name : h.kitchen_name_de
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Count();
            result.lookup = qry.OrderBy(x => x.Name).Skip(Skipcount(page,pageSize)).Take(pageSize).ToList();

            return result;
        }

        /// <summary>
        /// Get Suitable For List
        /// </summary>
        /// <returns></returns>
        public TemplateLookUp GetSuitableForList(string Lang, string term, int page, int pageSize, int type)
        {
            var result = new TemplateLookUp();

            var qry = from h in _context.List_SuitableFor
                      where h.suitablefor_type == type
                      select new Lookup()
                      {
                          Id = h.Id,
                          Name = Lang == "en-US" ? h.suitablefor_name : h.suitablefor_name_de
                      };

            if (!string.IsNullOrEmpty(term) && !string.IsNullOrWhiteSpace(term))
            {
                qry = qry.Where(x => x.Name.Contains(term.Trim()));
            }

            result.totalRecord = qry.Count();
            result.lookup = qry.OrderBy(x => x.Name).Skip(Skipcount(page,pageSize)).Take(pageSize).ToList();

            return result;
        }
        /// <summary>
        /// Retrieve subscription list.
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        public TemplateLookUp GetSubscriptionTypeList(string Lang)
        {
            var result = new TemplateLookUp();

            var qry = from h in _context.List_SubscriptionType
                      select new Lookup()
                      {
                          Id = h.Id,
                          Name = Lang == "en-US" ? h.Type_Name : h.Type_Name_de
                      };
            result.totalRecord = qry.Count();
            result.lookup = qry.OrderBy(x => x.Name).ToList();
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        public TemplateLookUpString GetRequestStatusList(string Lang)
        {
            var result = new TemplateLookUpString();

            var qry = from h in _context.RequestStatuses
                      select new LookupString()
                      {
                          Id = h.mice_status_type,
                          Name = Lang == "en-US" ? h.request_status : h.request_status_de
                      };
            result.totalRecord = qry.Count();
            result.lookup = qry.OrderBy(x => x.Name).ToList();
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public TemplateLookUpString GetRequestTypes(string lang)
        {
            var result = new TemplateLookUpString();
            var lookupTypes = (from lt in _context.RequestTypes
                           select new LookupString
                           {
                               Id = lt.mice_request_type,
                               Name = lang == "de-DE" ? lt.request_type_de : lt.request_type
                           }).ToList();
            result.lookup = lookupTypes;
            result.totalRecord = lookupTypes.Count;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public TemplateLookUpString GetRequestItemTypes(string lang)
        {
            var result = new TemplateLookUpString();
            var lookupTypes = (from lt in _context.RequestItemTypes
                           select new LookupString
                           {
                               Id = lt.mice_request_item_type,
                               Name = lang == "de-DE" ? lt.request_item_type_de : lt.request_item_type
                           }).ToList();
            result.lookup = lookupTypes;
            result.totalRecord = lookupTypes.Count;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public TemplateLookUpString GetRoomTypes(string lang)
        {
            var result = new TemplateLookUpString();
            var lookupTypes = (from lt in _context.RoomTypes
                           select new LookupString
                           {
                               Id = lt.mice_room_type,
                               Name = lang == "de-DE" ? lt.room_type_de : lt.room_type
                           }).ToList();
            result.lookup = lookupTypes;
            result.totalRecord = lookupTypes.Count;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public TemplateLookUpString GetRoomTechniques(string lang)
        {
            var result = new TemplateLookUpString();

            var lookupTypes = (from lt in _context.RoomTechniques
                               orderby lt.sequence
                           select new LookupString
                           {
                               Id = lt.mice_room_technique,
                               Name = lang == "de-DE" ? lt.room_technique_name_de : lt.room_technique_name
                           }).ToList();
            result.lookup = lookupTypes;
            result.totalRecord = lookupTypes.Count;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public TemplateLookUpString GetSeatingTypes(string lang)
        {
            var result = new TemplateLookUpString();
            var lookupTypes = (from lt in _context.SeatingTypes
                           select new LookupString
                           {
                               Id = lt.mice_seating_type,
                               Name = lang == "de-DE" ? lt.seating_type_de : lt.seating_type
                           }).ToList();
            result.lookup = lookupTypes;
            result.totalRecord = lookupTypes.Count;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public TemplateLookUpString GetDaysRunning(string lang)
        {
            var result = new TemplateLookUpString();
            var lookupTypes = (from lt in _context.List_DaysRunnings
                               select new LookupString
                               {
                                   Id = lt.mice_days_running,
                                   Name = lang == "de-DE" ? lt.days_running_de : lt.days_running
                               }).ToList();
            result.lookup = lookupTypes;
            result.totalRecord = lookupTypes.Count;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        public TemplateLookUpString GetTimeIntervals(int interval)
        {
            var result = new TemplateLookUpString();
            var lookupTypes = new List<LookupString>();

            var startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            var endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);

            while (startDate <= endDate)
            {
                lookupTypes.Add(new LookupString { Id = startDate.ToString("HH:mm"), Name = startDate.ToString("hh:mm tt") });
                startDate = startDate.AddMinutes(interval);
            }

            result.lookup = lookupTypes;
            result.totalRecord = lookupTypes.Count;
            return result;
        }

        public TemplateLookUpString GetCityList()
        {
            TemplateLookUpString templateLookUpString = new TemplateLookUpString();

            var cityList = (from ht in _context.Hotels
                            select new LookupString
                            {
                                Id = ht.city,
                                Name = ht.city
                            }).Union(from rt in _context.Restaurants
                                     select new LookupString
                                     {
                                         Id = rt.city,
                                         Name = rt.city
                                     }).Union(from l in _context.Locations
                                              select new LookupString
                                              {
                                                  Id = l.city,
                                                  Name = l.city
                                              }).ToList();

            templateLookUpString.totalRecord = cityList.Count();
            templateLookUpString.lookup = cityList;
            return templateLookUpString;
        }
        public TemplateLookUpString GetAbortReason(string lang)
        {
            var result = new TemplateLookUpString();
            var lookupTypes = (from lt in _context.AbortReasons
                               select new LookupString
                               {
                                   Id = lt.mice_abort_reason,
                                   Name = lang == "de-DE" ? lt.abort_reason_de : lt.abort_reason
                               }).ToList();
            result.lookup = lookupTypes;
            result.totalRecord = lookupTypes.Count;
            return result;
        }

        /// <summary>
        /// Skip count
        /// </summary>
        /// <returns></returns>
        private int Skipcount(int page, int pageSize)
        {
            int skipCount = page == 1 ? 0 : (pageSize * (page - 1));

            return skipCount;
        }

    }
}
