﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using DCP.Utilities;
using Newtonsoft.Json;

namespace DCP.Models
{
    public class AdminSecureModel
    {

    }
    public class UserRegisterModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.SalutaionRequired)]
        public string Salutation { get; set; }

        [Required(AllowEmptyStrings = false,ErrorMessage =ValidationMessages.UserFirstNameRequired)]
        public string First_Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.UserLastNameRequired)]
        public string Last_Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.EmailRequired)]
        [EmailAddress(ErrorMessage = ValidationMessages.EmailInvalid)]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string Company_Name { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string Department_Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.ActiveStatusRequired)]
        //[RegularExpression("True|False", ErrorMessage = ValidationMessages.ActiveStatusInvalid)]
        //[Range(typeof(bool), "true", "true", ErrorMessage = ValidationMessages.ActiveStatusInvalid)]
        public bool IsActive { get; set; }
        
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.SelfCreatedStatusRequired)]
        //[Range(typeof(bool), "true", "true", ErrorMessage = ValidationMessages.SelfCreatedStatusInvalid)]
        //[MustBeTrue(ErrorMessage = ValidationMessages.SelfCreatedStatusInvalid)]
        public bool Isself_Created { get; set; }
        
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.SubscriptionTypeRequired)]
        public int Subscription_Type_Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.ApprovedStatusRequired)]
        //[Range(typeof(bool), "true", "true", ErrorMessage = ValidationMessages.ApproveStatusInvalid)]
        public bool IsApproved { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.DeleteStatusRequired)]
        //[Range(typeof(bool), "true", "true", ErrorMessage = ValidationMessages.DeleteStatusInvalid)]
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Communication table data
        /// </summary>
        public string Street { get; set; }

        public string PostCode { get; set; }

        public string Place { get; set; }
        
        public string Telephone { get; set; }

        public string Tax_Indentifier_No { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.ContryCodeRequired)]
        public int Country_Id { get; set; }

        public bool? Hotel { get; set; }

        public bool? Location { get; set; }

        public bool? Restaurant { get; set; }

        public bool? GoogleMap { get; set; }
        public bool? UserMgmt { get; set; }
        public bool? RFPTemplate { get; set; }

        public bool? NearBy { get; set; }

        public int loggedInUserId { get; set; }
        public int? Userid { get; set; }

        public string CountryName { get; set; }

        public bool? isAdmin { get; set; }

        public string Password { get; set; }

        public byte[] Photo { get; set; }
        public string strPhoto { get; set; }

        public bool IsRegisterFromManageUser { get; set; }

    }
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class MustBeTrueAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value != null && value is bool; //&& (bool)value;
        }
    }

    public class UserPasswordModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.EmailRequired)]
        [EmailAddress(ErrorMessage = ValidationMessages.EmailInvalid)]
        public string Email { get; set; }
        
        public string Password { get; set; }

        public int Id { get; set; }

        public string strId { get; set; }
    }

    public  class UpdateUserModel
    {
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Salutation { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string First_Name { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Last_Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.EmailRequired)]
        [EmailAddress(ErrorMessage = ValidationMessages.EmailInvalid)]
        public string Email { get; set; }
        //[JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        //public string Password { get; set; }
        // [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.LoggedInUserid)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int? LogedInUserId { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Company_Name { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Department_Name { get; set; }
        //[Required(ErrorMessage = ValidationMessages.ActiveStatusRequired)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? IsActive { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Subscription_Type_Name { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        //[Required(ErrorMessage = ValidationMessages.DeleteStatusRequired)]
        public bool? IsDeleted { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Street { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string PostCode { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Place { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Telephone { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Tax_Indentifier_No { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int? Country_Id { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? Hotel { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? Location { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? Restaurant { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? GoogleMap { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? UserMgmt { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? RFPTemplate { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? NearBy { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool? isAdmin { get; set; }
    }


    public class ResponseModel
    {
        public int LogedInUserId { get; set; }
        public bool isSuccess { get; set; }
        public UpdateUserModel  UserModel{ get; set; }
    }

    public class ResponseUserModel
    {
        public int totalRecord { get; set; }
        public List<UserRegisterModel> users { get; set; }
    }

    public class UpdateProfileModel
    {
        public int LogedInUserId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.SalutaionRequired)]
        public string Salutation { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.UserFirstNameRequired)]
        public string First_Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.UserLastNameRequired)]
        public string Last_Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.EmailRequired)]
        [EmailAddress(ErrorMessage = ValidationMessages.EmailInvalid)]
        public string Email { get; set; }

        public string Street { get; set; }

        public string PostCode { get; set; }

        public string Place { get; set; }

        public string Telephone { get; set; }

        public string Tax_Indentifier_No { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.ContryCodeRequired)]
        public int Country_Id { get; set; }

    }

    public class UpdateProfileResponseModel
    {
        public int LogedInUserId { get; set; }
        public bool IsSuccess { get; set; }
        public UpdateProfileModel UpdateProfileModel { get; set; }
    }

    public class UpdateProfilePhotoModel
    {
        public int LogedInUserId { get; set; }
    }
}
