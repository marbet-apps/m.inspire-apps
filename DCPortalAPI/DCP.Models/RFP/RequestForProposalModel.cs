﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace DCP.Models.RFP
{
    public class RequestForProposalModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("rfp_type")]
        public string RFPType { get; set; }
        [JsonProperty("event_name")]
        public string EventName { get; set; }
        [JsonProperty("userid")]
        public int UserId { get; set; }
        [JsonProperty("mice_userid")]
        public int? MiceUserId { get; set; }
        [JsonProperty("days_running")]
        public int? DaysRunning { get; set; }
        [JsonProperty("is_submitted")]
        public bool IsSubmitted { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("created_by")]
        public int CreatedBy { get; set; }
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("modified_by")]
        public int? ModifiedBy { get; set; }
        [JsonProperty("modified_date")]
        public DateTime? ModifiedDate { get; set; }

        [JsonProperty("micerequests")]
        public List<RFPMiceRequestModel> RFPMiceRequests { get; set; }

        [JsonProperty("dayrunningname")]
        public string DayRunningName { get; set; }

        [JsonProperty("isviewmode")]
        public bool IsViewMode { get; set; }

        [JsonProperty]
        public DateTime? EventStartDate { get; set; }

        [JsonProperty]
        public DateTime? EventEndDate { get; set; }

        [JsonProperty]
        public string strEventStartDate { get; set; }

        [JsonProperty]
        public string strEventEndDate { get; set; }

        [JsonProperty("strid")]
        public string strId { get; set; }

    }
}
