﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class List_ConferenceFood
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("ConferenceFoodGroup")]
        public int conference_food_group_id { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string conference_food_name { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string conference_food_name_de { get; set; }

        [Required, MaxLength(100), Column(TypeName = "varchar")]
        public string mice_conference_food_name { get; set; }

        public int? sequence { get; set; }

        public List_ConferenceFoodGroup ConferenceFoodGroup { get; set; }
    }
}
