﻿using DCPPortalWeb.Models;
using System.Web.Mvc;

namespace DCPPortalWeb.Controllers
{
    public class HomeController : BaseController
    {
        /// <summary>
        /// Landing Page 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? history)
        {
            HomeModel HomeModel = new HomeModel();
            if(history != null && history.HasValue && history.Value == 1)
            {
                TempData["history"] = 1;
            }
            return View(HomeModel);
        }

        /// <summary>
        /// load left filter
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult leftfilter()
        {
            LeftFilters _Leftmodel = new LeftFilters();
            return PartialView("_LeftFilter", _Leftmodel);
        }

        /// <summary>
        /// Load middle contents
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult middlecontent()
        {
            return PartialView("_MiddleContent");
        }


    }
}