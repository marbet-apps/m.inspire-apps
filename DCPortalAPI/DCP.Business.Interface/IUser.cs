﻿using DCP.Entities;
using DCP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Business.Interface
{
    public interface IUser
    {
        

        ApiResult GetUserList(string search, string sortBy, int loggedInUserId);

        ApiResult DeleteUser(int userId);

        ApiResult GetUserDetail(int userId, string lang = "");

        ApiResult UpdateUser(UpdateUserModel model);

        ApiResult CheckAllowNewUser(string totalUser, int insetedRow);
    }
}
