﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class List_SuitableFor
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(500)]
        [Required]
        public string suitablefor_name { get; set; }
        [MaxLength(500)]
        [Required]
        public string suitablefor_name_de { get; set; }
        public int suitablefor_type { get; set; }

        public ICollection<SuitableForType> SuitableForType { get; set; }
    }
}
