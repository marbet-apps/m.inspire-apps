﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class RFPResource
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("RFPMiceRequest")]
        public int rfp_mice_request_id { get; set; }

        public int resource_id { get; set; }

        public int mice_resource_id { get; set; }

        [Required, MaxLength(50), Column(TypeName = "varchar")]
        public string status { get; set; }

        public int created_by { get; set; }

        public DateTime created_date { get; set; }

        public int? modified_by { get; set; }

        public DateTime? modified_date { get; set; }

        public RFPMiceRequest RFPMiceRequest { get; set; }
    }
}
