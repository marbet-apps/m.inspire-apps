﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class List_ConferenceFoodGroup
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string conference_food_group_name { get; set; }

        [Required, MaxLength(100), Column(TypeName = "nvarchar")]
        public string conference_food_group_name_de { get; set; }

        public int? sequence { get; set; }

        public ICollection<List_ConferenceFood> ConferenceFoods { get; set; }
    }
}
