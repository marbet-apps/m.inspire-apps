﻿var $MCV = {

    GetCartData: function () {

        $($c.controls.mloader).show();

        $.ajax({
            url: $bind.URL($u.path.GetCartList),
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (response) {

                if (response != undefined && response != null && response.code == "704") {
                    $MCV.LoadCartMap(response.data.ResourceModel);
                } else {
                    $managecode.Decision(false, "", response.code, response.message, "Filter", false);
                }

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
                $bind.EFilterLoader();
                $($c.controls.mloader).hide();
            }
        });

    },
    LoadCartMap: function (data) {

        var infowindow = $v.variables.infowindow;
        var bounds = $v.variables.bounds;

        $.each(data, function (i) {

            var htmlcontents = $search.drawpanel(data[i], data[i].type, $v.variables.i, "T", true, true, true);

            $($c.controls.drawcontents).append(htmlcontents);

            $search.loadimages(data[i].id, data[i].type);

            $v.variables.i = $v.variables.i + 1;

        });

        $bind.bindTooltip();

        if ((data.length) == 0) {
            $($c.controls.mapmsg).show();
            $($c.controls.drawcontents).html("<div id='lblnorecords' class='col-lg-offset-4 col-lg-4 col-md-4 col-sm-4 col-xs-4 ac mt20p'><img src='\\Images\\img_nodata.png'/>&nbsp;<br/><span class='f20'> <b>" + $($c.controls.sNoRecords).val() + "</b> </span></div>");
        } else {
            $($c.controls.mapmsg).hide();
        }

        $($c.controls.mloader).hide();

    },
    RedirectStartRFP: function () {
        window.localStorage.removeItem("activeLink");
       // location.href = "/RFP/StartRFP";
        window.open("/RFP/StartRFP", "_blank");
    }
}

$(document).ready(function () {

    setTimeout(function () {
        $Map.initializeSideMC();
        $MCV.GetCartData();
        $bind.ManageTab($c.controls.licart);
    }, 500);

});

