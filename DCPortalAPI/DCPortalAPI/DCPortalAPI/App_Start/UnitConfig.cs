﻿using DCP.Business;
using DCP.Business.Interface;
using DCP.Repository;
using DCP.Repository.Interface;
using DCPortalAPI.Settings;
using System.Web.Http;
using Unity;

namespace DCPortalAPI.App_Start
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            container.RegisterType<IBSecure, Secure>();
            container.RegisterType<IBSearch, Search>();
            container.RegisterType<IBLookUp, LookUp>();
            container.RegisterType<IRequestDetail, RequestDetail>();
            container.RegisterType<IRequestBL, RequestBL>();
            container.RegisterType(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            container.RegisterType<ISecureRepository, SecureRepository>();
            container.RegisterType<ILookUpRepository, LookUpRepository>();
            container.RegisterType<IRequestDetailRepository, RequestDetailRepository>();
            container.RegisterType<ISearchRepository, SearchRepository>();
            container.RegisterType<IRequestRepository, RequestRepository>();
            container.RegisterType<IUserRepository, UserRepository>();
            container.RegisterType<IUser, User>();
            container.RegisterType<ISyncBL, SyncBL>();
            container.RegisterType<ISyncRepository, SyncRepository>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityResolver(container);

        }
    }
}