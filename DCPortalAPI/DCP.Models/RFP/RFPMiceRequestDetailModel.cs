﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DCP.Mice.Models;
using Newtonsoft.Json;
namespace DCP.Models.RFP
{
    public class RFPMiceRequestDetailModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("rfp_micerequestid")]
        public int RFPMiceRequestId { get; set; }
        [JsonProperty("micerequestid")]
        public int MiceRequestId { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("request_offer_details")]
        public OfferModel RequestOfferDetails { get; set; }
        [JsonProperty("created_by")]
        public int CreatedBy { get; set; }
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("modified_by")]
        public int? ModifiedBy { get; set; }
        [JsonProperty("modified_date")]
        public DateTime? ModifiedDate { get; set; }
    }

    public class RFPMiceRequestWithProposalModel
    {
        [JsonProperty("rfp_id")]
        public int RfpId { get; set; }

        [JsonProperty("mice_user_id")]
        public int? MiceUserId { get; set; }

        [JsonProperty("days_running")]
        public int? DaysRunning { get; set; }

        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }

        [JsonProperty("event_name")]
        public string Event_Name { get; set; }
        
        [JsonProperty("hotel_misc_request_id")]
        public int? Hotel_Mice_Request_id { get; set; }
        
        [JsonProperty("hotel_rfp_misc_id")]
        public int Hotel_Rfp_Misc_Id { get; set; }

        [JsonProperty("hotel_Status")]
        public string Hotel_Status { get; set; }

        [JsonProperty("restaurant_misc_request_id")]
        public int? Restaurant_Misc_request_Id { get; set; }

        [JsonProperty("restaurant_rfp_misc_id")]
        public int Restaurant_Rfp_Misc_Id { get; set; }

        [JsonProperty("restaurant_Status")]
        public string Restaurant_Status { get; set; }

        [JsonProperty("location_misc_request_id")]
        public int? Location_Misc_request_Id { get; set; }

        [JsonProperty("location_rfp_misc_id")]
        public int Location_Rfp_Misc_Id { get; set; }

        [JsonProperty("location_Status")]
        public string Location_Status { get; set; }

        [JsonProperty("username")]
        public string UserName { get; set; }

        [JsonProperty("totalResource")]
        public int TotalResource { get; set; }

        [JsonProperty("hotelstatuscount")]
        public int HotelStatusCount { get; set; }

        [JsonProperty("restaurantstatuscount")]
        public int RestaurantStatusCount { get; set; }

        [JsonProperty("locationstatuscount")]
        public int LocationStatusCount { get; set; }

        [JsonProperty("totalhotelresource")]
        public int TotalHotelResource { get; set; }

        [JsonProperty("totalrestaurantresource")]
        public int TotalRestaurantResource { get; set; }

        [JsonProperty("totallocationresource")]
        public int TotalLocationResource { get; set; }
        ///INSPIRE-102
        [JsonProperty("issubmitted")]
        public bool IsSubmitted { get; set; }

        [JsonProperty("rfpidvalue")]
        public string RFPIdValue { get; set; }
    }

    public class MainRFPMiceRequestWithProposalModel
    {
        [JsonProperty("totalrecords")]
        public int TotalRecords { get; set; }

        [JsonProperty("rfpmicerequestwithproposalmodel")]
        public List<RFPMiceRequestWithProposalModel> RFPMiceRequestWithProposalModels { get; set; }

    }
}
