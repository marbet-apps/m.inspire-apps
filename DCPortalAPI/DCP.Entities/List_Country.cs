﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class List_Country
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(250)]
        public string country_name { get; set; }
        [Required]
        [MaxLength(250)]
        public string country_name_de { get; set; }
        [Column(TypeName = "varchar"),MaxLength(50)]
        public string country_code { get; set; }

        public ICollection<Hotels> Hotels { get; set; }
        public ICollection<Locations> Locations { get; set; }
        public ICollection<Restaurants> Restaurants { get; set; }
    }
}
