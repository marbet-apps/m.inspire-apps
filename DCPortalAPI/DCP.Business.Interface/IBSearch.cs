﻿using DCP.Entities;
using DCP.Models;

namespace DCP.Business.Interface
{
    public interface IBSearch
    {
        /// <summary>
        /// Search Resources
        /// </summary>
        ApiResult GetResourcesList(FilterModel SearchModel, string lang,int? UserId);

        ///// <summary>
        ///// Get Hotel By Id
        ///// </summary>
        //ApiResult GetHotelById(int Id, string Lang);

        ///// <summary>
        ///// Get Restaurant By Id
        ///// </summary>
        //ApiResult GetRestaurantById(int Id, string Lang);

        ///// <summary>
        ///// Get Location By Id
        ///// </summary>
        //ApiResult GetLocationById(int Id, string Lang);

        /// <summary>
        /// Get Images By Id
        /// </summary>
        ApiResult GetImageData(int id, string type);

        /// <summary>
        /// Get Near By Restaurant By Id
        /// </summary>
        ApiResult GetNearByRestaurantList(string lat, string lng, int? cid, string type, int distance);

        /// <summary>
        /// Get Near By Hotel By Id
        /// </summary>
        ApiResult GetNearByHotelList(string lat, string lng, int? cid, string type, int distance);

        /// <summary>
        /// Get Near By Location By Id
        /// </summary>
        ApiResult GetNearByLocationList(string lat, string lng, int? cid, string type, int distance);

        /// <summary>
        /// Get Hotel By Id
        /// </summary>
        ApiResult GetHotelInfo(int id, string Lang, int UserId);

        /// <summary>
        /// Get Restaurant By Id
        /// </summary>
        ApiResult GetRestaurantInfo(int id, string Lang, int UserId);

        /// <summary>
        /// Get Location By Id
        /// </summary>
        ApiResult GetLocationInfo(int id, string Lang, int UserId);

        /// <summary>
        /// Search RFP Resources
        /// </summary>
        ApiResult GetRFPResourceList(string Rids, string type, string lang);

        /// <summary>
        /// Search Cart Resources
        /// </summary>
        ApiResult GetCartList(int UserId, string lang);

        /// <summary>
        /// Get Cart Count
        /// </summary>
        ApiResult GetCartCount(int UserId, string lang);

        /// <summary>
        /// Save search history
        /// </summary>
        ApiResult SaveSearchHistory(string searchCriteria, int userId);

        /// <summary>
        /// Save search history
        /// </summary>
        ApiResult GetSearchCriteria(int userId);

    }

}
