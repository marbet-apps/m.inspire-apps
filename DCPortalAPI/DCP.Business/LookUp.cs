﻿using DCP.Business.Interface;
using DCP.Entities;
using DCP.Models;
using DCP.Repository.Interface;
using DCP.Utilities;
using System;
using System.Net;

namespace DCP.Business
{
    public class LookUp : IBLookUp
    {
        private ILookUpRepository _ILookUpRepository;

        public LookUp(ILookUpRepository _ILookUpRepository)
        {
            this._ILookUpRepository = _ILookUpRepository;
        }

        /// <summary>
        /// Get All Country List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetCountryList(string Lang, string term, int page, int pageSize, int type)
        {
            try
            {
                var List =  _ILookUpRepository.GetCountryList(Lang, term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get All Radius List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetRadiusList(string Lang, string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetRadiusList(Lang, term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);

            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get All Hotel Chain List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetChainList(string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetChainList(term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Hotel List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetHotelList(string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetHotelList(term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Restaurant List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetRestaurantList(string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetRestaurantList(term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Location List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetLocationList(string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetLocationList(term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get All Hotel Type List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetHotelTypeList(string Lang, string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetHotelTypeList(Lang, term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get Default Hotel Type List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetDefaultHotelTypeList(string Lang,int[] Ids)
        {
            try
            {
                var List = _ILookUpRepository.GetDefaultHotelTypeList(Lang,Ids);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get All Restaurant Type List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetRestaurantTypeList(string Lang, string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetRestaurantTypeList(Lang, term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get All Location Type List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetLocationTypeList(string Lang, string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetLocationTypeList(Lang, term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get All Kitchen List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetKitchenList(string Lang, string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetKitchenList(Lang, term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Get All Suitable For List
        /// </summary>
        /// <returns></returns>
        public ApiResult GetSuitableForList(string Lang, string term, int page, int pageSize, int type)
        {
            try
            {
                var List = _ILookUpRepository.GetSuitableForList(Lang, term, page, pageSize, type);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        /// <summary>
        /// Retrieve subscription list.
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        public ApiResult GetSubscriptionTypeList(string Lang)
        {
            try
            {
                var List = _ILookUpRepository.GetSubscriptionTypeList(Lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
        /// <summary>
        /// Retrieve Request status List
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        public ApiResult GetRequestStatusList(string Lang)
        {
            try
            {
                var List = _ILookUpRepository.GetRequestStatusList(Lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetRequestTypes(string Lang)
        {
            try
            {
                var List = _ILookUpRepository.GetRequestTypes(Lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetRequestItemTypes(string Lang)
        {
            try
            {
                var List = _ILookUpRepository.GetRequestItemTypes(Lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetRoomTypes(string Lang)
        {
            try
            {
                var List = _ILookUpRepository.GetRoomTypes(Lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetRoomTechniques(string Lang)
        {
            try
            {
                var List = _ILookUpRepository.GetRoomTechniques(Lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetSeatingTypes(string Lang)
        {
            try
            {
                var List = _ILookUpRepository.GetSeatingTypes(Lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetDaysRunning(string Lang)
        {
            try
            {
                var List = _ILookUpRepository.GetDaysRunning(Lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetTimeIntervals(int interval)
        {
            try
            {
                var List = _ILookUpRepository.GetTimeIntervals(interval);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }


        public ApiResult GetCityList()
        {
            try
            {
                var List = _ILookUpRepository.GetCityList();
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult GetAbortReason(string lang)
        {
            try
            {
                var List = _ILookUpRepository.GetAbortReason(lang);
                return ResultSet.SuccessResponse(ResponceCodes.Success.GetList, List, ManageEnum.status.success.ToString(), ResponceMessages.Success.Retrieved);
            }
            catch (WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
    }
}
