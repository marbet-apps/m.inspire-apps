﻿using DCP.Entities;
using DCP.Utilities;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace DCPortalAPI.Settings
{
    internal class TokenValidationHandler : DelegatingHandler
    {
        /// <summary>
        /// Retrieve Tocken
        /// </summary>
        /// <param name="request"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private static bool TryRetrieveToken(HttpRequestMessage request, out string token)
        {
            token = null;
            IEnumerable<string> authzHeaders;

            if (!request.Headers.TryGetValues("Authorization", out authzHeaders) || authzHeaders.Count() > 1)
            {
                return false;
            }

            var bearerToken = authzHeaders.ElementAt(0);
            token = bearerToken.StartsWith("Bearer ") ? bearerToken.Substring(7) : bearerToken;
            return true;

        }

        /// <summary>
        /// Send Message Async
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpStatusCode statusCode;

            string token;
            DCP.Entities.ApiResult ApiResult = new DCP.Entities.ApiResult();
            string TokenKey = Constants.TokenKey;

            //determine whether a jwt exists or not
            if (!TryRetrieveToken(request, out token))
            {
                //ApiResult = DCP.Business.Interface.result.Response(ErrorCodes.MissingToken.ToString(), "", ManageEnum.status.fail.ToString(), ErrorMessages.MissingToken);
                //return Task<HttpResponseMessage>.Factory.StartNew(() => new HttpResponseMessage() { StatusCode = HttpStatusCode.Unauthorized, Content = new StringContent(JsonConvert.SerializeObject(ApiResult), Encoding.UTF8, "application/json") });
                return base.SendAsync(request, cancellationToken);
            }

            try
            {
                var now = DateTime.UtcNow;
                var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(TokenKey));

                SecurityToken securityToken;
                JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

                TokenValidationParameters validationParameters = new TokenValidationParameters()
                {
                    ValidAudience = Constants.APIBaseURL,
                    ValidIssuer = Constants.APIBaseURL,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    LifetimeValidator = this.LifetimeValidator,
                    IssuerSigningKey = securityKey
                };

                //extract and assign the user of the jwt

                Thread.CurrentPrincipal = handler.ValidateToken(token, validationParameters, out securityToken);
                HttpContext.Current.User = handler.ValidateToken(token, validationParameters, out securityToken);

                return base.SendAsync(request, cancellationToken);
            }
            catch (SecurityTokenValidationException ex)
            {
                statusCode = HttpStatusCode.Unauthorized;
                ApiResult = ResultSet.ErrorResponse(((int)HttpStatusCode.Unauthorized).ToString(), ex);
            }
            catch (Exception ex)
            {
                statusCode = HttpStatusCode.InternalServerError;
                ApiResult = ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }

            return base.SendAsync(request, cancellationToken);
            // return Task<HttpResponseMessage>.Factory.StartNew(() => new HttpResponseMessage() { StatusCode = statusCode, Content = new StringContent(JsonConvert.SerializeObject(ApiResult), Encoding.UTF8, "application/json") });

        }

        /// <summary>
        /// Tocken Validator
        /// </summary>
        /// <param name="notBefore"></param>
        /// <param name="expires"></param>
        /// <param name="securityToken"></param>
        /// <param name="validationParameters"></param>
        /// <returns></returns>
        public bool LifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            if (expires != null)
            {
                if (DateTime.UtcNow < expires) return true;
            }

            return false;
        }

        /// <summary>
        /// Create JWT Tocken
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static string CreateToken(string username, int usertype)
        {
            //Set issued at date
            DateTime issuedAt = DateTime.UtcNow;

            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddMinutes(Convert.ToDouble(Constants.TokenExpInMin));

            var tokenHandler = new JwtSecurityTokenHandler();

            //create a identity and add claims to the user which we want to log in
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name,username),
                new Claim(ClaimTypes.Role,usertype.ToString())
            });

            //const string sec = TokenKey;
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(Constants.TokenKey));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);

            //create the jwt
            var token = tokenHandler.CreateJwtSecurityToken(issuer: Constants.APIBaseURL, audience: Constants.APIBaseURL,
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }

    }
}