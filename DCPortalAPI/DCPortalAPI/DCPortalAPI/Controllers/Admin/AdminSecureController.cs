﻿using DCP.Business.Interface;
using DCP.Models;
using DCP.Utilities;
using DCPortalAPI.Filter;
using DCPortalAPI.Settings;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using static DCP.Utilities.ManageConstants;

namespace DCPortalAPI.Controllers.Admin
{
    [RoutePrefix("api/AdminSecure")]
    [ValidateModel]
    public class AdminSecureController : ApiController
    {
        private IBSecure _IBSecure;
        private IRequestDetail _IRequestDetail;

        public AdminSecureController(IBSecure IBSecure, IRequestDetail _IRequestDetail)
        {
            this._IBSecure = IBSecure;
            this._IRequestDetail = _IRequestDetail;
        }
        /// <summary>
        /// API for administrator Login.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        public IHttpActionResult Login(AdminCredentialRequest login)
        {
            WriteLog.LogInformation(LogMessages.LoginStarted);

            try
            {
                var Response = _IBSecure.CheckAdminUserAuth(login.Email, Crypto.EncryptSHA512Managed(login.Password), login.UserType, out bool flag);

                if (flag)
                {
                    return Ok(Response).AddHeader("authorization", TokenValidationHandler.CreateToken(login.Email, login.UserType));
                }

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.LoginEnded);

            }
        }
        /// <summary>
        /// Register new user.
        /// </summary>
        /// <param name="userRegisterModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Register")]
        [Authorize]
        public IHttpActionResult Register(UserRegisterModel userRegisterModel)
        {
            WriteLog.LogInformation(LogMessages.RegisterStarted);

            try
            {
               
               // userRegisterModel.Password = Crypto.GeneratePassword(5);
                userRegisterModel.isAdmin = userRegisterModel.Subscription_Type_Name == 3 ? true : false;
                var Response = _IBSecure.RegisterUser(userRegisterModel, out bool flag);
                var lang = CommonMethod.GetLanguageHeader(Request.Headers);

                if(Response.Code=="701")
                {
                    var model = (RegisterationModel)Response.Data;
                    string idValue = Crypto.encrypt(model.Id.ToString());


                    string requestDateTime = Crypto.EnryptString(System.DateTime.Now.ToString("ddMMyyyyHHmm"));

                   

                    bool isSuccess = MailUtility.SendRegistrationMail(lang, userRegisterModel.First_Name, userRegisterModel.Last_Name, userRegisterModel.Email, userRegisterModel.Password, idValue, requestDateTime);

                    if (isSuccess == false)
                    {
                        Response.Code = ResponceCodes.Success.SendMailFail;
                        Response.Message = ResponceMessages.Success.SendMailFail;
                    }
                }

                

                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RegisterEnded);

            }
        }
        /// <summary>
        /// generate new password of the user.
        /// </summary>
        /// <param name="userPassword"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("ForgotPassword")]
        public IHttpActionResult ForgotPassword(UserPasswordModel userPassword)
        {
            WriteLog.LogInformation(LogMessages.GeneratePasswordStarted);

            try
            {
                string requestDateTime = Crypto.EnryptString(System.DateTime.Now.ToString("ddMMyyyyHHmm"));
                // userPassword.Password = Crypto.GeneratePassword(5);
                var result = _IBSecure.ForgotUserPassword(userPassword, out bool flag, out string name, requestDateTime);
                if (result.Code == "707")
                {
                    var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                    var model = (UserPasswordModel)result.Data;
                    model.strId = Crypto.encrypt(model.Id.ToString());
                   
                    bool isSuccess = MailUtility.SendForgotPasswordMail(lang, name, userPassword.Email, userPassword.Password,model.strId, requestDateTime);

                    if (isSuccess == false)
                    {
                        result.Code = ResponceCodes.Success.FogotPasswordMailFail;
                        result.Message = ResponceMessages.Success.FogotPasswordMailFail;
                    }
                }
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GeneratePasswordEnded);

            }
        }
        /// <summary>
        /// Get list of all user.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="activestatus"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllUser")]
        [Authorize]
        public IHttpActionResult GetAllUser(string email = "", string activestatus = "All", int page = 1, int pageSize = 50)
        {
            WriteLog.LogInformation(LogMessages.RetriveAllUserStarted);
            try
            {
                var result = _IBSecure.GetAllUser(activestatus, email, page, pageSize);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RetriveAllUserEnded);
            }
        }
        /// <summary>
        /// get user details.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("UserDetail")]
        [Authorize]
        public IHttpActionResult GetUserDetail(int userID)
        {
            WriteLog.LogInformation(LogMessages.RetriveAllUserStarted);
            try
            {
                var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                var result = _IBSecure.GetUserDetail(userID, lang);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RetriveAllUserEnded);
            }
        }
        /// <summary>
        /// Delete user details.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("DeleteUser")]
        [Authorize]
        public IHttpActionResult RemoveUser(int userID)
        {
            WriteLog.LogInformation(LogMessages.DeleteUser);
            try
            {
                var result = _IBSecure.RemoveUser(userID);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.DeleteUserEnded);
            }
        }
        /// <summary>
        /// API for change User's password.
        /// </summary>
        /// <param name="changePassword"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ChangePassword")]
        public IHttpActionResult ChangePassword(ChangePasswordModel changePassword)
        {
            WriteLog.LogInformation(LogMessages.ChangePasswordStarted);
            try
            {
                var result = _IBSecure.ChangePassword(changePassword);
                var lang = CommonMethod.GetLanguageHeader(Request.Headers);
                if (result.Code == "712")
                {
                    
                    MailUtility.SendPasswordGeneratedMail(lang, changePassword.First_Name + " " + changePassword.Last_Name, changePassword.Email);
                }
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.ChangePasswordEnd);
            }

        }
        /// <summary>
        /// Update user details Including password/Active status/Delete status.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpdateUser")]
        [Authorize]
        public IHttpActionResult UpdateUser(int userID, int loggedInUserID, UpdateUserModel user)
        {
            WriteLog.LogInformation(LogMessages.UpdateUserStarted);

            try
            {
                var result = _IBSecure.UpdateUser(userID, loggedInUserID, user);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.UpdateUserEnd);

            }
        }

        /// <summary>
        /// Retrive RFPMiceRequest with Proposal
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRFPMiceRequestWithProposal")]
        public IHttpActionResult GetRFPMiceRequestWithProposal(int page, int pageSize, string sortby = null, string search = null)
        {
            WriteLog.LogInformation(LogMessages.GetRFPMiceRequestWithProposalStarted);
            try
            {
                var userId = CommonMethod.GetUserIdHeader(Request.Headers);
                var result = _IRequestDetail.GetRFPMiceRequestWithProposal(page, pageSize, null, sortby, search);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.GetRFPMiceRequestWithProposalEnded);
            }
        }

        /// <summary>
        /// Retrive list for RFP.
        /// </summary>
        /// <param name="term"></param>
        /// <param name="status"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRFPList")]
        [Authorize]
        public IHttpActionResult GetRfpList(int page, int pageSize, string status, string term = "")
        {
            WriteLog.LogInformation(LogMessages.RFPListStarted);
            try
            {
                var result = _IRequestDetail.GetRfpList(page, pageSize, status, CommonMethod.GetHeaders(Request.Headers, "Lang"), term);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RFPListEnded);
            }

        }
        /// <summary>
        /// Retrive details for RFP.
        /// </summary>
        /// <param name="rfpId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRFPDetail")]
        [Authorize]
        public IHttpActionResult GetRFPDetails(int rfpId, string type)
        {
            WriteLog.LogInformation(LogMessages.RFPListStarted);
            try
            {
                var result = _IRequestDetail.GetRFPDetail(rfpId, 0, type);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.RFPListEnded);
            }
        }

        /// <summary>
        /// Update User Profile
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("UpdateUserProfile")]
        [Authorize]
        public IHttpActionResult UpdateUserProfile(UpdateProfileModel model)
        {
            WriteLog.LogInformation(LogMessages.UpdateUserProfileStarted);

            try
            {
                var result = _IBSecure.UpdateUseProfile(model);

                ////var res = (UpdateProfileResponseModel)result.Data;
                ////var resData = res.UpdateProfileModel;
                ////string idValue = Crypto.encrypt(resData.LogedInUserId.ToString());

                ////string requestDateTime = Crypto.EnryptString(System.DateTime.Now.ToString("ddMMyyyyHHmm"));
                ////bool isSuccess = MailUtility.SendRegistrationMail("en-US", resData.First_Name, resData.Last_Name, resData.Email, "", idValue, requestDateTime);

                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.UpdateUserProfileEnded);

            }
        }

        /// <summary>
        /// Upload image
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadImage")]
        [Authorize]
        public IHttpActionResult UploadImage()
        {
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                // Get the uploaded image from the Files collection
                var httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];

                int userId = Convert.ToInt32(HttpContext.Current.Request.Form["UserId"]);
                byte[] bytes;
                if (httpPostedFile != null)
                {
                    
                    Image source = Image.FromStream(httpPostedFile.InputStream);
                    double widthRatio = ((double)ProfileSetting.maxWidth) / source.Width;
                    double heightRatio = ((double)ProfileSetting.maxHeight) / source.Height;
                    double ratio = (widthRatio < heightRatio) ? widthRatio : heightRatio;
                    Image thumbnail = source.GetThumbnailImage((int)(source.Width * ratio), (int)(source.Height * ratio), null, IntPtr.Zero);
                    using (var memory = new MemoryStream())
                    {
                        thumbnail.Save(memory, source.RawFormat);
                        bytes = memory.ToArray();
                    }

                    //if(httpPostedFile.ContentLength> 512000)
                    //{

                    //}
                    //else
                    //{
                    //    using (BinaryReader br = new BinaryReader(httpPostedFile.InputStream))
                    //    {
                    //        bytes = br.ReadBytes(httpPostedFile.ContentLength);
                    //    }
                    //}





                    var result = _IBSecure.UploadImage(bytes,userId);

                    if(result.Code=="718")
                    {
                        return Json(new { base64imgage = Convert.ToBase64String(bytes) });
                    }
                }

                
            }
            return Json(new { base64imgage = "" });
        }

       /// <summary>
       /// Delete image
       /// </summary>
       /// <param name="userId"></param>
       /// <returns></returns>
        [HttpPost]
        [Route("DeleteImage")]
        [Authorize]
        public IHttpActionResult DeleteImage(UpdateProfilePhotoModel model)
        {
            WriteLog.LogInformation(LogMessages.DeleteImageStarted);

            try
            {
                var result = _IBSecure.DeleteImage(model.LogedInUserId);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.DeleteImageEnded);

            }
        }


       
    }
}