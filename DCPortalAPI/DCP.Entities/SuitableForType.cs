﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class SuitableForType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int dcp_transaction_id { get; set; }
        [Required]
        public int dcp_suitablefor_id { get; set; }
        [Required]
        public int dcp_suitablefor_type { get; set; }

        public List_SuitableFor SuitableFor { get; set; }
    }
}
