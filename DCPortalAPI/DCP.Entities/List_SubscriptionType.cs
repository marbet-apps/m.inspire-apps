﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
namespace DCP.Entities
{
    public class List_SubscriptionType
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Type_Name { get; set; }
        [Required]
        public string Type_Name_de { get; set; }

        public string Description { get; set; }

        public string Description_De { get; set; }

        public ICollection<User_Master> User_Master { get; set; }

    }
}
