﻿using DCP.Entities;
using DCP.Models;
using DCP.Repository.Interface;
using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
namespace DCP.Repository
{
    public class SyncRepository : ISyncRepository
    {
        private readonly DCPDbContext _context = null;

        public SyncRepository()
        {
            _context = new DCPDbContext();
        }
        public List<ResponseSyncModel> SyncHotels(MigrationHotelModel hotelMigrationModels)
        {
            ResponseSyncModel responseSyncModel = new ResponseSyncModel();
            List<ResponseSyncModel> responseModel = new List<ResponseSyncModel>();

            List<HotelMigrationModel> migrationModels = hotelMigrationModels.Hotels.Where(t => !string.IsNullOrEmpty(t.mice_id) && (t.Closed == null || t.Closed == false)).ToList();
            if (migrationModels != null && migrationModels.Count > 0)
            {
                foreach (var item in migrationModels)
                {
                    responseSyncModel = new ResponseSyncModel();
                    try
                    {
                        var hotel = _context.Hotels.Where(x => x.kb_id == item.kb_id).FirstOrDefault();
                        if (hotel != null)
                        {
                            hotel.kb_id = item.kb_id;
                            hotel.mice_id = item.mice_id.ToString();
                            hotel.hotel_name = item.hotel_name;
                            hotel.hotel_chain = item.hotel_chain;
                            hotel.country_id = item.country_id.HasValue ? item.country_id.Value : 0;
                            hotel.state = item.state;
                            hotel.city = item.city;
                            hotel.street = item.street;
                            hotel.zipcode = item.zipcode;
                            hotel.webaddress = item.webaddress;
                            hotel.star = item.star;
                            hotel.airport_id = item.airport_id;
                            hotel.airport_transfer_time = item.airport_transfer_time;
                            hotel.airport_distance = item.airport_distance;
                            hotel.total_rooms = item.total_rooms;
                            hotel.total_conference = item.total_conference;
                            hotel.size_of_largest_conference = item.size_of_largest_conference;
                            hotel.parking_spaces = item.parking_spaces;
                            hotel.miscellaneous_info = item.miscellaneous_info;
                            hotel.PP = item.PP;
                            hotel.green = item.green;
                            hotel.reopening = item.reopening;
                            hotel.blacked = item.blacked;
                            hotel.closed = item.Closed;
                            hotel.hotel_lan = item.hotel_lan;
                            hotel.hotel_lat = item.hotel_lat;
                            hotel.is360degreeview = item.is360degreeview;

                            //_context.Entry(hotel).State = System.Data.Entity.EntityState.Modified;

                        }
                        else
                        {
                            hotel = (new Hotels
                            {
                                kb_id = item.kb_id,
                                mice_id = item.mice_id,
                                hotel_name = item.hotel_name,
                                hotel_chain = item.hotel_chain,
                                country_id = item.country_id.HasValue ? item.country_id.Value : 0,
                                state = item.state,
                                city = item.city,
                                street = item.street,
                                zipcode = item.zipcode,
                                webaddress = item.webaddress,
                                star = item.star,
                                airport_id = item.airport_id,
                                airport_transfer_time = item.airport_transfer_time,
                                airport_distance = item.airport_distance,
                                total_rooms = item.total_rooms,
                                total_conference = item.total_conference,
                                size_of_largest_conference = item.size_of_largest_conference,
                                parking_spaces = item.parking_spaces,
                                miscellaneous_info = item.miscellaneous_info,
                                PP = item.PP,
                                green = item.green,
                                reopening = item.reopening,
                                blacked = item.blacked,
                                closed = item.Closed,
                                hotel_lan = item.hotel_lan,
                                hotel_lat = item.hotel_lat,
                                is360degreeview = item.is360degreeview,
                                created_by = 1,
                                modify_by = 0,
                                created_date = DateTime.Now,
                                modify_date = DateTime.Now
                            });

                            _context.Hotels.Add(hotel);
                        }
                        _context.SaveChanges();

                        var hotelType = _context.HotelType.Where(y => y.dcp_hotelinfo_id == hotel.dcp_hotelinfo_id).ToList();
                        _context.HotelType.RemoveRange(hotelType);

                        if(hotelMigrationModels.HotelType!=null && hotelMigrationModels.HotelType.Count>0)
                        {
                            List<kb_Hotel_Type> kb_Hotel_Types = hotelMigrationModels.HotelType.Where(t => t.kb_hotelinfo_id == item.kb_id).ToList();
                            foreach (var hotelTypeItem in kb_Hotel_Types)
                            {
                                var hotelTypeDbEntities = new Entities.HotelType()
                                {
                                    dcp_hotelinfo_id = hotel.dcp_hotelinfo_id,
                                    dcp_hoteltype_id = hotelTypeItem.smt_hoteltypelist_id
                                };
                                _context.HotelType.Add(hotelTypeDbEntities);
                            }
                        }
                       

                        var hotelPhoto = _context.Photos.Where(z => z.transaction_id == hotel.dcp_hotelinfo_id && z.photo_type == (int) ManageEnum.resourcetype.hotel).ToList();
                        _context.Photos.RemoveRange(hotelPhoto);

                        if(hotelMigrationModels.Photos!=null && hotelMigrationModels.Photos.Count>0)
                        {
                            List<Models.Photos> photoList = hotelMigrationModels.Photos.Where(t => t.kb_transaction_id == item.kb_id).ToList();
                            foreach (var photosItem in photoList)
                            {
                                Entities.Photos photos = new Entities.Photos();
                                photos.photo_name = photosItem.photo_name;
                                photos.photo_size = photosItem.photo_size;
                                photos.photo_type = photosItem.kb_photo_type;
                                photos.transaction_id = hotel.dcp_hotelinfo_id;
                                _context.Photos.Add(photos);
                            }
                        }
                        
                        _context.SaveChanges();

                        responseSyncModel.IsSyncSuccess = true;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "H";
                        responseModel.Add(responseSyncModel);
                    }
                    catch (Exception ex)
                    {
                        responseSyncModel.IsSyncSuccess = false;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "H";
                        responseModel.Add(responseSyncModel);
                    }
                }
            }

            migrationModels = hotelMigrationModels.Hotels.Where(t => !string.IsNullOrEmpty(t.mice_id) && (t.Closed != null && t.Closed == true)).ToList();
            if(migrationModels!=null && migrationModels.Count > 0)
            {
                foreach (var item in migrationModels)
                {
                    responseSyncModel = new ResponseSyncModel();
                    try
                    {
                        Hotels hotels = _context.Hotels.Where(t => t.kb_id == item.kb_id).FirstOrDefault();
                        if (hotels != null)
                        {
                            List<HotelType> hotelTypes = _context.HotelType.Where(t => t.dcp_hotelinfo_id == hotels.dcp_hotelinfo_id).ToList();
                            if (hotelTypes.Count > 0)
                            {
                                _context.HotelType.RemoveRange(hotelTypes);
                                _context.SaveChanges();
                            }

                            List<Entities.Photos> photos = _context.Photos.Where(t => t.photo_type == 0 && t.transaction_id == hotels.dcp_hotelinfo_id).ToList();
                            if (photos.Count() > 0)
                            {
                                _context.Photos.RemoveRange(photos);
                                _context.SaveChanges();
                            }

                            _context.Hotels.Remove(hotels);
                            _context.SaveChanges();
                            
                        }

                        responseSyncModel.IsSyncSuccess = true;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "H";
                        responseModel.Add(responseSyncModel);
                    }
                    catch (Exception)
                    {
                        responseSyncModel.IsSyncSuccess = false;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "H";
                        responseModel.Add(responseSyncModel);
                    }
                }
            }

            return responseModel;
        }
        public List<ResponseSyncModel> SyncLocations(MigrationLocationModel LocationMigrationModels)
        {
            List<ResponseSyncModel> responseModel = new List<ResponseSyncModel>();
            ResponseSyncModel responseSyncModel = new ResponseSyncModel();
            List<LocationMigerationModel> migrationModels =  LocationMigrationModels.Locations.Where(t => t.closed == null || t.closed == false).ToList();
            if (migrationModels != null && migrationModels.Count > 0)
            {
                foreach (var item in migrationModels)
                {
                    responseSyncModel = new ResponseSyncModel();
                    try
                    {
                        var location = _context.Locations.Where(x => x.kb_id == item.kb_id).FirstOrDefault();
                        if (location != null)
                        {
                            location.kb_id = item.kb_id;
                            location.mice_id = item.mice_id.ToString();
                            location.location_name = item.location_name;
                            location.country_id = item.country_id > 0 ? item.country_id : 0;
                            location.state = item.state;
                            location.city = item.city;
                            location.street = item.street;
                            location.zipcode = item.zipcode;
                            location.webaddress = item.webaddress;
                            location.technik = item.technik;
                            location.car_drive = item.car_drive;
                            location.room_height = item.room_height;
                            location.rent_begining = item.rent_begining;
                            location.catering = item.catering;
                            location.total_conference = item.total_conference;
                            location.size_of_largest_conference = item.size_of_largest_conference;
                            location.outdoor_possibility = item.outdoor_possibility;
                            location.miscellaneous_info = item.miscellaneous_info;
                            location.PP = item.PP;
                            location.green = item.green;
                            location.reopening = item.reopening;
                            location.blacked = item.blacked;
                            location.closed = item.closed;
                            location.location_lan = item.location_lan;
                            location.location_lat = item.location_lat;
                            location.is360degreeview = item.is360degreeview;
                            location.outdoor_details = item.outdoor_details;

                        }
                        else
                        {
                            location = (new Locations
                            {
                                kb_id = item.kb_id,
                                mice_id = item.mice_id,
                                location_name = item.location_name,
                                country_id = item.country_id > 0 ? item.country_id : 0,
                                state = item.state,
                                city = item.city,
                                street = item.street,
                                zipcode = item.zipcode,
                                webaddress = item.webaddress,
                                technik = item.technik,
                                car_drive = item.car_drive,
                                room_height = item.room_height,
                                rent_begining = item.rent_begining,
                                catering = item.catering,
                                total_conference = item.total_conference,
                                size_of_largest_conference = item.size_of_largest_conference,
                                outdoor_possibility = item.outdoor_possibility,
                                miscellaneous_info = item.miscellaneous_info,
                                PP = item.PP,
                                green = item.green,
                                reopening = item.reopening,
                                blacked = item.blacked,
                                closed = item.closed,
                                location_lan = item.location_lan,
                                location_lat = item.location_lat,
                                is360degreeview = item.is360degreeview,
                                outdoor_details = item.outdoor_details,
                                created_by = 1,
                                modify_by = 0,
                                created_date = DateTime.Now,
                                modify_date = DateTime.Now
                            });

                            _context.Locations.Add(location);
                        }
                        _context.SaveChanges();


                        var locationType = _context.LocationType.Where(y => y.dcp_locationinfo_id == location.dcp_locationinfo_id).ToList();
                        _context.LocationType.RemoveRange(locationType);

                        List<Models.LocationType> locationTypeList = LocationMigrationModels.LocationType.Where(t => t.kb_locationinfo_id == item.kb_id).ToList();
                        foreach (var locationTypeitem in locationTypeList)
                        {
                            var templocation = (new Entities.LocationType{

                                dcp_locationinfo_id = location.dcp_locationinfo_id,
                                dcp_locationtype_id = locationTypeitem.smt_list_locationtype_id

                            });
                            _context.LocationType.Add(templocation);
                        }


                        var photos = _context.Photos.Where(z => z.transaction_id == location.dcp_locationinfo_id && z.photo_type == (int)ManageEnum.resourcetype.location).ToList();
                        _context.Photos.RemoveRange(photos);

                        List<Models.Photos> photoList =  LocationMigrationModels.Photos.Where(t => t.kb_transaction_id == item.kb_id).ToList();
                        foreach (var Photositem in photoList)
                        {
                            var tempPhotos = (new Entities.Photos
                            {
                                photo_name = Photositem.photo_name,
                                photo_size = Photositem.photo_size,
                                photo_type = Photositem.kb_photo_type,
                                transaction_id = location.dcp_locationinfo_id,
                            });
                            _context.Photos.Add(tempPhotos);
                        }


                        var suitableForTypes = _context.SuitableForType.Where(a => a.dcp_transaction_id == location.dcp_locationinfo_id && a.dcp_suitablefor_type == (int)ManageEnum.resourcetype.location).ToList();
                        _context.SuitableForType.RemoveRange(suitableForTypes);

                        List<Models.KbSuitable> suitablesList =  LocationMigrationModels.KbSuitable.Where(t => t.kb_transaction_id == item.kb_id).ToList();
                        foreach (var suitableForTypesitem in suitablesList)
                        {
                            var tempSuitable = (new Entities.SuitableForType
                            {
                                dcp_suitablefor_type = suitableForTypesitem.kb_tripinfo_type,
                                dcp_suitablefor_id = suitableForTypesitem.smt_suitablefor_id,
                                dcp_transaction_id = location.dcp_locationinfo_id
                            });
                            _context.SuitableForType.Add(tempSuitable);
                        }

                        
                        _context.SaveChanges();
                        responseSyncModel.IsSyncSuccess = true;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "L";
                        responseModel.Add(responseSyncModel);
                    }
                    catch (Exception)
                    {
                        responseSyncModel.IsSyncSuccess = false;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "L";
                        responseModel.Add(responseSyncModel);
                    }
                }
            }

            migrationModels = LocationMigrationModels.Locations.Where(t =>  (t.closed != null && t.closed == true)).ToList();
            if(migrationModels != null && migrationModels.Count>0)
            {
                foreach (var item in migrationModels)
                {
                    responseSyncModel = new ResponseSyncModel();
                    try
                    {
                        
                        Locations locations = _context.Locations.Where(t => t.kb_id == item.kb_id).FirstOrDefault();
                        if (locations != null)
                        {
                            List<Entities.LocationType> locationTypes = _context.LocationType.Where(t => t.dcp_locationinfo_id == locations.dcp_locationinfo_id).ToList();
                            if (locationTypes.Count > 0)
                            {
                                _context.LocationType.RemoveRange(locationTypes);
                                _context.SaveChanges();
                            }

                            List<Entities.Photos> photos = _context.Photos.Where(t => t.photo_type == 2 && t.transaction_id == locations.dcp_locationinfo_id).ToList();
                            if (photos.Count() > 0)
                            {
                                _context.Photos.RemoveRange(photos);
                                _context.SaveChanges();
                            }

                            List<Entities.SuitableForType> suitableForTypes = _context.SuitableForType.Where(t => t.dcp_suitablefor_type == 2 && t.dcp_transaction_id == locations.dcp_locationinfo_id).ToList();
                            if (suitableForTypes.Count() > 0)
                            {
                                _context.SuitableForType.RemoveRange(suitableForTypes);
                                _context.SaveChanges();
                            }


                            _context.Locations.Remove(locations);
                            _context.SaveChanges();
                        }

                        responseSyncModel.IsSyncSuccess = true;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "L";
                        responseModel.Add(responseSyncModel);
                    }
                    catch (Exception)
                    {                        
                        responseSyncModel.IsSyncSuccess = false;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "L";
                        responseModel.Add(responseSyncModel);
                    }
                }
            }


            return responseModel;
        }


        public List<ResponseSyncModel> SyncRestaurants(MigrationResaurantModel ResaurantMigrationModels)
        {
            List<ResponseSyncModel> responseModel = new List<ResponseSyncModel>();
            ResponseSyncModel responseSyncModel = new ResponseSyncModel();

            List<RestaurantMigerationModel> migrationModels =  ResaurantMigrationModels.Restaurants.Where(t => t.closed == null || t.closed == false).ToList();

            if (migrationModels != null && migrationModels.Count > 0)
            {
                foreach (var item in migrationModels)
                {
                    responseSyncModel = new ResponseSyncModel();
                    try
                    {
                        var restaurant = _context.Restaurants.Where(x => x.kb_id == item.kb_id).FirstOrDefault();
                        if (restaurant != null)
                        {
                            restaurant.kb_id = item.kb_id;
                            restaurant.mice_id = item.mice_id.ToString();
                            restaurant.restaurant_name = item.restaurant_name;
                            restaurant.country_id = item.country_id.HasValue? item.country_id.Value : 0;
                            restaurant.state = item.state;
                            restaurant.city = item.city;
                            restaurant.street = item.street;
                            restaurant.zipcode = item.zipcode;
                            restaurant.webaddress = item.webaddress;
                            restaurant.total_conference = item.total_conference;
                            restaurant.size_of_largest_conference = item.size_of_largest_conference;
                            restaurant.outdoor_possibility = item.outdoor_possibility;
                            restaurant.miscellaneous_info = item.miscellaneous_info;
                            restaurant.PP = item.PP;
                            restaurant.green = item.green;
                            restaurant.reopening = item.reopening;
                            restaurant.blacked = item.blacked;
                            restaurant.closed = item.closed;
                            restaurant.restaurant_in_hotel = item.restaurant_in_hotel;
                            restaurant.hotelname = item.hotelname;
                            restaurant.restaurant_lan = item.restaurant_lan;
                            restaurant.restaurant_lat = item.restaurant_lat;
                            restaurant.is360degreeview = item.is360degreeview;
                            restaurant.outdoor_details = item.outdoor_details;
                            restaurant.capacity = item.capacity;
                            restaurant.capacity_of_largest_room = item.capacity_of_largest_room;
                            restaurant.minimum_turnover = item.minimum_turnover;

                        }
                        else
                        {
                            restaurant = (new Restaurants
                            {
                            kb_id = item.kb_id,
                            mice_id = item.mice_id.ToString(),
                            restaurant_name = item.restaurant_name,
                            country_id = item.country_id.HasValue ? item.country_id.Value : 0,
                            state = item.state,
                            city = item.city,
                            street = item.street,
                            zipcode = item.zipcode,
                            webaddress = item.webaddress,
                            total_conference = item.total_conference,
                            size_of_largest_conference = item.size_of_largest_conference,
                            outdoor_possibility = item.outdoor_possibility,
                            miscellaneous_info = item.miscellaneous_info,
                            PP = item.PP,
                            green = item.green,
                            reopening = item.reopening,
                            blacked = item.blacked,
                            closed = item.closed,
                            restaurant_in_hotel = item.restaurant_in_hotel,
                            hotelname = item.hotelname,
                            restaurant_lan = item.restaurant_lan,
                            restaurant_lat = item.restaurant_lat,
                            is360degreeview = item.is360degreeview,
                            outdoor_details = item.outdoor_details,
                            capacity = item.capacity,
                            capacity_of_largest_room = item.capacity_of_largest_room,
                            minimum_turnover = item.minimum_turnover,
                            created_by = 1,
                                modify_by = 0,
                                created_date = DateTime.Now,
                                modify_date = DateTime.Now
                            });

                            _context.Restaurants.Add(restaurant);
                        }
                        _context.SaveChanges();

                        var restaurnatType = _context.RestaurantType.Where(y => y.dcp_restaurantinfo_id == restaurant.dcp_restaurantinfo_id).ToList();
                        _context.RestaurantType.RemoveRange(restaurnatType);

                        List<kb_restaurant_type> restaurant_Types = ResaurantMigrationModels.RestaurantTType.Where(t => t.kb_restaurantinfo_id == item.kb_id).ToList();
                        foreach (var restaurantTypeitem in restaurant_Types)
                        { 
                            var restaurantType = (new Entities.RestaurantType
                            {

                                dcp_restaurantinfo_id = restaurant.dcp_restaurantinfo_id,
                                dcp_restauranttype_id = restaurantTypeitem.smt_restauranttypelist_id

                            });
                            _context.RestaurantType.Add(restaurantType);
                        }


                        var photos = _context.Photos.Where(z => z.transaction_id == restaurant.dcp_restaurantinfo_id && z.photo_type == (int)ManageEnum.resourcetype.restaurant).ToList();
                        _context.Photos.RemoveRange(photos);

                        List<Models.Photos> photoList = ResaurantMigrationModels.Photos.Where(t => t.kb_transaction_id == item.kb_id).ToList();
                        foreach (var Photositem in photoList)
                        {
                            var tempPhotos = (new Entities.Photos
                            {
                                photo_name = Photositem.photo_name,
                                photo_size = Photositem.photo_size,
                                photo_type = Photositem.kb_photo_type,
                                transaction_id = restaurant.dcp_restaurantinfo_id,
                            });
                            _context.Photos.Add(tempPhotos);
                        }

                        var suitableForTypes = _context.SuitableForType.Where(a => a.dcp_transaction_id == restaurant.dcp_restaurantinfo_id && a.dcp_suitablefor_type == (int)ManageEnum.resourcetype.restaurant).ToList();
                        _context.SuitableForType.RemoveRange(suitableForTypes);

                        List<KbSuitable> suitableList = ResaurantMigrationModels.KbSuitable.Where(t => t.kb_transaction_id == item.kb_id).ToList();
                        foreach (var suitableForTypesitem in suitableList)
                        {
                            var tempSuitable = (new Entities.SuitableForType
                            {
                                dcp_suitablefor_type = suitableForTypesitem.kb_tripinfo_type,
                                dcp_suitablefor_id = suitableForTypesitem.smt_suitablefor_id,
                                dcp_transaction_id = restaurant.dcp_restaurantinfo_id
                            });
                            _context.SuitableForType.Add(tempSuitable);
                        }

                        var lstkitchen = _context.KitchenType.Where(a => a.dcp_restaurantinfo_id == restaurant.dcp_restaurantinfo_id).ToList();
                        _context.KitchenType.RemoveRange(lstkitchen);


                        List<kb_kitchen> kitchenList = ResaurantMigrationModels.Kitchens.Where(t => t.kb_restaurantinfo_id == item.kb_id).ToList();
                        foreach (var kitchen in kitchenList)
                        {                           

                            var kitchenType = (new Entities.KitchenType
                            {
                                dcp_restaurantinfo_id = restaurant.dcp_restaurantinfo_id,
                                dcp_kitchentype_id = kitchen.smt_kitchenlist_id,                                
                            });

                            _context.KitchenType.Add(kitchenType);
                        }

                        _context.SaveChanges();

                        responseSyncModel.IsSyncSuccess = true;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "R";
                        responseModel.Add(responseSyncModel);
                    }
                    catch (Exception ex)
                    {
                        responseSyncModel.IsSyncSuccess = false;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "R";
                        responseModel.Add(responseSyncModel);
                    }
                }
            }

            migrationModels = ResaurantMigrationModels.Restaurants.Where(t => (t.closed != null && t.closed == true)).ToList();

            if (migrationModels != null && migrationModels.Count > 0)
            {
                foreach (var item in migrationModels)
                {
                    responseSyncModel = new ResponseSyncModel();
                    try
                    {
                        Restaurants restaurants = _context.Restaurants.Where(t => t.kb_id == item.kb_id).FirstOrDefault();
                        if (restaurants != null)
                        {
                            List<RestaurantType> restaurantType = _context.RestaurantType.Where(t => t.dcp_restaurantinfo_id == restaurants.dcp_restaurantinfo_id).ToList();
                            if (restaurantType.Count > 0)
                            {
                                _context.RestaurantType.RemoveRange(restaurantType);
                                _context.SaveChanges();
                            }

                            List<Entities.Photos> photos = _context.Photos.Where(t => t.photo_type == 1 && t.transaction_id == restaurants.dcp_restaurantinfo_id).ToList();
                            if (photos.Count() > 0)
                            {
                                _context.Photos.RemoveRange(photos);
                                _context.SaveChanges();
                            }

                            List<Entities.SuitableForType> suitableForTypes = _context.SuitableForType.Where(t => t.dcp_suitablefor_type == 1 && t.dcp_transaction_id == restaurants.dcp_restaurantinfo_id).ToList();
                            if (suitableForTypes.Count() > 0)
                            {
                                _context.SuitableForType.RemoveRange(suitableForTypes);
                                _context.SaveChanges();
                            }

                            List<Entities.KitchenType> kitchenTypes = _context.KitchenType.Where(t => t.dcp_restaurantinfo_id == restaurants.dcp_restaurantinfo_id).ToList();
                            if (kitchenTypes.Count() > 0)
                            {
                                _context.KitchenType.RemoveRange(kitchenTypes);
                                _context.SaveChanges();
                            }



                            _context.Restaurants.Remove(restaurants);
                            _context.SaveChanges();
                        }

                        responseSyncModel.IsSyncSuccess = true;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "R";
                        responseModel.Add(responseSyncModel);
                    }
                    catch (Exception)
                    {

                        responseSyncModel.IsSyncSuccess = false;
                        responseSyncModel.ResourceId = item.kb_id;
                        responseSyncModel.ResourceType = "R";
                        responseModel.Add(responseSyncModel);
                    }
                }
            }

                return responseModel;
        }

        public bool MigrateAirport(List<AirportModel> sourceTable)
        {
            try
            {
                List<List_Airport> lstAirport = _context.List_Airport.ToList();

                List<AirportModel> DestinationTable = lstAirport.Select(t => new AirportModel
                {
                    airport_id = t.Id,
                    airport_name = t.Airport_name,
                    country_id = t.country_id,
                    state = t.state,
                    city = t.city,
                    zipcode = t.zipcode,
                    airport_lan = t.airport_lan,
                    airport_lat = t.airport_lat
                }).ToList();

                var result = sourceTable.Except(DestinationTable, new CustomAirportDataRowComparer()).ToList();

                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        List_Airport airport = new List_Airport();
                        airport = _context.List_Airport.Where(t => t.Id == item.airport_id).FirstOrDefault();

                        if (airport == null)
                        {
                            airport = new List_Airport();
                        }

                        airport.Airport_name = item.airport_name;
                        airport.country_id = item.country_id;
                        airport.state = item.state;
                        airport.city = item.city;
                        airport.zipcode = item.zipcode;
                        airport.airport_lan = item.airport_lan;
                        airport.airport_lat = item.airport_lat;

                        if (airport.Id >0)
                        {
                            _context.Entry(airport).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {
                            airport.Id = item.airport_id;
                            _context.List_Airport.Add(airport);
                            _context.Entry(airport).State = System.Data.Entity.EntityState.Added;
                        }
                        _context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool MigrateCountry(List<CountryModel> sourceTable)
        {
            try
            {
                List<List_Country> lstCountry = _context.List_Country.ToList();

                List<CountryModel> DestinationTable = lstCountry.Select(t => new CountryModel
                {
                    id = t.Id,
                    country_name = t.country_name,
                    country_name_de = t.country_name_de,
                    country_code = t.country_code                   
                }).ToList();

                var result = sourceTable.Except(DestinationTable, new CustomCountryDataRowComparer()).ToList();

                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        List_Country country = new List_Country();
                        country = _context.List_Country.Where(t => t.Id == item.id).FirstOrDefault();

                        if(country==null)
                        {
                            country = new List_Country();
                        }


                        country.country_name = item.country_name;
                        country.country_name_de = item.country_name_de;
                        country.country_code = item.country_code;
                       

                        if (country.Id > 0)
                        {
                            _context.Entry(country).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {
                             country.Id = item.id;
                            _context.List_Country.Add(country);
                            _context.Entry(country).State = System.Data.Entity.EntityState.Added;
                        }
                        _context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool MigrateHotelType(List<HotelTypeModel> sourceTable)
        {
            try
            {
                List<List_HotelType> lstHotelType = _context.List_HotelType.ToList();

                List<HotelTypeModel> DestinationTable = lstHotelType.Select(t => new HotelTypeModel
                {
                    id = t.Id,
                    hoteltype_name = t.hoteltype_name,
                    hoteltype_name_de = t.hoteltype_name_de,
                   
                }).ToList();

                var result = sourceTable.Except(DestinationTable, new CustomHotelTypeDataRowComparer()).ToList();

                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        List_HotelType hotelType = new List_HotelType();
                        hotelType = _context.List_HotelType.Where(t => t.Id == item.id).FirstOrDefault();

                        if (hotelType == null)
                        {
                            hotelType = new List_HotelType();
                        }


                        hotelType.hoteltype_name = item.hoteltype_name;
                        hotelType.hoteltype_name_de = item.hoteltype_name_de;
                        


                        if (hotelType.Id > 0)
                        {
                            _context.Entry(hotelType).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {
                            hotelType.Id = item.id;
                            _context.List_HotelType.Add(hotelType);
                            _context.Entry(hotelType).State = System.Data.Entity.EntityState.Added;
                        }
                        _context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool MigrateRestaurantType(List<RestaurantTypeModel> sourceTable)
        {
            try
            {
                List<List_RestaurantType> lstRestaurantType = _context.List_RestaurantType.ToList();

                List<RestaurantTypeModel> DestinationTable = lstRestaurantType.Select(t => new RestaurantTypeModel
                {
                    id = t.Id,
                    restaurant_name = t.restaurant_name,
                    restaurant_name_de = t.restaurant_name_de,

                }).ToList();

                var result = sourceTable.Except(DestinationTable, new CustomRestaurantTypeDataRowComparer()).ToList();

                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        List_RestaurantType restaurantType = new List_RestaurantType();
                        restaurantType = _context.List_RestaurantType.Where(t => t.Id == item.id).FirstOrDefault();

                        if (restaurantType == null)
                        {
                            restaurantType = new List_RestaurantType();
                        }


                        restaurantType.restaurant_name = item.restaurant_name;
                        restaurantType.restaurant_name_de = item.restaurant_name_de;



                        if (restaurantType.Id > 0)
                        {
                            _context.Entry(restaurantType).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {
                            restaurantType.Id = item.id;
                            _context.List_RestaurantType.Add(restaurantType);
                            _context.Entry(restaurantType).State = System.Data.Entity.EntityState.Added;
                        }
                        _context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool MigrateLocationType(List<LocationTypeModel> sourceTable)
        {
            try
            {
                List<List_LocationType> lstlocationType = _context.List_LocationType.ToList();

                List<LocationTypeModel> DestinationTable = lstlocationType.Select(t => new LocationTypeModel
                {
                    id = t.Id,
                    location_name = t.location_name,
                    location_name_de = t.location_name_de,

                }).ToList();

                var result = sourceTable.Except(DestinationTable, new CustomLocationTypeDataRowComparer()).ToList();

                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        List_LocationType locationType = new List_LocationType();
                        locationType = _context.List_LocationType.Where(t => t.Id == item.id).FirstOrDefault();

                        if (locationType == null)
                        {
                            locationType = new List_LocationType();
                        }


                        locationType.location_name = item.location_name;
                        locationType.location_name_de = item.location_name_de;



                        if (locationType.Id > 0)
                        {
                            _context.Entry(locationType).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {

                            locationType.Id = item.id;
                            _context.List_LocationType.Add(locationType);
                            _context.Entry(locationType).State = System.Data.Entity.EntityState.Added;
                        }
                        _context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool MigrateKitchen(List<KitchenModel> sourceTable)
        {
            try
            {
                List<List_Kitchen> lstKitchen = _context.List_Kitchen.ToList();

                List<KitchenModel> DestinationTable = lstKitchen.Select(t => new KitchenModel
                {
                    id = t.Id,
                    kitchen_name = t.kitchen_name,
                    kitchen_name_de = t.kitchen_name_de,

                }).ToList();

                var result = sourceTable.Except(DestinationTable, new CustomKitchenDataRowComparer()).ToList();

                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        List_Kitchen kitchen = new List_Kitchen();
                        kitchen = _context.List_Kitchen.Where(t => t.Id == item.id).FirstOrDefault();

                        if (kitchen == null)
                        {
                            kitchen = new List_Kitchen();
                        }


                        kitchen.kitchen_name = item.kitchen_name;
                        kitchen.kitchen_name_de = item.kitchen_name_de;



                        if (kitchen.Id > 0)
                        {

                            _context.Entry(kitchen).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {

                            kitchen.Id = item.id;
                            _context.List_Kitchen.Add(kitchen);
                            _context.Entry(kitchen).State = System.Data.Entity.EntityState.Added;
                        }
                        _context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool MigrateSuitableFor(List<SuitableForModel> sourceTable)
        {
            try
            {
                List<List_SuitableFor> lstSuitableFor = _context.List_SuitableFor.ToList();

                List<SuitableForModel> DestinationTable = lstSuitableFor.Select(t => new SuitableForModel
                {
                    id = t.Id,
                    suitablefor_name = t.suitablefor_name,
                    suitablefor_name_de = t.suitablefor_name_de,
                    suitablefor_type = t.suitablefor_type

                }).ToList();

                var result = sourceTable.Except(DestinationTable, new CustomSuitableForDataRowComparer()).ToList();

                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        List_SuitableFor suitableFor = new List_SuitableFor();
                        suitableFor = _context.List_SuitableFor.Where(t => t.Id == item.id).FirstOrDefault();

                        if (suitableFor == null)
                        {
                            suitableFor = new List_SuitableFor();
                        }


                        suitableFor.suitablefor_name = item.suitablefor_name;
                        suitableFor.suitablefor_name_de = item.suitablefor_name_de;
                        suitableFor.suitablefor_type = item.suitablefor_type;


                        if (suitableFor.Id > 0)
                        {

                            _context.Entry(suitableFor).State = System.Data.Entity.EntityState.Modified;
                        }
                        else
                        {

                            suitableFor.Id = item.id;
                            _context.List_SuitableFor.Add(suitableFor);
                            _context.Entry(suitableFor).State = System.Data.Entity.EntityState.Added;
                        }
                        _context.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteResource(List<KbStatusModel> statusModels)
        {
            try
            {
                foreach (var item in statusModels)
                {
                    if(item.kb_type==0)
                    {
                        Hotels hotels = _context.Hotels.Where(t => t.kb_id == item.kb_id).FirstOrDefault();
                        if(hotels!=null)
                        {
                           List<HotelType> hotelTypes =  _context.HotelType.Where(t => t.dcp_hotelinfo_id == hotels.dcp_hotelinfo_id).ToList();
                            if(hotelTypes.Count>0)
                            {
                                _context.HotelType.RemoveRange(hotelTypes);
                                _context.SaveChanges();
                            }

                            List<Entities.Photos> photos = _context.Photos.Where(t => t.photo_type == 0 && t.transaction_id == hotels.dcp_hotelinfo_id).ToList();
                            if (photos.Count()>0)
                            {
                                _context.Photos.RemoveRange(photos);
                                _context.SaveChanges();
                            }

                            _context.Hotels.Remove(hotels);
                            _context.SaveChanges();

                            
                        }
                    }

                    if(item.kb_type==1)
                    {
                        Restaurants restaurants = _context.Restaurants.Where(t => t.kb_id == item.kb_id).FirstOrDefault();
                        if (restaurants != null)
                        {
                            List<RestaurantType> restaurantType = _context.RestaurantType.Where(t => t.dcp_restaurantinfo_id == restaurants.dcp_restaurantinfo_id).ToList();
                            if (restaurantType.Count > 0)
                            {
                                _context.RestaurantType.RemoveRange(restaurantType);
                                _context.SaveChanges();
                            }

                            List<Entities.Photos> photos = _context.Photos.Where(t => t.photo_type == 1 && t.transaction_id == restaurants.dcp_restaurantinfo_id).ToList();
                            if (photos.Count() > 0)
                            {
                                _context.Photos.RemoveRange(photos);
                                _context.SaveChanges();
                            }

                            List<Entities.SuitableForType> suitableForTypes = _context.SuitableForType.Where(t => t.dcp_suitablefor_type == 1 && t.dcp_transaction_id == restaurants.dcp_restaurantinfo_id).ToList();
                            if (suitableForTypes.Count() > 0)
                            {
                                _context.SuitableForType.RemoveRange(suitableForTypes);
                                _context.SaveChanges();
                            }

                            List<Entities.KitchenType> kitchenTypes = _context.KitchenType.Where(t => t.dcp_restaurantinfo_id == restaurants.dcp_restaurantinfo_id).ToList();
                            if (kitchenTypes.Count() > 0)
                            {
                                _context.KitchenType.RemoveRange(kitchenTypes);
                                _context.SaveChanges();
                            }



                            _context.Restaurants.Remove(restaurants);
                            _context.SaveChanges();
                        }
                    }

                    if (item.kb_type == 2)
                    {
                        Locations locations = _context.Locations.Where(t => t.kb_id == item.kb_id).FirstOrDefault();
                        if (locations != null)
                        {
                            List<Entities.LocationType> locationTypes = _context.LocationType.Where(t => t.dcp_locationinfo_id == locations.dcp_locationinfo_id).ToList();
                            if (locationTypes.Count > 0)
                            {
                                _context.LocationType.RemoveRange(locationTypes);
                                _context.SaveChanges();
                            }

                            List<Entities.Photos> photos = _context.Photos.Where(t => t.photo_type == 2 && t.transaction_id == locations.dcp_locationinfo_id).ToList();
                            if (photos.Count() > 0)
                            {
                                _context.Photos.RemoveRange(photos);
                                _context.SaveChanges();
                            }

                            List<Entities.SuitableForType> suitableForTypes = _context.SuitableForType.Where(t => t.dcp_suitablefor_type == 2 && t.dcp_transaction_id == locations.dcp_locationinfo_id).ToList();
                            if (suitableForTypes.Count() > 0)
                            {
                                _context.SuitableForType.RemoveRange(suitableForTypes);
                                _context.SaveChanges();
                            }


                            _context.Locations.Remove(locations);
                            _context.SaveChanges();
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }

    public class CustomAirportDataRowComparer : IEqualityComparer<AirportModel>
    {
        public bool Equals(AirportModel x, AirportModel y)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.airport_id == y.airport_id && x.airport_name == y.airport_name && x.country_id == y.country_id && x.state == y.state && x.city == y.city && x.zipcode == y.zipcode && x.airport_lan == y.airport_lan && x.airport_lat == y.airport_lat;
        }

        public int GetHashCode(AirportModel obj)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Get hash code for the Code field.
            int hashAirportId = obj.airport_id.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashAirportName = obj.airport_name.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashCountryid = obj.country_id == null ? 0 : obj.country_id.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashState = obj.state == null ? 0 : obj.state.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashCity = obj.city == null ? 0 : obj.city.GetHashCode();

            int hashZipcode = obj.zipcode == null ? 0 : obj.zipcode.GetHashCode();

            int hashairport_lan = obj.airport_lan == null ? 0 : obj.airport_lan.GetHashCode();

            int hashairport_lat = obj.airport_lat == null ? 0 : obj.airport_lat.GetHashCode();

            //Calculate the hash code for the product.
            return hashAirportId ^ hashAirportName ^ hashCountryid ^ hashState ^ hashCity ^ hashZipcode ^ hashairport_lan ^ hashairport_lat;
        }
    }

    public class CustomCountryDataRowComparer : IEqualityComparer<CountryModel>
    {
        public bool Equals(CountryModel x, CountryModel y)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.id == y.id && x.country_name == y.country_name && x.country_name_de == y.country_name_de && x.country_code == y.country_code;
        }

        public int GetHashCode(CountryModel obj)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Get hash code for the Code field.
            int hashId = obj.id.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashCountryName = obj.country_name.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashCountryNamede = obj.country_name_de.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashCountryCode = obj.country_code == null ? 0 : obj.country_code.GetHashCode();

           

            //Calculate the hash code for the product.
            return hashId ^ hashCountryName ^ hashCountryNamede ^ hashCountryCode;
        }
    }

    public class CustomHotelTypeDataRowComparer : IEqualityComparer<HotelTypeModel>
    {
        public bool Equals(HotelTypeModel x, HotelTypeModel y)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.id == y.id && x.hoteltype_name == y.hoteltype_name && x.hoteltype_name_de == y.hoteltype_name_de;
        }

        public int GetHashCode(HotelTypeModel obj)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Get hash code for the Code field.
            int hashId = obj.id.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashHotelTypeName = obj.hoteltype_name.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashHotelTypeNamede = obj.hoteltype_name_de.GetHashCode();

            //Calculate the hash code for the product.
            return hashId ^ hashHotelTypeName ^ hashHotelTypeNamede;
        }
    }

    public class CustomRestaurantTypeDataRowComparer : IEqualityComparer<RestaurantTypeModel>
    {
        public bool Equals(RestaurantTypeModel x, RestaurantTypeModel y)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.id == y.id && x.restaurant_name == y.restaurant_name && x.restaurant_name_de == y.restaurant_name_de;
        }

        public int GetHashCode(RestaurantTypeModel obj)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Get hash code for the Code field.
            int hashId = obj.id.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashRestaurantName = obj.restaurant_name.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashRestaurantNamede = obj.restaurant_name_de.GetHashCode();

            //Calculate the hash code for the product.
            return hashId ^ hashRestaurantName ^ hashRestaurantNamede;
        }
    }

    public class CustomLocationTypeDataRowComparer : IEqualityComparer<LocationTypeModel>
    {
        public bool Equals(LocationTypeModel x, LocationTypeModel y)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.id == y.id && x.location_name == y.location_name && x.location_name_de == y.location_name_de;
        }

        public int GetHashCode(LocationTypeModel obj)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Get hash code for the Code field.
            int hashId = obj.id.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashLocationName = obj.location_name.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashLocationNamede = obj.location_name_de.GetHashCode();

            //Calculate the hash code for the product.
            return hashId ^ hashLocationName ^ hashLocationNamede;
        }
    }

    public class CustomKitchenDataRowComparer : IEqualityComparer<KitchenModel>
    {
        public bool Equals(KitchenModel x, KitchenModel y)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.id == y.id && x.kitchen_name == y.kitchen_name && x.kitchen_name_de == y.kitchen_name_de;
        }

        public int GetHashCode(KitchenModel obj)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Get hash code for the Code field.
            int hashId = obj.id.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashKitchenName = obj.kitchen_name.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashKitchenNamede = obj.kitchen_name_de.GetHashCode();

            //Calculate the hash code for the product.
            return hashId ^ hashKitchenName ^ hashKitchenNamede;
        }
    }

    public class CustomSuitableForDataRowComparer : IEqualityComparer<SuitableForModel>
    {
        public bool Equals(SuitableForModel x, SuitableForModel y)
        {
            //Check whether the compared objects reference the same data.
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.id == y.id && x.suitablefor_name == y.suitablefor_name && x.suitablefor_name_de == y.suitablefor_name_de && x.suitablefor_type==y.suitablefor_type;
        }

        public int GetHashCode(SuitableForModel obj)
        {
            //Check whether the object is null
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Get hash code for the Code field.
            int hashId = obj.id.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashSuitableForName = obj.suitablefor_name.GetHashCode();

            //Get hash code for the Name field if it is not null.
            int hashSuitableForNamede = obj.suitablefor_name_de.GetHashCode();

            int hashSuitableForType = obj.suitablefor_type.GetHashCode();

            //Calculate the hash code for the product.
            return hashId ^ hashSuitableForName ^ hashSuitableForNamede ^ hashSuitableForType;
        }
    }

}
