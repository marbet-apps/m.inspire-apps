﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Utilities
{
    public class ManageSetting
    {
        public static readonly string MiceAgencyName = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["MiceAgencyName"]) ? ConfigurationManager.AppSettings["MiceAgencyName"] : "";
        public static readonly string MiceAgencyPassword = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["MiceAgencyPassword"]) ? ConfigurationManager.AppSettings["MiceAgencyPassword"] : "";

        public static string FormatDateValue(DateTime dateValue)
        {
            return dateValue.ToString("dd.MM.yyyy");
        }
    }
}
