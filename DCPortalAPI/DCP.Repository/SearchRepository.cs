﻿using DCP.Entities;
using DCP.Models;
using DCP.Models.RFP;
using DCP.Repository.Interface;
using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using static DCP.Utilities.ManageEnum;

namespace DCP.Repository
{
    public class SearchRepository : ISearchRepository
    {
        private DCPDbContext _context = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public SearchRepository()
        {
            this._context = new DCPDbContext();
        }

        /// <summary>
        /// Get Resources by applied filters
        /// </summary>
        /// <returns></returns>
        public BaseResourceModel GetResourcesList(FilterModel SearchModel, string lang,int? UserId)
        {
            BaseResourceModel ResponceData = new BaseResourceModel();

            IEnumerable<ResourceModel> Hotel = HotelFilters(SearchModel, lang,UserId);
            IEnumerable<ResourceModel> Restaurant = RestaurantFilters(SearchModel, lang,UserId);
            IEnumerable<ResourceModel> Location = LocationFilters(SearchModel, lang,UserId);

            if(UserId != null && UserId.HasValue)
            {
                Hotel = FilterUserCart(Hotel.ToList(), UserId.Value);
                Restaurant = FilterUserCart(Restaurant.ToList(), UserId.Value);
                Location = FilterUserCart(Location.ToList(), UserId.Value);
            }
            IEnumerable<ResourceModel> Data = Hotel.Union(Restaurant).Union(Location);

            var location = SearchModel.name;
            var filterByDistance = false;
            if (location != null && location != string.Empty && location.Length > 0 && location != ",")
            {
                double distance = SearchModel.distance.Value;

                Data = Data.Where(x => x.lat != null && x.lng != null && x.lat != 0 && x.lng != 0);
                Data.All(x =>
                {
                    // x.distance = _context.Database.SqlQuery<float>("DistanceKM @Lat1={0},@Lat2={1},@Long1={2},@Long2={3}", SearchModel.lat.Value, x.lat == null ? 0 : (float)x.lat.Value, SearchModel.lon.Value, x.lng == null ? 0 : (float)x.lng.Value).First();
                    x.diffDistance = DistanceKM(SearchModel.lat.Value, SearchModel.lon.Value, x.lat == null ? 0 : (float)x.lat.Value, x.lng == null ? 0 : (float)x.lng.Value);
                    return true;
                });
                Data = Data.Where(x => x.diffDistance <= distance);
                filterByDistance = true;
            }

            ResponceData.totalrecord = Data.Count();

            #region Sorting

            string sort = SearchModel.sort;

            if (filterByDistance)
            {
                Data = Data.OrderBy(x => x.diffDistance);
            } 
            else
            {
                if (sort == "resourceasc")
                {
                    Data = Data.OrderBy(x => x.name);
                }
                else if (sort == "resourcedesc")
                {
                    Data = Data.OrderByDescending(x => x.name);
                }
                else if (sort == "roomasc")
                {
                    Data = Data.OrderBy(x => x.htotalroom);
                }
                else if (sort == "roomdesc")
                {
                    Data = Data.OrderByDescending(x => x.htotalroom);
                }
                else if (sort == "confasc")
                {
                    Data = Data.OrderBy(x => x.htotalcroom).ThenBy(x => x.rtotal_conference);
                }
                else if (sort == "confdesc")
                {
                    Data = Data.OrderByDescending(x => x.htotalcroom).ThenByDescending(x => x.rtotal_conference);
                }
                else if (sort == "capresasc")
                {
                    Data = Data.OrderBy(x => x.rcapacity);
                }
                else if (sort == "capresdesc")
                {
                    Data = Data.OrderByDescending(x => x.rcapacity);
                }
                else if(sort== "restroomasc")
                {
                    Data = Data.OrderBy(x => x.rtotal_conference);
                }
                else if (sort == "restroomdesc")
                {
                    Data = Data.OrderByDescending(x => x.rtotal_conference);
                }
                else if (sort == "largestroomasc")
                {
                    Data = Data.OrderBy(x => x.rcapacity_of_largest_room);
                }
                else if (sort == "largestroomdesc")
                {
                    Data = Data.OrderByDescending(x => x.rcapacity_of_largest_room);
                }
                else if (sort == "capacityofroomasc")
                {
                    Data = Data.OrderBy(x => x.rcapacity);
                }
                else if (sort == "capacityofroomdesc")
                {
                    Data = Data.OrderByDescending(x => x.rcapacity);
                }
                else if(sort== "locconfasc")
                {
                    Data = Data.OrderBy(x => x.ltotal_conference);
                }
                else if (sort == "locconfdesc")
                {
                    Data = Data.OrderByDescending(x => x.ltotal_conference);
                }
                else if (sort == "locsizeofroomasc")
                {
                    Data = Data.OrderBy(x => x.lsize_of_largest_conference);
                }
                else if (sort == "locsizeofroomdesc")
                {
                    Data = Data.OrderByDescending(x => x.lsize_of_largest_conference);
                }
                else if (sort == "rentalasc")
                {
                    Data = Data.OrderBy(x => x.lrent_begining);
                }
                else if (sort == "rentaldesc")
                {
                    Data = Data.OrderByDescending(x => x.lrent_begining);
                }
                else if(sort == "starasc")
                {
                    Data = Data.OrderBy(x => x.stars);
                }
                else if (sort == "stardesc")
                {
                    Data = Data.OrderByDescending(x => x.stars);
                }
            }


            #endregion

            ResponceData.ResourceModel = Data.Skip(SearchModel.displayRecords).Take(SearchModel.pageSize).ToList();

            return ResponceData;
        }

        public void SaveSearchHistory(string searchCriteria, int userId)
        {
            //var jsSerializer = new JavaScriptSerializer();
            var searchHistory = new SearchHistory();
            searchHistory.search_criteria = searchCriteria;
            searchHistory.created_by = userId;
            searchHistory.created_date = DateTime.Now;

            _context.SearchHistory.Add(searchHistory);
            _context.SaveChanges();
        }

        public string GetSearchCriteria(int userId)
        {
            var searchHistory = _context.SearchHistory.Where(u => u.created_by == userId)
                .OrderByDescending(u => u.created_date).Select(s=> s.search_criteria).FirstOrDefault();
            return (searchHistory != null && searchHistory != "") ? searchHistory : "";

        }

        /// <summary>
        /// Get Hotel Data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ResourceModel> HotelFilters(FilterModel filterModel, string lang,int? UserId)
        {
            if (!filterModel.ishotel)
            {
                IEnumerable<ResourceModel> resourceModels = new List<ResourceModel>();

                return resourceModels;
            }

            WriteLog.LogInformation(LogMessages.HotelFilterStart);

            string hoteltype = filterModel.HFilterModel.typeids;
            int[] hotel_type = null;

            if (hoteltype != null && hoteltype != string.Empty && hoteltype.Length > 0)
            {
                int[] HotelTypeList = hoteltype.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                hotel_type = (_context.HotelType.Where(x => HotelTypeList.Contains(x.dcp_hoteltype_id)).Select(t => t.dcp_hotelinfo_id)).ToArray();
            }

            var query = (from h in _context.Hotels
                         join c in _context.List_Country on h.country_id equals c.Id into temp
                         from tmp in temp.DefaultIfEmpty()
                             //where h.country_id == filterModel.country
                         select new ResourceModel()
                         {
                             id = h.dcp_hotelinfo_id,
                             stars = h.star == null ? 0 : h.star,
                             name = h.hotel_name,
                             chainname = h.hotel_chain,
                             country = lang == "en-US" ? tmp.country_name : tmp.country_name_de,
                             city = h.city,
                             zipcode = h.zipcode,
                             htotalroom = h.total_rooms == null ? 0 : h.total_rooms,
                             htotalcroom = h.total_conference == null ? 0 : h.total_conference,
                             hsizecroom = h.size_of_largest_conference == null ? 0 : h.size_of_largest_conference,
                             hparking_spaces = h.parking_spaces == null ? 0 : h.parking_spaces,
                             lng = h.hotel_lan,
                             lat = h.hotel_lat,
                             time = h.airport_transfer_time,
                             is360degree = h.is360degreeview,
                             countryid = h.country_id,
                             isincart = false,
                             //cartId = (rcr.Id == null && rcr.user_id != UserId ? 0 : rcr.Id),
                             lsthoteltype = _context.List_HotelType.Join(_context.HotelType, l => l.Id, d => d.dcp_hoteltype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_hotelinfo_id == h.dcp_hotelinfo_id).Select(t => lang == "en-US" ? t.List.hoteltype_name : t.List.hoteltype_name_de),
                         }).AsEnumerable().Select(h => new ResourceModel()
                         {
                             id = h.id,
                             stars = h.stars,
                             name = h.name,
                             chainname = h.chainname,
                             country = h.country,
                             countryid = h.countryid,
                             city = h.city,
                             zipcode = h.zipcode,
                             htotalroom = h.htotalroom,
                             htotalcroom = h.htotalcroom,
                             hsizecroom = h.hsizecroom,
                             hparking_spaces = h.hparking_spaces,
                             lng = h.lng,
                             lat = h.lat,
                             time = h.time,
                             type = "H",
                             is360degree = h.is360degree,
                             isincart = h.isincart,
                             //cartId = h.cartId,
                             hoteltype = h.lsthoteltype == null ? "" : String.Join(", ", (h.lsthoteltype).ToArray())
                         });

            #region Filters

            if (filterModel.country != null && filterModel.country != string.Empty && filterModel.country.Length > 0)
            {
                int[] CountryIds = filterModel.country.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                query = query.Where(x => CountryIds.Contains(x.countryid));

            }

            if (hoteltype != null && hoteltype != string.Empty && hoteltype.Length > 0)
            {
                if (hotel_type != null && hotel_type.Length > 0)
                {
                    query = query.Where(x => hotel_type.Contains(x.id));
                }
                else
                {
                    query = query.Where(x => 1 == 2);
                }
            }

            string chain = filterModel.HFilterModel.hotelchains;

            if (chain != null && chain != string.Empty && chain.Length > 0)
            {
                string[] chainList = chain.Split(',').ToArray();
                query = query.Where(x => x.chainname != null && x.chainname != "");
                query = query.Where(x => chainList.Contains(x.chainname.ToLower()));
            }

            string city = filterModel.city;

            if (city != null && city != string.Empty && city.Length > 0)
            {
                query = query.Where(x => x.city.ToUpper() == (city.ToUpper()));
            }

            string name = filterModel.HFilterModel.hotelname;

            if (name != null && name != string.Empty && name.Length > 0)
            {
                query = query.Where(x => x.name.ToUpper().Contains(name.ToUpper()));
            }

            string star = filterModel.HFilterModel.stars;

            if (star != null && star != string.Empty && star.Length > 0)
            {

                int[] stars = star.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                query = query.Where(x => stars.Contains(x.stars.Value));
            }

            string totalroom = filterModel.HFilterModel.htotalroom;

            if (totalroom != null && totalroom != string.Empty && totalroom.Length > 0)
            {

                int[] totalrooms = totalroom.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                int item1 = totalrooms[0];
                int item2 = totalrooms[1];

                query = query.Where(x => x.htotalroom >= item1 && x.htotalroom <= item2);
            }

            string totalcroom = filterModel.HFilterModel.htotalcroom;

            if (totalcroom != null && totalcroom != string.Empty && totalcroom.Length > 0)
            {

                int[] totalcrooms = totalcroom.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                int item1 = totalcrooms[0];
                int item2 = totalcrooms[1];

                query = query.Where(x => x.htotalcroom >= item1 && x.htotalcroom <= item2);
            }

            string sizecroom = filterModel.HFilterModel.hsizecroom;

            if (sizecroom != null && sizecroom != string.Empty && sizecroom.Length > 0)
            {

                double[] sizecrooms = sizecroom.Split(',').Select(n => double.Parse(n.Trim())).ToArray();

                double item1 = sizecrooms[0];
                double item2 = sizecrooms[1];

                query = query.Where(x => x.hsizecroom >= item1 && x.hsizecroom <= item2);
            }

            string parkingspace = filterModel.HFilterModel.hparkingspace;

            if (parkingspace != null && parkingspace != string.Empty && parkingspace.Length > 0)
            {

                double[] parkingspaces = parkingspace.Split(',').Select(n => double.Parse(n.Trim())).ToArray();

                double item1 = parkingspaces[0];
                double item2 = parkingspaces[1];

                query = query.Where(x => x.hparking_spaces >= item1 && x.hparking_spaces <= item2);
            }

            string time = filterModel.HFilterModel.airporthours + "." + filterModel.HFilterModel.airportminutes;

            if (time != null && time != string.Empty && time.Length > 0 && time != "." && time != "0.0")
            {
                double times = Convert.ToDouble(time);

                query = query.Where(x => x.time != null && x.time != "0.0" && Convert.ToDouble(x.time) <= times);
            }

            #endregion

            WriteLog.LogInformation(LogMessages.HotelFilterEnd);

            return query;
        }

        /// <summary>
        /// Get Hotel Data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ResourceModel> Hotels(int[] Ids, string lang)
        {
            if (Ids.Length <= 0)
            {
                IEnumerable<ResourceModel> resourceModels = new List<ResourceModel>();

                return resourceModels;
            }

            var query = (from h in _context.Hotels
                         join c in _context.List_Country on h.country_id equals c.Id into temp
                         from tmp in temp.DefaultIfEmpty()
                        
                         where Ids.Contains(h.dcp_hotelinfo_id)
                         orderby h.hotel_name ascending
                         select new ResourceModel()
                         {
                             id = h.dcp_hotelinfo_id,
                             stars = h.star == null ? 0 : h.star,
                             name = h.hotel_name,
                             chainname = h.hotel_chain,
                             country = lang == "en-US" ? tmp.country_name : tmp.country_name_de,
                             city = h.city,
                             zipcode = h.zipcode,
                             htotalroom = h.total_rooms == null ? 0 : h.total_rooms,
                             htotalcroom = h.total_conference == null ? 0 : h.total_conference,
                             hsizecroom = h.size_of_largest_conference == null ? 0 : h.size_of_largest_conference,
                             hparking_spaces = h.parking_spaces == null ? 0 : h.parking_spaces,
                             lng = h.hotel_lan,
                             lat = h.hotel_lat,
                             time = h.airport_transfer_time,
                             is360degree = h.is360degreeview,
                             countryid = h.country_id,
                             
                             lsthoteltype = _context.List_HotelType.Join(_context.HotelType, l => l.Id, d => d.dcp_hoteltype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_hotelinfo_id == h.dcp_hotelinfo_id).Select(t => lang == "en-US" ? t.List.hoteltype_name : t.List.hoteltype_name_de),
                         }).AsEnumerable().Select(h => new ResourceModel()
                         {
                             id = h.id,
                             stars = h.stars,
                             name = h.name,
                             chainname = h.chainname,
                             country = h.country,
                             countryid = h.countryid,
                             city = h.city,
                             zipcode = h.zipcode,
                             htotalroom = h.htotalroom,
                             htotalcroom = h.htotalcroom,
                             hsizecroom = h.hsizecroom,
                             hparking_spaces = h.hparking_spaces,
                             lng = h.lng,
                             lat = h.lat,
                             time = h.time,
                             type = "H",
                            
                             is360degree = h.is360degree,
                             hoteltype = h.lsthoteltype == null ? "" : String.Join(", ", (h.lsthoteltype).ToArray())
                         });

            return query;
        }

        /// <summary>
        /// Get Restaurant Data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ResourceModel> RestaurantFilters(FilterModel filterModel, string lang,int? UserId)
        {

            WriteLog.LogInformation(LogMessages.RestaurantFilterStart);

            if (!filterModel.isrestaurant)
            {
                IEnumerable<ResourceModel> resourceModels = new List<ResourceModel>();

                return resourceModels;
            }

            string kitchen = filterModel.RFilterModel.kids;
            int[] restkitchen = null;

            if (kitchen != null && kitchen != string.Empty && kitchen.Length > 0)
            {
                int[] kitchenList = kitchen.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                restkitchen = (_context.KitchenType.Where(x => kitchenList.Contains(x.dcp_kitchentype_id)).Select(t => t.dcp_restaurantinfo_id)).ToArray();
            }

            string restauranttype = filterModel.RFilterModel.typeids;
            int[] restaurant_type = null;

            if (restauranttype != null && restauranttype != string.Empty && restauranttype.Length > 0)
            {
                int[] RestaurantTypeList = restauranttype.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                restaurant_type = (_context.RestaurantType.Where(x => RestaurantTypeList.Contains(x.dcp_restauranttype_id)).Select(t => t.dcp_restaurantinfo_id)).ToArray();
            }

            string SuitableFor = filterModel.RFilterModel.suitableids;
            int[] Suitable_Ids = null;

            if (SuitableFor != null && SuitableFor != string.Empty && SuitableFor.Length > 0)
            {
                int[] SuitableList = SuitableFor.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                Suitable_Ids = (_context.SuitableForType.Where(x => SuitableList.Contains(x.dcp_suitablefor_id) && x.dcp_suitablefor_type == (int)resourcetype.restaurant).Select(t => t.dcp_transaction_id)).ToArray();
            }

            var query = (from h in _context.Restaurants
                         join c in _context.List_Country on h.country_id equals c.Id into temp
                         from tmp in temp.DefaultIfEmpty()
                        
                             // where h.country_id == filterModel.country

                         select new ResourceModel()
                         {
                             id = h.dcp_restaurantinfo_id,
                             name = h.restaurant_name,
                             country = lang == "en-US" ? tmp.country_name : tmp.country_name_de,
                             countryid = h.country_id,
                             city = h.city,
                             zipcode = h.zipcode,
                             rcapacity = h.capacity == null ? 0 : h.capacity,
                             rtotal_conference = h.total_conference == null ? 0 : h.total_conference,
                             rcapacity_of_largest_room = h.capacity_of_largest_room == null ? 0 : h.capacity_of_largest_room,
                             rsize_of_largest_conference = h.size_of_largest_conference == null ? 0 : h.size_of_largest_conference,
                             rminimum_turnover = h.minimum_turnover == null ? 0 : h.minimum_turnover,
                             chkOP = h.outdoor_possibility,
                             chkRIH = h.restaurant_in_hotel,
                             lng = h.restaurant_lan,
                             lat = h.restaurant_lat,
                             is360degree = h.is360degreeview,
                             isincart = false,
                             //cartId = (rcr.Id == null && rcr.user_id != UserId ? 0 : rcr.Id),
                             lstkitchen = _context.List_Kitchen.Join(_context.KitchenType, l => l.Id, d => d.dcp_kitchentype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_restaurantinfo_id == h.dcp_restaurantinfo_id).Select(t => lang == "en-US" ? t.List.kitchen_name : t.List.kitchen_name_de),
                             lstrestauranttype = _context.List_RestaurantType.Join(_context.RestaurantType, l => l.Id, d => d.dcp_restauranttype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_restaurantinfo_id == h.dcp_restaurantinfo_id).Select(t => lang == "en-US" ? t.List.restaurant_name : t.List.restaurant_name_de),
                             lstsuitablefor = _context.List_SuitableFor.Join(_context.SuitableForType, l => l.Id, d => d.dcp_suitablefor_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_transaction_id == h.dcp_restaurantinfo_id && ld.Detail.dcp_suitablefor_type == (int)resourcetype.restaurant).Select(t => lang == "en-US" ? t.List.suitablefor_name : t.List.suitablefor_name_de),
                         }).AsEnumerable().Select(d => new ResourceModel()
                         {
                             id = d.id,
                             name = d.name,
                             country = d.country,
                             countryid = d.countryid,
                             city = d.city,
                             zipcode = d.zipcode,
                             rcapacity = d.rcapacity,
                             rtotal_conference = d.rtotal_conference,
                             rcapacity_of_largest_room = d.rcapacity_of_largest_room,
                             rsize_of_largest_conference = d.rsize_of_largest_conference,
                             rminimum_turnover = d.rminimum_turnover,
                             chkOP = d.chkOP,
                             chkRIH = d.chkRIH,
                             lng = d.lng,
                             lat = d.lat,
                             is360degree = d.is360degree,
                             type = "R",
                             isincart = d.isincart,
                             //cartId = d.cartId,
                             kitchen = d.lstkitchen == null ? "" : String.Join(", ", (d.lstkitchen).ToArray()),
                             restauranttype = d.lstrestauranttype == null ? "" : String.Join(", ", (d.lstrestauranttype).ToArray()),
                             suitablefor = d.lstsuitablefor == null ? "" : String.Join(", ", (d.lstsuitablefor).ToArray()),
                         });

            #region Filters

            if (filterModel.country != null && filterModel.country != string.Empty && filterModel.country.Length > 0)
            {
                int[] CountryIds = filterModel.country.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                query = query.Where(x => CountryIds.Contains(x.countryid));

            }

            if (kitchen != null && kitchen != string.Empty && kitchen.Length > 0)
            {
                if (restkitchen != null && restkitchen.Length > 0)
                {
                    query = query.Where(x => restkitchen.Contains(x.id));
                }
                else
                {
                    query = query.Where(x => 1 == 2);
                }
            }

            if (restauranttype != null && restauranttype != string.Empty && restauranttype.Length > 0)
            {
                if (restaurant_type != null && restaurant_type.Length > 0)
                {
                    query = query.Where(x => restaurant_type.Contains(x.id));
                }
                else
                {
                    query = query.Where(x => 1 == 2);
                }
            }

            if (SuitableFor != null && SuitableFor != string.Empty && SuitableFor.Length > 0)
            {
                if (Suitable_Ids != null && Suitable_Ids.Length > 0)
                {
                    query = query.Where(x => Suitable_Ids.Contains(x.id));
                }
                else
                {
                    query = query.Where(x => 1 == 2);
                }
            }

            string city = filterModel.city;

            if (city != null && city != string.Empty && city.Length > 0)
            {
                query = query.Where(x => x.city.ToUpper() == (city.ToUpper()));
            }

            string name = filterModel.RFilterModel.restaurantname;

            if (name != null && name != string.Empty && name.Length > 0)
            {
                query = query.Where(x => x.name.ToUpper().Contains(name.ToUpper()));
            }

            string totalroom = filterModel.RFilterModel.rtotalroom;

            if (totalroom != null && totalroom != string.Empty && totalroom.Length > 0)
            {

                int[] totalrooms = totalroom.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                int item1 = totalrooms[0];
                int item2 = totalrooms[1];

                query = query.Where(x => x.rtotal_conference >= item1 && x.rtotal_conference <= item2);
            }

            string Rsizecroom = filterModel.RFilterModel.rsizecroom;

            if (Rsizecroom != null && Rsizecroom != string.Empty && Rsizecroom.Length > 0)
            {

                int[] Rsizecrooms = Rsizecroom.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                int item1 = Rsizecrooms[0];
                int item2 = Rsizecrooms[1];

                query = query.Where(x => x.rsize_of_largest_conference >= item1 && x.rsize_of_largest_conference <= item2);
            }

            string roomcapacity = filterModel.RFilterModel.rroomcapacity;

            if (roomcapacity != null && roomcapacity != string.Empty && roomcapacity.Length > 0)
            {

                double[] roomcapacitys = roomcapacity.Split(',').Select(n => double.Parse(n.Trim())).ToArray();

                double item1 = roomcapacitys[0];
                double item2 = roomcapacitys[1];

                query = query.Where(x => x.rcapacity_of_largest_room >= item1 && x.rcapacity_of_largest_room <= item2);
            }

            string restaurantcapacity = filterModel.RFilterModel.rrestaurantcapacity;

            if (restaurantcapacity != null && restaurantcapacity != string.Empty && restaurantcapacity.Length > 0)
            {

                double[] restaurantcapacitys = restaurantcapacity.Split(',').Select(n => double.Parse(n.Trim())).ToArray();

                double item1 = restaurantcapacitys[0];
                double item2 = restaurantcapacitys[1];

                query = query.Where(x => x.rcapacity >= item1 && x.rcapacity <= item2);
            }

            string minimumturnover = filterModel.RFilterModel.rminimumturnover;

            if (minimumturnover != null && minimumturnover != string.Empty && minimumturnover.Length > 0)
            {

                float[] minimumturnovers = minimumturnover.Split(',').Select(n => float.Parse(n.Trim())).ToArray();

                float item1 = minimumturnovers[0];
                float item2 = minimumturnovers[1];

                query = query.Where(x => x.rminimum_turnover >= item1 && x.rminimum_turnover <= item2);
            }

            string chkoutdoor = filterModel.RFilterModel.chkoutdoor;

            if (chkoutdoor != null && chkoutdoor != string.Empty && chkoutdoor.Length > 0)
            {
                bool chkoutdoors = Convert.ToBoolean(filterModel.RFilterModel.chkoutdoor);

                query = query.Where(x => x.chkOP == chkoutdoors);

            }

            string chkrestinhotels = filterModel.RFilterModel.chkrestinhotel;

            if (chkrestinhotels != null && chkrestinhotels != string.Empty && chkrestinhotels.Length > 0)
            {
                bool chkrestinhotel = Convert.ToBoolean(filterModel.RFilterModel.chkrestinhotel);

                query = query.Where(x => x.chkRIH == chkrestinhotel);

            }

            #endregion

            WriteLog.LogInformation(LogMessages.RestaurantFilterEnd);

            return query;
        }

        /// <summary>
        /// Get Restaurant Data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ResourceModel> Restaurants(int[] Ids, string lang)
        {
            if (Ids.Length <= 0)
            {
                IEnumerable<ResourceModel> resourceModels = new List<ResourceModel>();

                return resourceModels;
            }

            var query = (from h in _context.Restaurants
                         join c in _context.List_Country on h.country_id equals c.Id into temp
                         from tmp in temp.DefaultIfEmpty()
                         
                         where Ids.Contains(h.dcp_restaurantinfo_id)
                         orderby h.restaurant_name ascending
                         select new ResourceModel()
                         {
                             id = h.dcp_restaurantinfo_id,
                             name = h.restaurant_name,
                             country = lang == "en-US" ? tmp.country_name : tmp.country_name_de,
                             countryid = h.country_id,
                             city = h.city,
                             zipcode = h.zipcode,
                             rcapacity = h.capacity == null ? 0 : h.capacity,
                             rtotal_conference = h.total_conference == null ? 0 : h.total_conference,
                             rcapacity_of_largest_room = h.capacity_of_largest_room == null ? 0 : h.capacity_of_largest_room,
                             rsize_of_largest_conference = h.size_of_largest_conference == null ? 0 : h.size_of_largest_conference,
                             rminimum_turnover = h.minimum_turnover == null ? 0 : h.minimum_turnover,
                             chkOP = h.outdoor_possibility,
                             chkRIH = h.restaurant_in_hotel,
                             lng = h.restaurant_lan,
                             lat = h.restaurant_lat,
                             is360degree = h.is360degreeview,
                             lstkitchen = _context.List_Kitchen.Join(_context.KitchenType, l => l.Id, d => d.dcp_kitchentype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_restaurantinfo_id == h.dcp_restaurantinfo_id).Select(t => lang == "en-US" ? t.List.kitchen_name : t.List.kitchen_name_de),
                             lstrestauranttype = _context.List_RestaurantType.Join(_context.RestaurantType, l => l.Id, d => d.dcp_restauranttype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_restaurantinfo_id == h.dcp_restaurantinfo_id).Select(t => lang == "en-US" ? t.List.restaurant_name : t.List.restaurant_name_de),
                             lstsuitablefor = _context.List_SuitableFor.Join(_context.SuitableForType, l => l.Id, d => d.dcp_suitablefor_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_transaction_id == h.dcp_restaurantinfo_id && ld.Detail.dcp_suitablefor_type == (int)resourcetype.restaurant).Select(t => lang == "en-US" ? t.List.suitablefor_name : t.List.suitablefor_name_de),
                         }).AsEnumerable().Select(d => new ResourceModel()
                         {
                             id = d.id,
                             name = d.name,
                             country = d.country,
                             countryid = d.countryid,
                             city = d.city,
                             zipcode = d.zipcode,
                             rcapacity = d.rcapacity,
                             rtotal_conference = d.rtotal_conference,
                             rcapacity_of_largest_room = d.rcapacity_of_largest_room,
                             rsize_of_largest_conference = d.rsize_of_largest_conference,
                             rminimum_turnover = d.rminimum_turnover,
                             chkOP = d.chkOP,
                             chkRIH = d.chkRIH,
                             lng = d.lng,
                             lat = d.lat,
                             is360degree = d.is360degree,
                             type = "R",
                             kitchen = d.lstkitchen == null ? "" : String.Join(",", (d.lstkitchen).ToArray()),
                             restauranttype = d.lstrestauranttype == null ? "" : String.Join(",", (d.lstrestauranttype).ToArray()),
                             suitablefor = d.lstsuitablefor == null ? "" : String.Join(",", (d.lstsuitablefor).ToArray()),
                         });

            return query;
        }

        private IEnumerable<ResourceModel> FilterUserCart(IEnumerable<ResourceModel> resources, int UserId)
        {
            RequestRepository requestRepository = new RequestRepository();
            RequestCartModel requestCartModel = requestRepository.GetRequestCart(UserId);

            if(requestCartModel != null && requestCartModel.CartResources != null)
            {
                //resources.ToList().ForEach(x=> requestCartModel.CartResources.ForEach( y=> if(y.Id == x.))
                foreach (var item in resources)
                {
                    foreach (var item1 in requestCartModel.CartResources)
                    {
                        if (item.id == item1.ResourceId && item.type == item1.ResourceType && requestCartModel.UserId == UserId)
                        {
                            item.isincart = true;
                        }
                    }
                }
            }
            return resources;
        }
        /// <summary>
        /// Get Location Data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ResourceModel> LocationFilters(FilterModel filterModel, string lang,int? UserId)
        {
            WriteLog.LogInformation(LogMessages.LocationFilterStart);

            if (!filterModel.islocation)
            {
                IEnumerable<ResourceModel> resourceModels = new List<ResourceModel>();

                return resourceModels;
            }

            string locationtype = filterModel.LFilterModel.typeids;
            int[] location_type = null;

            if (locationtype != null && locationtype != string.Empty && locationtype.Length > 0)
            {
                int[] LocationTypeList = locationtype.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                location_type = (_context.LocationType.Where(x => LocationTypeList.Contains(x.dcp_locationtype_id)).Select(t => t.dcp_locationinfo_id)).ToArray();

            }

            string SuitableFor = filterModel.LFilterModel.suitableids;
            int[] Suitable_Ids = null;

            if (SuitableFor != null && SuitableFor != string.Empty && SuitableFor.Length > 0)
            {
                int[] SuitableList = SuitableFor.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                Suitable_Ids = (_context.SuitableForType.Where(x => SuitableList.Contains(x.dcp_suitablefor_id) && x.dcp_suitablefor_type == (int)resourcetype.location).Select(t => t.dcp_transaction_id)).ToArray();
            }

            var query = (from h in _context.Locations
                         join c in _context.List_Country on h.country_id equals c.Id into temp
                         from tmp in temp.DefaultIfEmpty()
                             // where h.country_id == filterModel.country
                         select new ResourceModel()
                         {
                             id = h.dcp_locationinfo_id,
                             name = h.location_name,
                             country = lang == "en-US" ? tmp.country_name : tmp.country_name_de,
                             countryid = h.country_id,
                             city = h.city,
                             zipcode = h.zipcode,
                             ltotal_conference = h.total_conference == null ? 0 : h.total_conference,
                             lsize_of_largest_conference = h.size_of_largest_conference == null ? 0 : h.size_of_largest_conference,
                             lroom_height = h.room_height == null ? 0 : h.room_height,
                             lrent_begining = h.rent_begining == null ? 0 : h.rent_begining,
                             chkCP = h.catering,
                             chkTP = h.technik,
                             chkCD = h.car_drive,
                             chkOP = h.outdoor_possibility,
                             lng = h.location_lan,
                             lat = h.location_lat,
                             is360degree = h.is360degreeview,
                             isincart = false,
                             //cartId = (rcr.Id == null && rcr.user_id != UserId ? 0 : rcr.Id),
                             lstlocationtype = _context.List_LocationType.Join(_context.LocationType, l => l.Id, d => d.dcp_locationtype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_locationinfo_id == h.dcp_locationinfo_id).Select(t => lang == "en-US" ? t.List.location_name : t.List.location_name_de),
                             lstsuitablefor = _context.List_SuitableFor.Join(_context.SuitableForType, l => l.Id, d => d.dcp_suitablefor_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_transaction_id == h.dcp_locationinfo_id && ld.Detail.dcp_suitablefor_type == (int)resourcetype.location).Select(t => lang == "en-US" ? t.List.suitablefor_name : t.List.suitablefor_name_de),
                         }).AsEnumerable().Select(h => new ResourceModel()
                         {
                             id = h.id,
                             name = h.name,
                             country = h.country,
                             countryid = h.countryid,
                             city = h.city,
                             zipcode = h.zipcode,
                             ltotal_conference = h.ltotal_conference,
                             lsize_of_largest_conference = h.lsize_of_largest_conference,
                             lroom_height = h.lroom_height,
                             lrent_begining = h.lrent_begining,
                             chkCP = h.chkCP,
                             chkTP = h.chkTP,
                             chkCD = h.chkCD,
                             chkOP = h.chkOP,
                             lng = h.lng,
                             lat = h.lat,
                             is360degree = h.is360degree,
                             type = "L",
                             isincart = h.isincart,
                             //cartId = h.cartId,
                             locationtype = h.lstlocationtype == null ? "" : String.Join(", ", (h.lstlocationtype).ToArray()),
                             suitablefor = h.lstsuitablefor == null ? "" : String.Join(", ", (h.lstsuitablefor).ToArray()),
                         });

            #region Filters

            if (locationtype != null && locationtype != string.Empty && locationtype.Length > 0)
            {
                if (location_type != null && location_type.Length > 0)
                {
                    query = query.Where(x => location_type.Contains(x.id));
                }
                else
                {
                    query = query.Where(x => 1 == 2);
                }
            }

            if (filterModel.country != null && filterModel.country != string.Empty && filterModel.country.Length > 0)
            {
                int[] CountryIds = filterModel.country.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                query = query.Where(x => CountryIds.Contains(x.countryid));

            }

            if (SuitableFor != null && SuitableFor != string.Empty && SuitableFor.Length > 0)
            {
                if (Suitable_Ids != null && Suitable_Ids.Length > 0)
                {
                    query = query.Where(x => Suitable_Ids.Contains(x.id));
                }
                else
                {
                    query = query.Where(x => 1 == 2);
                }
            }

            string city = filterModel.city;

            if (city != null && city != string.Empty && city.Length > 0)
            {
                query = query.Where(x => x.city.ToUpper() == (city.ToUpper()));
            }

            string name = filterModel.LFilterModel.locationname;

            if (name != null && name != string.Empty && name.Length > 0)
            {
                query = query.Where(x => x.name.ToUpper().Contains(name.ToUpper()));
            }

            string totalroom = filterModel.LFilterModel.ltotalcroom;

            if (totalroom != null && totalroom != string.Empty && totalroom.Length > 0)
            {

                int[] totalrooms = totalroom.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                int item1 = totalrooms[0];
                int item2 = totalrooms[1];

                query = query.Where(x => x.ltotal_conference >= item1 && x.ltotal_conference <= item2);
            }

            string Rsizecroom = filterModel.LFilterModel.lsizecroom;

            if (Rsizecroom != null && Rsizecroom != string.Empty && Rsizecroom.Length > 0)
            {

                int[] Rsizecrooms = Rsizecroom.Split(',').Select(n => int.Parse(n.Trim())).ToArray();

                int item1 = Rsizecrooms[0];
                int item2 = Rsizecrooms[1];

                query = query.Where(x => x.lsize_of_largest_conference >= item1 && x.lsize_of_largest_conference <= item2);
            }

            string roomcapacity = filterModel.LFilterModel.lroomheight;

            if (roomcapacity != null && roomcapacity != string.Empty && roomcapacity.Length > 0)
            {

                double[] roomcapacitys = roomcapacity.Split(',').Select(n => double.Parse(n.Trim())).ToArray();

                double item1 = roomcapacitys[0];
                double item2 = roomcapacitys[1];

                query = query.Where(x => x.lroom_height >= item1 && x.lroom_height <= item2);
            }

            string restaurantcapacity = filterModel.LFilterModel.lrentalcost;

            if (restaurantcapacity != null && restaurantcapacity != string.Empty && restaurantcapacity.Length > 0)
            {

                double[] restaurantcapacitys = restaurantcapacity.Split(',').Select(n => double.Parse(n.Trim())).ToArray();

                double item1 = restaurantcapacitys[0];
                double item2 = restaurantcapacitys[1];

                query = query.Where(x => x.lrent_begining >= item1 && x.lrent_begining <= item2);
            }

            string chkoutdoor = filterModel.LFilterModel.chkoutdoor;

            if (chkoutdoor != null && chkoutdoor != string.Empty && chkoutdoor.Length > 0)
            {
                bool chkoutdoors = Convert.ToBoolean(filterModel.LFilterModel.chkoutdoor);

                query = query.Where(x => x.chkOP == chkoutdoors);

            }

            string chkCP = filterModel.LFilterModel.chkCP;

            if (chkCP != null && chkCP != string.Empty && chkCP.Length > 0)
            {
                bool chkCPs = Convert.ToBoolean(filterModel.LFilterModel.chkCP);

                query = query.Where(x => x.chkCP == chkCPs);

            }

            string chkTP = filterModel.LFilterModel.chkTP;

            if (chkTP != null && chkTP != string.Empty && chkTP.Length > 0)
            {
                bool chkTPs = Convert.ToBoolean(filterModel.LFilterModel.chkTP);

                query = query.Where(x => x.chkTP == chkTPs);

            }

            string chkCD = filterModel.LFilterModel.chkCD;

            if (chkCD != null && chkCD != string.Empty && chkCD.Length > 0)
            {
                bool chkCDs = Convert.ToBoolean(filterModel.LFilterModel.chkCD);

                query = query.Where(x => x.chkCD == chkCDs);

            }

            #endregion

            WriteLog.LogInformation(LogMessages.LocationFilterEnd);

            return query;
        }

        /// <summary>
        /// Get Location Data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ResourceModel> Locations(int[] Ids, string lang)
        {
            if (Ids.Length <= 0)
            {
                IEnumerable<ResourceModel> resourceModels = new List<ResourceModel>();

                return resourceModels;
            }

            var query = (from h in _context.Locations
                         join c in _context.List_Country on h.country_id equals c.Id into temp
                         from tmp in temp.DefaultIfEmpty()
                         
                         where Ids.Contains(h.dcp_locationinfo_id)
                         orderby h.location_name ascending
                         select new ResourceModel()
                         {
                             id = h.dcp_locationinfo_id,
                             name = h.location_name,
                             country = lang == "en-US" ? tmp.country_name : tmp.country_name_de,
                             countryid = h.country_id,
                             city = h.city,
                             zipcode = h.zipcode,
                             ltotal_conference = h.total_conference == null ? 0 : h.total_conference,
                             lsize_of_largest_conference = h.size_of_largest_conference == null ? 0 : h.size_of_largest_conference,
                             lroom_height = h.room_height == null ? 0 : h.room_height,
                             lrent_begining = h.rent_begining == null ? 0 : h.rent_begining,
                             chkCP = h.catering,
                             chkTP = h.technik,
                             chkCD = h.car_drive,
                             chkOP = h.outdoor_possibility,
                             lng = h.location_lan,
                             lat = h.location_lat,
                             is360degree = h.is360degreeview,
                        
                             lstlocationtype = _context.List_LocationType.Join(_context.LocationType, l => l.Id, d => d.dcp_locationtype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_locationinfo_id == h.dcp_locationinfo_id).Select(t => lang == "en-US" ? t.List.location_name : t.List.location_name_de),
                             lstsuitablefor = _context.List_SuitableFor.Join(_context.SuitableForType, l => l.Id, d => d.dcp_suitablefor_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_transaction_id == h.dcp_locationinfo_id && ld.Detail.dcp_suitablefor_type == (int)resourcetype.location).Select(t => lang == "en-US" ? t.List.suitablefor_name : t.List.suitablefor_name_de),
                         }).AsEnumerable().Select(h => new ResourceModel()
                         {
                             id = h.id,
                             name = h.name,
                             country = h.country,
                             countryid = h.countryid,
                             city = h.city,
                             zipcode = h.zipcode,
                             ltotal_conference = h.ltotal_conference,
                             lsize_of_largest_conference = h.lsize_of_largest_conference,
                             lroom_height = h.lroom_height,
                             lrent_begining = h.lrent_begining,
                             chkCP = h.chkCP,
                             chkTP = h.chkTP,
                             chkCD = h.chkCD,
                             chkOP = h.chkOP,
                             lng = h.lng,
                             lat = h.lat,
                             is360degree = h.is360degree,
                             type = "L",
                             locationtype = h.lstlocationtype == null ? "" : String.Join(",", (h.lstlocationtype).ToArray()),
                             suitablefor = h.lstsuitablefor == null ? "" : String.Join(",", (h.lstsuitablefor).ToArray()),
                         });

            return query;
        }

        /// <summary>
        /// Get Hotel Info By Id
        /// </summary>
        /// <returns></returns>
        public HotelModel GetHotelInfo(int Id, string lang, int UserId)
        {

            var dbRequestCart = (from rc in _context.RequestCarts
                                 join rr in _context.RequestCartResources on rc.Id equals rr.request_cart_id
                                 where rc.user_id== UserId && rr.resource_id== Id && rr.resource_type == 0
                                 select new { requestCartResourceId = rr.Id, resourceId = rr.resource_id, userId=rc.user_id }).ToList();
            bool isinCart = false;
            if(dbRequestCart.Count()>0)
            {
                isinCart = true;
            }

            var result = (from hoteldata in _context.Hotels
                          join c in _context.List_Country on hoteldata.country_id equals c.Id into temp
                          from tmp in temp.DefaultIfEmpty()
                          join c in _context.List_Airport on hoteldata.airport_id equals c.Id into temp1
                          from tmp1 in temp1.DefaultIfEmpty()
                          where hoteldata.dcp_hotelinfo_id == Id
                          select new HotelModel()
                          {
                              id = hoteldata.dcp_hotelinfo_id,
                              star = hoteldata.star,
                              total_conference = hoteldata.total_conference,
                              total_rooms = hoteldata.total_rooms,
                              hotel_chain = hoteldata.hotel_chain,
                              hotel_name = hoteldata.hotel_name,
                              country = lang == "en-US" ? tmp.country_name : tmp.country_name_de,
                              city = hoteldata.city,
                              state = hoteldata.state,
                              zipcode = hoteldata.zipcode,
                              street = hoteldata.street,
                              webaddress = hoteldata.webaddress,
                              miscellaneous_info = hoteldata.miscellaneous_info,
                              airport_name = tmp1.Airport_name,
                              airport_transfer_time = hoteldata.airport_transfer_time,
                              size_of_largest_conference = hoteldata.size_of_largest_conference != null ? hoteldata.size_of_largest_conference : 0,
                              airport_distance = hoteldata.airport_distance != null ? hoteldata.airport_distance : 0,
                              parking_spaces = hoteldata.parking_spaces != null ? hoteldata.parking_spaces : 0,
                              green = hoteldata.green,
                              blacked = hoteldata.blacked,
                              closed = hoteldata.closed,
                              lan = hoteldata.hotel_lan,
                              lat = hoteldata.hotel_lat,
                              is360degreeview = hoteldata.is360degreeview,
                              lsthoteltype = _context.List_HotelType.Join(_context.HotelType, h => h.Id, d => d.dcp_hoteltype_id, (h, d) => new { List = h, Detail = d }).Where(t => t.Detail.dcp_hotelinfo_id == hoteldata.dcp_hotelinfo_id).Select(lt => lang == "en-US" ? lt.List.hoteltype_name : lt.List.hoteltype_name_de),
                              isincart = isinCart, // dbRequestCart.Where(t => t.resourceId == hoteldata.dcp_hotelinfo_id && t.userId == UserId).Count() > 0 ? true : false
                              reopening=hoteldata.reopening

                          }).AsEnumerable().Select(h=>new HotelModel() {
                              id = h.id,
                              star = h.star,
                              total_conference = h.total_conference,
                              total_rooms = h.total_rooms,
                              hotel_chain = h.hotel_chain,
                              hotel_name = h.hotel_name,
                              country =  h.country ,
                              city = h.city,
                              state = h.state,
                              zipcode = h.zipcode,
                              street = h.street,
                              webaddress = h.webaddress,
                              miscellaneous_info = h.miscellaneous_info,
                              airport_name = h.airport_name,
                              airport_transfer_time = h.airport_transfer_time,
                              size_of_largest_conference = h.size_of_largest_conference,
                              airport_distance = h.airport_distance,
                              parking_spaces = h.parking_spaces,
                              green = h.green,
                              blacked = h.blacked,
                              closed = h.closed,
                              lan = h.lan,
                              lat = h.lat,
                              is360degreeview = h.is360degreeview,
                              hotel_type = h.lsthoteltype!=null? string.Join(",", h.lsthoteltype.ToArray()):"",
                              isincart=h.isincart,
                              reopening=h.reopening
                          }).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Get Restaurant Info By Id
        /// </summary>
        /// <returns></returns>
        public RestaurantModel GetRestaurantInfo(int Id, string lang, int UserId)
        {

            var dbRequestCart = (from rc in _context.RequestCarts
                                 join rr in _context.RequestCartResources on rc.Id equals rr.request_cart_id
                                 where rc.user_id == UserId && rr.resource_id == Id && rr.resource_type==1
                                 select new { requestCartResourceId = rr.Id, resourceId = rr.resource_id, userId = rc.user_id }).ToList();
            bool isinCart = false;
            if (dbRequestCart.Count() > 0)
            {
                isinCart = true;
            }
            var result = (from h in _context.Restaurants
                          join c in _context.List_Country on h.country_id equals c.Id into temp
                          from tmp in temp.DefaultIfEmpty()
                          where h.dcp_restaurantinfo_id == Id
                          select new RestaurantModel()
                          {
                              id = h.dcp_restaurantinfo_id,
                              restaurant_name = h.restaurant_name,
                              country = lang == "en-US" ? tmp.country_name : tmp.country_name_de,
                              city = h.city,
                              zipcode = h.zipcode,
                              street = h.street,
                              state = h.state,
                              webaddress = h.webaddress,
                              outdoor_details = h.outdoor_details,
                              miscellaneous_info = h.miscellaneous_info,
                              capacity = h.capacity == null ? 0 : h.capacity,
                              total_conference = h.total_conference == null ? 0 : h.total_conference,
                              capacity_of_largest_room = h.capacity_of_largest_room == null ? 0 : h.capacity_of_largest_room,
                              size_of_largest_conference = h.size_of_largest_conference == null ? 0 : h.size_of_largest_conference,
                              minimum_turnover = h.minimum_turnover == null ? 0 : h.minimum_turnover,
                              outdoor_possibility = h.outdoor_possibility,
                              restaurant_in_hotel = h.restaurant_in_hotel,
                              lstkitchen = _context.List_Kitchen.Join(_context.KitchenType, l => l.Id, d => d.dcp_kitchentype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_restaurantinfo_id == h.dcp_restaurantinfo_id).Select(t => lang == "en-US" ? t.List.kitchen_name : t.List.kitchen_name_de),
                              green = h.green,
                              blacked = h.blacked,
                              closed = h.closed,
                              lan = h.restaurant_lan,
                              lat = h.restaurant_lat,
                              is360degreeview = h.is360degreeview,
                              lstrestauranttype = _context.List_RestaurantType.Join(_context.RestaurantType, l => l.Id, d => d.dcp_restauranttype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_restaurantinfo_id == h.dcp_restaurantinfo_id).Select(t => lang == "en-US" ? t.List.restaurant_name : t.List.restaurant_name_de),
                              lstsuitableforwhat = _context.List_SuitableFor.Join(_context.SuitableForType, l => l.Id, d => d.dcp_suitablefor_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_transaction_id == h.dcp_restaurantinfo_id && ld.List.suitablefor_type == (int)resourcetype.restaurant).Select(t => lang == "en-US" ? t.List.suitablefor_name : t.List.suitablefor_name_de),
                              isincart = isinCart,
                              reopening=h.reopening
                              
                          }).AsEnumerable().Select(d => new RestaurantModel()
                          {
                              id = d.id,
                              restaurant_name = d.restaurant_name,
                              country = d.country,
                              city = d.city,
                              zipcode = d.zipcode,
                              street = d.street,
                              state = d.state,
                              webaddress = d.webaddress,
                              outdoor_details = d.outdoor_details,
                              miscellaneous_info = d.miscellaneous_info,
                              capacity = d.capacity,
                              total_conference = d.total_conference,
                              capacity_of_largest_room = d.capacity_of_largest_room,
                              size_of_largest_conference = d.size_of_largest_conference,
                              minimum_turnover = d.minimum_turnover,
                              outdoor_possibility = d.outdoor_possibility,
                              restaurant_in_hotel = d.restaurant_in_hotel,
                              kitchen = d.lstkitchen == null ? "" : String.Join(",", (d.lstkitchen).ToArray()),
                              green = d.green,
                              blacked = d.blacked,
                              closed = d.closed,
                              lan = d.lan,
                              lat = d.lat,
                              is360degreeview = d.is360degreeview,
                              restaurant_type = d.lstrestauranttype !=null ? string.Join(",", d.lstrestauranttype.ToArray()):"",
                              suitableforwhat = d.lstsuitableforwhat !=null ? string.Join(",",d.lstsuitableforwhat.ToArray()) : "",
                              isincart = d.isincart,
                              reopening = d.reopening
                          }).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Get Location Info By Id
        /// </summary>
        /// <returns></returns>
        public LocationModel GetLocationInfo(int Id, string lang, int UserId)
        {
            var dbRequestCart = (from rc in _context.RequestCarts
                                 join rr in _context.RequestCartResources on rc.Id equals rr.request_cart_id
                                 where rc.user_id == UserId && rr.resource_id == Id && rr.resource_type == 2
                                 select new { requestCartResourceId = rr.Id, resourceId = rr.resource_id, userId = rc.user_id }).ToList();
            bool isinCart = false;
            if (dbRequestCart.Count() > 0)
            {
                isinCart = true;
            }

            var result = (from h in _context.Locations
                          join c in _context.List_Country on h.country_id equals c.Id into temp
                          from tmp in temp.DefaultIfEmpty()
                          where h.dcp_locationinfo_id == Id
                          select new LocationModel()
                          {
                              id = h.dcp_locationinfo_id,
                              location_name = h.location_name,
                              country = lang == "en-US" ? tmp.country_name : tmp.country_name_de,
                              city = h.city,
                              zipcode = h.zipcode,
                              street = h.street,
                              state = h.state,
                              webaddress = h.webaddress,
                              outdoor_details = h.outdoor_details,
                              miscellaneous_info = h.miscellaneous_info,
                              total_conference = h.total_conference == null ? 0 : h.total_conference,
                              size_of_largest_conference = h.size_of_largest_conference == null ? 0 : h.size_of_largest_conference,
                              room_height = h.room_height == null ? 0 : h.room_height,
                              rent_begining = h.rent_begining == null ? 0 : h.rent_begining,
                              catering = h.catering,
                              technik = h.technik,
                              car_drive = h.car_drive,
                              outdoor_possibility = h.outdoor_possibility,
                              lstlocationtype = _context.List_LocationType.Join(_context.LocationType, l => l.Id, d => d.dcp_locationtype_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_locationinfo_id == h.dcp_locationinfo_id).Select(t => lang == "en-US" ? t.List.location_name : t.List.location_name_de),
                              green = h.green,
                              blacked = h.blacked,
                              closed = h.closed,
                              lan = h.location_lan,
                              lat = h.location_lat,
                              lstsuitableforwhat = _context.List_SuitableFor.Join(_context.SuitableForType, l => l.Id, d => d.dcp_suitablefor_id, (l, d) => new { List = l, Detail = d }).Where(ld => ld.Detail.dcp_transaction_id == h.dcp_locationinfo_id && ld.List.suitablefor_type == (int)resourcetype.location).Select(t => lang == "en-US" ? t.List.suitablefor_name : t.List.suitablefor_name_de),
                              isincart= isinCart,
                              reopening = h.reopening

                          }).AsEnumerable().Select(h => new LocationModel()
                          {
                              id = h.id,
                              location_name = h.location_name,
                              country = h.country,
                              city = h.city,
                              zipcode = h.zipcode,
                              street = h.street,
                              state = h.state,
                              webaddress = h.webaddress,
                              outdoor_details = h.outdoor_details,
                              miscellaneous_info = h.miscellaneous_info,
                              total_conference = h.total_conference == null ? 0 : h.total_conference,
                              size_of_largest_conference = h.size_of_largest_conference == null ? 0 : h.size_of_largest_conference,
                              room_height = h.room_height == null ? 0 : h.room_height,
                              rent_begining = h.rent_begining == null ? 0 : h.rent_begining,
                              catering = h.catering,
                              technik = h.technik,
                              car_drive = h.car_drive,
                              outdoor_possibility = h.outdoor_possibility,
                              location_type = h.lstlocationtype == null ? "" : String.Join(",", (h.lstlocationtype).ToArray()),
                              green = h.green,
                              blacked = h.blacked,
                              closed = h.closed,
                              lan = h.lan,
                              lat = h.lat,                            
                              suitableforwhat = h.lstsuitableforwhat==null?"": string.Join(",",h.lstsuitableforwhat.ToArray()),
                              isincart = h.isincart,
                              reopening = h.reopening
                          }).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// Get Image Info By Id
        /// </summary>
        /// <returns></returns>
        public List<PhotosModel> GetImageData(int id, string type)
        {
            var result = (from photos in _context.Photos
                          where photos.transaction_id == id && photos.photo_type == (type == "H" ? 0 : type == "R" ? 1 : 2)
                          select new PhotosModel()
                          {
                              photo_type = photos.photo_type,
                              photo_id = photos.transaction_id,
                              photo_name = photos.photo_name,
                              photo_size = photos.photo_size
                          }).ToList();

            if (result == null || result.Count() == 0)
            {
                result.Add(new PhotosModel() { photo_type = (type == "H" ? 0 : type == "R" ? 1 : 2), photo_id = id, photo_name = "no-images.png", photo_size = 0 });
            }

            return result;
        }

        /// <summary>
        /// Get Near By Restaurant
        /// </summary>
        /// <returns></returns>
        public List<NearByModel> GetNearByRestaurantList(string lat1, string lng1, int? id, string type, int distance)
        {
            float lat = float.Parse(lat1, new System.Globalization.CultureInfo("en-US").NumberFormat);
            float lng = float.Parse(lng1, new System.Globalization.CultureInfo("en-US").NumberFormat);

            //int? cid = GetcountryId(type, id.Value);

            var result = (from r in _context.Restaurants
                          where r.restaurant_lat != null && r.restaurant_lan != null && r.restaurant_lat != 0 && r.restaurant_lan != 0 //&& (r.country_id == cid.Value)
                          select new NearByModel()
                          {
                              id = r.dcp_restaurantinfo_id,
                              name = r.restaurant_name,
                              lng = r.restaurant_lan,
                              lat = r.restaurant_lat
                          }).ToList();

            result = result.Where(x => _context.Database.SqlQuery<float>("DistanceKM @Lat1={0},@Lat2={1},@Long1={2},@Long2={3}", lat, (float)x.lat.Value, lng, (float)x.lng.Value).First() <= distance).ToList();

            return result;
        }

        /// <summary>
        /// Get Near By Location
        /// </summary>
        /// <returns></returns>
        public List<NearByModel> GetNearByLocationList(string lat1, string lng1, int? id, string type, int distance)
        {
            float lat = float.Parse(lat1, new System.Globalization.CultureInfo("en-US").NumberFormat);
            float lng = float.Parse(lng1, new System.Globalization.CultureInfo("en-US").NumberFormat);

           // int? cid = GetcountryId(type, id.Value);

            var result = (from r in _context.Locations
                          where r.location_lat != null && r.location_lan != null && r.location_lat != 0 && r.location_lan != 0 //&& (r.country_id == cid.Value)
                          select new NearByModel()
                          {
                              id = r.dcp_locationinfo_id,
                              name = r.location_name,
                              lng = r.location_lan,
                              lat = r.location_lat
                          }).ToList();

            result = result.Where(x => _context.Database.SqlQuery<float>("DistanceKM @Lat1={0},@Lat2={1},@Long1={2},@Long2={3}", lat, (float)x.lat.Value, lng, (float)x.lng.Value).First() <= distance).ToList();

            return result;
        }

        /// <summary>
        /// Get Near By Hotel
        /// </summary>
        /// <returns></returns>
        public List<NearByModel> GetNearByHotelList(string lat1, string lng1, int? id, string type, int distance)
        {
            float lat = float.Parse(lat1, new System.Globalization.CultureInfo("en-US").NumberFormat);
            float lng = float.Parse(lng1, new System.Globalization.CultureInfo("en-US").NumberFormat);

           // int? cid = GetcountryId(type, id.Value);

            var result = (from r in _context.Hotels
                          where r.hotel_lat != null && r.hotel_lan != null && r.hotel_lat != 0 && r.hotel_lan != 0 //&& (r.country_id == cid.Value)
                          select new NearByModel()
                          {
                              id = r.dcp_hotelinfo_id,
                              name = r.hotel_name,
                              lng = r.hotel_lan,
                              lat = r.hotel_lat
                          }).ToList();

            result = result.Where(x => _context.Database.SqlQuery<float>("DistanceKM @Lat1={0},@Lat2={1},@Long1={2},@Long2={3}", lat, (float)x.lat.Value, lng, (float)x.lng.Value).First() <= distance).ToList();

            return result;
        }
        
        /// <summary>
        /// Get Country Id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private int? GetcountryId(string type, int id)
        {
            int? countryid = null;

            if (type == "H")
            {
                countryid = (from c in _context.Hotels where c.dcp_hotelinfo_id == id select c.country_id).FirstOrDefault();
            }
            else if (type == "R")
            {
                countryid = (from c in _context.Restaurants where c.dcp_restaurantinfo_id == id select c.country_id).FirstOrDefault();
            }
            else if (type == "L")
            {
                countryid = (from c in _context.Locations where c.dcp_locationinfo_id == id select c.country_id).FirstOrDefault();
            }

            return countryid;

        }

        /// <summary>
        /// Get RFP Resources by applied filters
        /// </summary>
        /// <returns></returns>
        public BaseResourceModel GetRFPResourceList(string Rids, string type, string lang)
        {
            BaseResourceModel ResponceData = new BaseResourceModel();

            #region Filters

            int[] Ids = null;

            if (Rids != null && Rids != string.Empty && Rids.Length > 0)
            {
                Ids = Rids.Split(',').Select(n => int.Parse(n.Trim())).ToArray();
            }

            #endregion

            if (type == "H")
            {
                ResponceData.ResourceModel = Hotels(Ids, lang).ToList();
            }
            else if (type == "R")
            {
                ResponceData.ResourceModel = Restaurants(Ids, lang).ToList();

            }
            else if (type == "L")
            {
                ResponceData.ResourceModel = Locations(Ids, lang).ToList();
            }

            ResponceData.totalrecord = ResponceData.ResourceModel.Count();

            return ResponceData;
        }

        /// <summary>
        /// Get Cart Resources
        /// </summary>
        /// <returns></returns>
        public BaseResourceModel GetCartList(int UserId, string lang)
        {
            BaseResourceModel ResponceData = new BaseResourceModel();

            int? cartid = _context.RequestCarts.Where(x => x.user_id == UserId).Select(x => x.Id).FirstOrDefault();

            if (cartid != null && cartid != 0)
            {
                int[] hids = _context.RequestCartResources.Where(x => x.request_cart_id == cartid && x.resource_type == 0).Select(x => x.resource_id).ToArray();

                IEnumerable<ResourceModel> Hotel = Hotels(hids, lang);

                int[] rids = _context.RequestCartResources.Where(x => x.request_cart_id == cartid && x.resource_type == 1).Select(x => x.resource_id).ToArray();

                IEnumerable<ResourceModel> Restaurant = Restaurants(rids, lang);

                int[] lids = _context.RequestCartResources.Where(x => x.request_cart_id == cartid && x.resource_type == 2).Select(x => x.resource_id).ToArray();

                IEnumerable<ResourceModel> Location = Locations(lids, lang);

                IEnumerable<ResourceModel> Data = Hotel.Union(Restaurant).Union(Location);

                ResponceData.totalrecord = Data.Count();

                Data = Data.OrderBy(x => x.name);

                ResponceData.ResourceModel = Data.ToList();

            }

            return ResponceData;
        }

        /// <summary>
        /// Get Cart Count
        /// </summary>
        /// <returns></returns>
        public BaseResourceModel GetCartCount(int UserId, string lang)
        {
            BaseResourceModel ResponceData = new BaseResourceModel();

            int? cartid = _context.RequestCarts.Where(x => x.user_id == UserId).Select(x => x.Id).FirstOrDefault();

            if (cartid != null && cartid != 0)
            {
                int[] hids = _context.RequestCartResources.Where(x => x.request_cart_id == cartid && x.resource_type == 0).Select(x => x.resource_id).ToArray();

                IEnumerable<ResourceModel> Hotel = Hotels(hids, lang);

                int[] rids = _context.RequestCartResources.Where(x => x.request_cart_id == cartid && x.resource_type == 1).Select(x => x.resource_id).ToArray();

                IEnumerable<ResourceModel> Restaurant = Restaurants(rids, lang);

                int[] lids = _context.RequestCartResources.Where(x => x.request_cart_id == cartid && x.resource_type == 2).Select(x => x.resource_id).ToArray();

                IEnumerable<ResourceModel> Location = Locations(lids, lang);

                IEnumerable<ResourceModel> Data = Hotel.Union(Restaurant).Union(Location);

                ResponceData.totalrecord = Data.Count();

            }

            return ResponceData;
        }

        private double DistanceKM(double Lat1, double Long1, double Lat2, double Long2)
        {
            double dLat1InRad = Lat1 * (Math.PI / 180.0);
            double dLong1InRad = Long1 * (Math.PI / 180.0);
            double dLat2InRad = Lat2 * (Math.PI / 180.0);
            double dLong2InRad = Long2 * (Math.PI / 180.0);

            double dLongitude = dLong2InRad - dLong1InRad;
            double dLatitude = dLat2InRad - dLat1InRad;

            // Intermediate result a.
            double a = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
                       Math.Cos(dLat1InRad) * Math.Cos(dLat2InRad) *
                       Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

            // Intermediate result c (great circle distance in Radians).
            double c = 2.0 * Math.Asin(Math.Sqrt(a));

            // Distance.
            // const Double kEarthRadiusMiles = 3956.0;
            return 6367.45 * c;
        }

    }
}
