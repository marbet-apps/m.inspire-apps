﻿using DCP.Entities;

namespace DCP.Business.Interface
{
    public interface IBLookUp
    {
        /// <summary>
        /// Get Country List
        /// </summary>
        ApiResult GetCountryList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Radius List
        /// </summary>
        ApiResult GetRadiusList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Hotel Chain List
        /// </summary>
        ApiResult GetChainList(string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Hotel List
        /// </summary>
        ApiResult GetHotelList(string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Restaurant List
        /// </summary>
        ApiResult GetRestaurantList(string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Location List
        /// </summary>
        ApiResult GetLocationList(string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Hotel Type List
        /// </summary>
        ApiResult GetHotelTypeList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Hotel Type List
        /// </summary>
        ApiResult GetDefaultHotelTypeList(string Lang, int[] Ids);

        /// <summary>
        /// Get Restaurant Type List
        /// </summary>
        ApiResult GetRestaurantTypeList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Location Type List
        /// </summary>
        ApiResult GetLocationTypeList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Kitchen List
        /// </summary>
        ApiResult GetKitchenList(string Lang, string term, int page, int pageSize, int type);

        /// <summary>
        /// Get Kitchen List
        /// </summary>
        ApiResult GetSuitableForList(string Lang, string term, int page, int pageSize, int type);
        /// <summary>
        /// Get Subscription list.
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        ApiResult GetSubscriptionTypeList(string Lang);
        /// <summary>
        /// Get Request List
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        ApiResult GetRequestStatusList(string Lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        ApiResult GetRequestTypes(string Lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        ApiResult GetRequestItemTypes(string Lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        ApiResult GetRoomTypes(string Lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        ApiResult GetRoomTechniques(string Lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        ApiResult GetSeatingTypes(string Lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        ApiResult GetDaysRunning(string Lang);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        ApiResult GetTimeIntervals(int interval);

        /// <summary>
        /// Get City List
        /// </summary>
        /// <returns></returns>
        ApiResult GetCityList();
        /// <summary>
        /// Get list of abort reason.
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        ApiResult GetAbortReason(string Lang);
    }

}
