﻿$(document).ready(function () {

   
    $($resetPassword.controls.divMsgValue).hide();

    $($resetPassword.controls.btnSubmit).click(function () {
        if ($resetPassword.validateChangePwd()) {
            $resetPassword.submitchangepassword();
        }
        else {
            return false;
        }

    });

    $($resetPassword.controls.btnCancel).click(function () {
        location.href = $($resetPassword.controls.hdnWebsiteURL).val() + "login";

    });

    
    var qryString = location.search.substr(1);
    if (qryString != null && qryString.length > 0) {
        
        var values = qryString.split('&');
        if (values != null && values.length > 0) {
            var ids = values[0].split('in_d=');
            if (ids != null && ids.length > 0) {
                $resetPassword.variable.in_d = ids[1];
            }

            ids = '';
            ids = values[1].split('in_t=');
            if (ids != null && ids.length > 0) {
                $resetPassword.variable.in_t = ids[1];
            }

            
            if (values.length > 2) {
                ids = '';
                ids = values[2].split('in_a=');
                if (ids != null && ids.length > 0) {
                    $resetPassword.variable.in_a = ids[1];
                }
            }
        }
    }

    $resetPassword.ValidateUser();

    $($resetPassword.controls.lblclickhere).hide();

    $($resetPassword.controls.lnkRefreshCaptcha).click(function(){
        $resetPassword.GetCaptcha();
    });

});

var $resetPassword = {

    controls: {
        txtnewpass: "#txtnewpass",
        txtconfirmpass: "#txtconfirmpass",
        hdnConfirmPassMsg: "#hdnConfirmPassMsg",
        btnSubmit: "#btnSubmit",
        hdnAPIBaseURL: "#hdnAPIBaseURL",
        divMsgValue: "#divMsgValue",
        lblMsgValue: "#lblMsgValue",
        divContent: "#divContent",
        lblclickhere: "#lblclickhere",
        hdnPasswordValidationMsg: "#hdnPasswordValidationMsg",
        txtcaptchatext: "#txtcaptchatext",
        hdnCaptchaValue: "#hdnCaptchaValue",
        btnCancel: "#btnCancel",
        hdnWebsiteURL: "#hdnWebsiteURL",
        lblPageTitle: "#lblPageTitle",
        hdnSomethingWrongMsg: "#hdnSomethingWrongMsg",
        lblUserName: "#lblUserName",
        lnkRefreshCaptcha:"#lnkRefreshCaptcha"
    },

    apiPath: {
        resetPasswordURL: "Secure/ResetPassword",
        ValidateUser: "Secure/ValidateUser"
      
    },
    variable: {
        in_d: '',
        in_t: '',
        in_a: ''
    },

    validateChangePwd: function () {
        
        var flag = true;

        var newpass = $($resetPassword.controls.txtnewpass).val();
        var confirmpass = $($resetPassword.controls.txtconfirmpass).val();
        var captcha = $($resetPassword.controls.txtcaptchatext).val();


        if (newpass == undefined || newpass == null || newpass == "") {
            $($resetPassword.controls.txtnewpass).addClass("Error");
            flag = false;
        } else {
            $($resetPassword.controls.txtnewpass).removeClass("Error");
        }

        if (confirmpass == undefined || confirmpass == null || confirmpass == "") {
            $($resetPassword.controls.txtconfirmpass).addClass("Error");
            flag = false;
        } else {
            $($resetPassword.controls.txtconfirmpass).removeClass("Error");
        }

        
        if (captcha == undefined || captcha == null || captcha == "") {
            $($resetPassword.controls.txtcaptchatext).addClass("Error");
            flag = false;
        } else {
            $($resetPassword.controls.txtcaptchatext).removeClass("Error");
        }

        if ($resetPassword.checkPasswordStrength() == false) {
            flag = false;
        }

        if (confirmpass != undefined || confirmpass != null || confirmpass != "" || newpass == undefined || newpass == null || newpass == "") {
            if ($($resetPassword.controls.txtnewpass).val() != $($resetPassword.controls.txtconfirmpass).val()) {
                $($resetPassword.controlstxtnewpass).addClass("Error");
                $($resetPassword.controls.txtconfirmpass).addClass("Error");
                $managecode.Decision(false, "", "", $($resetPassword.controls.hdnConfirmPassMsg).val(), "Login", false);
                flag = false;
            }
        }

        if (captcha != undefined && captcha != null && captcha != "") {
            if (captcha != $($resetPassword.controls.hdnCaptchaValue).val()) {
                $($resetPassword.controls.txtcaptchatext).addClass("Error");
                flag = false;
            }
            else {
                $($resetPassword.controls.txtcaptchatext).removeClass("Error");
            }           
        } 

        return flag;
    },
    submitchangepassword: function () {

        

        var login = {
            
            NewPassword: $($resetPassword.controls.txtnewpass).val(),
            IdValue: $resetPassword.variable.in_d,
            RequestedDate: $resetPassword.variable.in_t,
            RequestSendForActivateUser: $resetPassword.variable.in_a
        };

        $.ajax({
            type: "POST",
            url: $($resetPassword.controls.hdnAPIBaseURL).val() + $resetPassword.apiPath.resetPasswordURL,
            data: JSON.stringify(login),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {
                
                if (data != undefined && data != null && data.code == "723") {

                    //$($resetPassword.controls.lblMsgValue).show();
                    //$($resetPassword.controls.lblMsgValue).text($("#E" + data.code).val());
                    //$($resetPassword.controls.lblclickhere).show();
                    //$($resetPassword.controls.divContent).empty(); 
                    //$($resetPassword.controls.divMsgValue).show();
                    localStorage.setItem("rUserName", $($resetPassword.controls.lblUserName).text());
                    location.href = "ThankYou";

                } else {
                    $managecode.Decision(false, "", data.code, data.message, "Login", false);
                }

            },
            failure: function (data) {
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {

                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    },

    ValidateUser: function () {

        

        var login = {
            IdValue: $resetPassword.variable.in_d,
            RequestedDate: $resetPassword.variable.in_t,
            RequestSendForActivateUser: $resetPassword.variable.in_a
        };

        $.ajax({
            type: "POST",
            url: $($resetPassword.controls.hdnAPIBaseURL).val() + $resetPassword.apiPath.ValidateUser,
            data: JSON.stringify(login),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data, textStatus, response) {
                
                if (data != undefined && data != null && data.code == "722") {
                    //user exist
                    $($resetPassword.controls.divMsgValue).hide();
                    $($resetPassword.controls.lblUserName).text(data.data.First_Name + " " + data.data.Last_Name);
                }
                else {
                    $($resetPassword.controls.lblMsgValue).show();
                    $($resetPassword.controls.lblMsgValue).text($("#E" + data.code).val());
                    $($resetPassword.controls.lblMsgValue).addClass("ErrorMsg");
                    $($resetPassword.controls.divContent).empty();
                    $($resetPassword.controls.divMsgValue).show();
                    $($resetPassword.controls.lblPageTitle).hide();

                    if ($($resetPassword.controls.lblMsgValue).text().trim() == '') {
                        $($resetPassword.controls.lblMsgValue).text($($resetPassword.controls.hdnSomethingWrongMsg).val());
                    }
                }


                //if (data != undefined && data != null && data.code == "615") {

                //   $managecode.Decision(false, "", data.code, data.message, "login", false);
                //}
                //else if (data != undefined && data != null && data.code == "722") {
                //   //user exist
                //}
                //else if (data != undefined && data != null && data.code == "609") {
                //    $managecode.Decision(false, "", data.code, data.message, "login", false);
                //}
                //else if (data != undefined && data != null && data.code == "614") {
                //    $managecode.Decision(false, "", data.code, data.message, "login", false);
                //}



            },
            failure: function (data) {
                
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {
                
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });
    },

    checkPasswordStrength: function () {
        
       var  flag = false;
        var newpass = $($resetPassword.controls.txtnewpass).val();

        var number = /([0-9])/;
        var alphabets = /([a-z])/;
        var upperAlphabets = /([A-Z])/;
        var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;

        if (newpass.length < 6) {
            $($resetPassword.controls.txtnewpass).addClass("Error");
            $managecode.Decision(false, "", "", $($resetPassword.controls.hdnPasswordValidationMsg).val(), "login", false);
            flag = false;
        }
        else {
            if ((newpass.match(number) != null && newpass.match(number).length > 0) && (newpass.match(alphabets) != null && newpass.match(alphabets).length > 0)
                && (newpass.match(special_characters) != null && newpass.match(special_characters).length > 0) && (newpass.match(upperAlphabets) != null && newpass.match(upperAlphabets).length > 0) ) {
                $($resetPassword.controls.txtnewpass).removeClass("Error");
                flag = true;
            }
            else {

                $($resetPassword.controls.txtnewpass).addClass("Error");
                $managecode.Decision(false, "", "", $($resetPassword.controls.hdnPasswordValidationMsg).val(), "login", false);
                flag = false;

              
            }
        }

        return flag;
    },

    GetCaptcha: function () {

        $.ajax({
            type: "GET",
            url: "GetSessionContent",
            cache: false,
            contentType: "text/plain",
            async:false,
            success: function (data, textStatus, response) {
                
                if (data != undefined && data != null) {
                    $($resetPassword.controls.hdnCaptchaValue).val(data);
                }
            },
            failure: function (data) {
                
                $managecode.Decision(true, data, "", "", "error", false);
            },
            error: function (data) {                
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

        $('#imgCaptcha').attr("src", "GetCaptchaImage?r=" + Math.random());
    }
};


$.ajaxSetup({
    beforeSend: function (xhr) {

        
        var lang = $.cookie("Language");

        if (lang === undefined || lang === null || lang === "") {
            lang = "en-US";
        }
        xhr.setRequestHeader("Lang", lang);

    },
    error: function (jqXHR, textStatus, errorThrown) {

        $managecode.Decision(false, "", jqXHR.status, textStatus + ": " + errorThrown, "error", false);
    }
});