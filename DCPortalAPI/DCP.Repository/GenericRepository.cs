﻿using DCP.Entities;
using DCP.Repository.Interface;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace DCP.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private DCPDbContext _context = null;
        private DbSet<T> table = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public GenericRepository()
        {
            this._context = new DCPDbContext();
            table = _context.Set<T>();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_context"></param>
        public GenericRepository(DCPDbContext _context)
        {
            this._context = _context;
            table = _context.Set<T>();
        }

        /// <summary>
        /// Get All table data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll()
        {
            return table.ToList();
        }

        /// <summary>
        /// Get Single object
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetById(object id)
        {
            return table.Find(id);
        }

        /// <summary>
        /// Insert object
        /// </summary>
        /// <param name="obj"></param>
        public void Insert(T obj)
        {
            table.Add(obj);
        }

        /// <summary>
        /// Update Object
        /// </summary>
        /// <param name="obj"></param>
        public void Update(T obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }

        /// <summary>
        /// Delete Object
        /// </summary>
        /// <param name="id"></param>
        public void Delete(object id)
        {
            T existing = table.Find(id);
            table.Remove(existing);
        }

        /// <summary>
        /// Final Save to table
        /// </summary>
        public void Save()
        {
            _context.SaveChanges();
        }

    }
}
