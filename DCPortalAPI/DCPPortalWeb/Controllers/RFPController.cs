﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DCPPortalWeb.Controllers
{
    public class RFPController : BaseController
    {
        /// <summary>
        /// Start Request For Proposal
        /// </summary>
        /// <returns></returns>
        public ActionResult StartRFP()
        {
            return View();
        }

        /// <summary>
        /// My RFP
        /// </summary>
        /// <returns></returns>
        public ActionResult MyRFP()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddRoom()
        {
            return PartialView("_AddRoom");
        }

        public ActionResult RFPDetail()
        {
            return View();
        }
    }
}