﻿var $LookUp = {

    BindDropdown: function (id, pl, p, t, pS, m, ac) {

        $(id).select2({
            placeholder: function () {
                return pl;
            },
            multiple: m,
            allowClear: ac,
            width: "",
            minimumInputLength: 0,
            ajax: {
                url: $bind.URL(p),
                dataType: 'json',
                quietMillis: 250,
                data: function (term, page) {
                    if (term == undefined || term == null || term == "") {
                        return {
                            page: page,
                            type: t,
                            pageSize: pS
                        };
                    } else {
                        return {
                            term: term,
                            page: page,
                            type: t,
                            pageSize: pS
                        };
                    }
                },
                results: function (result, page, query) {

                    if (result.code != 704 && result.code != "704") {
                        $managecode.Decision(result.code, result.message, "DDL");
                    }

                    var data = result.data;
                    var more = (page * pS) < data.totalRecord;
                    var results = [];
                    Array.prototype.push.apply(results, data.lookup.map($bind.FormatObject));
                    return { results: results, more: more };
                },
                cache: false
            },
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) { return m; }
        });

    },
    BindDropdownWithImage: function (id, pl, p, t, pS, m, ac) {

        $(id).select2({
            placeholder: function () {
                return pl;
            },            
            multiple: m,
            allowClear: ac,
            width: "",
            minimumInputLength: 0,
            ajax: {
                url: $bind.URL(p),
                dataType: 'json',
                quietMillis: 250,
                data: function (term, page) {
                    if (term == undefined || term == null || term == "") {
                        return {
                            page: page,
                            type: t,
                            pageSize: pS
                        };
                    } else {
                        return {
                            term: term,
                            page: page,
                            type: t,
                            pageSize: pS
                        };
                    }
                },
                results: function (result, page, query) {

                    if (result.code != 704 && result.code != "704") {
                        $managecode.Decision(result.code, result.message, "DDL");
                    }
                    
                    var data = result.data;
                    var more = (page * pS) < data.totalRecord;
                    var results = [];
                    Array.prototype.push.apply(results, data.lookup.map($bind.FormatObject));
                    return { results: results, more: more };
                },
                cache: false
            },
            
            dropdownCssClass: "bigdrop",
            formatResult: $rfp.formatSeatingType,
            escapeMarkup: function (m) { return m; }
            
        });

    },
    ILookUp: function () {
        $LookUp.BindDropdown($c.controls.multihoteltypei, $($c.controls.hdnselecthotel).val(), $u.path.GetHotelTypeList, 0, 50, true, true);
        $LookUp.BindDropdown($c.controls.multirestauranttypei, $($c.controls.hdnselectrestaurant).val(), $u.path.GetRestaurantTypeList, 0, 50, true, true);
        $LookUp.BindDropdown($c.controls.multirestaurantkitcheni, $($c.controls.hdnselectkitchen).val(), $u.path.GetKitchenList, 0, 50, true, true);
        $LookUp.BindDropdown($c.controls.multirestaurantsuitablefori, $($c.controls.hdnselectsuitablefor).val(), $u.path.GetSuitableForList, 1, 50, true, true);
        $LookUp.BindDropdown($c.controls.multilocationsuitablefori, $($c.controls.hdnselectsuitablefor).val(), $u.path.GetSuitableForList, 2, 50, true, true);
        $LookUp.BindDropdown($c.controls.multilocationtypei, $($c.controls.hdnselectlocation).val(), $u.path.GetLocationTypeList, 0, 50, true, true);
        $LookUp.BindDropdown($c.controls.multihotelchaini, $($c.controls.hdnselecthotelchain).val(), $u.path.GetChainList, 0, 50, true, true);
       // $LookUp.BindDropdown($c.controls.ddlocountry, $($c.controls.hdnselectcountry).val(), $u.path.GetCountryList, 0, 50, true, true);
    },
    OLookUp: function () {
        $LookUp.BindDropdown($c.controls.ddlcountry, $($c.controls.hdnselectcountry).val(), $u.path.GetCountryList, 0, 50, true, true);
        $LookUp.BindDropdown($c.controls.ddlradius, $($c.controls.hdnselectradius).val(), $u.path.GetRadiusList, 0, 50, false, true);
        $LookUp.BindDropdown($c.controls.multihoteltype, $($c.controls.hdnselecthotel).val(), $u.path.GetHotelTypeList, 0, 50, true, true);
        $LookUp.BindDropdown($c.controls.multirestauranttype, $($c.controls.hdnselectrestaurant).val(), $u.path.GetRestaurantTypeList, 0, 50, true, true);
        $LookUp.BindDropdown($c.controls.multirestaurantkitchen, $($c.controls.hdnselectkitchen).val(), $u.path.GetKitchenList, 0, 50, true, true);
        $LookUp.BindDropdown($c.controls.multirestaurantsuitablefor, $($c.controls.hdnselectsuitablefor).val(), $u.path.GetSuitableForList, 1, 50, true, true);
        $LookUp.BindDropdown($c.controls.multilocationsuitablefor, $($c.controls.hdnselectsuitablefor).val(), $u.path.GetSuitableForList, 2, 50, true, true);
        $LookUp.BindDropdown($c.controls.multilocationtype, $($c.controls.hdnselectlocation).val(), $u.path.GetLocationTypeList, 0, 50, true, true);
    },
    ILookUpWithSelected: function (dd_id, url, selected_ids) {
        var jsonData = { url: url, ids: selected_ids };
        $.ajax({
            url: $bind.URL($u.path.GetSetSelectedLookupItems),
            type: "GET",
            dataType: "json",
            data: jsonData,
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (result) {

                if (result.code != 704 && result.code != "704") {
                    $managecode.Decision(false, "", result.code, result.message, "Search History", false);
                }
                data = result.data;
                if (data != null) {
                    $(dd_id).data().select2.updateSelection(data);
                }

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });


        
    }
}

$(document).ready(function () {

    $LookUp.OLookUp();

});

