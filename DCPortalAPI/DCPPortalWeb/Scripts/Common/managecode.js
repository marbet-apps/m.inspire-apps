﻿var $managecode = {

    codes: {
        SuccessSaved: "701",
        SuccessDeleted: "702",
        SuccessUpdated: "703",
        SuccessGetList: "704",
        SuccessGetToken: "705",
        SuccessUserRegistered: "706",
        SuccessPasswordCreated: "707",
        SuccessRetrieveAllUser: "708",
        SuccessRetrieveUser: "709",
        SuccessDeleteUser: "710",
        SuccessGetResource: "711",
        ErrorInvalidCredential: "601",
        ErrorSaved: "602",
        ErrorDeleted: "603",
        ErrorUpdated: "604",
        ErrorMissingToken: "605",
        ErrorModelValidateFail: "606",
        ErrorMissingParameter: "607",
        ErrorInactiveUser: "608",
        ErrorUserNotFound: "609",
        CommonUnAuthorized: "401",
        CommonForbidden: "403",
        CommonMissingArgument: "410",
        CommonFaultError: "411",
        SuccessDeletePhoto: "721",
        SuccessPasswordChanged: "712",
        SuccessUpdateProfile:"718"
    },
    Decision: function (isobj, obj, code, message, type, IsSucceed) {
       
        common.hideLoader();

        if (isobj) {

            if (obj.status == $managecode.codes.CommonUnAuthorized) {
                $managecode.Decision(false, "", obj.status, obj.message, type, IsSucceed);
            }else if (obj.responseJSON != null) {
                $managecode.Decision(false, "", obj.responseJSON.code, obj.responseJSON.message, type, IsSucceed);
            } else if (obj == null || obj == "") {
                $managecode.Decision(false, "", code, message, type, IsSucceed);
            }
            else {
                $managecode.Decision(false, "", obj.status, obj.message, type, IsSucceed);
            }
        }
        else {

            if (code == $managecode.codes.CommonUnAuthorized) {
                common.displayMessage($("#C" + $managecode.codes.CommonUnAuthorized).val(), IsSucceed);
                location.href = "/login";
            }
            else if (code == $managecode.codes.CommonForbidden) {
                common.displayMessage($("#C" + $managecode.codes.CommonForbidden).val(), IsSucceed);
            }
            else if (code == $managecode.codes.CommonMissingArgument) {
                common.displayMessage($("#C" + $managecode.codes.CommonMissingArgument).val(), IsSucceed);
            }
            else if (code == $managecode.codes.CommonFaultError) {
                common.displayMessage($("#E" + $managecode.codes.CommonFaultError).val(), IsSucceed);
            }
            else if (code == $managecode.codes.ErrorInvalidCredential) {
                common.displayMessage($("#E" + $managecode.codes.ErrorInvalidCredential).val(), IsSucceed);
            }
            else if (code == $managecode.codes.ErrorSaved) {
                common.displayMessage($("#E" + $managecode.codes.ErrorSaved).val(), IsSucceed);
            }
            else if (code == $managecode.codes.ErrorDeleted) {
                common.displayMessage($("#E" + $managecode.codes.ErrorDeleted).val(), IsSucceed);
            }
            else if (code == $managecode.codes.ErrorUpdated) {
                common.displayMessage($("#E" + $managecode.codes.ErrorUpdated).val(), IsSucceed);
            }
            else if (code == $managecode.codes.ErrorMissingToken) {
                common.displayMessage($("#E" + $managecode.codes.ErrorMissingToken).val(), IsSucceed);
            }
            else if (code == $managecode.codes.ErrorModelValidateFail) {
                common.displayMessage(message, IsSucceed);
            }
            else if (code == $managecode.codes.ErrorInactiveUser) {
                common.displayMessage($("#E" + $managecode.codes.ErrorInactiveUser).val(), IsSucceed);
            }
            else if (code == $managecode.codes.ErrorUserNotFound) {
                common.displayMessage($("#E" + $managecode.codes.ErrorUserNotFound).val(), IsSucceed);
            }
            else if (code == $managecode.codes.SuccessPasswordCreated) {
                common.displayMessage($("#E" + $managecode.codes.SuccessPasswordCreated).val(), IsSucceed);
            }
            else if (code == $managecode.codes.SuccessDeletePhoto) {
                common.displayMessage($("#E" + $managecode.codes.SuccessDeletePhoto).val(), IsSucceed);
            }
            else if (code == $managecode.codes.SuccessPasswordChanged) {
                common.displayMessage($("#E" + $managecode.codes.SuccessPasswordChanged).val(), IsSucceed);
            }
            else if (code == $managecode.codes.SuccessUpdateProfile) {
                common.displayMessage($("#E" + $managecode.codes.SuccessUpdateProfile).val(), IsSucceed);
            }
            else if (code == $managecode.codes.SuccessSaved) {
                common.displayMessage($("#E" + $managecode.codes.SuccessSaved).val(), IsSucceed);
            }
            else if (code == $managecode.codes.SuccessDeleted) {
                common.displayMessage($("#E" + $managecode.codes.SuccessDeleted).val(), IsSucceed);
            }
            else {
                common.displayMessage(message, IsSucceed);
            }

        }

    }

};