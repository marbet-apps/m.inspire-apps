﻿namespace DCP.Utilities
{
    public static class ManageEnum
    {
        public enum resourcetype
        {
            hotel = 0,
            restaurant = 1,
            location = 2
        }

        public enum status
        {
            success = 0,
            error = 1,
            fail = 2
        }

        //28-04-2020 - changes for DCPMiceRequests log entry
        public enum requestType
        {
            rfp = 0,           
            abortRequest = 1,
            abortResponse = 2,
            rfpdetailRequest = 3,
            rfpdetailResponse = 4,
            orderRequest = 5,
            orderResponse = 6            
        }
    }
}
