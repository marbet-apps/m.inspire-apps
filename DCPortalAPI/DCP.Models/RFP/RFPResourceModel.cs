﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Models.RFP
{
    public class RFPResourceModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("rfp_mice_requestid")]
        public int RFPMiceRequestId { get; set; }
        [JsonProperty("resourceid")]
        public int ResourceId { get; set; }
        [JsonProperty("mice_resourceid")]
        public int MiceResourceId { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("created_by")]
        public int CreatedBy { get; set; }
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("modified_by")]
        public int? ModifiedBy { get; set; }
        [JsonProperty("modified_date")]
        public DateTime? ModifiedDate { get; set;  }
    }
}
