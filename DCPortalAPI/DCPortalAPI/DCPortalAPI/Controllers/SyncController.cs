﻿using DCP.Business.Interface;
using DCP.Models;
using DCP.Utilities;
using DCPortalAPI.Settings;
using DCPortalAPI.Filter;
using System.Web.Http;
using System.Collections.Generic;

namespace DCPortalAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/sync")]
   // [AllowAnonymous]
    [ValidateModel]
    [SyncAuthorizeAttribute]
    [ExceptionHandler]
    public class SyncController : ApiController
    {
        private ISyncBL syncBL;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="syncBL"></param>
        public SyncController(ISyncBL syncBL)
        {
            this.syncBL = syncBL;
        }
        [HttpPost]
        [Route("synchotel")]
        public IHttpActionResult SyncHotel(MigrationHotelModel Hotels)
        {
            WriteLog.LogInformation(LogMessages.SyncHotelStarted);

            try
            {
                var Response = syncBL.SyncHotels(Hotels);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.SyncHotelEnded);

            }
        }

        [HttpPost]
        [Route("synclocation")]
        public IHttpActionResult SyncLocation(MigrationLocationModel Locations)
        {
            WriteLog.LogInformation(LogMessages.SyncHotelStarted);

            try
            {
                var Response = syncBL.SyncLocations(Locations);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.SyncHotelEnded);

            }
        }

        [HttpPost]
        [Route("syncrestaurant")]
        public IHttpActionResult SyncRestaurant(MigrationResaurantModel Restaurants)
        {
            WriteLog.LogInformation(LogMessages.SyncHotelStarted);

            try
            {
                var Response = syncBL.SyncRestaurants(Restaurants);
                return Ok(Response);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.SyncHotelEnded);

            }
        }

        [Route("migrateairport")]
        [HttpPost]
        public IHttpActionResult MigrateAirport(List<AirportModel> sourceTable)
        {
            WriteLog.LogInformation(LogMessages.MigrateAirportStarted);
            try
            {
                var result = this.syncBL.MigrateAirport(sourceTable);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.MigrateAirportEnded);
            }

        }

        [Route("migratecountry")]
        [HttpPost]
        public IHttpActionResult MigrateCountry(List<CountryModel> sourceTable)
        {
            WriteLog.LogInformation(LogMessages.MigrateCountryStarted);
            try
            {
                var result = this.syncBL.MigrateCountry(sourceTable);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.MigrateCountryEnded);
            }
        }

        [Route("migratehoteltype")]
        [HttpPost]
        public IHttpActionResult MigrateHotelType(List<HotelTypeModel> sourceTable)
        {
            WriteLog.LogInformation(LogMessages.MigrateHotelTypeStarted);
            try
            {
                var result = this.syncBL.MigrateHotelType(sourceTable);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.MigrateHotelTypeEnded);
            }
        }


        [Route("migraterestauranttype")]
        [HttpPost]
        public IHttpActionResult MigrateRestaurantType(List<RestaurantTypeModel> sourceTable)
        {
            WriteLog.LogInformation(LogMessages.MigrateRestaurantTypeStarted);
            try
            {
                var result = this.syncBL.MigrateRestaurantType(sourceTable);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.MigrateRestaurantTypeEnded);
            }
        }

        [Route("migratelocationtype")]
        [HttpPost]
        public IHttpActionResult MigrateLocationType(List<LocationTypeModel> sourceTable)
        {
            WriteLog.LogInformation(LogMessages.MigrateLocationTypeStarted);
            try
            {
                var result = this.syncBL.MigrateLocationType(sourceTable);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.MigrateLocationTypeEnded);
            }
        }

        [Route("migratekitchen")]
        [HttpPost]
        public IHttpActionResult MigrateKitchen(List<KitchenModel> sourceTable)
        {
            WriteLog.LogInformation(LogMessages.MigrateKitchenStarted);
            try
            {
                var result = this.syncBL.MigrateKitchen(sourceTable);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.MigrateKitchenEnded);
            }
        }


        [Route("migratesuitablefor")]
        [HttpPost]
        public IHttpActionResult MigrateSuitablefor(List<SuitableForModel> sourceTable)
        {
            WriteLog.LogInformation(LogMessages.MigrateSuitableforStarted);
            try
            {
                var result = this.syncBL.MigrateSuitablefor(sourceTable);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.MigrateSuitableforEnded);
            }
        }


        [Route("deleteresource")]
        [HttpPost]
        public IHttpActionResult DeleteResource(List<KbStatusModel> sourceTable)
        {
            WriteLog.LogInformation(LogMessages.DeleteResourceStarted);
            try
            {
                var result = this.syncBL.DeleteResource(sourceTable);
                return Ok(result);
            }
            finally
            {
                WriteLog.LogInformation(LogMessages.DeleteResourceEnded);
            }
        }


    }
}