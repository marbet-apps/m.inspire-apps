﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Entities
{
    public class HotelType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [ForeignKey("Hotels")]
        public int dcp_hotelinfo_id { get; set; }
        [Required]
        [ForeignKey("HotelTypes")]
        public int dcp_hoteltype_id { get; set; }

        public Hotels Hotels { get; set; }
        public List_HotelType HotelTypes { get; set; }
    }
}
