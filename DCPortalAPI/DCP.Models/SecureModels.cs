﻿using DCP.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace DCP.Models
{
    /// <summary>
    /// At the time of validate Credential by request
    /// </summary>
    public class CredentialRequest
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.EmailRequired)]
        [EmailAddress(ErrorMessage = ValidationMessages.EmailInvalid)]
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.PasswordRequired)]
        public string Password { get; set; }
        //[Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.UserTypeRequired)]
        //[Range(0, 1, ErrorMessage = ValidationMessages.UserTypeInvalid)]
        //public int UserType { get; set; }
    }


    public class AdminCredentialRequest
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.EmailRequired)]
        [EmailAddress(ErrorMessage = ValidationMessages.EmailInvalid)]
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.PasswordRequired)]
        public string Password { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.UserTypeRequired)]
        [Range(0, 1, ErrorMessage = ValidationMessages.UserTypeInvalid)]
        public int UserType { get; set; }
    }

    /// <summary>
    /// Send user object
    /// </summary>
    public class UserModel
    {
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        [JsonIgnore]
        public bool IsDelete { get; set; }
        [JsonIgnore]
        public int UserID { get; set; }
        public int User_Type { get; set; }
        public DateTime? Last_AccessDate { get; set; }

        public string Token { get; set; }
    }

    public class UserRights
    {
        public bool? IsHotel { get; set; }
        public bool? IsLocation { get; set; }
        public bool? IsRestaurant { get; set; }
        public bool? IsManageUser { get; set; }
    }
        
    public class ResponseUserLoginModel : UserRights
    {
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public int LoggedInUserID { get; set; }
        public int User_Type { get; set; }
        public string Last_AccessDate { get; set; }
        [JsonIgnore]
        public bool IsDelete { get; set; }

        //Start User photo changes  15 april
        public byte[] Photo { get; set; }
    }
    public class AdminUserModel
    {
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public int User_Type { get; set; }
        public DateTime? Last_AccessDate { get; set; }
    }
    public class RegisterationModel
    {
        public int Id { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool Isself_Created { get; set; }
        public DateTime Created_date { get; set; }
    }

    public class ChangePasswordModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.EmailRequired)]
        [EmailAddress(ErrorMessage = ValidationMessages.EmailInvalid)]
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.OldPasswordRequired)]
        public string OldPassword { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = ValidationMessages.NewPasswordRequired)]
        public string NewPassword { get; set; }

        public string First_Name { get; set; }

        public string Last_Name { get; set; }
    }

    public class ResetPasswordModel
    {
       
        public int Id { get; set; }
                      
        public string NewPassword { get; set; }

        public string IdValue { get; set; }

        public string RequestedDate { get; set; }

        public string RequestSendForActivateUser { get; set; }

       
    }



}
