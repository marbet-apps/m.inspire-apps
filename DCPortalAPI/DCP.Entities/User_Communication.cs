﻿using System;
using System.ComponentModel;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace DCP.Entities
{
    public class User_Communication
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("user_Masters")]
        public int User_Master_Id { get; set; }

        [MaxLength(4000), Column(TypeName = "varchar")]
        public string Street { get; set; }

        [MaxLength(20), Column(TypeName = "varchar")]
        public string PostCode { get; set; }

        [MaxLength(100), Column(TypeName = "varchar")]
        public string Place { get; set; }

        //[MaxLength(50), Column(TypeName = "varchar")]
        //public string Country_Code { get; set; }
        public int Country_Id { get; set; }
        [MaxLength(50), Column(TypeName = "varchar")]
        public string Telephone { get; set; }

        [MaxLength(20), Column(TypeName = "varchar")]
        public string Tax_Indentifier_No { get; set; }

        public int? created_by { get; set; }
        public DateTime? created_date { get; set; }
        public int? modified_by { get; set; }
        public DateTime? modify_date { get; set; }

        public User_Master user_Masters { get; set; }

    }
}
