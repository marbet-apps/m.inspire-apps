﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace DCPortalAPI.Settings
{
    public static class HttpActionResultExtentions
    {
        public static IHttpActionResult AddHeader(this IHttpActionResult result, string name, string values)
            => new HeaderActionResult(result, name, values);

        private class HeaderActionResult : IHttpActionResult
        {
            private readonly IHttpActionResult actionResult;

            private readonly Tuple<string,string> header;

            public HeaderActionResult(IHttpActionResult actionResult, string headerName, string headerValues)
            {
                this.actionResult = actionResult;

                header = Tuple.Create(headerName, headerValues);
            }

            public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                var response = await actionResult.ExecuteAsync(cancellationToken);

                response.Headers.Add(header.Item1, header.Item2);

                return response;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class CommonMethod
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="headers"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetHeaders(System.Net.Http.Headers.HttpRequestHeaders headers, string type)
        {
            if (headers.Contains(type))
            {
                return headers.GetValues(type).First();
            }
            return "";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static string GetLanguageHeader(System.Net.Http.Headers.HttpRequestHeaders headers)
        {
            return GetHeaders(headers, "Lang");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static int GetUserIdHeader(System.Net.Http.Headers.HttpRequestHeaders headers)
        {
            var userId = GetHeaders(headers, "UserId");
            return !string.IsNullOrWhiteSpace(userId) ? Convert.ToInt32(userId) : 0;
        }
    }

}