﻿
var $search = {
    loadleftfilter: function () {

        $($c.controls.PartialLeftFilter).each(function (i, e) {

            var url = $(e).data("url");

            if (url && url.length > 0) {
                $(e).load(url);
            }

        });

    },
    loadmiddlecontent: function () {

        $($c.controls.PartialMiddleContent).each(function (i, e) {

            var url = $(e).data("url");

            if (url && url.length > 0) {
                $(e).load(url);
            }

        });

        $($c.controls.drawcontents + " div").html('');



    },
    gatherprimaryinfo: function () {

        // Country
        $v.variables.multicountry = $($c.controls.ddlcountry).select2('data');

        // City
        $v.variables.city = $($c.controls.txtcity).val();

        $v.variables.ddlradius = $('#ddlradius').select2('data');

        // Star Info
        $(".fl-group__btn--stars-active").each(function () {
            $v.variables.stars.push($(this).attr("data-star"));
        });

        // No of rooms
        $v.variables.totalroom = $($c.controls.rstotalroom).val();

        // No of conference rooms
        $v.variables.totalcroom = $($c.controls.rstotalconfroom).val();

        // size of conference rooms
        $v.variables.sizecroom = $($c.controls.rsconfsize).val();

        // Hotel Type
        $v.variables.multihoteltype = $($c.controls.multihoteltype).select2('data');//$($c.controls.multihoteltype).val();

        // No of rooms
        $v.variables.Rtotalroom = $($c.controls.Rrstotalroom).val();

        // Size of largest room
        $v.variables.Rsizecroom = $($c.controls.Rrsroomsize).val();

        // Capacity of largest room
        $v.variables.roomcapacity = $($c.controls.Rrsroomcapacity).val();

        // Capacity of restaurants
        $v.variables.restaurantcapacity = $($c.controls.Rrsrestaurantcapacity).val();

        // Kitchen
        $v.variables.multikitchen = $($c.controls.multirestaurantkitchen).select2('data');

        // Restaurant Type
        $v.variables.multirestauranttype = $($c.controls.multirestauranttype).select2('data');

        // Suitable For R
        $v.variables.multisuitableforR = $($c.controls.multirestaurantsuitablefor).select2('data');

        // No of conf rooms
        $v.variables.Ltotalcroom = $($c.controls.Lrstotalconfroom).val();

        // Size of largest conf room
        $v.variables.Lsizecroom = $($c.controls.Lrsconfsize).val();

        // Room height
        $v.variables.roomheight = $($c.controls.Lrsroomheight).val();

        // Rental cost
        $v.variables.rentalcost = $($c.controls.Lrsrentalcost).val();

        // Location Type
        $v.variables.multilocationtype = $($c.controls.multilocationtype).select2('data');

        // Suitable For L
        $v.variables.multisuitableforL = $($c.controls.multilocationsuitablefor).select2('data');

        $v.variables.IsHotel = $($c.controls.chkhotel).is(":checked");
        $v.variables.IsRestaurant = $($c.controls.chkrestaurant).is(":checked");
        $v.variables.IsLocation = $($c.controls.chklocation).is(":checked");

    },
    setSliderValueIntoSearchTextBox(ctrl) {
        var val = $(ctrl).val();
        var minval = val.split(",")[0];
        var maxval = val.split(",")[1];

        $(ctrl).next("div").find("div").find(".lowValue").val(minval);
        $(ctrl).next("div").find("div").find(".highValue").val(maxval);

    },
    applyFromHistory: function () {
        //set from history
        var searchCriteria = $v.variables.historyCriteria;

        $v.variables.ddlradius = searchCriteria.distance;
        $v.variables.txtdestinationname = searchCriteria.name;
        if ($v.variables.ddlradius != null && $v.variables.txtdestinationname != null && $v.variables.txtdestinationname != "") {
            $($c.controls.liSearchDestination).tab('show');

            $('#searchCountryContent').removeClass("active");
            $('#searchDestinationContent').addClass("active");

            if (searchCriteria.selectedDistanceList != null && searchCriteria.selectedDistanceList != "") {
            var list = JSON.parse(searchCriteria.selectedDistanceList);
                if (list != null && list.length > 0) {
                    $('#ddlradius').val($v.variables.ddlradius);
                    $('#ddlradius').data().select2.updateSelection(list[0]);
                }
            }
            $($c.controls.txtdestinationname).val(searchCriteria.name);
            $($c.controls.txtdestinationname).attr("data-lat", searchCriteria.lat);
            $($c.controls.txtdestinationname).attr("data-lng", searchCriteria.lon);
        } else {
            // Country
            $v.variables.multicountry = searchCriteria.country;
            if ($v.variables.multicountry != null && $v.variables.multicountry != "") {
                $($c.controls.ddlcountry).data().select2.updateSelection(JSON.parse($v.variables.multicountry));
            }

            // City
            $v.variables.city = searchCriteria.city;
            $($c.controls.txtcity).val($v.variables.city);
        }

        // Star Info
        $v.variables.stars = searchCriteria.HFilterModel.stars.split(",")

        $(".fl-btnlg").each(function () {
            if ($v.variables.stars.indexOf($(this).attr("data-star")) != -1) {
                $bind.StarE(this, false);
            }
        });


        // No of rooms
        $v.variables.totalroom = searchCriteria.HFilterModel.htotalroom;
        $($c.controls.rslftotalroom).jRange('setValue', $v.variables.totalroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.rslftotalroom));

        // No of conference rooms
        $v.variables.totalcroom = searchCriteria.HFilterModel.htotalcroom;
        $($c.controls.rslftotalconfroom).jRange('setValue', $v.variables.totalcroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.rslftotalconfroom));

        // size of conference rooms
        $v.variables.sizecroom = searchCriteria.HFilterModel.hsizecroom;
        $($c.controls.rslfconfsize).jRange('setValue', $v.variables.sizecroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.rslfconfsize));

        // Parking Spaces
        $v.variables.parkingspace = searchCriteria.HFilterModel.hparkingspace;
        $($c.controls.rslfparkingspace).jRange('setValue', $v.variables.parkingspace);
        $search.setSliderValueIntoSearchTextBox($($c.controls.rslfparkingspace));

        $($c.controls.txthours).val(searchCriteria.HFilterModel.airporthours);
        $($c.controls.txtminutes).val(searchCriteria.HFilterModel.airportminutes);

        // Hotel Type
        $v.variables.multihoteltype = searchCriteria.HFilterModel.typeids;
        if ($v.variables.multihoteltype != null && $v.variables.multihoteltype != "") {
            $($c.controls.multihoteltypei).data().select2.updateSelection(JSON.parse($v.variables.multihoteltype));   
        }

        // Hotel chain
        $v.variables.multichain = searchCriteria.HFilterModel.hotelchains;
        if ($v.variables.multichain != null && $v.variables.multichain != "") {
            $($c.controls.multihotelchaini).data().select2.updateSelection(JSON.parse($v.variables.multichain));
        }

        // No of rooms
        $v.variables.Rtotalroom = searchCriteria.RFilterModel.rtotalroom;
        $($c.controls.Rrslftotalroom).jRange('setValue', $v.variables.Rtotalroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslftotalroom));

        // Size of largest room
        $v.variables.Rsizecroom = searchCriteria.RFilterModel.rsizecroom;
        $($c.controls.Rrslfroomsize).jRange('setValue', $v.variables.Rsizecroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfroomsize));

        // Capacity of largest room
        $v.variables.roomcapacity = searchCriteria.RFilterModel.rroomcapacity;
        $($c.controls.Rrslfroomcapacity).jRange('setValue', $v.variables.roomcapacity);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfroomcapacity));

        // Capacity of restaurants
        $v.variables.restaurantcapacity = searchCriteria.RFilterModel.rrestaurantcapacity;
        $($c.controls.Rrslfrestaurantcapacity).jRange('setValue', $v.variables.restaurantcapacity);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfrestaurantcapacity));

        // Kitchen
        $v.variables.multikitchen = searchCriteria.RFilterModel.kids;
        if ($v.variables.multikitchen != null && $v.variables.multikitchen != "") {
            $($c.controls.multirestaurantkitcheni).data().select2.updateSelection(JSON.parse($v.variables.multikitchen));
        }
        //$LookUp.ILookUpWithSelected($c.controls.multirestaurantkitcheni, $u.path.GetKitchenList, searchCriteria.RFilterModel.kids);

        // Restaurant Type
        $v.variables.multirestauranttype = searchCriteria.RFilterModel.typeids;
        if ($v.variables.multirestauranttype != null && $v.variables.multirestauranttype != "") {
            $($c.controls.multirestauranttypei).data().select2.updateSelection(JSON.parse($v.variables.multirestauranttype));
        }
        //$LookUp.ILookUpWithSelected($c.controls.multirestauranttypei, $u.path.GetRestaurantTypeList, searchCriteria.RFilterModel.typeids);

        // Suitable For R
        $v.variables.multisuitableforR = searchCriteria.RFilterModel.suitableids;
        if ($v.variables.multisuitableforR != null && $v.variables.multisuitableforR != "") {
            $($c.controls.multirestaurantsuitablefori).data().select2.updateSelection(JSON.parse($v.variables.multisuitableforR));
        }
        //$LookUp.ILookUpWithSelected($c.controls.multirestaurantsuitablefori, $u.path.GetSuitableForList, searchCriteria.RFilterModel.suitableids);
        // Minimum turnover
        $v.variables.minimumturnover = searchCriteria.RFilterModel.rminimumturnover;
        if ($v.variables.minimumturnover != null && $v.variables.minimumturnover != "") {
            $($c.controls.Rrslfminimumturnover).jRange('setValue', $v.variables.minimumturnover);
            $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfminimumturnover));
        }
        // Checkbox events
        if (searchCriteria.RFilterModel.chkoutdoor != null && searchCriteria.RFilterModel.chkoutdoor == "true") {
            $($c.controls.yes_radioOP).prop('checked', true);
            $($c.controls.maybe_radioOP).prop('checked', false);
            $($c.controls.no_radioOP).prop('checked', false);
        } else if (searchCriteria.RFilterModel.chkoutdoor != null && searchCriteria.RFilterModel.chkoutdoor == "false") {
            $($c.controls.yes_radioOP).prop('checked', false);
            $($c.controls.maybe_radioOP).prop('checked', false);
            $($c.controls.no_radioOP).prop('checked', true);
        }

        if (searchCriteria.RFilterModel.chkrestinhotel != null && searchCriteria.RFilterModel.chkrestinhotel == "true") {
            $($c.controls.yes_radioRIH).prop('checked', true);
            $($c.controls.maybe_radioRIH).prop('checked', false);
            $($c.controls.no_radioRIH).prop('checked', false);
        } else if (searchCriteria.RFilterModel.chkrestinhotel != null && searchCriteria.RFilterModel.chkrestinhotel == "false") {
            $($c.controls.yes_radioRIH).prop('checked', false);
            $($c.controls.maybe_radioRIH).prop('checked', false);
            $($c.controls.no_radioRIH).prop('checked', true);
        }


        // No of conf rooms
        $v.variables.Ltotalcroom = searchCriteria.LFilterModel.ltotalcroom;
        $($c.controls.Lrslftotalconfroom).jRange('setValue', $v.variables.Ltotalcroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslftotalconfroom));

        // Size of largest conf room
        $v.variables.Lsizecroom = searchCriteria.LFilterModel.lsizecroom;
        $($c.controls.Lrslfconfsize).jRange('setValue', $v.variables.Lsizecroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslfconfsize));

        // Room height
        $v.variables.roomheight = searchCriteria.LFilterModel.lroomheight;
        $($c.controls.Lrslfroomheight).jRange('setValue', $v.variables.roomheight);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslfroomheight));

        // Rental cost
        $v.variables.rentalcost = searchCriteria.LFilterModel.lrentalcost;
        $($c.controls.Lrslfrentalcost).jRange('setValue', $v.variables.rentalcost);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslfrentalcost));

        // Location Type
        $v.variables.multilocationtype = searchCriteria.LFilterModel.typeids;
        if ($v.variables.multilocationtype != null && $v.variables.multilocationtype != "") {
            $($c.controls.multilocationtypei).data().select2.updateSelection(JSON.parse($v.variables.multilocationtype));
        }
        //$LookUp.ILookUpWithSelected($c.controls.multilocationtypei, $u.path.GetSuitableForList, searchCriteria.RFilterModel.suitableids);

        // Suitable For L
        $v.variables.multisuitableforL = searchCriteria.LFilterModel.suitableids;
        if ($v.variables.multisuitableforL != null && $v.variables.multisuitableforL != "") {
            $($c.controls.multilocationsuitablefori).data().select2.updateSelection(JSON.parse($v.variables.multisuitableforL));
        }

        //Checkbox events

        if (searchCriteria.LFilterModel.chkoutdoor != null && searchCriteria.LFilterModel.chkoutdoor == "true") {
            $($c.controls.yes_radioOP).prop('checked', true);
            $($c.controls.maybe_radioOP).prop('checked', false);
            $($c.controls.no_radioOP).prop('checked', false);
        } else if (searchCriteria.LFilterModel.chkoutdoor != null && searchCriteria.LFilterModel.chkoutdoor == "false") {
            $($c.controls.yes_radioOP).prop('checked', false);
            $($c.controls.maybe_radioOP).prop('checked', false);
            $($c.controls.no_radioOP).prop('checked', true);
        }

        if (searchCriteria.LFilterModel.chkCP != null && searchCriteria.LFilterModel.chkCP == "true") {
            $($c.controls.yes_radioCP).prop('checked', true);
            $($c.controls.maybe_radioCP).prop('checked', false);
            $($c.controls.no_radioCP).prop('checked', false);
        } else if (searchCriteria.LFilterModel.chkCP != null && searchCriteria.LFilterModel.chkCP == "false") {
            $($c.controls.yes_radioCP).prop('checked', false);
            $($c.controls.maybe_radioCP).prop('checked', false);
            $($c.controls.no_radioCP).prop('checked', true);
        }

        if (searchCriteria.LFilterModel.chkTP != null && searchCriteria.LFilterModel.chkTP == "true") {
            $($c.controls.yes_radioTP).prop('checked', true);
            $($c.controls.maybe_radioTP).prop('checked', false);
            $($c.controls.no_radioTP).prop('checked', false);
        } else if (searchCriteria.LFilterModel.chkTP != null && searchCriteria.LFilterModel.chkTP == "false") {
            $($c.controls.yes_radioTP).prop('checked', false);
            $($c.controls.maybe_radioTP).prop('checked', false);
            $($c.controls.no_radioTP).prop('checked', true);
        }

        if (searchCriteria.LFilterModel.chkCD != null && searchCriteria.LFilterModel.chkCD == "true") {
            $($c.controls.yes_radioCD).prop('checked', true);
            $($c.controls.maybe_radioCD).prop('checked', false);
            $($c.controls.no_radioCD).prop('checked', false);
        } else if (searchCriteria.LFilterModel.chkCD != null && searchCriteria.LFilterModel.chkCD == "false") {
            $($c.controls.yes_radioCD).prop('checked', false);
            $($c.controls.maybe_radioCD).prop('checked', false);
            $($c.controls.no_radioCD).prop('checked', true);
        }


        // Include chk
        $v.variables.IsHotel = searchCriteria.ishotel;
        $v.variables.IsRestaurant = searchCriteria.isrestaurant;
        $v.variables.IsLocation = searchCriteria.islocation;
        $($c.controls.chkihotel).prop('checked', $v.variables.IsHotel);
        $($c.controls.chkirestaurant).prop('checked', $v.variables.IsRestaurant);
        $($c.controls.chkilocation).prop('checked', $v.variables.IsLocation);


    },
    applyprimaryinfo: function () {

        // Country
        $($c.controls.ddlcountry).data().select2.updateSelection($v.variables.multicountry);

        // Country
        $($c.controls.txtcity).val($v.variables.city);

        // Star Info
        $(".fl-btnlg").each(function () {
            if ($v.variables.stars.indexOf($(this).attr("data-star")) != -1) {
                $bind.StarE(this, false);
            }
        });


        // No of rooms
        $($c.controls.rslftotalroom).jRange('setValue', $v.variables.totalroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.rslftotalroom));


        // No of conference rooms
        $($c.controls.rslftotalconfroom).jRange('setValue', $v.variables.totalcroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.rslftotalconfroom));

        // size of conference rooms
        $($c.controls.rslfconfsize).jRange('setValue', $v.variables.sizecroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.rslfconfsize));

        // Hotel Type
        $($c.controls.multihoteltypei).data().select2.updateSelection($v.variables.multihoteltype);

        // No of rooms

        $($c.controls.Rrslftotalroom).jRange('setValue', $v.variables.Rtotalroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslftotalroom));

        // Size of largest room

        $($c.controls.Rrslfroomsize).jRange('setValue', $v.variables.Rsizecroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfroomsize));

        // Capacity of largest room

        $($c.controls.Rrslfroomcapacity).jRange('setValue', $v.variables.roomcapacity);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfroomcapacity));

        // Capacity of restaurants
        $($c.controls.Rrslfrestaurantcapacity).jRange('setValue', $v.variables.restaurantcapacity);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfrestaurantcapacity));

        // Kitchen
        $($c.controls.multirestaurantkitcheni).data().select2.updateSelection($v.variables.multikitchen);

        // Restaurant Type
        $($c.controls.multirestauranttypei).data().select2.updateSelection($v.variables.multirestauranttype);

        // Suitable For R
        $($c.controls.multirestaurantsuitablefori).data().select2.updateSelection($v.variables.multisuitableforR);

        // No of conf rooms
        $($c.controls.Lrslftotalconfroom).jRange('setValue', $v.variables.Ltotalcroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslftotalconfroom));

        // Size of largest conf room
        $($c.controls.Lrslfconfsize).jRange('setValue', $v.variables.Lsizecroom);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslfconfsize));

        // Room height
        $($c.controls.Lrslfroomheight).jRange('setValue', $v.variables.roomheight);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslfroomheight));

        // Rental cost
        $($c.controls.Lrslfrentalcost).jRange('setValue', $v.variables.rentalcost);
        $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslfrentalcost));

        // Location Type
        $($c.controls.multilocationtypei).data().select2.updateSelection($v.variables.multilocationtype);

        // Suitable For R
        $($c.controls.multilocationsuitablefori).data().select2.updateSelection($v.variables.multisuitableforL);

        // Include chk
        $($c.controls.chkihotel).prop('checked', $v.variables.IsHotel);
        $($c.controls.chkirestaurant).prop('checked', $v.variables.IsRestaurant);
        $($c.controls.chkilocation).prop('checked', $v.variables.IsLocation);

        $('#ddlradius').data().select2.updateSelection($v.variables.ddlradius);

        $bind.ShowHideLeftPanel();



    },
    resetcontrols: function (from, type) {

        $v.variables.NeedSearch = false;

        if (from == "O") {

            if (type == "A") {
                // Country
                $($c.controls.ddlcountry).select2("val", "");

                // City
                $($c.controls.txtcity).val("");

                // Destination
                $($c.controls.txtdestinationname).val("");
                $($c.controls.txtdestinationname).removeAttr("data-lng");
                $($c.controls.txtdestinationname).removeAttr("data-lat");

                // Radius
                $($c.controls.ddlradius).select2("val", "");

                // Include chk
                $($c.controls.chkhotel).prop('checked', true);
                $($c.controls.chkrestaurant).prop('checked', true);
                $($c.controls.chklocation).prop('checked', true);

                $validation.ValidateRights();

                $bind.ShowHideBottomPanel();
            }

            if (type == "A" || type == "H") {

                // Stars
                $v.variables.stars = [];
                $(".fl-btnsm").each(function () {
                    $(this).removeClass('fl-group__btn--stars-active').addClass('fl-group__btn--stars');
                });

                // No of rooms
                $v.variables.totalroom = "0,6000";
                $($c.controls.rstotalroom).jRange('setValue', $v.variables.totalroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.rstotalroom));

                // No of conference rooms
                $v.variables.totalcroom = "0,700";
                $($c.controls.rstotalconfroom).jRange('setValue', $v.variables.totalcroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.rstotalconfroom));

                // size of conference rooms
                $v.variables.sizecroom = "0,15000";
                $($c.controls.rsconfsize).jRange('setValue', $v.variables.sizecroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.rsconfsize));

                // Hotel Type
                $($c.controls.multihoteltype).select2("val", "");

            }

            // Restaurant 
            if (type == "A" || type == "R") {

                // No of rooms
                $v.variables.Rtotalroom = "0,40";
                $($c.controls.Rrstotalroom).jRange('setValue', $v.variables.Rtotalroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Rrstotalroom));

                // Size of largest room
                $v.variables.Rsizecroom = "0,3000";
                $($c.controls.Rrsroomsize).jRange('setValue', $v.variables.Rsizecroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Rrsroomsize));

                // Capacity of largest room
                $v.variables.roomcapacity = "0,15000";
                $($c.controls.Rrsroomcapacity).jRange('setValue', $v.variables.roomcapacity);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Rrsroomcapacity));

                // Capacity of restaurants
                $v.variables.restaurantcapacity = "0,3000";
                $($c.controls.Rrsrestaurantcapacity).jRange('setValue', $v.variables.restaurantcapacity);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Rrsrestaurantcapacity));

                // Kitchen
                $($c.controls.multirestaurantkitchen).select2("val", "");

                // Restaurant Type
                $($c.controls.multirestauranttype).select2("val", "");

                // Suitable For R
                $($c.controls.multirestaurantsuitablefor).select2("val", "");
            }

            // Location
            if (type == "A" || type == "L") {

                // No of conf rooms
                $v.variables.Ltotalcroom = "0,220";
                $($c.controls.Lrstotalconfroom).jRange('setValue', $v.variables.Ltotalcroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Lrstotalconfroom));

                // Size of largest conf room
                $v.variables.Lsizecroom = "0,86000";
                $($c.controls.Lrsconfsize).jRange('setValue', $v.variables.Lsizecroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Lrsconfsize));

                // Room height
                $v.variables.roomheight = "0,50";
                $($c.controls.Lrsroomheight).jRange('setValue', $v.variables.roomheight);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Lrsroomheight));

                // Rental cost
                $v.variables.rentalcost = "0,200000";
                $($c.controls.Lrsrentalcost).jRange('setValue', $v.variables.rentalcost);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Lrsrentalcost));

                // Location Type
                $($c.controls.multilocationtype).select2("val", "");

                // Suitable For L
                $($c.controls.multilocationsuitablefor).select2("val", "");

            }

        } else {

            if (type == "A") {

                // Country
                $($c.controls.ddlcountry).select2("val", "");

                // City
                $($c.controls.txtcity).val("");

            }

            // Hotels

            if (type == "H" || type == "A") {

                // Stars
                $v.variables.stars = [];
                $(".fl-btnlg").each(function () {
                    $(this).removeClass('fl-group__btn--stars-active').addClass('fl-group__btn--stars');
                });

                // No of rooms
                $v.variables.totalroom = "0,6000";
                $($c.controls.rslftotalroom).jRange('setValue', $v.variables.totalroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.rslftotalroom));

                // No of conference rooms
                $v.variables.totalcroom = "0,700";
                $($c.controls.rslftotalconfroom).jRange('setValue', $v.variables.totalcroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.rslftotalconfroom));

                // size of conference rooms
                $v.variables.sizecroom = "0,15000";
                $($c.controls.rslfconfsize).jRange('setValue', $v.variables.sizecroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.rslfconfsize));

                // Parking Spaces
                $v.variables.parkingspace = "0,1000";
                $($c.controls.rslfparkingspace).jRange('setValue', $v.variables.parkingspace);
                $search.setSliderValueIntoSearchTextBox($($c.controls.rslfparkingspace));

                // Surrounding Location
                $($c.controls.txtdistance).val("10");
                $($c.controls.txtlocation).val("");

                // Transfer Time from Airport

                $($c.controls.txthours).val("0");
                $($c.controls.txtminutes).val("0");

                // Chain
                $($c.controls.multihotelchaini).select2("val", "");

                // Hotel Type
                $($c.controls.multihoteltypei).select2("val", "");
            }

            // Locations

            if (type == "L" || type == "A") {

                // No of conf rooms
                $v.variables.Ltotalcroom = "0,220";
                $($c.controls.Lrslftotalconfroom).jRange('setValue', $v.variables.Ltotalcroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslftotalconfroom));

                // Size of largest conf room
                $v.variables.Lsizecroom = "0,86000";
                $($c.controls.Lrslfconfsize).jRange('setValue', $v.variables.Lsizecroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslfconfsize));

                // Room height
                $v.variables.roomheight = "0,50";
                $($c.controls.Lrslfroomheight).jRange('setValue', $v.variables.roomheight);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslfroomheight));

                // Rental cost
                $v.variables.rentalcost = "0,200000";
                $($c.controls.Lrslfrentalcost).jRange('setValue', $v.variables.rentalcost);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Lrslfrentalcost));

                // Location Type
                $($c.controls.multilocationtypei).select2("val", "");

                // Suitable For L
                $($c.controls.multilocationsuitablefori).select2("val", "");

                // Surrounding Location
                $($c.controls.txtdistance).val("10");
                $($c.controls.txtlocation).val("");

                // Checkbox events
                $("input[name=choice_radiopp][value=B]").prop('checked', true);
                $("input[name=choice_radiocp][value=B]").prop('checked', true);
                $("input[name=choice_radiotp][value=B]").prop('checked', true);
                $("input[name=choice_radiocd][value=B]").prop('checked', true);
            }

            // Restaurants

            if (type == "R" || type == "A") {

                // No of rooms
                $v.variables.Rtotalroom = "0,40";
                $($c.controls.Rrslftotalroom).jRange('setValue', $v.variables.Rtotalroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslftotalroom));

                // Size of largest room
                $v.variables.Rsizecroom = "0,3000";
                $($c.controls.Rrslfroomsize).jRange('setValue', $v.variables.Rsizecroom);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfroomsize));

                // Capacity of largest room
                $v.variables.roomcapacity = "0,15000";
                $($c.controls.Rrslfroomcapacity).jRange('setValue', $v.variables.roomcapacity);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfroomcapacity));

                // Capacity of restaurants
                $v.variables.restaurantcapacity = "0,3000";
                $($c.controls.Rrslfrestaurantcapacity).jRange('setValue', $v.variables.restaurantcapacity);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfrestaurantcapacity));

                // Minimum turnover
                $v.variables.minimumturnover = "0,15000";
                $($c.controls.Rrslfminimumturnover).jRange('setValue', $v.variables.minimumturnover);
                $search.setSliderValueIntoSearchTextBox($($c.controls.Rrslfminimumturnover));

                // Kitchen
                $($c.controls.multirestaurantkitcheni).select2("val", "");

                // Restaurant Type
                $($c.controls.multirestauranttypei).select2("val", "");

                // Suitable For R
                $($c.controls.multirestaurantsuitablefori).select2("val", "");

                // Surrounding Location
                $($c.controls.txtdistance).val("10");
                $($c.controls.txtlocation).val("");

                // Checkbox events
                $("input[name=choice_radioop][value=B]").prop('checked', true);
                $("input[name=choice_radiorih][value=B]").prop('checked', true);
            }

            if (type == "A") {

                $v.variables.IsHotel = true;
                $($c.controls.chkihotel).prop('checked', $v.variables.IsHotel);
                $v.variables.IsRestaurant = true;
                $($c.controls.chkirestaurant).prop('checked', $v.variables.IsRestaurant);
                $v.variables.IsLocation = true;
                $($c.controls.chkilocation).prop('checked', $v.variables.IsLocation);

                $validation.ValidateRights();

                $bind.ShowHideLeftPanel();
            }

        }

        $v.variables.NeedSearch = true;

        if (from == 'I') {
            $search.applysearch(type);
        }
    },
    GetHotelFilters: function (flag) {

        var hours = "";
        var minutes = "";

        $v.variables.stars = [];

        $(".fl-group__btn--stars-active").each(function () {
            $v.variables.stars.push($(this).attr("data-star"));
        });

        $v.variables.multihoteltype = $($c.controls.multihoteltypei).val();
        $v.variables.totalroom = $($c.controls.rslftotalroom).val();
        $v.variables.totalcroom = $($c.controls.rslftotalconfroom).val();
        $v.variables.parkingspace = $($c.controls.rslfparkingspace).val();
        $v.variables.sizecroom = $($c.controls.rslfconfsize).val();

        if (flag) {
            $v.variables.multichain = $($c.controls.multihotelchaini).val();
            hours = $($c.controls.txthours).val();
            minutes = $($c.controls.txtminutes).val();
        }

        var FilterModel = {
            'htotalroom': $v.variables.totalroom,
            'htotalcroom': $v.variables.totalcroom,
            'hsizecroom': $v.variables.sizecroom,
            'typeids': ($v.variables.multihoteltype == null || $v.variables.multihoteltype == "") ? "" : $v.variables.multihoteltype.toString(),
            'airporthours': hours,
            'airportminutes': minutes,
            'stars': $v.variables.stars.join(","),
            'hparkingspace': $v.variables.parkingspace,
            'hotelchains': ($v.variables.multichain == null || $v.variables.multichain == "") ? "" : $v.variables.multichain.toString()
        };

        return FilterModel;

    },
    GetRestaurantFilters: function (flag) {

        var chkoutdoor = "";
        var chkrestinhotel = "";

        $v.variables.multisuitableforR = $($c.controls.multirestaurantsuitablefori).val();
        $v.variables.multirestauranttype = $($c.controls.multirestauranttypei).val();
        $v.variables.multikitchen = $($c.controls.multirestaurantkitcheni).val();
        $v.variables.Rtotalroom = $($c.controls.Rrslftotalroom).val();
        $v.variables.Rsizecroom = $($c.controls.Rrslfroomsize).val();
        $v.variables.roomcapacity = $($c.controls.Rrslfroomcapacity).val();
        $v.variables.restaurantcapacity = $($c.controls.Rrslfrestaurantcapacity).val();
        $v.variables.minimumturnover = $($c.controls.Rrslfminimumturnover).val();

        if (flag) {

            if ($($c.controls.no_radioOP).is(":checked")) {
                chkoutdoor = "false";
            } else if ($($c.controls.yes_radioOP).is(":checked")) {
                chkoutdoor = "true";
            }

            if ($($c.controls.no_radioRIH).is(":checked")) {
                chkrestinhotel = "false";
            } else if ($($c.controls.yes_radioRIH).is(":checked")) {
                chkrestinhotel = "true";
            }

        }

        FilterModel = {
            'kids': ($v.variables.multikitchen == null || $v.variables.multikitchen == "") ? "" : $v.variables.multikitchen.toString(),
            'typeids': ($v.variables.multirestauranttype == null || $v.variables.multirestauranttype == "") ? "" : $v.variables.multirestauranttype.toString(),
            'suitableids': ($v.variables.multisuitableforR == null || $v.variables.multisuitableforR == "") ? "" : $v.variables.multisuitableforR.toString(),
            'rtotalroom': $v.variables.Rtotalroom,
            'rsizecroom': $v.variables.Rsizecroom,
            'rroomcapacity': $v.variables.roomcapacity,
            'rrestaurantcapacity': $v.variables.restaurantcapacity,
            'rminimumturnover': $v.variables.minimumturnover,
            'chkoutdoor': chkoutdoor,
            'chkrestinhotel': chkrestinhotel
        };

        return FilterModel;

    },
    GetLocationFilters: function (flag) {

        $v.variables.multisuitableforL = $($c.controls.multilocationsuitablefori).val();
        $v.variables.Ltotalcroom = $($c.controls.Lrslftotalconfroom).val();
        $v.variables.Lsizecroom = $($c.controls.Lrslfconfsize).val();
        $v.variables.roomheight = $($c.controls.Lrslfroomheight).val();
        $v.variables.rentalcost = $($c.controls.Lrslfrentalcost).val();
        $v.variables.multilocationtype = $($c.controls.multilocationtypei).val();

        var chkoutdoor = "";
        var chkCP = "";
        var chkTP = "";
        var chkCD = "";

        if (flag) {

            if ($($c.controls.no_radioPP).is(":checked")) {
                chkoutdoor = "false";
            } else if ($($c.controls.yes_radioPP).is(":checked")) {
                chkoutdoor = "true";
            }

            if ($($c.controls.no_radioCP).is(":checked")) {
                chkCP = "false";
            } else if ($($c.controls.yes_radioCP).is(":checked")) {
                chkCP = "true";
            }

            if ($($c.controls.no_radioTP).is(":checked")) {
                chkTP = "false";
            } else if ($($c.controls.yes_radioTP).is(":checked")) {
                chkTP = "true";
            }

            if ($($c.controls.no_radioCD).is(":checked")) {
                chkCD = "false";
            } else if ($($c.controls.yes_radioCD).is(":checked")) {
                chkCD = "true";
            }
        }

        FilterModel = {
            'ltotalcroom': $v.variables.Ltotalcroom,
            'lsizecroom': $v.variables.Lsizecroom,
            'lroomheight': $v.variables.roomheight,
            'lrentalcost': $v.variables.rentalcost,
            'typeids': ($v.variables.multilocationtype == null || $v.variables.multilocationtype == "") ? "" : $v.variables.multilocationtype.toString(),
            'suitableids': ($v.variables.multisuitableforL == null || $v.variables.multisuitableforL == "") ? "" : $v.variables.multisuitableforL.toString(),
            'chkoutdoor': chkoutdoor,
            'chkCP': chkCP,
            'chkTP': chkTP,
            'chkCD': chkCD
        };

        return FilterModel;
    },
    applyfilter: function (flag, istemp) {
        SearchModel = {
            'country': flag ? $($c.controls.ddlcountry).val() : $($c.controls.ddlcountry).val(),
            'city': flag ? $($c.controls.txtcity).val() : $($c.controls.txtcity).val(),
            'name': $($c.controls.txtdestinationname).val(),
            'lat': $($c.controls.txtdestinationname).attr("data-lat") != "" ? parseFloat($($c.controls.txtdestinationname).attr("data-lat")) : null,
            'lon': $($c.controls.txtdestinationname).attr("data-lng") != "" ? parseFloat($($c.controls.txtdestinationname).attr("data-lng")) : null,
            'distance': $($c.controls.ddlradius).val(),
            'HFilterModel': $search.GetHotelFilters(flag),
            'RFilterModel': $search.GetRestaurantFilters(flag),
            'LFilterModel': $search.GetLocationFilters(flag),
            'displayRecords': $v.variables.recordsDisplayed,
            'pageSize': 50,
            'sort': $($c.controls.sortby + ".bluefont").attr("data-sort") == undefined ? "resourceasc" : $($c.controls.sortby + ".bluefont").attr("data-sort"),
            'userId': 1,
            'ishotel': $v.variables.IsHotel,
            'isrestaurant': $v.variables.IsRestaurant,
            'islocation': $v.variables.IsLocation,
            'isFromHistory': false
        };

        if ($('#isFromHistory').val() != null && $('#isFromHistory').val() == "1" && $v.variables.historyCriteria != null) {
            SearchModel['isFromHistory'] = true;
            $('#isFromHistory').val("");
        }

        if (istemp) {
            return SearchModel;
        }

        return JSON.stringify(SearchModel);
    },
    RemoveResourceCart: function (event, obj, callee) {
        if (event != null) {
            event.preventDefault();
            event.stopPropagation();
        }
        var resourceId = $(obj).attr("data-id");
        var resourceType = $(obj).attr("data-type");
        $("#" + resourceId).addClass('pointernone');
        var URL = $u.path.RemoveResourceFromTheSelection;

        $.ajax({
            type: "DELETE",
            url: $bind.URL(URL) + '?resourceId=' + resourceId + '&resourceType=' + resourceType + '',
            //data: jsonData,
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (response) {
                $(".paneldiv").removeClass('pointernone');
                $bind.unbindTooltip();
                if (response != undefined && response != null && response.code == "702") {
                    if (callee != undefined && callee === 'infoDetail') {
                        //obj.outerHTML = "<a class='btn btn-primary request-rfp show-tooltip'  data-id='" + resourceId + "'  data-type='" + resourceType + "' id='btnRequestRFP' onClick='$search.AddResourceCart(this,\"infoDetail\");'><i class='glyphicon glyphicon-shopping-cart'></i>&nbsp;" + $($c.controls.hdnadd).val() + "</a>";
                        obj.outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sAddBookmark).val() + "'  data-id='" + resourceId + "'  data-type='" + resourceType + "' id='btnRequestRFP' onClick='$search.AddResourceCart(event, this,\"infoDetail\");'><i class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></a>";
                    }
                    else if ($(obj).parent().closest('div').parent().parent().attr("id") == "drawcontents") {
                        obj.outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sAddBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.AddResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></i> </a> ";
                        var sidemap = $("#sidemap").find(".card [data-id='" + resourceId + "']")[0];
                        if (sidemap != null && sidemap.outerHTML != null) {
                            $("#sidemap").find(".card [data-id='" + resourceId + "']")[0].outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sAddBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.AddResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></i> </a> ";
                        }

                        //obj.outerHTML = "<div class='overlayadd' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.AddResourceCart(this);'><div class='corner-overlayadd-content-remove corner-overlayadd-content-remove-left'><span class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></span></div ></div >";
                        //$("#sidemap").find("div [data-id='" + resourceId + "']")[0].outerHTML = "<div class='overlayadd' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.AddResourceCart(this);'><div class='corner-overlayadd-content-remove corner-overlayadd-content-remove-left'><span class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></span></div ></div >";
                    }
                    else {
                        //    $("div [data-id='" + resourceId + "']")[0].outerHTML = "<div class='overlayadd' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.AddResourceCart(this);'><div class='corner-overlayadd-content-remove corner-overlayadd-content-remove-left'><span class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></span></div ></div >";
                        //    $("#drawcontents").find("div [data-id='" + resourceId + "']")[0].outerHTML = "<div class='overlayadd' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.AddResourceCart(this);'><div class='corner-overlayadd-content-remove corner-overlayadd-content-remove-left'><span class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></span></div ></div >";

                        $(".card [data-id='" + resourceId + "']")[0].outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sAddBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.AddResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></i> </a>";
                        $("#drawcontents").find(".card [data-id='" + resourceId + "']")[0].outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sAddBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.AddResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></i> </a>";
                        var sidemap = $("#sidemap").find(".card [data-id='" + resourceId + "']")[0];
                        if (sidemap != null && sidemap.outerHTML != null) {
                            $("#sidemap").find(".card [data-id='" + resourceId + "']")[0].outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sAddBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.AddResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></i> </a>";
                        }

                    }
                    $managecode.Decision(false, "", response.code, response.message, "Filter", true);
                    $bind.GetCartCount();
                } else {
                    $managecode.Decision(false, "", response.code, response.message, "Filter", false);
                }

            },
            error: function (data, status, error) {
                $(".paneldiv").removeClass('pointernone');
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

    },
    AddResourceCart: function (event, obj, callee) {
        if (event != null) {
            event.preventDefault();
            event.stopPropagation();
        }
        var cartResources = [];
        var resourceId = $(obj).attr("data-id");
        var resourceType = $(obj).attr("data-type");

        $("#" + resourceId).addClass('pointernone');

        var cartResource = {
            ResourceId: resourceId,
            ResourceType: resourceType,
            IsAddedToRFP: false
        };

        cartResources.push(cartResource);

        var requestCart = {
            CartResources: cartResources
        };

        var cURL = $u.path.AddToCart;

        $.ajax({
            type: "POST",
            url: $bind.URL(cURL),
            data: JSON.stringify(requestCart),
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                $bind.unbindTooltip();
                $(".paneldiv").removeClass('pointernone');
                if (response != undefined && response != null && response.code == "701") {
                    if (callee != undefined && callee === 'infoDetail') {
                        // obj.outerHTML = "<a class='btn btn-primary request-rfp show-tooltip'  data-id='" + resourceId + "'  data-type='" + resourceType + "' id='btnRequestRFP' onClick='$search.RemoveResourceCart(this,\"infoDetail\");'><i class='glyphicon glyphicon-shopping-cart'></i>&nbsp;" + $($c.controls.hdnremove).val() + "</a>";
                        obj.outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "'  data-id='" + resourceId + "'  data-type='" + resourceType + "' id='btnRequestRFP' onClick='$search.RemoveResourceCart(event, this,\"infoDetail\");'><i class='glyphicon glyphicon-bookmark bookmarkfont'></i></a>";
                    }
                    else if ($(obj).parent().closest('div').parent().parent().attr("id") == "drawcontents") {
                       // obj.outerHTML = "<div class='overlayadd remove' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.RemoveResourceCart(this);'><div class='corner-overlayadd-content-remove'><span class='glyphicon glyphicon-bookmark bookmarkfont'></span></div ></div >";
                        obj.outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.RemoveResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont'></i> </a>";
                        var sidemap = $("#sidemap").find(".card [data-id='" + resourceId + "']")[0];
                        if (sidemap != null && sidemap.outerHTML != null) {
                            $("#sidemap").find(".card [data-id='" + resourceId + "']")[0].outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.RemoveResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont'></i> </a>";
                        }

                    }
                    else {
                        //$("div [data-id='" + resourceId + "']")[0].outerHTML = "<div class='overlayadd remove' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.RemoveResourceCart(this);'><div class='corner-overlayadd-content-remove'><span class='glyphicon glyphicon-bookmark bookmarkfont'></span></div ></div >";
                        //$("#drawcontents").find("div [data-id='" + resourceId + "']")[0].outerHTML = "<div class='overlayadd remove' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.RemoveResourceCart(this);'><div class='corner-overlayadd-content-remove'><span class='glyphicon glyphicon-bookmark bookmarkfont'></span></div ></div >";
                        $(".card [data-id='" + resourceId + "']")[0].outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.RemoveResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont'></i> </a>";
                        $("#drawcontents").find(".card [data-id='" + resourceId + "']")[0].outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.RemoveResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont'></i> </a>";
                        var sidemap = $("#sidemap").find(".card [data-id='" + resourceId + "']")[0];
                        if (sidemap != null && sidemap.outerHTML != null) {
                            $("#sidemap").find(".card [data-id='" + resourceId + "']")[0].outerHTML = "<a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "' data-id=" + resourceId + " data-type=" + resourceType + " onclick='$search.RemoveResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont'></i> </a>"
                        }
                        
                    }
                    $managecode.Decision(false, "", response.code, response.message, "Filter", true);
                    $bind.GetCartCount();
                } else {
                    $managecode.Decision(false, "", response.code, response.message, "Filter", false);
                }

            },
            error: function (data, status, error) {
                $(".paneldiv").removeClass('pointernone');
                $managecode.Decision(true, data, "", "", "error", false);
                $bind.EFilterLoader();
                $($c.controls.mloader).hide();
            }

        });

    },
    retrievedata: function (flag) {

        $($c.controls.mloader).show();

        var jasonData = $search.applyfilter(flag, false);

        var curl = $u.path.loaddata;

        $.ajax({
            url: $bind.URL(curl),
            data: jasonData,
            type: "POST",
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {

                if (response != undefined && response != null && response.code == "704") {

                    response = response.data;

                    if (response != undefined && response != "" && response != null) {

                        var data = response.ResourceModel;
                        var margin = false;
                        $.each(data, function (i) {

                            if (data.length == (i + 1)) {
                                margin = true;
                            }

                            var htmlcontents = $search.drawpanel(data[i], data[i].type, $v.variables.i, "C", true, false, false);

                            margin = false;

                            $($c.controls.drawcontents).append(htmlcontents);

                            setTimeout(function () { $search.loadimages(data[i].id, data[i].type); }, 100);

                            $v.variables.i = $v.variables.i + 1;

                        });

                        $v.variables.recordsDisplayed = $v.variables.recordsDisplayed + response.ResourceModel.length;
                        $v.variables.totalRecords = response.totalrecord;

                        $($c.controls.lbldisplayed).text($v.variables.recordsDisplayed);
                        $($c.controls.lbltotal).text($v.variables.totalRecords);

                        if ($v.variables.Isopen == 1) {
                            $bind.showdiv();
                        }

                        if ($v.variables.recordsDisplayed != $v.variables.totalRecords && $v.variables.recordsDisplayed != 0) {
                            $($c.controls.drawbutton).show();
                        } else {
                            $($c.controls.drawbutton).hide();
                        }

                        if ($v.variables.totalRecords == 0) {
                            $($c.controls.drawcontents).html("<div id='lblnorecords' class='col-lg-offset-4 col-lg-4 col-md-4 col-sm-4 col-xs-4 ac mt20p'><img src='\\Images\\img_nodata.png'/>&nbsp;<br/><span class='f20'> <b>" + $($c.controls.sNoRecords).val() + "</b> </span></div>");
                        } else {
                            $($c.controls.lblnorecords).remove();
                        }

                    }

                } else {
                    $managecode.Decision(false, "", response.code, response.message, "Filter", false);
                }

                $($c.controls.mloader).hide();
                setTimeout(function () {

                    $('.show-tooltip').tooltip({ placement: 'top' });
                    $('.show-tooltipb').tooltip({ placement: 'bottom' });

                }, 3);

                $bind.EFilterLoader();

                $($c.controls.liNoofRoom).hide();
                $($c.controls.liNoofConference).hide();
                $($c.controls.liSizeofLargestroom).hide();
                $($c.controls.liHotelStar).hide();

                $($c.controls.liCapacityOfRestaurant).hide();
                $($c.controls.liResCapacityOflargestRoom).hide();
                $($c.controls.liResNoofConference).hide();

                $($c.controls.lilocNoofConference).hide();
                $($c.controls.lilocSizeofLargestroom).hide();
                $($c.controls.liRentalCost).hide();

                if ($($c.controls.chkihotel).is(":checked") && $($c.controls.chkirestaurant).is(":checked") == false && $($c.controls.chkilocation).is(":checked") == false) {
                    $($c.controls.liNoofRoom).show();
                    $($c.controls.liNoofConference).show();
                    $($c.controls.liSizeofLargestroom).show();
                    $($c.controls.liHotelStar).show();
                }

                if ($($c.controls.chkirestaurant).is(":checked") == true && $($c.controls.chkihotel).is(":checked") == false && $($c.controls.chkilocation).is(":checked") == false) {
                    $($c.controls.liCapacityOfRestaurant).show();
                    $($c.controls.liResCapacityOflargestRoom).show();
                    $($c.controls.liResNoofConference).show();
                }



                if ($($c.controls.chkilocation).is(":checked") && $($c.controls.chkirestaurant).is(":checked") == false && $($c.controls.chkihotel).is(":checked") == false) {
                    $($c.controls.lilocNoofConference).show();
                    $($c.controls.lilocSizeofLargestroom).show();
                    $($c.controls.liRentalCost).show();
                }


            },
            error: function (data, status, error) {

                $managecode.Decision(true, data, "", "", "error", false);
                $bind.EFilterLoader();
                $($c.controls.mloader).hide();
            }
        });

    },
    drawpanel: function (response, type, num, from, IsMap, IsSmall, IsDelete) {

        var name = response.name;
        var templates = "";
        var href = '/info/GetHotelInfo/' + response.id;
        var src = '\\Images\\hotelT.png';
        var Msrc = '\\Images\\hotel_circle_small.png';
        var resourceName = $($c.controls.hdnhotelLabel).val();
        var address = response.city;

        if (type == "R") {
            href = '/info/GetRestaurantInfo/' + response.id;
            src = '\\Images\\restaurantT.png';
            resourceName = $($c.controls.hdnRestaurantLabel).val();
            Msrc = '\\Images\\restaurant_circle_small.png';
        } else if (type == "L") {
            href = '/info/GetLocationInfo/' + response.id;
            src = '\\Images\\locationT.png';
            Msrc = '\\Images\\location_circle_small.png';
            resourceName = $($c.controls.hdnLocationLabel).val();
        }

        if (response.zipcode != undefined && response.zipcode != '' && response.zipcode != null) {
            address = address + ',' + response.zipcode;
        }

        if (response.country != undefined && response.country != '' && response.country != null) {
            address = address + ',' + response.country;
        }

        var templatesAdd = "<div class='ellipsis'>";

        if (address != undefined && address != '' && address != null) {
            templatesAdd += "<span class='CONTENT11 show-tooltip' title='" + address +"'>" + address + "&nbsp;</span>";
        }

        templatesAdd += "</div>";

        if (IsSmall == true) {

            templates += "<div data-type='" + type + "' class='paneldiv removablediv zoomclass" + $v.variables.i + " Cards' data-card='" + $v.variables.i + "' id='" + response.id + "' >";

            templates += "<div class='card card-5 box defaultBorder' data-type='" + type + "' data-id='" + response.id + "' data-card='" + $v.variables.i + "'";
            if (IsMap == true) {
                templates += "data-url='" + href + "' onclick='$bind.ShowMapIcon(this, " + IsDelete +");'>";
            } else {
                templates += "data-url='" + href + "' onclick='$bind.OpenDetailOnClick(this);'>";
            }

            templates += "<div class='mt-15'>";

            if (IsDelete == true) {
                templates += "<a data-id='" + response.id + "' data-type='" + type + "' class='deleteresource pointer show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "' onClick='$bind.deletecartresource(event, this)'><i class='glyphicon glyphicon-trash deleteResource'></i></a>";
            }


            templates += "<div class='col-lg-2 col-md-3 col-sm-3 col-xs-3 ac p0'>";

            templates += "<img class='fl' src='" + Msrc + "' />";

            templates += " </div>";

            templates += " <div class='col-lg-10 col-md-9 col-sm-9 col-xs-9 p0'>";

            templates += "<div class='hotelname ellipsis'>";

            templates += "<a href='" + href + "' onclick='$bind.ClickStopPropagation(event);' target='_blank' class='CONTENTLink show-tooltip' title='" + name + "'>" + name + "</a>";

            if (response.chainname != undefined && response.chainname != '' && response.chainname != null) {
                templates += '<div class="CONTENT11 show-tooltip" title="' + response.chainname + '">' + response.chainname + '</div>';
            }

            templates += "</div>";

            templates += templatesAdd;

            templates += " </div></div>";

            templates += " <div class='col-lg-12 mt-15 pr0'>";

            templates += "<div class='col-lg-5 col-md-12 col-sm-12 col-xs-12 p0 imagediv' id='imgo" + type + response.id + "' data-url='/Search/loadimages/" + response.id + "'>";


            templates += "<div class='tos-wrapper tos-inline tos-fx-slide tos-fit tos-opening tos-hover tos-opened'>";
            templates += "<div class='tos-slider' style='left: 0%;'>";
            templates += "<div class='tos-slide' style='width: 100%;'>";

            templates += "<div class='loading bborder'>";
            templates += "<div class='loading-bar'></div>";
            templates += "<div class='loading-bar'></div>";
            templates += "<div class='loading-bar'></div>";
            templates += "<div class='loading-bar'></div>";

            templates += "</div></div></div></div></div>";

            templates += "<div class='col-lg-7 col-md-12 col-sm-12 col-xs-12 pr5 contentdiv'>";

            if (type == "H") {

                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sNoofRoom).val() + "'>" + $($c.controls.sNoofRoom).val() + "</div> <div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.htotalroom + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sNoofConference).val() + "'>" + $($c.controls.sNoofConference).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.htotalcroom + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sSizeofLargestroom).val() + "'>" + $($c.controls.sSizeofLargestroom).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + parseFloat(response.hsizecroom).toFixed(2) + "</div>";
                templates += "</div>";

            }
            else if (type == "R") {
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sNoofRoom).val() + "'>" + $($c.controls.sNoofRoom).val() + "</div> <div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.rtotal_conference + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sCapacityOfRoom).val() + "'>" + $($c.controls.sCapacityOfRoom).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.rcapacity_of_largest_room + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sCapacityOfRestaurant).val() + "'>" + $($c.controls.sCapacityOfRestaurant).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.rcapacity + "</div>";
                templates += "</div>";
            }
            else if (type == "L") {
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sNoofConference).val() + "'>" + $($c.controls.sNoofConference).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.ltotal_conference + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sSizeofLargestroom).val() + "'>" + $($c.controls.sSizeofLargestroom).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + parseFloat(response.lsize_of_largest_conference).toFixed(2) + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sRentalCost).val() + "'>" + $($c.controls.sRentalCost).val() + "</div> <div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + parseFloat(response.lrent_begining).toFixed(2) + "</div>";
                templates += "</div>";
            }

            templates += "</div></div></div></div>";

            if (IsMap == true) {

                setTimeout(function () {
                    $search.maphtml(templates, response, type, num, name, "S");
                }, 100);

            }

        } else {

            templates += "<div data-type='" + type + "' class='col-lg-4 col-md-6 col-sm-6 col-xs-12 paneldiv removablediv zoomclass" + $v.variables.i + " Cards' data-card='" + $v.variables.i + "' id='" + response.id + "' >";

            templates += "<div class='card card-5 box defaultBorder' data-id='" + response.id + "' data-type='" + type + "' data-card='" + $v.variables.i + "'";

            if (from == "C" || from == "T") {
                templates += "data-url='" + href + "' onclick='$bind.ShowMapIcon(this, " + IsDelete +");'>";
            } else {
                templates += "data-url='" + href + "' onclick='$bind.OpenDetailOnClick(this);'>";
            }


            templates += "<div class='mt-15' style='position: relative'>";

            if (IsDelete == true) {
                templates += "<a data-id='" + response.id + "' data-type='" + type + "' class='deleteresource pointer show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "' onClick='$bind.deletecartresource(event, this)'><i class='glyphicon glyphicon-trash deleteResource'></i></a>";
            }

            if (response.isincart == true) {
                templates += "<div class='addtocart'> <a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sRemoveBookmark).val() + "' data-id=" + response.id + " data-type=" + type + " onClick='$search.RemoveResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont'></i> </a> </div>";
            }
            else {
                templates += "<div class='addtocart'> <a class='btn draw-borderB request-rfp show-tooltip' title='" + $($c.controls.sAddBookmark).val() + "' data-id=" + response.id + " data-type=" + type + "  onClick='$search.AddResourceCart(event, this);'> <i class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></i> </a> </div>";
            }

            //if (response.isincart == true) {
            //    templates += "<div class='overlayadd remove' data-id=" + response.id + " data-type=" + type + " onClick='$search.RemoveResourceCart(this);'> <div class='corner-overlayadd-content-remove'> <span class='glyphicon glyphicon-bookmark bookmarkfont'></span> </div></div> ";
            //}
            //else {
            //    templates += "<div class='overlayadd' data-id=" + response.id + " data-type=" + type + "  onClick='$search.AddResourceCart(this);'> <div class='corner-overlayadd-content'> <span class='glyphicon glyphicon-bookmark bookmarkfont whitefont'></span> </div></div> ";
            //}

            templates += "<div class='col-lg-2 col-md-3 col-sm-3 col-xs-3 ac p0'>";

            templates += "<img class='fl' src='" + Msrc + "' />";

            templates += " </div>";

            templates += " <div class='col-lg-10 col-md-9 col-sm-9 col-xs-9 p0 card-info-height Mh75'>";

            templates += "<div class='hotelname cardellipsis'>";

            templates += "<a href='" + href + "' onclick='$bind.ClickStopPropagation(event);' target='_blank' class='CONTENTLink show-tooltip' title='" + name + "'>" + name + "</a>";

            if (response.chainname != undefined && response.chainname != '' && response.chainname != null) {
                templates += '<div class="CONTENT11 show-tooltip" title="' + response.chainname + '">' + response.chainname + '</div>';
            }

            templates += "</div>";

            if (type == "H") {


                if (response.stars != undefined && response.stars != '' && response.stars != null && response.stars != 0) {
                    templates += "<div class='ellipsis show-tooltip cansmall'>";

                    for (i = 0; i < parseInt(response.stars); i++) {
                        templates += "<span><svg xmlns='http://www.w3.org/2000/svg' focusable='false' tabindex='-1' width='12' height='12' viewBox='0 0 12 12'><path class='svg-color--primary' fill='#F6AB3F' d='M11.988 5.21c-.052-.275-.27-.488-.545-.534l-3.604-.6L6.63.455C6.542.184 6.287 0 6 0s-.542.184-.632.456L4.16 4.076l-3.603.6c-.275.046-.493.26-.545.533-.052.273.072.55.312.695L3.2 7.63l-1.165 3.493c-.093.28.01.59.25.758.115.08.25.12.382.12.148 0 .295-.05.416-.146L6 9.52l2.917 2.333c.12.098.27.147.416.147.133 0 .267-.04.38-.12.244-.17.346-.478.252-.758L8.8 7.63l2.876-1.725c.24-.144.364-.422.312-.696z'></path></svg></span>&nbsp;";
                    }
                    templates += "</div>";
                }


                templates += templatesAdd;

                if (response.hoteltype != undefined && response.hoteltype != '' && response.hoteltype != null) {

                    templates += "<div class='ellipsis-2-line show-tooltip cansmall' title='" + response.hoteltype + "'>";
                    templates += "<b><span class='smallfont pointer' >" + response.hoteltype + "</span></b>";
                    templates += "</div>";

                }

            } else if (type == "R") {

                templates += templatesAdd;

                if (response.kitchen != undefined && response.kitchen != '' && response.kitchen != null) {

                    templates += "<div class='ellipsis show-tooltip cansmall' title='" + response.kitchen + "'>";
                    templates += "<b><span class='smallfont pointer' >" + response.kitchen + "</span></b>";
                    templates += "</div>";

                }

                if (response.restauranttype != undefined && response.restauranttype != '' && response.restauranttype != null) {

                    templates += "<div class='ellipsis-2-line show-tooltip cansmall' title='" + response.restauranttype + "'>";
                    templates += "<b><span class='smallfont pointer' >" + response.restauranttype + "</span></b>";
                    templates += "</div>";

                }

            } else if (type == "L") {

                templates += templatesAdd;

                if (response.locationtype != undefined && response.locationtype != '' && response.locationtype != null) {

                    templates += "<div class='ellipsis-2-line show-tooltip cansmall' title='" + response.locationtype + "'>";
                    templates += "<b><span class='smallfont pointer '>" + response.locationtype + "</span></b>";
                    templates += "</div>";

                }
            }

            templates += " </div></div>";

            //if (type == "R" && from != "T" && from != "X" && response.chkRIH != null && response.chkRIH == true) {
            //    templates += "<div class='ribbon ribbon-top-left'><span><div class='inhotel'>In-hotel</div></span></div>";
            //}

            templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 largeimage p0 imagediv' id='imgo" + type + response.id + "' data-url='/Search/loadimages/" + response.id + "'>";
            templates += "<div class='tos-wrapper tos-inline tos-fx-slide tos-fit tos-opening tos-hover tos-opened'>";
            templates += "<div class='tos-slider' style='left: 0%;'>";
            templates += "<div class='tos-slide' style='width: 100%;'>";
            templates += "<div class='loading bborder'>";
            templates += "<div class='loading-bar'></div>";
            templates += "<div class='loading-bar'></div>";
            templates += "<div class='loading-bar'></div>";
            templates += "<div class='loading-bar'></div>";
            templates += "</div></div></div></div></div>";

            templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 iconview cansmall'>";

            if (type == "H") {

                templates += ajaxRequest.CheckRights("R") ? "<div class='pointer show-tooltip plr2 modal-buttons col-md-3 col-sm-3 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByRestaurant).val() + "' onclick='$search.openrestaurant(event, this);' data-name='" + name + "' data-id='" + response.id + "'  data-lng='" + response.lng + "' data-lat='" + response.lat + "' id='diar'> <div class='amenities-icon'><img src='\\Images\\nearby_restaurant.png' /></div></div>" : "";
                templates += ajaxRequest.CheckRights("L") ? "<div class='pointer show-tooltip plr2 modal-buttons col-md-3 col-sm-3 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByLocation).val() + "' onclick='$search.openlocation(event, this);' data-name='" + name + "' data-id='" + response.id + "'  data-lng='" + response.lng + "' data-lat='" + response.lat + "' id='dial'> <div class='amenities-icon'><img src='\\Images\\nearby_location.png' /></div></div>" : "";

            } else if (type == "R") {

                templates += ajaxRequest.CheckRights("H") ? "<div class='pointer show-tooltip plr2 modal-buttons col-md-3 col-sm-3 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByHotel).val() + "' onclick='$search.openhotel(event, this);' data-name='" + name + "' data-id='" + response.id + "'  data-lng='" + response.lng + "' data-lat='" + response.lat + "' id='diar'> <div class='amenities-icon'><img  src='\\Images\\nearby_hotel.png' /></div></div>" : "";
                templates += ajaxRequest.CheckRights("L") ? "<div class='pointer show-tooltip plr2 modal-buttons col-md-3 col-sm-3 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByLocation).val() + "' onclick='$search.openlocation(event, this);' data-name='" + name + "' data-id='" + response.id + "'  data-lng='" + response.lng + "' data-lat='" + response.lat + "' id='dial'> <div class='amenities-icon'><img  src='\\Images\\nearby_location.png' /></div></div>" : "";

            } else if (type == "L") {

                templates += ajaxRequest.CheckRights("H") ? "<div class='pointer show-tooltip plr2 modal-buttons col-md-3 col-sm-3 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByHotel).val() + "' onclick='$search.openhotel(event, this);' data-name='" + name + "' data-id='" + response.id + "'  data-lng='" + response.lng + "' data-lat='" + response.lat + "' id='dial'> <div class='amenities-icon'><img  src='\\Images\\nearby_hotel.png' /></div></div>" : "";
                templates += ajaxRequest.CheckRights("R") ? "<div class='pointer show-tooltip plr2 modal-buttons col-md-3 col-sm-3 col-xs-3' data-type='" + type + "' title='" + $($c.controls.sNearByRestaurant).val() + "' onclick='$search.openrestaurant(event, this);' data-name='" + name + "' data-id='" + response.id + "'  data-lng='" + response.lng + "' data-lat='" + response.lat + "' id='diar'> <div class='amenities-icon'><img   src='\\Images\\nearby_restaurant.png' /></div></div>" : "";

            }

            templates += "<div class='pointer show-tooltip plr2 modal-buttons col-md-3 col-sm-3 col-xs-3' title='" + $($c.controls.sViewOnMap).val() + "' data-name='" + name + "'  data-lng='" + response.lng + "' data-lat='" + response.lat + "' data-type='" + type + "' onclick='$search.openmap(event, this);' id='diam'> <div class='amenities-icon'><img  src='\\Images\\mapview.png' /></div></div>";

            if (response.is360degree != undefined && response.is360degree != '' && response.is360degree != null && response.is360degree == true) {

                var url = 'https://www.google.com/maps/@?api=1&map_action=pano&viewpoint=' + response.lat + ',' + response.lng + '&heading=-20&pitch=38&fov=80';

                templates += "<div class='notify-bubble ' ></div><a class='pointer show-tooltipb plr2 360Degree  modal-buttons col-md-3 col-sm-3 col-xs-3' onclick='$bind.ClickStopPropagation(event);' target='_blank' href='" + url + "' title='" + $($c.controls.s360degreeview).val() + "'  > <div class='amenities-icon'><img  src='\\Images\\streetview.png' /></div></a>";
            }

            templates += "</div>";



            templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 pr5 mt15 contentdiv'>";

            if (type == "H") {

                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sNoofRoom).val() + "'>" + $($c.controls.sNoofRoom).val() + "</div> <div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.htotalroom + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sNoofConference).val() + "'>" + $($c.controls.sNoofConference).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.htotalcroom + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sSizeofLargestroom).val() + "'>" + $($c.controls.sSizeofLargestroom).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + parseFloat(response.hsizecroom).toFixed(2) + "</div>";
                templates += "</div>";

            }
            else if (type == "R") {
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sNoofRoom).val() + "'>" + $($c.controls.sNoofRoom).val() + "</div> <div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.rtotal_conference + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sCapacityOfRoom).val() + "'>" + $($c.controls.sCapacityOfRoom).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.rcapacity_of_largest_room + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sCapacityOfRestaurant).val() + "'>" + $($c.controls.sCapacityOfRestaurant).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.rcapacity + "</div>";
                templates += "</div>";
            }
            else if (type == "L") {
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sNoofConference).val() + "'>" + $($c.controls.sNoofConference).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + response.ltotal_conference + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sSizeofLargestroom).val() + "'>" + $($c.controls.sSizeofLargestroom).val() + "</div><div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + parseFloat(response.lsize_of_largest_conference).toFixed(2) + "</div>";
                templates += "</div>";
                templates += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 p0 mb10'>";
                templates += "<div class='col-lg-9 col-md-9 col-sm-9 col-xs-9 CONTENT11 lh20 pl0 pr0 show-tooltip' title='" + $($c.controls.sRentalCost).val() + "'>" + $($c.controls.sRentalCost).val() + "</div> <div class='badge col-lg-3 col-md-3 col-sm-3 col-xs-3' >" + parseFloat(response.lrent_begining).toFixed(2) + "</div>";
                templates += "</div>";
            }

            templates += "</div>";

            templates += "</div>";
            templates += "</div>";
            templates += "</div>";

            templates += "</div>";

            if (from == "T" || from == "X") {
                templates = templates.replace("col-lg-6 col-md-12 col-sm-12 col-xs-12 h150 ac paneldiv removablediv", "row col-lg-12 col-md-12 col-sm-12 col-xs-12 ac paneldiv removablediv prplmrml0");
            }

            if (from == "C" || from == "T") {
                setTimeout(function () {
                    $search.maphtml(templates, response, type, num, name, "L");
                }, 100);

            } else if (from != "X") {

                templates = templates.replace("col-lg-6 col-md-12 col-sm-12 col-xs-12 h150 ac paneldiv removablediv", "row col-lg-12 col-md-12 col-sm-12 col-xs-12 ac paneldiv removablediv prplmrml0");
                templates = templates.replace("col-lg-3 col-md-3 col-sm-3 col-xs-12 pl30 ac p10", "col-lg-3 col-md-3 col-sm-3 col-xs-12 pl30 ac p10 displaynone");
                templates = templates.replace("col-lg-9 col-md-9 col-sm-9 col-xs-12 p5", "col-lg-12 col-md-12 col-sm-12 col-xs-12 p5");
                templates = templates.replace("bborder card card-5 box", "bborder card card-5");
                templates = templates.replace("col-lg-12 col-md-12 col-sm-12 col-xs-12 btnchk blbr5", "col-lg-12 col-md-12 col-sm-12 col-xs-12 btnchk blbr51");

            }

            setTimeout(function () {
                $('.resourceName').tooltip({ placement: 'left' });
            }, 3);
        }

        //console.log(templates);
        return templates;

    },
    maphtml: function (htmlcontents, data, type, num, name, from) {

        if (data.lat != undefined && data.lat != null && data.lat != "" && data.lng != undefined && data.lng != null && data.lng != "") {

            htmlcontents = htmlcontents.replace('col-lg-7 col-md-12 col-sm-12 col-xs-12 pr5 contentdiv', 'col-lg-12 col-md-12 col-sm-12 col-xs-12 pr5 contentdiv');

            htmlcontents = htmlcontents.replace('addtocart', 'mapaddtocart');

            var $htmlcontents = $(htmlcontents).find('.imagediv').remove().end();

            $Map.LoadSideMap(data.lat, data.lng, $htmlcontents[0].innerHTML, type, num, name, from);

        } else {

            $v.variables.markers.push("");

        }

    },
    loadimages: function (id, t) {

        var jasonData = { id: id, type: t };

        $.ajax({
            url: $bind.URL($u.path.loadimgdata),
            data: jasonData,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (result) {

                if (result.code != 704 && result.code != "704") {
                    $managecode.Decision(false, "", result.code, result.message, "Photos", false);
                }

                data = result.data;

                $("#imgo" + t + id).empty();

                if (data != null && data.length > 0) {
                    data.forEach(function (item, indx) {
                        var img = "<a href=" + $bind.ImageURL(item.photo_name) + " class='img-fluid'> <img src='" + $bind.ImageURL(item.photo_name) + "' data-imgname='" + item.photo_name + "' class='img-strip'/></a>";
                        $("#imgo" + t + id).append(img);
                        //console.log(id);
                    });

                    try {

                        $("#imgo" + t + id + " a").tosrus({
                            buttons: true,
                            pagination: {
                                add: true,
                                type: 'thumbnails'
                            }
                        });

                        var tos = $("#imgo" + t + id).tosrus({
                            slides: {
                                visible: 1
                            }
                        });
                    } catch (e) { console.log(e); }

                    if ($('#imgo' + t + id + ' .tos-slider').find('div').length - 1 != 0) {

                        $('#imgo' + t + id).data('curslide', '0');
                        $('#imgo' + t + id + '>div').append('<a class="tos-prev" href="javascript:void(0);"><span></span></a><a class="tos-next" href="javascript:void(0);"><span></span></a>');

                        if ((data.length - 1).toString() == $("#imgo" + t + id).data('curslide')) {
                            $('#imgo' + t + id + '>div .tos-next').addClass('tos-disabled');
                        }

                        if ($('#imgo' + t + id).data('curslide') == '0') {
                            $('#imgo' + t + id + '>div .tos-prev').addClass('tos-disabled');
                        }

                        $('#imgo' + t + id + '>div .tos-next').click(function () {
                            var total_slides = $('#imgo' + t + id + ' .tos-slider').find('div').length - 1;
                            if (!$('#imgo' + t + id + '>div .tos-next').hasClass('tos-disabled')) {
                                if ((total_slides).toString() != $("#imgo" + t + id).data('curslide')) {
                                    $('#imgo' + t + id).data('curslide', (parseInt($("#imgo" + t + id).data('curslide')) + 1).toString());
                                    $('#imgo' + t + id + ' .tos-slider').css('left', (parseInt($("#imgo" + t + id).data('curslide')) * -100).toString() + '%');
                                    $('#imgo' + t + id + '>div .tos-prev').removeClass('tos-disabled');
                                }
                                if ((total_slides).toString() == $("#imgo" + t + id).data('curslide')) {
                                    $('#imgo' + t + id + '>div .tos-next').addClass('tos-disabled');
                                }
                            }
                        });

                        $('#imgo' + t + id + '>div .tos-prev').click(function () {
                            if (!$('#imgo' + t + id + '>div .tos-prev').hasClass('tos-disabled')) {
                                if ($('#imgo' + t + id).data('curslide') != '0') {
                                    $('#imgo' + t + id + ' .tos-slider').css('left', ((parseInt($("#imgo" + t + id).data('curslide')) * -100) + 100).toString() + '%');
                                    $('#imgo' + t + id).data('curslide', (parseInt($("#imgo" + t + id).data('curslide')) - 1).toString());
                                    $('#imgo' + t + id + '>div .tos-next').removeClass('tos-disabled');
                                }
                                if ($('#imgo' + t + id).data('curslide') == '0') {
                                    $('#imgo' + t + id + '>div .tos-prev').addClass('tos-disabled');
                                }
                            }
                        });

                    }

                    $('#imgo' + t + id).removeClass("displaynone");
                    $('#img' + t + id).remove();

                }

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

    },
    applysearch: function (type) {

        if ($validation.mandateprimary()) {

            $bind.FilterLoader();

            $(".removablediv").remove();

            $v.variables.recordsDisplayed = 0;
            $v.variables.totalRecords = 0;
            $v.variables.center = [];
            $($c.controls.lblmapdisplayed).text("-");
            $Map.initializeSideM();

            setTimeout(function () {
                $search.retrievedata(true);
                //$bind.top100();
            }, 1);

        }

    },
    openhotel: function (event, e) {
        event.preventDefault();
        event.stopPropagation();
        $($c.controls.modeltitle).text("  " + $(e).attr('data-name'));
        $v.variables.nearbymarkers = [];

        if ($(e).attr('data-lng') == null || $(e).attr('data-lng') == undefined || $(e).attr('data-lng') == '' || $(e).attr('data-lng') == 'null') {
            $bind.ClearPopupwindow();
            $($c.controls.mapmsg).show(1000);

        } else {
            $($c.controls.mapmsg).hide();
            $($c.controls.listview).show();
            $($c.controls.map).show();
            $search.GetNearByHotelList($(e).attr('data-id'), $(e).attr('data-lng'), $(e).attr('data-lat'), $(e).attr('data-name'), $(e).attr('data-type'), $($c.controls.NearByKM).val().split(",")[0]);
        }
        $bind.openmodel();
    },
    openrestaurant: function (event, e) {
        event.preventDefault();
        event.stopPropagation();
        $($c.controls.modeltitle).text("  " + $(e).attr('data-name'));
        $v.variables.nearbymarkers = [];

        if ($(e).attr('data-lng') == null || $(e).attr('data-lng') == undefined || $(e).attr('data-lng') == '' || $(e).attr('data-lng') == 'null') {
            $bind.ClearPopupwindow();
            $($c.controls.mapmsg).show(1000);

        } else {
            $($c.controls.mapmsg).hide();
            $($c.controls.listview).show();
            $($c.controls.map).show();
            $search.GetNearByRestaurantList($(e).attr('data-id'), $(e).attr('data-lng'), $(e).attr('data-lat'), $(e).attr('data-name'), $(e).attr('data-type'), $($c.controls.NearByKM).val().split(",")[0]);
        }

        $bind.openmodel();
    },
    openlocation: function (event, e) {
        event.preventDefault();
        event.stopPropagation();
        $($c.controls.modeltitle).text("  " + $(e).attr('data-name'));
        $v.variables.nearbymarkers = [];

        if ($(e).attr('data-lng') == null || $(e).attr('data-lng') == undefined || $(e).attr('data-lng') == '' || $(e).attr('data-lng') == 'null') {
            $bind.ClearPopupwindow();
            $($c.controls.mapmsg).show(1000);

        } else {
            $($c.controls.mapmsg).hide();
            $($c.controls.listview).show();
            $($c.controls.map).show();
            $search.GetNearByLocationList($(e).attr('data-id'), $(e).attr('data-lng'), $(e).attr('data-lat'), $(e).attr('data-name'), $(e).attr('data-type'), $($c.controls.NearByKM).val().split(",")[0]);
        }
        $bind.openmodel();
    },
    openmap: function (event, e) {
        event.preventDefault();
        event.stopPropagation();
        $($c.controls.modeltitle).text("  " + $(e).attr('data-name'));
        $v.variables.nearbymarkers = [];
        if ($(e).attr('data-lng') == null || $(e).attr('data-lng') == undefined || $(e).attr('data-lng') == '' || $(e).attr('data-lng') == 'null') {
            $bind.ClearPopupwindow();
            $($c.controls.mapmsg).show(1000);
            $
        } else {
            $($c.controls.mapmsg).hide();
            $($c.controls.listview).hide();
            $($c.controls.afurtherfilter).hide();
            $($c.controls.map).show();
            setTimeout(function () {
                $Map.initializeM($(e).attr('data-lng'), $(e).attr('data-lat'), $(e).attr('data-name'), $(e).attr('data-type'));
            }, 2000);
        }
        $bind.openmodel();
    },
    openinfo: function (e) {

        $search.GetRInfo($(e).attr('data-id'), $(e).attr('data-type'), $(e).attr('data-for'));


        if ($(e).attr('data-for') == "I") {
            $($c.controls.infomodal).modal('toggle');
            $($c.controls.infotitle).text("  " + $(e).attr('data-name') + " (" + $(e).attr('data-dist') + " - " + $(e).attr('data-time') + ")");
        } else {
            $($c.controls.modeltitle).text("  " + $(e).attr('data-name'));
            $bind.openmodel();
            $($c.controls.mapmsg).hide();
            $($c.controls.listview).hide();
            $($c.controls.afurtherfilter).hide();
            $($c.controls.map).html('');
            $($c.controls.map).show();
        }

    },
    GetNearByRestaurantList: function (id, lng, lat, name, type, distance) {

        if ($('.ddldistance').val() != undefined && $('.ddldistance').val() != distance) {
            distance = $('.ddldistance').val();
        }

        $bind.ClearPopupwindow();
        $bind.StartLoader("M");

        //var cid;

        //if (type == "H") {
        //    cid = $($c.controls.ddlhotelcountry).val();
        //} else if (type == "R") {
        //    cid = $($c.controls.ddlrestaurantcountry).val();
        //} else if (type == "L") {
        //    cid = $($c.controls.ddllocationcountry).val();
        //}

        var jasonData = { lng: (lng), lat: (lat), cid: id, type: type, distance: distance };

        $.ajax({
            url: $bind.URL($u.path.GetNearByRestaurantList),
            data: jasonData,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (result) {

                if (result.code != 704 && result.code != "704") {
                    $managecode.Decision(false, "", result.code, result.message, "NearBy", false);
                }

                $($c.controls.nearbylist).empty();

                var nearby = $bind.Bindneardistance(id, lng, lat, name, type, distance, "R");

                $($c.controls.nearbylist).append(nearby);

                $Map.initializeR(lng, lat, result.data, name, type, id);

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

    },
    GetNearByHotelList: function (id, lng, lat, name, type, distance) {

        if ($('.ddldistance').val() != undefined && $('.ddldistance').val() != distance) {
            distance = $('.ddldistance').val();
        }

        $bind.ClearPopupwindow();
        $bind.StartLoader("M");

        //var cid;

        //if (type == "H") {
        //    cid = $($c.controls.ddlhotelcountry).val();
        //} else if (type == "R") {
        //    cid = $($c.controls.ddlrestaurantcountry).val();
        //} else if (type == "L") {
        //    cid = $($c.controls.ddllocationcountry).val();
        //}

        var jasonData = { lng: (lng), lat: (lat), cid: id, type: type, distance: distance };

        $.ajax({
            url: $bind.URL($u.path.GetNearByHotelList),
            data: jasonData,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (result) {

                if (result.code != 704 && result.code != "704") {
                    $managecode.Decision(false, "", result.code, result.message, "NearBy", false);
                }

                $($c.controls.nearbylist).empty();

                var nearby = $bind.Bindneardistance(id, lng, lat, name, type, distance, "H");

                $($c.controls.nearbylist).append(nearby);

                $Map.initializeH(lng, lat, result.data, name, type, id);

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

    },
    GetNearByLocationList: function (id, lng, lat, name, type, distance) {

        if ($('.ddldistance').val() != undefined && $('.ddldistance').val() != distance) {
            distance = $('.ddldistance').val();
        }

        $bind.ClearPopupwindow();
        $bind.StartLoader("M");

        var jasonData = { lng: (lng), lat: (lat), cid: id, type: type, distance: distance };

        $.ajax({
            url: $bind.URL($u.path.GetNearByLocationList),
            data: jasonData,
            type: "GET",
            dataType: "json",
            contentType: 'application/json; charset=utf-8',
            async: true,
            cache: false,
            success: function (result) {

                if (result.code != 704 && result.code != "704") {
                    $managecode.Decision(false, "", result.code, result.message, "NearBy", false);
                }

                $($c.controls.nearbylist).empty();

                var nearby = $bind.Bindneardistance(id, lng, lat, name, type, distance, "L");

                $($c.controls.nearbylist).append(nearby);

                $Map.initializeL(lng, lat, result.data, name, type, id);

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

    },
    GetRInfo: function (id, type, frm) {

        var jasonData = { Rids: id, type: type };
        var resourceContent = "";

        $.ajax({
            url: $bind.URL($u.path.GetResourcesDetails),
            data: jasonData,
            type: "GET",
            async: false,
            cache: false,
            success: function (result) {

                if (result.code != null && (result.code == 704 || result.code == "704")) {

                    var resources = result.data.ResourceModel;

                    if (resources !== undefined && resources !== null) {
                        $.each(resources, function (index, value) {

                            resourceContent = $search.drawpanel(value, value.type, index, "X", false, true, false);

                        });
                    }

                }
                else {
                    $managecode.Decision(false, "", result.code, result.message, "NearBy", false);
                }

            },
            error: function (data, status, error) {
                $managecode.Decision(true, data, "", "", "error", false);
            }
        });

        return resourceContent;


    },
    sendupdate: function (type) {

        var email = $($c.controls.sAdminUpdateMail).val();
        var name;

        if (type == "H") {
            name = $($c.controls.txtinsidehotelname).val();
            type = $($c.controls.hdnhotel).val();
        } else if (type == "R") {
            name = $($c.controls.txtinsiderestname).val()
            type = $($c.controls.hdnrestaurant).val();
        } else if (type == "L") {
            name = $($c.controls.txtinsidelocname).val()
            type = $($c.controls.hdnlocation).val();
        }

        var subject = $($c.controls.sUpdateMailSubject).val() + " " + name + " - (" + type + ")";

        $.get("/Email/Discover/Notification.html", function (html_string) {

            // var body;

            // console.log($(KB_Common.controls.hdnLanguage).val().toUpperCase());

            //if ($(KB_Common.controls.hdnLanguage).val().toUpperCase() == "EN-US") {
            //    body = html_string.substring(html_string.indexOf("EnUSStart") + 9, html_string.indexOf("EnUSEnd"));
            //} else {
            //    body = html_string.substring(html_string.indexOf("EnDEStart") + 9, html_string.indexOf("EnDEEnd"));
            //}

            wnd = window.open('mailto:' + email + '?subject=' + encodeURIComponent(subject.replace("&", "and")) + '&body=' + encodeURIComponent(html_string.replace("$$name$$", $($c.controls.hdnUser).val())));
        }, 'html');


        //window.location = 'mailto:' + email + '?subject=' + subject + '&body=' + emailBody;

    },
    applyfullInfo: function (type, data) {

        if (type == "H") {

            // Country

            $($c.controls.ddlhotelcountry).val(data.country);
            $($c.controls.ddlhotelcountry).select2({ allowClear: true });

            // City

            $($c.controls.txthotelcity).val(data.city);

            // Name

            $($c.controls.txthotelname).val(data.name);

            // Stars

            // $v.variables.stars = data.stars.split(",");

            $(".fl-btnlg").each(function () {
                var star = $(this).attr("data-star");
                for (a in data.stars) {
                    if (star.toString() == data.stars[a].toString()) {
                        $(this).addClass('fl-group__btn--stars-active');
                    }
                }
            });

            // No of rooms

            $v.variables.totalroom = data.totalroom;
            $($c.controls.rslftotalroom).jRange('setValue', $v.variables.totalroom);

            // No of conference rooms

            $v.variables.totalcroom = data.totalcroom;
            $($c.controls.rslftotalconfroom).jRange('setValue', $v.variables.totalcroom);

            // size of conference rooms

            $v.variables.sizecroom = data.sizecroom;
            $($c.controls.rslfconfsize).jRange('setValue', $v.variables.sizecroom);

            // Parking Space

            $v.variables.parkingspace = data.parkingspace;
            $($c.controls.rslfparkingspace).jRange('setValue', $v.variables.parkingspace);

            // Hotel Type
            if (data.typeids != null && data.typeids != undefined) {
                $($c.controls.multihoteltypei).val(data.typeids.split(",")).trigger("change");
            }

            // Surrounding Location

            $($c.controls.txtdistance).val(data.distance);
            $($c.controls.txtlocation).val(data.loc);

            if (data.location != null && data.location != undefined && data.location.length > 0) {
                $($c.controls.txtlocation).attr("data-lng", data.location.split(",")[0]);
                $($c.controls.txtlocation).attr("data-lat", data.location.split(",")[1])
            }

            // Transfer Time from Airport

            $($c.controls.txthours).val(data.hours);
            $($c.controls.txtminutes).val(data.minutes);

            // Chain

            if (data.chains != null && data.chains != undefined) {
                $($c.controls.multihotelchaini).val(data.chains.split(",")).trigger("change");
            }

        }
        else if (type == "R") {

            // Country

            $($c.controls.ddlrestaurantcountry).val(data.country);
            $($c.controls.ddlrestaurantcountry).select2({ allowClear: true });

            // City

            $($c.controls.txtrestaurantcity).val(data.city);

            // Name

            $($c.controls.txtrestaurantname).val(data.name);

            // No of rooms

            $v.variables.Rtotalroom = data.rtotalroom;
            $($c.controls.Rrslftotalroom).jRange('setValue', $v.variables.Rtotalroom);

            // Size of largest room

            $v.variables.Rsizecroom = data.rsizecroom;
            $($c.controls.Rrslfroomsize).jRange('setValue', $v.variables.Rsizecroom);

            // Capacity of largest room

            $v.variables.roomcapacity = data.roomcapacity;
            $($c.controls.Rrslfroomcapacity).jRange('setValue', $v.variables.roomcapacity);

            // Capacity of restaurants

            $v.variables.restaurantcapacity = data.restaurantcapacity;
            $($c.controls.Rrslfrestaurantcapacity).jRange('setValue', $v.variables.restaurantcapacity);

            // Minimum turnover
            if (data.minimumturnover != null && data.minimumturnover != undefined) {
                $v.variables.minimumturnover = data.minimumturnover;
            }

            $($c.controls.Rrslfminimumturnover).jRange('setValue', $v.variables.minimumturnover);

            // Kitchen
            if (data.kids != null && data.kids != undefined) {
                $($c.controls.multirestaurantkitcheni).val(data.kids.split(",")).trigger("change");
            }

            // Restaurant Type
            if (data.typeids != null && data.typeids != undefined) {
                $($c.controls.multirestauranttypei).val(data.typeids.split(",")).trigger("change");
            }

            // Suitable For
            // console.log(data.Suitableids);
            if (data.suitableids != null && data.suitableids != undefined) {
                $($c.controls.multirestaurantsuitablefori).val(data.suitableids.split(",")).trigger("change");
            }

            // Surrounding Location
            $($c.controls.txtdistance).val(data.distance);
            $($c.controls.txtlocation).val(data.loc);

            if (data.location != null && data.location != undefined && data.location.length > 0) {
                $($c.controls.txtlocation).attr("data-lng", data.location.split(",")[0]);
                $($c.controls.txtlocation).attr("data-lat", data.location.split(",")[1])
            }

            // Checkbox events
            if (data.chkoutdoor != null && data.chkoutdoor == "true") {
                $($c.controls.yes_radioOP).prop('checked', true);
                $($c.controls.maybe_radioOP).prop('checked', false);
                $($c.controls.no_radioOP).prop('checked', false);
            } else if (data.chkoutdoor != null && data.chkoutdoor == "false") {
                $($c.controls.yes_radioOP).prop('checked', false);
                $($c.controls.maybe_radioOP).prop('checked', false);
                $($c.controls.no_radioOP).prop('checked', true);
            }

            if (data.chkrestinhotel != null && data.chkrestinhotel == "true") {
                $($c.controls.yes_radioRIH).prop('checked', true);
                $($c.controls.maybe_radioRIH).prop('checked', false);
                $($c.controls.no_radioRIH).prop('checked', false);
            } else if (data.chkrestinhotel != null && data.chkrestinhotel == "false") {
                $($c.controls.yes_radioRIH).prop('checked', false);
                $($c.controls.maybe_radioRIH).prop('checked', false);
                $($c.controls.no_radioRIH).prop('checked', true);
            }

        }
        else if (type == "L") {

            // Country

            $($c.controls.ddllocationcountry).val(data.country);
            $($c.controls.ddllocationcountry).select2({ allowClear: true });

            // City

            $($c.controls.txtlocationcity).val(data.city);

            // Name

            $($c.controls.txtlocationname).val(data.name);

            // No of conf rooms

            $v.variables.Ltotalcroom = data.ltotalcroom;
            $($c.controls.Lrslftotalconfroom).jRange('setValue', $v.variables.Ltotalcroom);

            // Size of largest conf room

            $v.variables.Lsizecroom = data.lsizecroom;
            $($c.controls.Lrslfconfsize).jRange('setValue', $v.variables.Lsizecroom);

            // Room height

            $v.variables.roomheight = data.roomheight;
            $($c.controls.Lrslfroomheight).jRange('setValue', $v.variables.roomheight);

            // Rental cost

            $v.variables.rentalcost = data.rentalcost;
            $($c.controls.Lrslfrentalcost).jRange('setValue', $v.variables.rentalcost);

            // Location Type
            if (data.multilocationtype != null && data.multilocationtype != undefined) {
                $($c.controls.multilocationtypei).val(data.multilocationtype.split(",")).trigger("change");
            }

            // Suitable For
            // console.log(data.Suitableids);
            if (data.suitableids != null && data.suitableids != undefined) {
                $($c.controls.multilocationsuitablefori).val(data.suitableids.split(",")).trigger("change");
            }

            // Surrounding Location

            $($c.controls.txtdistance).val(data.distance);
            $($c.controls.txtlocation).val(data.loc);

            if (data.location != null && data.location != undefined && data.location.length > 0) {
                $($c.controls.txtlocation).attr("data-lng", data.location.split(",")[0]);
                $($c.controls.txtlocation).attr("data-lat", data.location.split(",")[1])
            }

            // Checkbox events

            if (data.chkoutdoor != null && data.chkoutdoor == "true") {
                $($c.controls.yes_radioOP).prop('checked', true);
                $($c.controls.maybe_radioOP).prop('checked', false);
                $($c.controls.no_radioOP).prop('checked', false);
            } else if (data.chkoutdoor != null && data.chkoutdoor == "false") {
                $($c.controls.yes_radioOP).prop('checked', false);
                $($c.controls.maybe_radioOP).prop('checked', false);
                $($c.controls.no_radioOP).prop('checked', true);
            }

            if (data.chkcp != null && data.chkcp == "true") {
                $($c.controls.yes_radioCP).prop('checked', true);
                $($c.controls.maybe_radioCP).prop('checked', false);
                $($c.controls.no_radioCP).prop('checked', false);
            } else if (data.chkcp != null && data.chkcp == "false") {
                $($c.controls.yes_radioCP).prop('checked', false);
                $($c.controls.maybe_radioCP).prop('checked', false);
                $($c.controls.no_radioCP).prop('checked', true);
            }

            if (data.chktp != null && data.chktp == "true") {
                $($c.controls.yes_radioTP).prop('checked', true);
                $($c.controls.maybe_radioTP).prop('checked', false);
                $($c.controls.no_radioTP).prop('checked', false);
            } else if (data.chktp != null && data.chktp == "false") {
                $($c.controls.yes_radioTP).prop('checked', false);
                $($c.controls.maybe_radioTP).prop('checked', false);
                $($c.controls.no_radioTP).prop('checked', true);
            }

            if (data.chkcd != null && data.chkcd == "true") {
                $($c.controls.yes_radioCD).prop('checked', true);
                $($c.controls.maybe_radioCD).prop('checked', false);
                $($c.controls.no_radioCD).prop('checked', false);
            } else if (data.chkcd != null && data.chkcd == "false") {
                $($c.controls.yes_radioCD).prop('checked', false);
                $($c.controls.maybe_radioCD).prop('checked', false);
                $($c.controls.no_radioCD).prop('checked', true);
            }

        }

        if (data.chkpp != null && data.chkpp == "true") {
            $($c.controls.yes_radioPP).prop('checked', true);
            $($c.controls.maybe_radioPP).prop('checked', false);
            $($c.controls.no_radioPP).prop('checked', false);
        } else if (data.chkpp != null && data.chkpp == "false") {
            $($c.controls.yes_radioPP).prop('checked', false);
            $($c.controls.maybe_radioPP).prop('checked', false);
            $($c.controls.no_radioPP).prop('checked', true);
        }

        if (data.chkclose != null && data.chkclose == "true") {
            $($c.controls.yes_radioClose).prop('checked', true);
            $($c.controls.maybe_radioClose).prop('checked', false);
            $($c.controls.no_radioClose).prop('checked', false);
        } else if (data.chkclose != null && data.chkclose == "false") {
            $($c.controls.yes_radioClose).prop('checked', false);
            $($c.controls.maybe_radioClose).prop('checked', false);
            $($c.controls.no_radioClose).prop('checked', true);
        }


        if (data.chkblocked != null && data.chkblocked == "true") {
            $($c.controls.yes_radioBlack).prop('checked', true);
            $($c.controls.maybe_radioBlack).prop('checked', false);
            $($c.controls.no_radioBlack).prop('checked', false);
        } else if (data.chkblocked != null && data.chkblocked == "false") {
            $($c.controls.yes_radioBlack).prop('checked', false);
            $($c.controls.maybe_radioBlack).prop('checked', false);
            $($c.controls.no_radioBlack).prop('checked', true);
        }

        if (data.chkgreen != null && data.chkgreen == "true") {
            $($c.controls.yes_radioGreen).prop('checked', true);
            $($c.controls.maybe_radioGreen).prop('checked', false);
            $($c.controls.no_radioGreen).prop('checked', false);
        } else if (data.chkgreen != null && data.chkgreen == "false") {
            $($c.controls.yes_radioGreen).prop('checked', false);
            $($c.controls.maybe_radioGreen).prop('checked', false);
            $($c.controls.no_radioGreen).prop('checked', true);
        }

        if (data.chkreopen != null && data.chkreopen == "true") {
            $($c.controls.yes_radioReopen).prop('checked', true);
            $($c.controls.maybe_radioReopen).prop('checked', false);
            $($c.controls.no_radioReopen).prop('checked', false);
        } else if (data.chkreopen != null && data.chkreopen == "false") {
            $($c.controls.yes_radioReopen).prop('checked', false);
            $($c.controls.maybe_radioReopen).prop('checked', false);
            $($c.controls.no_radioReopen).prop('checked', true);
        }

    },
    applysortby: function (e, sort) {
        $($c.controls.drawbutton).hide();
        $($c.controls.sortby).removeClass("bluefont");
        $(e).addClass("bluefont");
        $search.applysearch($(e).attr('data-type'));
        $($c.controls.sorted).html("<span class='show-tooltip' title='" + sort +"'>" + sort +"</span>" + ": " + $(e).siblings('.name')[0].outerHTML + "&nbsp;" + $(e)[0].outerHTML + " &nbsp; <span class='caret'></span>");
    },
    applyrfpsortby: function (e, sort) {
        $($c.controls.drawbutton).hide();
        $($c.controls.sortby).removeClass("bluefont");
        $(e).addClass("bluefont");
        $v.variables.recordsDisplayed = 0;
        $v.variables.totalRecords = 0;
        $v.variables.i = 0;
        $($c.controls.sorted).html("<span class='show-tooltip' title='" + sort + "'>" + sort + "</span>" + ": " + $(e).siblings('.name')[0].outerHTML + "&nbsp;" + $(e)[0].outerHTML + " &nbsp; <span class='caret'></span>");
        $MRFP.GetRFPListData(true);

    }
}

$(document).ready(function () {
  
    $('#searchTab .nav-pills a').on('shown.bs.tab', function (event) {
        var current = $(event.target).data('tabid');         // active tab
        var prev = $(event.relatedTarget).data('tabid');  // previous tab

        if (current != null && prev != null && current != '' && prev != '') {
            if (current == 'searchDestinationContent' && prev == 'searchCountryContent') {
                $($c.controls.ddlcountry).select2("val", "");
                $($c.controls.txtcity).val('');
            } else if (current == 'searchCountryContent' && prev == 'searchDestinationContent') {
                // Destination
                $($c.controls.txtdestinationname).val("");
                $($c.controls.txtdestinationname).removeAttr("data-lng");
                $($c.controls.txtdestinationname).removeAttr("data-lat");

                // Radius
                $($c.controls.ddlradius).select2("val", "");
            }

        }

    });
    $($c.controls.btnsearch).on("click", function (e) {


        if ($validation.mandateprimary()) {

            $('body').css('backgroundImage', 'url(../../Images/img_banner_search.png)');

    
            $search.gatherprimaryinfo();
            $bind.CommonHideShow();
            $bind.CommonStartFlow();
            //$("#liSearchCountry").removeClass("active");
            //$("#liSearchDestination").addClass("active");
            //$("#searchCountryContent").removeClass("active");
            //$("#searchDestinationContent").addClass("active");
            //$("#searchTab ul").css({ "display": "none" });
            //$("#searchTab").attr("id", "");
            $LookUp.BindDropdown($c.controls.ddlradius, $($c.controls.hdnselectradius).val(), $u.path.GetRadiusList, 0, 50, false, true);
        }

    });



});
