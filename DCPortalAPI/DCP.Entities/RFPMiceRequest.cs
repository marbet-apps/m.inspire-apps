﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DCP.Entities
{
    public class RFPMiceRequest
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("RFP")]
        public int rfp_id { get; set; }

        public int? mice_request_id { get; set; }

        public int resource_type { get; set; }

        public string comments { get; set; }

        public string comments2 { get; set; }

        public string comments3 { get; set; }

        public string comments4 { get; set; }

        public int? city_id { get; set; }

        public int? place_id { get; set; }

        public int? country_id { get; set; }

        [Required, MaxLength(50), Column(TypeName = "varchar")]
        public string status { get; set; }

        public int created_by { get; set; }

        public DateTime created_date { get; set; }

        public int? modified_by { get; set; }

        public DateTime? modified_date { get; set; }

        public RequestForProposal RFP { get; set; }

        public ICollection<RFPResource> RFPResources { get; set; }

        public ICollection<RFPMiceRequestItems> RFPMiceRequestItems { get; set; }

        public ICollection<RFPMiceRequestDetails> RFPMiceRequestDetails { get; set; }

        public ICollection<RFPMiceRequestOrders> RFPMiceRequestOrders { get; set; }
    }
}
