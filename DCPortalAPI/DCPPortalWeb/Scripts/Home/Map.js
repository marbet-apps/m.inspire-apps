﻿
var markers = [];
var $Map = {


    initializeSideMC: function () {

        var mapOptions;

        mapOptions = {
            center: { lat: 11.122222220, lng: -7.978656677 },
            zoom: 5,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            fullscreenControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
        };

        $v.variables.markers = [];

        $v.variables.sidemap = new google.maps.Map(document.getElementById('mycartmap'), mapOptions);
        $v.variables.infowindow = new google.maps.InfoWindow();
        $v.variables.bounds = new google.maps.LatLngBounds();
        $v.variables.i = 0;

        google.maps.event.addListener($v.variables.sidemap, 'idle', function () {
            $Map.showVisibleMarkers($v.variables.sidemap);
        });


    },
    initializeSideA: function () {

        var mapOptions;

        if ($v.variables.templat != null && $v.variables.templng != null && $v.variables.tempzoom != null) {

            mapOptions = {
                center: new google.maps.LatLng($v.variables.templat, $v.variables.templng),
                zoom: parseInt($v.variables.tempzoom),
                scaleControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                fullscreenControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                }
            };

        } else {

            mapOptions = {
                center: { lat: 11.122222220, lng: -7.978656677 },
                zoom: 5,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                fullscreenControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                }
            };

        }

        $($c.controls.sidemap).empty();
        $v.variables.markers = [];

        $v.variables.sidemap = new google.maps.Map(document.getElementById('advancemap'), mapOptions);
        $v.variables.infowindow = new google.maps.InfoWindow();
        $v.variables.bounds = new google.maps.LatLngBounds();
        $v.variables.i = 0;

        $Map.GetandSetTempdata();

        google.maps.event.addListener($v.variables.sidemap, "center_changed", function () {

            $Map.GetandSetTempdata();

        });

        google.maps.event.addListener($v.variables.sidemap, "zoom_changed", function () {

            $Map.GetandSetTempdata();

        });

    },
    initializeSideM: function () {

        var mapOptions = {
            center: { lat: 11.122222220, lng: -7.978656677 },
            zoom: 11,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            fullscreenControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            }
        };

        $($c.controls.sidemap).empty();
        $v.variables.markers = [];

        $v.variables.sidemap = new google.maps.Map(document.getElementById('sidemap'), mapOptions);
        $v.variables.infowindow = new google.maps.InfoWindow();
        $v.variables.bounds = new google.maps.LatLngBounds();
        $v.variables.i = 0;

        google.maps.event.addListener($v.variables.sidemap, 'idle', function () {
            if ($v.variables.Isopen == 1) {
                $Map.showVisibleMarkers($v.variables.sidemap);
            }
        });

    },
    initializeR: function (lng, lat, data, name, type, id) {
        setTimeout(function () {
            $Map.MapFeature(lng, lat, data, "R", name, type, id);
        }, 2000);
    },
    initializeL: function (lng, lat, data, name, type, id) {
        setTimeout(function () {
            $Map.MapFeature(lng, lat, data, "L", name, type, id);
        }, 2000);
    },
    initializeH: function (lng, lat, data, name, type, id) {
        setTimeout(function () {
            $Map.MapFeature(lng, lat, data, "H", name, type, id);
        }, 2000);
    },
    initializeM: function (lng, lat, name, type) {

        if (lng == undefined || lng == null || lng == "" || lat == undefined || lat == null || lat == "") {

            lng = 0;
            lat = 0;

            $($c.controls.mapmsg).show(1000);

        }

        var mapOptions = {
            center: { lat: parseFloat(lat), lng: parseFloat(lng) },
            zoom: 18
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var infowindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            animation: google.maps.Animation.BOUNCE,
            map: map,
            label: { text: type, color: 'transparent' }
        });


        if (type == "H") {
            marker.setIcon('/Images/hotelm.png');
        } else if (type == "R") {
            marker.setIcon('/Images/restaurantm.png');
        } else if (type == "L") {
            marker.setIcon('/Images/locationm.png');
        }

        bounds.extend(marker.position);

        //google.maps.event.addListener(marker, 'click', (function (marker, i) {
        //    return function () {
        //        infowindow.setContent('<b> ' + name + ' </b>');
        //        infowindow.open(map, marker);
        //    }
        //})(marker, i));

        //var listener = google.maps.event.addListener(map, "idle", function () {
        //    google.maps.event.removeListener(listener);
        //});

    },


    autocompleteDestination: function () {

        var input = document.getElementById("txtdestinationname");

        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.addListener('place_changed', function () {

            var info = autocomplete.getPlace();

            if (info != null && info != undefined && info != "") {
                $($c.controls.txtdestinationname).attr("data-lng", info.geometry.location.lng());
                $($c.controls.txtdestinationname).attr("data-lat", info.geometry.location.lat());
                $($c.controls.txtdestinationname).attr("data-name", $($c.controls.txtdestinationname).val());
            }

        });

    },
    MapFeature: function (lng, lat, data, type, name, from, oid) {

        var mapOptions = {
            center: { lat: parseFloat(lat), lng: parseFloat(lng) },
            zoom: 15,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            fullscreenControl: false
            //fullscreenControlOptions: {
            //    position: google.maps.ControlPosition.LEFT_CENTER
            //}
        };

        var map = new google.maps.Map(document.getElementById('map'), mapOptions);
        var bounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();

        //$Map.MapMarker(lat, lng, "H", map, infowindow, bounds, name, parseFloat(lat), parseFloat(lng));


        if (data != null && data.length > 0) {

            for (i = 0; i < data.length; i++) {
                //alert(data.length);
                // $($c.controls.nearbylist).append('<li class="list-group-item"><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;' + data[i].name + '</li>');
                $Map.MapMarker(data[i].lat, data[i].lng, type, map, bounds, infowindow, data[i].name, parseFloat(lat), parseFloat(lng), name, from, i, data[i].id, oid);

            }

            $($c.controls.afurtherfilter).show();

        } else {
            $Map.LoadViewMap(map, bounds, infowindow, parseFloat(lat), parseFloat(lng), from, 0, name, name, name, "", "", "", oid, oid);
        }

        //if (data.length < 3) {
        //$(document).ready(function () {
        //setTimeout(function () {
        //    google.maps.event.trigger(map, "resize");
        //    map.setZoom(17);
        //    map.fitBounds(bounds);
        //    map.panToBounds(bounds);
        //}, 2000);
        //});
        //}

    },
    MapMarker: function (lat, lng, type, map, bounds, infowindow, name, lato, lngo, oname, from, count, id, oid) {

        var destinationIcon = '';
        var originIcon = '';

        if (type == "R") {
            destinationIcon = '/Images/restaurantm.png';
            originIcon = '/Images/hotelm.png';
        }

        if (type == "L") {
            destinationIcon = '/Images/Locationm.png';
            originIcon = '/Images/hotelm.png';
        }

        if (type == "H") {
            destinationIcon = '/Images/Hotelm.png';
            originIcon = '/Images/hotelm.png';
        }

        var geocoder = new google.maps.Geocoder;
        var service = new google.maps.DistanceMatrixService;

        service.getDistanceMatrix({
            origins: [{ lat: lato, lng: lngo }],
            destinations: [{ lat: lat, lng: lng }],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function (response, status) {
            if (status !== 'OK') {

                alert('Error was: ' + status);
            } else {


                var originList = response.originAddresses;
                var destinationList = response.destinationAddresses;

                var showGeocodedAddressOnMap = function (asDestination, Or, De, Di, Du) {
                    var icon = asDestination ? destinationIcon : originIcon;
                    return function (results, status) {
                        if (status === 'OK') {


                            map.fitBounds(bounds.extend(results[0].geometry.location));

                            if (asDestination == true) {

                                marker = new google.maps.Marker({
                                    map: map,
                                    animation: google.maps.Animation.DROP,
                                    position: new google.maps.LatLng(lat, lng),
                                    icon: icon,
                                    label: { text: type, color: 'transparent' }
                                });



                                var directionsService = new google.maps.DirectionsService();
                                var directionsDisplay = new google.maps.DirectionsRenderer({
                                    suppressMarkers: true,
                                    draggable: true
                                });

                                directionsDisplay.setMap(map);

                                var request = {
                                    origin: new google.maps.LatLng(lato, lngo),
                                    destination: new google.maps.LatLng(lat, lng),
                                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                                };

                                directionsService.route(request, function (response, status) {
                                    if (status == google.maps.DirectionsStatus.OK) {
                                        directionsDisplay.setDirections(response);
                                    }
                                });

                            } else {

                                marker = new google.maps.Marker({
                                    map: map,
                                    animation: google.maps.Animation.DROP,
                                    position: new google.maps.LatLng(lato, lngo),
                                    icon: icon,
                                    label: { text: type, color: 'transparent' }
                                });

                            }

                            bounds.extend(marker.position);

                            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                return function () {

                                    if (De == "" || De == null) {
                                        infowindow.setContent('<b> ' + oname + ' </b><br>' + Or + '');
                                    }
                                    else {
                                        infowindow.setContent('<b> ' + name + ' </b><br>' + Or + ' to ' + De + ': ' + Di + ' in ' + Du + '<br>');
                                    }
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));

                            var listener = google.maps.event.addListener(map, "idle", function () {
                                google.maps.event.removeListener(listener);
                            });

                            //} else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                            //    setTimeout(function () {
                            //        showGeocodedAddressOnMap(asDestination, Or, De, Di, Du);
                            //    }, 250);
                        } else {
                            alert('Geocode was not successful due to: ' + status);
                        }
                    };
                };

                for (var i = 0; i < originList.length; i++) {

                    var results = response.rows[i].elements;
                    if (count == 0) {
                        $Map.LoadViewMap(map, bounds, infowindow, lato, lngo, from, i, oname, name, originList[i], "", "", "", id, oid, false);
                    }

                    //geocoder.geocode({ 'address': originList[i] },
                    //showGeocodedAddressOnMap(false, originList[i], "", "", "", lat, lng, lato, lngo));

                    for (var j = 0; j < results.length; j++) {
                        //geocoder.geocode({ 'address': destinationList[j] },
                        //showGeocodedAddressOnMap(true, originList[i], destinationList[j], results[j].distance.text, results[j].duration.text, lat, lng, lato, lngo));
                        $Map.LoadViewMap(map, bounds, infowindow, lat, lng, type, j, oname, name, originList[i], destinationList[j], results[j].distance.text, results[j].duration.text, id, oid, true);
                        var Code = ('<span class="nearbyellipsis" title="' + name.toString() + '">' + name.toString() + '</span>&nbsp;(' + results[j].distance.text + ' | ' + results[j].duration.text).toString();

                        //24/09/2019 - Nearby list set image with text insteadof srno.
                        var img = '';
                        if (type == "R") {
                            img = "<img src='/Images/nearby_restaurant.png' class='w16' id='img Rest'>";
                        }
                        else if (type == "L") {
                            img = "<img src='/Images/nearby_location.png' class='w16' id='img Rest'>";
                        }
                        else if (type == "H") {
                            img = "<img src='/Images/nearby_hotel.png' class='w16' id='img Rest'>";
                        }

                        $($c.controls.nearbylist).append('<li class="list-group-item pointer nearzoomclass' + (count) + ' Cards" data-card="' + (count) + '" data-type="' + type + '" data-id="' + id + '" onclick="$bind.ShowNearByMapIcon(this);"><b>' + img + '</b> &nbsp;' + Code + ')</li>');

                    }

                }

            }
        });

    },
    LoadSideMap: function (lat, lng, htmlcontents, Types, i, name, from) {

        var map = $v.variables.sidemap;
        var infowindow = $v.variables.infowindow;
        var bounds = $v.variables.bounds;

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            animation: google.maps.Animation.DROP,
            map: map,
            label: { text: Types, color: 'transparent' }
        });

        if (Types == "H") {
            marker.setIcon('/Images/hotelm.png');
        } else if (Types == "L") {

            marker.setIcon('/Images/locationm.png');
        } else if (Types == "R") {
            marker.setIcon('/Images/restaurantm.png');
        }

        bounds.extend(marker.position);

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {

                infowindow.setContent(htmlcontents);
                infowindow.open(map, marker);

            }
        })(marker, i));

        //marker.addListener('mouseover', function () {
        //    infowindow.setContent(htmlcontents);
        //    infowindow.open(map, this);
        //});

        map.fitBounds(bounds);
        map.panToBounds(bounds);

        // Keep marker instances in a global array
        $v.variables.markers.push(marker);

        //var listener = google.maps.event.addListener(map, "idle", function () {
        //    google.maps.event.removeListener(listener);
        //});

        $Map.ChangeInfowindow(infowindow, from);


    },
    LoadViewMap: function (map, bounds, infowindow, lat, lng, Types, i, oname, name, Or, De, Di, Du, id, oid, flag) {

        marker = new google.maps.Marker({
            id: id,
            position: new google.maps.LatLng(lat, lng),
            animation: google.maps.Animation.DROP,
            map: map,
            label: { text: Types, color: 'transparent' }
        });

        if (Types == "H") {
            marker.setIcon('/Images/hotelm.png');
        } else if (Types == "L") {
            marker.setIcon('/Images/locationm.png');
        } else if (Types == "R") {
            marker.setIcon('/Images/restaurantm.png');
        }

        //24/09/2019 -> all marker add in one array for stop animation except clickable marker
        markers.push(marker);
        //***************

        bounds.extend(marker.position);
        map.fitBounds(bounds);
        map.panToBounds(bounds);

        var href = '/info/GetHotelInfo/';

        if (Types == "R") {
            href = '/info/GetRestaurantInfo/';
        } else if (Types == "L") {
            href = '/info/GetLocationInfo/';
        }

        //24/09/2019 -> Comment else code so, resolve issue of overwrite content on selected resource.
        if (De == "" || De == null) {
            infowindow.setContent('<div><b> ' + oname + ' </b><a href="' + (href + oid) + '" target="_blank" class="smallfont pointer bluefont" data-for ="I" data-type="' + Types + '"  data-id="' + oid + '" data-name="' + oname + '" data-dist="' + Di + '" data-time="' + Du + '"  id="diao"> ' + $($c.controls.sMore).val() + '</a></div>');
        }
        //else {
        //    infowindow.setContent('<div><b> ' + name + ' </b><a href="' + (href + id) + '" target="_blank" class="smallfont pointer bluefont" data-for ="I" data-type="' + Types + '"  data-id="' + id + '" data-name="' + name + '" data-dist="' + Di + '" data-time="' + Du + '"  id="diao"> ' + $($c.controls.sMore).val() + '</a></div>');
        //}

        google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {

                var href = '/info/GetHotelInfo/';
                var data = "", Info = "";

                if (Types == "R") {
                    href = '/info/GetRestaurantInfo/';
                } else if (Types == "L") {
                    href = '/info/GetLocationInfo/';
                }

                if (De == "" || De == null) {

                    data = '<div><b> ' + oname + ' </b><a href="' + (href + oid) + '" target="_blank"  class="smallfont pointer bluefont" data-for ="I" data-type="' + Types + '"  data-id="' + oid + '" data-name="' + oname + '" data-dist="' + Di + '" data-time="' + Du + '"  id="diao"> ' + $($c.controls.sMore).val() + '</a></div>';

                    Rid = oid;

                }
                else {

                    data = '<div><b> ' + name + ' </b><a href="' + (href + id) + '" target="_blank" class="smallfont pointer bluefont" data-for ="I" data-type="' + Types + '"  data-id="' + id + '" data-name="' + name + '" data-dist="' + Di + '" data-time="' + Du + '"  id="diao"> <b>' + Di + ' in ' + Du + '</b> &nbsp;&nbsp;' + $($c.controls.sMore).val() + '</a></div>';

                    Rid = id;

                }

                Info = $search.GetRInfo(Rid, Types, "");

                data += ((Info == null || Info == undefined) ? "" : Info.replace("imagediv", "imagediv displaynone").replace("col-lg-7 col-md-12 col-sm-12 col-xs-12 pr5 contentdiv", "col-lg-12 col-md-12 col-sm-12 col-xs-12 pr5 contentdiv").replace("ellipsis",""));

                //24/09/2019 -> When click on marker set animation for selected marker and stop animation of other marker
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setAnimation(null);
                }

                marker.setAnimation(google.maps.Animation.BOUNCE);
                //******************

                infowindow.setContent(data);
                // infowindow.setContent(htmlcontents);
                infowindow.open(map, marker);

                //24/09/2019 -> When click on marker activate selected item.
                $(".list-group-item").removeClass("liback");

                $(".list-group-item").each(function () {
                    if ($(this).attr("data-card") != undefined || $(this).attr("data-card") != null) {
                        if (marker.id.toString() == $(this).attr("data-id").toString()) {
                            $(this).addClass("liback");
                        }
                    }
                });

            }
        })(marker, i));



        if (flag) {
            $v.variables.nearbymarkers.push(marker);
        } else {
            infowindow.open(map, marker);
        }

        //var listener = google.maps.event.addListener(map, "idle", function () {
        //    google.maps.event.removeListener(listener);
        //});

    },
    LoadAdvanceMap: function (response, flag) {

        var markerClusterer = null;
        var latLng;

        var Hdata = response.DataEntity;
        var Rdata = response.RDataEntity;
        var Ldata = response.LDataEntity;

        var infowindow = $v.variables.infowindow;
        var bounds = $v.variables.bounds;

        $.each(Hdata, function (i) {

            var htmlcontents = $search.drawpanel(Hdata[i], "H", $v.variables.i, "A", true, true, false);
            latLng = new google.maps.LatLng(Hdata[i].lat, Hdata[i].lng);

            var Hmarker = new google.maps.Marker({
                position: latLng,
                animation: google.maps.Animation.DROP,
            });

            Hmarker.setIcon('/Images/hotelm.png');
            if (flag) {
                bounds.extend(Hmarker.position);
            }
            $v.variables.markers.push(Hmarker);

            google.maps.event.addListener(Hmarker, 'click', (function (Hmarker, i) {
                return function () {
                    infowindow.setContent(htmlcontents);
                    infowindow.open($v.variables.sidemap, Hmarker);
                }
            })(Hmarker, i));

            $v.variables.i = $v.variables.i + 1;

        });

        $.each(Rdata, function (i) {

            var htmlcontents = $search.drawpanel(Rdata[i], "R", $v.variables.i, "A", true, true, false);

            latLng = new google.maps.LatLng(Rdata[i].lat, Rdata[i].lng);

            var Rmarker = new google.maps.Marker({
                position: latLng,
                animation: google.maps.Animation.DROP,
            });

            Rmarker.setIcon('/Images/restaurantm.png');

            if (flag) {
                bounds.extend(Rmarker.position);
            }

            $v.variables.markers.push(Rmarker);

            google.maps.event.addListener(Rmarker, 'click', (function (Rmarker, i) {
                return function () {
                    infowindow.setContent(htmlcontents);
                    infowindow.open($v.variables.sidemap, Rmarker);
                }
            })(Rmarker, i));

            $v.variables.i = $v.variables.i + 1;
        });

        $.each(Ldata, function (i) {

            var htmlcontents = $search.drawpanel(Ldata[i], "L", $v.variables.i, "A", true, true, false);

            latLng = new google.maps.LatLng(Ldata[i].lat, Ldata[i].lng);

            var Lmarker = new google.maps.Marker({
                position: latLng,
                animation: google.maps.Animation.DROP,
            });

            Lmarker.setIcon('/Images/locationm.png');

            if (flag) {
                bounds.extend(Lmarker.position);
            }

            $v.variables.markers.push(Lmarker);

            google.maps.event.addListener(Lmarker, 'click', (function (Lmarker, i) {
                return function () {
                    infowindow.setContent(htmlcontents);
                    infowindow.open($v.variables.sidemap, Lmarker);
                }
            })(Lmarker, i));

            $v.variables.i = $v.variables.i + 1;
        });

        markerClusterer = new MarkerClusterer($v.variables.sidemap, $v.variables.markers, {
            maxZoom: 10,
            minimumClusterSize: 15,
            imagePath: '/Images/Discover/C'
        });

        if (flag) {

            $v.variables.sidemap.fitBounds(bounds);
            $v.variables.sidemap.panToBounds(bounds);

        }

        $Map.ChangeInfowindow(infowindow, "S");

        if ((Hdata.length + Rdata.length + Ldata.length) == 0) {
            $($c.controls.mapmsg).show();
        } else {
            $($c.controls.mapmsg).hide();
        }

    },
    showVisibleMarkers: function (map) {

        var bounds = map.getBounds(), count = 0;

        $('.removablediv').hide();

        var markers = $v.variables.markers;

        for (var i = 0; i < markers.length; i++) {

            var marker = markers[i], infoPanel = $('.zoomclass' + i);

            //console.log(marker)

            if (marker != null && marker != undefined && marker != "" && bounds.contains(marker.getPosition()) === true) {

                // console.log(infoPanel);
                infoPanel.show();
                count++;
            }
            else {
                infoPanel.hide();
            }

        }

        $($c.controls.lblmapdisplayed).text(count);

        //$('html, body').animate({
        //    scrollTop: 92
        //}, 1);
    },
    ChangeInfowindow: function (infowindow, from) {
       
        var minheight = ((from == "L") ? "300px" : "190px"); 

        google.maps.event.addListener(infowindow, 'domready', function () {
       
            var icOuter = $('.gm-style-iw-c');

            icOuter.css({ width: '400px', height: minheight, padding: '0px' });

            var iwOuter = $('.gm-style-iw-d');

            iwOuter.css({ left: '15px', top: '15px', width: '418', height: minheight, padding: '0px' });

            var iwBackground = iwOuter.prev();

            iwBackground.children(':nth-child(2)').css({ 'display': 'none' });

            iwBackground.children(':nth-child(4)').css({ 'display': 'none' });

            iwOuter.parent().parent().css({ left: '1px' });

            iwOuter.parent().css({ height: minheight });

            var iwCloseBtn = iwOuter.next();

            iwCloseBtn.css({ opacity: '1', right: '-2px', top: '-2px' });
          
            //console.log($($($(this)[0].content)[0]).attr("data-card"));

            var infos = $($($(this)[0].content)[0]);

            $($c.controls.card5).removeClass("border");
            $("#" + infos.attr("data-id")).find(".card-5").addClass("border");

            var ci = infos.attr("data-card");

            var markers = $v.variables.markers;

            $Map.ResetMapIcons(markers, ci);

        });

    },
    GetandSetTempdata: function () {

        $v.variables.mapCentre = $v.variables.sidemap.getCenter();
        $v.variables.templat = $v.variables.mapCentre.lat();
        $v.variables.templng = $v.variables.mapCentre.lng();
        $v.variables.tempzoom = $v.variables.sidemap.getZoom();

    },
    ResetMapIcons: function (markers, ci) {

        for (var i = 0; i < markers.length; i++) {

            var itype = markers[i].getLabel();

            if (i.toString() == ci.toString()) {

                if (markers[i] != undefined && markers[i] != null && markers[i] != "") {

                    if (itype.text == "H") {
                        markers[i].setIcon('\\Images\\ShowHMap.png');
                    } else if (itype.text == "R") {
                        markers[i].setIcon('\\Images\\ShowRMap.png');
                    } else if (itype.text == "L") {
                        markers[i].setIcon('\\Images\\ShowLMap.png');
                    }

                    markers[i].setAnimation(google.maps.Animation.DROP);

                }

            }
            else {

                if (markers[i] != undefined && markers[i] != null && markers[i] != "") {

                    if (itype.text == "H") {
                        markers[i].setIcon('\\Images\\hotelm.png');
                    } else if (itype.text == "R") {
                        markers[i].setIcon('\\Images\\restaurantm.png');
                    } else if (itype.text == "L") {
                        markers[i].setIcon('\\Images\\locationm.png');
                    }

                    markers[i].setAnimation(null);
                }

            }

        }
    }
}