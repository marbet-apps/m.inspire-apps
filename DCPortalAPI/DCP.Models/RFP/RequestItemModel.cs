﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCP.Models.RFP
{
    public class RequestItemModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("rfp_micerequestid")]
        public int RFPMiceRequestId { get; set; }
        [JsonProperty("rfp_id")]
        public int RFPId { get; set; }
        [JsonProperty("request_itemtype")]
        public string RequestItemType { get; set; }
        [JsonProperty("requestitems")]
        public List<RequestItemObject> RequestItems { get; set; }
        [JsonProperty("created_by")]
        public int CreatedBy { get; set; }
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("modified_by")]
        public int? ModifiedBy { get; set; }
        [JsonProperty("modified_date")]
        public DateTime? ModifiedDate { get; set; }
    }

    public class RequestItemObject
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("quantity")]
        public int Quantity { get; set; }
        [JsonProperty("lang_code")]
        public string LangCode { get; set; }
        [JsonProperty("reserve_fromdate")]
        public DateTime ReserveFromDate { get; set; }
        [JsonProperty("display_reserve_fromdate")]
        public string DisplayReserveFromDate
        {
            get
            {
                return ReserveFromDate.ToString("dd.MM.yyyy");
            }
        }
        [JsonProperty("reserve_todate")]
        public DateTime ReserveToDate { get; set; }
        
        [JsonProperty("display_reserve_todate")]
        public string DisplayReserveToDate
        {
            get
            {
                return ReserveToDate.ToString("dd.MM.yyyy");
            }
        }
        [JsonProperty("display_date_time")]
        public string DisplayDateTime
        {
            get
            {
                if(StartTime != null && StartTime != "" && EndTime != null && EndTime != "")
                {
                    return ReserveFromDate.ToString("dd.MM.yyyy") + " " + StartTime + " - " + EndTime;
                } else
                {
                    return ReserveFromDate.ToString("dd.MM.yyyy");
                }
            }
        }
        [JsonProperty("start_time")]
        public string StartTime { get; set; }
        [JsonProperty("end_time")]
        public string EndTime { get; set; }
        [JsonProperty("budget")]
        public double Budget { get; set; }
        [JsonProperty("display_budget")]
        public string Display_Budget { get {
                return LangCode != null ?Budget.ToString(System.Globalization.CultureInfo.CreateSpecificCulture(LangCode)) : Budget.ToString();
            } }
        [JsonProperty("comments")]
        public string Comments { get; set; }
        [JsonProperty("room_type")]
        public string RoomType { get; set; }
        [JsonProperty("roomtypein_english")]
        public string RoomTypeInEnglish { get; set; }
        [JsonProperty("roomtypein_german")]
        public string RoomTypeInGerman { get; set; }
        [JsonProperty("display_roomtype")]
        public string DisplayRoomType
        {
            get
            {
                return LangCode == "de-DE" ? RoomTypeInGerman : RoomTypeInEnglish;
            }
        }
        [JsonProperty("seating_type")]
        public string SeatingType { get; set; }
        [JsonProperty("seating_type_english")]
        public string SeatingTypeInEnglish { get; set; }
        [JsonProperty("seating_type_german")]
        public string SeatingTypeInGerman { get; set; }
        [JsonProperty("display_seating_type")]
        public string DisplaySeatingType
        {
            get
            {
                return LangCode == "de-DE" ? SeatingTypeInGerman : SeatingTypeInEnglish;
            }
        }
        [JsonProperty("technique")]
        public RoomTechnique Technique { get; set; }
        [JsonProperty("food")]
        public ConferenceFood Food { get; set; }
        [JsonProperty("roomsize")]
        public double RoomSize { get; set; }
    }

    public class RoomTechnique
    {
        [JsonProperty("pens_and_notepads")]
        public bool PensAndNotepads { get; set; }
        [JsonProperty("canvas")]
        public int Canvas { get; set; }
        [JsonProperty("overhead_projector")]
        public int OverHeadProjector { get; set; }
        [JsonProperty("flipchart")]
        public int FlipChart { get; set; }
        [JsonProperty("wall")]
        public int Wall { get; set; }
        [JsonProperty("presentation_case")]
        public int PresentationCase { get; set; }
        [JsonProperty("beamer")]
        public int Beamer { get; set; }
        [JsonProperty("isdn")]
        public int ISDN { get; set; }
        [JsonProperty("internet")]
        public int Internet { get; set; }
        [JsonProperty("microphone")]
        public int Microphone { get; set; }
        [JsonProperty("wireless_microphone")]
        public int WirelessMicrophone { get; set; }
        [JsonProperty("sound_system")]
        public int SoundSystem { get; set; }
        [JsonProperty("rostrum")]
        public int Rostrum { get; set; }
    }

    public class ConferenceFood
    {
        [JsonProperty("morning_two_drinks")]
        public bool MorningTwoDrinks { get; set; }
        [JsonProperty("morning_drinks_flat")]
        public bool MorningDrinksFlat { get; set; }
        [JsonProperty("morning_coffee_and_tea")]
        public bool MorningCoffeeAndTea { get; set; }
        [JsonProperty("morning_welcome_coffee_and_tea")]
        public bool MorningWelcomeCoffeeAndTea { get; set; }
        [JsonProperty("morning_coffee_and_tea_break")]
        public bool MorningCoffeeAndTeaBreak { get; set; }
        [JsonProperty("midday_lunch_buffet")]
        public bool MiddayLunchBuffet { get; set; }
        [JsonProperty("midday_lunch_one_dish")]
        public bool MiddayLunchOneDish { get; set; }
        [JsonProperty("midday_lunch_two_course_meal")]
        public bool MiddayLunchTwoCourseMeal { get; set; }
        [JsonProperty("midday_lunch_three_course_meal")]
        public bool MiddayLunchThreeCourseMeal { get; set; }
        [JsonProperty("midday_lunch_snack")]
        public bool MiddayLunchSnack { get; set; }
        [JsonProperty("midday_one_drink")]
        public bool MiddayOneDrink { get; set; }
        [JsonProperty("midday_drinks_flat")]
        public bool MiddayDrinksFlat { get; set; }
        [JsonProperty("midday_coffee_and_tea_break")]
        public bool MiddayCoffeeAndTeaBreak { get; set; }
        [JsonProperty("second_coffebreak")]
        public bool SecondCoffeeBreak { get; set; }
        [JsonProperty("evening_dinner_buffet")]
        public bool EveningDinnerBuffet { get; set; }
        [JsonProperty("evening_dinner_one_dish")]
        public bool EveningDinnerOneDish { get; set; }
        [JsonProperty("evening_dinner_two_course_meal")]
        public bool EveningDinnerTwoCourseMeal { get; set; }
        [JsonProperty("evening_dinner_three_course_meal")]
        public bool EveningDinnerThreeCourseMeal { get; set; }
        [JsonProperty("evening_dinner_snack")]
        public bool EveningDinnerSnack { get; set; }
        [JsonProperty("gala_dinner")]
        public bool GalaDinner { get; set; }
        [JsonProperty("evening_one_drink")]
        public bool EveningOneDrink { get; set; }
        [JsonProperty("evening_drinks_flat")]
        public bool EveningDrinkFlat { get; set; }
    }
}
