﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DCP.Business.Interface;
using DCP.Entities;
using DCP.Models;
using DCP.Repository.Interface;
using DCP.Utilities;

namespace DCP.Business
{
    public class SyncBL : ISyncBL
    {
        private ISyncRepository syncRepository;
        public SyncBL(ISyncRepository syncRepository)
        {
            this.syncRepository = syncRepository;
        }
        public ApiResult SyncHotels(MigrationHotelModel migrationModel)
        {
            try
            {
                var response = syncRepository.SyncHotels(migrationModel);
                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);

            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);

            }
        }

        public ApiResult SyncLocations(MigrationLocationModel migrationModel)
        {
            try
            {
                var response = syncRepository.SyncLocations(migrationModel);
                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);

            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);

            }
        }
        public ApiResult SyncRestaurants(MigrationResaurantModel migrationModel)
        {
            try
            {
                var response = syncRepository.SyncRestaurants(migrationModel);
                return ResultSet.SuccessResponse(ResponceCodes.Success.Saved, response, ManageEnum.status.success.ToString(), ResponceMessages.Success.Saved);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);

            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);

            }
        }

        public ApiResult MigrateAirport(List<AirportModel> sourceTable)
        {

            try
            {
                bool result = this.syncRepository.MigrateAirport(sourceTable);
                return ResultSet.SuccessResponse(ResponceCodes.Success.MigrationSuccessed, result, ManageEnum.status.success.ToString(), ResponceMessages.Success.MigrationSuccess);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }

        }


        public ApiResult MigrateCountry(List<CountryModel> sourceTable)
        {
            try
            {
                bool result = this.syncRepository.MigrateCountry(sourceTable);
                return ResultSet.SuccessResponse(ResponceCodes.Success.MigrationSuccessed, result, ManageEnum.status.success.ToString(), ResponceMessages.Success.MigrationSuccess);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult MigrateHotelType(List<HotelTypeModel> sourceTable)
        {
            try
            {
                bool result = this.syncRepository.MigrateHotelType(sourceTable);
                return ResultSet.SuccessResponse(ResponceCodes.Success.MigrationSuccessed, result, ManageEnum.status.success.ToString(), ResponceMessages.Success.MigrationSuccess);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult MigrateRestaurantType(List<RestaurantTypeModel> sourceTable)
        {
            try
            {
                bool result = this.syncRepository.MigrateRestaurantType(sourceTable);
                return ResultSet.SuccessResponse(ResponceCodes.Success.MigrationSuccessed, result, ManageEnum.status.success.ToString(), ResponceMessages.Success.MigrationSuccess);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }


        public ApiResult MigrateLocationType(List<LocationTypeModel> sourceTable)
        {
            try
            {
                bool result = this.syncRepository.MigrateLocationType(sourceTable);
                return ResultSet.SuccessResponse(ResponceCodes.Success.MigrationSuccessed, result, ManageEnum.status.success.ToString(), ResponceMessages.Success.MigrationSuccess);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult MigrateKitchen(List<KitchenModel> sourceTable)
        {
            try
            {
                bool result = this.syncRepository.MigrateKitchen(sourceTable);
                return ResultSet.SuccessResponse(ResponceCodes.Success.MigrationSuccessed, result, ManageEnum.status.success.ToString(), ResponceMessages.Success.MigrationSuccess);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }
        
        public ApiResult MigrateSuitablefor(List<SuitableForModel> sourceTable)
        {
            try
            {
                bool result = this.syncRepository.MigrateSuitableFor(sourceTable);
                return ResultSet.SuccessResponse(ResponceCodes.Success.MigrationSuccessed, result, ManageEnum.status.success.ToString(), ResponceMessages.Success.MigrationSuccess);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

        public ApiResult DeleteResource(List<KbStatusModel> statusModels)
        {
            try
            {
                bool result = this.syncRepository.DeleteResource(statusModels);
                return ResultSet.SuccessResponse(ResponceCodes.Success.MigrationSuccessed, result, ManageEnum.status.success.ToString(), ResponceMessages.Success.MigrationSuccess);
            }
            catch (System.Net.WebException ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.InternalServerError).ToString(), ex);
            }
            catch (Exception ex)
            {
                return ResultSet.ErrorResponse(((int)HttpStatusCode.BadRequest).ToString(), ex);
            }
        }

    }
}
